package com.decenturion.mobile.di.dagger.tokendetails.transaction;

import com.decenturion.mobile.business.tokendetails.transaction.ITransactionInteractor;
import com.decenturion.mobile.business.tokendetails.transaction.TransactionInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.traide.TraideRepository;
import com.decenturion.mobile.ui.fragment.token.transaction.presenter.ITransactionPresenter;
import com.decenturion.mobile.ui.fragment.token.transaction.presenter.TransactionPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionModule {

    @Provides
    @TransactionScope
    ITraideRepository provideITraideRepository(OrmHelper ormHelper) {
        return new TraideRepository(ormHelper);
    }

    @Provides
    @TransactionScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TransactionScope
    ITransactionInteractor provideITransactionInteractor(IResidentClient IResidentClient,
                                                         ITokenRepository ITokenRepository,
                                                         ITokenClient iTokenClient,
                                                         ITraideRepository iTraideRepository) {

        return new TransactionInteractor(IResidentClient,
                ITokenRepository,
                iTokenClient,
                iTraideRepository);
    }

    @Provides
    @TransactionScope
    ITransactionPresenter provideITransactionPresenter(ITransactionInteractor iTransactionInteractor) {
        return new TransactionPresenter(iTransactionInteractor);
    }
}
