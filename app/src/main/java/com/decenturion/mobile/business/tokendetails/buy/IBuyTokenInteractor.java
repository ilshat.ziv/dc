package com.decenturion.mobile.business.tokendetails.buy;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.network.request.SaleModel;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResult;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.decenturion.mobile.network.response.traide.sale.OnSaleResultResult;
import com.decenturion.mobile.network.response.traide.state.StateTraideResult;
import com.decenturion.mobile.ui.fragment.token.buy.model.BuyTokenModelView;

import rx.Observable;

public interface IBuyTokenInteractor {

    Observable<BuyTokenModelView> getByTokenInfo(int tokenId);

    Observable<BuyTokenModelView> getInfoSale();

    Observable<OnSaleResultResult> getInfoSale(double amount,
                                               @NonNull String category,
                                               @NonNull String coinUUID,
                                               @NonNull String cyrrency,
                                               @NonNull String residentUUID);

    Observable<CreateTraideResult> createTraide(@Nullable String receiveAddress,
                                                boolean isInternal,
                                                @NonNull String category,
                                                @NonNull String coinUUID,
                                                @NonNull String cyrrency,
                                                @NonNull String residentUUID);
}
