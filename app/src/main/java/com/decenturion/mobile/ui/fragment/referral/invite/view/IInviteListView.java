package com.decenturion.mobile.ui.fragment.referral.invite.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteListModelView;

public interface IInviteListView extends IScreenFragmentView {

    void onBindViewData(@NonNull InviteListModelView model);
}
