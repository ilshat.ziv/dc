package com.decenturion.mobile.ui.fragment.token.seller.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.token.details.ISellerTokenDetailsInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.seller.details.model.SellerTokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.token.seller.details.view.ISellerTokenDetailsView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SellerTokenDetailsPresenter extends Presenter<ISellerTokenDetailsView> implements ISellerTokenDetailsPresenter {

    private ISellerTokenDetailsInteractor mISellerTokenDetailsInteractor;
    private SellerTokenDetailsModelView mTokenDetailsModelView;

    public SellerTokenDetailsPresenter(ISellerTokenDetailsInteractor iSellerTokenDetailsInteractor) {
        mISellerTokenDetailsInteractor = iSellerTokenDetailsInteractor;
        mTokenDetailsModelView = new SellerTokenDetailsModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {

    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mTokenDetailsModelView = new SellerTokenDetailsModelView(tokenId);
        Observable<SellerTokenDetailsModelView> obs = mISellerTokenDetailsInteractor.getTokenDetails(tokenId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerTokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory) {
        showProgressView();

        mTokenDetailsModelView = new SellerTokenDetailsModelView(tokenId);
        mTokenDetailsModelView.setResidentUUID(residentUuid);
        mTokenDetailsModelView.setCoinUUID(tokenCoinUuid);
        mTokenDetailsModelView.setCategory(tokenCategory);

        Observable<SellerTokenDetailsModelView> obs = mISellerTokenDetailsInteractor.getTokenDetails(mTokenDetailsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerTokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SellerTokenDetailsModelView model) {

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTokenDetailsModelView = null;
    }
}
