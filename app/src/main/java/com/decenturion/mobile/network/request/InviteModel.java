package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteModel {

    @JsonProperty("email")
    private String mEmail;

    @JsonIgnore
    private String mToken;

    public InviteModel() {
    }

    @JsonIgnore
    public void setEmail(@NonNull String email) {
        mEmail = email;
    }

    @JsonIgnore
    public void setToken(String token) {
        mToken = token;
    }
}
