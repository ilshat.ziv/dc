package com.decenturion.mobile.repository.resident;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.Settings;
import com.decenturion.mobile.network.response.model.Wallet;

import java.util.List;

public interface IResidentRepository {

    void saveLocale(@NonNull String locale);

    void saveResidentInfo(@NonNull OrmResidentModel resident) throws Exception;

    void saveResidentInfo(@NonNull Resident resident) throws Exception;

    void savePassportInfo(@NonNull Passport passport) throws Exception;

    void savePhotoInfo(@NonNull Photo photo) throws Exception;

    void saveSettings(@NonNull Settings settings) throws Exception;

    void saveWallets(@NonNull List<Wallet> walletList) throws Exception;

    OrmResidentModel getResidentInfo();

    OrmPassportModel getPassportInfo();

    OrmPhotoModel getPhotoInfo();

    OrmSettingsModel getSettingsInfo();

    void clearData() throws Exception;

    void clearResidentInfo() throws Exception;
}
