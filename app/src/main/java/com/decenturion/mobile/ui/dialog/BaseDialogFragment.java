package com.decenturion.mobile.ui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.snack.ISnackManager;
import com.decenturion.mobile.app.snack.SnackManager;
import com.decenturion.mobile.app.snack.TypeMessage;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.architecture.view.IScreenDialogFragmentView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseDialogFragment extends DialogFragment implements IScreenDialogFragmentView {

    private Unbinder mUnbinder;
    protected ISnackManager mISnackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_MaterialDialog);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mISnackManager = new SnackManager(view);
        bindView(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindView();
    }

    private void bindView(@NonNull View view) {
        mUnbinder = ButterKnife.bind(this, view);
    }

    private void unbindView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    @Override
    public IScreenActivityView getIScreenActivityView() {
        return null;
    }

    @Override
    public void showMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Normal, String.valueOf(message));
    }

    @Override
    public void showSuccessMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Success, String.valueOf(message));
    }

    @Override
    public void showErrorMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Error, String.valueOf(message));
    }

    @Override
    public void showWarningMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Warning, String.valueOf(message));
    }

    @Override
    public void showProgressView() {

    }

    @Override
    public void hideProgressView() {

    }
}
