package com.decenturion.mobile.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.R;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class SpinnerDateDialog extends SingleDialogFragment implements DatePicker.OnDateChangedListener {

	@Nullable
	@BindView(R.id.datePickerView)
	protected DatePicker mDatePickerView;

	private OnChangeDateListener mOnChangeDateListener;
	private String mTitle;

	private long mDateTime;
	private int mDay;
	private int mMonth;
	private int mYear;

	public static void show(@NonNull FragmentManager fm, int requestCode,
							OnChangeDateListener changeDateListener,
							long date, String title) {
		final SpinnerDateDialog dialog = new SpinnerDateDialog(changeDateListener, date, title);

		Bundle bundle = new Bundle();
		bundle.putInt(Args.RQUEST_CODE, requestCode);
		dialog.setArguments(bundle);

		dialog.show(fm, SpinnerDateDialog.class.getName());
	}

	public SpinnerDateDialog() {
	}

	public SpinnerDateDialog(OnChangeDateListener changeDateListener) {
		mOnChangeDateListener = changeDateListener;
	}

	public SpinnerDateDialog(OnChangeDateListener changeDateListener, long datetime) {
		this(changeDateListener);
		mDateTime = (datetime == 0L) ? System.currentTimeMillis() : datetime;
	}

	public SpinnerDateDialog(OnChangeDateListener changeDateListener, String title) {
		this(changeDateListener);
		mDateTime = System.currentTimeMillis();
		mTitle = title;
	}

	public SpinnerDateDialog(OnChangeDateListener changeDateListener, long datetime, String title) {
		this(changeDateListener, datetime);
		mTitle = title;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		setTitleText(mTitle);
//		setPositiveText(R.string.fr_dialog_transaction_fail_positive);
//		setNegativeText(R.string.fr_dialog_transaction_fail_nagative);

		MaterialDialog dialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
		setContentView(R.layout.fr_dialog_spinner_date);

		ButterKnife.bind(this, dialog.getView());

		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

		setupUi();

		return dialog;
	}

	private void setupUi() {
		assert mDatePickerView != null;
		setDateTime(mDateTime);
		mDatePickerView.init(mYear, mMonth - 1, mDay, this);
		mDatePickerView.invalidate();
	}

	@Override
	public void onPositiveClickButton() {
		assert mDatePickerView != null;
		if (mOnChangeDateListener != null) {
			mOnChangeDateListener.changeData(getDate());
		}

		dismiss();
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mYear = year;
		mMonth = monthOfYear + 1;
		mDay = dayOfMonth;

		assert mDatePickerView != null;
		mDatePickerView.invalidate();
	}

	private void setDateTime(long date) {
		if (date > 0) {
			Date dates = new Date(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dates);
			mDay = calendar.get(Calendar.DAY_OF_MONTH);
			mMonth = calendar.get(Calendar.MONTH) + 1;
			mYear = calendar.get(Calendar.YEAR);
		}
	}

	@SuppressLint("SimpleDateFormat")
	public long getDate() {
		SimpleDateFormat format = new SimpleDateFormat(DateTimeUtils.Format.SIMPLE_FORMAT);
		try {
			Date date = format.parse(mDay + "." + mMonth + "." + mYear);
			return date.getTime();
		} catch (ParseException e) {
			LoggerUtils.exception(e);
			return 0;
		}
	}

	public interface OnChangeDateListener {

		/**
		 * @param date
		 *
		 * Данную дату надо перед отправкой конвертировать в дату по GMT
		 *
		 */

		void changeData(long date);
	}

	public interface Args {
		String RQUEST_CODE = "REQUEST_CODE";
		String TITLE = "TITLE";
	}
}