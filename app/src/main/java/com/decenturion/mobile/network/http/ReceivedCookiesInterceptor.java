package com.decenturion.mobile.network.http;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.token.TokenPrefsOptions;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

public class ReceivedCookiesInterceptor implements Interceptor {

    private IPrefsManager mIPrefsManager;

    public ReceivedCookiesInterceptor(@NonNull IPrefsManager IPrefsManager) {
        super();
        mIPrefsManager = IPrefsManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                if (header.contains("decenturion=")) {
                    cookies.add(header);
                }
            }

            if (!cookies.isEmpty() ) {
                mIPrefsManager.getITokensPrefsManager()
                        .setParam(TokenPrefsOptions.Keys.COOKIES, cookies);
            }
        }

        return originalResponse;
    }
}