package com.decenturion.mobile.di.dagger.token.details;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.token.details.ISellerTokenDetailsInteractor;
import com.decenturion.mobile.business.token.details.SellerTokenDetailsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.token.seller.details.presenter.ISellerTokenDetailsPresenter;
import com.decenturion.mobile.ui.fragment.token.seller.details.presenter.SellerTokenDetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SellerTokenDetailsModule {

    @Provides
    @SellerTokenDetailsScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @SellerTokenDetailsScope
    ISellerTokenDetailsInteractor provideISellerTokenDetailsInteractor(IResidentClient IResidentClient,
                                                          ITokenRepository ITokenRepository,
                                                          ITokenClient iTokenClient,
                                                          ILocalistionManager iLocalistionManager) {

        return new SellerTokenDetailsInteractor(IResidentClient, ITokenRepository, iTokenClient,
                iLocalistionManager);
    }

    @Provides
    @SellerTokenDetailsScope
    ISellerTokenDetailsPresenter provideISellerTokenDetailsPresenter(ISellerTokenDetailsInteractor iSellerTokenDetailsInteractor) {
        return new SellerTokenDetailsPresenter(iSellerTokenDetailsInteractor);
    }
}
