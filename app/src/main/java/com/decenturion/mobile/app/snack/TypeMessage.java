package com.decenturion.mobile.app.snack;

public enum TypeMessage {
    Normal, Success, Info, Warning, Error
}