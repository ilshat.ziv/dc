package com.decenturion.mobile.ui.fragment.score.details.model;

public class PriceModelView {

    private double mPrice;

    private String mCoin;

    public PriceModelView(double price, String coin) {
        mPrice = price;
        mCoin = coin;
    }

    public double getPrice() {
        return mPrice;
    }

    public String getCoin() {
        return mCoin;
    }
}
