package com.decenturion.mobile.business.settings.delivery.panding;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.model.DeliveryPendingSettingsModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import rx.Observable;

public class DeliveryPendingSettingsInteractor implements IDeliveryPendingSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public DeliveryPendingSettingsInteractor(IResidentClient IResidentClient,
                                             IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<DeliveryPendingSettingsModelView> getPassportInfo() {
        Observable<Boolean> obs2 = mIResidentClient.getResidencePaymentInfo()
                .map(d -> !CollectionUtils.isEmpty(d.getResult()));

        return Observable.zip(
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getResidentInfo()),
                obs2,
                DeliveryPendingSettingsModelView::new
        );
    }

}
