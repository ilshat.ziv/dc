package com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model.DeliveredSettingsModelView;

public interface IDeliveredSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull DeliveredSettingsModelView model3);
}
