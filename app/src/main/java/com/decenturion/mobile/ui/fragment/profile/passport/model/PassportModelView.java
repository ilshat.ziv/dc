package com.decenturion.mobile.ui.fragment.profile.passport.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.ReferralSettings;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResult;
import com.decenturion.mobile.ui.fragment.seller.passport.model.BannerModelView;
import com.decenturion.mobile.utils.DateTimeUtils;

public class PassportModelView {

    // Bio
    private String mName;
    private String mFirstname;
    private String mLastname;
    private String mBirth;
    private String mSex;
    private String mCountry;
    private String mCity;

    // Balance
    private String mCoin;
    private String mBalance;

    private String mMoney;
    private String mPower;
    private String mGlory;

    private PhotoModelView mPhotoModelView;
    private String mTelegramContact;
    private String mViberContact;
    private String mWechatContact;
    private String mWhatsappContact;

    private BannerModelView mBannerModelView;

    public PassportModelView() {
    }

    public PassportModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1, OrmPhotoModel m2) {
        // Balance
        mCoin = "DCNT";
        mBalance = m0.getBalance();

        mMoney = m0.getMoney();
        mPower = m0.getPower();
        mGlory = m0.getGlory();

        // Bio
        mName = m1.getName();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();
        long datetime = DateTimeUtils.getDatetime(m1.getBirth(), DateTimeUtils.Format.FORMAT_ISO_8601);
        datetime = DateTimeUtils.convertToUTC(datetime);
        mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);

        mSex = m1.getSex();
        mCountry = m1.getCountry();
        mCity = m1.getCity();

        mTelegramContact = m0.getTelegram();
        mWhatsappContact = m0.getWhatsApp();
        mWechatContact = m0.getWechat();
        mViberContact = m0.getViber();

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public PassportModelView(SellerResidentResult sellerResidentResult) {
        mCoin = "DCNT";
        mBalance = sellerResidentResult.getBalance();

        mMoney = sellerResidentResult.getMoney();
        mGlory = sellerResidentResult.getGlory();
        mPower = sellerResidentResult.getPower();

        mFirstname = sellerResidentResult.getFirstName();
        mLastname = sellerResidentResult.getLastName();

        String birth = sellerResidentResult.getBirth();
        if (!TextUtils.isEmpty(birth)) {
            long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.FORMAT_ISO_8601);
            datetime = DateTimeUtils.convertToUTC(datetime);
            mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);
        }

        mSex = sellerResidentResult.getSex();
        mCountry = sellerResidentResult.getCountry();
        mCity = sellerResidentResult.getCity();

        mTelegramContact = sellerResidentResult.getTelegram();
        mWhatsappContact = sellerResidentResult.getWhatsapp();
        mWechatContact = sellerResidentResult.getWechat();
        mViberContact = sellerResidentResult.getViber();

        ReferralSettings paySetting = sellerResidentResult.getPaySetting();
        if (paySetting != null && paySetting.isEnable()) {
            mBannerModelView = new BannerModelView(sellerResidentResult);
        }
//        String phone = sellerResidentResult.getPhone();
//        if (phone != null) {
//            mPhotoModelView = new PhotoModelView(phone);
//        }
    }

    public PassportModelView(ResidentResult residentResult) {
        mCoin = "DCNT";
        Resident r = residentResult.getResident();
        Passport p = residentResult.getPassport();
        mBalance = r.getBalance();

        mMoney = r.getMoney();
        mGlory = r.getGlory();
        mPower = r.getPower();

        mFirstname = p.getFirstname();
        mLastname = p.getLastname();

        String birth = p.getBirth();
        if (!TextUtils.isEmpty(birth)) {
            long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.FORMAT_ISO_8601);
            datetime = DateTimeUtils.convertToUTC(datetime);
            mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);
        }

        mSex = p.getSex();
        mCountry = p.getCountry();
        mCity = p.getCity();

        mTelegramContact = r.getTelegram();
        mWhatsappContact = r.getWhatsapp();
        mWechatContact = r.getWechat();
        mViberContact = r.getViber();

        Photo m2 = residentResult.getPhoto();
        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public String getCoin() {
        return mCoin;
    }

    public String getBalance() {
        return mBalance;
    }

    public String getName() {
        return mName;
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getBirth() {
        return mBirth;
    }

    public String getSex() {
        return mSex;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public String getMoney() {
        return mMoney;
    }

    public String getPower() {
        return mPower;
    }

    public String getGlory() {
        return mGlory;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }

    public String getTelegramContact() {
        return mTelegramContact;
    }

    public String getViberContact() {
        return mViberContact;
    }

    public String getWechatContact() {
        return mWechatContact;
    }

    public String getWhatsappContact() {
        return mWhatsappContact;
    }

    public BannerModelView getBannerModelView() {
        return mBannerModelView;
    }
}
