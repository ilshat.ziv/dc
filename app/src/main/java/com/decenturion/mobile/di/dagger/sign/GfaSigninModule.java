package com.decenturion.mobile.di.dagger.sign;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.sign.GfaSigninInteractor;
import com.decenturion.mobile.business.sign.IGfaSigninInteractor;
import com.decenturion.mobile.business.signin.ISigninInteractor;
import com.decenturion.mobile.business.signin.SigninInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.sign.gfa.presenter.GfaSigninPresenter;
import com.decenturion.mobile.ui.fragment.sign.gfa.presenter.IGfaSigninPresenter;
import com.decenturion.mobile.ui.fragment.sign.in.presenter.ISigninPresenter;
import com.decenturion.mobile.ui.fragment.sign.in.presenter.SigninPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class GfaSigninModule {

    @Provides
    @GfaSigninScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @GfaSigninScope
    IGfaSigninInteractor provideIGfaSigninInteractor(IResidentClient iResidentClient,
                                               IResidentRepository iResidentRepository) {
        return new GfaSigninInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @GfaSigninScope
    IGfaSigninPresenter provideIGfaSigninPresenter(
            IGfaSigninInteractor iSigninInteractor,
            IPrefsManager iPrefsManager) {

        return new GfaSigninPresenter(iSigninInteractor, iPrefsManager);
    }
}
