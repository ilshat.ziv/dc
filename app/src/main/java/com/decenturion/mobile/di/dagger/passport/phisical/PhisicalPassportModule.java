package com.decenturion.mobile.di.dagger.passport.phisical;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.passport.phisical.IPhisicalPassportInteractor;
import com.decenturion.mobile.business.passport.phisical.PhisicalPassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.phisical.presenter.IPhisicalPassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.phisical.presenter.PhisicalPassportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PhisicalPassportModule {

    @Provides
    @PhisicalPassportScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @PhisicalPassportScope
    IPhisicalPassportInteractor provideIPhisicalPassportInteractor(IResidentClient iResidentClient,
                                                                   IResidentRepository iResidentRepository,
                                                                   IPrefsManager iPrefsManager) {
        return new PhisicalPassportInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @PhisicalPassportScope
    IPhisicalPassportPresenter provideIPhisicalPassportPresenter(Context context,
                                                                 IPhisicalPassportInteractor iPhisicalPassportInteractor,
                                                                 IPrefsManager iPrefsManager,
                                                                 ILocalistionManager iLocalistionManager) {
        return new PhisicalPassportPresenter(context,
                iPhisicalPassportInteractor,
                iPrefsManager,
                iLocalistionManager);
    }
}
