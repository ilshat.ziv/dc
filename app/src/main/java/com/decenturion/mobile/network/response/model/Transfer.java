package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transfer {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("to")
    private String mTo;

    public Transfer() {}

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getTo() {
        return mTo;
    }
}
