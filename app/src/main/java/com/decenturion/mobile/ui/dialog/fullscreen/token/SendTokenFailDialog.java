package com.decenturion.mobile.ui.dialog.fullscreen.token;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.ui.dialog.fullscreen.FullScreenDialogFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendTokenFailDialog extends FullScreenDialogFragment {

    /* UI */

    @Nullable
    @BindView(R.id.messageTextView)
    TextView mMessageTextView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    public static void show(@NonNull FragmentManager fragmentManager) {
        final SendTokenFailDialog dialog = new SendTokenFailDialog();
        dialog.show(fragmentManager, SendTokenFailDialog.class.getName());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        AppComponent component = appDelegate
                .getIDIManager()
                .getAppComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_score_send_token_fail);
        setFullVisual(true);
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_send_token_fail_title));
        setEnabledPositiveClickButton(false);

        ButterKnife.bind(this, view);

        setupUI();

        return view;
    }

    private void setupUI() {
        String string = mILocalistionManager.getLocaleString(R.string.app_send_token_fail_message);
        Spanned text = Html.fromHtml(string);
        assert mMessageTextView != null;
        mMessageTextView.setText(text);
    }


    @OnClick(R.id.repeatTransferButtonView)
    protected void repeatTransfer() {
        dismiss();
    }
}