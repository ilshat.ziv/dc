package com.decenturion.mobile.ui.fragment.passport.online.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.passport.online.view.IOnlinePassportView;

public interface IOnlinePassportPresenter extends IPresenter<IOnlinePassportView> {

    void bindViewData();

    void saveViewData(@NonNull String value, @NonNull String value1, @NonNull String value2,
                      boolean checked, boolean checked1);

    void uploadPhoto(@NonNull Bitmap photo);

    void setPassportData(
            @NonNull String photo,
            @NonNull String firstName,
                         @NonNull String secondName,
                         long birth,
                         @NonNull String sex,
                         @NonNull String country,
                         @NonNull String city);
}

