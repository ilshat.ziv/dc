package com.decenturion.mobile.ui.activity.walkthrough;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.walkthrough.WalkthroughFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class WalkthroughActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
        } else {
            Bundle bundle = null;
            Intent intent = getIntent();
            if (intent != null) {
                bundle = intent.getExtras();
            }

            iNavigationManager.navigateTo(WalkthroughFragment.class, bundle);
        }
    }
}
