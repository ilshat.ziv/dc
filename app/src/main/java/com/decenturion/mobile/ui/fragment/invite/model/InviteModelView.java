package com.decenturion.mobile.ui.fragment.invite.model;

import android.support.annotation.NonNull;

public class InviteModelView {

    private String mInviteEmail;
    private String mToken;

    public InviteModelView() {
    }

    public String getInviteEmail() {
        return mInviteEmail;
    }

    public void setInviteEmail(@NonNull String email) {
        mInviteEmail = email;
    }

    public void setToken(@NonNull String token) {
        mToken = token;
    }

    public String getToken() {
        return mToken;
    }
}
