package com.decenturion.mobile.ui.fragment.settings.mail.presenter;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.mail.IMailingSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.subscribe.SubscribeResponse;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.mail.model.MailingSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.mail.view.IMailingSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MailingSettingsPresenter extends Presenter<IMailingSettingsView> implements IMailingSettingsPresenter {

    private IMailingSettingsInteractor mIMailingSettingsInteractor;
    private MailingSettingsModelView mMailingSettingsModelView;
    private ILocalistionManager mILocalistionManager;

    public MailingSettingsPresenter(IMailingSettingsInteractor iMailingSettingsInteractor,
                                    ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mIMailingSettingsInteractor = iMailingSettingsInteractor;
        mMailingSettingsModelView = new MailingSettingsModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMailingSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<MailingSettingsModelView> obs = mIMailingSettingsInteractor.getResidentInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MailingSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(MailingSettingsModelView model) {
        mMailingSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveChanges(boolean isSubscribe) {
        showProgressView();

        Observable<SubscribeResponse> obs = mIMailingSettingsInteractor.saveChanges(isSubscribe)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SubscribeResponse>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(SubscribeResponse response) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

}
