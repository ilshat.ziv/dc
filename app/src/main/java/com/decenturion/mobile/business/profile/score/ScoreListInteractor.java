package com.decenturion.mobile.business.profile.score;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreModelView;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class ScoreListInteractor implements IScoreListInteractor {

    private ILocalistionManager mILocalistionManager;

    public ScoreListInteractor(ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<ScoreListModelView> getScoreList() {
        List<ScoreModelView> list = new ArrayList<>();
        list.add(new ScoreModelView(
                mILocalistionManager.getLocaleString(R.string.app_score_main_title),
                R.drawable.ic_logo,
                ScoreType.MAIN
        ));
        list.add(new ScoreModelView(
                mILocalistionManager.getLocaleString(R.string.app_score_trade_title),
                R.drawable.ic_logo,
                ScoreType.TRADE
        ));
        list.add(new ScoreModelView(
                mILocalistionManager.getLocaleString(R.string.app_score_futures_title),
                R.drawable.ic_logo,
                ScoreType.FUTURES
        ));

        return Observable.just(new ScoreListModelView(list));
    }
}
