package com.decenturion.mobile.di.dagger.settings.action;

import com.decenturion.mobile.ui.fragment.settings.action.view.ActionListSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ActionListSettingsModule.class
})

@ActionListSettingsScope
public interface ActionListSettingsComponent {

    void inject(ActionListSettingsFragment view);
}
