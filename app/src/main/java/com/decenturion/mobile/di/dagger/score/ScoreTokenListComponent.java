package com.decenturion.mobile.di.dagger.score;

import com.decenturion.mobile.ui.fragment.profile_v2.score.item.view.ScoreTokenListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ScoreTokenListModule.class
})

@ScoreTokenListScope
public interface ScoreTokenListComponent {

    void inject(ScoreTokenListFragment view);
}
