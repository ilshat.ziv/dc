package com.decenturion.mobile.business.tokendetails.buy;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.request.CreateTraideModel;
import com.decenturion.mobile.network.request.SaleModel;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResponse;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.decenturion.mobile.network.response.traide.sale.OnSaleResponse;
import com.decenturion.mobile.network.response.traide.sale.OnSaleResultResult;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.ui.fragment.token.buy.model.BuyTokenModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class BuyTokenInteractor implements IBuyTokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private ITokenRepository mITokenRepository;
    private ITraideRepository mITraideRepository;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    public BuyTokenInteractor(IResidentClient IResidentClient,
                              ITokenClient iTokenClient,
                              ITokenRepository ITokenRepository,
                              IPrefsManager iPrefsManager,
                              ITraideRepository iTraideRepository,
                              ILocalistionManager iLocalistionManager) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mITokenRepository = ITokenRepository;
        mIPrefsManager = iPrefsManager;
        mITraideRepository = iTraideRepository;

        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<BuyTokenModelView> getByTokenInfo(int tokenId) {
        return Observable.just(mITokenRepository.getTokenInfoById(tokenId))
                .map(BuyTokenModelView::new);
    }

    @Override
    public Observable<BuyTokenModelView> getInfoSale() {
        return getInfoSale(1,
                "external",
                "699bc32c-5ff4-c539-c955-7d3441fcfd73",
                "btc",
                "5072f176-6258-6651-3ccd-2df23d121183")
                .map(BuyTokenModelView::new);
    }

    @Override
    public Observable<OnSaleResultResult> getInfoSale(double amount,
                                                      @NonNull String category,
                                                      @NonNull String coinUUID,
                                                      @NonNull String cyrrency,
                                                      @NonNull String residentUUID) {
        SaleModel model = new SaleModel();
        model.setAmount(String.valueOf(amount));
        model.setCategory(category);
        model.setCoinId(coinUUID);
        model.setCurrency(cyrrency);
        model.setResidentUuid(residentUUID);

        return mITokenClient.getTraideParams(model)
                .map(OnSaleResponse::getResult)
                .map(d -> {
                    if (d.isOverflow()) {
                        String localeString = mILocalistionManager.getLocaleString(R.string.app_account_buytokens_overflow);
                        d.setOnsaleAmount(localeString.replace("{coin}", d.getCoin().getSymbol()));
                    } else {
                        d.setOnsaleAmount(d.getOnsaleAmount() + " " + d.getCurrency());
                    }
                    return d;
                });
    }

    @Override
    public Observable<CreateTraideResult> createTraide(@Nullable String receiveAddress,
                                                       boolean isInternal,
                                                       @NonNull String category,
                                                       @NonNull String coinUUID,
                                                       @NonNull String cyrrency,
                                                       @NonNull String residentUUID) {
        CreateTraideModel model = new CreateTraideModel();
        model.setReceiveAddress(receiveAddress);
        model.setInternal(isInternal);
        model.setCategory(category);
        model.setCoinId(coinUUID);
        model.setCurrency(cyrrency);
        model.setResidentUuid(residentUUID);

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        String email = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, "");
        model.setEmail(email);

        return mITokenClient.createTraide(model)
                .map(CreateTraideResponse::getResult)
                .doOnNext(d -> {
                    try {
                        mITraideRepository.saveTraideInfo(d);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }
}
