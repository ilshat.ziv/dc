package com.decenturion.mobile.ui.fragment.settings.delivery.main.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;

public interface IDeliverySettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull CoinModelView model1,
                        @NonNull CountryModelView model2,
                        @NonNull DeliverySettingsModelView model3);

    void onBindPayModelView(@NonNull PaymentModelView model);

    void onDeliveryPassportDataSeted();
}
