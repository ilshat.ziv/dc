package com.decenturion.mobile.ui.fragment.settings.details.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPhotoModel;

public class PhotoModelView {

    private String mOriginal;
    private String mMin;

    public PhotoModelView() {
    }

    PhotoModelView(@NonNull OrmPhotoModel model) {
        mOriginal = model.getOriginal();
        mMin = model.getMin();
    }

    public String getOriginal() {
        return mOriginal;
    }

    public String getMin() {
        return mMin;
    }
}
