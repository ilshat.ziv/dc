package com.decenturion.mobile.ui.fragment.pass.restore.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.pass.restore.RestorePassComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.restore.RestorePassActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.view.ConfirmEmailRestorePassFragment;
import com.decenturion.mobile.ui.fragment.pass.restore.model.RestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.restore.presenter.IRestorePassPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class RestorePassFragment extends BaseFragment implements IRestorePassView,
        View.OnClickListener,
        ChangeComponentListener {

    /* UI */

    @BindView(R.id.contactView)
    EditTextView mContactView;

    @BindView(R.id.restorePassButtonView)
    LocalizedButtonView mRestorePassButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlView;
    private IFloatViewManager mIFloatViewManager;

    private String mContact;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IRestorePassPresenter mIRestorePassPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
        setHasOptionsMenu(true);

        FragmentActivity activity = getActivity();
        assert activity != null;
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mContact = arguments.getString(RestorePassActivity.CONTACT);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        RestorePassComponent restorePassComponent = appDelegate
                .getIDIManager()
                .plusRestorePassComponent();
        restorePassComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_restore_pass, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();
        mRestorePassButtonView.setEnabled(false);

        mContactView.showLabel();
        mContactView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_recovery_email_title));
        mContactView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_recovery_email_placeholder));
        mContactView.editMode();
        mContactView.setVisibleDivider(true);
        mContactView.setChangeDataModelListener(this);

        mIFloatViewManager.bindIFloatView(mControlView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_recovery_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIRestorePassPresenter.bindView(this);
        mIRestorePassPresenter.bindViewData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIRestorePassPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.restorePassButtonView)
    protected void onRestorePass() {
        mIRestorePassPresenter.restorePass(mContactView.getValue(), "");
    }

    @Override
    public void onRestorePassSuccess() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        Bundle bundle = new Bundle();
        bundle.putString(RestorePassActivity.CONTACT, mContactView.getValue());
        iNavigationManager.navigateTo(ConfirmEmailRestorePassFragment.class, bundle);
    }

    @Override
    public void onBindViewData(@NonNull RestorePassModelView model) {
        String contact = model.getContact();
        if (!TextUtils.isEmpty(contact)) {
            mContact = contact;
        }
        mContactView.setText(mContact);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIRestorePassPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlView);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onChangeComponentData(View view) {
        String email = mContactView.getValue();
        boolean b = !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

        mRestorePassButtonView.setEnabled(b);
    }
}
