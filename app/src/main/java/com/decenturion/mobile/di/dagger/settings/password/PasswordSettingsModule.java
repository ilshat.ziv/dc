package com.decenturion.mobile.di.dagger.settings.password;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.password.IPasswordSettingsInteractor;
import com.decenturion.mobile.business.settings.password.PasswordSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.password.presenter.IPasswordSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.password.presenter.PasswordSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PasswordSettingsModule {

    @Provides
    @PasswordSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @PasswordSettingsScope
    IPasswordSettingsInteractor provideIPasswordSettingsInteractor(IResidentClient iResidentClient,
                                                                  IResidentRepository iResidentRepository) {
        return new PasswordSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @PasswordSettingsScope
    IPasswordSettingsPresenter provideIPasswordSettingsPresenter(IPasswordSettingsInteractor iPasswordSettingsInteractor,
                                                                 ILocalistionManager iLocalistionManager) {
        return new PasswordSettingsPresenter(iPasswordSettingsInteractor, iLocalistionManager);
    }
}
