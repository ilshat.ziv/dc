package com.decenturion.mobile.ui.fragment.profile_v2.score.item.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.score.ScoreTokenListComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.score.ScoreActivity;
import com.decenturion.mobile.ui.activity.token.TokenActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.ScoreTokenListAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter.IScoreTokenListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.ui.fragment.score.details.view.ScoreTokenDetailsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class ScoreTokenListFragment extends BaseFragment implements IScoreTokenListView,
        ScoreTokenListAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private ScoreType mScoreType;

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.tokenListView)
    RecyclerView mTokenListView;
    private ScoreTokenListAdapter mTokenListAdapter;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IScoreTokenListPresenter mIScoreTokenListPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mScoreType = (ScoreType) bundle.getSerializable(ScoreActivity.SCORE_TYPE);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ScoreTokenListComponent component = appDelegate
                .getIDIManager()
                .plusScoreTokenListComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_token_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        initToolbar();
        mRefreshView.setOnRefreshListener(this);

        Context context = requireContext();
        mTokenListView.setLayoutManager(new LinearLayoutManager(context));
        mTokenListAdapter = new ScoreTokenListAdapter(context, this);
        mTokenListView.setAdapter(mTokenListAdapter);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        switch (mScoreType) {
            case MAIN : {
                iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_score_main_title));
                break;
            }
            case TRADE : {
                iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_score_trade_title));
                break;
            }
            case FUTURES : {
                iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_score_futures_title));
            }
        }

        iToolbarController.setNavigationOnClickListener(v -> {
            ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity1.getINavigationManager();
            iNavigationManager.navigateToBack();
        });
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIScoreTokenListPresenter.bindView(this);
        mIScoreTokenListPresenter.bindViewData(mScoreType);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIScoreTokenListPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIScoreTokenListPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull ScoreTokenListModelView model) {
        mTokenListAdapter.replaceDataSet(model.getTokentModelList());
    }

    @Override
    public void onSignout() {
    }

    @Override
    public void onItemClick(ScoreTokenModelView model) {
        ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity1.getINavigationManager();

        Bundle bundle = new Bundle();
        bundle.putInt(ScoreTokenDetailsFragment.TOKEN_ID, model.getId());
        bundle.putString(ScoreTokenDetailsFragment.TOKEN_COIN_UUID, model.getCoinUUID());
        bundle.putString(ScoreTokenDetailsFragment.TOKEN_CATEGORY, model.getCategory());
        bundle.putSerializable(TokenActivity.TOKEN_SCORE_TYPE, mScoreType);

        iNavigationManager.navigateTo(ScoreTokenDetailsFragment.class, true, bundle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIScoreTokenListPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mIScoreTokenListPresenter.bindViewData(mScoreType);
    }
}
