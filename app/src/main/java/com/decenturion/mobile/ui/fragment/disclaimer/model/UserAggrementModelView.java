package com.decenturion.mobile.ui.fragment.disclaimer.model;

import android.support.annotation.NonNull;

public class UserAggrementModelView {

    private String mTerms;

    public UserAggrementModelView() {
    }

    public UserAggrementModelView(@NonNull String terms) {
        mTerms = terms;
    }

    public String getTerms() {
        return mTerms;
    }
}
