package com.decenturion.mobile.di.dagger.score.trade;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.score.trade.ITradeScoreSendTokenInteractor;
import com.decenturion.mobile.business.score.trade.TradeScoreSendTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.trade.presenter.ITradeScoreSendTokenPresenter;
import com.decenturion.mobile.ui.fragment.score.send.trade.presenter.TradeScoreSendTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TradeScoreSendTokenModule {

    @Provides
    @TradeScoreSendTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TradeScoreSendTokenScope
    ITradeScoreSendTokenInteractor provideITradeScoreSendTokenInteractor(
            IResidentClient iResidentClient,
            IScoreClient iScoreClient,
            ITokenRepository iTokenRepository) {

        return new TradeScoreSendTokenInteractor(iResidentClient, iScoreClient, iTokenRepository);
    }

    @Provides
    @TradeScoreSendTokenScope
    ITradeScoreSendTokenPresenter provideITradeScoreSendTokenPresenter(
            ITradeScoreSendTokenInteractor iTradeScoreSendTokenInteractor,
            IPrefsManager iPrefsManager,
            ILocalistionManager iLocalistionManager) {

        return new TradeScoreSendTokenPresenter(
                iTradeScoreSendTokenInteractor,
                iPrefsManager, iLocalistionManager);
    }
}
