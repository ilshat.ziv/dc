package com.decenturion.mobile.di.dagger.tokendetails.details;

import com.decenturion.mobile.ui.fragment.token.details.view.TokenDetailsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TokenDetailsModule.class
})

@TokenDetailsScope
public interface TokenDetailsComponent {

    void inject(TokenDetailsFragment view);
}
