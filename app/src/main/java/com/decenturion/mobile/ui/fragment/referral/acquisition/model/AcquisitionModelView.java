package com.decenturion.mobile.ui.fragment.referral.acquisition.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;

public class AcquisitionModelView {

    private String mCostingMethod;
    private String mPrice;
    private String mTokenCount;
    private String mTokenBanner;

    public AcquisitionModelView(@NonNull ReferralSettingsResult d) {
        mCostingMethod = d.getCostingMethod();
        switch (mCostingMethod) {
            case "fixed" : {
                mPrice = d.getFixedCost();
                mTokenCount = d.getFixedTokenCount();
                mTokenBanner = d.getFixedComment();
                break;
            }
            case "percent" : {
                mPrice = String.valueOf(d.getPercent());
                mTokenCount = d.getPercentTokenCount();
                mTokenBanner = d.getPercentComment();
                break;
            }
        }
    }

    public String getCostingMethod() {
        return mCostingMethod;
    }

    public String getPrice() {
        return mPrice;
    }

    public String getTokenCount() {
        return mTokenCount;
    }

    public String getTokenBanner() {
        return mTokenBanner;
    }
}
