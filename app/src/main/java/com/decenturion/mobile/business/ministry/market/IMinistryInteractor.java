package com.decenturion.mobile.business.ministry.market;

import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryModelView;

import rx.Observable;

public interface IMinistryInteractor {

    Observable<MinistryModelView> getMinistryModelView();
}
