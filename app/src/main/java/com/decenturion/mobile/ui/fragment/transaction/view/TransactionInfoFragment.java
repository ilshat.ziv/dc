package com.decenturion.mobile.ui.fragment.transaction.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.transaction.TransactionInfoComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.transaction.model.TransactionInfoModelView;
import com.decenturion.mobile.ui.fragment.transaction.presenter.ITransactionInfoPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class TransactionInfoFragment extends BaseFragment implements ITransactionInfoView,
        View.OnClickListener {

    public static final String TRANSACTION_ID = "TRANSACTION_ID";

    public String mTransactionID;

    /* UI */

    @BindView(R.id.amountTraidView)
    TextView mAmountTraidView;


    @BindView(R.id.idTraidView)
    TextView mIdTraidView;

    @BindView(R.id.iconStateTraidView)
    ImageView mIconStateTraidView;

    @BindView(R.id.stateTraidView)
    TextView mStateTraidView;


    @BindView(R.id.iconStateTransactionView)
    ImageView mIconStateTransactionView;

    @BindView(R.id.stateTransactionView)
    TextView mStateTransactionView;


    @BindView(R.id.traidIdView)
    SimpleTextView mTraidIdView;

    @BindView(R.id.sendAddressView)
    SimpleTextView mSendAddressView;

    @BindView(R.id.receivAddressView)
    SimpleTextView mReceivAddressView;

    /* DI */

    @Inject
    ITransactionInfoPresenter mITraidInfoPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTransactionID = arguments.getString(TRANSACTION_ID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TransactionInfoComponent component = appDelegate
                .getIDIManager()
                .plusTransactionInfoComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_traide_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        mTraidIdView.showLabel();
        mTraidIdView.setLabel("ID");
        mTraidIdView.setHint("ID");

        mSendAddressView.showLabel();
        mSendAddressView.setLabel("Отправитель");
        mSendAddressView.setHint("Отправитель");

        mReceivAddressView.showLabel();
        mReceivAddressView.setLabel("Получатель");
        mReceivAddressView.setHint("Получатель");

        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle("");
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITraidInfoPresenter.bindView(this);
        mITraidInfoPresenter.bindViewData(mTransactionID);
    }

    @Override
    public void onResume() {
        super.onResume();
        mITraidInfoPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mITraidInfoPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mITraidInfoPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull TransactionInfoModelView model) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();

        long datetime = DateTimeUtils.getDatetime(model.getDateTraid(), DateTimeUtils.Format.FORMAT_ISO_8601);
        datetime = DateTimeUtils.convertToUTC(datetime);
        String title = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TITLE_TRAID_INFO_DATETIME);
        iToolbarController.setTitle(title);

        String amountTraid = model.getAmountTraid();

        if (amountTraid.contains("-")) {
            mAmountTraidView.setTextColor(Color.RED);
            mAmountTraidView.setText(amountTraid + " " + model.getCoin());
        } else {
            mAmountTraidView.setTextColor(Color.GREEN);
            mAmountTraidView.setText("+" + amountTraid + " " + model.getCoin());
        }

        String localeString = mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_deal_title);
        localeString = localeString.replace("{dealnumber}", model.getFrontendId());
        mIdTraidView.setText(localeString);
        mStateTraidView.setText(model.getTraidStatusName());
        String traidStatus = model.getTraidStatus();
        switch (traidStatus) {
            case "success": {
                mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_succes);
                mStateTraidView.setTextColor(Color.GREEN);
                break;
            }
            case "fail": {
                mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_fail);
                mStateTraidView.setTextColor(Color.RED);
                break;
            }
            case "fail-success": {
                mIconStateTraidView.setImageResource(R.drawable.bg_traide_state_fail_success);
                mStateTraidView.setTextColor(Color.GREEN);
                break;
            }
            case "done": {
                mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_done);
                mStateTraidView.setTextColor(Color.WHITE);
                break;
            }
        }

        String transactionStatus = model.getTransactionStatus();
        switch (transactionStatus) {
            case "success": {
                mIconStateTransactionView.setImageResource(R.drawable.bg_traid_state_succes);
                mStateTransactionView.setTextColor(Color.GREEN);
                break;
            }
            case "fail": {
                mIconStateTransactionView.setImageResource(R.drawable.bg_traid_state_fail);
                mStateTransactionView.setTextColor(Color.RED);
                break;
            }
            case "fail-success": {
                mIconStateTransactionView.setImageResource(R.drawable.bg_traide_state_fail_success);
                mStateTransactionView.setTextColor(Color.GREEN);
                break;
            }
            case "done": {
                mIconStateTransactionView.setImageResource(R.drawable.bg_traid_state_done);
                mStateTransactionView.setTextColor(Color.WHITE);
                break;
            }
            default : {
                mStateTransactionView.setVisibility(View.GONE);
                mIconStateTransactionView.setVisibility(View.GONE);
            }
        }

        String text = model.getIdTraide();
        mTraidIdView.setText(TextUtils.isEmpty(text) ? "-" : text);
        text = model.getSendAddresss();
        mSendAddressView.setText(TextUtils.isEmpty(text) ? "-" : text);
        text = model.getreceiveAddress();
        mReceivAddressView.setText(TextUtils.isEmpty(text) ? "-" : text);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
