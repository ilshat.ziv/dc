package com.decenturion.mobile.ui.dialog.fullscreen.token;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.ui.dialog.fullscreen.FullScreenDialogFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScoreSendTokenSuccessDialog extends FullScreenDialogFragment {

    public static final String COIN = "COIN";
    public static final String AMOUNT = "AMOUNT";
    public static final String TYPE_TRANSACTION = "TYPE_TRANSACTION";

    private String mCoin;
    private String mAmount;
    private int mTypeTransaction;

    /* UI */

    @Nullable
    @BindView(R.id.messageTextView)
    TextView mMessageTextView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
    }

    private void initArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCoin = bundle.getString(COIN);
            mAmount = bundle.getString(AMOUNT);
            mTypeTransaction = bundle.getInt(TYPE_TRANSACTION, 0);
        }
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        AppComponent component = appDelegate
                .getIDIManager()
                .getAppComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_score_send_token_success);
        setFullVisual(true);
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_send_token_success_title));
        setEnabledPositiveClickButton(false);

        ButterKnife.bind(this, view);

        setupUI();

        return view;
    }

    private void setupUI() {
        String text = "";
        switch (mTypeTransaction) {
            case 0 : {
                text = mILocalistionManager.getLocaleString(R.string.app_send_token_success_tocitizen_message);
                break;
            }
            case 1 : {
                text = mILocalistionManager.getLocaleString(R.string.app_send_token_success_toexternal_message);
                break;
            }
            case 2 : {
                text = mILocalistionManager.getLocaleString(R.string.app_send_token_success_totrade_message);
                break;
            }
            case 3 : {
                text = mILocalistionManager.getLocaleString(R.string.app_send_token_success_tomain_message);
                break;
            }
        }

        text = text.replace("{count}", mAmount);
        text = text.replace("{symbol}", mCoin);
        mMessageTextView.setText(text);
    }

    @OnClick(R.id.showTransactionsButtonView)
    protected void showTransactions() {
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        FragmentActivity activity = requireActivity();
        activity.finish();
    }

    public static void show(@NonNull FragmentManager fragmentManager, @NonNull Bundle bundle) {
        final ScoreSendTokenSuccessDialog dialog = new ScoreSendTokenSuccessDialog();
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, ScoreSendTokenSuccessDialog.class.getName());
    }

}