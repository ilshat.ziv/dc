package com.decenturion.mobile.ui.fragment.profile_v2.deals.presenter;

import com.decenturion.mobile.business.profile.traids.IDealListInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.view.IDealListView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DealListPresenter extends Presenter<IDealListView> implements IDealListPresenter {

    private IDealListInteractor mITraidsInteractor;
    private DealListModelView mDealListModelView;

    private boolean isLoadingPage;

    public DealListPresenter(IDealListInteractor iTraidsInteractor) {
        mITraidsInteractor = iTraidsInteractor;
    }

    @Override
    public void bindViewData() {
        if (isLoadingPage) {
            return;
        }

        showProgressView();

        Observable<DealListModelView> obs = mITraidsInteractor.getDealsInfo(
                new DealListModelView()
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DealListModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(DealListModelView model) {
        mDealListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void onBottomScrolled() {
        if (mDealListModelView.getOffset() >= mDealListModelView.getTotal() || isLoadingPage) {
            return;
        }
        uploadData();
    }

    private void uploadData() {
        showProgressView();

        Observable<DealListModelView> obs = mITraidsInteractor.getDealsInfo(
                mDealListModelView
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DealListModelView>) this::uploadDataSuccess,
                this::uploadDataFailure
        );
    }

    private void uploadDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadDataSuccess(DealListModelView model) {
        mDealListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        isLoadingPage = true;
    }

    @Override
    public void hideProgressView() {
        super.hideProgressView();
        isLoadingPage = false;
    }
}
