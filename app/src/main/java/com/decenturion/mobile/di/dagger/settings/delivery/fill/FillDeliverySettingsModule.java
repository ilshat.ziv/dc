package com.decenturion.mobile.di.dagger.settings.delivery.fill;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.fill.FillDeliverySettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.fill.IFillDeliverySettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.presenter.FillDeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.presenter.IFillDeliverySettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class FillDeliverySettingsModule {

    @Provides
    @FillDeliverySettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @FillDeliverySettingsScope
    IFillDeliverySettingsInteractor provideIFillDeliverySettingsInteractor(IResidentClient iResidentClient,
                                                                           IResidentRepository iResidentRepository,
                                                                           IPrefsManager iPrefsManager) {
        return new FillDeliverySettingsInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @FillDeliverySettingsScope
    IFillDeliverySettingsPresenter provideICreateDeliverySettingsPresenter(Context context,
                                                                           IFillDeliverySettingsInteractor iPhisicalPassportInteractor,
                                                                           IPrefsManager iPrefsManager,
                                                                           ILocalistionManager iLocalistionManager) {
        return new FillDeliverySettingsPresenter(context,
                iPhisicalPassportInteractor,
                iPrefsManager,
                iLocalistionManager);
    }
}
