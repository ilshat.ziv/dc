package com.decenturion.mobile.ui.fragment.pass.confirm.phone.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.pass.confirm.phone.ConfirmPhoneRestorePassComponent;
import com.decenturion.mobile.ui.activity.sign.ISignActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.model.ConfirmPhoneRestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.presenter.IConfirmPhoneRestorePassPresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmPhoneRestorePassFragment extends BaseFragment implements IConfirmPhoneRestorePassView {

    /* UI */

    @BindView(R.id.codeView)
    EditTextView mCodeView;

    @BindView(R.id.labelView)
    TextView mlabelView;


    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IConfirmPhoneRestorePassPresenter mIRestorePassPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ConfirmPhoneRestorePassComponent confirmPhoneRestorePassComponent = appDelegate
                .getIDIManager()
                .plusConfirmPhoneRestorePassComponent();
        confirmPhoneRestorePassComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_restore_pass_confirm_phone, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mCodeView.showLabel();
        mCodeView.setLabel("Код активации");
        mCodeView.setHint("Код активации");
        mCodeView.editMode();
        mCodeView.setVisibleDivider(true);
    }

    private void initToolbar() {
        ISignActivity activity = (ISignActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
//        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_title));
        iToolbarController.setTitle("Восстановление пароля");
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIRestorePassPresenter.bindView(this);
        mIRestorePassPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIRestorePassPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIRestorePassPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.sendCodeButtonView)
    protected void onSendCode() {
        mIRestorePassPresenter.sendSmsCode(mCodeView.getValue());
    }

    @Override
    public void onRestorePassSuccess() {
    }

    @Override
    public void onBindViewData(@NonNull ConfirmPhoneRestorePassModelView model) {
        mlabelView.setText("Смс-код для восстановления пароля в " +
                "течение нескольких минут будет отправлен на телефон " + model.getPhone() +
                " Введите код активации в поле ниже.");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIRestorePassPresenter.unbindView();
    }
}
