package com.decenturion.mobile.di.dagger.tokendetails.info;

import com.decenturion.mobile.ui.fragment.token.info.v2.view.CategoryTokenFragment;
import com.decenturion.mobile.ui.fragment.token.info.v1.view.TokenInfoFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TokenInfoModule.class
})

@TokenInfoScope
public interface TokenInfoComponent {

    void inject(TokenInfoFragment view);

    void inject(CategoryTokenFragment view);
}
