package com.decenturion.mobile.ui.activity.token;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.token.buy.view.BuyTokenFragment;
import com.decenturion.mobile.ui.fragment.token.details.view.TokenDetailsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.utils.LoggerUtils;

public class TokenActivity extends SingleFragmentActivity {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_COIN_UUID = "TOKEN_COIN_UUID";
    public static final String TOKEN_SCORE_TYPE = "TOKEN_SCORE_TYPE";
    public static final String TOKEN_CATEGORY = "TOKEN_CATEGORY";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";

    public static final String ACTION_BY_TOKEN = "ACTION_BY_TOKEN";
    public static final int ACTION_BUY_TOKEN = 1;

    private int mActionByToken;

    private String mTokenCoinUuid;
    private int mTokenId;

    private Bundle mBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
        } else {
            switch (mActionByToken) {
                case ACTION_BUY_TOKEN: {
                    iNavigationManager.navigateTo(BuyTokenFragment.class, mBundle);
                    break;
                }
                default: {
                    iNavigationManager.navigateTo(TokenDetailsFragment.class, mBundle);
                    break;
                }
            }
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mBundle = intent.getExtras();

            mTokenId = intent.getIntExtra(TOKEN_ID, 0);
            mActionByToken = intent.getIntExtra(ACTION_BY_TOKEN, 0);
            mTokenCoinUuid = intent.getStringExtra(TOKEN_COIN_UUID);
        }

        if (mTokenId == 0 && TextUtils.isEmpty(mTokenCoinUuid)) {
            LoggerUtils.exception(new Exception("TOKEN_ID is not be 0 or TOKEN_COIN_UUID is not be emty"));
            finish();
        }
    }
}
