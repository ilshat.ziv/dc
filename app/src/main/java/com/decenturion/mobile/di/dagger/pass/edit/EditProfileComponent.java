package com.decenturion.mobile.di.dagger.pass.edit;

import dagger.Subcomponent;

@Subcomponent(modules = {
        EditProfileModule.class
})

@EditProfileScope
public interface EditProfileComponent {
}
