package com.decenturion.mobile.business.seller.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;

import rx.Observable;

public interface ISellerTokenInteractor {

    Observable<TokenModelView> getTokenModelView(@NonNull String residentUUID);
}
