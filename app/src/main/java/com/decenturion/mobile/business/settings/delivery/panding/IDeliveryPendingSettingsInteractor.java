package com.decenturion.mobile.business.settings.delivery.panding;

import com.decenturion.mobile.ui.fragment.settings.delivery.pending.model.DeliveryPendingSettingsModelView;

import rx.Observable;

public interface IDeliveryPendingSettingsInteractor {

    Observable<DeliveryPendingSettingsModelView> getPassportInfo();
}
