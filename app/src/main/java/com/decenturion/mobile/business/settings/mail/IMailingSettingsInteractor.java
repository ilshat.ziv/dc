package com.decenturion.mobile.business.settings.mail;

import com.decenturion.mobile.network.response.subscribe.SubscribeResponse;
import com.decenturion.mobile.ui.fragment.settings.mail.model.MailingSettingsModelView;

import rx.Observable;

public interface IMailingSettingsInteractor {

    Observable<MailingSettingsModelView> getResidentInfo();

    Observable<SubscribeResponse> saveChanges(boolean isSubscribe);
}
