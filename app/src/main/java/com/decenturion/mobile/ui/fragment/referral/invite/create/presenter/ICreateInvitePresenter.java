package com.decenturion.mobile.ui.fragment.referral.invite.create.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.invite.create.view.ICreateInviteView;

public interface ICreateInvitePresenter extends IPresenter<ICreateInviteView> {

    void createInvite(String price);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
