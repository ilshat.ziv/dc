package com.decenturion.mobile.ui.fragment.referral.invite.create.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.referral.invite.create.ICreateInviteInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.referral.invite.create.CreateInviteResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.invite.create.view.ICreateInviteView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CreateInvitePresenter extends Presenter<ICreateInviteView> implements ICreateInvitePresenter {

    private ICreateInviteInteractor mICreateInviteInteractor;

    public CreateInvitePresenter(ICreateInviteInteractor iCreateInviteInteractor) {
        mICreateInviteInteractor = iCreateInviteInteractor;
    }

    @Override
    public void createInvite(String price) {
        showProgressView();

        Observable<CreateInviteResult> obs = mICreateInviteInteractor.createInvite(price)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<CreateInviteResult>) this::createInviteSuccess,
                this::createInviteFailure
        );
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    private void createInviteFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void createInviteSuccess(CreateInviteResult model) {
        hideProgressView();

        if (mView != null) {
            mView.onCreateInviteSuccess(model.getUrl());
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
