package com.decenturion.mobile.business.score.main;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.request.score.send.SendToExternalModel;
import com.decenturion.mobile.network.request.score.send.SendToTradeModel;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendTokenModelView;

import rx.Observable;

public interface IMainScoreSendTokenInteractor {

    Observable<MainScoreSendTokenModelView> getCoinInfo(@NonNull MainScoreSendTokenModelView model);

    Observable<SendTokenResult> sendToCitizen(
            @NonNull String coinUUID,
            @NonNull String coinCategory,
            @NonNull String dcntWallet,
            @NonNull String amount,
            @NonNull String method,
            @NonNull String twofa);

    Observable<SendTokenResult> sendToExternal(@NonNull SendToExternalModel model);

    Observable<SendTokenResult> sendToTrade(@NonNull SendToTradeModel model);
}
