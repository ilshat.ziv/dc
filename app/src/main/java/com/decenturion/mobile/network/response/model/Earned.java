package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Earned {

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("amount")
    private String mAmount;

    // в earned - будут начисленные суммы, в новом поле paid - выплаченные

    @JsonProperty("earned")
    private String mEarned;

    @JsonProperty("paid")
    private String mPaid;


    public Earned() {
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getEarned() {
        return mEarned;
    }

    @JsonIgnore
    public String getPaid() {
        return mPaid;
    }
}
