package com.decenturion.mobile.ui.fragment.referral.acquisition.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.di.dagger.referral.acquisition.AcquisitionComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.MultiEditTextView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.acquisition.model.AcquisitionModelView;
import com.decenturion.mobile.ui.fragment.referral.acquisition.presenter.IAcquisitionPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class AcquisitionFragment extends BaseFragment implements IAcquisitionView {

    /* UI */

    @BindView(R.id.acquisitionTextView)
    LocalizedTextView mAcquisitionTextView;

    @BindView(R.id.priceView)
    EditTextView mPriceView;

    @BindView(R.id.usdView)
    Button mUsdView;

    @BindView(R.id.percentView)
    Button mPercentView;

    @BindView(R.id.amountTokensView)
    EditTextView mAmountTokensView;

    @BindView(R.id.textForBannerView)
    MultiEditTextView mTextForBannerView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IResourceManager mIResourceManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IAcquisitionPresenter mIAcquisitionPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        AcquisitionComponent component = appDelegate
                .getIDIManager()
                .plusAcquisitionComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_acquisition, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mAcquisitionTextView.setText(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_by_usdt_text));

        mPriceView.setLabel(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_price_title));
        mPriceView.setHint(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_price_placeholder));
        mPriceView.setTypeInput(InputType.TYPE_CLASS_NUMBER);

        mAmountTokensView.setLabel(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_amount_title));
        String string1 = mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_amount_placeholder);
        mAmountTokensView.setHint(string1.replace("{count}", String.valueOf(0)));
        mAmountTokensView.setVisibleDivider(true);
        mAmountTokensView.setTypeInput(InputType.TYPE_CLASS_NUMBER);

        mTextForBannerView.setLabel(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_banner_title));
        String string = mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_banner_placeholder);
        mTextForBannerView.setHint(string.replace("{count}", String.valueOf(0)));
        mTextForBannerView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_title));
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        iToolbarController.setNavigationOnClickListener(v -> {
            ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity1.getINavigationManager();
            iNavigationManager.navigateToBack();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIAcquisitionPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIAcquisitionPresenter.bindViewData();
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIAcquisitionPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onBindViewData(@NonNull AcquisitionModelView model) {
        switch (model.getCostingMethod()) {
            case "percent" : {
                onSetPercent();
                break;
            }
            default : {
                onSetUsd();
            }
        }

        mPriceView.setText(model.getPrice());
        mAmountTokensView.setText(model.getTokenCount());
        mTextForBannerView.setText(model.getTokenBanner());
    }

    @Override
    public void onResume() {
        super.onResume();
        mIAcquisitionPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIAcquisitionPresenter.unbindView();
        mIAcquisitionPresenter.onSaveInstanceState(outState);
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIAcquisitionPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @OnClick(R.id.usdView)
    protected void onSetUsd() {
        mUsdView.setBackground(mIResourceManager.getDrawable(R.drawable.bg_empty_square_white));
        mUsdView.setTextColor(mIResourceManager.getColor(R.color.white));
        mUsdView.setTag(true);

        mPercentView.setBackground(mIResourceManager.getDrawable(R.drawable.bg_fill_square_white));
        mPercentView.setTextColor(mIResourceManager.getColor(R.color.black));
        mPercentView.setTag(false);
    }

    @OnClick(R.id.percentView)
    protected void onSetPercent() {
        mPercentView.setBackground(mIResourceManager.getDrawable(R.drawable.bg_empty_square_white));
        mPercentView.setTextColor(mIResourceManager.getColor(R.color.white));
        mPercentView.setTag(true);

        mUsdView.setBackground(mIResourceManager.getDrawable(R.drawable.bg_fill_square_white));
        mUsdView.setTextColor(mIResourceManager.getColor(R.color.black));
        mUsdView.setTag(false);
    }

    @OnClick(R.id.setPriceButtonView)
    protected void onSetPriceAcquisition() {
        boolean b = (Boolean) mPercentView.getTag();

        mIAcquisitionPresenter.setPrice(
                b ? "percent" : "fixed",
                mPriceView.getValue(),
                mAmountTokensView.getValue(),
                mTextForBannerView.getValue()
        );
    }

    @Override
    public void onSetPriceSuccess() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
