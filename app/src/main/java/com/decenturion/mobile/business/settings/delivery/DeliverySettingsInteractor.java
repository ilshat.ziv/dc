package com.decenturion.mobile.business.settings.delivery;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.DeliveryPassportModel;
import com.decenturion.mobile.network.request.PaymentModel;
import com.decenturion.mobile.network.request.UpdatePassportModel;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class DeliverySettingsInteractor implements IDeliverySettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public DeliverySettingsInteractor(IResidentClient IResidentClient,
                                      IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<DeliverySettingsModelView> getPassportInfo() {
        Observable<Boolean> obs2 = mIResidentClient.getResidencePaymentInfo()
                .map(d -> !CollectionUtils.isEmpty(d.getResult()));

        return Observable.zip(
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getResidentInfo()),
                obs2,
                DeliverySettingsModelView::new
        );
    }

    @Override
    public Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName,
                                                          @NonNull String secondName,
                                                          long birth,
                                                          @NonNull String sex,
                                                          @NonNull String country,
                                                          @NonNull String city) {
        UpdatePassportModel model = new UpdatePassportModel();
        model.setFirstName(firstName);
        model.setLastName(secondName);
        model.setBirth(DateTimeUtils.getDateFormat(birth, DateTimeUtils.Format.SIMPLE_FORMAT_2));
        model.setSex(sex);
        model.setCountry(country);
        model.setCity(city);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.updatePassport(m))
                .doOnNext(d -> {
                    try {
                        UpdatePassportResult result = d.getResult();
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    private String isVaildSex(String sex) {
        if (TextUtils.isEmpty(sex)) {
            throw new RuntimeException("Sex is empty");
        }
        return sex;
    }

    private long isVaildBirth(long birth) {
        if (birth == 0) {
            throw new RuntimeException("Birth is not 0");
        }
        return birth;
    }

    private String isVaildFirstName(String firstName) {
        if (TextUtils.isEmpty(firstName)) {
            throw new RuntimeException("FirstName is empty");
        }
        return firstName;
    }

    private String isVaildSecondName(String secondName) {
        if (TextUtils.isEmpty(secondName)) {
            throw new RuntimeException("SecondName is empty");
        }
        return secondName;
    }

    @Override
    public Observable<DeliverySettingsModelView> setDeliveryData(@NonNull String phone,
                                                              @NonNull String address,
                                                              @NonNull String city,
                                                              @NonNull String state,
                                                              @NonNull String country,
                                                              @NonNull String zipcode) {

        DeliveryPassportModel model = new DeliveryPassportModel();
        model.setPhone(phone);
        model.setAddress(address);
        model.setCity(city);
        model.setState(state);
        model.setCountry(country);
        model.setZip(zipcode);


        return Observable.just(model)
                .map(this::isValidModel)
                .flatMap(m -> mIResidentClient.delivery(m))
                .map(d -> {
                    DeliveryPassportResult result = d.getResult();
                    try {
                        mIResidentRepository.savePassportInfo(result.getPassport());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    return result;
                })
                .map(DeliverySettingsModelView::new);
    }

    private DeliveryPassportModel isValidModel(DeliveryPassportModel d) {
        isVaildState(d.getState());
        isVaildAddress(d.getAddress());
        isVaildCity(d.getCity());
        isVaildCountry(d.getCountry());
        isVaildPhone(d.getPhone());
        isVaildZipCode(d.getZip());
        return d;
    }

    private void isVaildState(String state) {
        if (TextUtils.isEmpty(state)) {
            throw new RuntimeException("State is empty");
        }
    }

    private void isVaildAddress(String address) {
        if (TextUtils.isEmpty(address)) {
            throw new RuntimeException("Address is empty");
        }
    }

    private void isVaildCity(String city) {
        if (TextUtils.isEmpty(city)) {
            throw new RuntimeException("City is empty");
        }
    }

    private void isVaildCountry(String country) {
        if (TextUtils.isEmpty(country)) {
            throw new RuntimeException("Country is empty");
        }
    }

    private void isVaildPhone(String phone) {
        if (TextUtils.isEmpty(phone)) {
            throw new RuntimeException("Phone is empty");
        }
    }

    private void isVaildZipCode(String zipcode) {
        if (TextUtils.isEmpty(zipcode)) {
            throw new RuntimeException("ZipCode is empty");
        }
    }

    @Override
    public Observable<PaymentModelView> getPaymentAddress(@NonNull String coin) {
        return Observable.just(new PaymentModel(coin))
                .flatMap(d -> mIResidentClient.paymentDeliver(d))
                .map(DeliverPaymentResponse::getResult)
                .map(PaymentModelView::new);
    }
}
