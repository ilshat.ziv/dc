package com.decenturion.mobile.business.passport.online;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.ui.fragment.passport.online.model.OnlinePassportModelView;

import rx.Observable;

public interface IOnlinePassportInteractor {

    Observable<OnlinePassportModelView> getProfileInfo();

    Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap);

    Observable<UpdatePassportResult> setPassportData(@NonNull String photo,
                                                     @NonNull String firstName,
                                                     @NonNull String secondName,
                                                     long birth,
                                                     @NonNull String sex,
                                                     @NonNull String country,
                                                     @NonNull String city);

}
