package com.decenturion.mobile.di.dagger.token;

import com.decenturion.mobile.ui.fragment.profile_v2.token.view.TokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TokenModule.class
})

@TokenScope
public interface TokenComponent {

    void inject(TokenFragment view);
}
