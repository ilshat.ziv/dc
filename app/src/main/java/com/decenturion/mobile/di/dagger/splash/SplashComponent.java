package com.decenturion.mobile.di.dagger.splash;

import com.decenturion.mobile.ui.activity.splash.view.SplashScreenActivity;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SplashModule.class
})

@SplashScope
public interface SplashComponent {

    void inject(SplashScreenActivity view);
}
