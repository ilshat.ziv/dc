package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketCoinsToken {

    @JsonProperty("ask_price")
    private String mAskPrice;

    @JsonProperty("category")
    private String mCategroy;

    @JsonProperty("coin")
    private TokenCoin mTokenCoin;

    @JsonProperty("startup")
    private Startup mStartup;

    public MarketCoinsToken() {
    }

    @JsonIgnore
    public String getAskPrice() {
        return mAskPrice;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategroy;
    }

    @JsonIgnore
    public TokenCoin getTokenCoin() {
        return mTokenCoin;
    }

    @JsonIgnore
    public Startup getStartup() {
        return mStartup;
    }
}
