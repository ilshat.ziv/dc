package com.decenturion.mobile.ui.dialog.bottomsheet;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.photo.CropImageActivity;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.CameraDialog;
import com.github.florent37.camerafragment.configuration.Configuration;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoDialogFragment extends BaseBottomSheetDialog {

    private boolean isCropImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
    }

    private void initArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isCropImage = bundle.getBoolean(Args.IS_CROP_IMAGE, false);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fr_bs_get_photo, null);
        dialog.setContentView(contentView);

        ButterKnife.bind(this, contentView);
//        setTitle(getString(R.string.set_source));

        setPeekHeight(contentView);
    }

    @OnClick(R.id.getFromCameraView)
    protected void getFromCamera() {
        startCameraActivity();
    }


    @OnClick(R.id.getFromGalleryView)
    protected void getFromGallery() {
        startGalery();
    }

    @SuppressLint("WrongConstant")
    private void startCameraActivity() {
        Activity activity = requireActivity();
        if (isCropImage) {
            Intent intent = new Intent(getActivity(), CropImageActivity.class);
            intent.putExtra("requestCode", IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP);
            activity.startActivityForResult(intent, IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP);
        } else {
            Configuration configuration = new Configuration.Builder().build();
            CameraDialog.show(getFragmentManager(), configuration, IMainActivity.ACTION_IMAGE_FROM_CAMERA);
        }


        dismiss();
    }

    @SuppressLint("WrongConstant")
    private void startGalery() {
        Activity activity = requireActivity();
        final String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
//                Toast.makeText(activity, "Show rationale", Toast.LENGTH_LONG).show();
//            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, IMainActivity.REQUEST_IMAGE_CAPTURE);
//            }

        } else {
            if (isCropImage) {
                Intent intent = new Intent(activity, CropImageActivity.class);
                intent.putExtra("requestCode", IMainActivity.ACTION_IMAGE_FROM_GALARY_AND_CROP);
                activity.startActivityForResult(intent, IMainActivity.ACTION_IMAGE_FROM_GALARY_AND_CROP);
            } else {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);//todo надо написать адаптацию
                activity.startActivityForResult(pickPhoto, IMainActivity.ACTION_IMAGE_FROM_GALARY);
            }

            dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case IMainActivity.REQUEST_CAMERA_PERMISSION: {
                startCameraActivity();
                break;
            }
            case IMainActivity.REQUEST_IMAGE_CAPTURE: {
                startGalery();
                break;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMainActivity.REQUEST_CAMERA_PERMISSION: {
                startCameraActivity();
                break;
            }
            case IMainActivity.REQUEST_IMAGE_CAPTURE: {
                startGalery();
                break;
            }
        }
    }

    public interface Args {
        String TEMP_FILE_NAME = "TEMP_FILE_NAME";
        String IS_CROP_IMAGE = "IS_CROP_IMAGE";
    }
}
