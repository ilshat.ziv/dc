package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnableGfaResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean mResult;

    public EnableGfaResponse() {
    }

    @JsonIgnore
    public boolean isResult() {
        return mResult;
    }
}
