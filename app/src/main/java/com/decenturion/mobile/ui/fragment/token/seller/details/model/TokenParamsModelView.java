package com.decenturion.mobile.ui.fragment.token.seller.details.model;

public class TokenParamsModelView {

    private String mName;
    private String mValue;
    private int mIconRes;

    public TokenParamsModelView(String name, String value, int iconRes) {
        mName = name;
        mValue = value;
        mIconRes = iconRes;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }

    public int getIconRes() {
        return mIconRes;
    }
}
