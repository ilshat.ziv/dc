package com.decenturion.mobile.ui.fragment.pass.reset.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.pass.reset.ResetPassComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.PasswordView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.reset.model.ResetPasswordModelView;
import com.decenturion.mobile.ui.fragment.pass.reset.presenter.IResetPasswordPresenter;
import com.decenturion.mobile.ui.fragment.validate.view.ValidateEmailFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ResetPasswordFragment extends BaseFragment implements IResetPasswordView
        , View.OnClickListener {

    private String mToken;

    /* UI */

    @BindView(R.id.newPasswordView)
    PasswordView mNewPasswordView;

    @BindView(R.id.retypePasswordView)
    PasswordView mRetypePasswordView;

    @BindView(R.id.saveChangesButtonView)
    FloatView mSaveChangesButtonView;


    /* DI */

    @Inject
    IResetPasswordPresenter mIResetPasswordPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mToken = arguments.getString(SignActivity.RESET_TOKEN);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ResetPassComponent resetPassComponent = appDelegate
                .getIDIManager()
                .plusResetPassComponent();
        resetPassComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_reset_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        mNewPasswordView.showLabel();
        mNewPasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_passwordreset_new_title));
        mNewPasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_passwordreset_new_placeholder));
        mNewPasswordView.setVisibleDivider(true);

        mRetypePasswordView.showLabel();
        mRetypePasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_passwordreset_retype_title));
        mRetypePasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_passwordreset_retype_placeholder));
        mRetypePasswordView.setVisibleDivider(true);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_passwordreset_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIResetPasswordPresenter.bindView(this);
        mIResetPasswordPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIResetPasswordPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIResetPasswordPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIResetPasswordPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull ResetPasswordModelView model) {
        mNewPasswordView.setText(model.getNewPassword());
        mRetypePasswordView.setText(model.getReypePassword());
    }

    @Override
    public void onResetPasswordSuccess(int step) {
        FragmentActivity activity = requireActivity();
        switch (step) {
            case 0 : {
                resendEmail();
                break;
            }
            case 2 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 3 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.PHISICAL_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 4 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ACTIVATION_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 5 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.WELLCOME);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 6 : {
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
                break;
            }
        }
    }

    @Override
    public void resendEmail() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(ValidateEmailFragment.class, true);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mIResetPasswordPresenter.resetPassword(
                mToken,
                mNewPasswordView.getValue(),
                mRetypePasswordView.getValue());
    }
}