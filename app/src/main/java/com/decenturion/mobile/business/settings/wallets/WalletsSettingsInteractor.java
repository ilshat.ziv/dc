package com.decenturion.mobile.business.settings.wallets;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.WalletModel;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.wallets.WalletResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletsSettingsModelView;

import rx.Observable;

public class WalletsSettingsInteractor implements IWalletsSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public WalletsSettingsInteractor(IResidentClient IResidentClient,
                                     IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<Boolean> saveWallets(@NonNull String btcAddress, @NonNull String ethAddress,
                                           @NonNull String twofacode) {
        WalletModel model = new WalletModel();
        model.setAddress(btcAddress);
        model.setCoin("btc");
        model.setTwoFACode(twofacode);

        Observable<Boolean> obs1 = Observable.just(model)
                .flatMap(m -> mIResidentClient.saveWallet(m))
                .map(d -> true);

        model = new WalletModel();
        model.setAddress(ethAddress);
        model.setCoin("eth");
        model.setTwoFACode(twofacode);

        Observable<Boolean> obs2 = Observable.just(model)
                .flatMap(m -> mIResidentClient.saveWallet(m))
                .map(d -> true);

        if (!TextUtils.isEmpty(ethAddress) && !TextUtils.isEmpty(btcAddress)) {
            return Observable.zip(obs1, obs2, (aBoolean, aBoolean2) -> aBoolean2);
        } else if (!TextUtils.isEmpty(ethAddress)) {
            return obs2;
        } else {
            return obs1;
        }
    }

    @Override
    public Observable<WalletsSettingsModelView> getWalletsInfo() {
        return mIResidentClient.getWallets()
                .map(WalletResponse::getResult)
                .map(WalletsSettingsModelView::new);
    }

}
