package com.decenturion.mobile.di.dagger.settings.main;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.main.ISettingsInteractor;
import com.decenturion.mobile.business.settings.main.SettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.main.presenter.ISettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.main.presenter.SettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {

    @Provides
    @SettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SettingsScope
    ISettingsInteractor provideISettingsInteractor(IResidentClient iResidentClient,
                                                   IResidentRepository iResidentRepository,
                                                   IPrefsManager iPrefsManager) {
        return new SettingsInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @SettingsScope
    ISettingsPresenter provideISettingsPresenter(ISettingsInteractor iSettingsInteractor) {
        return new SettingsPresenter(iSettingsInteractor);
    }
}
