package com.decenturion.mobile.business.referral.invite;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResult;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteListModelView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;

import rx.Observable;

public interface IInviteListInteractor {

    Observable<InviteListModelView> getInvitations();

    Observable<RemoveInviteResult> removeInvite(int inviteId);
}
