package com.decenturion.mobile.ui.dialog.fullscreen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.decenturion.mobile.app.snack.ISnackManager;
import com.decenturion.mobile.app.snack.SnackManager;
import com.decenturion.mobile.app.snack.TypeMessage;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.architecture.view.IScreenDialogFragmentView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FullDialog extends FullScreenDialogFragment implements IScreenDialogFragmentView {

    protected Unbinder mUnbinder;

    ISnackManager mISnackManager;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mISnackManager = new SnackManager(view);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    @Override
    public IScreenActivityView getIScreenActivityView() {
        FragmentActivity activity = getActivity();
        return (IScreenActivityView) activity;
    }

    @Override
    public void showMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Normal, message);
    }

    @Override
    public void showSuccessMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Success, message);
    }

    @Override
    public void showErrorMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Error, message);
    }

    @Override
    public void showWarningMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Warning, message);
    }

    @Override
    public void showProgressView() {

    }

    @Override
    public void hideProgressView() {

    }
}
