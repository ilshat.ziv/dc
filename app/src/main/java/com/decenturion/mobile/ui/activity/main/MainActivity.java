package com.decenturion.mobile.ui.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.ui.activity.DrawerActivity;
import com.decenturion.mobile.ui.fragment.profile_v2.main.view.ProfileFragment;
import com.decenturion.mobile.ui.fragment.seller.main.view.SellerProfileFragment;
import com.decenturion.mobile.ui.widget.BaseTabLayout;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends DrawerActivity implements IMainActivity {

    private Bundle mData;
    private String mUUID;

    /* UI */

    @BindView(R.id.tabLayoutView)
    BaseTabLayout mTabView;

    @BindView(R.id.collapsedContentView)
    FrameLayout mCollapsedContentView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IPrefsManager mIPrefsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs(getIntent());
        injectToDagger();
        setupUi();
        initNavigation(savedInstanceState);
    }

    private void initArgs(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }
        mData = intent.getExtras();
        if (mData != null) {
            mUUID = mData.getString(SellerProfileFragment.SELLER_UUID);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initArgs(intent);
        initNavigation(null);
    }

    private void initNavigation(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            getINavigationManager().restoreInstanceState(savedInstanceState);
        } else {
            IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
            String residentUUID = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_UUID, "");
            if (!TextUtils.isEmpty(mUUID)) {
                getINavigationManager().navigateTo(SellerProfileFragment.class, false, mData);
            } else if (!TextUtils.isEmpty(residentUUID)) {
                getINavigationManager().navigateTo(ProfileFragment.class, false);
            } else {
                LoggerUtils.exception(new RuntimeException("Wrong resident state"));
                finish();
            }
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = (AppDelegate) getApplication();
        DIManager idiManager = (DIManager) appDelegate.getIDIManager();
        AppComponent appComponent = idiManager.getAppComponent();
        appComponent.inject(this);
    }

    private void setupUi() {
        Menu menu = mNavigationView.getMenu();

        MenuItem item = menu.getItem(0);
        item.setTitle(mILocalistionManager.getLocaleString(R.string.app_menu_account));
        item = menu.getItem(1);
        item.setTitle(mILocalistionManager.getLocaleString(R.string.app_menu_ministry));
        item = menu.getItem(2);
        item.setTitle(mILocalistionManager.getLocaleString(R.string.app_menu_referral));
        item = menu.getItem(3);
        item.setTitle(mILocalistionManager.getLocaleString(R.string.app_menu_support));
        item = menu.getItem(4);
        item.setTitle(mILocalistionManager.getLocaleString(R.string.app_menu_settings));

        mTabView.setVisibility(View.GONE);
    }

    @Override
    public TabLayout getTabLayoutView() {
        return mTabView;
    }
}
