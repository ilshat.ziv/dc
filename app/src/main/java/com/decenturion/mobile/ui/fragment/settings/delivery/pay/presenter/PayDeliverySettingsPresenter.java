package com.decenturion.mobile.ui.fragment.settings.delivery.pay.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.business.settings.delivery.pay.IPayDeliverySettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PayDeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.view.IPayDeliverySettingsView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PayDeliverySettingsPresenter extends Presenter<IPayDeliverySettingsView> implements IPayDeliverySettingsPresenter {

    private Context mContext;
    private IPayDeliverySettingsInteractor mIPayDeliverySettingsInteractor;

    private PayDeliverySettingsModelView mPayDeliverySettingsModelView;
    private CoinModelView mCoinModelView;

    public PayDeliverySettingsPresenter(Context context,
                                        IPayDeliverySettingsInteractor iPayDeliverySettingsInteractor) {

        mContext = context;
        mIPayDeliverySettingsInteractor = iPayDeliverySettingsInteractor;

        mPayDeliverySettingsModelView = new PayDeliverySettingsModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mPayDeliverySettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<PayDeliverySettingsModelView> obs = mIPayDeliverySettingsInteractor.getDeliveryData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PayDeliverySettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(PayDeliverySettingsModelView model) {
        mPayDeliverySettingsModelView = model;

        ArrayList<Coin> list = initCoinData();
        mCoinModelView = new CoinModelView(list);

        if (mView != null) {
            mView.onBindViewData(mCoinModelView, model);
        }
        hideProgressView();
    }

    private ArrayList<Coin> initCoinData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/payment.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void getPaymentAddress(@NonNull String coin) {
        if (TextUtils.isEmpty(coin)) {
            return;
        }

        showProgressView();

        Observable<PayDeliverySettingsModelView> obs = mIPayDeliverySettingsInteractor.genPaymentAddress(coin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PayDeliverySettingsModelView>) this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void genPaymentAddressSuccess(PayDeliverySettingsModelView address) {
        if (mView != null) {
            mView.onBindPaymentViewData(address);
        }
        hideProgressView();
    }

    private void genPaymentAddressFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
