package com.decenturion.mobile.repository.resident;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.database.dao.OrmPassportDAO;
import com.decenturion.mobile.database.dao.OrmPhotoDAO;
import com.decenturion.mobile.database.dao.OrmResidentDAO;
import com.decenturion.mobile.database.dao.OrmSettingsDAO;
import com.decenturion.mobile.database.dao.OrmWalletDAO;
import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.decenturion.mobile.database.model.OrmWalletModel;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.Settings;
import com.decenturion.mobile.network.response.model.Wallet;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;
import java.util.List;

public class ResidentRepository implements IResidentRepository {

    private IPrefsManager mIPrefsManager;
    private OrmHelper mOrmHelper;

    public ResidentRepository(@NonNull IPrefsManager iPrefsManager, @NonNull OrmHelper ormHelper) {
        mIPrefsManager = iPrefsManager;
        mOrmHelper = ormHelper;
    }

    @Override
    public void saveLocale(@NonNull String locale) {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.LOCALE, locale);
    }

    @Override
    public void saveResidentInfo(@NonNull OrmResidentModel resident) throws Exception {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_UUID, resident.getUuid());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, resident.getEmail());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_INVITED, resident.isInvited());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_PAID, resident.isDeliveryPaid());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, resident.isG2fa());

        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmResidentDAO dao = new OrmResidentDAO(connectionSource, OrmResidentModel.class);
        dao.clear();
        dao.createOrUpdate(resident);
    }

    @Override
    public void saveResidentInfo(@NonNull Resident resident) throws Exception {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_UUID, resident.getUuid());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, resident.getEmail());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_INVITED, resident.isInvited());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_PAID, resident.isDeliveryPaid());
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, resident.isG2fa());

        OrmResidentModel model = new OrmResidentModel(resident);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmResidentDAO dao = new OrmResidentDAO(connectionSource, OrmResidentModel.class);
        dao.clear();
        dao.createOrUpdate(model);
    }

    @Override
    public void savePassportInfo(@NonNull Passport passport) throws Exception {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        Delivery delivery = passport.getDelivery();
        if (delivery != null) {
            boolean b = !TextUtils.isEmpty(delivery.getCity()) &&
                    !TextUtils.isEmpty(delivery.getAddress()) &&
                    !TextUtils.isEmpty(delivery.getCountry()) &&
                    !TextUtils.isEmpty(delivery.getPhone()) &&
                    !TextUtils.isEmpty(delivery.getState()) &&
                    !TextUtils.isEmpty(delivery.getZip());

            iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_FILL_DATA, b);
        }

        OrmPassportModel model = new OrmPassportModel(passport);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmPassportDAO dao = new OrmPassportDAO(connectionSource, OrmPassportModel.class);
        dao.clear();
        dao.createOrUpdate(model);
    }

    @Override
    public void savePhotoInfo(@NonNull Photo photo) throws Exception {
        OrmPhotoModel model = new OrmPhotoModel(photo);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmPhotoDAO dao = new OrmPhotoDAO(connectionSource, OrmPhotoModel.class);
        dao.clear();
        dao.createOrUpdate(model);
    }

    @Override
    public void saveSettings(@NonNull Settings settings) throws Exception {
        OrmSettingsModel model = new OrmSettingsModel(settings);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmSettingsDAO dao = new OrmSettingsDAO(connectionSource, OrmSettingsModel.class);
        dao.clear();
        dao.createOrUpdate(model);
    }

    @Override
    public void saveWallets(@NonNull List<Wallet> walletList) throws Exception {
        List<OrmWalletModel> list = new ArrayList<>();
        for (Wallet wallet : walletList) {
            OrmWalletModel model = new OrmWalletModel(wallet);
            list.add(model);
        }

        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmWalletDAO dao = new OrmWalletDAO(connectionSource, OrmWalletModel.class);
        dao.bulkInsert(list);
    }

    @Override
    public OrmResidentModel getResidentInfo() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmResidentDAO dao = new OrmResidentDAO(connectionSource, OrmResidentModel.class);
            QueryBuilder<OrmResidentModel, Integer> builder = dao.queryBuilder();
            return builder.queryForFirst();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OrmPassportModel getPassportInfo() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmPassportDAO dao = new OrmPassportDAO(connectionSource, OrmPassportModel.class);
            QueryBuilder<OrmPassportModel, Integer> builder = dao.queryBuilder();
            return builder.queryForFirst();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OrmPhotoModel getPhotoInfo() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmPhotoDAO dao = new OrmPhotoDAO(connectionSource, OrmPhotoModel.class);
            QueryBuilder<OrmPhotoModel, Integer> builder = dao.queryBuilder();
            return builder.queryForFirst();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OrmSettingsModel getSettingsInfo() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmSettingsDAO dao = new OrmSettingsDAO(connectionSource, OrmSettingsModel.class);
            QueryBuilder<OrmSettingsModel, Integer> builder = dao.queryBuilder();
            return builder.queryForFirst();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void clearData() throws Exception {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.LOCALE, ClientPrefOptions.Defaults.LOCALE);
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_UUID, "");
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, "");
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_INVITED, false);
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_PAID, false);
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);

        ITokensPrefsManager iTokensPrefsManager = mIPrefsManager.getITokensPrefsManager();
        iTokensPrefsManager.clear();

        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmResidentDAO dao = new OrmResidentDAO(connectionSource, OrmResidentModel.class);
        dao.clear();

        OrmWalletDAO dao1 = new OrmWalletDAO(connectionSource, OrmWalletModel.class);
        dao1.clear();

        OrmSettingsDAO dao2 = new OrmSettingsDAO(connectionSource, OrmSettingsModel.class);
        dao2.clear();

        OrmPhotoDAO dao3 = new OrmPhotoDAO(connectionSource, OrmPhotoModel.class);
        dao3.clear();

        OrmPassportDAO dao4 = new OrmPassportDAO(connectionSource, OrmPassportModel.class);
        dao4.clear();
    }

    @Override
    public void clearResidentInfo() throws Exception {
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmResidentDAO dao = new OrmResidentDAO(connectionSource, OrmResidentModel.class);
        dao.clear();
    }
}
