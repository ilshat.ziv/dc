package com.decenturion.mobile.ui.fragment.token.buy.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedCheckBox;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.tokendetails.buy.BuyTokenComponent;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.agreement.UserAggrementActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.buy.model.BuyTokenModelView;
import com.decenturion.mobile.ui.fragment.token.buy.presenter.IBuyTokenPresenter;
import com.decenturion.mobile.ui.fragment.token.transaction.view.TransactionFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.LinkCheckBoxView;
import com.decenturion.mobile.ui.widget.LinkTextView;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class BuyTokenFragment extends BaseFragment implements IBuyTokenView,
        View.OnClickListener,
        LinkTextView.OnLinkTextViewListener,
        CompoundButton.OnCheckedChangeListener {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_COIN_UUID = "TOKEN_COIN_UUID";
    public static final String TOKEN_CATEGORY = "TOKEN_CATEGORY";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";

    private String mResidentUuid;
    private String mTokenCategory;
    private String mTokenCoinUuid;
    private String mPaymentMethod;
    private int mTokenId;

    /* UI */

    @BindView(R.id.receiveAddressView)
    EditTextView mReceiveAddressView;

    @BindView(R.id.internalTranslateView)
    LocalizedCheckBox mInternalTranslateView;


    @BindView(R.id.saleView)
    TextView mSaleView;

    @BindView(R.id.sellLabelView)
    LocalizedTextView mSellLabelView;

    @BindView(R.id.priceOneView)
    TextView mPriceOneView;

    @BindView(R.id.totalView)
    TextView mTotalView;


    @BindView(R.id.gasView)
    CheckBox mGasView;

    @BindView(R.id.agreeView)
    LinkCheckBoxView mAgreeView;


    @BindView(R.id.buyButtonView)
    LinearLayout mBuyButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IBuyTokenPresenter mIBuyTokenPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID, 0);
            mPaymentMethod = arguments.getString(PAYMENT_METHOD);
            mTokenCoinUuid = arguments.getString(TOKEN_COIN_UUID);
            mTokenCategory = arguments.getString(TOKEN_CATEGORY);
            mResidentUuid = arguments.getString(RESIDENT_UUID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        BuyTokenComponent buyTokenComponent = appDelegate
                .getIDIManager()
                .plusBuyTokenComponent();
        buyTokenComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_buy_token, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mReceiveAddressView.showLabel();
//        mReceiveAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_receiveAddress_placeholder));
        mReceiveAddressView.setLabel("ERC-20 address");
        mReceiveAddressView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_receiveAddress_placeholder));
        mReceiveAddressView.editMode();
        mReceiveAddressView.setVisibleDivider(true);

        mInternalTranslateView.setChecked(true);
        mInternalTranslateView.setOnCheckedChangeListener(this);

        mAgreeView.setChecked(false);
//        mAgreeView.setText(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_agree_checkbox));
        mAgreeView.setText(mILocalistionManager.getLocaleString(R.string.app_signup_termsofuse));
        mAgreeView.setOnLinkClickListener(this);
        mAgreeView.setOnCheckedChangeListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIBuyTokenPresenter.bindView(this);

        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {

            mIBuyTokenPresenter.bindViewData(
                    mResidentUuid,
                    mTokenId,
                    mPaymentMethod,
                    mTokenCategory,
                    mTokenCoinUuid
            );
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIBuyTokenPresenter.unbindView();
        try {
            mIBuyTokenPresenter.onSaveInstanceState(outState,
                    mReceiveAddressView.getValue(),
                    mInternalTranslateView.isChecked(),
                    mAgreeView.isChecked(),
                    mGasView.isChecked()
            );
        } catch (Exception e) {
            // TODO: 04.10.2018 проследить в крашлитикс
            LoggerUtils.exception(e);
        }

        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIBuyTokenPresenter.bindView(this);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIBuyTokenPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIBuyTokenPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull BuyTokenModelView model) {
        String localeString = mILocalistionManager.getLocaleString(R.string.app_account_buytokens_price);
        localeString = localeString.replace("{symbol}", model.getCoin());
        mSellLabelView.setText(localeString);

        mInternalTranslateView.setEnabled(!TextUtils.equals(model.getCategory(), "internal"));
        mSaleView.setText(model.getOnSale());
        mPriceOneView.setText(model.getPriceFor1());
        mTotalView.setText(model.getTotal());

        // TODO: 24.09.2018 костыль
        if (!mAgreeView.isChecked()) {
            mAgreeView.setChecked(model.isAgreeTermsOfuse());
        }
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.buyButtonView)
    protected void onBuy() {
        boolean on = (mInternalTranslateView.isChecked() || !TextUtils.isEmpty(mReceiveAddressView.getValue()))
                && mAgreeView.isChecked();
        if (on) {
            mIBuyTokenPresenter.createTraide(mReceiveAddressView.getValue(), mInternalTranslateView.isChecked());
        }
    }

    @Override
    public void onTraideCreated(@NonNull CreateTraideResult result) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        Bundle bundle = new Bundle();
        bundle.putString(TransactionFragment.TRAIDE_UUID, result.getUuid());
        iNavigationManager.navigateTo(TransactionFragment.class, bundle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_USER_AGGREEMENT) {
            mAgreeView.setChecked(resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    public void onLinkClick() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, UserAggrementActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_USER_AGGREEMENT);
    }

    @Override
    public void onTextClick() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        boolean on = (mInternalTranslateView.isChecked() || !TextUtils.isEmpty(mReceiveAddressView.getValue()))
//                && mAgreeView.isChecked();
//        mBuyButtonView.setEnabled(on);

        if (buttonView.getId() == R.id.internalTranslateView) {
            if (isChecked) {
                mReceiveAddressView.setVisibility(View.GONE);
            } else {
                mReceiveAddressView.setVisibility(View.VISIBLE);
            }
        }
    }
}
