package com.decenturion.mobile.ui.fragment.pass.confirm.phone.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.restore.RestorePassResponse;
import com.decenturion.mobile.network.response.restore.RestorePassResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.model.ConfirmPhoneRestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.view.IConfirmPhoneRestorePassView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ConfirmPhoneRestorePassPresenter extends Presenter<IConfirmPhoneRestorePassView> implements IConfirmPhoneRestorePassPresenter {

    private IRestorePassInteractor mIRestorePassInteractor;
    private ConfirmPhoneRestorePassModelView mRestorePassModelView;
    private IPrefsManager mIPrefsManager;

    public ConfirmPhoneRestorePassPresenter(IRestorePassInteractor iSigninInteractor, IPrefsManager iPrefsManager) {
        mIRestorePassInteractor = iSigninInteractor;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mRestorePassModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<ConfirmPhoneRestorePassModelView> obs = Observable.just(mRestorePassModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ConfirmPhoneRestorePassModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ConfirmPhoneRestorePassModelView model) {
        mRestorePassModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void sendSmsCode(@NonNull String data) {
        mRestorePassModelView = new ConfirmPhoneRestorePassModelView(data);

        showProgressView();

        Observable<RestorePassResponse> obs = mIRestorePassInteractor.restorePass(data, "")
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<RestorePassResult>) this::restorePassSuccess,
                this::restorePassFailure
        );
    }

    private void restorePassSuccess(RestorePassResult result) {
        hideProgressView();
//        Resident resident = result.getResident();

//        int step = resident.getStep();
//        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
//        iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

        if (mView != null) {
            mView.onRestorePassSuccess();
        }
    }

    private void restorePassFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());
                return;
            }
        }
        showErrorView(throwable);
    }
}
