package com.decenturion.mobile.ui.fragment.seller.passport.view;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.di.dagger.seller.passport.SellerPassportComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.component.banner.BannerViewComponent;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.seller.passport.model.BannerModelView;
import com.decenturion.mobile.ui.fragment.seller.passport.presenter.ISellerPassportPresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.RefreshView;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SellerPassportFragment extends BaseFragment implements ISellerPassportView,
        BannerViewComponent.OnBannerViewListener,
        SwipeRefreshLayout.OnRefreshListener {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    private String mResidentUuid;

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    // Balance

    @BindView(R.id.balanceValueView)
    TextView mBalanceValueView;

    @BindView(R.id.balanceCoinView)
    TextView mBalanceCoinView;

    @BindView(R.id.moneyView)
    TextView mMoneyView;

    @BindView(R.id.powerView)
    TextView mPowerView;

    @BindView(R.id.gloryView)
    TextView mGloryView;

    // Bio

    @BindView(R.id.birthdayView)
    SimpleTextView mBirthdayView;

    @BindView(R.id.sexView)
    SpinnerView mSexView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.cityView)
    SimpleTextView mCityView;

    @BindView(R.id.localWorldwideView)
    SimpleTextView mLocalWorldwideView;

    @BindView(R.id.topWorldwideView)
    SimpleTextView mTopWorldwideView;

    @Nullable
    @BindView(R.id.bannerView)
    BannerViewComponent mBannerViewComponent;

    /* DI */

    @Inject
    ISellerPassportPresenter mISellerPassportPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IPrefsManager mIPrefsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mResidentUuid = arguments.getString(RESIDENT_UUID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SellerPassportComponent sellerPassportComponent = appDelegate
                .getIDIManager()
                .plusSellerPassportComponent();
        sellerPassportComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_passport_seller, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        mRefreshView.setOnRefreshListener(this);

        mBirthdayView.showLabel();
        mBirthdayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_dob));
        mBirthdayView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_dob));
        mBirthdayView.simpleMode();
        mBirthdayView.setVisibleDivider(true);

        mSexView.showLabel();
        mSexView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_sex));
        mSexView.simpleMode();
        mSexView.setVisibleDivider(true);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_country));
        mCountryView.simpleMode();
        mCountryView.setVisibleDivider(true);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_city));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_city));
        mCityView.simpleMode();
        mCityView.setVisibleDivider(true);

        mLocalWorldwideView.showLabel();
        mLocalWorldwideView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_toplocal));
        mLocalWorldwideView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_toplocal));
        mLocalWorldwideView.simpleMode();
        mLocalWorldwideView.setVisibleDivider(true);

        mTopWorldwideView.showLabel();
        mTopWorldwideView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_topworldwide));
        mTopWorldwideView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_topworldwide));
        mTopWorldwideView.simpleMode();
        mTopWorldwideView.setVisibleDivider(true);

        mBannerViewComponent.setOnBannerViewListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISellerPassportPresenter.bindView(this);
        mISellerPassportPresenter.bindViewData(mResidentUuid);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISellerPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISellerPassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull SexModelView model,
                               @NonNull CountryModelView model1,
                               @NonNull PassportModelView model2) {

        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();

        String hidden = mILocalistionManager.getLocaleString(R.string.app_privacy_hidden);

        String firstname = model2.getFirstname();
        firstname = TextUtils.isEmpty(firstname) ? hidden : firstname ;

        String lastname = model2.getLastname();
        lastname = TextUtils.isEmpty(lastname) ? hidden : lastname ;

        iToolbarController.setTitle(firstname + " " + lastname);

        // Balance
        mBalanceCoinView.setText(model2.getCoin());
        String balance = model2.getBalance();
        if (TextUtils.isEmpty(balance)) {
            mBalanceValueView.setText(hidden);
        } else {
            mBalanceValueView.setText(balance);
        }

        String money1 = model2.getMoney();
        if (TextUtils.isEmpty(money1)) {
            mMoneyView.setText(hidden);
        } else {
            String money = "X " + money1;
            mMoneyView.setText(money);
        }
        String power1 = model2.getPower();
        if (TextUtils.isEmpty(power1)) {
            mPowerView.setText(hidden);
        } else {
            String power = "X " + power1;
            mPowerView.setText(power);
        }

        String glory1 = model2.getGlory();
        if (TextUtils.isEmpty(glory1)) {
            mGloryView.setText(hidden);
        } else {
            String glory = "X " + glory1;
            mGloryView.setText(glory);
        }

        mSexView.setData(hidden, model.getSexData());
        mCountryView.setData(hidden, model1.getCountryData());

        String birth = model2.getBirth();
        if (TextUtils.isEmpty(birth)) {
            mBirthdayView.setText(hidden);
        } else {
            mBirthdayView.setText(birth);
        }

        String sex = model2.getSex();
        if (!TextUtils.isEmpty(sex)) {
            mSexView.selectItem(sex);
        }
        String country = model2.getCountry();
        if (!TextUtils.isEmpty(country)) {
            mCountryView.selectItem(model2.getCountry());
        }
        String city = model2.getCity();
        city = TextUtils.isEmpty(city) ? hidden : city ;
        mCityView.setText(city);

//        mLocalWorldwideView.setText("1440");
//        mTopWorldwideView.setText("5496");

        initBanner(model2.getBannerModelView());
    }

    private void initBanner(BannerModelView model) {
        if (model == null || mBannerViewComponent == null) {
            return;
        }

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        int param = iClientPrefsManager.getParam(ClientPrefOptions.Keys.SIGN_STEP, -1);
        if (param < 4) {
            mBannerViewComponent.setVisibility(View.VISIBLE);
            mBannerViewComponent.setTitle(model.getTitle());
            String string = mILocalistionManager.getLocaleString(R.string.app_referral_banner_price);
            mBannerViewComponent.setReferralPrice(string.replace("{price}", model.getReferralPrice()));
            string = mILocalistionManager.getLocaleString(R.string.app_referral_banner_subprice);
            mBannerViewComponent.setBasePrice(string.replace("{price}", model.getBasePrice()));
        }
    }

    @OnClick(R.id.telegramContactView)
    protected void onTelegramContact() {
        PassportModelView model = mISellerPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String telegramContact = model.getTelegramContact();
        if (!TextUtils.isEmpty(telegramContact)) {
            if (isAppInstalled("org.telegram.messenger")) {
                String url = "https://t.me/" + telegramContact.replace("@", "");
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            } else {
                launchMarket("org.telegram.messenger");
            }
        }
    }

    @OnClick(R.id.whatsappContactView)
    protected void onWhatsappContact() {
        PassportModelView model = mISellerPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String whatsappContact = model.getWhatsappContact();
        if (!TextUtils.isEmpty(whatsappContact)) {
            if (isAppInstalled("com.whatsapp")) {
                String url = "https://wa.me/" + whatsappContact;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            } else {
                launchMarket("com.whatsapp");
            }
        }
    }

    @OnClick(R.id.wechatContactView)
    protected void onWechatContact() {
        PassportModelView model = mISellerPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String wechatContact = model.getWechatContact();
        if (!TextUtils.isEmpty(wechatContact)) {
            if (isAppInstalled("com.tencent.mm")) {
                String url = "weixin://dl/chat?" + wechatContact;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    startActivity(browserIntent);
                } catch (ActivityNotFoundException e) {
                    LoggerUtils.exception(e);
                }
            } else {
                launchMarket("com.tencent.mm");
            }
        }
    }

    @OnClick(R.id.viberContactView)
    protected void onViberContact() {
        PassportModelView model = mISellerPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String viberContact = model.getViberContact();
        if (!TextUtils.isEmpty(viberContact)) {
            if (isAppInstalled("com.viber.voip")) {
                Uri uri = Uri.parse("tel:" + Uri.encode(viberContact));
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
                intent.setData(uri);
                startActivity(intent);
            } else {
                launchMarket("com.viber.voip");
            }
        }
    }

    private boolean isAppInstalled(String uri) {
        Context context = getContext();
        assert context != null;
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            LoggerUtils.exception(e);
        }

        return false;
    }

    private void launchMarket(String packageName) {
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
        } catch (ActivityNotFoundException e) {
            LoggerUtils.exception(e);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
        }
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISellerPassportPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onGetCitizen() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, SignActivity.class);
        PassportModelView profileView = mISellerPassportPresenter.getProfileView();
        BannerModelView bannerModelView = profileView.getBannerModelView();
        intent.putExtra(SignActivity.BANNER_REFERALL_KEY, bannerModelView.getReferralId());
        activity.startActivity(intent);
        // TODO: 07.11.2018 Проверить кейс, когда пользователь уже был в экране ввхода и потом перешел по ссылке
        activity.finishAffinity();
    }

    @Override
    public void onGetCitizenInfo() {
        // TODO: 07.11.2018 показывать диалог описания
        // https://trello.com/c/h4JKXla2/64-%D0%BB%D0%B5%D0%BD%D0%B4%D0%B8%D0%BD%D0%B3-%D0%B3%D1%80%D0%B0%D0%B6%D0%B4%D0%B0%D0%BD%D0%B8%D0%BD%D0%B0-%D1%81-%D0%B1%D0%B0%D0%BD%D0%B5%D1%80%D0%BE%D0%BC
    }

    @Override
    public void onGetCitizenInfo2() {
        // TODO: 07.11.2018 показывать диалог описания
        // https://trello.com/c/h4JKXla2/64-%D0%BB%D0%B5%D0%BD%D0%B4%D0%B8%D0%BD%D0%B3-%D0%B3%D1%80%D0%B0%D0%B6%D0%B4%D0%B0%D0%BD%D0%B8%D0%BD%D0%B0-%D1%81-%D0%B1%D0%B0%D0%BD%D0%B5%D1%80%D0%BE%D0%BC
    }

    @Override
    public void onRefresh() {
        mISellerPassportPresenter.bindViewData(mResidentUuid);
        mRefreshView.hide();
    }
}
