package com.decenturion.mobile.ui.fragment.profile_v2.score.item;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoreTokenListAdapter extends RecyclerView.Adapter<ScoreTokenListAdapter.ViewHolder> {

    private Context mContext;
    private final LayoutInflater mInflater;
    private List<ScoreTokenModelView> mTokenModelList;

    private OnItemClickListener mOnItemClickListener;

    public ScoreTokenListAdapter(@NonNull Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public ScoreTokenListAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<ScoreTokenModelView> tokenModelList) {
        mTokenModelList = tokenModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTokenModelList);
    }

    @NonNull
    @Override
    public ScoreTokenListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_token_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreTokenListAdapter.ViewHolder holder, int position) {
        final ScoreTokenModelView startup = mTokenModelList.get(position);
        holder.bind(startup);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Nullable
        @BindView(R.id.labelView)
        LocalizedTextView mLabelView;

        @Nullable
        @BindView(R.id.iconView)
        ImageView iconView;

        @Nullable
        @BindView(R.id.nameWalletView)
        TextView valueView;

        @Nullable
        @BindView(R.id.walletValueView)
        TextView walletValueView;

        @Nullable
        @BindView(R.id.walletExchangeView)
        TextView walletExchangeView;

        public View view;
        public ScoreTokenModelView startup;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void bind(@NonNull ScoreTokenModelView tokenModel) {
            this.startup = tokenModel;

            String iconUrl = tokenModel.getIconUrl();
            if (TextUtils.isEmpty(iconUrl)) {
                assert this.iconView != null;
                this.iconView.setImageResource(tokenModel.getIconRes());
            } else {
                Picasso.with(mContext)
                        .load(iconUrl)
                        .resize(192, 192)
                        .onlyScaleDown()
                        .into(this.iconView);
            }

            assert this.valueView != null;
            this.valueView.setText(tokenModel.getName());
            assert this.walletValueView != null;
            this.walletValueView.setText(tokenModel.getBalance());
            if (this.walletExchangeView != null) {
                String wallet = tokenModel.getWallet();
                this.walletExchangeView.setText(wallet != null ? String.valueOf(wallet) : "");
            }
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(this.startup);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ScoreTokenModelView model);
    }
}
