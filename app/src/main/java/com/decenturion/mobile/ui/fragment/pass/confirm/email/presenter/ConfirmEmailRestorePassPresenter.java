package com.decenturion.mobile.ui.fragment.pass.confirm.email.presenter;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.model.ConfirmEmailRestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.view.IConfirmEmailRestorePassView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ConfirmEmailRestorePassPresenter extends Presenter<IConfirmEmailRestorePassView> implements IConfirmEmailRestorePassPresenter {

    private IRestorePassInteractor mIRestorePassInteractor;
    private ConfirmEmailRestorePassModelView mRestorePassModelView;
    private IPrefsManager mIPrefsManager;

    public ConfirmEmailRestorePassPresenter(IRestorePassInteractor iSigninInteractor, IPrefsManager iPrefsManager) {
        mIRestorePassInteractor = iSigninInteractor;
        mIPrefsManager = iPrefsManager;
        mRestorePassModelView = new ConfirmEmailRestorePassModelView("");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mRestorePassModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<ConfirmEmailRestorePassModelView> obs = Observable.just(mRestorePassModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ConfirmEmailRestorePassModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ConfirmEmailRestorePassModelView model) {
        mRestorePassModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
