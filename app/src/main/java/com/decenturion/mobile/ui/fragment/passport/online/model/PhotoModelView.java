package com.decenturion.mobile.ui.fragment.passport.online.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.model.Photo;

public class PhotoModelView extends BaseResponse {

    private String mOriginal;
    private String mMin;

    public PhotoModelView() {
    }

    public PhotoModelView(@NonNull OrmPhotoModel model) {
        mOriginal = model.getOriginal();
        mMin = model.getMin();
    }

    public PhotoModelView(@NonNull Photo model) {
        mOriginal = model.getOriginal();
        mMin = model.getMin();
    }

    public String getOriginal() {
        return mOriginal;
    }

    public String getMin() {
        return mMin;
    }
}
