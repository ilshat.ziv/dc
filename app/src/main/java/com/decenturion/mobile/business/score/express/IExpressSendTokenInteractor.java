package com.decenturion.mobile.business.score.express;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.fragment.score.send.express.model.ExpressSendTokenModelView;

import rx.Observable;

public interface IExpressSendTokenInteractor {

    Observable<ExpressSendTokenModelView> getCoinInfo(@NonNull ExpressSendTokenModelView model);

    Observable<SendTokenResult> sendToken(@NonNull String coinUUID, @NonNull String twofa);
}
