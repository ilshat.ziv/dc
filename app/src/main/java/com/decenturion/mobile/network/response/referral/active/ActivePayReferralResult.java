package com.decenturion.mobile.network.response.referral.active;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivePayReferralResult {

    @JsonProperty("email")
    private String mEmail;

    public ActivePayReferralResult() {
    }

    @JsonIgnore
    public String getEmail() {
        return mEmail;
    }
}
