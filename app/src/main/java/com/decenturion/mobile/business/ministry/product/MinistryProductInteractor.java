package com.decenturion.mobile.business.ministry.product;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.business.ministry.product.model.PaymenDataModel;
import com.decenturion.mobile.network.client.ministry.IMinistryClient;
import com.decenturion.mobile.network.request.PaymentAddressModel;
import com.decenturion.mobile.network.request.PaymentStatusAddressModel;
import com.decenturion.mobile.network.response.ministry.MinistryPaymentAddressResponse;
import com.decenturion.mobile.network.response.ministry.MinistryResponse;
import com.decenturion.mobile.network.response.ministry.MinistryResult;
import com.decenturion.mobile.network.response.ministry.MinistryStatusPaymentAddressResponse;
import com.decenturion.mobile.network.response.ministry.MinistryStatusPaymentResult;
import com.decenturion.mobile.network.response.model.MinistryItem;
import com.decenturion.mobile.network.response.model.MinistryItemPrice;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.ministry.product.model.MinistryProductModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.PaymentModelView;

import rx.Observable;

public class MinistryProductInteractor implements IMinistryProductInteractor {

    private IMinistryClient mIMinistryClient;
    private IResidentRepository mIResidentRepository;

    public MinistryProductInteractor(IMinistryClient iMinistryClient,
                                     IResidentRepository iResidentRepository) {

        mIMinistryClient = iMinistryClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<MinistryProductModelView> getMinistryModelView(@NonNull String category) {
        Observable<MinistryProductModelView> obs1 = Observable.just(new MinistryProductModelView(category));
        Observable<MinistryItemPrice> obs2 =  mIMinistryClient.getPriceProduct()
                .map(MinistryResponse::getResult)
                .map(d -> getMinistryItemPrice(category, d));

        return Observable.zip(obs1, obs2, (ministryProductModelView, ministryItemPrice) -> {
            ministryProductModelView.setUsdtPrice(ministryItemPrice.getPrice());
            return ministryProductModelView;
        });
    }

    private MinistryItemPrice getMinistryItemPrice(@NonNull String category, @NonNull MinistryResult d) {
        switch (category) {
            case "external": {
                MinistryItem external = d.getExternal();
                return external.getUsdt();
            }
            case "honorary": {
                MinistryItem honorary = d.getHonorary();
                return honorary.getUsdt();
            }
            case "senator": {
                MinistryItem senator = d.getSenator();
                return senator.getUsdt();
            }
            default: {
                MinistryItem internal = d.getInternal();
                return internal.getUsdt();
            }
        }
    }

    private MinistryItemPrice getMinistryItemPrice(@NonNull String coin, @NonNull MinistryItem item) {
        switch (coin) {
            case "bch": {
                return item.getBch();
            }
            case "eth": {
                return item.getEth();
            }
            case "usdt": {
                return item.getUsdt();
            }
            default: {
                return item.getBtc();
            }
        }
    }

    @Override
    public Observable<MinistryProductModelView> genPaymentAddress(@NonNull PaymenDataModel model) {
        return genPaymentAddress(model.getCategory(), model.getCoin(), model.getAmount());
    }

    @Override
    public Observable<MinistryProductModelView> genPaymentAddress(@NonNull String category, @NonNull String coin) {
        Observable<MinistryStatusPaymentResult> obs1 = Observable.just(new PaymentStatusAddressModel(category, coin))
                .flatMap(d -> mIMinistryClient.statusPaymentAddress(d))
                .map(MinistryStatusPaymentAddressResponse::getResult);

        Observable<MinistryProductModelView> obs2 = Observable.just(new MinistryProductModelView(category));

        return Observable.zip(obs1, obs2, (s, model) -> {
            PaymentModelView paymentModelView = new PaymentModelView();
            paymentModelView.setTypePay(coin);
            paymentModelView.setPriceOne(s.getAmount());
            paymentModelView.setCoinsPay(s.getAmount());
            paymentModelView.setAddress(s.getAddress());

            model.setPaymentModelView(paymentModelView);
            return model;
        });
    }

    @Override
    public Observable<MinistryProductModelView> genPaymentAddress(@NonNull String category,
                                                                  @NonNull String coin,
                                                                  @NonNull String amount) {
        Observable<MinistryItemPrice> obs3 = mIMinistryClient.getPriceProduct(category, amount, coin)
                .map(MinistryResponse::getResult)
                .map(d -> {
                    switch (category) {
                        case "external": {
                            MinistryItem external = d.getExternal();
                            return getMinistryItemPrice(coin, external);
                        }
                        case "honorary": {
                            MinistryItem honorary = d.getHonorary();
                            return getMinistryItemPrice(coin, honorary);
                        }
                        case "senator": {
                            MinistryItem senator = d.getSenator();
                            return getMinistryItemPrice(coin, senator);
                        }
                        default: {
                            MinistryItem internal = d.getInternal();
                            return getMinistryItemPrice(coin, internal);
                        }
                    }
                });


        Observable<String> obs1;
        if (TextUtils.equals(category, "honorary") || TextUtils.equals(category, "senator")) {
            obs1 = Observable.just(new PaymentStatusAddressModel(category, coin))
                    .flatMap(d -> mIMinistryClient.statusPaymentAddress(d))
                    .map(MinistryStatusPaymentAddressResponse::getResult)
                    .map(MinistryStatusPaymentResult::getAddress);
        } else {
            obs1 = Observable.just(new PaymentAddressModel(category, coin))
                    .flatMap(d -> mIMinistryClient.paymentAddress(d))
                    .map(MinistryPaymentAddressResponse::getResult);
        }

        Observable<MinistryProductModelView> obs2 = Observable.just(new MinistryProductModelView(category));

        return Observable.zip(obs1, obs3, obs2, (s, model1, model) -> {
            PaymentModelView paymentModelView = new PaymentModelView();
            paymentModelView.setTypePay(coin);
            paymentModelView.setPriceOne(model1.getPrice());
            paymentModelView.setCoinsPay(model1.getTotalPrice());
            paymentModelView.setAddress(s);

            model.setPaymentModelView(paymentModelView);
            return model;
        });
    }
}
