package com.decenturion.mobile.ui.toolbar;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class DToolbar extends LinearLayout {

    public DToolbar(Context context) {
        super(context);
    }

    public DToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
