package com.decenturion.mobile.ui.fragment.ministry.market;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryProduct;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MinistryAdapter extends RecyclerView.Adapter<MinistryAdapter.ViewHolder> {

    private final Context mContext;

    private final LayoutInflater mInflater;
    private List<MinistryProduct> mMinistryProductList;

    private OnItemClickListener mOnItemClickListener;

    private ILocalistionManager mILocalistionManager;

    private MinistryAdapter(@NonNull Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public MinistryAdapter(@NonNull Context context,
                           OnItemClickListener listener,
                           ILocalistionManager iLocalistionManager) {
        this(context);
        mOnItemClickListener = listener;
        mILocalistionManager = iLocalistionManager;
    }

    public void replaceDataSet(List<MinistryProduct> tokenModelList) {
        mMinistryProductList = tokenModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mMinistryProductList);
    }

    @NonNull
    @Override
    public MinistryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_ministry_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MinistryProduct model = mMinistryProductList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productTypeView)
        TextView productTypeView;

        @BindView(R.id.productNameView)
        TextView productNameView;

        @BindView(R.id.priceView)
        TextView priceView;

        @BindView(R.id.optionsView)
        RecyclerView optionsView;
        MinistryOptionAdapter ministryOptionAdapter;


        @BindView(R.id.actionButtonLabelView)
        TextView actionButtonLabelView;

        @BindView(R.id.actionButtonView)
        ConstraintLayout actionButtonView;

        @BindView(R.id.actionInfoButtonView)
        ConstraintLayout actionInfoButtonView;

        @BindView(R.id.descriptionView)
        TextView descriptionView;

        public final View view;
        public MinistryProduct model;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

            this.actionButtonView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(ViewHolder.this.model);
                }
            });

            this.actionInfoButtonView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onInfoProduct(ViewHolder.this.model);
                }
            });

            ministryOptionAdapter = new MinistryOptionAdapter(mContext);
            optionsView.setLayoutManager(new LinearLayoutManager(mContext));
            optionsView.setAdapter(ministryOptionAdapter);
        }

        public void bind(@NonNull MinistryProduct model) {
            this.model = model;

            this.productTypeView.setText(model.getType());
            this.productNameView.setText(model.getName());
            Spanned text = Html.fromHtml(model.getPrice());
            this.priceView.setText(text);

            String prefix = mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_buy);
            if (TextUtils.equals(model.getCategory(), "honorary")
                    || TextUtils.equals(model.getCategory(), "senator")) {
                prefix = mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_buy);
            }

//            this.actionButtonLabelView.setText(prefix + " " +model.getName());
            this.actionButtonLabelView.setText(prefix);

            this.descriptionView.setText(model.getDescription());
//            this.ministryOptionAdapter.replaceDataSet(model.getOptionList());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(MinistryProduct model);

        void onInfoProduct(MinistryProduct model);
    }
}
