package com.decenturion.mobile.ui.dialog.ministry;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class DcntClassicFeaturesDialog extends SingleDialogFragment {

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        DIManager idiManager = (DIManager) appDelegate.getIDIManager();
        idiManager.getAppComponent().inject(this);
    }


    public static void show(@NonNull FragmentManager fragmentManager) {
        DcntClassicFeaturesDialog dialog = new DcntClassicFeaturesDialog();
        dialog.show(fragmentManager, DcntClassicFeaturesDialog.class.getSimpleName());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_features_dialog_title));
        setNegativeTextString(mILocalistionManager.getLocaleString(R.string.app_constants_close));

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_ministry_internal_features);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());
        setVisibilityPositiveClickButton(false);

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        return arbitrationRequestDialog;
    }
}
