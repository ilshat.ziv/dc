package com.decenturion.mobile.business.settings.details;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.details.ChangeDetailsResponse;
import com.decenturion.mobile.ui.fragment.settings.details.model.DetailsSettingsModelView;

import rx.Observable;

public interface IDetailsSettingsInteractor {

    Observable<DetailsSettingsModelView> getPassportInfo();

    Observable<ChangeDetailsResponse> saveChanges(@NonNull String bio,
                                                  @NonNull String telegram,
                                                  @NonNull String whatsApp,
                                                  @NonNull String viber,
                                                  @NonNull String wechat);

    Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap);
}
