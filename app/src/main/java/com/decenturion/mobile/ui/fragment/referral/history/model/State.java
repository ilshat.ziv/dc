package com.decenturion.mobile.ui.fragment.referral.history.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class State implements ISpinnerItem {

    private String mName;
    private String mKey;

    public State(String name) {
        mName = name;
    }

    public State(String name, String key) {
        mName = name;
        mKey = key;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String getItemName() {
        return mName;
    }

    @Override
    public String getItemKey() {
        return mKey;
    }
}
