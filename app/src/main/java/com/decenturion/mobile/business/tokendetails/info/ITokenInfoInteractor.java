package com.decenturion.mobile.business.tokendetails.info;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.token.info.v1.model.TokenInfoModelView;

import rx.Observable;

public interface ITokenInfoInteractor {

    Observable<TokenInfoModelView> getTokenInfo(int tokenId);

    Observable<com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView> getTokenCategoryInfo(@NonNull String tokenCategory);
}
