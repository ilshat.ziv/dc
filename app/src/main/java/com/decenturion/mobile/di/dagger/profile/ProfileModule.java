package com.decenturion.mobile.di.dagger.profile;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.profile.IProfileInteractor;
import com.decenturion.mobile.business.profile.ProfileInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.main.presenter.IProfilePresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.main.presenter.ProfilePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileModule {

    @Provides
    @ProfileScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ProfileScope
    IProfileInteractor provideIProfileInteractor(IResidentClient iResidentClient,
                                                  IResidentRepository iResidentRepository,
                                                 ILocalistionManager iLocalistionManager) {
        return new ProfileInteractor(iResidentClient, iResidentRepository, iLocalistionManager);
    }

    @Provides
    @ProfileScope
    IProfilePresenter provideIProfilePresenter(IProfileInteractor iProfileInteractor,
                                               Context context,
                                               ILocalistionManager iLocalistionManager) {
        return new ProfilePresenter(iProfileInteractor, context, iLocalistionManager);
    }
}
