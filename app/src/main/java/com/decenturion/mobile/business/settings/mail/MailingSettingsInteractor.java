package com.decenturion.mobile.business.settings.mail;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.SubscribeModel;
import com.decenturion.mobile.network.response.subscribe.SubscribeResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.mail.model.MailingSettingsModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class MailingSettingsInteractor implements IMailingSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public MailingSettingsInteractor(IResidentClient IResidentClient,
                                     IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<MailingSettingsModelView> getResidentInfo() {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(MailingSettingsModelView::new);
    }

    @Override
    public Observable<SubscribeResponse> saveChanges(boolean isSubscribe) {
        return Observable.just(new SubscribeModel(isSubscribe))
                .flatMap(m -> mIResidentClient.subscribe(m))
                .doOnNext(d -> {
                    OrmResidentModel resident = mIResidentRepository.getResidentInfo();
                    resident.setSubscribed(isSubscribe);
                    try {
                        mIResidentRepository.saveResidentInfo(resident);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

}
