package com.decenturion.mobile.ui.fragment.pass.confirm.email.model;

public class ConfirmEmailRestorePassModelView {

    private String mEmail;

    public ConfirmEmailRestorePassModelView(String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }
}
