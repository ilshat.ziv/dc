package com.decenturion.mobile.ui.fragment.passport.wellcome.view;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.passport.wellcome.model.WellcomePassportModelView;

public interface IWellcomePassportView extends IScreenFragmentView {

    void onBindViewData(WellcomePassportModelView model);

    void onSkipedWellcome();
}
