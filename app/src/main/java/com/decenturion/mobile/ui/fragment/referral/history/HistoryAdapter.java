package com.decenturion.mobile.ui.fragment.referral.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModel;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<HistoryModel> mTraideModelList;

    private OnItemClickListener mOnItemClickListener;

    private HistoryAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public HistoryAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<HistoryModel> traideModelList) {
        mTraideModelList = traideModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTraideModelList);
    }

    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_referral_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.ViewHolder holder, int position) {
        final HistoryModel model = mTraideModelList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        @BindView(R.id.labelView)
        TextView labelView;

        @BindView(R.id.valueView)
        TextView valueView;

        @BindView(R.id.amountView)
        TextView amountView;

        HistoryModel model;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            this.view.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(model);
                }
            });
        }

        public void bind(@NonNull HistoryModel param) {
            this.model = param;

            this.labelView.setText(model.getDate());
            this.valueView.setText(model.getAction());
            this.amountView.setText(model.getAmount());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(HistoryModel model);
    }
}
