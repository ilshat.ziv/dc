package com.decenturion.mobile.ui.fragment.ministry.market.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.ministry.market.MinistryComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.ministry.MinistryActivity;
import com.decenturion.mobile.ui.activity.ministry.honorary.HonoraryInfoActivity;
import com.decenturion.mobile.ui.activity.ministry.senator.SenatorInfoActivity;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.ministry.DcntClassicFeaturesDialog;
import com.decenturion.mobile.ui.dialog.ministry.DcntLiquidFeaturesDialog;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.ministry.info.MinistryHonoraryInfoFragment;
import com.decenturion.mobile.ui.fragment.ministry.info.MinistrySenatorInfoFragment;
import com.decenturion.mobile.ui.fragment.ministry.market.MinistryAdapter;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryModelView;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryProduct;
import com.decenturion.mobile.ui.fragment.ministry.market.presenter.IMinistryPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class MinistryFragment extends BaseFragment implements IMinistryView,
        MinistryAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.ministryListView)
    RecyclerView mMinistryListView;
    private MinistryAdapter mMinistryAdapter;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IMinistryPresenter mIMinistryPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);

    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        MinistryComponent component = appDelegate
                .getIDIManager()
                .plusMinistryComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_ministry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();

    }

    private void setupUi() {
        initToolbar();

        Context context = getContext();
        assert context != null;
        mMinistryListView.setLayoutManager(new LinearLayoutManager(context));
        mMinistryAdapter = new MinistryAdapter(context, this, mILocalistionManager);
        mMinistryListView.setAdapter(mMinistryAdapter);

        mRefreshView.setOnRefreshListener(this);
    }

    private void initToolbar() {
        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_ministry_title));
        iToolbarController.setSubTitle("");

        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.GONE);

        FragmentManager supportFragmentManager = getFragmentManager();
        assert supportFragmentManager != null;
        Fragment f = supportFragmentManager.findFragmentByTag("ProfileCollapsAppBarFragment");
        if (f != null) {
            FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIMinistryPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIMinistryPresenter.bindViewData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mIMinistryPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIMinistryPresenter.unbindView();
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIMinistryPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onBindViewData(@NonNull MinistryModelView model) {
        mMinistryAdapter.replaceDataSet(model.getMinistryProductList());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIMinistryPresenter.unbindView();
    }

    @Override
    public void onItemClick(MinistryProduct model) {
        String category = model.getCategory();
        Intent intent = new Intent(requireActivity(), MinistryActivity.class);
        intent.putExtra(MinistryActivity.CATEGORY, category);
        startActivity(intent);
    }

    @Override
    public void onInfoProduct(MinistryProduct model) {
        switch (model.getCategory()) {
            case "internal" : {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                IDialogManager iDialogManager = activity.getIDialogManager();
                iDialogManager.showDialog(DcntClassicFeaturesDialog.class);
                break;
            }
            case "external" : {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                IDialogManager iDialogManager = activity.getIDialogManager();
                iDialogManager.showDialog(DcntLiquidFeaturesDialog.class);
                break;
            }
            case "honorary" : {
                Intent intent = new Intent(requireActivity(), HonoraryInfoActivity.class);
                startActivity(intent);
                break;
            }
            case "senator" : {
                Intent intent = new Intent(requireActivity(), SenatorInfoActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void onRefresh() {
        mRefreshView.hide();
        mIMinistryPresenter.bindViewData();
    }
}
