package com.decenturion.mobile.business.token.edit;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.token.edit.model.EditPriceModelView;

import rx.Observable;

public interface IEditPriceTokenInteractor {

    Observable<EditPriceModelView> getTokenDetailsModule(int tokenId);

    Observable<EditPriceModelView> saveChanges(
            int TokenId,
            @NonNull String coinId,
            @NonNull String sell,
            @NonNull String category);
}
