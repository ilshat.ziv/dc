package com.decenturion.mobile.di.dagger.seller.passport;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface SellerPassportScope {
}
