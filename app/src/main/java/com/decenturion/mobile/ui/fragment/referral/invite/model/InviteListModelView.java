package com.decenturion.mobile.ui.fragment.referral.invite.model;

import com.decenturion.mobile.network.response.model.Invite;
import com.decenturion.mobile.network.response.referral.invite.ReferralInviteResult;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class InviteListModelView {

    private int mUsed;

    private List<InviteModel> mHistoryModelList;
    private int mTotal;
    private long mOffset;
    public final int LIMIT = 15;

    public InviteListModelView() {
    }

    public InviteListModelView(List<InviteModel> list, int total, int used) {
        mHistoryModelList = list;
        mTotal = total;
        mUsed = used;
    }

    public InviteListModelView(ReferralInviteResult d) {
        List<Invite> list = d.getList();
        if (CollectionUtils.isEmpty(list)) {
            mHistoryModelList = null;
        }
        mTotal = d.getTotal();
        mUsed = d.getUsed();
    }

    public int getCountUnuseInvites() {
        return mTotal - mUsed;
    }

    public int getUsed() {
        return mUsed;
    }

    public List<InviteModel> getHistoryList() {
        return mHistoryModelList;
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }

}
