package com.decenturion.mobile.ui.fragment.passport.activation.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.business.DecenturionConstants;
import com.decenturion.mobile.business.passport.activation.IActivationPassportInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.model.ActivationModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.view.IActivationPassportView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ActivationPassportPresenter extends Presenter<IActivationPassportView> implements IActivationPassportPresenter{

    private Context mContext;
    private IActivationPassportInteractor mIActivationPassportInteractor;
    private ActivationModelView mActivationPassportModelView;
    private CountryModelView mCountryModelView;
    private CoinModelView mCoinModelView;

    public ActivationPassportPresenter(Context context, IActivationPassportInteractor iActivationPassportInteractor) {
        mContext = context;
        mIActivationPassportInteractor = iActivationPassportInteractor;

        mActivationPassportModelView = new ActivationModelView();
        mActivationPassportModelView.setTypePay("btc");
        mActivationPassportModelView.setCoinsPay("0.1000");
        mActivationPassportModelView.setCyrrencePay(DecenturionConstants.RESIDENCE_PAY + " $");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mActivationPassportModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<ActivationModelView> obs = Observable.just(mActivationPassportModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ActivationModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ActivationModelView model) {
        mActivationPassportModelView = model;

        ArrayList<Coin> list = initCoinData();
        mCoinModelView = new CoinModelView(list);
        if (mView != null) {
            mView.onBindViewData(mCoinModelView, model);
        }
        hideProgressView();
    }

    private ArrayList<Coin> initCoinData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/coin.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void getPaymentAddress(@NonNull String coin) {
        if (TextUtils.isEmpty(coin)) {
            return;
        }

        showProgressView();

        Observable<ActivationModelView> obs = mIActivationPassportInteractor.genPaymentAddress(coin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ActivationModelView>) this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void genPaymentAddressSuccess(ActivationModelView address) {
        if (mView != null) {
            mView.onBindPaymentViewData(address);
        }
        hideProgressView();
    }

    private void genPaymentAddressFailure(Throwable throwable) {
        hideProgressView();
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
