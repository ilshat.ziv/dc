package com.decenturion.mobile.network.response.wallets;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveWalletResponse extends BaseResponse {

    @JsonProperty("result")
    private SaveWalletResult mResult;

    public SaveWalletResponse() {
    }

    @JsonIgnore
    public SaveWalletResult getResult() {
        return mResult;
    }
}
