package com.decenturion.mobile.business.traid;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.trades.TradesResponse;
import com.decenturion.mobile.network.response.trades.TradesResult;
import com.decenturion.mobile.network.response.traide.state.StateTraideResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.ui.fragment.traide.model.TraidInfoModelView;

import rx.Observable;

public class TraidInfoInteractor implements ITraidInfoInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IResidentRepository mIResidentRepository;
    private IWalletRepository mIWalletRepository;
    private ITokenRepository mITokenRepository;
    private ITraideRepository mITraideRepository;

    public TraidInfoInteractor(IResidentClient IResidentClient,
                               ITokenClient iTokenClient,
                               IResidentRepository iResidentRepository,
                               IWalletRepository iWalletRepository,
                               ITokenRepository iTokenRepository,
                               ITraideRepository iTraideRepository) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIResidentRepository = iResidentRepository;
        mIWalletRepository = iWalletRepository;
        mITokenRepository = iTokenRepository;
        mITraideRepository = iTraideRepository;
    }

    @Override
    public Observable<TraidInfoModelView> getTraidInfo(@NonNull String traidUUID) {
//        return Observable.just(value);
        Observable<TraidInfoModelView> obs1 = Observable.just(mITraideRepository.getTraide(traidUUID))
                .map(d -> {
                    if (d != null) {
                        return new TraidInfoModelView(d);
                    } else {
                        return null;
                    }
                });

        Observable<TraidInfoModelView> obs2 = getTraidInfoFromServer2(traidUUID);

        return Observable.concat(obs1, obs2)
                .takeFirst(this::isTraidValid);

//        return mITokenClient.getDealsByTradeUUID(traidUUID)
//                .map(TradesResponse::getTransactionList)
//                .map(TradesResult::getWrapperTradeList)
//                .map(d -> d.get(0))
//                .map(TransactionInfoModelView::new);
    }

    @NonNull
    private Observable<TraidInfoModelView> getTraidInfoFromServer(@NonNull String traidUUID) {
        return mITokenClient.getTraide(traidUUID)
                    .map(StateTraideResponse::getResult)
                    .map(TraidInfoModelView::new);
    }

    @NonNull
    private Observable<TraidInfoModelView> getTraidInfoFromServer2(@NonNull String traidUUID) {
        return mITokenClient.getDeals("",
                0,
                999999
        )
                .map(TradesResponse::getResult)
                .map(TradesResult::getWrapperTradeList)
                .flatMapIterable(list -> list)
                .filter(d -> {
                    Trade trade = d.getTrade();
                    return TextUtils.equals(trade.getUuid(), traidUUID);
                })
                .map(TraidInfoModelView::new)
                .toList()
                .map(d -> d.get(0));
    }

    private boolean isTraidValid(TraidInfoModelView d) {
        return d != null;// && !TextUtils.isEmpty(d.getTransactionStatus());
    }
}
