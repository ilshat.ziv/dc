package com.decenturion.mobile.business.twofa;

import com.decenturion.mobile.ui.fragment.twofa.model.EnableTwoFAModelView;

import rx.Observable;

public interface IEnableTwoFAInteractor {

    Observable<Boolean> saveChanges(String secretString, String secretCode);

    Observable<EnableTwoFAModelView> getInfo();
}
