package com.decenturion.mobile.business.ministry.product.model;

public class PaymenDataModel {

    private String mCoin;
    private String mAmount;
    private String mCategory;

    public PaymenDataModel(String coin, String amount, String category) {
        mCoin = coin;
        mAmount = amount;
        mCategory = category;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getCategory() {
        return mCategory;
    }
}
