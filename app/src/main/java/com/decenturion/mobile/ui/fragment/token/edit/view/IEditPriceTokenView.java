package com.decenturion.mobile.ui.fragment.token.edit.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.edit.model.EditPriceModelView;

public interface IEditPriceTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull EditPriceModelView model);

    void onChangesSaved();
}
