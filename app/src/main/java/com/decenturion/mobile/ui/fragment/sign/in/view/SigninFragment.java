package com.decenturion.mobile.ui.fragment.sign.in.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.signin.SigninComponent;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.restore.RestorePassActivity;
import com.decenturion.mobile.ui.activity.sign.ISignActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.PasswordView;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.access.AccessDenyDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.sign.gfa.view.GfaSigninFragment;
import com.decenturion.mobile.ui.fragment.sign.in.model.SigninModelView;
import com.decenturion.mobile.ui.fragment.sign.in.presenter.ISigninPresenter;
import com.decenturion.mobile.ui.fragment.sign.up.view.SignupFragment;
import com.decenturion.mobile.ui.fragment.validate.view.ValidateEmailFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SigninFragment extends BaseFragment implements ISigninView,
        ChangeComponentListener {

    /* UI */

    @BindView(R.id.emailView)
    EditTextView mEmailView;

    @BindView(R.id.passwordView)
    PasswordView mPasswordView;

    @BindView(R.id.controlView)
    FloatView mControlView;
    private IFloatViewManager mIFloatViewManager;

    @BindView(R.id.signinButtonView)
    LocalizedButtonView mSigninButtonView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISigninPresenter mISigninPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SigninComponent signinComponent = appDelegate
                .getIDIManager()
                .plusSigninComponent();
        signinComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_signin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();

    }

    private void setupUi() {
        initToolbar();

        mEmailView.showLabel();
        mEmailView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_email_title));
        mEmailView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_email_placeholder));
        mEmailView.editMode();
        mEmailView.setTypeInput(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mEmailView.setVisibleDivider(true);
        mEmailView.setChangeDataModelListener(this);

        mPasswordView.showLabel();
        mPasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_password_title));
        mPasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_password_placeholder));
        mPasswordView.editMode();
        mPasswordView.setVisibleDivider(true);
        mPasswordView.setChangeDataModelListener(this);

        mIFloatViewManager.bindIFloatView(mControlView);
    }

    private void initToolbar() {
        ISignActivity activity = (ISignActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISigninPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISigninPresenter.bindViewData();
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISigninPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISigninPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISigninPresenter.unbindView();
        if (mEmailView == null) {
            mISigninPresenter.onSaveInstanceState(outState);
        } else {
            mISigninPresenter.onSaveInstanceState(outState,
                    mEmailView.getValue(),
                    mPasswordView.getValue()
            );
        }
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_up);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_register));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_signin_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_up : {
                ISignActivity activity = (ISignActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(SignupFragment.class);

                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBindViewData(@NonNull SigninModelView model) {
        mEmailView.setText(model.getEmail());
        mPasswordView.setText(model.getPassword());
    }

    @OnClick(R.id.signinButtonView)
    protected void onSignIn() {
        String email = mEmailView.getValue();
        String password = mPasswordView.getValue();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            showErrorMessage(mILocalistionManager.getLocaleString(R.string.app_alert_fillallthefields));
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            String localeString = mILocalistionManager.getLocaleString(R.string.app_alert_failed_email);
            showErrorMessage(localeString.replace("{email}", email));
            return;
        }

        mISigninPresenter.signin(email, password, null);
    }

    @OnClick(R.id.restorePassView)
    protected void onRestorePass() {
        Intent intent = new Intent(getContext(), RestorePassActivity.class);
        intent.putExtra(RestorePassActivity.CONTACT, mEmailView.getValue());
        startActivity(intent);
    }

    @Override
    public void onSigninSuccess(int step) {
        FragmentActivity activity = requireActivity();
        switch (step) {
            case 0 : {
                onResendEmail();
                break;
            }
            case 2 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 3 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.PHISICAL_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 4 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ACTIVATION_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 5 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.WELLCOME);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 6 : {
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
                break;
            }
        }
    }

    @Override
    public void onResendEmail() {
        ISignActivity activity = (ISignActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(ValidateEmailFragment.class, true);
    }

    @Override
    public void onResendEmail(@NonNull String email) {
        ISignActivity activity = (ISignActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        Bundle bundle = new Bundle();
        bundle.putString(ValidateEmailFragment.EMAIL, email);
        iNavigationManager.navigateTo(ValidateEmailFragment.class, true, bundle);
    }

    @Override
    public void onSignByG2fa() {
        ISignActivity activity = (ISignActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(GfaSigninFragment.class, true);
    }

    @Override
    public void onAccessRestricted() {
        ISignActivity activity = (ISignActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(AccessDenyDialog.class);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISigninPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlView);
    }

    @Override
    public void onChangeComponentData(View view) {
//        try {
//            String email = mEmailView.getValue();
//            boolean b = !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() &&
//                    !TextUtils.isEmpty(mPasswordView.getValue());
//
//            mSigninButtonView.setEnabled(b);
//        } catch (Exception e) {
//            // TODO: 25.09.2018 проследить в крашлитикс
//            LoggerUtils.exception(e);
//        }
    }
}
