package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EditPriceToken {

    @JsonProperty("updated_at")
    private String mDateUpdate;

    @JsonProperty("coin_id")
    private String mCoinId;

    @JsonProperty("ask_price")
    private String mSell;

    @JsonProperty("bid_price")
    private String mBuy;

    @JsonProperty("balance")
    private String mBalance;

    @JsonProperty("category")
    private String mCategory;

    public EditPriceToken() {
    }

    @JsonIgnore
    public String getCoinId() {
        return mCoinId;
    }

    @JsonIgnore
    public void setSell(String sell) {
        mSell = sell;
    }

    @JsonIgnore
    public void setBuy(String buy) {
        mBuy = buy;
    }

    @JsonIgnore
    public String getSell() {
        return mSell;
    }

    @JsonIgnore
    public String getBuy() {
        return mBuy;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getDateUpdate() {
        return mDateUpdate;
    }

    @JsonIgnore
    public String getBalance() {
        return mBalance;
    }
}
