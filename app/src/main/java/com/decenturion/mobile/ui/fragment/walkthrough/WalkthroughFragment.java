package com.decenturion.mobile.ui.fragment.walkthrough;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.di.dagger.walkthrough.WalkthroughComponent;
import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class WalkthroughFragment extends BaseFragment {

    /* UI */

    @BindView(R.id.tutorialPagerView)
    ViewPager mViewPager;

    @BindView(R.id.viewPagerDotsView)
    LinearLayout mViewPageIndicatorView;

    /* DI */

    @Inject
    IPrefsManager mIPrefsManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        WalkthroughComponent walkthroughComponent = appDelegate
                .getIDIManager()
                .plusWalkthroughComponent();
        walkthroughComponent.inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_walkthrough, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        int[] layoutResource = new int[]{
                R.layout.walkthrough_item_1,
                R.layout.walkthrough_item_2,
                R.layout.walkthrough_item_3,
                R.layout.walkthrough_item_4,
                R.layout.walkthrough_item_5,
        };

        Context context = getContext();
        WalkthroughAdapter pageAdapter = new WalkthroughAdapter(context, layoutResource);
        mViewPager.setAdapter(pageAdapter);
        mViewPager.setCurrentItem(0);
        new ViewPageIndecator(context, mViewPager, mViewPageIndicatorView);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_procced);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_onboarding_skip));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fr_walkthrough_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_procced: {
                IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                iClientPrefsManager.setParam(ClientPrefOptions.Keys.IS_SKIP_WALKTHROUGH, true);

                FragmentActivity activity = requireActivity();
                Intent intent = new Intent(activity, SignActivity.class);
                Bundle arguments = getArguments();
                if (arguments != null) {
                    intent.putExtras(arguments);
                }
                startActivity(intent);
                activity.finish();

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void initToolbar() {
        SingleFragmentActivity activity = (SingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle("");
        iToolbarController.removeNavigationButton();
    }
}
