package com.decenturion.mobile.ui.fragment.referral.statistic.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.statistic.view.IStatisticReferralView;

public interface IStatisticReferralPresenter extends IPresenter<IStatisticReferralView> {

    void bindViewData();
}
