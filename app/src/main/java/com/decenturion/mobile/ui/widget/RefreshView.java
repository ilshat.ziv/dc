package com.decenturion.mobile.ui.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import com.decenturion.mobile.R;

public class RefreshView extends SwipeRefreshLayout {

    public RefreshView(Context context) {
        super(context);
        setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);
    }

    public RefreshView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary);
    }

    public void hide() {
        post(() -> setRefreshing(false));
    }

    public void show() {
        post(() -> setRefreshing(true));
    }
}
