package com.decenturion.mobile.app.prefs.token;

public class TokenPrefsOptions {

    public interface Keys {
        String CSRF_TOKEN = "CSRF_TOKEN";
        String COOKIES = "COOKIES";
    }

    public interface Defaults {
    }
}
