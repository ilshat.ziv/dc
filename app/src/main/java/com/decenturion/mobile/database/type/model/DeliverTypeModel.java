package com.decenturion.mobile.database.type.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.Delivery;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliverTypeModel {

    @JsonProperty("deliver")
    private Delivery mDelivery;

    public DeliverTypeModel() {
    }

    @JsonIgnore
    public DeliverTypeModel(@NonNull Delivery delivery) {
        mDelivery = delivery;
    }

    @JsonIgnore
    public Delivery getDelivery() {
        return mDelivery;
    }
}
