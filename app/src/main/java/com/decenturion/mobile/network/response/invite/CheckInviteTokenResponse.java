package com.decenturion.mobile.network.response.invite;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInviteTokenResponse extends BaseResponse {

    @JsonProperty("result")
    private CheckInviteTokenResult mResult;

    public CheckInviteTokenResponse() {
    }

    @JsonIgnore
    public CheckInviteTokenResult getResult() {
        return mResult;
    }
}
