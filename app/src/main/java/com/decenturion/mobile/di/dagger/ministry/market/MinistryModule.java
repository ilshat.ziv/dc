package com.decenturion.mobile.di.dagger.ministry.market;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.ministry.market.IMinistryInteractor;
import com.decenturion.mobile.business.ministry.market.MinistryInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.ministry.IMinistryClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.ministry.market.presenter.IMinistryPresenter;
import com.decenturion.mobile.ui.fragment.ministry.market.presenter.MinistryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MinistryModule {

    @Provides
    @MinistryScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @MinistryScope
    IMinistryInteractor provideIMinistryInteractor(
            IResidentClient iResidentClient,
            IMinistryClient iMinistryClient,
            IResidentRepository iResidentRepository,
            ILocalistionManager iLocalistionManager) {

        return new MinistryInteractor(iResidentClient,
                iMinistryClient,
                iResidentRepository,
                iLocalistionManager);
    }

    @Provides
    @MinistryScope
    IMinistryPresenter provideIMinistryPresenter(IMinistryInteractor iMinistryInteractor) {
        return new MinistryPresenter(iMinistryInteractor);
    }
}
