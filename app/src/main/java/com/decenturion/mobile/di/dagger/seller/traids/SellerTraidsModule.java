package com.decenturion.mobile.di.dagger.seller.traids;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.seller.traids.ISellerTraidsInteractor;
import com.decenturion.mobile.business.seller.traids.SellerTraidsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.seller.trade.presenter.ISellerTraidsPresenter;
import com.decenturion.mobile.ui.fragment.seller.trade.presenter.SellerTraidsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SellerTraidsModule {

    @Provides
    @SellerTraidsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SellerTraidsScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }


    @Provides
    @SellerTraidsScope
    ISellerTraidsInteractor provideISellerTraidsInteractor(IResidentClient IResidentClient,
                                                     ITokenClient iTokenClient,
                                                     IResidentRepository iResidentRepository,
                                                     ITokenRepository iTokenRepository) {

        return new SellerTraidsInteractor(IResidentClient,
                iTokenClient,
                iResidentRepository,
                iTokenRepository);
    }

    @Provides
    @SellerTraidsScope
    ISellerTraidsPresenter provideISellerTraidsPresenter(ISellerTraidsInteractor iSellerTraidsInteractor) {
        return new SellerTraidsPresenter(iSellerTraidsInteractor);
    }
}
