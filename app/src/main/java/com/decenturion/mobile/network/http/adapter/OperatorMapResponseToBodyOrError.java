package com.decenturion.mobile.network.http.adapter;

import android.text.TextUtils;

import com.decenturion.mobile.network.Utils;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.CsrfAccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.http.exception.NotNetConnectionException;
import com.decenturion.mobile.network.http.exception.RestrictedException;
import com.decenturion.mobile.network.response.BadResponse;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.utils.JsonUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

import rx.Observable;
import rx.Observable.Operator;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * A version of {@link Observable#map(Func1)} which lets us trigger {@code onError} without having
 * to use {@link Observable#flatMap(Func1)} which breaks producer requests from propagating.
 */
final class OperatorMapResponseToBodyOrError<T> implements Operator<T, Response<T>> {

    private static final OperatorMapResponseToBodyOrError<Object> INSTANCE =
            new OperatorMapResponseToBodyOrError<>();


    @SuppressWarnings("unchecked") // Safe because of erasure.
    public static <R> OperatorMapResponseToBodyOrError<R> instance() {
        return (OperatorMapResponseToBodyOrError<R>) INSTANCE;
    }


    @Override
    public Subscriber<? super Response<T>> call(final Subscriber<? super T> child) {
        return new Subscriber<Response<T>>(child) {
            @Override
            public void onNext(Response<T> response) {
                T body = response.body();

                if (response.isSuccessful()) {
                    child.onNext(body);
                } else {
                    switch (response.code()) {
                        case 401 : {
                            String message = response.message();
                            AccessException e = new AccessException(TextUtils.isEmpty(message) ? "Not authorized" : message);
                            child.onError(e);
                            break;
                        }
                        case 403 :
                        case 400 : {
                            BadResponse response1 = null;
                            try {
                                String raw = response.errorBody().string();
                                response1 = (BadResponse) JsonUtils.getObjectFromString(raw, BadResponse.class);
                            } catch (IOException e) {
                                LoggerUtils.exception(e);
                            }

                            if (response1 != null) {
                                InputDataException e = new InputDataException(response1);

                                ArrayList<Error> errorList = e.getErrorList();
                                Error error = errorList.get(0);
                                switch (error.getKey()) {
                                    case "csrf" : {
                                        child.onError(new CsrfAccessException(error.getMessage()));
                                        break;
                                    }
                                    case "session" : {
                                        child.onError(new AccessException(error.getMessage()));
                                        break;
                                    }
                                    default: {
                                        child.onError(e);
                                    }
                                }
                            } else {
                                String message = response.message();
                                if (TextUtils.equals(message, "Forbidden")
                                        || TextUtils.equals(message, "Access Denied")) {
                                    String httpParams = response.raw().toString();
                                    String ipAddress = Utils.getIpAddress();
                                    LoggerUtils.exception(new RuntimeException("HTTP 403 Forbidden: " + ipAddress + " " + httpParams));

                                    RestrictedException e = new RestrictedException("Access is denied, please contact support.");
                                    child.onError(e);
                                } else {
                                    AccessException e = new AccessException(TextUtils.isEmpty(message) ? "Not authorized" : message);
                                    child.onError(e);
                                }
                            }
                            break;
                        }
                        default : {
                            HttpException e = new HttpException(response);
                            child.onError(e);
                        }
                    }
                }
            }

            @Override
            public void onCompleted() {
                child.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof SocketTimeoutException
                        || e instanceof SSLHandshakeException
                        || e instanceof JsonMappingException
                        || e instanceof InterruptedIOException) {
                    LoggerUtils.exception(e);
                    child.onError(new RuntimeException("Oops, an error occurred"));
                    return;
                }

                String message = e.getMessage();
                if (message != null && (
                        message.contains("No address associated with hostname")
                        || message.contains("Failed to connect to")
                        || message.contains("timeout")
                )) {
                    NotNetConnectionException e1 = new NotNetConnectionException();
                    child.onError(e1);
                } else {
                    child.onError(e);
                }
            }
        };
    }
}

