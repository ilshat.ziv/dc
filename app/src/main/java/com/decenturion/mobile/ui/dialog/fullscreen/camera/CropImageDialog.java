package com.decenturion.mobile.ui.dialog.fullscreen.camera;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.dialog.fullscreen.FullDialog;
import com.decenturion.mobile.utils.LoggerUtils;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.model.AspectRatio;
import com.yalantis.ucrop.util.SelectedStateListDrawable;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView.TransformImageListener;
import com.yalantis.ucrop.view.UCropView;
import com.yalantis.ucrop.view.widget.AspectRatioTextView;
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView;
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView.ScrollingListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class CropImageDialog extends FullDialog {

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.text_view_scale)
    protected TextView mTextViewScalePercent;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.scale_scroll_wheel)
    protected HorizontalProgressWheelView mScaleScrollWheel;

    @BindView(com.yalantis.ucrop.R.id.ucrop)
    protected UCropView mUCropView;

    @BindView(com.yalantis.ucrop.R.id.ucrop_photobox)
    protected ViewGroup mPhotoBox;

    protected GestureCropImageView mGestureCropImageView;

    protected OverlayView mOverlayView;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.state_aspect_ratio)
    protected ViewGroup mWrapperStateAspectRatio;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.state_rotate)
    protected ViewGroup mWrapperStateRotate;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.state_scale)
    protected ViewGroup mWrapperStateScale;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.layout_aspect_ratio)
    protected ViewGroup mLayoutAspectRatio;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.layout_rotate_wheel)
    protected ViewGroup mLayoutRotate;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.layout_scale_wheel)
    protected ViewGroup mLayoutScale;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.text_view_rotate)
    protected TextView mTextViewRotateAngle;

    @Nullable
    @BindView(com.yalantis.ucrop.R.id.rotate_scroll_wheel)
    protected HorizontalProgressWheelView mRotateScrollWheel;

    public static final int DEFAULT_COMPRESS_QUALITY = 90;
    public static final CompressFormat DEFAULT_COMPRESS_FORMAT;
    public static final int NONE = 0;
    public static final int SCALE = 1;
    public static final int ROTATE = 2;
    public static final int ALL = 3;
    private static final String TAG = "UCropActivity";
    private static final int TABS_COUNT = 3;
    private static final int SCALE_WIDGET_SENSITIVITY_COEFFICIENT = 15000;
    private static final int ROTATE_WIDGET_SENSITIVITY_COEFFICIENT = 42;
    private String mToolbarTitle;
    private int mToolbarColor;
    private int mStatusBarColor;
    private int mActiveWidgetColor;
    private int mToolbarWidgetColor;
    private int mLogoColor;
    private boolean mShowBottomControls;
    private boolean mShowLoader = true;

    private List<ViewGroup> mCropAspectRatioViews = new ArrayList();
    private View mBlockingView;
    private CompressFormat mCompressFormat;
    private int mCompressQuality;
    private int[] mAllowedGestures;
    private TransformImageListener mImageListener;
    private final View.OnClickListener mStateClickListener;

    private String mPathFile;
    private String mFileName;
    private int mRequestCode;

    public static void show(@NonNull FragmentManager fm, int requestCode) {
        final CropImageDialog dialog = new CropImageDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(CameraDialog.Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, CropImageDialog.class.getName());
    }

    public static void show(@NonNull FragmentManager fm, Bundle bundle, int requestCode) {
        final CropImageDialog dialog = new CropImageDialog();

        bundle.putInt(CameraDialog.Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, CropImageDialog.class.getName());
    }

    public CropImageDialog() {
        this.mCompressFormat = DEFAULT_COMPRESS_FORMAT;
        this.mCompressQuality = 90;
        this.mAllowedGestures = new int[]{1, 2, 3};
        this.mImageListener = new TransformImageListener() {
            public void onRotate(float currentAngle) {
                CropImageDialog.this.setAngleText(currentAngle);
            }

            public void onScale(float currentScale) {
                CropImageDialog.this.setScaleText(currentScale);
            }

            public void onLoadComplete() {
                CropImageDialog.this.mUCropView.animate()
                        .alpha(1.0F)
                        .setDuration(300L)
                        .setInterpolator(new AccelerateInterpolator());
                CropImageDialog.this.mBlockingView.setClickable(false);
                CropImageDialog.this.mShowLoader = false;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.supportInvalidateOptionsMenu();
                }
            }

            public void onLoadFailure(@NonNull Exception e) {
                showErrorMessage("Photo is not loading");
                LoggerUtils.exception(e);
                CropImageDialog.this.setResultError(e);
                dismiss();
            }
        };
        this.mStateClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (!v.isSelected()) {
                    CropImageDialog.this.setWidgetState(v.getId());
                }

            }
        };
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initArgs();
//        this.setInitialState();
//        this.addBlockingView();
    }

    private void initArgs() {
        final Bundle arguments = getArguments();
        if (arguments != null) {
            mRequestCode = arguments.getInt(CameraDialog.Args.REQUEST_CODE);
            mFileName = arguments.getString(CameraDialog.Args.FILE_NAME);
            mPathFile = arguments.getString(CameraDialog.Args.PATH_FILE_SAVE);
        }

        Intent intent = new Intent();//this.getIntent();
        this.setupViews(intent);
        this.setImageData(intent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_crop_image);
//        setFullVisual(false);
        setTitleText(CropImageDialog.class.getSimpleName());

        return view;//inflater.inflate(R.layout.fr_ucrop, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.initArgs();
        this.setInitialState();
        this.addBlockingView();
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onPositiveClickButton() {
        this.cropAndSaveImage();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }

        activity.getMenuInflater().inflate(com.yalantis.ucrop.R.menu.ucrop_menu_activity, menu);
        MenuItem menuItemLoader = menu.findItem(com.yalantis.ucrop.R.id.menu_loader);
        Drawable menuItemLoaderIcon = menuItemLoader.getIcon();
        if (menuItemLoaderIcon != null) {
            try {
                menuItemLoaderIcon.mutate();
                menuItemLoaderIcon.setColorFilter(this.mToolbarWidgetColor, PorterDuff.Mode.SRC_ATOP);
                menuItemLoader.setIcon(menuItemLoaderIcon);
            } catch (IllegalStateException var6) {
                Log.i("UCropActivity", String.format("%s - %s", new Object[]{var6.getMessage(), this.getString(com.yalantis.ucrop.R.string.ucrop_mutate_exception_hint)}));
            }

            ((Animatable) menuItemLoader.getIcon()).start();
        }

        MenuItem menuItemCrop = menu.findItem(com.yalantis.ucrop.R.id.menu_crop);
        Drawable menuItemCropIcon = menuItemCrop.getIcon();
        if (menuItemCropIcon != null) {
            menuItemCropIcon.mutate();
            menuItemCropIcon.setColorFilter(this.mToolbarWidgetColor, PorterDuff.Mode.SRC_ATOP);
            menuItemCrop.setIcon(menuItemCropIcon);
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(com.yalantis.ucrop.R.id.menu_crop).setVisible(!this.mShowLoader);
        menu.findItem(com.yalantis.ucrop.R.id.menu_loader).setVisible(this.mShowLoader);
        super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == com.yalantis.ucrop.R.id.menu_crop) {
            this.cropAndSaveImage();
        } else if (item.getItemId() == 16908332) {
            dismiss();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onStop() {
        super.onStop();
        if (this.mGestureCropImageView != null) {
            this.mGestureCropImageView.cancelAllAnimations();
        }

    }

    private void setImageData(@NonNull Intent intent) {
        Uri inputUri = (Uri) getArguments().getParcelable("com.yalantis.ucrop.InputUri");//intent.getParcelableExtra("com.yalantis.ucrop.InputUri");
        Uri outputUri = (Uri) getArguments().getParcelable("com.yalantis.ucrop.OutputUri");//intent.getParcelableExtra("com.yalantis.ucrop.OutputUri");
        this.processOptions(intent);

        if (inputUri != null && outputUri != null) {
            try {
                this.mGestureCropImageView.setImageUri(inputUri, outputUri);
            } catch (Exception e) {
                showErrorMessage("Ошибка при загрузке файла");
                LoggerUtils.exception(e);
                this.setResultError(e);
                dismiss();
            }
        } else {
            this.setResultError(new NullPointerException(this.getString(com.yalantis.ucrop.R.string.ucrop_error_input_data_is_absent)));
            showErrorMessage("Файл не найден");
            LoggerUtils.exception(new RuntimeException("Файл не найден"));
            dismiss();
        }

    }

    private void processOptions(@NonNull Intent intent) {
        String compressionFormatName = intent.getStringExtra("com.yalantis.ucrop.CompressionFormatName");
        CompressFormat compressFormat = null;
        if (!TextUtils.isEmpty(compressionFormatName)) {
            compressFormat = CompressFormat.valueOf(compressionFormatName);
        }

        this.mCompressFormat = compressFormat == null ? DEFAULT_COMPRESS_FORMAT : compressFormat;
        this.mCompressQuality = getArguments().getInt("com.yalantis.ucrop.CompressionQuality", 90);
        int[] allowedGestures = intent.getIntArrayExtra("com.yalantis.ucrop.AllowedGestures");
        if (allowedGestures != null && allowedGestures.length == 3) {
            this.mAllowedGestures = allowedGestures;
        }

        this.mGestureCropImageView.setMaxBitmapSize(intent.getIntExtra("com.yalantis.ucrop.MaxBitmapSize", 0));
        this.mGestureCropImageView.setMaxScaleMultiplier(intent.getFloatExtra("com.yalantis.ucrop.MaxScaleMultiplier", 10.0F));
        this.mGestureCropImageView.setImageToWrapCropBoundsAnimDuration((long) intent.getIntExtra("com.yalantis.ucrop.ImageToCropBoundsAnimDuration", 500));
        this.mOverlayView.setFreestyleCropEnabled(intent.getBooleanExtra("com.yalantis.ucrop.FreeStyleCrop", false));
        this.mOverlayView.setDimmedColor(intent.getIntExtra("com.yalantis.ucrop.DimmedLayerColor", this.getResources().getColor(R.color.light_gray)));
        this.mOverlayView.setCircleDimmedLayer(intent.getBooleanExtra("com.yalantis.ucrop.CircleDimmedLayer", false));
        this.mOverlayView.setShowCropFrame(intent.getBooleanExtra("com.yalantis.ucrop.ShowCropFrame", true));
        this.mOverlayView.setCropFrameColor(intent.getIntExtra("com.yalantis.ucrop.CropFrameColor", this.getResources().getColor(com.yalantis.ucrop.R.color.ucrop_color_default_crop_frame)));
        this.mOverlayView.setCropFrameStrokeWidth(intent.getIntExtra("com.yalantis.ucrop.CropFrameStrokeWidth", this.getResources().getDimensionPixelSize(com.yalantis.ucrop.R.dimen.ucrop_default_crop_frame_stoke_width)));
        this.mOverlayView.setShowCropGrid(intent.getBooleanExtra("com.yalantis.ucrop.ShowCropGrid", true));
        this.mOverlayView.setCropGridRowCount(intent.getIntExtra("com.yalantis.ucrop.CropGridRowCount", 2));
        this.mOverlayView.setCropGridColumnCount(intent.getIntExtra("com.yalantis.ucrop.CropGridColumnCount", 2));
        this.mOverlayView.setCropGridColor(intent.getIntExtra("com.yalantis.ucrop.CropGridColor", this.getResources().getColor(com.yalantis.ucrop.R.color.ucrop_color_default_crop_grid)));
        this.mOverlayView.setCropGridStrokeWidth(intent.getIntExtra("com.yalantis.ucrop.CropGridStrokeWidth", this.getResources().getDimensionPixelSize(com.yalantis.ucrop.R.dimen.ucrop_default_crop_grid_stoke_width)));
        float aspectRatioX = intent.getFloatExtra("com.yalantis.ucrop.AspectRatioX", 0.0F);
        float aspectRatioY = intent.getFloatExtra("com.yalantis.ucrop.AspectRatioY", 0.0F);
        int aspectRationSelectedByDefault = intent.getIntExtra("com.yalantis.ucrop.AspectRatioSelectedByDefault", 0);
        ArrayList<AspectRatio> aspectRatioList = intent.getParcelableArrayListExtra("com.yalantis.ucrop.AspectRatioOptions");
        if (aspectRatioX > 0.0F && aspectRatioY > 0.0F) {
            if (this.mWrapperStateAspectRatio != null) {
                this.mWrapperStateAspectRatio.setVisibility(View.GONE);
            }

            this.mGestureCropImageView.setTargetAspectRatio(aspectRatioX / aspectRatioY);
        } else if (aspectRatioList != null && aspectRationSelectedByDefault < aspectRatioList.size()) {
            this.mGestureCropImageView.setTargetAspectRatio(((AspectRatio) aspectRatioList.get(aspectRationSelectedByDefault)).getAspectRatioX() / ((AspectRatio) aspectRatioList.get(aspectRationSelectedByDefault)).getAspectRatioY());
        } else {
            this.mGestureCropImageView.setTargetAspectRatio(0.75F);
        }

        int maxSizeX = getArguments().getInt("com.yalantis.ucrop.MaxSizeX", 0);
        int maxSizeY = getArguments().getInt("com.yalantis.ucrop.MaxSizeY", 0);
        if (maxSizeX > 0 && maxSizeY > 0) {
            this.mGestureCropImageView.setMaxResultImageSizeX(maxSizeX);
            this.mGestureCropImageView.setMaxResultImageSizeY(maxSizeY);
        }

    }

    private void setupViews(@NonNull Intent intent) {
        this.mStatusBarColor = getArguments().getInt("com.yalantis.ucrop.StatusBarColor", ContextCompat.getColor(getContext(), R.color.black));
        this.mToolbarColor = getArguments().getInt("com.yalantis.ucrop.ToolbarColor", ContextCompat.getColor(getContext(), R.color.black));
        this.mActiveWidgetColor = getArguments().getInt("com.yalantis.ucrop.UcropColorWidgetActive", ContextCompat.getColor(getContext(), R.color.black));

        this.mToolbarWidgetColor = getArguments().getInt("com.yalantis.ucrop.UcropToolbarWidgetColor", ContextCompat.getColor(getContext(), com.yalantis.ucrop.R.color.ucrop_color_toolbar_widget));
        this.mToolbarTitle = intent.getStringExtra("com.yalantis.ucrop.UcropToolbarTitleText");
        this.mToolbarTitle = !TextUtils.isEmpty(this.mToolbarTitle) ? this.mToolbarTitle : this.getResources().getString(com.yalantis.ucrop.R.string.ucrop_label_edit_photo);
        this.mLogoColor = intent.getIntExtra("com.yalantis.ucrop.UcropLogoColor", ContextCompat.getColor(getContext(), com.yalantis.ucrop.R.color.ucrop_color_default_logo));
        this.mShowBottomControls = !intent.getBooleanExtra("com.yalantis.ucrop.HideBottomControls", false);
//        this.setupAppBar();
        this.initiateRootViews();
        if (this.mShowBottomControls) {
            View.inflate(getContext(), com.yalantis.ucrop.R.layout.ucrop_controls, mPhotoBox);
            this.mWrapperStateAspectRatio = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.state_aspect_ratio);
            this.mWrapperStateAspectRatio.setOnClickListener(this.mStateClickListener);
            this.mWrapperStateAspectRatio.setVisibility(View.GONE);

            this.mWrapperStateRotate = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.state_rotate);
            this.mWrapperStateRotate.setOnClickListener(this.mStateClickListener);

            this.mWrapperStateScale = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.state_scale);
            this.mWrapperStateScale.setOnClickListener(this.mStateClickListener);

            this.setupAspectRatioWidget(intent);
            this.setupRotateWidget();
            this.setupScaleWidget();
            this.setupStatesWrapper();
        }

    }

    private void initiateRootViews() {
        this.mGestureCropImageView = this.mUCropView.getCropImageView();
        this.mOverlayView = this.mUCropView.getOverlayView();
        this.mGestureCropImageView.setTransformImageListener(this.mImageListener);
        ImageView imageView = (ImageView) getView().findViewById(com.yalantis.ucrop.R.id.image_view_logo);
        imageView.setColorFilter(this.mLogoColor, PorterDuff.Mode.SRC_ATOP);
    }

    private void setupStatesWrapper() {
        ImageView stateScaleImageView = (ImageView) getView().findViewById(com.yalantis.ucrop.R.id.image_view_state_scale);
        ImageView stateRotateImageView = (ImageView) getView().findViewById(com.yalantis.ucrop.R.id.image_view_state_rotate);
        ImageView stateAspectRatioImageView = (ImageView) getView().findViewById(com.yalantis.ucrop.R.id.image_view_state_aspect_ratio);
        stateScaleImageView.setImageDrawable(new SelectedStateListDrawable(stateScaleImageView.getDrawable(), this.mActiveWidgetColor));
        stateRotateImageView.setImageDrawable(new SelectedStateListDrawable(stateRotateImageView.getDrawable(), this.mActiveWidgetColor));
        stateAspectRatioImageView.setImageDrawable(new SelectedStateListDrawable(stateAspectRatioImageView.getDrawable(), this.mActiveWidgetColor));
    }

    @TargetApi(21)
    private void setStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            if (window != null) {
                window.addFlags(-2147483648);
                window.setStatusBarColor(color);
            }
        }

    }

    @SuppressLint("RestrictedApi")
    private void setupAspectRatioWidget(@NonNull Intent intent) {
        int aspectRationSelectedByDefault = intent.getIntExtra("com.yalantis.ucrop.AspectRatioSelectedByDefault", 1);
        ArrayList<AspectRatio> aspectRatioList = intent.getParcelableArrayListExtra("com.yalantis.ucrop.AspectRatioOptions");
        if (aspectRatioList == null || aspectRatioList.isEmpty()) {
            aspectRationSelectedByDefault = 1;
            aspectRatioList = new ArrayList();
            aspectRatioList.add(new AspectRatio((String) null, 1.0F, 1.0F));
            aspectRatioList.add(new AspectRatio((String) null, 4.0F, 3.0F));
            aspectRatioList.add(new AspectRatio(this.getString(com.yalantis.ucrop.R.string.ucrop_label_original).toUpperCase(), 0.0F, 0.0F));
            aspectRatioList.add(new AspectRatio((String) null, 3.0F, 2.0F));
            aspectRatioList.add(new AspectRatio((String) null, 16.0F, 9.0F));
        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, -1);
        lp.weight = 1.0F;
        Iterator var8 = aspectRatioList.iterator();

        while (var8.hasNext()) {
            AspectRatio aspectRatio = (AspectRatio) var8.next();
            FrameLayout wrapperAspectRatio = (FrameLayout) this.getLayoutInflater(null).inflate(com.yalantis.ucrop.R.layout.ucrop_aspect_ratio, (ViewGroup) null);
            wrapperAspectRatio.setLayoutParams(lp);
            AspectRatioTextView aspectRatioTextView = (AspectRatioTextView) wrapperAspectRatio.getChildAt(0);
            aspectRatioTextView.setActiveColor(this.mActiveWidgetColor);
            aspectRatioTextView.setAspectRatio(aspectRatio);

            mLayoutAspectRatio = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.layout_aspect_ratio);
            mLayoutAspectRatio.addView(wrapperAspectRatio);
            this.mCropAspectRatioViews.add(wrapperAspectRatio);
        }

        ((ViewGroup) this.mCropAspectRatioViews.get(aspectRationSelectedByDefault)).setSelected(true);
        var8 = this.mCropAspectRatioViews.iterator();

        while (var8.hasNext()) {
            ViewGroup cropAspectRatioView = (ViewGroup) var8.next();
            cropAspectRatioView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CropImageDialog.this.mGestureCropImageView.setTargetAspectRatio(((AspectRatioTextView) ((ViewGroup) v).getChildAt(0)).getAspectRatio(v.isSelected()));
                    CropImageDialog.this.mGestureCropImageView.setImageToWrapCropBounds();
                    /*if (!v.isSelected()) {
                        Iterator var2 = CropImageDialog.this.mCropAspectRatioViews.iterator();

                        while (var2.hasNext()) {
                            ViewGroup cropAspectRatioView = (ViewGroup) var2.next();
                            cropAspectRatioView.setSelected(cropAspectRatioView == v);
                        }
                    }*/

                }
            });
        }

    }

    private void setupRotateWidget() {
        mTextViewRotateAngle = (TextView) getView().findViewById(com.yalantis.ucrop.R.id.text_view_rotate);
        mRotateScrollWheel = (HorizontalProgressWheelView) getView().findViewById(com.yalantis.ucrop.R.id.rotate_scroll_wheel);
        mRotateScrollWheel.setScrollingListener(new ScrollingListener() {
            public void onScroll(float delta, float totalDistance) {
                CropImageDialog.this.mGestureCropImageView.postRotate(delta / 42.0F);
            }

            public void onScrollEnd() {
                CropImageDialog.this.mGestureCropImageView.setImageToWrapCropBounds();
            }

            public void onScrollStart() {
                CropImageDialog.this.mGestureCropImageView.cancelAllAnimations();
            }
        });
        mRotateScrollWheel.setMiddleLineColor(this.mActiveWidgetColor);
        getView().findViewById(com.yalantis.ucrop.R.id.wrapper_reset_rotate).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImageDialog.this.resetRotation();
            }
        });
        getView().findViewById(com.yalantis.ucrop.R.id.wrapper_rotate_by_angle).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImageDialog.this.rotateByAngle(90);
            }
        });
    }

    @Optional
    @OnClick(com.yalantis.ucrop.R.id.wrapper_rotate_by_angle)
    protected void rotateRotate() {
        CropImageDialog.this.resetRotation();
    }

    @Optional
    @OnClick(com.yalantis.ucrop.R.id.wrapper_rotate_by_angle)
    protected void rotateImageTo90() {
        CropImageDialog.this.rotateByAngle(90);
    }

    private void setupScaleWidget() {
        mScaleScrollWheel = (HorizontalProgressWheelView) getView().findViewById(com.yalantis.ucrop.R.id.scale_scroll_wheel);
        mScaleScrollWheel.setScrollingListener(new ScrollingListener() {
            public void onScroll(float delta, float totalDistance) {
                if (delta > 0.0F) {
                    CropImageDialog.this.mGestureCropImageView.zoomInImage(CropImageDialog.this.mGestureCropImageView.getCurrentScale() + delta * ((CropImageDialog.this.mGestureCropImageView.getMaxScale() - CropImageDialog.this.mGestureCropImageView.getMinScale()) / 15000.0F));
                } else {
                    CropImageDialog.this.mGestureCropImageView.zoomOutImage(CropImageDialog.this.mGestureCropImageView.getCurrentScale() + delta * ((CropImageDialog.this.mGestureCropImageView.getMaxScale() - CropImageDialog.this.mGestureCropImageView.getMinScale()) / 15000.0F));
                }

            }

            public void onScrollEnd() {
                CropImageDialog.this.mGestureCropImageView.setImageToWrapCropBounds();
            }

            public void onScrollStart() {
                CropImageDialog.this.mGestureCropImageView.cancelAllAnimations();
            }
        });
        mScaleScrollWheel.setMiddleLineColor(mActiveWidgetColor);
    }

    private void setAngleText(float angle) {
        if (this.mTextViewRotateAngle != null) {
            this.mTextViewRotateAngle.setText(String.format(Locale.getDefault(), "%.1f°", new Object[]{Float.valueOf(angle)}));
        }

    }

    private void setScaleText(float scale) {
        if (this.mTextViewScalePercent != null) {
            this.mTextViewScalePercent.setText(String.format(Locale.getDefault(), "%d%%", new Object[]{Integer.valueOf((int) (scale * 100.0F))}));
        }

    }

    private void resetRotation() {
        this.mGestureCropImageView.postRotate(-this.mGestureCropImageView.getCurrentAngle());
        this.mGestureCropImageView.setImageToWrapCropBounds();
    }

    private void rotateByAngle(int angle) {
        this.mGestureCropImageView.postRotate((float) angle);
        this.mGestureCropImageView.setImageToWrapCropBounds();
    }

    private void setInitialState() {
        if (this.mShowBottomControls) {
            if (this.mWrapperStateAspectRatio.getVisibility() == View.VISIBLE) {
                this.setWidgetState(com.yalantis.ucrop.R.id.state_aspect_ratio);
            } else {
                this.setWidgetState(com.yalantis.ucrop.R.id.state_scale);
            }
        } else {
            this.setAllowedGestures(0);
        }

    }

    private void setWidgetState(@IdRes int stateViewId) {
        if (this.mShowBottomControls) {
            this.mWrapperStateAspectRatio.setSelected(stateViewId == com.yalantis.ucrop.R.id.state_aspect_ratio);
            this.mWrapperStateRotate.setSelected(stateViewId == com.yalantis.ucrop.R.id.state_rotate);
            this.mWrapperStateScale.setSelected(stateViewId == com.yalantis.ucrop.R.id.state_scale);
            this.mLayoutAspectRatio.setVisibility(stateViewId == com.yalantis.ucrop.R.id.state_aspect_ratio ? View.VISIBLE : View.GONE);

            mLayoutRotate = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.layout_rotate_wheel);
            this.mLayoutRotate.setVisibility(stateViewId == com.yalantis.ucrop.R.id.state_rotate ? View.VISIBLE : View.GONE);
            mLayoutScale = (ViewGroup) getView().findViewById(com.yalantis.ucrop.R.id.layout_scale_wheel);
            this.mLayoutScale.setVisibility(stateViewId == com.yalantis.ucrop.R.id.state_scale ? View.VISIBLE : View.GONE);
            if (stateViewId == com.yalantis.ucrop.R.id.state_scale) {
                this.setAllowedGestures(0);
            } else if (stateViewId == com.yalantis.ucrop.R.id.state_rotate) {
                this.setAllowedGestures(1);
            } else {
                this.setAllowedGestures(2);
            }

        }
    }

    private void setAllowedGestures(int tab) {
        this.mGestureCropImageView.setScaleEnabled(this.mAllowedGestures[tab] == 3 || this.mAllowedGestures[tab] == 1);
        this.mGestureCropImageView.setRotateEnabled(this.mAllowedGestures[tab] == 3 || this.mAllowedGestures[tab] == 2);
    }

    private void addBlockingView() {
        if (this.mBlockingView == null) {
            this.mBlockingView = new View(getContext());
            android.widget.RelativeLayout.LayoutParams lp = new android.widget.RelativeLayout.LayoutParams(-1, -1);
            this.mBlockingView.setLayoutParams(lp);
            this.mBlockingView.setClickable(true);
        }

        mPhotoBox.addView(this.mBlockingView);
    }

    protected void cropAndSaveImage() {
        this.mBlockingView.setClickable(true);
        this.mShowLoader = true;
        FragmentActivity activity = getActivity();
        activity.supportInvalidateOptionsMenu();

        this.mGestureCropImageView.cropAndSaveImage(this.mCompressFormat, this.mCompressQuality, new BitmapCropCallback() {

            public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                CropImageDialog.this.setResultUri(resultUri, CropImageDialog.this.mGestureCropImageView.getTargetAspectRatio(), offsetX, offsetY, imageWidth, imageHeight);
                Intent intent = new Intent();
                intent.putExtra(CameraDialog.Args.PATH_FILE_SAVE, resultUri.getPath());

                IScreenActivityView iScreenActivityView = getIScreenActivityView();
                iScreenActivityView.onFragmentResult(mRequestCode, Activity.RESULT_OK, intent);

                dismiss();
            }

            public void onCropFailure(@NonNull Throwable t) {
                CropImageDialog.this.setResultError(t);
                showErrorMessage("Ошибка при загрузке изображения: " + t.getMessage());
                LoggerUtils.exception(new RuntimeException("Ошибка при загрузке изображения: " + t.getMessage()));

                IScreenActivityView iScreenActivityView = getIScreenActivityView();
                iScreenActivityView.onFragmentResult(mRequestCode, Activity.RESULT_CANCELED, null);

                dismiss();
            }
        });
    }

    protected void setResultUri(Uri uri, float resultAspectRatio, int offsetX, int offsetY, int imageWidth, int imageHeight) {
//        this.setResult(-1, (new Intent()).putExtra("com.yalantis.ucrop.OutputUri", uri).putExtra("com.yalantis.ucrop.CropAspectRatio", resultAspectRatio).putExtra("com.yalantis.ucrop.ImageWidth", imageWidth).putExtra("com.yalantis.ucrop.ImageHeight", imageHeight).putExtra("com.yalantis.ucrop.OffsetX", offsetX).putExtra("com.yalantis.ucrop.OffsetY", offsetY));
    }

    protected void setResultError(Throwable throwable) {
//        this.setResult(96, (new Intent()).putExtra("com.yalantis.ucrop.Error", throwable));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentActivity activity = requireActivity();
        activity.finish();
    }

    static {
        DEFAULT_COMPRESS_FORMAT = CompressFormat.JPEG;
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface GestureTypes {
    }

}