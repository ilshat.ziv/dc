package com.decenturion.mobile.ui.fragment.referral.history.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.referral.history.IHistoryTransactionListInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;
import com.decenturion.mobile.ui.fragment.referral.history.model.State;
import com.decenturion.mobile.ui.fragment.referral.history.view.IHistoryView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class HistoryPresenter extends Presenter<IHistoryView> implements IHistoryPresenter {

    private IHistoryTransactionListInteractor mIHistoryTransactionListInteractor;
    private ILocalistionManager mILocalistionManager;

    private HistoryModelView mHistoryModelView;

    // TODO: 24.09.2018 костыль, поставить защиту от повторающихся запросов
    private boolean isLoadingPage;

    public HistoryPresenter(IHistoryTransactionListInteractor iHistoryTransactionListInteractor,
                            ILocalistionManager iLocalistionManager) {
        mIHistoryTransactionListInteractor = iHistoryTransactionListInteractor;
        mILocalistionManager = iLocalistionManager;

        mHistoryModelView = new HistoryModelView();
    }

    @Override
    public void bindViewData(String type) {
        mHistoryModelView = new HistoryModelView();
        mHistoryModelView.setType(type);

        if (isLoadingPage) {
            return;
        }

        showProgressView();

        Observable<HistoryModelView> obs = mIHistoryTransactionListInteractor.getTransactionsInfo(
                mHistoryModelView
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<HistoryModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(HistoryModelView model) {
        mHistoryModelView = model;
        model.setStates(initStateData());
        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    @NonNull
    private ArrayList<State> initStateData() {
        ArrayList<State> stateData = new ArrayList<>();
        stateData.add(new State(mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_invite), "invite"));
        stateData.add(new State(mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_status), "status"));
        stateData.add(new State(mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_deposit), "deposit"));
        stateData.add(new State(mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_referral), "referral"));
        return stateData;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void onBottomScrolled() {
        if (mHistoryModelView.getOffset() >= mHistoryModelView.getTotal() || isLoadingPage) {
            return;
        }
        uploadData();
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    private void uploadData() {
        showProgressView();

        Observable<HistoryModelView> obs = mIHistoryTransactionListInteractor.getTransactionsInfo(
                mHistoryModelView
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<HistoryModelView>) this::uploadDataSuccess,
                this::uploadDataFailure
        );
    }

    private void uploadDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadDataSuccess(HistoryModelView model) {
        mHistoryModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        isLoadingPage = true;
    }

    @Override
    public void hideProgressView() {
        super.hideProgressView();
        isLoadingPage = false;
    }
}
