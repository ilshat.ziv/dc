package com.decenturion.mobile.ui.fragment.token.send.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.send.SendTokenComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.twofa.EnableTwoFAActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.token.send.SendTokenSuccessFragmentDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.send.model.SendToModelView;
import com.decenturion.mobile.ui.fragment.token.send.model.SendTokenModelView;
import com.decenturion.mobile.ui.fragment.token.send.presenter.ISendTokenPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SendTokenFragment extends BaseFragment implements ISendTokenView,
        SpinnerView.OnSpinnerItemClickListener {

    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_SCORE_TYPE = "TOKEN_SCORE_TYPE";

    private int mTokenId;

    /* UI */

    @BindView(R.id.nameTokenView)
    TextView mNameTokenView;

    @BindView(R.id.sendToView)
    SpinnerView mSenToView;

    @BindView(R.id.walletAddressView)
    EditTextView mDcnWalletView;

    @BindView(R.id.amountView)
    EditTextView mAmountView;

    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    ISendTokenPresenter mISendTokenPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SendTokenComponent sendTokenComponent = appDelegate
                .getIDIManager()
                .plusSendTokenComponent();
        sendTokenComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_send_token_new, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSenToView.showLabel();
        mSenToView.setLabel(mILocalistionManager.getLocaleString(R.string.app_token_send_to_title));
        mSenToView.setVisibleDivider(true);
        mSenToView.setOnSpinnerItemClickListener(this);

        mDcnWalletView.showLabel();
        mDcnWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_send_wallet_title));
        mDcnWalletView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_send_wallet_placeholder2));
        mDcnWalletView.editMode();
        mDcnWalletView.setVisibleDivider(true);

        mAmountView.showLabel();
        mAmountView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_send_amount_title));
        mAmountView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_send_amount_placeholder));
        mAmountView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mAmountView.editMode();
        mAmountView.setVisibleDivider(true);

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel("Google Authenticator");
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_send_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setNavigationOnClickListener(v -> {
            ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity1.getINavigationManager();
            iNavigationManager.navigateToBack();
        });
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISendTokenPresenter.bindView(this);
        mISendTokenPresenter.bindViewData(mTokenId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISendTokenPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISendTokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISendTokenPresenter.unbindView();
        mISendTokenPresenter.onSaveInstanceState(outState,
                mDcnWalletView.getValue(),
                mAmountView.getValue(),
                mSecretCodeView.getValue()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISendTokenPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull SendTokenModelView model, @NonNull SendToModelView model1) {
        mNameTokenView.setText(model.getSymbol());

        mDcnWalletView.setText(model.getDcntWallet());
        mAmountView.setText(model.getAmount());
        mSecretCodeView.setText(model.getTwoFA());

        mSenToView.setData(model1.getSendToData());

        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        String string = mILocalistionManager.getLocaleString(R.string.app_score_main_title);
        iToolbarController.setTitle(string.replace("{symbol}", model.getSymbol()));
    }

    @OnClick(R.id.sendTokensButtonView)
    protected void onSendTokens() {
        mISendTokenPresenter.sendToken(
                mDcnWalletView.getValue(),
                mAmountView.getValue(),
                mSecretCodeView.getValue()
        );
    }

    @Override
    public void onEnableTwoFA() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, EnableTwoFAActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_ENABLE_TWOFA);
    }

    @Override
    public void onSendTokenSuccess() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(SendTokenSuccessFragmentDialog.class);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_ENABLE_TWOFA && resultCode == Activity.RESULT_OK) {
            onSendTokens();
        }
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        String itemKey = iSpinnerItem.getItemKey();
        switch (itemKey) {
            case "0" : {
                mDcnWalletView.setVisibility(View.VISIBLE);
                mAmountView.setVisibility(View.VISIBLE);
                mSecretCodeView.setVisibility(View.VISIBLE);
                break;
            }
            case "1" : {
                mDcnWalletView.setVisibility(View.VISIBLE);
                mAmountView.setVisibility(View.VISIBLE);
                mSecretCodeView.setVisibility(View.VISIBLE);
                break;
            }
            case "2" : {
                mDcnWalletView.setVisibility(View.GONE);
                mAmountView.setVisibility(View.GONE);
                mSecretCodeView.setVisibility(View.VISIBLE);
                break;
            }
        }
    }
}
