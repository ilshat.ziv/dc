package com.decenturion.mobile.ui.fragment.pass.edit.model;

import java.util.ArrayList;

public class SexModelView {

    private ArrayList<Sex> mSexData;

    public SexModelView() {
    }

    public SexModelView(ArrayList<Sex> countryData) {
        mSexData = countryData;
    }

    public ArrayList<Sex> getSexData() {
        return mSexData;
    }
}
