package com.decenturion.mobile.ui.fragment.seller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.ui.fragment.seller.token.view.SellerTokenFragment;
import com.decenturion.mobile.ui.fragment.seller.passport.view.SellerPassportFragment;
import com.decenturion.mobile.ui.fragment.seller.trade.view.SellerTraidsFragment;

public class SellerProfileAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private SparseArray<Fragment> mHashMap;
    private Bundle mBundle;

    private ILocalistionManager mILocalistionManager;

    private SellerProfileAdapter(FragmentManager fm, ILocalistionManager iLocalistionManager) {
        super(fm);
        this.mNumOfTabs = 3;
        mHashMap = new SparseArray<>();
        mILocalistionManager = iLocalistionManager;
    }

    public SellerProfileAdapter(FragmentManager fm, ILocalistionManager iLocalistionManager, Bundle bundle) {
        this(fm, iLocalistionManager);
        mBundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        if (mHashMap.get(position) != null) {
            return mHashMap.get(position);
        }
        Fragment fragment = null;
        switch (position) {
            case 0 : {
                fragment = new SellerPassportFragment();
                fragment.setArguments(mBundle);
                break;
            }
            case 1 : {
                fragment = new SellerTokenFragment();
                fragment.setArguments(mBundle);
                break;
            }
            case 2 : {
                fragment = new SellerTraidsFragment();
                fragment.setArguments(mBundle);
                break;
            }
            default : {
                return null;
            }
        }
        mHashMap.put(position, fragment);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0 : {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_passport);
            }
            case 1 : {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_tokens);
            }
            case 2 : {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_deals);
            }
        }
        throw new RuntimeException();
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}