package com.decenturion.mobile.ui.fragment.score.send.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.score.send.main.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.main.view.IMainScoreSendTokenView;

public interface IMainScoreSendTokenPresenter extends IPresenter<IMainScoreSendTokenView> {

    void bindViewData(int tokenId);

    void onSaveInstanceState(@NonNull Bundle outState,
                             String dcntWallet,
                             String amount,
                             String twofa);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    void sendToken(@NonNull String dcntWallet,
                   @NonNull String amount,
                   MethodSendToken method,
                   @NonNull String twofa
    );

    void sendToExternal(@NonNull String address, @NonNull String amount, @NonNull String twofa);

    void sendToTrade(@NonNull String amount, @NonNull MethodSendToken method, @NonNull String twofa);
}
