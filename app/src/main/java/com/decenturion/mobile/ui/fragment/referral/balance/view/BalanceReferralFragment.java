package com.decenturion.mobile.ui.fragment.referral.balance.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.balance.model.BalanceReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.balance.presenter.IBalanceReferralPresenter;

import javax.inject.Inject;

import butterknife.BindView;

public class BalanceReferralFragment extends BaseFragment implements IBalanceReferralView {

    private String mTotal;
    private String mHold;
    private String mAvailable;

    /* UI */

    @BindView(R.id.balanceView)
    TextView mBalanceView;

    @BindView(R.id.freeBalanceView)
    TextView mFreeBalanceView;

    @BindView(R.id.frezeeBalanceView)
    TextView mFrezeeBalanceView;

    /* DI */

    @Inject
    IBalanceReferralPresenter mIStatisticReferralPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ReferralComponent component = appDelegate
                .getIDIManager()
                .plusReferallComponent();
        component.inject(this);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTotal = arguments.getString(Args.TOTAL, "0.0");
            mHold = arguments.getString(Args.HOLD, "0.0");
            mAvailable = arguments.getString(Args.AVAILABLE, "0.0");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_referral_balance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBalanceView.setText(mTotal);
        mFreeBalanceView.setText(mAvailable);
        mFrezeeBalanceView.setText(mHold);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIStatisticReferralPresenter.bindView(this);
        mIStatisticReferralPresenter.bindViewData();
    }

    @Override
    public void onBindViewData(@NonNull BalanceReferralModelView model) {
//        mPriceCitizenView.setText(model.getCitizenPrice() + " $");
//        mEarnedBtcView.setText(model.getEarnedBtc() + " btc");
//        mEarnedEthView.setText(model.getEarnedEth() + " eth");
    }

    @Override
    public void onResume() {
        super.onResume();
        mIStatisticReferralPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIStatisticReferralPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIStatisticReferralPresenter.unbindView();
    }

    public interface Args {
        String TOTAL = "TOTAL";
        String AVAILABLE = "AVAILABLE";
        String HOLD = "HOLD";
    }
}