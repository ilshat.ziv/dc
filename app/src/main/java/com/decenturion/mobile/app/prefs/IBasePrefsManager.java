package com.decenturion.mobile.app.prefs;

import android.support.annotation.NonNull;

import java.util.HashSet;
import java.util.Set;

public interface IBasePrefsManager {

    boolean getParam(String key, boolean defValue);

    double getParam(String key, double defValue);

    String getParam(String key, String defValue);

    int getParam(String key, int defValue);

    long getParam(String key, long defValue);

    void setParam(@NonNull String nameSharedPreference, String key, Object value);

    void setParam(String key, Object value);

    void clear();

    Set getParam(String cookies, HashSet hashSet);
}
