package com.decenturion.mobile.di.dagger.referral.history;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.referral.history.HistoryTransactionListInteractor;
import com.decenturion.mobile.business.referral.history.IHistoryTransactionListInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.referral.history.presenter.HistoryPresenter;
import com.decenturion.mobile.ui.fragment.referral.history.presenter.IHistoryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ReferralHistoryModule {

    @Provides
    @ReferralHistorylScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ReferralHistorylScope
    IHistoryTransactionListInteractor provideIHistoryTransactionListInteractor(IReferralClient iReferralClient,
                                                                               ILocalistionManager iLocalistionManager) {

        return new HistoryTransactionListInteractor(iReferralClient, iLocalistionManager);
    }

    @Provides
    @ReferralHistorylScope
    IHistoryPresenter provideIHistoryPresenter(IHistoryTransactionListInteractor iHistoryTransactionListInteractor,
                                               ILocalistionManager iLocalistionManager) {
        return new HistoryPresenter(iHistoryTransactionListInteractor, iLocalistionManager);
    }
}
