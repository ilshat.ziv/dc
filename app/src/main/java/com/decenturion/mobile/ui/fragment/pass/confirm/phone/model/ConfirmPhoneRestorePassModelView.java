package com.decenturion.mobile.ui.fragment.pass.confirm.phone.model;

public class ConfirmPhoneRestorePassModelView {

    private String mPhone;

    public ConfirmPhoneRestorePassModelView(String phone) {
        mPhone = phone;
    }

    public String getPhone() {
        return mPhone;
    }
}
