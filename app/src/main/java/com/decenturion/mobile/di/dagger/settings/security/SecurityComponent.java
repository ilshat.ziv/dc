package com.decenturion.mobile.di.dagger.settings.security;

import com.decenturion.mobile.ui.fragment.settings.security.view.SecurityFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SecurityModule.class
})

@SecurityScope
public interface SecurityComponent {

    void inject(SecurityFragment view);
}
