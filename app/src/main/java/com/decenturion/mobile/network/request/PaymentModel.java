package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentModel {

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("ordered")
    private boolean isOrdered = true;

    public PaymentModel() {
    }

    @JsonIgnore
    public PaymentModel(String coin) {
        mCoin = coin;
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoin = coin;
    }
}
