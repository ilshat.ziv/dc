package com.decenturion.mobile.ui.fragment.passport.phisical.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.passport.phisical.view.IPhisicalPassportView;

public interface IPhisicalPassportPresenter extends IPresenter<IPhisicalPassportView> {

    void bindViewData();

    void setDeliveryData(@NonNull String value,
                         @NonNull String value1,
                         @NonNull String value2,
                         @NonNull String value3,
                         @NonNull String selection,
                         @NonNull String value4);

    void getPaymentAddress(@NonNull String coin);

    void skipDelivery();
}
