package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStatusAddressModel {

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("currency")
    private String mCurrency;

    public PaymentStatusAddressModel() {
    }

    @JsonIgnore
    public PaymentStatusAddressModel(String status, String currency) {
        mStatus = status;
        mCurrency = currency;
    }

    @JsonIgnore
    public void setStatus(String status) {
        mStatus = status;
    }

    @JsonIgnore
    public void setCurrency(String currency) {
        mCurrency = currency;
    }
}
