package com.decenturion.mobile.ui.fragment.referral.invite.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.referral.invite.IInviteListInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteListModelView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;
import com.decenturion.mobile.ui.fragment.referral.invite.view.IInviteListView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class InviteListPresenter extends Presenter<IInviteListView> implements IInviteListPresenter {

    private IInviteListInteractor mIInviteListInteractor;
    private InviteListModelView mActionListModelView;

    // TODO: 24.09.2018 костыль, поставить защиту от повторающихся запросов
    private boolean isLoadingPage;

    public InviteListPresenter(IInviteListInteractor iInviteListInteractor) {
        mIInviteListInteractor = iInviteListInteractor;
    }

    @Override
    public void bindViewData() {
        if (isLoadingPage) {
            return;
        }

        showProgressView();

        Observable<InviteListModelView> obs = mIInviteListInteractor.getInvitations()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<InviteListModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(InviteListModelView model) {
        mActionListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void onBottomScrolled() {
        if (mActionListModelView.getOffset() >= mActionListModelView.getTotal() || isLoadingPage) {
            return;
        }
        uploadData();
    }

    @Override
    public void removeInvite(int inviteId) {
        showProgressView();

        Observable<InviteListModelView> obs = mIInviteListInteractor.removeInvite(inviteId)
                .map(d -> {
                    List<InviteModel> historyList = mActionListModelView.getHistoryList();
                    for (InviteModel inviteModel : historyList) {
                        if (inviteModel.getId() == d.getId()) {
                            historyList.remove(inviteModel);
                            return mActionListModelView;
                        }
                    }
                    return mActionListModelView;
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<InviteListModelView>) this::removeInviteSuccess,
                this::removeInviteFailure
        );
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    private void removeInviteSuccess(InviteListModelView model) {
        mActionListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void removeInviteFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadData() {
//        showProgressView();
//
//        Observable<InviteListModelView> obs = mIInviteListInteractor.getInvitations(
//                mActionListModelView
//        )
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<InviteListModelView>) this::uploadDataSuccess,
//                this::uploadDataFailure
//        );
    }

    private void uploadDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadDataSuccess(InviteListModelView model) {
        mActionListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        isLoadingPage = true;
    }

    @Override
    public void hideProgressView() {
        super.hideProgressView();
        isLoadingPage = false;
    }
}
