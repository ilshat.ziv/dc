package com.decenturion.mobile.business.aggrement;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.ui.fragment.disclaimer.model.UserAggrementModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import rx.Observable;

public class UserAggrementInteractor implements IUserAggrementInteractor {

    private ILocalistionManager mILocalistionManager;
    private Context mContext;

    public UserAggrementInteractor(Context context, ILocalistionManager iLocalistionManager) {
        mContext = context;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<UserAggrementModelView> getTermsInfo() {
        return Observable.just(mILocalistionManager.getLocale())
                .map(d -> {
                    String text = null;
                    try {
                        text = readFromFile(mContext, "data/Terms_" + d + ".html");
                        if (TextUtils.isEmpty(text)) {
                            text = readFromFile(mContext, "data/Terms_" + ILocalistionManager.DEFAULT_LOCALISATION + ".html");
                        }
                    } catch (IOException e) {
                        LoggerUtils.exception(e);
                    }

                    return text;
                })
                .map(UserAggrementModelView::new);
    }

    private static String readFromFile(Context context, String fileName) throws IOException {
        String result = "";

        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(fileName);

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString;
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            result = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            LoggerUtils.exception(e);
        }

        return result;
    }
}
