package com.decenturion.mobile.ui.fragment.profile_v2.log.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;

public interface ILogsView extends IScreenFragmentView {

    void onBindViewData(@NonNull LogModelView model);

    void logoutSuccess();
}
