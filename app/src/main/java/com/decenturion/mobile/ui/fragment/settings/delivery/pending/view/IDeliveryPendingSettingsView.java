package com.decenturion.mobile.ui.fragment.settings.delivery.pending.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.model.DeliveryPendingSettingsModelView;

public interface IDeliveryPendingSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull DeliveryPendingSettingsModelView model3);
}
