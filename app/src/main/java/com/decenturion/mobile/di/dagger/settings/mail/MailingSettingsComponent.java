package com.decenturion.mobile.di.dagger.settings.mail;

import com.decenturion.mobile.ui.fragment.settings.mail.view.MailingSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        MailingSettingsModule.class
})

@MailingSettingsScope
public interface MailingSettingsComponent {

    void inject(MailingSettingsFragment view);
}
