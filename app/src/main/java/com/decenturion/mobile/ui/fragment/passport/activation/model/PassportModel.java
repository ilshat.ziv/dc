package com.decenturion.mobile.ui.fragment.passport.activation.model;


import com.decenturion.mobile.network.response.model.Resident;

public class PassportModel {

    private boolean isActive;

    public PassportModel() {
    }

    public PassportModel(Resident resident) {
        this.isActive = resident.isActive();
    }

    public boolean isActive() {
        return isActive;
    }
}
