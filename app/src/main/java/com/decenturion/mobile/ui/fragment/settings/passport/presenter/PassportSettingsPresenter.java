package com.decenturion.mobile.ui.fragment.settings.passport.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.passport.IPassportSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PassportSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.Sex;
import com.decenturion.mobile.ui.fragment.settings.passport.model.SexModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.view.IPassportSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PassportSettingsPresenter extends Presenter<IPassportSettingsView> implements IPassportSettingsPresenter {

    private Context mContext;
    private IPassportSettingsInteractor mIPassportSettingsInteractor;
    private ILocalistionManager mILocalistionManager;

    private PassportSettingsModelView mPassportSettingsModelView;
    private CountryModelView mCountryModelView;
    private SexModelView mSexModelView;

    public PassportSettingsPresenter(Context context,
                                     IPassportSettingsInteractor iPassportSettingsInteractor,
                                     ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mContext = context;
        mIPassportSettingsInteractor = iPassportSettingsInteractor;

        mPassportSettingsModelView = new PassportSettingsModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        bundle.putString("Birth", mPassportSettingsModelView.getBirth());
        bundle.putString("City", mPassportSettingsModelView.getCity());
        bundle.putString("Country", mPassportSettingsModelView.getCountry());
        bundle.putString("Firstname", mPassportSettingsModelView.getFirstname());
        bundle.putString("Lastname", mPassportSettingsModelView.getLastname());
        bundle.putString("State", mPassportSettingsModelView.getSex());

        PhotoModelView photoModelView = mPassportSettingsModelView.getPhotoModelView();
        if (photoModelView != null) {
            bundle.putString("Min", photoModelView.getMin());
            bundle.putString("Original", photoModelView.getOriginal());
        }
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mPassportSettingsModelView.setBirth(savedInstanceState.getString("Birth", ""));
        mPassportSettingsModelView.setCity(savedInstanceState.getString("City", ""));
        mPassportSettingsModelView.setCountry(savedInstanceState.getString("Country", ""));
        mPassportSettingsModelView.setFirstname(savedInstanceState.getString("Firstname", ""));
        mPassportSettingsModelView.setLastname(savedInstanceState.getString("Lastname", ""));
        mPassportSettingsModelView.setSex(savedInstanceState.getString("State", ""));

        String minPhoto = savedInstanceState.getString("Min", "");
        String photo = savedInstanceState.getString("Original", "");
        if (!TextUtils.isEmpty(minPhoto) || !TextUtils.isEmpty(photo)) {
            mPassportSettingsModelView.setPhotoModelView(new PhotoModelView(minPhoto, photo));
        }

        Observable<PassportSettingsModelView> obs = Observable.just(mPassportSettingsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PassportSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData() {
        Observable<PassportSettingsModelView> obs = mIPassportSettingsInteractor.getPassportInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PassportSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(PassportSettingsModelView model) {
        mPassportSettingsModelView = model;

        mSexModelView = new SexModelView(initGenderData());
        mCountryModelView = new CountryModelView(initCountryData());

        if (mView != null) {
            mView.onBindViewData(mSexModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveChanges(@NonNull String firstName,
                            @NonNull String secondName,
                            long birth,
                            @NonNull String sex,
                            @NonNull String country,
                            @NonNull String city) {
        showProgressView();

        Observable<UpdatePassportResponse> obs = mIPassportSettingsInteractor.saveChanges(
                firstName,
                secondName,
                birth,
                sex,
                country,
                city)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UpdatePassportResponse>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(UpdatePassportResponse response) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));
        if (mView != null) {
            mView.onSavedChanges();
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    @NonNull
    private ArrayList<Sex> initGenderData() {
        ArrayList<Sex> sexData = new ArrayList<>();
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_male), "male"));
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_female), "female"));
        return sexData;
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    @Override
    public void uploadPhoto(@NonNull Bitmap photo) {
        showProgressView();

        Observable<UploadPhotoResponse> obs = mIPassportSettingsInteractor.uploadPhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UploadPhotoResponse>) this::uploadPhotoSuccess,
                this::uploadPhotoFailure
        );
    }

    private void uploadPhotoFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void uploadPhotoSuccess(UploadPhotoResponse uploadPhotoResponse) {
        hideProgressView();
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

}
