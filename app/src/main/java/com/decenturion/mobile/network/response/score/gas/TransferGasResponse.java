package com.decenturion.mobile.network.response.score.gas;

import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.model.TransferGas;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferGasResponse extends BaseResponse {

    @JsonProperty("result")
    private TransferGas mResult;

    public TransferGasResponse() {
    }

    @JsonIgnore
    public TransferGas getResult() {
        return mResult;
    }
}
