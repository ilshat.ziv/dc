package com.decenturion.mobile.ui.fragment.passport.phisical.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PhisicalPassportModelView;

public interface IPhisicalPassportView extends IScreenFragmentView {

    void onBindViewData(@NonNull CoinModelView model2,
                        @NonNull CountryModelView model1,
                        @NonNull PhisicalPassportModelView model);

    void onDeliveryPassportDataSeted(@NonNull PhisicalPassportModelView model);

    void onBindPaymentViewData(PhisicalPassportModelView address);

    void onSkipedDelivery();
}
