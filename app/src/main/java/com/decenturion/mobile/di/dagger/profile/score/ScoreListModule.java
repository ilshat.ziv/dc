package com.decenturion.mobile.di.dagger.profile.score;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.profile.score.IScoreListInteractor;
import com.decenturion.mobile.business.profile.score.ScoreListInteractor;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.presenter.IScoreListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.presenter.ScoreListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ScoreListModule {

    @Provides
    @ScoreListScope
    IScoreListInteractor provideIScoreListInteractor(ILocalistionManager iLocalistionManager) {
        return new ScoreListInteractor(iLocalistionManager);
    }

    @Provides
    @ScoreListScope
    IScoreListPresenter provideIScoreListPresenter(IScoreListInteractor iScoreListInteractor) {
        return new ScoreListPresenter(iScoreListInteractor);
    }
}
