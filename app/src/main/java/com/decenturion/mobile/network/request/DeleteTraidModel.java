package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteTraidModel {

    @JsonProperty("uuid")
    private String mTraidUUID;


    public DeleteTraidModel() {
    }

    @JsonIgnore
    public DeleteTraidModel(String traidUUID) {
        mTraidUUID = traidUUID;
    }

    @JsonIgnore
    public void setTraidUUID(@NonNull String traidUUID) {
        mTraidUUID = traidUUID;
    }
}
