package com.decenturion.mobile.ui.dialog.token.send;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;

import butterknife.ButterKnife;

public class SendTokenFailFragmentDialog extends SingleDialogFragment {

    /* UI */


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
//            mTitle = arguments.getString(TITLE, mTitle);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText("Send DCNT token");
        setPositiveText(R.string.fr_dialog_send_token_success_positive);
        setNegativeText(R.string.fr_dialog_send_token_fail_nagative);

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_send_token_fail);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        setupUi();

        return arbitrationRequestDialog;
    }

    private void setupUi() {
    }

    @Override
    public void onPositiveClickButton() {
    }

    @Override
    public void onNegativeClickButton() {
    }
}
