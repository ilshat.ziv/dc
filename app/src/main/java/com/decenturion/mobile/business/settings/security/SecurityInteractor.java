package com.decenturion.mobile.business.settings.security;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.DisableGfaModel;
import com.decenturion.mobile.network.request.EnableGfaModel;
import com.decenturion.mobile.network.request.UpdateSettingsModel;

import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.Settings;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.network.response.settings.UpdateSettingsResponse;
import com.decenturion.mobile.network.response.settings.UpdateSettingsResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class SecurityInteractor implements ISecurityInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public SecurityInteractor(IResidentClient IResidentClient,
                              IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<SecurityModelView> getSettingsInfo() {
        Observable<SecurityModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new SecurityModelView(d.getSettings(), resident.isG2fa());
                });


        return obs1
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    OrmResidentModel m1 = mIResidentRepository.getResidentInfo();
                    OrmSettingsModel m2 = mIResidentRepository.getSettingsInfo();
                    return new SecurityModelView(m2, m1.isG2fa());
                });
    }

    @Override
    public Observable<SecurityModelView> saveChanges(boolean isShowName,
                                                     boolean isShowBirth,
                                                     boolean isShowPhoto,
                                                     boolean isShowTokens,
                                                     boolean isShowMoney,
                                                     boolean isShowPower,
                                                     boolean isShowGlory,

                                                     boolean isEnabledGfa, boolean isEnableGfa,
                                                     String secretString, String secretCode) {

        Observable<Settings> obs1 = saveSettings(isShowName, isShowBirth, isShowPhoto, isShowTokens,
                isShowMoney, isShowPower, isShowGlory);

        Observable<Boolean> obs2;
        if (isEnabledGfa != isEnableGfa) {
            obs2 = saveGfa(isEnableGfa, secretString, secretCode);
        } else {
            obs2 = mIResidentClient.getResident()
                    .map(ResidentResponse::getResult)
                    .map(ResidentResult::getResident)
                    .map(d -> {
                        try {
                            mIResidentRepository.saveResidentInfo(d);
                        } catch (Exception e) {
                            LoggerUtils.exception(e);
                        }

                        return d.isG2fa();
                    });
        }

        return Observable.zip(obs1, obs2, SecurityModelView::new);
    }

    private Observable<Settings> saveSettings(boolean isShowName,
                              boolean isShowBirth,
                              boolean isShowPhoto,
                              boolean isShowTokens,
                              boolean isShowMoney,
                              boolean isShowPower,
                              boolean isShowGlory) {

        UpdateSettingsModel model = new UpdateSettingsModel();
        model.setShowName(isShowName);
        model.setShowBirth(isShowBirth);
        model.setShowPhoto(isShowPhoto);
        model.setShowTokens(isShowTokens);
        model.setShowMoney(isShowMoney);
        model.setShowGlory(isShowGlory);
        model.setShowPower(isShowPower);

        return mIResidentClient.updateSettings(model)
                .map(UpdateSettingsResponse::getResult)
                .map(UpdateSettingsResult::getSettings)
                .doOnNext(d -> {
                    try {
                        mIResidentRepository.saveSettings(d);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    private Observable<Boolean> saveGfa(boolean isEnable, @NonNull String secretString, @NonNull String secretCode) {
        if (isEnable) {
            EnableGfaModel model = new EnableGfaModel();
            model.setSecret(secretString);
            model.setCode(secretCode);
            return mIResidentClient.enableG2fa(model)
                    .map(d -> true)
                    .doOnNext(this::updateGfaState);
        } else {
            DisableGfaModel model = new DisableGfaModel();
            model.setCode(secretCode);
            return mIResidentClient.disableG2fa(model)
                    .map(d -> false)
                    .doOnNext(this::updateGfaState);
        }
    }

    private void updateGfaState(Boolean d) {
        try {
            OrmResidentModel residentInfo = mIResidentRepository.getResidentInfo();
            residentInfo.setG2fa(d);
            mIResidentRepository.saveResidentInfo(residentInfo);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
