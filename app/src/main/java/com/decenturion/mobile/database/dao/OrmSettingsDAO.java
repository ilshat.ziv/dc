package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmSettingsDAO extends BaseDao<OrmSettingsModel, Integer> {

    public OrmSettingsDAO(ConnectionSource connectionSource,
                          Class<OrmSettingsModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
