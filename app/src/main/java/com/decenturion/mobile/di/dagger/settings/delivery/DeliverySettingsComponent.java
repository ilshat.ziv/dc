package com.decenturion.mobile.di.dagger.settings.delivery;

import com.decenturion.mobile.ui.fragment.settings.delivery.DeliverySettingsPayCompleteFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.DeliverySettingsPayFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.view.DeliverySettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DeliverySettingsModule.class
})

@DeliverySettingsScope
public interface DeliverySettingsComponent {

    void inject(DeliverySettingsPayFragment view);

    void inject(DeliverySettingsPayCompleteFragment view);

    void inject(DeliverySettingsFragment view);
}
