package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestorePassModel {

    @JsonProperty("username")
    private String mUsername;

    @JsonProperty("recaptcha")
    private String mCaptcha;

    public RestorePassModel() {
    }

    @JsonIgnore
    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    @JsonIgnore
    public void setCaptcha(String captcha) {
        mCaptcha = captcha;
    }
}
