package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;

public class NotNetConnectionException extends AppException {

    public NotNetConnectionException() {
        super("Check internet connection");
    }

    public NotNetConnectionException(String message) {
        super(message);
    }

    public NotNetConnectionException(Throwable throwable) {
        super(throwable.getMessage());
    }
}
