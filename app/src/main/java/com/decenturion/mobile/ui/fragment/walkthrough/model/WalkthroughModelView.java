package com.decenturion.mobile.ui.fragment.walkthrough.model;

import java.util.List;

public class WalkthroughModelView {

    private List<PageModelView> mPageModelViewList;

    public WalkthroughModelView() {
    }

    public WalkthroughModelView(List<PageModelView> pageModelViewList) {
        mPageModelViewList = pageModelViewList;
    }

    public List<PageModelView> getPageModelViewList() {
        return mPageModelViewList;
    }
}
