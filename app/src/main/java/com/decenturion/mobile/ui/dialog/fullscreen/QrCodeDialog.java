package com.decenturion.mobile.ui.dialog.fullscreen;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.utils.LoggerUtils;
import com.google.zxing.WriterException;

import javax.inject.Inject;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.WINDOW_SERVICE;

public class QrCodeDialog extends FullScreenDialogFragment {

    public static final String QR_CODE_DATA = "QR_CODE_DATA";
    private String mQrCodeData;

    /* UI */

    @BindView(R.id.qrCodeImageView)
    ImageView mImageView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    public static void show(@NonNull FragmentManager fm, String tag, String qrCodeData) {
        final QrCodeDialog dialog = new QrCodeDialog();
        Bundle bundle = new Bundle();
        bundle.putString(QR_CODE_DATA, qrCodeData);
        dialog.setArguments(bundle);
        dialog.show(fm, tag);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        AppComponent component = appDelegate
                .getIDIManager()
                .getAppComponent();
        component.inject(this);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mQrCodeData = arguments.getString(QR_CODE_DATA, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        setContentView(R.layout.fr_dialog_qrcode);
        setFullVisual(true);
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_qrcode_title));

        setEnabledPositiveClickButton(false);

        assert view != null;
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (TextUtils.isEmpty(mQrCodeData)) {
            LoggerUtils.exception(new RuntimeException("QrCode data must be not empty"));
            dismiss();
        } else {
            setupUi();
        }
    }

    private void setupUi() {
        FragmentActivity activity = requireActivity();
        WindowManager manager = (WindowManager) activity.getSystemService(WINDOW_SERVICE);
        assert manager != null;
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        QRGEncoder qrgEncoder = new QRGEncoder(
                mQrCodeData,
                null,
                QRGContents.Type.TEXT,
                smallerDimension);
        try {
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            mImageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            LoggerUtils.exception(e);
        }
    }
}
