package com.decenturion.mobile.di.dagger.pass.reset;

import com.decenturion.mobile.ui.fragment.pass.reset.view.ResetPasswordFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ResetPassModule.class
})

@ResetPassScope
public interface ResetPassComponent {

    void inject(ResetPasswordFragment view);
}
