package com.decenturion.mobile.business.passport.phisical;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.DeliveryPassportModel;
import com.decenturion.mobile.network.request.PaymentModel;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.delivery.skip.SkipDeliveryResult;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResponse;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PhisicalPassportModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class PhisicalPassportInteractor implements IPhisicalPassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public PhisicalPassportInteractor(IResidentClient IResidentClient,
                                      IResidentRepository iResidentRepository,
                                      IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<PassportModelView> getProfileInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                PassportModelView::new);
    }

    @Override
    public Observable<PhisicalPassportModelView> getPhisicalPassportData() {
        Observable<PhisicalPassportModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new PhisicalPassportModelView(resident, passport, photo);
                })
                .onErrorReturn(throwable -> null);

        Observable<PhisicalPassportModelView> obs2 = Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                PhisicalPassportModelView::new);

        return Observable.concat(obs1, obs2)
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<DeliveryPassportResult> setDeliveryData(@NonNull String phone,
                                                              @NonNull String address,
                                                              @NonNull String city,
                                                              @NonNull String state,
                                                              @NonNull String country,
                                                              @NonNull String zipcode) {
        DeliveryPassportModel model = new DeliveryPassportModel();
        model.setPhone(phone);
        model.setAddress(address);
        model.setCity(city);
        model.setState(state);
        model.setCountry(country);
        model.setZip(zipcode);

        return Observable.just(model)
                .map(this::isValidModel)
                .flatMap(m -> mIResidentClient.delivery(m))
                .map(d -> {
                    DeliveryPassportResult result = d.getResult();
                    Resident resident = result.getResident();
                    int step = resident.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return result;
                });
    }

    private DeliveryPassportModel isValidModel(DeliveryPassportModel d) {
        isVaildState(d.getState());
        isVaildAddress(d.getAddress());
        isVaildCity(d.getCity());
        isVaildCountry(d.getCountry());
        isVaildPhone(d.getPhone());
        isVaildZipCode(d.getZip());

        return d;
    }

    private void isVaildState(String state) {
        if (TextUtils.isEmpty(state)) {
            throw new RuntimeException("State is empty");
        }
    }

    private void isVaildAddress(String address) {
        if (TextUtils.isEmpty(address)) {
            throw new RuntimeException("Address is empty");
        }
    }

    private void isVaildCity(String city) {
        if (TextUtils.isEmpty(city)) {
            throw new RuntimeException("City is empty");
        }
    }

    private void isVaildCountry(String country) {
        if (TextUtils.isEmpty(country)) {
            throw new RuntimeException("Country is empty");
        }
    }

    private void isVaildPhone(String phone) {
        if (TextUtils.isEmpty(phone)) {
            throw new RuntimeException("Phone is empty");
        }
    }

    private void isVaildZipCode(String zipcode) {
        if (TextUtils.isEmpty(zipcode)) {
            throw new RuntimeException("ZipCode is empty");
        }
    }

    @Override
    public Observable<PhisicalPassportModelView> genPaymentAddress(@NonNull String coin) {
        Observable<DeliverPaymentResult> obs1 = Observable.just(new PaymentModel(coin))
                .flatMap(d -> mIResidentClient.paymentDeliver(d))
                .map(DeliverPaymentResponse::getResult);

        Observable<Resident> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(ResidentResult::getResident)
                .doOnNext(d -> {
                    int step = d.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);
                });

        if (TextUtils.equals(coin, "dcnt")) {
            return obs2.map(PhisicalPassportModelView::new);
        } else {
            return obs2.flatMap(d -> {
                if (d.isActive()) {
                    return Observable.just(new PhisicalPassportModelView(d));
                } else {
                    return Observable.zip(obs1, Observable.just(d), PhisicalPassportModelView::new);
                }
            });
        }
    }

    @Override
    public Observable<SkipDeliveryResult> skipDelivery() {
        return mIResidentClient.skipDelivery()
                .map(d -> {
                    SkipDeliveryResult result = d.getResult();

                    Resident resident = result.getResident();
                    int step = resident.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    return result;
                });
    }
}
