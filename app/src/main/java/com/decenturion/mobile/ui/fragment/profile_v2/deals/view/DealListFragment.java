package com.decenturion.mobile.ui.fragment.profile_v2.deals.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.traids.TraidsComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.TraidsAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealModel;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.presenter.IDealListPresenter;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class DealListFragment extends BaseFragment implements IDealListView,
        TraidsAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.dealListView)
    BaseRecyclerView mTraidListView;
    private TraidsAdapter mTraidsAdapter;

    /* DI */

    @Inject
    IDealListPresenter mIDealListPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TraidsComponent traidsComponent = appDelegate
                .getIDIManager()
                .plusTraidsComponent();
        traidsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_deal_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mRefreshView.setOnRefreshListener(this);
        mTraidListView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mTraidListView.setLayoutManager(new LinearLayoutManager(context));
        mTraidsAdapter = new TraidsAdapter(context, this);
        mTraidListView.setAdapter(mTraidsAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDealListPresenter.bindView(this);
        mIDealListPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDealListPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDealListPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull DealListModelView model) {
        mTraidsAdapter.replaceDataSet(model.getTokentModelList());
    }

    @Override
    public void onSignout() {
//        FragmentActivity activity = requireActivity();
//        Intent intent = new Intent(activity, SignActivity.class);
//        startActivity(intent);
//        activity.finish();
    }

    @Override
    public void onItemClick(DealModel model) {
//        Intent intent = new Intent(requireActivity(), TraidActivity.class);
//        intent.putExtra(TraidActivity.TRAID_UUID, model.getTraidId());
//        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIDealListPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mIDealListPresenter.bindViewData();
    }

    @Override
    public void onBottomScrolled() {
        mIDealListPresenter.onBottomScrolled();
    }
}
