package com.decenturion.mobile.ui.fragment.pass.edit.model;

public class Country {

    private String mName;
    private String mCode;
    private String mCountryCode;

    public Country(String name, String code, String countryCode) {
        mName = name;
        mCode = code;
        mCountryCode = countryCode;
    }

    public String getName() {
        return mName;
    }

    public String getCode() {
        return mCode;
    }

    public String getCountryCode() {
        return mCountryCode;
    }
}
