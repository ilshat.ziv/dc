package com.decenturion.mobile.di.dagger.tokendetails.buy;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.tokendetails.buy.BuyTokenInteractor;
import com.decenturion.mobile.business.tokendetails.buy.IBuyTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.traide.TraideRepository;
import com.decenturion.mobile.ui.fragment.token.buy.presenter.BuyTokenPresenter;
import com.decenturion.mobile.ui.fragment.token.buy.presenter.IBuyTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BuyTokenModule {

    @Provides
    @BuyTokenScope
    ITraideRepository provideITraideRepository(OrmHelper ormHelper) {
        return new TraideRepository(ormHelper);
    }

    @Provides
    @BuyTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @BuyTokenScope
    IBuyTokenInteractor provideIBuyTokenInteractor(IResidentClient IResidentClient,
                                                   ITokenClient iTokenClient,
                                                   ITokenRepository ITokenRepository,
                                                   IPrefsManager iPrefsManager,
                                                   ITraideRepository iTraideRepository,
                                                   ILocalistionManager iLocalistionManager) {

        return new BuyTokenInteractor(
                IResidentClient,
                iTokenClient,
                ITokenRepository,
                iPrefsManager,
                iTraideRepository,
                iLocalistionManager
        );
    }

    @Provides
    @BuyTokenScope
    IBuyTokenPresenter provideIBuyTokenPresenter(IBuyTokenInteractor iBuyTokenInteractor) {
        return new BuyTokenPresenter(iBuyTokenInteractor);
    }
}
