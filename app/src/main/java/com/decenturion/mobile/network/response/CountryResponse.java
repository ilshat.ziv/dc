package com.decenturion.mobile.network.response;

import com.decenturion.mobile.network.response.model.Country;

import java.util.ArrayList;

public class CountryResponse {

    private ArrayList<Country> mCountryList;

    public CountryResponse() {
    }

    public ArrayList<Country> getCountryList() {
        return mCountryList;
    }
}
