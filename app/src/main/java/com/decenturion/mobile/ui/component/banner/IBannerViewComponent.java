package com.decenturion.mobile.ui.component.banner;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.component.Component;

public interface IBannerViewComponent extends Component {

    void setTitle(@NonNull String title);

    void setReferralPrice(@NonNull String referralPrice);

    void setBasePrice(@NonNull String basePrice);
}
