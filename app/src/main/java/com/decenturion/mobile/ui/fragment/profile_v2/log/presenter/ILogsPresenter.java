package com.decenturion.mobile.ui.fragment.profile_v2.log.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.log.view.ILogsView;

public interface ILogsPresenter extends IPresenter<ILogsView> {

    void bindViewData();

    void onBottomScrolled();

    void killAllSessions();
}
