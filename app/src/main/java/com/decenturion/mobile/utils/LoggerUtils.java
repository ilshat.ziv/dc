package com.decenturion.mobile.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.NotNetConnectionException;

import timber.log.Timber;

public final class LoggerUtils {

    public static void exception(@NonNull Throwable throwable) {
        Timber.e(throwable);
        if (!BuildConfig.DEBUG) {
            if (throwable instanceof NotNetConnectionException
                || throwable instanceof AccessException && TextUtils.equals(throwable.getMessage(), "Not authorized")) {
                return;
            }

            Crashlytics.logException(throwable);
        }
    }

    public static void debug(@Nullable Object o) {
        Timber.d(String.valueOf(o));
    }
}