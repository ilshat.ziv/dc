package com.decenturion.mobile.business.score;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;

import rx.Observable;

public interface IScoreTokenListInteractor {

    Observable<ScoreTokenListModelView> getTokenListModelView(@NonNull ScoreTokenListModelView model);
}
