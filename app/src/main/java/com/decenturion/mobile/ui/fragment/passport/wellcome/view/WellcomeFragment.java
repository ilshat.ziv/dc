package com.decenturion.mobile.ui.fragment.passport.wellcome.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.passport.welcome.WellcomeComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.passport.wellcome.model.WellcomePassportModelView;
import com.decenturion.mobile.ui.fragment.passport.wellcome.presenter.IWellcomePresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class WellcomeFragment extends BaseFragment implements IWellcomePassportView {

    @BindView(R.id.textView)
    LocalizedTextView mTextView;

    @BindView(R.id.wellcomeSignatureView)
    TextView mWellcomeSignatureView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IWellcomePresenter mIWellcomePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        WellcomeComponent wellcomeComponent = appDelegate
                .getIDIManager()
                .plusWellcomeComponent();
        wellcomeComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_wellcome, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        String localeString = mILocalistionManager.getLocaleString(R.string.app_welcome_text);
        mTextView.setText(localeString);

        localeString = mILocalistionManager.getLocaleString(R.string.app_welcome_signature);
        Spanned text = Html.fromHtml(localeString);
        mWellcomeSignatureView.setText(text);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_welcome_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIWellcomePresenter.bindView(this);
        mIWellcomePresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIWellcomePresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIWellcomePresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIWellcomePresenter.unbindView();
    }

    @Override
    public void onBindViewData(WellcomePassportModelView model) {
        String text = mILocalistionManager.getLocaleString(R.string.app_welcome_text);
        text = text.replace("{firstname}", model.getFirstname());
        mTextView.setText(text);
    }

    @Override
    public void onSkipedWellcome() {
        FragmentActivity activity = getActivity();
        assert activity != null;
        Intent intent = new Intent(activity, MainActivity.class);
        startActivity(intent);
        activity.finish();
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        mIWellcomePresenter.skipWellcomeMessage();
    }

    @OnClick(R.id.shareButtonView)
    public void onShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, BuildConfig.HOST);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, mILocalistionManager.getLocaleString(R.string.app_welcome_share)));
    }
}
