package com.decenturion.mobile.business.score.trade;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.request.score.send.SendToMainModel;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.TransferGas;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.score.gas.TransferGasResponse;
import com.decenturion.mobile.network.response.send.SendTokenResponse;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendBalanceModel;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendOptionModel;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendTokenModelView;
import com.decenturion.mobile.utils.DateTimeUtils;

import rx.Observable;

public class TradeScoreSendTokenInteractor implements ITradeScoreSendTokenInteractor {

    private IResidentClient mIResidentClient;
    private IScoreClient mIScoreClient;
    private ITokenRepository mITokenRepository;

    public TradeScoreSendTokenInteractor(IResidentClient IResidentClient,
                                         IScoreClient iScoreClient,
                                         ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mIScoreClient = iScoreClient;
        mITokenRepository = iTokenRepository;
    }

//    @Override
//    public Observable<TradeScoreSendTokenModelView> getCoinInfo(@NonNull TradeScoreSendTokenModelView model) {
//        Observable<OrmTokenModel> obs1 = Observable.just(model.getTokenId())
//                .map(d -> mITokenRepository.getTokenById(d));
//
//        Observable<TradeScoreSendTokenModelView> obs2 = Observable.just(model);
//
//        return Observable.zip(obs1, obs2, (ormTokenModel, sendTokenModelView) -> {
//            sendTokenModelView.setCoinCategory(ormTokenModel.getCategory());
//            CoinTypeModel coinTypeModel = ormTokenModel.getCoinTypeModel();
//            sendTokenModelView.setCoinUUID(coinTypeModel.getUuid());
//            sendTokenModelView.setSymbol(coinTypeModel.getSymbol());
//            return sendTokenModelView;
//        });
//    }

    @Override
    public Observable<TradeScoreSendTokenModelView> getCoinInfo(@NonNull TradeScoreSendTokenModelView model) {
        Observable<OrmTokenModel> obs1 = Observable.just(model.getTokenId())
                .map(d -> mITokenRepository.getTokenById(d));

        Observable<TradeScoreSendTokenModelView> obs2 = Observable.just(model);

        Observable<TransferGas> obs3 = mIScoreClient.getTransferGas()
                .map(TransferGasResponse::getResult);

        Observable<TradeScoreSendBalanceModel> obs4 = getBalance();

        return Observable.zip(obs1, obs2, obs3, obs4,
                (ormTokenModel, sendTokenModelView, transferGas, balance) -> {
                    sendTokenModelView.setCoinCategory(ormTokenModel.getCategory());
                    sendTokenModelView.setTokenName(ormTokenModel.getFullSymbolName());
                    CoinTypeModel coinTypeModel = ormTokenModel.getCoinTypeModel();
                    sendTokenModelView.setCoinUUID(coinTypeModel.getUuid());
                    sendTokenModelView.setSymbol(coinTypeModel.getSymbol());
                    sendTokenModelView.setTokenName(ormTokenModel.getFullSymbolName());

                    sendTokenModelView.setTradeScoreSendBalanceModel(balance);

                    String freeCooldown = ormTokenModel.getFreeCooldown();
                    long datetime = DateTimeUtils.getDatetime(freeCooldown, DateTimeUtils.Format.FORMAT_UTC);
                    TradeScoreSendOptionModel option = new TradeScoreSendOptionModel(datetime, transferGas);
                    sendTokenModelView.setTradeScoreSendOptionModel(option);

                    return sendTokenModelView;
                });
    }

    private Observable<TradeScoreSendBalanceModel> getBalance() {
        return mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    double balance = Double.parseDouble(d.getEthBalance());
                    return new TradeScoreSendBalanceModel(d.getPassportCoin(), balance, resident.getWalletNum());
                });
    }

    @Override
    public Observable<SendTokenResult> sendToMain(@NonNull SendToMainModel model) {
        return mIScoreClient.sendToMain(model)
                .map(SendTokenResponse::getResult);
    }
}
