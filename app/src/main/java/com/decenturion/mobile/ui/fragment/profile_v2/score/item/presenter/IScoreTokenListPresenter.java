package com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.view.IScoreTokenListView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;

public interface IScoreTokenListPresenter extends IPresenter<IScoreTokenListView> {

    void bindViewData(ScoreType type);
}
