package com.decenturion.mobile.ui.fragment.seller.trade.model;

import java.util.List;

public class SellerTraideModelView {

    private List<SellerTraideModel> mTokentModelList;

    public SellerTraideModelView() {
    }

    public SellerTraideModelView(List<SellerTraideModel> d) {
        mTokentModelList = d;
    }

    public List<SellerTraideModel> getTokentModelList() {
        return mTokentModelList;
    }
}
