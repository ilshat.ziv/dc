package com.decenturion.mobile.network.response;

import com.decenturion.mobile.network.response.model.Token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokensResponse extends BaseResponse {

    @JsonProperty("result")
    private ArrayList<Token> mResult;

    public TokensResponse() {
    }

    @JsonIgnore
    public ArrayList<Token> getResult() {
        return mResult;
    }
}
