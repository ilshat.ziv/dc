package com.decenturion.mobile.ui.fragment.pass.restore.presenter;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.restore.RestorePassResponse;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.fragment.pass.restore.model.RestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.restore.view.IRestorePassView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RestorePassPresenter extends Presenter<IRestorePassView> implements IRestorePassPresenter {

    private IRestorePassInteractor mIRestorePassInteractor;
    private RestorePassModelView mRestorePassModelView;

    public RestorePassPresenter(IRestorePassInteractor iSigninInteractor) {
        mIRestorePassInteractor = iSigninInteractor;

        mRestorePassModelView = new RestorePassModelView("");
    }

    @Override
    public void bindViewData() {
        Observable<RestorePassModelView> obs = Observable.just(mRestorePassModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<RestorePassModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(RestorePassModelView model) {
        mRestorePassModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void restorePass(@NonNull String data, String captcha) {
        mRestorePassModelView = new RestorePassModelView(data);

        showProgressView();

        Observable<RestorePassResponse> obs = mIRestorePassInteractor.restorePass(data, captcha)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<RestorePassResponse>) this::restorePassSuccess,
                this::restorePassFailure
        );
    }

    private void restorePassSuccess(RestorePassResponse result) {
        hideProgressView();

        if (mView != null) {
            mView.onRestorePassSuccess();
        }
    }

    private void restorePassFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                String key = error.getKey();
                switch (key) {
                    case "recaptcha" : {
                        reCaptcha();
                        return;
                    }
                }
                showErrorView(error.getMessage());
                return;
            }
        }
        showErrorView(throwable);
    }

    private void reCaptcha() {
        if (mView == null) {
            return;
        }

        IScreenActivityView iScreenActivityView = mView.getIScreenActivityView();
        Activity activity = (Activity) iScreenActivityView;
        SafetyNet.getClient(activity)
                .verifyWithRecaptcha(BuildConfig.CAPTCHA_ANDROID_KEY)
                .addOnSuccessListener(activity,
                        response -> {
                            String tokenResult = response.getTokenResult();
                            restorePass(
                                    mRestorePassModelView.getContact(),
                                    tokenResult
                            );
                        })
                .addOnFailureListener(activity, e -> {
                    if (e instanceof ApiException) {
                        ApiException apiException = (ApiException) e;
                        int statusCode = apiException.getStatusCode();
                        String statusCodeString = CommonStatusCodes.getStatusCodeString(statusCode);

                        LoggerUtils.exception(new Exception("Error: ApiException status code" + statusCodeString));
                    } else {
                        LoggerUtils.exception(new Exception("Error: " + e.getMessage()));
                    }

                    if (mView != null) {
                        showErrorView("Google authorization error, please try again");
                    }
                });
    }
}
