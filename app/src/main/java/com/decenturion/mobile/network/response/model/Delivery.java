package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Delivery extends BaseResponse {

    @JsonProperty("phone")
    private String mPhone;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("country")
    private String mCountry;

    @JsonProperty("city")
    private String mCity;

    @JsonProperty("state")
    private String mState;

    @JsonProperty("zip")
    private String mZip;

    public Delivery() {
    }

    @JsonIgnore
    public String getPhone() {
        return mPhone;
    }

    @JsonIgnore
    public String getAddress() {
        return mAddress;
    }

    @JsonIgnore
    public String getCountry() {
        return mCountry;
    }

    @JsonIgnore
    public String getCity() {
        return mCity;
    }

    @JsonIgnore
    public String getState() {
        return mState;
    }

    @JsonIgnore
    public String getZip() {
        return mZip;
    }
}
