package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class SimpleTextView extends UBaseView {

    @Nullable
    @BindView(R.id.valueView)
    TextView mValueView;

    public SimpleTextView(Context context) {
        super(context);
    }

    public SimpleTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SimpleTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/Canada Type - VoxRound-Light.otf");
            assert mValueView != null;
            mValueView.setTypeface(typeface);
        }
    }

    public void setHint(String hint) {
        init();
        assert mValueView != null;
        mValueView.setHint(String.valueOf(hint));
    }

    @Override
    public void hideLabel() {
        init();
        assert mLabelView != null;
        mLabelView.setVisibility(GONE);
    }

    public void setText(String text) {
        init();
        assert mValueView != null;
        mValueView.setText(text != null ? String.valueOf(text) : "");
    }

    public String getText() {
        init();
        assert mValueView != null;
        return mValueView.getText().toString();
    }

    @Override
    public void setMustFill(boolean isMust) {

    }
}