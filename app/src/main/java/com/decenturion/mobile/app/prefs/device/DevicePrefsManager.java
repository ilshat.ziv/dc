package com.decenturion.mobile.app.prefs.device;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.BasePrefsManager;

import java.util.concurrent.TimeUnit;

public class DevicePrefsManager extends BasePrefsManager implements IDevicePrefsManager {

    private static final String SHARED_PREFERENCES_NAME = "DEVICE_PREFS";

    public DevicePrefsManager(@NonNull Context context) {
        super(context, SHARED_PREFERENCES_NAME);
    }

    @Override
    public  boolean isSyncDatesValid() {
        long remoteServerTime = mSharedPreferences.getLong(DevicePrefsOptions.Keys.REMOTE_SERVER_TIME, 0);
        long localTime = mSharedPreferences.getLong(DevicePrefsOptions.Keys.LOCAL_TIME, 0);
        return !(remoteServerTime == 0 || TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis() - localTime) >= 24);
    }

    @Override
    public void saveSyncDates(final long remoteServerTime, final long localTime) {
        mSharedPreferences.edit().putLong(DevicePrefsOptions.Keys.REMOTE_SERVER_TIME, remoteServerTime)
                .putLong(DevicePrefsOptions.Keys.LOCAL_TIME, localTime)
                .apply();
    }

    @Override
    public void unsetSyncDates() {
        mSharedPreferences.edit().putLong(DevicePrefsOptions.Keys.REMOTE_SERVER_TIME, 0)
                .putLong(DevicePrefsOptions.Keys.LOCAL_TIME, 0)
                .apply();
    }

    @Override
    public long getSyncDates() {
        long remoteServerTime = mSharedPreferences.getLong(DevicePrefsOptions.Keys.REMOTE_SERVER_TIME, 0);
        long localTime = mSharedPreferences.getLong(DevicePrefsOptions.Keys.LOCAL_TIME, 0);
        long difLocalTime = System.currentTimeMillis() - localTime;
        if (remoteServerTime != 0 && TimeUnit.MILLISECONDS.toHours(difLocalTime) <= 24) {
            return remoteServerTime + difLocalTime;
        }
        return 0;
    }

}
