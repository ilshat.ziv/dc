package com.decenturion.mobile.network.response.traide.state;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StateTraideResponse extends BaseResponse {

    @JsonProperty("result")
    private StateTraideResult mResult;

    public StateTraideResponse() {
    }

    @JsonIgnore
    public StateTraideResult getResult() {
        return mResult;
    }
}
