package com.decenturion.mobile.di.dagger.seller.token;

import com.decenturion.mobile.ui.fragment.seller.token.view.SellerTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SellerTokenModule.class
})

@SellerTokenScope
public interface SellerTokenComponent {

    void inject(SellerTokenFragment view);
}
