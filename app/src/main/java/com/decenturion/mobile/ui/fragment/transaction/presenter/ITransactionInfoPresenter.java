package com.decenturion.mobile.ui.fragment.transaction.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.transaction.view.ITransactionInfoView;

public interface ITransactionInfoPresenter extends IPresenter<ITransactionInfoView> {

    void bindViewData(@NonNull String traidUUID);
}
