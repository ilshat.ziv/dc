package com.decenturion.mobile.di.dagger.tokendetails.transaction;

import com.decenturion.mobile.ui.dialog.token.DeleteDealDialog;
import com.decenturion.mobile.ui.fragment.token.transaction.view.TransactionFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TransactionModule.class
})

@TransactionScope
public interface TransactionComponent {

    void inject(TransactionFragment view);

    void inject(DeleteDealDialog view);
}
