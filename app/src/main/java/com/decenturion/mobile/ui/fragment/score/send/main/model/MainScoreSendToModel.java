package com.decenturion.mobile.ui.fragment.score.send.main.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class MainScoreSendToModel implements ISpinnerItem {

    private String mTo;
    private String mKey;

    public MainScoreSendToModel(@NonNull String to, @NonNull String key) {
        mTo = to;
        mKey = key;
    }

    public MainScoreSendToModel(@NonNull String to, @NonNull Key key) {
        this(to, key.name());
    }

    @Override
    public String getItemName() {
        return mTo;
    }

    @Override
    public String getItemKey() {
        return mKey;
    }

    public enum Key {
        TOCITIZEN,
        TOEXTERNAL,
        TOTRADE,
    }
}
