package com.decenturion.mobile.ui.fragment.settings.password.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.password.model.PasswordSettingsModelView;

public interface IPasswordSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull PasswordSettingsModelView modelView);
}
