package com.decenturion.mobile.network.response.signin;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SigninResponse extends BaseResponse {

    @JsonProperty("result")
    private SigninResult mResult;

    public SigninResponse() {
    }

    @JsonIgnore
    public SigninResult getResult() {
        return mResult;
    }
}
