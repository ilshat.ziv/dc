package com.decenturion.mobile.ui.fragment.referral.main.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.referral.ReferralActivity;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.main.ReferralAdapter;
import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.main.presenter.IReferralPresenter;
import com.decenturion.mobile.ui.fragment.walkthrough.ViewPageIndecator;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ReferralFragment extends BaseFragment implements IReferralView {

    /* UI */

    @BindView(R.id.referralLinkView)
    SimpleTextView mReferralLinkView;

    @BindView(R.id.referralProgrammOptions)
    ViewPager mViewPager;

    @BindView(R.id.viewPagerDotsView)
    LinearLayout mViewPageIndicatorView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IReferralPresenter mIReferralPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ReferralComponent component = appDelegate
                .getIDIManager()
                .plusReferallComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_referral_v2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mReferralLinkView.setLabel(mILocalistionManager.getLocaleString(R.string.app_referral_link_title));
        mReferralLinkView.setHint(mILocalistionManager.getLocaleString(R.string.app_referral_link_placeholder));

        FragmentManager fm = getChildFragmentManager();
        ReferralAdapter adapter = new ReferralAdapter(fm, null);
        mViewPager.setAdapter(adapter);
        new ViewPageIndecator(getContext(), mViewPager, mViewPageIndicatorView);
    }

    private void initToolbar() {
        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_referral_title));
        iToolbarController.setSubTitle("");

        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.GONE);

        FragmentManager supportFragmentManager = getFragmentManager();
        assert supportFragmentManager != null;
        Fragment f = supportFragmentManager.findFragmentByTag("ProfileCollapsAppBarFragment");
        if (f != null) {
            FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIReferralPresenter.bindView(this);
        mIReferralPresenter.bindViewData();
    }

    @Override
    public void onBindViewData(@NonNull ReferralModelView model) {
        mReferralLinkView.setText(model.getReferralLink());

        FragmentManager fm = getChildFragmentManager();
        ReferralAdapter adapter = new ReferralAdapter(fm, model);
        mViewPager.setAdapter(adapter);
        new ViewPageIndecator(getContext(), mViewPager, mViewPageIndicatorView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIReferralPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIReferralPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIReferralPresenter.unbindView();
    }

    @OnClick(R.id.referralLinkComponentView)
    protected void onCopyReferralLink() {
        Context context = getContext();
        assert context != null;
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("ReferallLink", mReferralLinkView.getText());
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }

    @OnClick(R.id.shareButtonView)
    protected void onShareReferralLink() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mReferralLinkView.getText());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    @OnClick(R.id.historyTransactionButtonView)
    protected void onShowHistoryTransaction() {
        Intent intent = new Intent(requireActivity(), ReferralActivity.class);
        intent.putExtra(ReferralActivity.TYPE_SCREEN, ReferralActivity.HISTORY_TRANSACTION);
        startActivity(intent);
    }

    @OnClick(R.id.citizenAcquisitionButtonView)
    protected void onCitizenAcquisition() {
        Intent intent = new Intent(requireActivity(), ReferralActivity.class);
        intent.putExtra(ReferralActivity.TYPE_SCREEN, ReferralActivity.ACQUISITION);
        startActivity(intent);
    }

    @OnClick(R.id.createInviteButtonView)
    protected void onCreateInvite() {
        Intent intent = new Intent(requireActivity(), ReferralActivity.class);
        intent.putExtra(ReferralActivity.TYPE_SCREEN, ReferralActivity.INVITATIONS);
        startActivity(intent);
    }
}
