package com.decenturion.mobile.di.dagger.pass.confirm.phone;

import com.decenturion.mobile.ui.fragment.pass.confirm.phone.view.ConfirmPhoneRestorePassFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ConfirmPhoneRestorePassModule.class
})

@ConfirmPhoneRestorePassScope
public interface ConfirmPhoneRestorePassComponent {

    void inject(ConfirmPhoneRestorePassFragment view);
}
