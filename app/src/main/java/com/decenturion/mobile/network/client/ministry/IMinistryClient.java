package com.decenturion.mobile.network.client.ministry;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.request.PaymentAddressModel;
import com.decenturion.mobile.network.request.PaymentStatusAddressModel;
import com.decenturion.mobile.network.response.ministry.MinistryPaymentAddressResponse;
import com.decenturion.mobile.network.response.ministry.MinistryResponse;
import com.decenturion.mobile.network.response.ministry.MinistryStatusPaymentAddressResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface IMinistryClient {

    @GET(BuildConfig.API_V + "ministry/trade/price")
    Observable<MinistryResponse> getPriceProduct();

    @GET(BuildConfig.API_V + "ministry/trade/price")
    Observable<MinistryResponse> getPriceProduct(@Query("category") String category,
                                          @Query("amount") String amount,
                                          @Query("currency") String currency);

    @POST(BuildConfig.API_V + "ministry/trade/address")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<MinistryPaymentAddressResponse> paymentAddress(@Body PaymentAddressModel model);

    @POST(BuildConfig.API_V + "ministry/status/address")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<MinistryStatusPaymentAddressResponse> statusPaymentAddress(@Body PaymentStatusAddressModel model);
}
