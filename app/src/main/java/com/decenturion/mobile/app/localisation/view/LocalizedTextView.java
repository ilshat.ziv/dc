package com.decenturion.mobile.app.localisation.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.di.IDIManager;
import com.decenturion.mobile.di.dagger.AppComponent;

import javax.inject.Inject;

public class LocalizedTextView extends AppCompatTextView {

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    public LocalizedTextView(Context context) {
        super(context);
        initView(context, null);
    }

    public LocalizedTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public LocalizedTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(@NonNull Context context, @Nullable AttributeSet attrs) {
        if (attrs != null && attrs.getAttributeCount() > 0) {
            injectDI(context);
            initLocaleText(context, attrs);
        }
    }

    @SuppressLint("Recycle")
    public void initLocaleText(@NonNull Context context, @Nullable AttributeSet attrs) {
        int[] set = {android.R.attr.text};
        TypedArray a = context.obtainStyledAttributes(attrs, set);
        int resourceId = a.getResourceId(0, -1);
        if (resourceId == -1) {
            return;
        }

        String string = mILocalistionManager.getLocaleString(resourceId);
        setText(string);
    }

    private void injectDI(@NonNull Context context) {
        Context applicationContext = context.getApplicationContext();
        AppDelegate appDelegate = (AppDelegate) applicationContext;
        IDIManager idiManager = appDelegate.getIDIManager();
        AppComponent appComponent = ((DIManager) idiManager).getAppComponent();
        appComponent.inject(this);
    }
}
