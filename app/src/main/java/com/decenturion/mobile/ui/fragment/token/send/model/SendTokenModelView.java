package com.decenturion.mobile.ui.fragment.token.send.model;

public class SendTokenModelView {

    private int mTokenId;
    private String mCoinUUID;
    private String mCoinCategory;
    private String mDcntWallet;
    private String mAmount;
    private String mTwoFA;
    private String mSymbol;

    public SendTokenModelView() {
    }

    public SendTokenModelView(int tokenId) {
        mTokenId = tokenId;
    }

    public int getTokenId() {
        return mTokenId;
    }

    public String getCoinUUID() {
        return mCoinUUID;
    }

    public String getCoinCategory() {
        return mCoinCategory;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getTwoFA() {
        return mTwoFA;
    }

    public String getDcntWallet() {
        return mDcntWallet;
    }

    public void setCoinUUID(String coinUUID) {
        mCoinUUID = coinUUID;
    }

    public void setCoinCategory(String coinCategory) {
        mCoinCategory = coinCategory;
    }

    public void setTwoFA(String twoFA) {
        mTwoFA = twoFA;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public void setDcntWallet(String dcntWallet) {
        mDcntWallet = dcntWallet;
    }

    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    public String getSymbol() {
        return mSymbol;
    }
}
