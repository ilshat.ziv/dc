package com.decenturion.mobile.di.dagger.referral.invite.create;

import com.decenturion.mobile.ui.fragment.referral.invite.create.view.CreateInviteFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        CreateInviteModule.class
})

@CreateInviteScope
public interface CreateInviteComponent {

    void inject(CreateInviteFragment view);
}
