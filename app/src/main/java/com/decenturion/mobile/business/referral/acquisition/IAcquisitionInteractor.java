package com.decenturion.mobile.business.referral.acquisition;

import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;
import com.decenturion.mobile.ui.fragment.referral.acquisition.model.AcquisitionModelView;

import rx.Observable;

public interface IAcquisitionInteractor {

    Observable<AcquisitionModelView> getReferralInfo();

    Observable<ReferralSettingsResult> setPrice(String type,
                                                String price, int amount, String bannerText);
}
