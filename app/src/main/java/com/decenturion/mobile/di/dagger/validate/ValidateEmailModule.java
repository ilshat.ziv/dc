package com.decenturion.mobile.di.dagger.validate;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.validate.IValidEmailInteractor;
import com.decenturion.mobile.business.validate.ValidEmailInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.validate.presenter.IValidEmailPresenter;
import com.decenturion.mobile.ui.fragment.validate.presenter.ValidEmailPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ValidateEmailModule {

    @Provides
    @ValidateEmailScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ValidateEmailScope
    IValidEmailInteractor provideIValidEmailInteractor(IResidentClient iResidentClient,
                                                       IResidentRepository iResidentRepository,
                                                       IPrefsManager iPrefsManager) {
        return new ValidEmailInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @ValidateEmailScope
    IValidEmailPresenter provideIValidEmailPresenter(IValidEmailInteractor iValidEmailInteractor) {
        return new ValidEmailPresenter(iValidEmailInteractor);
    }
}
