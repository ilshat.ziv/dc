package com.decenturion.mobile.business.edit;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.ui.fragment.pass.edit.model.EditProfileModelView;

import rx.Observable;

public interface IEditProfileInteractor {

    Observable<EditProfileModelView> getProfileInfo();

    Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName,
                                                   @NonNull String secondName,
                                                   long birth,
                                                   @NonNull String sex,
                                                   @NonNull String country,
                                                   @NonNull String city);

    Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap);
}
