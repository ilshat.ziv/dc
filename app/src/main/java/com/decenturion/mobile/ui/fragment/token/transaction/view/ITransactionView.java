package com.decenturion.mobile.ui.fragment.token.transaction.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;

public interface ITransactionView extends IScreenFragmentView {

    void onBindViewData(@NonNull TransactionModelView model);

    void onTransactionConfirmed();

    void onErrorTransaction();

    void onTransactionCanceled();
}
