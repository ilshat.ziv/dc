package com.decenturion.mobile.ui.fragment.seller.main.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.seller.main.model.SellerProfileModelView;

public interface ISellerProfileView extends IScreenFragmentView {

    void onBindViewData(@NonNull SellerProfileModelView model);

    void onShare(@NonNull String url);
}
