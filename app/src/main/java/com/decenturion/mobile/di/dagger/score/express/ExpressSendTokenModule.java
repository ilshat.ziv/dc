package com.decenturion.mobile.di.dagger.score.express;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.score.express.ExpressSendTokenInteractor;
import com.decenturion.mobile.business.score.express.IExpressSendTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.express.presenter.ExpressSendTokenPresenter;
import com.decenturion.mobile.ui.fragment.score.send.express.presenter.IExpressSendTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ExpressSendTokenModule {

    @Provides
    @ExpressSendTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @ExpressSendTokenScope
    IExpressSendTokenInteractor provideIExpressSendTokenInteractor(IResidentClient iResidentClient,
                                                                     ITokenRepository iTokenRepository) {
        return new ExpressSendTokenInteractor(iResidentClient, iTokenRepository);
    }

    @Provides
    @ExpressSendTokenScope
    IExpressSendTokenPresenter provideIExpressSendTokenPresenter(
            IExpressSendTokenInteractor iExpressSendTokenInteractor,
            IPrefsManager iPrefsManager,
            ILocalistionManager iLocalistionManager) {

        return new ExpressSendTokenPresenter(iExpressSendTokenInteractor, iPrefsManager, iLocalistionManager);
    }
}
