package com.decenturion.mobile.di.dagger.score.token;

import com.decenturion.mobile.ui.fragment.score.details.view.ScoreTokenDetailsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ScoreTokenDetailsModule.class
})

@ScoreTokenDetailsScope
public interface ScoreTokenDetailsComponent {

    void inject(ScoreTokenDetailsFragment view);
}
