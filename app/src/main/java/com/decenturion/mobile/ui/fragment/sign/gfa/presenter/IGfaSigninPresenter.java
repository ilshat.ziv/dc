package com.decenturion.mobile.ui.fragment.sign.gfa.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.sign.gfa.view.IGfaSigninView;

public interface IGfaSigninPresenter extends IPresenter<IGfaSigninView> {

    void gfaSignin(@NonNull String gfaCode);

    void bindViewData();
}
