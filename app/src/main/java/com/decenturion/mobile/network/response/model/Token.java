package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token {

    @JsonProperty("ask_price")
    private String mAskPrice;

    @JsonProperty("bid_price")
    private String mBidPrice;

    @JsonProperty("balance")
    private String mBalance;

    @JsonProperty("category")
    private String mCategroy;

    @JsonProperty("coin")
    private TokenCoin mTokenCoin;

    @JsonProperty("startup")
    private Startup mStartup;

    @JsonProperty("full_symbol")
    private String mFullSymbol;

    //2018-12-13 06:30:52 +0000 +0000
    //0001-01-01 00:00:00 +0000 UTC
    @JsonProperty("free_cooldown")
    private String mFreeCooldown;

    public Token() {
    }

    @JsonIgnore
    public String getAskPrice() {
        return mAskPrice;
    }

    @JsonIgnore
    public String getBidPrice() {
        return mBidPrice;
    }

    @JsonIgnore
    public String getBalance() {
        return mBalance;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategroy;
    }

    @JsonIgnore
    public TokenCoin getTokenCoin() {
        return mTokenCoin;
    }

    @JsonIgnore
    public Startup getStartup() {
        return mStartup;
    }

    @JsonIgnore
    public String getFullSymbol() {
        return mFullSymbol;
    }

    @JsonIgnore
    public String getFreeCooldown() {
        return mFreeCooldown;
    }
}
