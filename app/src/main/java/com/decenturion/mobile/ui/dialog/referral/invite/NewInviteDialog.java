package com.decenturion.mobile.ui.dialog.referral.invite;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewInviteDialog extends SingleDialogFragment implements INewInviteView {

    public static final String INVITE_URL = "INVITE_URL";
    private String mInviteURl;

    /* UI */

    @BindView(R.id.referralLinkView)
    SimpleTextView mReferralLinkView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mInviteURl = arguments.getString(INVITE_URL, null);
        }
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        ReferralComponent component = appDelegate
                .getIDIManager()
                .plusReferallComponent();
        component.inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_referral_invite_create_title));
        setNegativeTextString(mILocalistionManager.getLocaleString(R.string.app_constants_close));

        MaterialDialog dialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_referall_invite_create);

        ButterKnife.bind(this, dialog.getView());
        setVisibilityPositiveClickButton(false);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        setupUi();

        return dialog;
    }

    private void setupUi() {
        mReferralLinkView.hideLabel();
        mReferralLinkView.setText(mInviteURl);
        mReferralLinkView.setOnClickListener(v -> onCopyReferral());
    }

    @OnClick(R.id.referralLinkComponentView)
    protected void onCopyReferral() {
        Context context = getContext();
        assert context != null;
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("INVITE_URL", mInviteURl);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
        }
    }

}
