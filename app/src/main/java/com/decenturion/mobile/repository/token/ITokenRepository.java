package com.decenturion.mobile.repository.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.response.model.Token;

import java.util.List;

public interface ITokenRepository {

    List<OrmTokenModel> getTokens();

    OrmTokenModel getTokenById(int tokenId);

    OrmTokenModel getTokenInfoById(int tokenId);

    void saveTokenInfo(@NonNull Token token) throws Exception;

    void saveTokenInfo(@NonNull OrmTokenModel model) throws Exception;
}
