package com.decenturion.mobile.business.support.model;

public class SupportMessage {

    private String mTheme;
    private String mMessage;

    public SupportMessage(String theme, String message) {
        mTheme = theme;
        mMessage = message;
    }

    public String getTheme() {
        return mTheme;
    }

    public String getMessage() {
        return mMessage;
    }
}
