package com.decenturion.mobile.di.dagger.header;

import com.decenturion.mobile.ui.fragment.header.view.HeaderFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        HeaderModule.class
})

@HeaderScope
public interface HeaderComponent {

    void inject(HeaderFragment view);
}
