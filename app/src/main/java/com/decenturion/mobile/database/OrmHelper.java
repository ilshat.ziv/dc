package com.decenturion.mobile.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.model.OrmTraideModel;
import com.decenturion.mobile.database.model.OrmWalletModel;
import com.decenturion.mobile.database.type.DeliveryType;
import com.decenturion.mobile.database.type.TransferType;
import com.decenturion.mobile.utils.LoggerUtils;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.field.DataPersisterManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class OrmHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = Constants.DB_NAME;

    public OrmHelper(@NonNull Context context) {
        super(context, DATABASE_NAME, null, BuildConfig.VERSION_CODE);
    }

    public OrmHelper(@NonNull Context context, @NonNull SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, BuildConfig.VERSION_CODE);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            DataPersisterManager.registerDataPersisters(DeliveryType.getSingleton());
            DataPersisterManager.registerDataPersisters(TransferType.getSingleton());

            TableUtils.createTable(connectionSource, OrmResidentModel.class);
            TableUtils.createTable(connectionSource, OrmPassportModel.class);
            TableUtils.createTable(connectionSource, OrmWalletModel.class);
            TableUtils.createTable(connectionSource, OrmPhotoModel.class);
            TableUtils.createTable(connectionSource, OrmSettingsModel.class);
            TableUtils.createTable(connectionSource, OrmTokenModel.class);
            TableUtils.createTable(connectionSource, OrmTraideModel.class);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            if (oldVersion <= 27) {

                TableUtils.dropTable(connectionSource, OrmResidentModel.class, true);
                TableUtils.dropTable(connectionSource, OrmPassportModel.class, true);
                TableUtils.dropTable(connectionSource, OrmWalletModel.class, true);
                TableUtils.dropTable(connectionSource, OrmPhotoModel.class, true);
                TableUtils.dropTable(connectionSource, OrmSettingsModel.class, true);
                TableUtils.dropTable(connectionSource, OrmTokenModel.class, true);
                TableUtils.dropTable(connectionSource, OrmTraideModel.class, true);

                // recreate entity
                DataPersisterManager.registerDataPersisters(DeliveryType.getSingleton());
                DataPersisterManager.registerDataPersisters(TransferType.getSingleton());

                TableUtils.createTable(connectionSource, OrmResidentModel.class);
                TableUtils.createTable(connectionSource, OrmPassportModel.class);
                TableUtils.createTable(connectionSource, OrmWalletModel.class);
                TableUtils.createTable(connectionSource, OrmPhotoModel.class);
                TableUtils.createTable(connectionSource, OrmSettingsModel.class);
                TableUtils.createTable(connectionSource, OrmTokenModel.class);
                TableUtils.createTable(connectionSource, OrmTraideModel.class);

            } else if ((BuildConfig.FLAVOR.equals("demo") && newVersion >= 77)
                    || (BuildConfig.FLAVOR.equals("live") && newVersion >= 65)) {

                TableUtils.dropTable(connectionSource, OrmResidentModel.class, true);
                TableUtils.createTable(connectionSource, OrmResidentModel.class);

                TableUtils.dropTable(connectionSource, OrmTokenModel.class, true);
                TableUtils.createTable(connectionSource, OrmTokenModel.class);

            } else if ((BuildConfig.FLAVOR.equals("demo") && newVersion >= 69)
                    || (BuildConfig.FLAVOR.equals("live") && newVersion >= 60)) {

                TableUtils.dropTable(connectionSource, OrmResidentModel.class, true);
                TableUtils.createTable(connectionSource, OrmResidentModel.class);
            }
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void close() {
        super.close();
    }

    interface Constants {
        String DB_NAME = "decenturion";
    }
}

