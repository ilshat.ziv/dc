package com.decenturion.mobile.di.dagger.aggrement;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.aggrement.IUserAggrementInteractor;
import com.decenturion.mobile.business.aggrement.UserAggrementInteractor;
import com.decenturion.mobile.ui.fragment.disclaimer.presenter.IUserAggrementPresenter;
import com.decenturion.mobile.ui.fragment.disclaimer.presenter.UserAggrementPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UserAggrementModule {

    @Provides
    @UserAggrementScope
    IUserAggrementInteractor provideIUserAggrementInteractor(Context context,
                                                             ILocalistionManager iLocalistionManager) {
        return new UserAggrementInteractor(context, iLocalistionManager);
    }

    @Provides
    @UserAggrementScope
    IUserAggrementPresenter provideIUserAggrementPresenter(IUserAggrementInteractor iUserAggrementInteractor) {
        return new UserAggrementPresenter(iUserAggrementInteractor);
    }

}
