package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.utils.LoggerUtils;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public abstract class BaseDao<T, ID> extends BaseDaoImpl<T, ID> {

    Class<T> mDataClass;

    protected BaseDao(ConnectionSource connectionSource, Class<T> dataClass) throws Exception {
        super(connectionSource, dataClass);
        mDataClass = dataClass;
    }

    public boolean insertRow(T tableParam) {
        try {
            create(tableParam);
            return true;
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return false;
    }

    public boolean bulkInsert(Collection<T> tableParams) {
        try {
            callBatchTasks(() -> {
                for (T tableParam : tableParams) {
                    insertRow(tableParam);
                }
                return true;
            });
        } catch (SQLException e) {
            LoggerUtils.exception(e);
            return false;
        }
        return true;
    }

    public CreateOrUpdateStatus createOrUpdate2(T data) {
        try {
            return super.createOrUpdate(data);
        } catch (SQLException e) {
            LoggerUtils.exception(e);
            return new CreateOrUpdateStatus(false, false, -1);
        }
    }

    public void insertDataThread(final T data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                insertRow(data);
            }
        }).start();
    }

    public void clear() {
        try {
            TableUtils.clearTable(getConnectionSource(), mDataClass);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public List<T> queryForAll(){
        try {
            return super.queryForAll();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return null;
    }

    @Override
    public int delete(T data) {
        try {
            return super.delete(data);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return 0;
    }

    @Override
    public int delete(Collection<T> data) {
        try {
            return super.delete(data);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return 0;
    }

    public List<T> getFirstRows(int countRows) {
        QueryBuilder<T, ID> queryBuilder = queryBuilder();
        try {
            return queryBuilder
                    .limit(countRows)
                    .query();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return null;
    }

    @Override
    public int update(T data) {
        try {
            return super.update(data);
        } catch (SQLException e) {
            LoggerUtils.exception(e);
        }
        return -1;
    }

    public String getTableName() {
        return getTableConfig().getTableName();
    }
}
