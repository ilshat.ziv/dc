package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class LinkedTextView extends UBaseView implements View.OnClickListener {

    @Nullable
    @BindView(R.id.valueView)
    TextView mValueView;

    public LinkedTextView(Context context) {
        super(context);
    }

    public LinkedTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LinkedTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/Canada Type - VoxRound-Light.otf");
            mValueView.setTypeface(typeface);
        }
    }

    public void setHint(String hint) {
        init();
        mValueView.setHint(String.valueOf(hint));
    }

    public void setText(String text) {
        init();
        mValueView.setText(text != null ? String.valueOf(text) : "");

    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        this.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}