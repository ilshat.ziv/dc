package com.decenturion.mobile.business.passport.activation;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.passport.activation.model.ActivationModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.model.PassportModel;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public interface IActivationPassportInteractor {

    Observable<PassportModelView> getProfileInfo();

    Observable<ActivationModelView> genPaymentAddress(@NonNull String coin);
}
