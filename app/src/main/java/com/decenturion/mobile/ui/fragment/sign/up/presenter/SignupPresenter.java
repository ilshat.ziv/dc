package com.decenturion.mobile.ui.fragment.sign.up.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.business.signup.ISignupInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.signup.SignupResult;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.fragment.sign.up.model.SignupModelView;
import com.decenturion.mobile.ui.fragment.sign.up.view.ISignupView;
import com.decenturion.mobile.network.http.exception.InvalidCaptchaException;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;

import java.util.ArrayList;

import rx.Observable;
import rx.functions.Action1;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SignupPresenter extends Presenter<ISignupView> implements ISignupPresenter {

    private ISignupInteractor mISignupInteractor;

    private SignupModelView mSignupModelView;

    public SignupPresenter(ISignupInteractor ISignupInteractor) {
        mISignupInteractor = ISignupInteractor;
        mSignupModelView = new SignupModelView();

        mSignupModelView.setAgreeTermsOfuse(false);
        mSignupModelView.setAgreeEmailConfirmation(true);
    }

    @Override
    public void bindViewData(@Nullable String inviteToken) {
        showProgressView();

        mSignupModelView.setInviteCode(inviteToken);

        Observable<SignupModelView> obs;

        if (TextUtils.isEmpty(inviteToken)) {
            obs = Observable.just(mSignupModelView)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        } else {
            obs = mISignupInteractor.checkInviteToken(inviteToken)
                    .map(d -> {
                        mSignupModelView.setUsername(d);
                        return mSignupModelView;
                    })
                    .onErrorReturn(throwable -> mSignupModelView)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
        }

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SignupModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

//    @Override
//    public void bindViewData(String inviteToken, String referallKey) {
//        showProgressView();
//
//        mSignupModelView.setInviteCode(inviteToken);
//
//        Observable<SignupModelView> obs;
//
//        if (!TextUtils.isEmpty(referallKey)) {
//            obs = mISignupInteractor.activeReferallCode(referallKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else if (TextUtils.isEmpty(inviteToken)) {
//            obs = Observable.just(mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else {
//                obs = mISignupInteractor.checkInviteToken(inviteToken)
//                        .map(d -> {
//                            mSignupModelView.setUsername(d);
//                            return mSignupModelView;
//                        })
//                        .onErrorReturn(throwable -> mSignupModelView)
//                        .subscribeOn(Schedulers.newThread())
//                        .observeOn(AndroidSchedulers.mainThread());
//        }
//
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<SignupModelView>) this::bindViewDataSuccess,
//                this::bindViewDataFailure
//        );
//    }

//    @Override
//    public void bindViewData(String inviteToken, String referralKey, String payReferralKey) {
//        showProgressView();
//
//        mSignupModelView.setInviteCode(inviteToken);
//
//        Observable<SignupModelView> obs;
//
//        if (!TextUtils.isEmpty(referralKey)) {
//            obs = mISignupInteractor.activeReferallCode(referralKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else if (TextUtils.isEmpty(payReferralKey)) {
//            obs = mISignupInteractor.activePayReferralCode(payReferralKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else if (TextUtils.isEmpty(inviteToken)) {
//            obs = Observable.just(mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else {
//            obs = mISignupInteractor.checkInviteToken(inviteToken)
//                    .map(d -> {
//                        mSignupModelView.setUsername(d);
//                        return mSignupModelView;
//                    })
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        }
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<SignupModelView>) this::bindViewDataSuccess,
//                this::bindViewDataFailure
//        );
//    }

//    @Override
//    public void bindViewData(String inviteToken) {
//        showProgressView();
//
//        mSignupModelView.setInviteCode(inviteToken);
//
//        Observable<SignupModelView> obs;
//
//        if (!TextUtils.isEmpty(referralKey)) {
//            obs = mISignupInteractor.activeReferallCode(referralKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else if (!TextUtils.isEmpty(payReferralKey)) {
//            obs = mISignupInteractor.activePayReferralCode(payReferralKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else if (!TextUtils.isEmpty(bannerReferralKey)) {
//            obs = mISignupInteractor.activeBannerReferralCode(bannerReferralKey)
//                    .map(d -> mSignupModelView)
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else
//        if (TextUtils.isEmpty(inviteToken)) {
//            obs = Observable.just(mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else {
//            obs = mISignupInteractor.checkInviteToken(inviteToken)
//                    .map(d -> {
//                        mSignupModelView.setUsername(d);
//                        return mSignupModelView;
//                    })
//                    .onErrorReturn(throwable -> mSignupModelView)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread());
//        }
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<SignupModelView>) this::bindViewDataSuccess,
//                this::bindViewDataFailure
//        );
//    }

    private void bindViewDataSuccess(SignupModelView model) {
        mSignupModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    @NonNull String username,
                                    @NonNull String password,
                                    @NonNull String retypePassword,
                                    @NonNull String captcha,
                                    boolean isAgreeTerms,
                                    boolean isSubscribe,
                                    @Nullable String inviteCode,
                                    @Nullable String referallKey,
                                    @Nullable String payReferallKey,
                                    @Nullable String bannerReferralKey) {

        bundle.putString("username", username);
        bundle.putString("password", password);
        bundle.putString("retypePassword", retypePassword);
        bundle.putString("captcha", captcha);
        bundle.putBoolean("isAgreeTerms", isAgreeTerms);
        bundle.putBoolean("isSubscribe", isSubscribe);
        bundle.putString("inviteCode", inviteCode);
        bundle.putString("referallKey", referallKey);
        bundle.putString("payReferallKey", payReferallKey);
        bundle.putString("bannerReferralKey", bannerReferralKey);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mSignupModelView.setUsername(savedInstanceState.getString("username", ""));
        mSignupModelView.setPassword(savedInstanceState.getString("password", ""));
        mSignupModelView.setRetypePassword(savedInstanceState.getString("retypePassword", ""));
        mSignupModelView.setSubscribe(savedInstanceState.getBoolean("isSubscribe", false));
        mSignupModelView.setInviteCode(savedInstanceState.getString("inviteCode", ""));
        mSignupModelView.setAgreeEmailConfirmation(savedInstanceState.getBoolean("isSubscribe", false));
        mSignupModelView.setAgreeTermsOfuse(savedInstanceState.getBoolean("isAgreeTerms", false));

        Observable<SignupModelView> obs = Observable.just(mSignupModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SignupModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void saveViewData(@NonNull String value,
                             @NonNull String value1,
                             @NonNull String value2,
                             boolean checked,
                             boolean checked1) {

        mSignupModelView.setUsername(value);
        mSignupModelView.setPassword(value1);
        mSignupModelView.setRetypePassword(value2);
        mSignupModelView.setAgreeTermsOfuse(checked);
        mSignupModelView.setAgreeEmailConfirmation(checked1);
    }

    @Override
    public void signup(@NonNull String username,
                       @NonNull String password,
                       @NonNull String retypePassword,
                       @NonNull String captcha,
                       boolean isAgreeTerms,
                       boolean isSubscribe,
                       @Nullable String inviteCode,
                       String referralCode,
                       String payReferralCode,
                       String bannerReferralCode) {

        mSignupModelView.setUsername(username);
        mSignupModelView.setPassword(password);
        mSignupModelView.setRetypePassword(retypePassword);
        mSignupModelView.setAgreeTermsOfuse(isSubscribe);
        mSignupModelView.setAgreeEmailConfirmation(isSubscribe);
        mSignupModelView.setSubscribe(isSubscribe);
        mSignupModelView.setSource(captcha);
        mSignupModelView.setInviteCode(inviteCode);
        mSignupModelView.setReferralCode(referralCode);
        mSignupModelView.setPayInviteCode(payReferralCode);
        mSignupModelView.setBannerCode(bannerReferralCode);

        showProgressView();

        Observable<SignupResult> obs = mISignupInteractor.signup(
                username,
                password,
                retypePassword,
                captcha,
                isSubscribe,
                inviteCode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SignupResult>) this::signupSuccess,
                this::signupFailure
        );
    }

    private void signupSuccess(SignupResult result) {
        int step = result.getStep();

        if (!TextUtils.isEmpty(mSignupModelView.getReferralCode())) {
            Observable<Boolean> obs = mISignupInteractor.activeReferallCode(mSignupModelView.getReferralCode())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
            mIDCompositeSubscription.subscribe(obs,
                    (Action1<Boolean>) b -> {
                        hideProgressView();

                        if (mView != null) {
                            mView.onSignupSuccess(mSignupModelView, step);
                        }
                    },
                    this::signupFailure
            );
        } else if (!TextUtils.isEmpty(mSignupModelView.getBannerCode())) {
            Observable<Boolean> obs = mISignupInteractor.activeBannerReferralCode(mSignupModelView.getBannerCode())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
            mIDCompositeSubscription.subscribe(obs,
                    (Action1<Boolean>) b -> {
                        hideProgressView();

                        if (mView != null) {
                            mView.onSignupSuccess(mSignupModelView, step);
                        }
                    },
                    this::signupFailure
            );
        } else if (!TextUtils.isEmpty(mSignupModelView.getPayInviteCode())) {
            Observable<Boolean> obs = mISignupInteractor.activePayReferralCode(mSignupModelView.getPayInviteCode())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());
            mIDCompositeSubscription.subscribe(obs,
                    (Action1<Boolean>) b -> {
                        hideProgressView();

                        if (mView != null) {
                            mView.onSignupSuccess(mSignupModelView, step);
                        }
                    },
                    this::signupFailure
            );
        } else {
            hideProgressView();

            if (mView != null) {
                mView.onSignupSuccess(mSignupModelView, step);
            }
        }
    }

    private void signupFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                String key = error.getKey();
                switch (key) {
//                    case "username" : {
//                        if (mView != null) {
//                            mView.onResendEmail(mSignupModelView);
//                        }
//                        break;
//                    }
                    case "recaptcha": {
                        reCaptcha();
                        break;
                    }
                    default: {
                        showErrorView(error.getMessage());
                    }
                }

                return;
            }
        } else if (throwable instanceof InvalidCaptchaException) {
            reCaptcha();

            return;
        }
        showErrorView(throwable);
    }

    private void reCaptcha() {
        if (mView == null) {
            return;
        }

        IScreenActivityView iScreenActivityView = mView.getIScreenActivityView();
        Activity activity = (Activity) iScreenActivityView;
        SafetyNet.getClient(activity)
                .verifyWithRecaptcha(BuildConfig.CAPTCHA_ANDROID_KEY)
                .addOnSuccessListener(activity,
                        response -> {
                            String tokenResult = response.getTokenResult();
                            signup(
                                    mSignupModelView.getUsername(),
                                    mSignupModelView.getPassword(),
                                    mSignupModelView.getRetypePassword(),
                                    tokenResult,
                                    mSignupModelView.isAgreeTermsOfuse(),
                                    mSignupModelView.isSubscribe(),
                                    mSignupModelView.getInviteCode(),
                                    mSignupModelView.getReferralCode(),
                                    mSignupModelView.getPayInviteCode(),
                                    mSignupModelView.getBannerCode()
                            );
                        })
                .addOnFailureListener(activity, e -> {
                    if (e instanceof ApiException) {
                        ApiException apiException = (ApiException) e;
                        int statusCode = apiException.getStatusCode();
                        String statusCodeString = CommonStatusCodes.getStatusCodeString(statusCode);

                        LoggerUtils.exception(new Exception("Error: ApiException status code" + statusCodeString));
                    } else {
                        LoggerUtils.exception(new Exception("Error: " + e.getMessage()));
                    }

                    if (mView != null) {
                        showErrorView("Google authorization error, please try again");
                    }
                });
    }
}
