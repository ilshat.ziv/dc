package com.decenturion.mobile.ui.fragment.pass.reset.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.reset.model.ResetPasswordModelView;

public interface IResetPasswordView extends IScreenFragmentView {

    void onBindViewData(@NonNull ResetPasswordModelView modelView);

    void onResetPasswordSuccess(int step);

    void resendEmail();
}
