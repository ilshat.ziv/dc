package com.decenturion.mobile.di.dagger.header;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.header.HeaderInteractor;
import com.decenturion.mobile.business.header.IHeaderInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.header.presenter.HeaderPresenter;
import com.decenturion.mobile.ui.fragment.header.presenter.IHeaderPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class HeaderModule {

    @Provides
    @HeaderScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @HeaderScope
    IHeaderInteractor provideIHeaderInteractor(IResidentClient iResidentClient,
                                                IResidentRepository iResidentRepository,
                                               ILocalistionManager iLocalistionManager) {
        return new HeaderInteractor(iResidentClient, iResidentRepository, iLocalistionManager);
    }

    @Provides
    @HeaderScope
    IHeaderPresenter provideIHeaderPresenter(IHeaderInteractor iHeaderInteractor) {
        return new HeaderPresenter(iHeaderInteractor);
    }
}
