package com.decenturion.mobile.di.dagger.referral.invite.create;

import com.decenturion.mobile.business.referral.invite.create.CreateInviteInteractor;
import com.decenturion.mobile.business.referral.invite.create.ICreateInviteInteractor;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.ui.fragment.referral.invite.create.presenter.CreateInvitePresenter;
import com.decenturion.mobile.ui.fragment.referral.invite.create.presenter.ICreateInvitePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateInviteModule {

    @Provides
    @CreateInviteScope
    ICreateInviteInteractor provideICreateInviteInteractor(IReferralClient iReferralClient) {
        return new CreateInviteInteractor(iReferralClient);
    }

    @Provides
    @CreateInviteScope
    ICreateInvitePresenter provideICreateInvitePresenter(ICreateInviteInteractor iCreateInviteInteractor) {
        return new CreateInvitePresenter(iCreateInviteInteractor);
    }
}
