package com.decenturion.mobile.business.send;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.CheckDcntWalletModel;
import com.decenturion.mobile.network.request.score.SendTokenModel;
import com.decenturion.mobile.network.response.send.SendTokenResponse;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.network.response.wallets.CheckWalletResponse;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.token.send.model.SendTokenModelView;

import rx.Observable;

public class SendTokenInteractor implements ISendTokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenRepository mITokenRepository;

    public SendTokenInteractor(IResidentClient IResidentClient, ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mITokenRepository = iTokenRepository;
    }

    @Override
    public Observable<SendTokenModelView> getCoinInfo(@NonNull SendTokenModelView model) {
        Observable<OrmTokenModel> obs1 = Observable.just(model.getTokenId())
                .map(d -> mITokenRepository.getTokenById(d));

        Observable<SendTokenModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (ormTokenModel, sendTokenModelView) -> {
            sendTokenModelView.setCoinCategory(ormTokenModel.getCategory());
            CoinTypeModel coinTypeModel = ormTokenModel.getCoinTypeModel();
            sendTokenModelView.setCoinUUID(coinTypeModel.getUuid());
            sendTokenModelView.setSymbol(coinTypeModel.getSymbol());
            return sendTokenModelView;
        });
    }

    @Override
    public Observable<SendTokenResult> sendToken(
            @NonNull String coinUUID,
            @NonNull String coinCategory,
            @NonNull String dcntWallet,
            @NonNull String amount,
            @NonNull String twofa) {

        SendTokenModel model = new SendTokenModel();
        model.setCoin(coinUUID);
        model.setCategory(coinCategory);
        model.setDCNTWallet(dcntWallet);
        model.setAmount(amount);
        model.setTwoFa(twofa);

        CheckDcntWalletModel model1 = new CheckDcntWalletModel(dcntWallet);
        return mIResidentClient.checkDCNTWallet(model1)
                .map(CheckWalletResponse::getResult)
                .map(d -> {
                    if (!d.isPresence()) {
                        throw new RuntimeException("Address is not a valid ERC-20");
                    }

                    return true;
                })
                .flatMap(d -> mIResidentClient.sendToken(model))
                .map(SendTokenResponse::getResult);
    }
}
