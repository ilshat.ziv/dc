package com.decenturion.mobile.ui.activity.passport;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.passport.activation.view.ActivationPassportFragment;
import com.decenturion.mobile.ui.fragment.passport.online.view.OnlinePassportFragment;
import com.decenturion.mobile.ui.fragment.passport.phisical.view.PhisicalPassportFragment;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.WellcomeFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class PassportActivity extends SingleFragmentActivity {

    public static final String TYPE_PASSPORT = "TYPE_PASSPORT";
    public static final int ONLINE_PASSPORT = 1;
    public static final int PHISICAL_PASSPORT = 2;
    public static final int ACTIVATION_PASSPORT = 3;
    public static final int WELLCOME = 4;

    private int mTypeSettings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
            return;
        }

        switch (mTypeSettings) {
            case ONLINE_PASSPORT : {
                iNavigationManager.navigateTo(OnlinePassportFragment.class);
                break;
            }
            case PHISICAL_PASSPORT : {
                iNavigationManager.navigateTo(PhisicalPassportFragment.class);
                break;
            }
            case ACTIVATION_PASSPORT : {
                iNavigationManager.navigateTo(ActivationPassportFragment.class);
                break;
            }
            case WELLCOME : {
                iNavigationManager.navigateTo(WellcomeFragment.class);
                break;
            }
            default : {
                break;
            }
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mTypeSettings = intent.getIntExtra(TYPE_PASSPORT, ONLINE_PASSPORT);
        }
    }
}
