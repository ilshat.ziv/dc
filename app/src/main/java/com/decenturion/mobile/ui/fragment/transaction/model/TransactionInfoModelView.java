package com.decenturion.mobile.ui.fragment.transaction.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTraideModel;
import com.decenturion.mobile.database.type.model.TransferTypeModel;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.Transfer;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.traide.state.StateTraideResult;

public class TransactionInfoModelView {

    private int mTraidId;

    private String mFrontendId;
    private String mCoinId;
    private String mCoin;
    private String mSell;
    private String mBuy;
    private String mCategory;
    private String mDateTraid;

    private String mIdTraide;

    private String mAmountTraid;
    private String mTraidStatus;
    private String mTraidStatusName;
    private String mTransactionStatus;
    private String mSendAddresss;
    private String mReceiveAddress;

    public TransactionInfoModelView(WrapperTrade wrapperTrade) {
        Trade trade = wrapperTrade.getTrade();
        mAmountTraid = trade.getAmount();
        mTraidStatus = trade.getStatus();
        mTraidStatusName = trade.getStatus();
        mDateTraid = trade.getCreatedAt();
        mCoin = trade.getCoin();
        mFrontendId = trade.getFrontendId();
        mIdTraide = trade.getTxid();

        Transfer transfer = wrapperTrade.getTransfer();
        mTransactionStatus = transfer.getStatus();
        mSendAddresss = transfer.getTo();
        mReceiveAddress = trade.getTradeAddress();
    }

    public TransactionInfoModelView(@NonNull OrmTraideModel d) {
        mFrontendId = d.getFrontendId();
        mAmountTraid = d.getAmount();
        mTraidStatus = d.getStatus();
        mTraidStatusName = d.getStatus();
        mDateTraid = d.getExpiredAt();
        mIdTraide = d.getTxid();

        TransferTypeModel transfer = d.getTransfer();
        mTransactionStatus = transfer.getStatus();
        mSendAddresss = transfer.getTo();
        mReceiveAddress = d.getTradeAddress();

        mCoin = d.getCoin();
    }

    public TransactionInfoModelView(StateTraideResult result) {
        mAmountTraid = result.getAmount();
    }

    public String getCoinId() {
        return mCoinId;
    }

    public void setSell(String sell) {
        mSell = sell;
    }

    public void setBuy(String buy) {
        mBuy = buy;
    }

    public String getSell() {
        return mSell;
    }

    public String getBuy() {
        return mBuy;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getDateTraid() {
        return mDateTraid;
    }

    public String getIdTraide() {
        return mIdTraide;
    }

    public String getAmountTraid() {
        return mAmountTraid;
    }

    public String getTraidStatus() {
        return mTraidStatus;
    }

    public String getTraidStatusName() {
        return mTraidStatusName;
    }

    public String getTransactionStatus() {
        return mTransactionStatus;
    }

    public String getSendAddresss() {
        return mSendAddresss;
    }

    public String getreceiveAddress() {
        return mReceiveAddress;
    }

    public void setIdTraide(String idTraide) {
        mIdTraide = idTraide;
    }

    public void setAmountTraid(String amountTraid) {
        mAmountTraid = amountTraid;
    }

    public void setTraidStatus(String traidStatus) {
        mTraidStatus = traidStatus;
    }

    public void setTraidStatusName(String traidStatusName) {
        mTraidStatusName = traidStatusName;
    }

    public void setTransactionStatus(String transactionStatus) {
        mTransactionStatus = transactionStatus;
    }

    public void setSendAddress(String sendAddresss) {
        mSendAddresss = sendAddresss;
    }

    public void setReceiveAddress(String receiveAddress) {
        mReceiveAddress = receiveAddress;
    }

    public void setDateTraid(String dateTraid) {
        mDateTraid = dateTraid;
    }

    public String getFrontendId() {
        return mFrontendId;
    }
}
