package com.decenturion.mobile.ui.activity.splash.presenter;

import com.decenturion.mobile.ui.activity.splash.view.ISplashView;
import com.decenturion.mobile.ui.architecture.presenter.IPresenter;

public interface ISplashPresenter extends IPresenter<ISplashView> {

    void bindViewData();
}
