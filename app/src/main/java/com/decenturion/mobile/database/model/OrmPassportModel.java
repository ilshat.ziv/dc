package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.type.DeliveryType;
import com.decenturion.mobile.database.type.model.DeliverTypeModel;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_passport_model")
public class OrmPassportModel {

    private final static String ID = "id";
    private final static String UUID = "uuid";
    private final static String PHONE = "phone";
    private final static String NAME = "name";
    private final static String FIRSTNAME = "firstname";
    private final static String LASTNAME = "lastname";
    private final static String BIRTH = "birth";
    private final static String SEX = "sex";
    private final static String COUNTRY = "country";
    private final static String CITY = "city";
    private final static String ISSUED = "issued";
    private final static String RECEIVED = "received";
    private final static String ORDERED = "ordered";
    private final static String VERIFICATION_STATUS_TEXT = "verification_status_text";
    private final static String VERIFICATION_COMMENT = "verification_comment";
    private final static String DELIVERY = "delivery";
    private final static String TRACK_NUMBER = "track_number";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = UUID)
    private String mUuid;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = TRACK_NUMBER)
    private String mTrackNumber;

    @DatabaseField(dataType = DataType.STRING, columnName = PHONE)
    private String mPhoneNumber;

    @DatabaseField(dataType = DataType.STRING, columnName = NAME)
    private String mName;

    @DatabaseField(dataType = DataType.STRING, columnName = FIRSTNAME)
    private String mFirstname;

    @DatabaseField(dataType = DataType.STRING, columnName = LASTNAME)
    private String mLastname;

    @DatabaseField(dataType = DataType.STRING, columnName = BIRTH)
    private String mBirth;

    @DatabaseField(dataType = DataType.STRING, columnName = SEX)
    private String mSex;

    @DatabaseField(dataType = DataType.STRING, columnName = COUNTRY)
    private String mCountry;

    @DatabaseField(dataType = DataType.STRING, columnName = CITY)
    private String mCity;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = ISSUED)
    private boolean isIssued;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = RECEIVED)
    private boolean isReceived;

    @DatabaseField(dataType = DataType.STRING, columnName = ORDERED)
    private String mOrdered;

    @DatabaseField(dataType = DataType.STRING, columnName = VERIFICATION_STATUS_TEXT)
    private String mVerificationStatusText;

    @DatabaseField(dataType = DataType.STRING, columnName = VERIFICATION_COMMENT)
    private String mVerificationComment;

    @DatabaseField(persisterClass = DeliveryType.class, columnName = DELIVERY)
    private DeliverTypeModel mDeliverTypeModel;

    public OrmPassportModel() {
    }

    public OrmPassportModel(@NonNull Passport passport) {
        mId = 1;
        mUuid = passport.getUuid();
        mPhoneNumber = passport.getPhoneNumber();
        mName = passport.getName();
        mFirstname = passport.getFirstname();
        mLastname = passport.getLastname();
        mBirth = passport.getBirth();
        mSex = passport.getSex();
        mCountry = passport.getCountry();
        mCity = passport.getCity();
        this.isIssued = passport.isIssued();
        this.isReceived = passport.isReceived();
        mOrdered = passport.getOrdered();
        mVerificationStatusText = passport.getVerificationStatusText();
        mVerificationComment = passport.getVerificationComment();
        mTrackNumber = passport.getTrackNumber();

        Delivery delivery = passport.getDelivery();
        if (delivery != null) {
            mDeliverTypeModel = new DeliverTypeModel(delivery);
        }
    }

    public String getUuid() {
        return mUuid;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public String getName() {
        return mName;
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getBirth() {
        return mBirth;
    }

    public String getSex() {
        return mSex;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public boolean isIssued() {
        return isIssued;
    }

    public boolean isReceived() {
        return isReceived;
    }

    public String getOrdered() {
        return mOrdered;
    }

    public String getVerificationStatusText() {
        return mVerificationStatusText;
    }

    public String getVerificationComment() {
        return mVerificationComment;
    }

    public Delivery getDelivery() {
        return mDeliverTypeModel.getDelivery();
    }

    public String getTrackNumber() {
        return mTrackNumber;
    }
}
