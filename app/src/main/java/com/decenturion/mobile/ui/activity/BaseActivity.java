package com.decenturion.mobile.ui.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity implements IScreenActivityView {

    private Unbinder mUnbinder;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        bindView();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        bindView();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        bindView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindView();
    }

    private void bindView() {
        mUnbinder = ButterKnife.bind(this);
    }

    private void unbindView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void showMessage(@NonNull String message) {

    }

    @Override
    public void showSuccessMessage(@NonNull String message) {

    }

    @Override
    public void showErrorMessage(@NonNull String message) {

    }

    @Override
    public void showWarningMessage(@NonNull String message) {

    }

    @Override
    public void showProgressView() {

    }

    @Override
    public void hideProgressView() {

    }
}
