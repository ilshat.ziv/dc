package com.decenturion.mobile.network.response.payment.citizen;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CitizenPaymentResponse extends BaseResponse {

    @JsonProperty("result")
    private CitizenPaymentResult mCitizenPaymentResult;

    public CitizenPaymentResponse() {
    }

    @JsonIgnore
    public CitizenPaymentResult getResult() {
        return mCitizenPaymentResult;
    }
}
