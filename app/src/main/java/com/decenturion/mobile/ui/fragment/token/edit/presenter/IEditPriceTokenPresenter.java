package com.decenturion.mobile.ui.fragment.token.edit.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.edit.view.IEditPriceTokenView;

public interface IEditPriceTokenPresenter extends IPresenter<IEditPriceTokenView> {

    void bindViewData(int tokenId);

    void saveChanges(int tokenId, @NonNull String sell);

    void onSaveInstanceState(@NonNull Bundle bundle, int tokenId, @NonNull String buy, @NonNull String sell);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
