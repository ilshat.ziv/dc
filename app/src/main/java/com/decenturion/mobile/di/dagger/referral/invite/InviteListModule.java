package com.decenturion.mobile.di.dagger.referral.invite;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.referral.invite.IInviteListInteractor;
import com.decenturion.mobile.business.referral.invite.InviteListInteractor;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.ui.fragment.referral.invite.presenter.IInviteListPresenter;
import com.decenturion.mobile.ui.fragment.referral.invite.presenter.InviteListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class InviteListModule {

    @Provides
    @InviteListlScope
    IInviteListInteractor provideIInviteListInteractor(IReferralClient iReferralClient,
                                                                   ILocalistionManager iLocalistionManager) {

        return new InviteListInteractor(iReferralClient, iLocalistionManager);
    }

    @Provides
    @InviteListlScope
    IInviteListPresenter provideIInviteListPresenter(IInviteListInteractor iInviteListInteractor) {
        return new InviteListPresenter(iInviteListInteractor);
    }
}
