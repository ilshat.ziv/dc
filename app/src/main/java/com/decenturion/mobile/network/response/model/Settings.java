package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Settings {

    @JsonProperty("show_name")
    private boolean isShowName;

    @JsonProperty("show_dob")
    private boolean isShowBirth;

    @JsonProperty("show_photo")
    private boolean isShowPhoto;

    @JsonProperty("show_tokens")
    private boolean isShowTokens;

    @JsonProperty("show_money")
    private boolean isShowMoney;

    @JsonProperty("show_power")
    private boolean isShowPower;

    @JsonProperty("show_glory")
    private boolean isShowGlory;

    public Settings() {
    }

    @JsonIgnore
    public boolean isShowName() {
        return isShowName;
    }

    @JsonIgnore
    public boolean isShowBirth() {
        return isShowBirth;
    }

    @JsonIgnore
    public boolean isShowPhoto() {
        return isShowPhoto;
    }

    @JsonIgnore
    public boolean isShowTokens() {
        return isShowTokens;
    }

    @JsonIgnore
    public boolean isShowMoney() {
        return isShowMoney;
    }

    @JsonIgnore
    public boolean isShowPower() {
        return isShowPower;
    }

    @JsonIgnore
    public boolean isShowGlory() {
        return isShowGlory;
    }
}
