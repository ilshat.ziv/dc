package com.decenturion.mobile.ui.fragment.settings.delivery.fill.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.view.IFillDeliverySettingsView;

public interface IFillDeliverySettingsPresenter extends IPresenter<IFillDeliverySettingsView> {

    void bindViewData();

    void setDeliveryData(@NonNull String value,
                         @NonNull String value1,
                         @NonNull String value2,
                         @NonNull String value3,
                         @NonNull String selection,
                         @NonNull String value4);

    void uploadPhoto(@NonNull Bitmap photo);
}
