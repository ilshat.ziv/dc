package com.decenturion.mobile.repository.wallet;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.database.dao.OrmWalletDAO;
import com.decenturion.mobile.database.model.OrmWalletModel;
import com.decenturion.mobile.network.response.model.Wallet;
import com.decenturion.mobile.utils.LoggerUtils;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;
import java.util.List;

public class WalletRepository implements IWalletRepository {

    private OrmHelper mOrmHelper;

    public WalletRepository(@NonNull OrmHelper ormHelper) {
        mOrmHelper = ormHelper;
    }

    @Override
    public void saveWalletInfo(@NonNull Wallet wallet) throws Exception {
        OrmWalletModel model = new OrmWalletModel(wallet);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmWalletDAO dao = new OrmWalletDAO(connectionSource, OrmWalletModel.class);
        dao.createOrUpdate(model);
    }

    @Override
    public List<OrmWalletModel> getWalletInfo() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmWalletDAO dao = new OrmWalletDAO(connectionSource, OrmWalletModel.class);
            return dao.queryForAll();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return new ArrayList<>();
    }
}
