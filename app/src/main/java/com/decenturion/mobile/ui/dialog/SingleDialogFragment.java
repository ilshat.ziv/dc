package com.decenturion.mobile.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.snack.SnackManager;

import butterknife.BindView;

public abstract class SingleDialogFragment extends BaseDialogFragment {

	private MaterialDialog mDialog;

	@Nullable
	@BindView(R.id.mainContent)
	LinearLayout mMainContent;

	@Nullable
	@BindView(R.id.waitContent)
	LinearLayout mWaitContent;

	private String mTitleText;
	private int mPositiveText = R.string.ok;
	private String mPositiveTextString;
	private int mNegativeText = R.string.cancel;
	private String mNegativeTextString;

	public void setPositiveText(int positiveText) {
		mPositiveText = positiveText;
	}

	public void setPositiveTextString(String text) {
		mPositiveTextString = text;
	}

	public void setNegativeTextString(String text) {
		mNegativeTextString = text;
	}

	public void setNegativeText(int negativeText) {
		mNegativeText = negativeText;
	}

	public void setTitleText(String titleText) {
		mTitleText = titleText;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Context context = getContext();
		assert context != null;
		Resources resources = context.getResources();
		final MaterialDialog.Builder builder = new MaterialDialog.Builder(requireActivity())
                .positiveText(mPositiveText)
                .customView(R.layout.view_dialog_parent, true)
				.positiveColorRes(R.color.colorAccent)
				.backgroundColor(Color.parseColor("#1F1F1F"))
				.negativeColor(resources.getColor(R.color.white))
				.positiveColor(resources.getColor(R.color.white))
                .negativeText(mNegativeText)
                .negativeColorRes(R.color.light_gray)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog materialDialog) {
                        onPositiveClickButton();
                    }

                    @Override
                    public void onNegative(MaterialDialog materialDialog) {
                        onNegativeClickButton();
                    }
                })
                .autoDismiss(false);

        if (!TextUtils.isEmpty(mPositiveTextString)) {
        	builder.positiveText(mPositiveTextString);
		}

		if (!TextUtils.isEmpty(mNegativeTextString)) {
			builder.negativeText(mNegativeTextString);
		}

        if (!TextUtils.isEmpty(mTitleText)) {
			builder.title(mTitleText);
		}

		mDialog = builder.build();

		AssetManager assets = getContext().getAssets();
		Typeface typeface = Typeface.createFromAsset(assets, "font/Canada Type - VoxRound-Medium.otf");

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			mDialog.getTitleView().setTextAppearance(R.style.AppTheme_MaterialDialogTitle);
		} else {
			mDialog.getTitleView().setTextAppearance(getActivity(), R.style.AppTheme_MaterialDialogTitle);
		}

		mDialog.getActionButton(DialogAction.NEGATIVE).setTypeface(typeface);
		mDialog.getActionButton(DialogAction.POSITIVE).setTypeface(typeface);
		mDialog.getActionButton(DialogAction.NEUTRAL).setTypeface(typeface);

		View view = mDialog.getView();
		mMainContent = view.findViewById(R.id.mainContent);
		mWaitContent = view.findViewById(R.id.waitContent);

		mISnackManager = new SnackManager(view);

		return mDialog;
	}

	private void initView() {
		if (mMainContent != null && mWaitContent != null) {
			return;
		}
		View view = mDialog.getView();
		mMainContent = view.findViewById(R.id.mainContent);
		mWaitContent = view.findViewById(R.id.waitContent);
	}

	public Dialog getDialog() {
		return mDialog;
	}

	public void setContentView(int layout) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(layout, null);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);

		initView();
		if (mMainContent != null) {
			mMainContent.addView(view);
			view.setLayoutParams(params);
		}
	}

	public void onPositiveClickButton() {
	}

	public void onNegativeClickButton() {
		dismiss();
	}

	protected void setEnabledPositiveClickButton(boolean isEnabled) {
		mDialog.getActionButton(DialogAction.POSITIVE).setEnabled(isEnabled);
	}

	protected void setVisibilityNegativeClickButton(boolean isVisible) {
		if (isVisible) {
			mDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.VISIBLE);
		} else {
			mDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
		}
	}

	protected void setVisibilityPositiveClickButton(boolean isVisible) {
		if (isVisible) {
			mDialog.getActionButton(DialogAction.POSITIVE).setVisibility(View.VISIBLE);
		} else {
			mDialog.getActionButton(DialogAction.POSITIVE).setVisibility(View.GONE);
		}
	}

	private void setVisibilityActions(boolean isVisible) {
		setVisibilityPositiveClickButton(isVisible);
		setVisibilityNegativeClickButton(isVisible);
	}
}