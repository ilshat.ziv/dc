package com.decenturion.mobile.network.response.version;

import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.version.AppMinVersionResult;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppMinVersionResponse extends BaseResponse {

    @JsonProperty("result")
    private AppMinVersionResult mResult;

    public AppMinVersionResponse() {
    }

    @JsonIgnore
    public AppMinVersionResult getResult() {
        return mResult;
    }
}
