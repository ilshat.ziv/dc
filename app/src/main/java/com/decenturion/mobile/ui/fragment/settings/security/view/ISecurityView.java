package com.decenturion.mobile.ui.fragment.settings.security.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;

public interface ISecurityView extends IScreenFragmentView {

    void onBindViewData(@NonNull SecurityModelView model);

    void onChangesSaved(@NonNull SecurityModelView modelView);
}
