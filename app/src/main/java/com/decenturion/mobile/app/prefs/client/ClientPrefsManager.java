package com.decenturion.mobile.app.prefs.client;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.BasePrefsManager;

public class ClientPrefsManager extends BasePrefsManager implements IClientPrefsManager {

    private static final String SHARED_PREFERENCES_NAME = "CLIENT_PREFS";

    public ClientPrefsManager(@NonNull Context context) {
        super(context, SHARED_PREFERENCES_NAME);
    }

    @Override
    public void clear() {
        boolean b = getParam(ClientPrefOptions.Keys.IS_SKIP_WALKTHROUGH, false);
        super.clear();
        setParam(ClientPrefOptions.Keys.IS_SKIP_WALKTHROUGH, b);
    }
}
