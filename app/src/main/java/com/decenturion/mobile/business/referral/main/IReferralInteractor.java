package com.decenturion.mobile.business.referral.main;

import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;

import rx.Observable;

public interface IReferralInteractor {

    Observable<ReferralModelView> getReferralInfo();
}
