package com.decenturion.mobile.network.http.converter;

import com.fasterxml.jackson.databind.ObjectReader;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

final class JacksonResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private final ObjectReader mAdapter;

    JacksonResponseBodyConverter(ObjectReader adapter) {
        this.mAdapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        try {
            return mAdapter.readValue(value.charStream());
        } finally {
            value.close();
        }
    }
}