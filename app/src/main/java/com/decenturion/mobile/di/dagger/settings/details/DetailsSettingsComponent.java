package com.decenturion.mobile.di.dagger.settings.details;

import com.decenturion.mobile.ui.fragment.settings.details.view.DetailsSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DetailsSettingsModule.class
})

@DetailsSettingsScope
public interface DetailsSettingsComponent {

    void inject(DetailsSettingsFragment view);
}
