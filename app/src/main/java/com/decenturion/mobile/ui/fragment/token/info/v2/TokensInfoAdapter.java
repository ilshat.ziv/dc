package com.decenturion.mobile.ui.fragment.token.info.v2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.ui.fragment.token.info.v2.view.CategoryTokenFragment;

public class TokensInfoAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private SparseArray<Fragment> mHashMap;

    private ILocalistionManager mILocalistionManager;

    TokensInfoAdapter(FragmentManager fm, ILocalistionManager iLocalistionManager) {
        super(fm);
        this.mNumOfTabs = 3;
        mHashMap = new SparseArray<>();
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Fragment getItem(int position) {
        if (mHashMap.get(position) != null) {
            return mHashMap.get(position);
        }
        Fragment fragment = null;
        switch (position) {
            case 0: {
                Bundle bundle = new Bundle();
                bundle.putString(CategoryTokenFragment.CATEGORY, "passport");
                fragment = new CategoryTokenFragment();
                fragment.setArguments(bundle);
                break;
            }
            case 1: {
                Bundle bundle = new Bundle();
                bundle.putString(CategoryTokenFragment.CATEGORY, "classic");
                fragment = new CategoryTokenFragment();
                fragment.setArguments(bundle);
                break;
            }
            case 2: {
                Bundle bundle = new Bundle();
                bundle.putString(CategoryTokenFragment.CATEGORY, "liquid");
                fragment = new CategoryTokenFragment();
                fragment.setArguments(bundle);
                break;
            }
        }
        mHashMap.put(position, fragment);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_tabitem_passport);
            }
            case 1: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_tabitem_internal);
            }
            case 2: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_tabitem_external);
            }
        }
        throw new RuntimeException();
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}