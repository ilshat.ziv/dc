package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenCoin {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("contract")
    private String mContract;

    @JsonProperty("symbol")
    private String mSymbol;

    @JsonProperty("name")
    private String mName;

    @JsonProperty("decimals")
    private int mDecimals;

    public TokenCoin() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getContract() {
        return mContract;
    }

    @JsonIgnore
    public String getSymbol() {
        return mSymbol;
    }

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public int getDecimals() {
        return mDecimals;
    }

    @JsonIgnore
    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    @JsonIgnore
    public void setContract(String contract) {
        mContract = contract;
    }

    @JsonIgnore
    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    @JsonIgnore
    public void setName(String name) {
        mName = name;
    }

    @JsonIgnore
    public void setDecimals(int decimals) {
        mDecimals = decimals;
    }
}
