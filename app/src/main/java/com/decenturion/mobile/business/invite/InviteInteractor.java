package com.decenturion.mobile.business.invite;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.InviteModel;
import com.decenturion.mobile.network.response.invite.send.InviteResponse;
import com.decenturion.mobile.network.response.invite.send.InviteResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;

import rx.Observable;

public class InviteInteractor implements IInviteInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public InviteInteractor(IResidentClient IResidentClient,
                            IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<InviteResult> invite(@NonNull String email, @NonNull String token) {
        InviteModel model = new InviteModel();
        model.setEmail(email);
        model.setToken(token);
        return mIResidentClient.invite(model)
                .map(InviteResponse::getResult);
    }
}
