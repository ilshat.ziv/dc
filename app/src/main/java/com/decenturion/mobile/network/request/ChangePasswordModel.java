package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordModel {

    @JsonProperty("old_password")
    private String mOldPassword;

    @JsonProperty("password")
    private String mPassword;

    public ChangePasswordModel() {
    }

    @JsonIgnore
    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    @JsonIgnore
    public void setOldPassword(@NonNull String oldPassword) {
        mOldPassword = oldPassword;
    }
}
