package com.decenturion.mobile.ui.fragment.settings.mail.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.mail.view.IMailingSettingsView;

public interface IMailingSettingsPresenter extends IPresenter<IMailingSettingsView> {

    void bindViewData();

    void saveChanges(boolean isSubscribe);
}
