package com.decenturion.mobile.app.prefs.device;

import com.decenturion.mobile.app.prefs.IBasePrefsManager;

public interface IDevicePrefsManager extends IBasePrefsManager {

    boolean isSyncDatesValid();

    void saveSyncDates(long remoteServerTime, long localTime);

    void unsetSyncDates();

    long getSyncDates();
}
