package com.decenturion.mobile.app.prefs;

import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.app.prefs.device.IDevicePrefsManager;
import com.decenturion.mobile.app.prefs.locale.ILocalePrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;

public interface IPrefsManager {

    IDevicePrefsManager getIDevicePrefsManager();

    IClientPrefsManager getIClientPrefsManager();

    ITokensPrefsManager getITokensPrefsManager();

    ILocalePrefsManager getILocalePrefsManager();

}
