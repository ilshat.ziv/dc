package com.decenturion.mobile.ui.fragment.token.transaction.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.model.OrmTraideModel;

public class TransactionModelView {

    private String mTraideUUID;
    private String mAmount;
    private String mCategory;
    private String mCurrency;
    private String mMexpiredAt;
    private String mFrontendId;
    private boolean isInternal;
    private String mPrice;
    private String mReceiveAddress;
    private String mRefundAddress;
    private String mStatus;
    private String mTotalPrice;
    private String mTradeAddress;
    private String mTxid;

    public TransactionModelView() {
    }

    public TransactionModelView(String traideUUID) {
        mTraideUUID = traideUUID;
    }

    public TransactionModelView(@NonNull OrmTokenModel model) {
        model.getId();
    }

    public TransactionModelView(@NonNull OrmTraideModel d) {
        mTraideUUID = d.getUuid();
        mAmount = d.getAmount();
        mCategory = d.getCategory();
        mCurrency = d.getCurrency();
        mMexpiredAt = d.getExpiredAt();
        mFrontendId = d.getFrontendId();
        isInternal = d.isInternal();
        mPrice = d.getPrice();
        mReceiveAddress = d.getReceiveAddress();
        mRefundAddress = d.getRefundAddress();
        mStatus = d.getStatus();
        mTotalPrice = d.getTotalPrice();
        mTradeAddress = d.getTradeAddress();
        mTxid = d.getTxid();
    }

    public void setTraideUUID(String traideUUID) {
        mTraideUUID = traideUUID;
    }

    public String getTraideUUID() {
        return mTraideUUID;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public String getMexpiredAt() {
        return mMexpiredAt;
    }

    public String getFrontendId() {
        return mFrontendId;
    }

    public boolean isInternal() {
        return isInternal;
    }

    public String getPrice() {
        return mPrice;
    }

    public String getReceiveAddress() {
        return mReceiveAddress;
    }

    public String getRefundAddress() {
        return mRefundAddress;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getTotalPrice() {
        return mTotalPrice;
    }

    public String getTradeAddress() {
        return mTradeAddress;
    }

    public String getTxid() {
        return mTxid;
    }
}
