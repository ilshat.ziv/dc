package com.decenturion.mobile.ui.activity.splash.presenter;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.splash.ISplashInteractor;

import com.decenturion.mobile.exception.UnsupportedVersionAppException;
import com.decenturion.mobile.network.http.exception.RestrictedException;
import com.decenturion.mobile.ui.activity.splash.view.ISplashView;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SplashPresenter extends Presenter<ISplashView> implements ISplashPresenter {

    private ISplashInteractor mISplashInteractor;
    private IPrefsManager mIPrefsManager;

    public SplashPresenter(ISplashInteractor iSplashInteractor, IPrefsManager iPrefsManager) {
        mISplashInteractor = iSplashInteractor;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public void bindViewData() {
        Observable<Boolean> obs = mISplashInteractor.buildApp()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<Boolean>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(Boolean b) {
        hideProgressView();
        if (mView != null) {
            mView.onSuccess();
        }
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof UnsupportedVersionAppException) {
            if (mView != null) {
                mView.onUpdateApp();
            }
        } else if (throwable instanceof RestrictedException) {
            if (mView != null) {
                mView.onAccessRestricted();
            }
        } else {
            LoggerUtils.exception(throwable);
            if (mView != null) {
                mView.onSuccess();
            }
        }
    }
}
