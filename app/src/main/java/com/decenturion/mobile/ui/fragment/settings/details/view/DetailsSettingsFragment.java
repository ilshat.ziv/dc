package com.decenturion.mobile.ui.fragment.settings.details.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.details.DetailsSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.MultiEditTextView;
import com.decenturion.mobile.ui.component.ProfileImageView;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.dialog.bottomsheet.PhotoDialogFragment;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.details.model.DetailsSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.details.presenter.IDetailsSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailsSettingsFragment extends BaseFragment implements IDetailsSettingsView,
        ProfileImageView.OnPhotoViewListener,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.bioView)
    MultiEditTextView mBioView;

    @BindView(R.id.mailView)
    SimpleTextView mMailView;

    @BindView(R.id.telegramContactView)
    EditTextView mTelegramContactView;

    @BindView(R.id.whatsappContactView)
    EditTextView mWhatsappContactView;

    @BindView(R.id.wechatContactView)
    EditTextView mWechatContactView;

    @BindView(R.id.viberContactView)
    EditTextView mViberContactView;

    @BindView(R.id.saveChangesButtonView)
    FloatView mSaveChangesButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IDetailsSettingsPresenter mIDetailsSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        DetailsSettingsComponent detailsSettingsComponent = appDelegate
                .getIDIManager()
                .plusDetailsSettingsComponent();
        detailsSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_details_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mBioView.showLabel();
        mBioView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_details_bio_title));
        mBioView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_details_bio_placeholder));
        mBioView.editMode();
        mBioView.setVisibleDivider(true);

        mMailView.showLabel();
        mMailView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_email_title));
        mMailView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_email_placeholder));
        mMailView.setVisibleDivider(true);

        mTelegramContactView.showLabel();
        mTelegramContactView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_details_telegram_title));
        mTelegramContactView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_details_telegram_placeholder));
        mTelegramContactView.editMode();
        mTelegramContactView.setVisibleDivider(true);

        mWhatsappContactView.showLabel();
        mWhatsappContactView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_details_whatsapp_title));
        mWhatsappContactView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_details_whatsapp_placeholder));
        mWhatsappContactView.editMode();
        mWhatsappContactView.setVisibleDivider(true);

        mWechatContactView.showLabel();
        mWechatContactView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_details_wechat_title));
        mWechatContactView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_details_wechat_placeholder));
        mWechatContactView.editMode();
        mWechatContactView.setVisibleDivider(true);

        mViberContactView.showLabel();
        mViberContactView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_details_viber_title));
        mViberContactView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_details_viber_placeholder));
        mViberContactView.editMode();
        mViberContactView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mSaveChangesButtonView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_details_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDetailsSettingsPresenter.bindView(this);
        mIDetailsSettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDetailsSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDetailsSettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIDetailsSettingsPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mSaveChangesButtonView);
    }

    @OnClick(R.id.saveChangesButtonView)
    public void onSaveChanges() {
        mIDetailsSettingsPresenter.saveChanges(mBioView.getValue(),
                mTelegramContactView.getValue(),
                mWhatsappContactView.getValue(),
                mViberContactView.getValue(),
                mWechatContactView.getValue());
    }

    @Override
    public void onBindViewData(@NonNull DetailsSettingsModelView model) {
        mBioView.setText(model.getAbout());
        mMailView.setText(model.getEmail());
        mTelegramContactView.setText(model.getTelegram());
        mViberContactView.setText(model.getViber());
        mWechatContactView.setText(model.getWechat());
        mWhatsappContactView.setText(model.getWhatsApp());
    }

    @Override
    public void onSavedChanges() {
//        IMainActivity activity = (IMainActivity) requireActivity();
//        INavigationManager iNavigationManager = activity.getINavigationManager();
//        iNavigationManager.navigateToBack();
    }

    @Override
    public void onUploadPhoto() {
        BottomSheetDialogFragment bottomSheetDialogFragment = new PhotoDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(PhotoDialogFragment.Args.IS_CROP_IMAGE, true);
        bottomSheetDialogFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onShowPhoto(Bitmap bitmap) {
    }

    @Override
    public void onShowPhoto(String urlImage) {
    }

    @Override
    public void onPhotoUploaded(@NonNull Bitmap bitmap) {
        mIDetailsSettingsPresenter.uploadPhoto(bitmap);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
