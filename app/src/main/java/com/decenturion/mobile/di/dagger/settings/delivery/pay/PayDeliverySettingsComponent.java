package com.decenturion.mobile.di.dagger.settings.delivery.pay;

import com.decenturion.mobile.ui.fragment.settings.delivery.pay.view.PayDeliverySettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PayDeliverySettingsModule.class
})

@PayDeliverySettingsSettingsScope
public interface PayDeliverySettingsComponent {

    void inject(PayDeliverySettingsFragment view);
}
