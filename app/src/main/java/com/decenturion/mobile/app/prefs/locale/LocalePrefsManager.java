package com.decenturion.mobile.app.prefs.locale;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.BasePrefsManager;

public class LocalePrefsManager extends BasePrefsManager implements ILocalePrefsManager {

    private static final String SHARED_PREFERENCES_NAME = "LOCALE_PREFS";

    public LocalePrefsManager(@NonNull Context context) {
        super(context, SHARED_PREFERENCES_NAME);
    }

}
