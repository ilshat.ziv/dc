package com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view.IDeliveredSettingsView;

public interface IDeliveredSettingsPresenter extends IPresenter<IDeliveredSettingsView> {

    void bindViewData();
}
