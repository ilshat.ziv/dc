package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class StartupView extends UBaseView {

    @Nullable
    @BindView(R.id.nameWalletView)
    TextView mValueView;

    @Nullable
    @BindView(R.id.walletValueView)
    TextView mWalletValueView;

    @Nullable
    @BindView(R.id.walletExchangeView)
    TextView mWalletExchangeView;

    public StartupView(Context context) {
        super(context);
    }

    public StartupView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StartupView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    public void setHint(String hint) {
        init();
        mValueView.setHint(String.valueOf(hint));
    }

    public void setText(String text) {
        init();
        mValueView.setText(text != null ? String.valueOf(text) : "");

    }

    public void setTokenBalance(double balance) {
        init();
        assert mWalletValueView != null;
        mWalletValueView.setText(formatDecimal(balance));
    }

    public void setTokenBalance(String balance) {
        init();
        assert mWalletValueView != null;
        mWalletValueView.setText(balance);
    }

    private String formatDecimal(double balance) {
        if(balance == (long) balance) {
            return String.format("%d", (long) balance);
        } else {
            return String.format("%s", balance);
        }
    }

    public void setTokenCoin(String text) {
        init();
        mWalletExchangeView.setText(text != null ? String.valueOf(text) : "");

    }

    @Override
    public void setMustFill(boolean isMust) {

    }
}