package com.decenturion.mobile.network.response.score;

import com.decenturion.mobile.network.response.model.Token;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FutureScoreTokenListResult {

    @JsonProperty("List")
    private List<Token> mTokenList;

    @JsonProperty("total")
    private int mTotal;

    public FutureScoreTokenListResult() {
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }

    @JsonIgnore
    public List<Token> getTokenList() {
        return mTokenList;
    }
}
