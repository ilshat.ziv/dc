package com.decenturion.mobile.ui.fragment.pass.reset.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.pass.reset.view.IResetPasswordView;

public interface IResetPasswordPresenter extends IPresenter<IResetPasswordView> {

    void bindViewData();

    void resetPassword(@NonNull String currentPassword,
                       @NonNull String newPassword,
                       @NonNull String retypePassword);
}
