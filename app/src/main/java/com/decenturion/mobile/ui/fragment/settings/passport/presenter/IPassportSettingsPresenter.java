package com.decenturion.mobile.ui.fragment.settings.passport.presenter;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.passport.view.IPassportSettingsView;

public interface IPassportSettingsPresenter extends IPresenter<IPassportSettingsView> {

    void bindViewData();

    void saveChanges(@NonNull String firstName,
                     @NonNull String secondName,
                     long birth,
                     @NonNull String sex,
                     @NonNull String country,
                     @NonNull String city);

    void uploadPhoto(@NonNull Bitmap photo);

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
