package com.decenturion.mobile.ui.component;

import android.widget.ImageView;

public interface Component {

    ImageView getIconView();

    void hideLabel();

    void showLabel();

    void setMustFill(boolean isMust);

    void editMode();

    void simpleMode();

    enum Mode {
        EDIT, SIMPLE
    }
}
