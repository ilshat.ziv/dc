package com.decenturion.mobile.ui.fragment.score.send.main.model;

import java.util.ArrayList;

public class MainScoreSendToModelView {

    private ArrayList<MainScoreSendToModel> mSendToData;

    public MainScoreSendToModelView(ArrayList<MainScoreSendToModel> sendToData) {
        mSendToData = sendToData;
    }

    public ArrayList<MainScoreSendToModel> getSendToData() {
        return mSendToData;
    }
}
