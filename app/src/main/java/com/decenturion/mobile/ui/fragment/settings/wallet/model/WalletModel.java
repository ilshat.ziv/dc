package com.decenturion.mobile.ui.fragment.settings.wallet.model;

import android.support.annotation.NonNull;

public class WalletModel {

    private String mAddress;
    private String mCoin;

    public WalletModel() {
    }

    public WalletModel(String address, String coin) {
        mAddress = address;
        mCoin = coin;
    }

    public WalletModel(@NonNull String address) {
        mAddress = address;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public void setCoin(String coin) {
        mCoin = coin;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getCoin() {
        return mCoin;
    }
}
