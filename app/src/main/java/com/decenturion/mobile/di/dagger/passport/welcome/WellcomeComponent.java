package com.decenturion.mobile.di.dagger.passport.welcome;

import com.decenturion.mobile.ui.fragment.passport.wellcome.view.WellcomeFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        WellcomeModule.class
})

@WellcomeScope
public interface WellcomeComponent {

    void inject(WellcomeFragment view);
}
