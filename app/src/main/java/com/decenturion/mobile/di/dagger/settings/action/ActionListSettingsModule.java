package com.decenturion.mobile.di.dagger.settings.action;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.profile.main.ILogsInteractor;
import com.decenturion.mobile.business.profile.main.LogsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.action.presenter.ActionListSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.action.presenter.IActionListSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActionListSettingsModule {

    @Provides
    @ActionListSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ActionListSettingsScope
    ILogsInteractor provideILogsInteractor(IResidentClient IResidentClient,
                                           IResidentRepository iResidentRepository,
                                           IPrefsManager iPrefsManager,
                                           ILocalistionManager iLocalistionManager) {
        return new LogsInteractor(IResidentClient, iResidentRepository, iPrefsManager, iLocalistionManager);
    }

    @Provides
    @ActionListSettingsScope
    IActionListSettingsPresenter provideIActionListSettingsPresenter(ILogsInteractor iLogsInteractor) {
        return new ActionListSettingsPresenter(iLogsInteractor);
    }
}
