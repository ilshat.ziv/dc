package com.decenturion.mobile.ui.activity.ministry;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.ministry.honorary.view.MinistryHonoraryProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.product.view.MinistryExternalProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.product.view.MinistryInternalProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.senator.view.MinistrySenatorProductFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class MinistryActivity extends SingleFragmentActivity {

    public static final String CATEGORY = "CATEGORY";
    private String mCategory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putString(MinistryInternalProductFragment.CATEGORY, mCategory);

        switch (mCategory) {
            case "honorary" : {
                iNavigationManager.navigateTo(MinistryHonoraryProductFragment.class, bundle);
                break;
            }
            case "senator" : {
                iNavigationManager.navigateTo(MinistrySenatorProductFragment.class, bundle);
                break;
            }
            case "external" : {
                iNavigationManager.navigateTo(MinistryExternalProductFragment.class, bundle);
                break;
            }
            default : {
                iNavigationManager.navigateTo(MinistryInternalProductFragment.class, bundle);
            }
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mCategory = intent.getStringExtra(CATEGORY);
        }
    }
}
