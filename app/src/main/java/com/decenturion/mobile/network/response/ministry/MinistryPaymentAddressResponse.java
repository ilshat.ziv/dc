package com.decenturion.mobile.network.response.ministry;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryPaymentAddressResponse extends BaseResponse {

    @JsonProperty("result")
    private String mResult;

    public MinistryPaymentAddressResponse() {
    }

    @JsonIgnore
    public String getResult() {
        return mResult;
    }
}
