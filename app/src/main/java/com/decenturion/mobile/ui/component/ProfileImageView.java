package com.decenturion.mobile.ui.component;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.CameraDialog;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileImageView extends RelativeLayout implements
        Component {

    @Nullable
    @BindView(R.id.imagePhotoView)
    ImageView mProfilePhotoView;

    @Nullable
    @BindView(R.id.uploadPhotoView)
    ConstraintLayout mUploadPhotoButtonView;

    private Context mContext;

    private OnPhotoViewListener mOnPhotoViewListener;

    private Bitmap mBitmap;
    private String mUrlImage;

    private boolean isEditable;

    public ProfileImageView(Context context) {
        super(context);
        init(context);
    }

    public ProfileImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProfileImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
    }

    private boolean isInitView() {
        return  (mProfilePhotoView != null && mUploadPhotoButtonView != null);
    }

    private void init() {
        if (isInitView()) {
            return;
        }
        ButterKnife.bind(this);
        if (!isInitView()) {
            return;
        }

        rebuildView();
    }

    @SuppressLint("WrongConstant")
    private void rebuildView() {
        if (mBitmap != null || !TextUtils.isEmpty(mUrlImage)) {
            mProfilePhotoView.setVisibility(VISIBLE);
            mUploadPhotoButtonView.setVisibility(GONE);
//            mAddImageView.setVisibility(VISIBLE);
            if (mBitmap != null) {
                mProfilePhotoView.setImageBitmap(mBitmap);
            }
        } else {
            mUploadPhotoButtonView.setVisibility(VISIBLE);
//            mAddNewImageView.setImageResource(R.drawable.img_profile_black);
//            mAddNewImageView.setVisibility(VISIBLE);
//            mAddImageView.setVisibility(GONE);
            mProfilePhotoView.setVisibility(GONE);
        }
    }

    @OnClick({R.id.imagePhotoView, R.id.uploadPhotoView})
    protected void onClickProfileView() {
        if (isEditable) {
            mOnPhotoViewListener.onUploadPhoto();
            return;
        }

        if (mUrlImage != null) {
            if (mOnPhotoViewListener != null) {
                mOnPhotoViewListener.onShowPhoto(mUrlImage);
            }
        } else if (mBitmap != null) {
            if (mOnPhotoViewListener != null) {
                mOnPhotoViewListener.onShowPhoto(mBitmap);
            }
        }
    }

    public void addBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return;
        }
        mBitmap = bitmap;
//        if (mChangeDataModelListener != null) {
//            mChangeDataModelListener.onChangeComponentData(this);
//        }
        rebuildView();
    }

    public Bitmap getImage() {
        return mBitmap;
    }

    public void addUrlImage(String urlImage) {
        init();
        mUrlImage = urlImage;

        if (!TextUtils.isEmpty(urlImage)) {
            Picasso.with(mContext)
                    .load(urlImage)
//                    .resize(2048, 1600)
//                    .onlyScaleDown()
//                    .placeholder(R.drawable.img_profile_black)
//                    .error(R.drawable.img_profile_black)
                    .into(mProfilePhotoView);
        }
        rebuildView();
    }

    public String getUrlImage() {
        return mUrlImage;
    }

    public void setOnPhotoViewListener(OnPhotoViewListener listener) {
        mOnPhotoViewListener = listener;
    }

//    public void setChangeDataModelListener(ChangeComponentListener changeDataModelListener) {
//        mChangeDataModelListener = changeDataModelListener;
//    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();
    }

    public void getResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        Bundle extras = (data != null) ? data.getExtras() : null;

        if (extras == null) {
            Exception throwable = new Exception("extras is null");
            LoggerUtils.exception(throwable);
            return;
        }

        switch (requestCode){
            case IMainActivity.REQUEST_IMAGE_CAPTURE : {
                byte[] datas = extras.getByteArray("data");
                Bitmap bitmap = BitmapFactory.decodeByteArray(datas, 0, datas.length);
                bitmap = ImageUtils.scaleBitmap(bitmap, 1000);
                bitmap = ImageUtils.compressBitmap(bitmap, 100);

                addBitmap(bitmap);
                mOnPhotoViewListener.onPhotoUploaded(bitmap);
                break;
            }
            case IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP :
            case IMainActivity.ACTION_IMAGE_FROM_GALARY_AND_CROP :
            case IMainActivity.ACTION_IMAGE_FROM_CAMERA : {
                String pathToFile = extras.getString(CameraDialog.Args.PATH_FILE_SAVE, "");
                File file = new File(pathToFile);
                if (file.exists()) {
                    Bitmap bitmap = ImageUtils.loadBitmap(file);
                    if (bitmap != null) {
                        bitmap = ImageUtils.scaleBitmap(bitmap, 1000);
                        bitmap = ImageUtils.compressBitmap(bitmap, 100);
                        bitmap = ImageUtils.fixRotateBitmap2(bitmap, pathToFile);

                        addBitmap(bitmap);
                        mOnPhotoViewListener.onPhotoUploaded(bitmap);
                    }
                }
                break;
            }
            case IMainActivity.ACTION_IMAGE_FROM_GALARY : {
                Uri uri = data.getData();
                if (uri == null) {
                    Exception throwable = new Exception("uri is null");
                    LoggerUtils.exception(throwable);
                    return;
                }

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), uri);
                    bitmap = ImageUtils.scaleBitmap(bitmap, 1000);
                    bitmap = ImageUtils.compressBitmap(bitmap, 100);
                    bitmap = ImageUtils.fixRotateBitmap2(bitmap, ImageUtils.getPathFromMediaUri(getContext(), uri));

                    addBitmap(bitmap);
                    mOnPhotoViewListener.onPhotoUploaded(bitmap);
                } catch (IOException e) {
                    LoggerUtils.exception(e);
                }
                break;
            }
        }
    }

    @Override
    public ImageView getIconView() {
        return null;
    }

    @Override
    public void hideLabel() {

    }

    @Override
    public void showLabel() {

    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {
        init();
        isEditable = true;
    }

    @Override
    public void simpleMode() {
        init();
        isEditable = false;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void clearImages() {
        if (mBitmap == null) {
            mBitmap = null;
            mUrlImage = null;
        }
        rebuildView();
    }

    public interface OnPhotoViewListener {
        void onUploadPhoto();
        void onShowPhoto(Bitmap bitmap);
        void onShowPhoto(String urlImage);

        void onPhotoUploaded(@NonNull Bitmap bitmap);
    }
}
