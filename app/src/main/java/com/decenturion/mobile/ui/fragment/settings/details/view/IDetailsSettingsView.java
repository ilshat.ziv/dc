package com.decenturion.mobile.ui.fragment.settings.details.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.details.model.DetailsSettingsModelView;

public interface IDetailsSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull DetailsSettingsModelView model);

    void onSavedChanges();
}
