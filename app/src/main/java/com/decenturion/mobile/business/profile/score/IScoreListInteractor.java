package com.decenturion.mobile.business.profile.score;

import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreListModelView;

import rx.Observable;

public interface IScoreListInteractor {

    Observable<ScoreListModelView> getScoreList();
}
