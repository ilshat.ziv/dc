package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Passport extends BaseResponse {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("number")
    private String mPhoneNumber;

    /**
     * Ilshat Zaripov
     */
    @JsonProperty("name")
    private String mName;

    @JsonProperty("firstname")
    private String mFirstname;

    @JsonProperty("lastname")
    private String mLastname;

    /**
     * 1991-04-06T00:00:00Z
     */
    @JsonProperty("dob")
    private String mBirth;

    @JsonProperty("sex")
    private String mSex;

    @JsonProperty("country")
    private String mCountry;

    @JsonProperty("city")
    private String mCity;

    @JsonProperty("issued")
    private boolean isIssued;

    @JsonProperty("received")
    private boolean isReceived;

    @JsonProperty("ordered")
    private String mOrdered;

    @JsonProperty("verification_status_text")
    private String mVerificationStatusText;

    @JsonProperty("verification_comment")
    private String mVerificationComment = null;

    @JsonProperty("delivery")
    private Delivery mDelivery;

    @JsonProperty("track_number")
    private String mTrackNumber;

    public Passport() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public String getFirstname() {
        return mFirstname;
    }

    @JsonIgnore
    public String getLastname() {
        return mLastname;
    }

    @JsonIgnore
    public String getBirth() {
        return mBirth;
    }

    @JsonIgnore
    public String getSex() {
        return mSex;
    }

    @JsonIgnore
    public String getCountry() {
        return mCountry;
    }

    @JsonIgnore
    public String getCity() {
        return mCity;
    }

    @JsonIgnore
    public boolean isIssued() {
        return isIssued;
    }

    @JsonIgnore
    public boolean isReceived() {
        return isReceived;
    }

    @JsonIgnore
    public String getOrdered() {
        return mOrdered;
    }

    @JsonIgnore
    public String getVerificationStatusText() {
        return mVerificationStatusText;
    }

    @JsonIgnore
    public String getVerificationComment() {
        return mVerificationComment;
    }

    @JsonIgnore
    public Delivery getDelivery() {
        return mDelivery;
    }

    @JsonIgnore
    public String getTrackNumber() {
        return mTrackNumber;
    }
}
