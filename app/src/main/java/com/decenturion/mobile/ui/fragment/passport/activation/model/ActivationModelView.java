package com.decenturion.mobile.ui.fragment.passport.activation.model;

import android.text.TextUtils;

import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.citizen.CitizenPaymentResult;

public class ActivationModelView {

    private String mTypePay;
    private String mCoinsPay;
    private String mCyrrencePay;
    private String mQrCode;
    private String mAddress;

    private PassportModel mPassportModel;

    public ActivationModelView() {
    }

    public ActivationModelView(CitizenPaymentResult d, Resident resident) {
        String coin1 = d.getCoin();
        if (TextUtils.equals(coin1, "btc")) {
            String s = "bitcoin:{address}?amount={amount}";
            mQrCode = s.replace("{address}", d.getAddress())
                    .replace("{amount}", String.valueOf(d.getAmount()));
        } else {
            mQrCode = d.getAddress();
        }

        mAddress = d.getAddress();
        mTypePay = d.getCoin();
        mCoinsPay = String.valueOf(d.getAmount());
        mCoinsPay = mCoinsPay.replaceAll("(\\.0*$)|(0*$)", "");
        if (TextUtils.equals(coin1, "dcnt")) {
            mCyrrencePay = " ";
        } else {
            mCyrrencePay = resident.getDepositPrice() + " $";
        }

        mPassportModel = new PassportModel(resident);
    }

    public ActivationModelView(Resident d) {
        mPassportModel = new PassportModel(d);
    }

    public String getTypePay() {
        return mTypePay;
    }

    public String getCoinsPay() {
        return mCoinsPay;
    }

    public String getCyrrencePay() {
        return mCyrrencePay;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getQrCode() {
        return mQrCode;
    }

    public void setTypePay(String typePay) {
        mTypePay = typePay;
    }

    public void setCoinsPay(String coinsPay) {
        mCoinsPay = coinsPay;
    }

    public void setCyrrencePay(String cyrrencePay) {
        mCyrrencePay = cyrrencePay;
    }

    public PassportModel getPassportModel() {
        return mPassportModel;
    }
}
