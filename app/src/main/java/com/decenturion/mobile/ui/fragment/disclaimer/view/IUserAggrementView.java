package com.decenturion.mobile.ui.fragment.disclaimer.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.disclaimer.model.UserAggrementModelView;

public interface IUserAggrementView extends IScreenFragmentView {

    void onBindViewData(@NonNull UserAggrementModelView model);

}
