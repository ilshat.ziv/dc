package com.decenturion.mobile.ui.fragment.token.details.view;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.tokendetails.details.TokenDetailsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.token.TokenActivity;
import com.decenturion.mobile.ui.activity.token.send.SendTokenActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.ui.fragment.token.details.TokenParamsAdapter;
import com.decenturion.mobile.ui.fragment.token.details.model.TokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.token.details.model.TokenParamsModelView;
import com.decenturion.mobile.ui.fragment.token.details.presenter.ITokenDetailsPresenter;
import com.decenturion.mobile.ui.fragment.token.edit.view.EditPriceTokenFragment;
import com.decenturion.mobile.ui.fragment.token.info.v2.TokenInfoFragment;
import com.decenturion.mobile.ui.fragment.token.send.view.SendTokenFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class TokenDetailsFragment extends BaseFragment implements ITokenDetailsView,
        View.OnClickListener,
        TokenParamsAdapter.OnItemClickListener {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_COIN_UUID = "TOKEN_COIN_UUID";
    public static final String TOKEN_CATEGORY = "TOKEN_CATEGORY";

    private String mResidentUuid;
    private String mTokenCategory;
    private String mTokenCoinUuid;
    private ScoreType mScoreType;
    private int mTokenId;

    /* UI */

    @BindView(R.id.tokenNameView)
    TextView mTokenNameView;

    @BindView(R.id.tokenParamsListView)
    RecyclerView mTokenParamsListView;
    private TokenParamsAdapter mTokenParamsAdapter;

    @BindView(R.id.buyValueView)
    TextView mBuyValueView;

    @BindView(R.id.sellValueView)
    TextView mSellValueView;

    @BindView(R.id.amountValueView)
    TextView mAmountValueView;

    @BindView(R.id.tokenInfoView)
    LinearLayout mTokenInfoView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScrollView;

    @BindView(R.id.sendTokenButtonView)
    LocalizedButtonView mSendTokenButtonView;

    @BindView(R.id.declineButtonView)
    LocalizedButtonView mDeclineButtonView;

    @BindView(R.id.editPriceButtonView)
    LocalizedButtonView mEditPriceButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ITokenDetailsPresenter mITokenDetailsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = getActivity();
        assert activity != null;
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mTokenId = bundle.getInt(TOKEN_ID, 0);
            mTokenCoinUuid = bundle.getString(TOKEN_COIN_UUID);
            mTokenCategory = bundle.getString(TOKEN_CATEGORY);
            mResidentUuid = bundle.getString(RESIDENT_UUID);
            mScoreType = (ScoreType) bundle.getSerializable(TokenActivity.TOKEN_SCORE_TYPE);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TokenDetailsComponent tokenDetailsComponent = appDelegate
                .getIDIManager()
                .plusTokenDetailsComponent();
        tokenDetailsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_passport_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        Context context = getContext();
        assert context != null;
        mTokenParamsListView.setLayoutManager(new LinearLayoutManager(context));
        mTokenParamsAdapter = new TokenParamsAdapter(context, this);
        mTokenParamsListView.setAdapter(mTokenParamsAdapter);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITokenDetailsPresenter.bindView(this);
//        if (savedInstanceState == null ||
//                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            if (mTokenId > 0) {
                mITokenDetailsPresenter.bindViewData(mTokenId);
            } else {
                mITokenDetailsPresenter.bindViewData(mResidentUuid, mTokenId, mTokenCoinUuid, mTokenCategory);
            }
//        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mITokenDetailsPresenter.unbindView();
        mITokenDetailsPresenter.onSaveInstanceState(outState);
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mITokenDetailsPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull TokenDetailsModelView model) {
        mTokenNameView.setText(model.getTokenName());
        mTokenParamsAdapter.replaceDataSet(model.getParamsModelViews());

        String sellPrice = model.getSellPrice();
        if (!TextUtils.isEmpty(sellPrice)) {
            String text = sellPrice + " USDT";
            mSellValueView.setText(text);
        }

        String amount = model.getAmount();
        if (!TextUtils.isEmpty(amount)) {
            String text = amount + " " + model.getCoin();
            mAmountValueView.setText(text);
        } else {
            mAmountValueView.setVisibility(View.GONE);
        }

        if (TextUtils.equals(model.getCoin(), "DCNT")) {
            mTokenInfoView.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.equals(model.getCategory(), "passport")
                && TextUtils.isEmpty(mResidentUuid)
                && mScoreType != ScoreType.FUTURES) {
            mEditPriceButtonView.setVisibility(View.VISIBLE);
            mSendTokenButtonView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.tokenInfoView)
    protected void onTokenInfo() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        Bundle bundle = new Bundle();
        bundle.putInt(TokenDetailsFragment.TOKEN_ID, mTokenId);
        bundle.putString(TokenInfoFragment.CATEGORY, mTokenCategory);
        iNavigationManager.navigateTo(TokenInfoFragment.class, true, bundle);
    }

    @OnClick(R.id.declineButtonView)
    protected void onDecline() {
    }

    @OnClick(R.id.sendTokenButtonView)
    protected void onSendToken() {
        Intent intent = new Intent(requireActivity(), SendTokenActivity.class);
        TokenDetailsModelView model = mITokenDetailsPresenter.getModelView();
        intent.putExtra(SendTokenFragment.TOKEN_ID, model.getId());
        intent.putExtra(SendTokenFragment.TOKEN_SCORE_TYPE, model.getId());
        startActivity(intent);
    }

    @OnClick(R.id.editPriceButtonView)
    protected void onEditPriceTokens() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        Bundle bundle = new Bundle();
        bundle.putInt(TOKEN_ID, mTokenId);
        iNavigationManager.navigateTo(EditPriceTokenFragment.class, true, bundle);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onItemClick(TokenParamsModelView model) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getValue()));
        try {
            startActivity(browserIntent);
        } catch (ActivityNotFoundException e) {
            LoggerUtils.exception(e);
        }
    }
}
