package com.decenturion.mobile.ui.activity.agreement;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.disclaimer.view.UserAggrementFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class UserAggrementActivity extends SingleFragmentActivity implements IUserAggrementActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INavigationManager iNavigationManager = getINavigationManager();
        iNavigationManager.navigateTo(UserAggrementFragment.class);
    }

    @Override
    public void aggree() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void disaggree() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
