package com.decenturion.mobile.di.dagger.support;

import com.decenturion.mobile.business.support.ISupportInteractor;
import com.decenturion.mobile.business.support.SupportInteractor;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.ui.fragment.support.presenter.ISupportPresenter;
import com.decenturion.mobile.ui.fragment.support.presenter.SupportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SupportModule {


    @Provides
    @SupportScope
    ISupportInteractor provideISupportInteractor(IResidentClient IResidentClient) {
        return new SupportInteractor(IResidentClient);
    }

    @Provides
    @SupportScope
    ISupportPresenter provideISupportPresenter(ISupportInteractor iSupportInteractor) {
        return new SupportPresenter(iSupportInteractor);
    }
}
