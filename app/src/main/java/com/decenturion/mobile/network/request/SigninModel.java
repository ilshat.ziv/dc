package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SigninModel {

    @JsonProperty("username")
    private String mUsername;

    @JsonProperty("password")
    private String mPassword;

    @JsonProperty("recaptcha")
    private String mReCaptcha;

    public SigninModel() {
    }

    @JsonIgnore
    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    @JsonIgnore
    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    @JsonIgnore
    public void setReCaptcha(String reCaptcha) {
        mReCaptcha = reCaptcha;
    }
}
