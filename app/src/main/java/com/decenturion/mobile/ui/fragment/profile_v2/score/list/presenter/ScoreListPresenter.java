package com.decenturion.mobile.ui.fragment.profile_v2.score.list.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.profile.score.IScoreListInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.view.IScoreListView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ScoreListPresenter extends Presenter<IScoreListView> implements IScoreListPresenter {

    private IScoreListInteractor mIScoreListInteractor;
    private ScoreListModelView mScoreListModelView;

    public ScoreListPresenter(IScoreListInteractor iScoreListInteractor) {
        mIScoreListInteractor = iScoreListInteractor;
        mScoreListModelView = new ScoreListModelView();
    }

    @Override
    public void bindViewData() {
        Observable<ScoreListModelView> obs = mIScoreListInteractor.getScoreList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ScoreListModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(@NonNull ScoreListModelView model) {
        mScoreListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }
}
