package com.decenturion.mobile.ui.fragment.traide.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.traide.model.TraidInfoModelView;

public interface ITraidInfoView extends IScreenFragmentView {

    void onBindViewData(@NonNull TraidInfoModelView model);
}
