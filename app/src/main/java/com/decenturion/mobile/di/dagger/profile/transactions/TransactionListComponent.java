package com.decenturion.mobile.di.dagger.profile.transactions;

import com.decenturion.mobile.ui.fragment.profile_v2.transactions.view.TransactionListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TransactionListModule.class
})

@TransactionListScope
public interface TransactionListComponent {

    void inject(TransactionListFragment view);
}
