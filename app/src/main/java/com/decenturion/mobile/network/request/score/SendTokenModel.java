package com.decenturion.mobile.network.request.score;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendTokenModel {

    @JsonProperty("recipient")
    private String mDCNTWallet;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCoinCategory;

    @JsonProperty("coin")
    private String mCoinUUID;

    @JsonProperty("code")
    private String mTwoFa;

    @JsonProperty("type")
    private String mType;

    public SendTokenModel() {
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoinUUID = coin;
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCoinCategory = category;
    }

    @JsonIgnore
    public void setDCNTWallet(String DCNTWallet) {
        mDCNTWallet = DCNTWallet;
    }

    @JsonIgnore
    public void setAmount(String amount) {
        mAmount = amount;
    }

    @JsonIgnore
    public void setTwoFa(String m2Fa) {
        this.mTwoFa = m2Fa;
    }

    @JsonIgnore
    public void setType(String type) {
        mType = type;
    }
}
