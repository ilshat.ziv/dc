package com.decenturion.mobile.ui.fragment.profile_v2.transactions.model;

import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class TransactionListModelView {

    private List<TransactionModel> mTransactionList;
    private int mTotal;
    private long mOffset;
    public final int LIMIT = 10;

    public TransactionListModelView() {
    }

    public TransactionListModelView(List<TransactionModel> d) {
        mTransactionList = d;
    }

    public TransactionListModelView(List<TransactionModel> list, int total) {
        mTransactionList = list;
        mTotal = total;
    }

    public List<TransactionModel> getTransactionList() {
        return mTransactionList;
    }

    public void addTransactions(List<TransactionModel> list) {
        if (CollectionUtils.isEmpty(mTransactionList)) {
            mTransactionList = new ArrayList<>();
        }

        if (!CollectionUtils.isEmpty(list)) {
            mTransactionList.addAll(list);
        }
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }
}
