package com.decenturion.mobile.di.dagger.settings.password;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface PasswordSettingsScope {
}
