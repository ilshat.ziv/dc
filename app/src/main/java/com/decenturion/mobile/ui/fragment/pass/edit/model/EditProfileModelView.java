package com.decenturion.mobile.ui.fragment.pass.edit.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.utils.DateTimeUtils;

public class EditProfileModelView {

    private String mName;
    private String mFirstname;
    private String mLastname;
    private String mBirth;
    private String mSex;
    private String mCountry;
    private String mCity;
    private PhotoModelView mPhotoModelView;

    public EditProfileModelView() {
    }

    public EditProfileModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1, OrmPhotoModel m2) {
        mName = m0.getEmail();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();

        long datetime = DateTimeUtils.getDatetime(m1.getBirth(), DateTimeUtils.Format.FORMAT_ISO_8601);
        datetime = DateTimeUtils.convertToUTC(datetime);
        mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);

        mSex = m1.getSex();
        mCountry = m1.getCountry();
        mCity = m1.getCity();

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getBirth() {
        return mBirth;
    }

    public String getSex() {
        return mSex;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }
}
