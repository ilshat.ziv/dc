package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryPassportModel {

    @JsonProperty("phone")
    private String mPhone;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("country")
    private String mCountry;

    @JsonProperty("city")
    private String mCity;

    @JsonProperty("state")
    private String mState;

    @JsonProperty("zip")
    private String mZip;

    public DeliveryPassportModel() {
    }

    @JsonIgnore
    public void setPhone(String phone) {
        mPhone = phone;
    }

    @JsonIgnore
    public void setAddress(String address) {
        mAddress = address;
    }

    @JsonIgnore
    public void setCountry(String country) {
        mCountry = country;
    }

    @JsonIgnore
    public void setCity(String city) {
        mCity = city;
    }

    @JsonIgnore
    public void setState(String state) {
        mState = state;
    }

    @JsonIgnore
    public void setZip(String zip) {
        mZip = zip;
    }

    @JsonIgnore
    public String getPhone() {
        return mPhone;
    }

    @JsonIgnore
    public String getAddress() {
        return mAddress;
    }

    @JsonIgnore
    public String getCountry() {
        return mCountry;
    }

    @JsonIgnore
    public String getCity() {
        return mCity;
    }

    @JsonIgnore
    public String getState() {
        return mState;
    }

    @JsonIgnore
    public String getZip() {
        return mZip;
    }
}
