package com.decenturion.mobile.ui.fragment.pass.confirm.email.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.pass.confirm.email.ConfirmEmailRestorePassComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.restore.RestorePassActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.model.ConfirmEmailRestorePassModelView;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.presenter.IConfirmEmailRestorePassPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmEmailRestorePassFragment extends BaseFragment implements IConfirmEmailRestorePassView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.labelView)
    TextView mLabelView;

    private String mContact;


    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IConfirmEmailRestorePassPresenter mIConfirmEmailRestorePassPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
        setHasOptionsMenu(true);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mContact = arguments.getString(RestorePassActivity.CONTACT);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ConfirmEmailRestorePassComponent confirmEmailRestorePassComponent = appDelegate
                .getIDIManager()
                .plusConfirmEmailRestorePassComponent();
        confirmEmailRestorePassComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_restore_pass_confirm_email, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_recovery_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIConfirmEmailRestorePassPresenter.bindView(this);
        mIConfirmEmailRestorePassPresenter.bindViewData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIConfirmEmailRestorePassPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.closeButtonView)
    protected void onClose() {
        FragmentActivity activity = requireActivity();
        activity.finish();
    }

    @Override
    public void onBindViewData(@NonNull ConfirmEmailRestorePassModelView model) {
        String contact = model.getEmail();
        contact = TextUtils.isDigitsOnly(contact) ? mContact : contact;

        String[] mails = contact.split("@");
        contact = mails[0].substring(0, 2) + "****@" + mails[1];

        String localeString = mILocalistionManager.getLocaleString(R.string.app_signin_recovery_text_sent);
//        String localeString = "На адрес <font color='#ffffff'>" + contact + "</font> выслано пиьмо со ссылкой для восстановления доступа";
        localeString = localeString.replace("{email}", contact);
        Spanned text = Html.fromHtml(localeString);
        mLabelView.setText(text);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIConfirmEmailRestorePassPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
