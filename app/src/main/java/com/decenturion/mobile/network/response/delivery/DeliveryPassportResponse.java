package com.decenturion.mobile.network.response.delivery;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryPassportResponse extends BaseResponse {

    @JsonProperty("result")
    private DeliveryPassportResult mSigninResult;

    public DeliveryPassportResponse() {
    }

    @JsonIgnore
    public DeliveryPassportResult getResult() {
        return mSigninResult;
    }
}
