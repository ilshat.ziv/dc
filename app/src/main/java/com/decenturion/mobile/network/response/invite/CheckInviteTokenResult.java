package com.decenturion.mobile.network.response.invite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInviteTokenResult {

    @JsonProperty("email")
    private String mEmail;

    public CheckInviteTokenResult() {
    }

    @JsonIgnore
    public String getEmail() {
        return mEmail;
    }
}
