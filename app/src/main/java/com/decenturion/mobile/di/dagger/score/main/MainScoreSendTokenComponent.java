package com.decenturion.mobile.di.dagger.score.main;

import com.decenturion.mobile.ui.dialog.token.send.SendTokenSuccessFragmentDialog;
import com.decenturion.mobile.ui.fragment.score.send.main.view.MainScoreSendTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        MainScoreSendTokenModule.class
})

@MainScoreSendTokenScope
public interface MainScoreSendTokenComponent {

    void inject(MainScoreSendTokenFragment view);

    void inject(SendTokenSuccessFragmentDialog view);
}
