package com.decenturion.mobile.ui.fragment.referral.invite.create.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;

public class CreateInviteModelView {

    private String mPrice;
    private String mTokenCount;
    private String mTokenBanner;

    public CreateInviteModelView(@NonNull ReferralSettingsResult d) {
        mPrice = d.getFixedCost();
        mTokenCount = d.getFixedTokenCount();
        mTokenBanner = d.getFixedComment();
    }

    public String getPrice() {
        return mPrice;
    }

    public String getTokenCount() {
        return mTokenCount;
    }

    public String getTokenBanner() {
        return mTokenBanner;
    }
}
