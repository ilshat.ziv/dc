package com.decenturion.mobile.ui.fragment.profile_v2.score.list.model;

import java.util.List;

public class ScoreListModelView {

    private List<ScoreModelView> mTokentModelList;

    public ScoreListModelView() {
    }

    public ScoreListModelView(List<ScoreModelView> d) {
        mTokentModelList = d;
    }

    public List<ScoreModelView> getTokentModelList() {
        return mTokentModelList;
    }

}
