package com.decenturion.mobile.business.passport.activation;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.PaymentModel;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.citizen.CitizenPaymentResponse;
import com.decenturion.mobile.network.response.payment.citizen.CitizenPaymentResult;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.activation.model.ActivationModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public class ActivationPassportInteractor implements IActivationPassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public ActivationPassportInteractor(IResidentClient IResidentClient,
                                        IResidentRepository iResidentRepository,
                                        IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<PassportModelView> getProfileInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                PassportModelView::new);
    }

    @Override
    public Observable<ActivationModelView> genPaymentAddress(@NonNull String coin) {
        Observable<CitizenPaymentResult> obs1;
        if (TextUtils.equals(coin, "dcnt")) {
            obs1 = Observable.just(mIResidentRepository.getResidentInfo())
                    .map(OrmResidentModel::getWalletNum)
                    .map(d -> {
                        CitizenPaymentResult citizenPaymentResult = new CitizenPaymentResult();
                        citizenPaymentResult.setAddress(d);
                        citizenPaymentResult.setCoin("dcnt");
                        citizenPaymentResult.setAmount(1);
                        citizenPaymentResult.setAmountLeft(1);

                        return citizenPaymentResult;
                    });
        } else {
            obs1 = Observable.just(new PaymentModel(coin))
                    .flatMap(d -> mIResidentClient.paymentCitizen(d))
                    .map(CitizenPaymentResponse::getResult);
        }

        Observable<Resident> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(ResidentResult::getResident)
                .doOnNext(d -> {
                    int step = d.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);
                });

        return obs2.flatMap(d -> {
            if (d.isActive()) {
                return Observable.just(new ActivationModelView(d));
            } else {
                return Observable.zip(obs1, Observable.just(d), ActivationModelView::new);
            }
        });
    }

}
