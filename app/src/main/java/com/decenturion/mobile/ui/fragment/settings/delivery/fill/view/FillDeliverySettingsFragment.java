package com.decenturion.mobile.ui.fragment.settings.delivery.fill.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.DecenturionConstants;
import com.decenturion.mobile.di.dagger.settings.delivery.fill.FillDeliverySettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.ProfileImageView;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.dialog.bottomsheet.PhotoDialogFragment;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.passport.online.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view.DeliveredSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.DeliveryDataModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.FillDeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.presenter.IFillDeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.view.PayDeliverySettingsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class FillDeliverySettingsFragment extends BaseFragment implements IFillDeliverySettingsView,
        ChangeComponentListener,
        View.OnClickListener,
        ProfileImageView.OnPhotoViewListener {

    /* UI */

    @BindView(R.id.titleView)
    TextView mTitleView;


    @BindView(R.id.payPassportDataView)
    LinearLayout mPayPassportDataView;

    @BindView(R.id.payStateView)
    LocalizedTextView mPayStateView;


    @BindView(R.id.deliveryPassportDataView)
    LinearLayout mDeliveryPassportDataView;

    @BindView(R.id.pasportDataView)
    LinearLayout mPasportDataView;


    @BindView(R.id.photoView)
    ProfileImageView mProfileImageView;

    @BindView(R.id.photoLabelView)
    LocalizedTextView mPhotoLabelView;


    @BindView(R.id.phoneView)
    EditTextView mPhoneView;

    @BindView(R.id.addressView)
    EditTextView mAddressView;

    @BindView(R.id.cityView)
    EditTextView mCityView;

    @BindView(R.id.stateView)
    EditTextView mStateView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.zipView)
    EditTextView mZipView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mProceedButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IPrefsManager mIPrefsManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IFillDeliverySettingsPresenter mIPhisicalPassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        FillDeliverySettingsComponent component = appDelegate
                .getIDIManager()
                .plusFillDeliverySettingsComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_fill_delivery_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mProceedButtonView.setEnabled(false);

        String localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_cost_title);
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean param = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_INVITED, false);
        if (param) {
            mTitleView.setText(localeString.replace("{passportCost}", DecenturionConstants.DELIVERY_PAY));
        } else {
            mTitleView.setText(localeString.replace("{passportCost}", "0"));
        }


        mPayStateView.setText(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_text_pay));

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_phone_title));
        mPhoneView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_phone_placeholder));
        mPhoneView.editMode();
        mPhoneView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mPhoneView.setVisibleDivider(true);
        mPhoneView.setChangeDataModelListener(this);

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_address_title));
        mAddressView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_address_placeholder));
        mAddressView.editMode();
        mAddressView.setVisibleDivider(true);
        mAddressView.setChangeDataModelListener(this);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_city_title));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_city_placeholder));
        mCityView.editMode();
        mCityView.setVisibleDivider(true);
        mCityView.setChangeDataModelListener(this);

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_region_title));
        mStateView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_region_placeholder));
        mStateView.editMode();
        mStateView.setVisibleDivider(true);
        mStateView.setChangeDataModelListener(this);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_country_title));
        mCountryView.setVisibleDivider(true);

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_zip_title));
        mZipView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_zip_placeholder));
        mZipView.editMode();
        mZipView.setVisibleDivider(true);
        mZipView.setChangeDataModelListener(this);

        mProfileImageView.setOnPhotoViewListener(this);
        mProfileImageView.editMode();

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPhisicalPassportPresenter.bindView(this);
        mIPhisicalPassportPresenter.bindViewData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mProfileImageView.getResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPhisicalPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIPhisicalPassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
        mIPhisicalPassportPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull CountryModelView model1,
                               @NonNull FillDeliverySettingsModelView model) {
//        if (model.isDeliveryPaid()) {
//            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
//            assert activity != null;
//            INavigationManager iNavigationManager = activity.getINavigationManager();
//            iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
//            return;
//        }

        mCountryView.setData(
                mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_country_placeholder),
                model1.getCountryData());

        DeliveryDataModelView deliverDataModelView = model.getDeliverDataModelView();
        mPhoneView.setText(deliverDataModelView.getPhone());
        mAddressView.setText(deliverDataModelView.getAddress());
        mCityView.setText(deliverDataModelView.getCity());
        mStateView.setText(deliverDataModelView.getState());
        mCountryView.selectItem(deliverDataModelView.getCountry());
        mZipView.setText(deliverDataModelView.getZip());

        PhotoModelView photoModelView = model.getPhotoModelView();
        if (photoModelView != null) {
            mProfileImageView.addUrlImage(photoModelView.getOriginal());
        }
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        mIPhisicalPassportPresenter.setDeliveryData(
                mPhoneView.getValue(),
                mAddressView.getValue(),
                mCityView.getValue(),
                mStateView.getValue(),
                mCountryView.getSelection(),
                mZipView.getValue()
        );
    }

    @Override
    public void onDeliveryPassportDataSeted(@NonNull FillDeliverySettingsModelView model) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        if (model.isInvited()) {
            if (model.isDeliveryPaid()) {
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
            } else {
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(PayDeliverySettingsFragment.class);
            }
        } else {
            INavigationManager iNavigationManager = activity.getINavigationManager();
            iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
        }
    }

    @Override
    public void onChangeComponentData(View view) {
        boolean b = !TextUtils.isEmpty(mPhoneView.getValue()) &&
                !TextUtils.isEmpty(mAddressView.getValue()) &&
                !TextUtils.isEmpty(mCityView.getValue()) &&
                !TextUtils.isEmpty(mStateView.getValue()) &&
                !TextUtils.isEmpty(mZipView.getValue()) &&
                !TextUtils.isEmpty(mZipView.getValue()) &&
                !TextUtils.isEmpty(mCountryView.getSelection());

        mProceedButtonView.setEnabled(b);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onUploadPhoto() {
        BottomSheetDialogFragment bottomSheetDialogFragment = new PhotoDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(PhotoDialogFragment.Args.IS_CROP_IMAGE, true);
        bottomSheetDialogFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onShowPhoto(Bitmap bitmap) {

    }

    @Override
    public void onShowPhoto(String urlImage) {

    }

    @Override
    public void onPhotoUploaded(@NonNull Bitmap bitmap) {
        mIPhisicalPassportPresenter.uploadPhoto(bitmap);
        mPhotoLabelView.setText(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_editphoto));
        mPhotoLabelView.setOnClickListener(v -> {
            if (mProfileImageView.isEditable()) {
                onUploadPhoto();
            }
        });
        onChangeComponentData(null);
    }
}
