package com.decenturion.mobile.network.response.score;

import com.decenturion.mobile.network.response.model.Token;

import com.decenturion.mobile.network.response.model.score.Transfer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScoreTokenListResult {

    @JsonProperty("list")
    private List<Token> mTokenList;

    @JsonProperty("total")
    private int mTotal;

    @JsonProperty("transfers_list")
    private List<Transfer> mTransfers;

    public ScoreTokenListResult() {
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }

    @JsonIgnore
    public List<Transfer> getTransfers() {
        return mTransfers;
    }

    @JsonIgnore
    public List<Token> getTokenList() {
        return mTokenList;
    }
}
