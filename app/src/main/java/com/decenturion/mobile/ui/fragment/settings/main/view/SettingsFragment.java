package com.decenturion.mobile.ui.fragment.settings.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.main.SettingsComponent;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.settings.SettingsActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.NextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.main.presenter.ISettingsPresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingsFragment extends BaseFragment implements ISettingsView {

    /* UI */

    @BindView(R.id.accountSettingsView)
    NextView mAccountSettingsView;

    @BindView(R.id.passportSettingsView)
    NextView mPassportSettingsView;

    @BindView(R.id.deliverySettingsView)
    NextView mDeliverySettingsView;

    @BindView(R.id.detailsSettingsView)
    NextView mDetailsSettingsView;

    @BindView(R.id.mailingSettingsView)
    NextView mMailingSettingsView;

    @BindView(R.id.walletSettingsView)
    NextView mWalletSettingsView;

    @BindView(R.id.securitySettingsView)
    NextView mSecuritySettingsView;

    @BindView(R.id.actionLogSettingsView)
    NextView mActionLogSettingsView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISettingsPresenter mISettingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SettingsComponent settingsComponent = appDelegate
                .getIDIManager()
                .plusSettingsComponent();
        settingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_settings_v2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mAccountSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_account));
        mPassportSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_passport));
        mDeliverySettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_delivery));
        mDetailsSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_details));
        mMailingSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_mailinglists));
        mWalletSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_title));
        mSecuritySettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_security));
        mActionLogSettingsView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_actionlog));
    }

    private void initToolbar() {
        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_title));
        iToolbarController.setSubTitle("");

        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.GONE);

        FragmentManager supportFragmentManager = getFragmentManager();
        assert supportFragmentManager != null;
        Fragment f = supportFragmentManager.findFragmentByTag("ProfileCollapsAppBarFragment");
        if (f != null) {
            FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISettingsPresenter.bindView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISettingsPresenter.unbindView();
    }

    @OnClick(R.id.accountSettingsView)
    protected void onAccountSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.ACCOUNT_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.passportSettingsView)
    protected void onPassportSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.PASSPORT_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.deliverySettingsView)
    protected void onDeliverySettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.DELIVERY_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.detailsSettingsView)
    protected void onDetailsSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.DETAILS_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.mailingSettingsView)
    protected void onMailingSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.MAILING_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.actionLogSettingsView)
    protected void onActionLogSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.ACTION_LOG_SETTINGS);
        startActivity(intent);
    }


    @OnClick(R.id.walletSettingsView)
    protected void onWalletSettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.WALLET_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.securitySettingsView)
    protected void onSecuritySettings() {
        Intent intent = new Intent(requireActivity(), SettingsActivity.class);
        intent.putExtra(SettingsActivity.TYPE_SETTINGS, SettingsActivity.SECYRITY_SETTINGS);
        startActivity(intent);
    }

    @OnClick(R.id.logoutView)
    protected void onLogout() {
        mISettingsPresenter.logout();
    }

    @Override
    public void logoutSuccess() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, SignActivity.class);
        startActivity(intent);
        activity.finish();
    }
}
