package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SigninGfaModel {

    @JsonProperty("code")
    private String mCode;

    public SigninGfaModel() {
    }

    @JsonIgnore
    public void setCode(@NonNull String code) {
        mCode = code;
    }
}
