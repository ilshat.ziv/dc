package com.decenturion.mobile.ui.fragment.support.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.support.view.ISupportView;

public interface ISupportPresenter extends IPresenter<ISupportView> {

    void sendMessage(@NonNull String theme, @NonNull String message);
}
