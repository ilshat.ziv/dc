package com.decenturion.mobile.ui.activity.splash.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.app.prefs.device.DevicePrefsOptions;
import com.decenturion.mobile.app.prefs.device.IDevicePrefsManager;
import com.decenturion.mobile.di.dagger.splash.SplashComponent;
import com.decenturion.mobile.ui.activity.BaseActivity;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.seller.SellerActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.activity.splash.Route;
import com.decenturion.mobile.ui.activity.splash.RouteProcessor;
import com.decenturion.mobile.ui.activity.splash.presenter.ISplashPresenter;
import com.decenturion.mobile.ui.activity.update.UpdateAppActivity;
import com.decenturion.mobile.ui.activity.walkthrough.WalkthroughActivity;
import com.decenturion.mobile.ui.dialog.DialogManager;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.access.AccessDenyDialog;
import com.decenturion.mobile.ui.fragment.seller.main.view.SellerProfileFragment;

import javax.inject.Inject;

public class SplashScreenActivity extends BaseActivity implements ISplashView {

    /* DI */

    @Inject
    ISplashPresenter mISplashPresenter;

    @Inject
    IPrefsManager mIPrefsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = (AppDelegate) getApplication();
        SplashComponent splashComponent = appDelegate
                .getIDIManager()
                .plusSplashComponent();
        splashComponent.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mISplashPresenter.bindView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mISplashPresenter.bindViewData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mISplashPresenter.unbindView();
    }

    @Override
    public void onSuccess() {
        IDevicePrefsManager iDevicePrefsManager = mIPrefsManager.getIDevicePrefsManager();
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        Intent intent = getIntent();

        int version = iDevicePrefsManager.getParam(DevicePrefsOptions.Keys.APP_VERSION_CODE, -1);
        if (version != BuildConfig.VERSION_CODE && (BuildConfig.VERSION_CODE == 18 || BuildConfig.VERSION_CODE == 20)) {
            iClientPrefsManager.clear();
            iDevicePrefsManager.setParam(DevicePrefsOptions.Keys.APP_VERSION_CODE, BuildConfig.VERSION_CODE);
        }

        int step = iClientPrefsManager.getParam(ClientPrefOptions.Keys.SIGN_STEP, -1);

        RouteProcessor routeProcessor = new RouteProcessor();
        Route route = routeProcessor.routeProcess(intent);
        if (route != null && route.getType() == Route.TypeRoute.CITIZEN) {
            String param = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_UUID, "");
            Bundle bundle = route.getBundle();
            if (TextUtils.equals(param, bundle.getString(SellerProfileFragment.SELLER_UUID))) {
                route = null;
            }
        }

        if (step > 5 || (route != null && route.getType() == Route.TypeRoute.CITIZEN)) {
            Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
            if (step < 5) {
                intent1 = new Intent(getApplicationContext(), SellerActivity.class);
            }
            if (route != null) {
                intent1.putExtras(route.getBundle());
            }
            startActivity(intent1);
        } else if (step < 2) {
            boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.IS_SKIP_WALKTHROUGH, false);
            if (!b) {
                Intent intent1 = new Intent(getApplicationContext(), WalkthroughActivity.class);
                if (route != null) {
                    intent1.putExtras(route.getBundle());
                }
                startActivity(intent1);
                ActivityCompat.finishAffinity(this);
                return;
            }

            Intent intent1 = new Intent(getApplicationContext(), SignActivity.class);
            if (route != null) {
                intent1.putExtras(route.getBundle());
            }
            startActivity(intent1);
        } else if (step == 2) {
            intent = new Intent(getApplicationContext(), PassportActivity.class);
            intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
            startActivity(intent);
        } else if (step == 3) {
            intent = new Intent(getApplicationContext(), PassportActivity.class);
            intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.PHISICAL_PASSPORT);
            startActivity(intent);
        } else if (step == 4) {
            intent = new Intent(getApplicationContext(), PassportActivity.class);
            intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ACTIVATION_PASSPORT);
            startActivity(intent);
        } else {
            intent = new Intent(getApplicationContext(), PassportActivity.class);
            intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.WELLCOME);
            startActivity(intent);
        }

        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void showErrorMessage(@NonNull String message) {
        super.showErrorMessage(message);
        Process.killProcess(Process.myPid());
    }

    @Override
    public void onUpdateApp() {
        startActivity(new Intent(this, UpdateAppActivity.class));
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void onAccessRestricted() {
        IDialogManager iDialogManager = new DialogManager(getSupportFragmentManager());
        iDialogManager.showDialog(AccessDenyDialog.class);
    }
}
