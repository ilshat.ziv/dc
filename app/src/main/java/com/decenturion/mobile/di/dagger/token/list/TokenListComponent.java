package com.decenturion.mobile.di.dagger.token.list;

import com.decenturion.mobile.ui.fragment.profile_v2.score.item.view.ScoreTokenListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TokenListModule.class
})

@TokenListScope
public interface TokenListComponent {

//    void inject(ScoreTokenListFragment view);
}
