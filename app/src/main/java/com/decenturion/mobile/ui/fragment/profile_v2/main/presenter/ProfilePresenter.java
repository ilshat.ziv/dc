package com.decenturion.mobile.ui.fragment.profile_v2.main.presenter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.profile.IProfileInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.main.model.ProfileModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.main.view.IProfileView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ProfilePresenter extends Presenter<IProfileView> implements IProfilePresenter {

    private Context mContext;
    private IProfileInteractor mIProfileInteractor;
    private ILocalistionManager mILocalistionManager;

    private ProfileModelView mProfileModelView;

    public ProfilePresenter(IProfileInteractor iProfileInteractor,
                            Context context,
                            ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mContext = context;
        mIProfileInteractor = iProfileInteractor;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<ProfileModelView> obs = mIProfileInteractor.getProfileInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ProfileModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ProfileModelView model) {
        mProfileModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void onCopyWalletAddress() {
        Observable<ProfileModelView> obs = Observable.just(mProfileModelView);
        mIDCompositeSubscription.subscribe(obs,
                (Action1<ProfileModelView>) this::copyWalletAddressSuccess,
                this::copyWalletAddressFailure
        );
    }

    private void copyWalletAddressFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void copyWalletAddressSuccess(ProfileModelView profileModelView) {
        mProfileModelView = profileModelView;

        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("DecenturionUserWallet", profileModelView.getWalletNum());
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }

    @Override
    public void onCopyProfileLink() {
        Observable<ProfileModelView> obs = Observable.just(mProfileModelView);
        mIDCompositeSubscription.subscribe(obs,
                (Action1<ProfileModelView>) this::copyProfileLinkSuccess,
                this::copyProfileLinkFailure
        );
    }

    private void copyProfileLinkFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void copyProfileLinkSuccess(ProfileModelView profileModelView) {
        mProfileModelView = profileModelView;

        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        String url = BuildConfig.HOST + "/" + profileModelView.getUID();
        ClipData clip = ClipData.newPlainText("DecenturionUserLink", url);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }

    @Override
    public void onCopyReferalLinc() {
        Observable<ProfileModelView> obs = Observable.just(mProfileModelView);
        mIDCompositeSubscription.subscribe(obs,
                (Action1<ProfileModelView>) this::copyReferalLincSuccess,
                this::copyReferalLincFailure
        );
    }

    private void copyReferalLincFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void copyReferalLincSuccess(ProfileModelView profileModelView) {
        mProfileModelView = profileModelView;

        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        String url = BuildConfig.HOST + "/signup/" + profileModelView.getReferralId();
        ClipData clip = ClipData.newPlainText("ReferallLink", url);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }


    @Override
    public void onShare() {
        Observable<ProfileModelView> obs = Observable.just(mProfileModelView);
        mIDCompositeSubscription.subscribe(obs,
                (Action1<ProfileModelView>) this::getShareDataSuccess,
                this::getShareDataFailure
        );
    }

    private void getShareDataFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void getShareDataSuccess(ProfileModelView profileModelView) {
        if (mView != null) {
            String url = BuildConfig.HOST + "/" + profileModelView.getUID();
            mView.onShare(url);
        }
    }

}
