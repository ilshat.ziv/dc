package com.decenturion.mobile.di.dagger.token.info;

import com.decenturion.mobile.ui.fragment.token.info.v2.TokenInfoFragment;
import com.decenturion.mobile.ui.fragment.token.info.v2.view.CategoryTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TokenInfoModule.class
})

@TokenInfoScope
public interface TokenInfoComponent {

    void inject(TokenInfoFragment view);

    void inject(CategoryTokenFragment view);
}
