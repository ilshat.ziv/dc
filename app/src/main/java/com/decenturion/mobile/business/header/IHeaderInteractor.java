package com.decenturion.mobile.business.header;

import com.decenturion.mobile.ui.fragment.header.model.HeaderModelView;

import rx.Observable;

public interface IHeaderInteractor {

    Observable<HeaderModelView> getProfileInfo();
}
