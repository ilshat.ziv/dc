package com.decenturion.mobile.ui.fragment.settings.delivery.pay.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;

public class PayDeliverySettingsModelView {

    private boolean isInvited;
    private boolean isActive;
    private boolean isDeliveryPaid;

    private DeliveryDataModelView mDeliverDataModelView;

    private PaymentModelView mPaymentModelView;

    public PayDeliverySettingsModelView() {
    }

    public PayDeliverySettingsModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.isActive() || m0.getStep() > 4;
        this.isDeliveryPaid = m0.isDeliveryPaid();

        Delivery delivery = m1.getDelivery();
        mDeliverDataModelView = new DeliveryDataModelView(delivery);
    }

    public PayDeliverySettingsModelView(Resident resident) {
        this.isInvited = resident.isInvited();
        this.isActive = resident.isActive() || resident.getStep() > 4;
        this.isDeliveryPaid = resident.isDeliveryPaid();

        DeliverPaymentResult deliverPaymentResult = new DeliverPaymentResult();
        deliverPaymentResult.setAddress(resident.getWalletNum());
        deliverPaymentResult.setCoin("dcnt");
        deliverPaymentResult.setAmount(1);
        deliverPaymentResult.setAmountLeft(1);
        mPaymentModelView = new PaymentModelView(deliverPaymentResult);
    }

    public PayDeliverySettingsModelView(DeliverPaymentResult deliverPaymentResult, Resident resident) {
        this.isInvited = resident.isInvited();
        this.isActive = resident.isActive() || resident.getStep() > 4;
        this.isDeliveryPaid = resident.isDeliveryPaid();

        mPaymentModelView = new PaymentModelView(deliverPaymentResult);
    }

    public PayDeliverySettingsModelView(Resident m0, Passport m1) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.isActive();
        this.isDeliveryPaid = m0.isDeliveryPaid();

        Delivery delivery = m1.getDelivery();
        mDeliverDataModelView = new DeliveryDataModelView(delivery);
    }

    public boolean isInvited() {
        return this.isInvited;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }

    public DeliveryDataModelView getDeliverDataModelView() {
        return mDeliverDataModelView;
    }

    public PaymentModelView getPaymentModelView() {
        return mPaymentModelView;
    }
}
