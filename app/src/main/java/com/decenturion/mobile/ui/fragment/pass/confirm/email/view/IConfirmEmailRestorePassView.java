package com.decenturion.mobile.ui.fragment.pass.confirm.email.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.model.ConfirmEmailRestorePassModelView;

public interface IConfirmEmailRestorePassView extends IScreenFragmentView {

    void onBindViewData(@NonNull ConfirmEmailRestorePassModelView model);
}
