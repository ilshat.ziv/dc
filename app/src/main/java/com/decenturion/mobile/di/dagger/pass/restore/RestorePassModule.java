package com.decenturion.mobile.di.dagger.pass.restore;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.business.restore.RestorePassInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.restore.presenter.IRestorePassPresenter;
import com.decenturion.mobile.ui.fragment.pass.restore.presenter.RestorePassPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class RestorePassModule {

    @Provides
    @RestorePassScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @RestorePassScope
    IRestorePassInteractor provideIRestorePassInteractor(IResidentClient iResidentClient,
                                                         IResidentRepository iResidentRepository) {
        return new RestorePassInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @RestorePassScope
    IRestorePassPresenter provideIRestorePassPresenter(IRestorePassInteractor iRestorePassInteractor) {
        return new RestorePassPresenter(iRestorePassInteractor);
    }
}
