package com.decenturion.mobile.di.dagger.seller.traids;

import com.decenturion.mobile.ui.fragment.seller.trade.view.SellerTraidsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SellerTraidsModule.class
})

@SellerTraidsScope
public interface SellerTraidsComponent {

    void inject(SellerTraidsFragment view);
}
