package com.decenturion.mobile.ui.fragment.referral.invite.create.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;

public interface ICreateInviteView extends IScreenFragmentView {

    void onCreateInviteSuccess(@NonNull String url);
}
