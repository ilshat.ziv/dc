package com.decenturion.mobile.di.dagger.ministry.product;

import com.decenturion.mobile.ui.fragment.ministry.product.view.MinistryExternalProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.honorary.view.MinistryHonoraryProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.product.view.MinistryInternalProductFragment;
import com.decenturion.mobile.ui.fragment.ministry.senator.view.MinistrySenatorProductFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        MinistryProductModule.class
})

@MinistryProductScope
public interface MinistryProductComponent {

    void inject(MinistryInternalProductFragment view);

    void inject(MinistrySenatorProductFragment view);

    void inject(MinistryHonoraryProductFragment view);

    void inject(MinistryExternalProductFragment view);
}
