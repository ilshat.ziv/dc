package com.decenturion.mobile.di.dagger.ministry.product;

import android.content.Context;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.ministry.product.IMinistryProductInteractor;
import com.decenturion.mobile.business.ministry.product.MinistryProductInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.ministry.IMinistryClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.ministry.honorary.presenter.IMinistryHonoraryPresenter;
import com.decenturion.mobile.ui.fragment.ministry.honorary.presenter.MinistryHonoraryPresenter;
import com.decenturion.mobile.ui.fragment.ministry.product.presenter.IMinistryProductPresenter;
import com.decenturion.mobile.ui.fragment.ministry.product.presenter.MinistryProductPresenter;
import com.decenturion.mobile.ui.fragment.ministry.senator.presenter.IMinistrySenatorPresenter;
import com.decenturion.mobile.ui.fragment.ministry.senator.presenter.MinistrySenatorPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MinistryProductModule {

    @Provides
    @MinistryProductScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @MinistryProductScope
    IMinistryProductInteractor provideIMinistryProductInteractor(IMinistryClient iMinistryClient,
                                             IResidentRepository iResidentRepository) {

        return new MinistryProductInteractor(iMinistryClient, iResidentRepository);
    }

    @Provides
    @MinistryProductScope
    IMinistryProductPresenter provideIMinistryProductPresenter(Context context,
                                                               IMinistryProductInteractor iMinistryProductInteractor) {
        return new MinistryProductPresenter(context, iMinistryProductInteractor);
    }

    @Provides
    @MinistryProductScope
    IMinistrySenatorPresenter provideIMinistrySenatorPresenter(Context context,
                                                               IMinistryProductInteractor iMinistryProductInteractor) {
        return new MinistrySenatorPresenter(context, iMinistryProductInteractor);
    }

    @Provides
    @MinistryProductScope
    IMinistryHonoraryPresenter provideIMinistryHonoraryPresenter(Context context,
                                                                IMinistryProductInteractor iMinistryProductInteractor) {
        return new MinistryHonoraryPresenter(context, iMinistryProductInteractor);
    }
}
