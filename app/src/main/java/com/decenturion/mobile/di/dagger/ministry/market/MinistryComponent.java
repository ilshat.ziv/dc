package com.decenturion.mobile.di.dagger.ministry.market;

import com.decenturion.mobile.ui.fragment.ministry.market.view.MinistryFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        MinistryModule.class
})

@MinistryScope
public interface MinistryComponent {

    void inject(MinistryFragment view);
}
