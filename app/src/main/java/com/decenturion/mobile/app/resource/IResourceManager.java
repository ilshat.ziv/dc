package com.decenturion.mobile.app.resource;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.AnimRes;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.view.animation.Animation;

public interface IResourceManager {

    int getColor(@ColorRes int idResource);

    ColorStateList getColorStateList(@ColorRes int idResource);

    float getDimension(@DimenRes int idResource);

    String getString(@StringRes int idResource);

    int getInteger(@IntegerRes int idResource);

    Drawable getDrawable(@DrawableRes int idResource);

    Bitmap getBitmapFDrawer(@DrawableRes int idResource);

    Animation getAnimation(@AnimRes int idResource);

    String getResourceName(int resourceId);
}
