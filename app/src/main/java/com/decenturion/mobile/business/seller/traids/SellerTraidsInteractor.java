package com.decenturion.mobile.business.seller.traids;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.trades.TradesResponse;
import com.decenturion.mobile.network.response.trades.TradesResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModel;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModelView;

import rx.Observable;

public class SellerTraidsInteractor implements ISellerTraidsInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IResidentRepository mIResidentRepository;
    private ITokenRepository mITokenRepository;

    public SellerTraidsInteractor(IResidentClient IResidentClient,
                                  ITokenClient iTokenClient,
                                  IResidentRepository iResidentRepository,
                                  ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIResidentRepository = iResidentRepository;
        mITokenRepository = iTokenRepository;
    }

    @Override
    public Observable<SellerTraideModelView> getTraidsModelView(@NonNull String sellerUUID) {
        return mITokenClient.getDeals(
                sellerUUID,
                0,
                999999
        )
                .map(TradesResponse::getResult)
                .map(TradesResult::getWrapperTradeList)
                .flatMapIterable(list -> list)
                .filter(this::isNotCancelled)
                .map(SellerTraideModel::new)
                .toList()
                .map(SellerTraideModelView::new);
    }

    @NonNull
    private Boolean isNotCancelled(WrapperTrade d) {
        Trade trade = d.getTrade();
        String status = trade.getStatus();
        return !TextUtils.equals(status, "cancelled");
    }
}
