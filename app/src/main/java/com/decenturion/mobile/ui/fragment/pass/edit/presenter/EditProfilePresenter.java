package com.decenturion.mobile.ui.fragment.pass.edit.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.edit.IEditProfileInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.EditProfileModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.Sex;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.view.IEditProfileView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class EditProfilePresenter extends Presenter<IEditProfileView> implements IEditProfilePresenter {

    private Context mContext;
    private IEditProfileInteractor mIEditProfileInteractor;
    private ILocalistionManager mILocalistionManager;

    private EditProfileModelView mEditProfileModelView;
    private CountryModelView mCountryModelView;
    private SexModelView mSexModelView;

    public EditProfilePresenter(Context context,
                                IEditProfileInteractor iEditProfileInteractor,
                                ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mContext = context;
        mIEditProfileInteractor = iEditProfileInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEditProfileModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<EditProfileModelView> obs = mIEditProfileInteractor.getProfileInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EditProfileModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(EditProfileModelView model) {
        mEditProfileModelView = model;

        ArrayList<Sex> sexData = initGenderData();
        mSexModelView = new SexModelView(sexData);

        ArrayList<Country> list = initCountryData();
        mCountryModelView = new CountryModelView(list);

        if (mView != null) {
            mView.onBindViewData(mSexModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    @NonNull
    private ArrayList<Sex> initGenderData() {
        ArrayList<Sex> sexData = new ArrayList<>();
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_male), "male"));
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_female), "female"));
        return sexData;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveChanges(@NonNull String firstName,
                            @NonNull String secondName,
                            long birth,
                            @NonNull String sex,
                            @NonNull String country,
                            @NonNull String city) {
        showProgressView();

        Observable<UpdatePassportResponse> obs = mIEditProfileInteractor.saveChanges(firstName,
                secondName,
                birth,
                sex,
                country,
                city)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UpdatePassportResponse>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(UpdatePassportResponse response) {
        hideProgressView();
        if (mView != null) {
            mView.saveChangesSuccess();
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    @Override
    public void uploadPhoto(@NonNull Bitmap photo) {
        showProgressView();

        Observable<UploadPhotoResponse> obs = mIEditProfileInteractor.uploadPhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UploadPhotoResponse>) this::uploadPhotoSuccess,
                this::uploadPhotoFailure
        );
    }

    private void uploadPhotoFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void uploadPhotoSuccess(UploadPhotoResponse uploadPhotoResponse) {
        hideProgressView();
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

}
