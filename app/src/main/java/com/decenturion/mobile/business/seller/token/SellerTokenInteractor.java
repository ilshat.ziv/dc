package com.decenturion.mobile.business.seller.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.market.MarketTokensResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModel;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;

import rx.Observable;

public class SellerTokenInteractor implements ISellerTokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IResidentRepository mIResidentRepository;
    private IWalletRepository mIWalletRepository;
    private ITokenRepository mITokenRepository;

    public SellerTokenInteractor(IResidentClient IResidentClient,
                                 ITokenClient iTokenClient,
                                 IResidentRepository iResidentRepository,
                                 IWalletRepository iWalletRepository,
                                 ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIResidentRepository = iResidentRepository;
        mIWalletRepository = iWalletRepository;
        mITokenRepository = iTokenRepository;
    }

    @Override
    public Observable<TokenModelView> getTokenModelView(@NonNull String residentUUID) {
        return mITokenClient.getBuyTokens(residentUUID)
                .map(MarketTokensResponse::getResult)
                .flatMapIterable(d -> d)
                .map(d -> {
                    OrmTokenModel model = new OrmTokenModel(d);
                    return new TokenModel(model, d.getStartup().getLogo(), null);
                })
                .toList()
                .map(TokenModelView::new);
    }
}
