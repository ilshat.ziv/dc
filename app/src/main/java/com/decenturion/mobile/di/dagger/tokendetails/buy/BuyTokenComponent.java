package com.decenturion.mobile.di.dagger.tokendetails.buy;

import com.decenturion.mobile.ui.fragment.token.buy.view.BuyTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        BuyTokenModule.class
})

@BuyTokenScope
public interface BuyTokenComponent {

    void inject(BuyTokenFragment view);
}
