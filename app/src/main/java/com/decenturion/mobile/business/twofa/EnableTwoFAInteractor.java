package com.decenturion.mobile.business.twofa;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.EnableGfaModel;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;
import com.decenturion.mobile.ui.fragment.twofa.model.EnableTwoFAModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class EnableTwoFAInteractor implements IEnableTwoFAInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public EnableTwoFAInteractor(IResidentClient IResidentClient,
                                 IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<EnableTwoFAModelView> getInfo() {
        return Observable.just(new EnableTwoFAModelView());
    }

    @Override
    public Observable<Boolean> saveChanges(String secretString, String secretCode) {
        EnableGfaModel model = new EnableGfaModel();
        model.setSecret(secretString);
        model.setCode(secretCode);

        return mIResidentClient.enableG2fa(model)
                .map(d -> true)
                .doOnNext(this::updateGfaState);
    }

    private void updateGfaState(Boolean d) {
        try {
            OrmResidentModel residentInfo = mIResidentRepository.getResidentInfo();
            residentInfo.setG2fa(d);
            mIResidentRepository.saveResidentInfo(residentInfo);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
