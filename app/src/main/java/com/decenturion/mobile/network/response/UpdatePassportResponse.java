package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdatePassportResponse extends BaseResponse {

    @JsonProperty("result")
    private UpdatePassportResult mResult;

    public UpdatePassportResponse() {
    }

    @JsonIgnore
    public UpdatePassportResult getResult() {
        return mResult;
    }
}
