package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Action {

    @JsonProperty("action")
    private String mAction;

    @JsonProperty("browser")
    private String mBrowser;

    @JsonProperty("date")
    private String mDate;

    @JsonProperty("ip")
    private String mIp;

    public Action() {
    }

    @JsonIgnore
    public String getAction() {
        return mAction;
    }

    @JsonIgnore
    public String getBrowser() {
        return mBrowser;
    }

    @JsonIgnore
    public String getDate() {
        return mDate;
    }

    @JsonIgnore
    public String getIp() {
        return mIp;
    }
}
