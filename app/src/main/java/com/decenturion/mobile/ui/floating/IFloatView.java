package com.decenturion.mobile.ui.floating;

public interface IFloatView {

    void onFloatIn();

    void onFloatOut();
}
