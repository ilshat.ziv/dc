package com.decenturion.mobile.di.dagger.invite;


import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.invite.IInviteInteractor;
import com.decenturion.mobile.business.invite.InviteInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.invite.presenter.IInvitePresenter;
import com.decenturion.mobile.ui.fragment.invite.presenter.InvitePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class InviteModule {

    @Provides
    @InviteScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @InviteScope
    IInviteInteractor provideIInviteInteractor(IResidentClient iResidentClient,
                                                    IResidentRepository iResidentRepository) {
        return new InviteInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @InviteScope
    IInvitePresenter provideIInvitePresenter(IInviteInteractor iInviteInteractor) {
        return new InvitePresenter(iInviteInteractor);
    }
}
