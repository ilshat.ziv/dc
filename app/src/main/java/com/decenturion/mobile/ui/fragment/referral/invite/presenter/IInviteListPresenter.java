package com.decenturion.mobile.ui.fragment.referral.invite.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;
import com.decenturion.mobile.ui.fragment.referral.invite.view.IInviteListView;

public interface IInviteListPresenter extends IPresenter<IInviteListView> {

    void bindViewData();

    void onBottomScrolled();

    void removeInvite(int inviteId);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
