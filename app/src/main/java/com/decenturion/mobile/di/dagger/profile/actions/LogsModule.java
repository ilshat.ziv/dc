package com.decenturion.mobile.di.dagger.profile.actions;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.profile.main.ILogsInteractor;
import com.decenturion.mobile.business.profile.main.LogsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.log.presenter.ILogsPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.log.presenter.LogsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LogsModule {

    @Provides
    @LogsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @LogsScope
    ILogsInteractor provideILogsInteractor(IResidentClient IResidentClient,
                                           IResidentRepository iResidentRepository,
                                           IPrefsManager iPrefsManager,
                                           ILocalistionManager iLocalistionManager) {
        return new LogsInteractor(IResidentClient, iResidentRepository, iPrefsManager, iLocalistionManager);
    }

    @Provides
    @LogsScope
    ILogsPresenter provideILogsPresenter(ILogsInteractor iLogsInteractor) {
        return new LogsPresenter(iLogsInteractor);
    }
}
