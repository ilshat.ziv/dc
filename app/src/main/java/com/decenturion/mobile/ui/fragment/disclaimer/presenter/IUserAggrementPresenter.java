package com.decenturion.mobile.ui.fragment.disclaimer.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.disclaimer.view.IUserAggrementView;

public interface IUserAggrementPresenter extends IPresenter<IUserAggrementView> {

    void bindViewData();

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
