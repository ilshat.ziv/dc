package com.decenturion.mobile.network.response.resident;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResidentResponse extends BaseResponse {

    @JsonProperty("result")
    private ResidentResult mSigninResult;

    public ResidentResponse() {
    }

    @JsonIgnore
    public ResidentResult getResult() {
        return mSigninResult;
    }
}
