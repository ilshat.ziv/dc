package com.decenturion.mobile.ui.fragment.settings.action.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.action.ActionListSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;
import com.decenturion.mobile.ui.fragment.settings.action.LogsAdapter;
import com.decenturion.mobile.ui.fragment.settings.action.presenter.IActionListSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ActionListSettingsFragment extends BaseFragment implements IActionListSettingsView,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener{

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.actionListView)
    BaseRecyclerView mActionLogView;
    private LogsAdapter mActionLogAdapter;

    /* DI */

    @Inject
    IActionListSettingsPresenter mIActionListSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ActionListSettingsComponent LogsComponent = appDelegate
                .getIDIManager()
                .plusActionListSettingsComponent();
        LogsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_action_log_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mRefreshView.setOnRefreshListener(this);
        mActionLogView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mActionLogView.setLayoutManager(new LinearLayoutManager(context));
        mActionLogAdapter = new LogsAdapter(context);
        mActionLogView.setAdapter(mActionLogAdapter);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_actionlog_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIActionListSettingsPresenter.bindView(this);
        mIActionListSettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIActionListSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIActionListSettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIActionListSettingsPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.killAllSessionsView)
    protected void onKillAllSessions() {
        mIActionListSettingsPresenter.killAllSessions();
    }

    @Override
    public void onBindViewData(@NonNull LogModelView model) {
        mActionLogAdapter.replaceDataSet(model.getActionList());
    }

    @Override
    public void logoutSuccess() {
        FragmentActivity activity = requireActivity();
        activity.startActivity(new Intent(activity, SignActivity.class));
        activity.finishAffinity();
    }

    @Override
    public void onRefresh() {
        mRefreshView.hide();
        mIActionListSettingsPresenter.bindViewData();
    }

    @Override
    public void onBottomScrolled() {
        mIActionListSettingsPresenter.onBottomScrolled();
    }
}
