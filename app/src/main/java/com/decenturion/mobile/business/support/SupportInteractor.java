package com.decenturion.mobile.business.support;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.support.model.SupportMessage;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.SupportModel;

import rx.Observable;

public class SupportInteractor implements ISupportInteractor {

    private IResidentClient mIResidentClient;

    public SupportInteractor(IResidentClient IResidentClient) {
        mIResidentClient = IResidentClient;
    }

    @Override
    public Observable<Boolean> sendMessage(@NonNull SupportMessage model) {
        return mIResidentClient.sendSupport(new SupportModel(model))
                .map(d -> true);
    }
}
