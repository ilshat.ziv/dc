package com.decenturion.mobile.business.transaction;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.trades.TradesResponse;
import com.decenturion.mobile.network.response.trades.TradesResult;
import com.decenturion.mobile.network.response.traide.state.StateTraideResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.ui.fragment.transaction.model.TransactionInfoModelView;

import rx.Observable;

public class TransactionInfoInteractor implements ITransactionInfoInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IResidentRepository mIResidentRepository;
    private IWalletRepository mIWalletRepository;
    private ITokenRepository mITokenRepository;
    private ITraideRepository mITraideRepository;

    public TransactionInfoInteractor(IResidentClient IResidentClient,
                                     ITokenClient iTokenClient,
                                     IResidentRepository iResidentRepository,
                                     IWalletRepository iWalletRepository,
                                     ITokenRepository iTokenRepository,
                                     ITraideRepository iTraideRepository) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIResidentRepository = iResidentRepository;
        mIWalletRepository = iWalletRepository;
        mITokenRepository = iTokenRepository;
        mITraideRepository = iTraideRepository;
    }

    @Override
    public Observable<TransactionInfoModelView> getTransactionInfo(@NonNull String traidUUID) {
        Observable<TransactionInfoModelView> obs1 = Observable.just(mITraideRepository.getTraide(traidUUID))
                .map(d -> {
                    if (d != null) {
                        return new TransactionInfoModelView(d);
                    } else {
                        return null;
                    }
                });

        Observable<TransactionInfoModelView> obs2 = getTraidInfoFromServer2(traidUUID);

        return Observable.concat(obs1, obs2)
                .takeFirst(this::isTraidValid);

//        return mITokenClient.getDealsByTradeUUID(traidUUID)
//                .map(TradesResponse::getTransactionList)
//                .map(TradesResult::getWrapperTradeList)
//                .map(d -> d.get(0))
//                .map(TransactionInfoModelView::new);
    }

    @NonNull
    private Observable<TransactionInfoModelView> getTraidInfoFromServer(@NonNull String traidUUID) {
        return mITokenClient.getTraide(traidUUID)
                    .map(StateTraideResponse::getResult)
                    .map(TransactionInfoModelView::new);
    }

    @NonNull
    private Observable<TransactionInfoModelView> getTraidInfoFromServer2(@NonNull String traidUUID) {
        return mITokenClient.getDeals("",
                0,
                999999
        )
                .map(TradesResponse::getResult)
                .map(TradesResult::getWrapperTradeList)
                .flatMapIterable(list -> list)
                .filter(d -> {
                    Trade trade = d.getTrade();
                    return TextUtils.equals(trade.getUuid(), traidUUID);
                })
                .map(TransactionInfoModelView::new)
                .toList()
                .map(d -> d.get(0));
    }

    private boolean isTraidValid(TransactionInfoModelView d) {
        return d != null;// && !TextUtils.isEmpty(d.getTransactionStatus());
    }
}
