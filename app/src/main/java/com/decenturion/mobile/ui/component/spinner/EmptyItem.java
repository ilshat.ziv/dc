package com.decenturion.mobile.ui.component.spinner;

import android.support.annotation.NonNull;

public class EmptyItem implements ISpinnerItem {

    private String mHint;

    public EmptyItem() {
    }

    public EmptyItem(@NonNull String hint) {
        mHint = hint;
    }

    @Override
    public String getItemName() {
        return mHint;
    }

    @Override
    public String getItemKey() {
        return null;
    }
}
