package com.decenturion.mobile.database.type.model;

import com.decenturion.mobile.network.response.model.Startup;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StartupTypeModel extends Startup {

    public StartupTypeModel() {
    }

    @JsonIgnore
    public StartupTypeModel(Startup startup) {
        setAlias(startup.getAlias());
        setDescription(startup.getDescription());
        setDescriptions(startup.getDescriptions());
        setDescriptionTitle(startup.getDescriptionTitle());
        setLogo(startup.getLogo());
        setName(startup.getName());
        setPresentation(startup.getPresentation());
        setSmartContract(startup.getSmartContract());
        setTokenAmount(startup.getTokenAmount());
        setTokenName(startup.getTokenName());
        setTokenSymbol(startup.getTokenSymbol());
        setUrl(startup.getUrl());
        setUuid(startup.getUuid());
        setWhitepaper(startup.getWhitepaper());
        setYoutubeId(startup.getYoutubeId());
        setTokenAmountText(startup.getTokenAmountText());
    }
}
