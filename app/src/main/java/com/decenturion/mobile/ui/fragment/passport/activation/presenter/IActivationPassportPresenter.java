package com.decenturion.mobile.ui.fragment.passport.activation.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.passport.activation.view.IActivationPassportView;

public interface IActivationPassportPresenter extends IPresenter<IActivationPassportView> {

    void bindViewData();

    void getPaymentAddress(@NonNull String coin);
}
