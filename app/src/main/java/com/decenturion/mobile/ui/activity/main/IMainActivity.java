package com.decenturion.mobile.ui.activity.main;

import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;

public interface IMainActivity extends ISingleFragmentActivity {

    int GET_ADDRESS_CODE = 302;
    int REQUEST_IMAGE_CAPTURE = 403;
    int REQUEST_CAMERA_PERMISSION = 404;
    int ACTION_IMAGE_FROM_GALARY = 405;
    int ACTION_IMAGE_FROM_CAMERA = 406;
    int ACTION_IMAGE_FROM_CAMERA_AND_CROP = 407;
    int ACTION_IMAGE_FROM_CAMERA_BEFORE_CROP = 408;
    int ACTION_IMAGE_FROM_GALARY_BEFORE_CROP = 409;
    int ACTION_IMAGE_FROM_GALARY_AND_CROP = 411;
    int ACTION_IMAGE_AFTER_CROP = 410;
    int ACTION_USER_AGGREEMENT = 412;
    int ACTION_ENABLE_TWOFA = 413;
    int ACTION_CREATE_INVITE = 414;
    int ACTION_REMOVE_INVITE = 415;
}
