package com.decenturion.mobile.ui.fragment.settings.passport.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class Sex implements ISpinnerItem {

    private String mName;
    private String mKey;

    public Sex(String name) {
        mName = name;
    }

    public Sex(String name, String key) {
        mName = name;
        mKey = key;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String getItemName() {
        return mName;
    }

    @Override
    public String getItemKey() {
        return mKey;
    }
}
