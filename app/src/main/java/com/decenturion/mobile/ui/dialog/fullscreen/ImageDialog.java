package com.decenturion.mobile.ui.dialog.fullscreen;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.utils.LoggerUtils;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoView;

public class ImageDialog extends FullScreenDialogFragment {

    private static String mUrlImage;
    private static Bitmap mBitmap;

    /* UI */

    @Nullable
    @BindView(R.id.imageView)
    PhotoView mPhotoView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    public static void show(@NonNull FragmentManager fm, Bitmap bitmap, String url) {
        mUrlImage = url;
        mBitmap = bitmap;
        final ImageDialog dialog = new ImageDialog();
        dialog.show(fm, ImageDialog.class.getName());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        AppComponent component = appDelegate
                .getIDIManager()
                .getAppComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_image);
        setFullVisual(true);
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_photo_title));
        setEnabledPositiveClickButton(false);

        ButterKnife.bind(this, view);

        if (!TextUtils.isEmpty(mUrlImage)) {
            Picasso.with(getContext())
                    .load(mUrlImage)
                    .into(mPhotoView);
        } else if (mBitmap != null) {

            try {
                //https://stackoverflow.com/questions/3647993/android-bitmaps-loaded-from-gallery-are-rotated-in-imageview
                //http://sylvana.net/jpegcrop/exif_orientation.html
//                ExifInterface exif = new ExifInterface(filePath);
//                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                int orientation = 1;

                switch (orientation) {
                    case 7 :
                    case 8 : {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(-90);
                        mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
                        break;
                    }
                    case 4 :
                    case 3 : {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(180);
                        mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
                        break;
                    }
                    case 5 :
                    case 6 : {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
                        break;
                    }
                }
            } catch (Exception e) {
                LoggerUtils.exception(e);;
            }

            mPhotoView.setImageBitmap(mBitmap);
        } else {
            dismiss();
            return view;
        }

        return view;
    }

    @Override
    public void onPositiveClickButton() {
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mBitmap = null;
        mUrlImage = null;
    }
}