package com.decenturion.mobile.ui.fragment.transaction.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.transaction.model.TransactionInfoModelView;

public interface ITransactionInfoView extends IScreenFragmentView {

    void onBindViewData(@NonNull TransactionInfoModelView model);
}
