package com.decenturion.mobile.di.dagger.app;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.database.OrmHelper;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {

    private OrmHelper mOrmHelper;

    public DbModule(@NonNull Context context) {
        mOrmHelper = OpenHelperManager.getHelper(context, OrmHelper.class);
    }

    @Singleton
    @Provides
    public OrmHelper getOrmHelper() {
        mOrmHelper.getWritableDatabase();
        return mOrmHelper;
    }

//    @Provides
//    SponsorDAO provideSponsorDAO() {
//        return getWritableDataBase().getSponsorDAO();
//    }

}
