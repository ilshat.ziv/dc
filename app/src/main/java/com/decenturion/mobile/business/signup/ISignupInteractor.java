package com.decenturion.mobile.business.signup;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.network.response.signup.SignupResult;

import rx.Observable;

public interface ISignupInteractor {

    Observable<SignupResult> signup(@NonNull String username,
                                    @NonNull String password,
                                    @NonNull String retypePassword,
                                    @NonNull String captcha,
                                    boolean isSubscribe,
                                    @Nullable String inviteCode);

    Observable<String> checkInviteToken(@NonNull String inviteToken);

    Observable<Boolean> activeReferallCode(@NonNull String referallKey);

    Observable<Boolean> activePayReferralCode(@NonNull String referralKey);

    Observable<Boolean> activeBannerReferralCode(@NonNull String referralKey);
}
