package com.decenturion.mobile.ui.fragment.score.send.trade.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedCheckBox;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.score.trade.TradeScoreSendTokenComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.twofa.EnableTwoFAActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.dialog.fullscreen.token.ScoreSendTokenSuccessDialog;
import com.decenturion.mobile.ui.dialog.fullscreen.token.SendTokenFailDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.score.send.trade.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendBalanceModel;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendOptionModel;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendTokenModelView;
import com.decenturion.mobile.ui.fragment.score.send.trade.presenter.ITradeScoreSendTokenPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class TradeScoreSendTokenFragment extends BaseFragment implements ITradeScoreSendTokenView {

    public static final String TOKEN_ID = "TOKEN_ID";

    private int mTokenId;
    private String mTokenName;

    /* UI */

    @BindView(R.id.nameTokenView)
    TextView mNameTokenView;

    @BindView(R.id.amountView)
    BuyTokenView mAmountView;


    @BindView(R.id.optionSendInfoView)
    LinearLayout mOptionSendInfoView;

    @BindView(R.id.addressReceiveValueView)
    TextView mAddressReceiveValueView;

    @BindView(R.id.gasValueView)
    TextView mGasValueView;

    @BindView(R.id.balanceLabelView)
    TextView mBalanceLabelView;

    @BindView(R.id.balanceValueView)
    TextView mBalanceValueView;


    @BindView(R.id.methodSendView)
    LinearLayout mMethodSendView;

    @BindView(R.id.simpleMethodSendTextView)
    LocalizedTextView mSimpleMethodSendTextView;


    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.expressMethodSendButtonView)
    LocalizedCheckBox mExpressMethodSendButtonView;

    @BindView(R.id.sendTokensButtonView)
    LocalizedButtonView mSendTokensButtonView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    ITradeScoreSendTokenPresenter mISendTokenPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TradeScoreSendTokenComponent component = appDelegate
                .getIDIManager()
                .plusTradeScoreSendTokenComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_score_trade_token_send, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mAmountView.showLabel();
        mAmountView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_amount_title));
        mAmountView.setHint(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_amount_placeholder));
        mAmountView.editMode();
        mAmountView.setVisibleDivider(true);

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel("Google Authenticator");
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_send_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mExpressMethodSendButtonView.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_express));
            } else {
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_normal));
            }
        });

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_title));
        iToolbarController.setNavigationOnClickListener(v -> {
            ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity1.getINavigationManager();
            iNavigationManager.navigateToBack();
        });
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISendTokenPresenter.bindView(this);
        mISendTokenPresenter.bindViewData(mTokenId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISendTokenPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISendTokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISendTokenPresenter.unbindView();
        mISendTokenPresenter.onSaveInstanceState(outState,
                mAmountView.getValue(),
                mSecretCodeView.getValue()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISendTokenPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull TradeScoreSendTokenModelView model) {
        mTokenName = model.getTokenName();

        String string = mILocalistionManager.getLocaleString(R.string.app_score_trade_send_subtitle);
        Spanned subtitle = Html.fromHtml(string.replace("{fullname}", model.getTokenName()));
        mNameTokenView.setText(subtitle);

        mAmountView.setText(model.getAmount());
        mAmountView.setCoin(model.getSymbol());
        mSecretCodeView.setText(model.getTwoFA());

        switch (model.getTypeSend()) {
            case URGENT: {
                mExpressMethodSendButtonView.setChecked(true);
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_express));
            }
            default:{
                mExpressMethodSendButtonView.setChecked(false);
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_trade_send_normal));
            }
        }

        TradeScoreSendOptionModel model2 = model.getTradeScoreSendOptionModel();
        long date = model2.getFreeCoolDown();
        if (date > System.currentTimeMillis()) {
            String dateFormat = DateTimeUtils.getDateFormat(date, DateTimeUtils.Format.DEAL_DATETIME);
            String string1 = mILocalistionManager.getLocaleString(R.string.app_score_trade_send_normal_warning);
            mSimpleMethodSendTextView.setText(string1.replace("{datetime}", dateFormat));
            mSimpleMethodSendTextView.setVisibility(View.VISIBLE);
            mExpressMethodSendButtonView.setChecked(true);
            mExpressMethodSendButtonView.setEnabled(false);
        }

        // TOEXTERNAL
        TradeScoreSendBalanceModel balanceModel = model.getTradeScoreSendBalanceModel();
        mAddressReceiveValueView.setText(balanceModel.getAddress());
        String string1 = mILocalistionManager.getLocaleString(R.string.app_score_trade_send_balance_title);
        mBalanceLabelView.setText(string1.replace("{currency}", balanceModel.getCoin()));

        String balance = balanceModel.getBalance() + " " + balanceModel.getCoin();
        mBalanceValueView.setText(balance);

        String gas = model2.getGas() + " " + model2.getCurrence();
        mGasValueView.setText(gas);
    }

    @OnClick(R.id.sendTokensButtonView)
    protected void onSendTokens() {
//        if (mExpressMethodSendButtonView.isChecked()) {
//            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
//            INavigationManager iNavigationManager = activity.getINavigationManager();
//            Bundle bundle = new Bundle();
//            bundle.putInt(TOKEN_ID, mTokenId);
//            iNavigationManager.navigateTo(ExpressSendTokenFragment.class, true, bundle);
//        } else {
        mISendTokenPresenter.sendToken(
                mAmountView.getValue(),
                mExpressMethodSendButtonView.isChecked() ? MethodSendToken.URGENT : MethodSendToken.NORMAL,
                mSecretCodeView.getValue()
        );
//        }
    }

    @Override
    public void onEnableTwoFA() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, EnableTwoFAActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_ENABLE_TWOFA);
    }

    @Override
    public void onSendTokenSuccess() {
        Bundle bundle = new Bundle();
        bundle.putInt(ScoreSendTokenSuccessDialog.TYPE_TRANSACTION, 3);
        bundle.putString(ScoreSendTokenSuccessDialog.COIN, mTokenName);
        bundle.putString(ScoreSendTokenSuccessDialog.AMOUNT, mAmountView.getValue());
        ScoreSendTokenSuccessDialog.show(requireFragmentManager(), bundle);
    }

    @Override
    public void onSendTokenFail() {
        SendTokenFailDialog.show(requireFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_ENABLE_TWOFA && resultCode == Activity.RESULT_OK) {
            onSendTokens();
        }
    }
}
