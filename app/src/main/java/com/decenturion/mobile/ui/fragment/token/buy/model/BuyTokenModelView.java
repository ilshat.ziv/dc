package com.decenturion.mobile.ui.fragment.token.buy.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.response.model.TokenCoin;
import com.decenturion.mobile.network.response.traide.sale.OnSaleResultResult;

public class BuyTokenModelView {

    private int mTokenId;

    private String mNumberOfTokens;
    private String mOnSale;
    private String mPriceFor1;
    private String mTotal;

    private String mCoin;
    private String mCategory;
    private String mUUID;
    private String mResidentUUID;
    private String mCyrrency;

    private boolean isInternalTransaction;
    private String mReceiveAddress;
    private boolean isAgreeTermsOfuse;
    private boolean isGas;

    public BuyTokenModelView() {
    }

    public BuyTokenModelView(int tokenId, String cyrrency) {
        mTokenId = tokenId;
        mCyrrency = cyrrency;
    }


    public BuyTokenModelView(@NonNull String sellerUUID, int tokenId, @NonNull String cyrrency) {
        mResidentUUID = sellerUUID;
        mTokenId = tokenId;
        mCyrrency = cyrrency;
    }

    public BuyTokenModelView(@NonNull OrmTokenModel model) {
        model.getId();
    }

    public BuyTokenModelView(@NonNull OnSaleResultResult d) {
        TokenCoin coin = d.getCoin();
        String symbol = coin.getSymbol();

        mOnSale = d.getOnsaleAmount() + " " + d.getCurrency();
        mPriceFor1 = d.getPrice() + " " + d.getCurrency();
        mTotal = d.getTotalPrice() + " " + d.getCurrency();
        mNumberOfTokens = d.getAmount();

        mCoin = symbol;
        mCategory = d.getCategory();
        mUUID = coin.getUuid();
    }

    public String getNumberOfTokens() {
        return mNumberOfTokens;
    }

    public int getTokenId() {
        return mTokenId;
    }

    public String getOnSale() {
        return mOnSale;
    }

    public String getPriceFor1() {
        return mPriceFor1;
    }

    public String getTotal() {
        return mTotal;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getUUID() {
        return mUUID;
    }

    public String getResidentUUID() {
        return mResidentUUID;
    }

    public String getCyrrency() {
        return mCyrrency;
    }

    public void setTokenId(int tokenId) {
        mTokenId = tokenId;
    }

    public void setNumberOfTokens(String numberOfTokens) {
        mNumberOfTokens = numberOfTokens;
    }

    public void setOnSale(String onSale) {
        mOnSale = onSale;
    }

    public void setPriceFor1(String priceFor1) {
        mPriceFor1 = priceFor1;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

    public void setCoin(String coin) {
        mCoin = coin;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public void setUUID(String UUID) {
        mUUID = UUID;
    }

    public void setResidentUUID(String residentUUID) {
        mResidentUUID = residentUUID;
    }

    public void setCyrrency(String cyrrency) {
        mCyrrency = cyrrency;
    }

    public void setAgreeTermsOfuse(boolean agreeTermsOfuse) {
        isAgreeTermsOfuse = agreeTermsOfuse;
    }

    public boolean isAgreeTermsOfuse() {
        return this.isAgreeTermsOfuse;
    }

    public String getReceiveAddress() {
        return mReceiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        mReceiveAddress = receiveAddress;
    }

    public boolean isInternalTransaction() {
        return isInternalTransaction;
    }

    public void setInternalTransaction(boolean internalTransaction) {
        isInternalTransaction = internalTransaction;
    }

    public void setIsGas(boolean isGas) {
        this.isGas = isGas;
    }
}
