package com.decenturion.mobile.ui.activity.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.snack.ISnackManager;
import com.decenturion.mobile.app.snack.SnackManager;
import com.decenturion.mobile.app.snack.TypeMessage;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.dialog.bottomsheet.PhotoDialogFragment;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.CameraDialog;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.CropImageDialog;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.github.florent37.camerafragment.configuration.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CropImageActivity extends AppCompatActivity implements IScreenActivityView {

    private boolean isStartCropDialog;
    private int mRequestCode;

    ISnackManager mISnackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_crop_image);

        View view = getWindow().getDecorView().findViewById(android.R.id.content);
        mISnackManager = new SnackManager(view);
        if (savedInstanceState != null) {
            return;
        }

        initArgs();

        if (mRequestCode == IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP) {
//            Uri uri = getTempFileUri();
//            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//            startActivityForResult(takePicture, IMainActivity.ACTION_IMAGE_FROM_CAMERA_BEFORE_CROP);
            Configuration configuration = new Configuration.Builder().build();
            CameraDialog.show(getSupportFragmentManager(), configuration, IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);//todo надо написать адаптацию
            startActivityForResult(pickPhoto, IMainActivity.ACTION_IMAGE_FROM_GALARY_BEFORE_CROP);
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null) {
            mRequestCode = intent.getIntExtra("requestCode", -1);
        }
    }

//    @Subscribe
//    public void onActivityResult(@NonNull OnActivityResult event) {
//        onActivityResult(event.getRequestCode(), event.getResultCode(), event.getData());
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        switch (requestCode) {
            case IMainActivity.ACTION_IMAGE_AFTER_CROP: {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
                break;
            }
            case IMainActivity.ACTION_IMAGE_FROM_CAMERA_BEFORE_CROP: {
                isStartCropDialog = true;
                break;
            }
            case IMainActivity.ACTION_IMAGE_FROM_GALARY_BEFORE_CROP: {
                Uri uri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    bitmap = ImageUtils.scaleBitmap(bitmap, 1000);
                    bitmap = ImageUtils.compressBitmap(bitmap, 100);
                    saveBitmap(bitmap);
                } catch (Exception e) {
                    LoggerUtils.exception(e);
                    mISnackManager.showLongSnack(TypeMessage.Error, e.getMessage());
                    setResult(RESULT_CANCELED);
                    finish();
                    return;
                }
                isStartCropDialog = true;
                break;
            }
            case IMainActivity.ACTION_IMAGE_FROM_GALARY_AND_CROP: {
                setResult(RESULT_OK, data);
                finish();
                break;
            }
        }
    }

    private void saveBitmap(Bitmap bitmap) {
        File pictureFile = getTempImageFile();
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            LoggerUtils.exception(e);
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private File getTempImageFile() {
        Context context = getApplicationContext();
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir.getAbsolutePath() + "/" + PhotoDialogFragment.Args.TEMP_FILE_NAME + ".jpg");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (isStartCropDialog) {
            Uri outputUri;
            Uri inputUri;

            Context context = getApplicationContext();
            File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File file = new File(storageDir + "/" + PhotoDialogFragment.Args.TEMP_FILE_NAME + ".jpg");

            inputUri = Uri.fromFile(file);
            outputUri = inputUri;

            Bundle bundle = new Bundle();
//            bundle.putInt(CameraDialog.Args.REQUEST_CODE, mRequestCode);
            bundle.putInt(CameraDialog.Args.REQUEST_CODE, IMainActivity.ACTION_IMAGE_FROM_GALARY);
            bundle.putParcelable("com.yalantis.ucrop.OutputUri", inputUri);
            bundle.putParcelable("com.yalantis.ucrop.InputUri", outputUri);

//            bundle.putInt("com.yalantis.ucrop.StatusBarColor", ContextCompat.getColor(getApplicationContext(), R.color.primary_dark));
//            bundle.putInt("com.yalantis.ucrop.ToolbarColor", ContextCompat.getColor(getApplicationContext(), R.color.primary));
//            bundle.putInt("com.yalantis.ucrop.UcropColorWidgetActive", ContextCompat.getColor(getApplicationContext(), R.color.primary));

//        bundle.putString("com.yalantis.ucrop.CompressionFormatName", String.valueOf(Bitmap.CompressFormat.JPEG));
//        bundle.putString("com.yalantis.ucrop.CompressionFormatName", String.valueOf(Bitmap.CompressFormat.PNG));
            bundle.putInt("com.yalantis.ucrop.CompressionQuality", 100);

//        bundle.putInt("com.yalantis.ucrop.MaxSizeX", Preference.getParam(UtopiaSettings.Key.AVATAR_WIDTH, UtopiaSettings.Default.AVATAR_WIDTH));
            bundle.putInt("com.yalantis.ucrop.MaxSizeX", ClientPrefOptions.Defaults.AVATAR_WIDTH);
//        bundle.putInt("com.yalantis.ucrop.MaxSizeY", Preference.getParam(UtopiaSettings.Key.AVATAR_HEIGHT, UtopiaSettings.Default.AVATAR_HEIGHT));
            bundle.putInt("com.yalantis.ucrop.MaxSizeY", ClientPrefOptions.Defaults.AVATAR_HEIGHT);

            CropImageDialog.show(getSupportFragmentManager(), bundle, IMainActivity.ACTION_IMAGE_FROM_GALARY_AND_CROP);
            isStartCropDialog = false;
        }
    }

    private File createImageFile(@NonNull Context context) {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            File file = new File(storageDir + "/" + PhotoDialogFragment.Args.TEMP_FILE_NAME + ".jpg");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            return file;
        } catch (IOException e) {
            LoggerUtils.exception(e);
        }
        return null;
    }

    public Uri getTempFileUri() {
        File photoFile = createImageFile(getApplicationContext());
        return FileProvider.getUriForFile(this,
                BuildConfig.FILE_PROVIDER,
                photoFile);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        setResult(resultCode, data);
        finish();
    }

    @Override
    public void showMessage(@NonNull String message) {

    }

    @Override
    public void showSuccessMessage(@NonNull String message) {

    }

    @Override
    public void showErrorMessage(@NonNull String message) {

    }

    @Override
    public void showWarningMessage(@NonNull String message) {

    }

    @Override
    public void showProgressView() {

    }

    @Override
    public void hideProgressView() {

    }
}