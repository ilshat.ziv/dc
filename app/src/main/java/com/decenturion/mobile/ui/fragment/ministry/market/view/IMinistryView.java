package com.decenturion.mobile.ui.fragment.ministry.market.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryModelView;

public interface IMinistryView extends IScreenFragmentView {

    void onBindViewData(@NonNull MinistryModelView model);
}
