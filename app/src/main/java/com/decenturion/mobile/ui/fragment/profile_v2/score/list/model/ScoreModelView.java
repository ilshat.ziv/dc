package com.decenturion.mobile.ui.fragment.profile_v2.score.list.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;

public class ScoreModelView {

    private final String mName;
    @DrawableRes
    private final int mIcon;
    private final ScoreType mTypeScore;

    public ScoreModelView(@NonNull String name, @DrawableRes int icon, ScoreType typeScore) {
        mName = name;
        mIcon = icon;
        mTypeScore = typeScore;
    }

    public ScoreType getTypeScore() {
        return mTypeScore;
    }

    public String getName() {
        return mName;
    }

    public int getIcon() {
        return mIcon;
    }
}
