package com.decenturion.mobile.network.response.settings;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateSettingsResponse extends BaseResponse {

    @JsonProperty("result")
    private UpdateSettingsResult mResult;

    public UpdateSettingsResponse() {
    }

    @JsonIgnore
    public UpdateSettingsResult getResult() {
        return mResult;
    }
}
