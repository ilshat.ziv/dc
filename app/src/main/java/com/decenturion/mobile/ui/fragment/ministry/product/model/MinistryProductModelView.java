package com.decenturion.mobile.ui.fragment.ministry.product.model;

public class MinistryProductModelView {

    private String mCategory;
    private String mUsdtPrice;

    private PaymentModelView mPaymentModelView;

    public MinistryProductModelView() {
    }

    public MinistryProductModelView(String category) {
        mCategory = category;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getUsdtPrice() {
        return mUsdtPrice;
    }

    public void setUsdtPrice(String usdtPrice) {
        mUsdtPrice = usdtPrice;
    }

    public void setPaymentModelView(PaymentModelView paymentModelView) {
        mPaymentModelView = paymentModelView;
    }

    public PaymentModelView getPaymentModelView() {
        return mPaymentModelView;
    }
}
