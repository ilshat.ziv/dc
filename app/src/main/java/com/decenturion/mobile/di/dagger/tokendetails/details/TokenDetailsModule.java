package com.decenturion.mobile.di.dagger.tokendetails.details;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.tokendetails.details.ITokenDetailsInteractor;
import com.decenturion.mobile.business.tokendetails.details.TokenDetailsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.token.details.presenter.ITokenDetailsPresenter;
import com.decenturion.mobile.ui.fragment.token.details.presenter.TokenDetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TokenDetailsModule {

    @Provides
    @TokenDetailsScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TokenDetailsScope
    ITokenDetailsInteractor provideITokenInteractor(IResidentClient IResidentClient,
                                                    ITokenRepository ITokenRepository,
                                                    ITokenClient iTokenClient,
                                                    ILocalistionManager iLocalistionManager) {

        return new TokenDetailsInteractor(IResidentClient, ITokenRepository, iTokenClient,
                iLocalistionManager);
    }

    @Provides
    @TokenDetailsScope
    ITokenDetailsPresenter provideITokenDetailsPresenter(ITokenDetailsInteractor iTokenDetailsInteractor) {
        return new TokenDetailsPresenter(iTokenDetailsInteractor);
    }
}
