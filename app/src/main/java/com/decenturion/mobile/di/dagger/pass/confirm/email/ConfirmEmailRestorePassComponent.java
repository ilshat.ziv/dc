package com.decenturion.mobile.di.dagger.pass.confirm.email;

import com.decenturion.mobile.ui.fragment.pass.confirm.email.view.ConfirmEmailRestorePassFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ConfirmEmailRestorePassModule.class
})

@ConfirmEmailRestorePassScope
public interface ConfirmEmailRestorePassComponent {

    void inject(ConfirmEmailRestorePassFragment view);
}
