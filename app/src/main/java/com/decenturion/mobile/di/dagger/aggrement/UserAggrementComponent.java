package com.decenturion.mobile.di.dagger.aggrement;

import com.decenturion.mobile.ui.fragment.disclaimer.view.UserAggrementFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        UserAggrementModule.class
})

@UserAggrementScope
public interface UserAggrementComponent {

    void inject(UserAggrementFragment view);
}
