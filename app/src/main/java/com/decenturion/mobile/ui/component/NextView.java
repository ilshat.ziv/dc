package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class NextView extends UBaseView {

    @Nullable
    @BindView(R.id.valueView)
    TextView mValueView;

    public NextView(Context context) {
        super(context);
    }

    public NextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/Canada Type - VoxRound-Light.otf");
            mValueView.setTypeface(typeface);
        }
    }

    public void setHint(String hint) {
        init();
        mValueView.setHint(String.valueOf(hint));
    }

    public void setText(String text) {
        init();
        mValueView.setText(text != null ? String.valueOf(text) : "");

    }

    @Override
    public void setMustFill(boolean isMust) {

    }
}