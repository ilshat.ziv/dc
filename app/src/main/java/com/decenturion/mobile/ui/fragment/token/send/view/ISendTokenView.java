package com.decenturion.mobile.ui.fragment.token.send.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.send.model.SendToModelView;
import com.decenturion.mobile.ui.fragment.token.send.model.SendTokenModelView;

public interface ISendTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull SendTokenModelView model, @NonNull SendToModelView model1);

    void onEnableTwoFA();

    void onSendTokenSuccess();
}
