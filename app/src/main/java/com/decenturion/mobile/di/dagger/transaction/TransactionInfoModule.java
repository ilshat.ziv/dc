package com.decenturion.mobile.di.dagger.transaction;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.traid.ITraidInfoInteractor;
import com.decenturion.mobile.business.traid.TraidInfoInteractor;
import com.decenturion.mobile.business.transaction.ITransactionInfoInteractor;
import com.decenturion.mobile.business.transaction.TransactionInfoInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.traide.TraideRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.traide.presenter.ITraidInfoPresenter;
import com.decenturion.mobile.ui.fragment.traide.presenter.TraidInfoPresenter;
import com.decenturion.mobile.ui.fragment.transaction.presenter.ITransactionInfoPresenter;
import com.decenturion.mobile.ui.fragment.transaction.presenter.TransactionInfoPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionInfoModule {

    @Provides
    @TransactionInfoScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TransactionInfoScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @TransactionInfoScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TransactionInfoScope
    ITraideRepository provideITraideRepository(OrmHelper ormHelper) {
        return new TraideRepository(ormHelper);
    }

    @Provides
    @TransactionInfoScope
    ITransactionInfoInteractor provideITransactionInfoInteractor(IResidentClient IResidentClient,
                                                 ITokenClient iTokenClient,
                                                 IResidentRepository iResidentRepository,
                                                 IWalletRepository iWalletRepository,
                                                 ITokenRepository iTokenRepository,
                                                     ITraideRepository iTraideRepository) {

        return new TransactionInfoInteractor(
                IResidentClient,
                iTokenClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository,
                iTraideRepository
        );
    }

    @Provides
    @TransactionInfoScope
    ITransactionInfoPresenter provideITransactionInfoPresenter(ITransactionInfoInteractor iTransactionInfoInteractor) {
        return new TransactionInfoPresenter(iTransactionInfoInteractor);
    }
}
