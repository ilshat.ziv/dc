package com.decenturion.mobile.ui.fragment.profile_v2.deals.model;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

public class DealModel {

    private String mTraidId;
    private String mTraidDate;
    private String mTraidAmount;
    private String mCoin;
    private String mTraidStatus;

    public DealModel() {
    }

    public DealModel(OrmTokenModel model) {
    }

    public DealModel(WrapperTrade d) {
        Trade trade = d.getTrade();
        mTraidId = trade.getUuid();

        String createdAt = trade.getCreatedAt();
        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            mTraidDate = "Сегодня " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mTraidDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }


        mTraidAmount = trade.getAmount();
        mCoin = trade.getCoin();
        mTraidStatus = trade.getStatus();
    }

    public DealModel(WrapperTrade d, ILocalistionManager iLocalistionManager) {
        Trade trade = d.getTrade();
        mTraidId = trade.getUuid();

        String createdAt = trade.getCreatedAt();
        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            String s = iLocalistionManager.getLocaleString(R.string.app_constants_today);
            mTraidDate = s + " " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mTraidDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }


        mTraidAmount = trade.getAmount();
        mCoin = trade.getCoin();
        mTraidStatus = trade.getStatus();
    }

    public String getTraidId() {
        return mTraidId;
    }

    public String getTraidDate() {
        return mTraidDate;
    }

    public String getTraidAmount() {
        return mTraidAmount;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getTraidStatus() {
        return mTraidStatus;
    }

    public void setTraidId(String traidId) {
        mTraidId = traidId;
    }

    public void setTraidDate(String traidDate) {
        mTraidDate = traidDate;
    }

    public void setTraidAmount(String traidAmount) {
        mTraidAmount = traidAmount;
    }

    public void setCoin(String coin) {
        mCoin = coin;
    }

    public void setTraidStatus(String traidStatus) {
        mTraidStatus = traidStatus;
    }
}