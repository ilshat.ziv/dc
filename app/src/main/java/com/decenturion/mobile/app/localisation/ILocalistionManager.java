package com.decenturion.mobile.app.localisation;

import android.support.annotation.NonNull;

import java.util.LinkedHashMap;

public interface ILocalistionManager {

    String DEFAULT_LOCALISATION = "en_US";

    String getLocale();

    String getLocaleString(@NonNull String key);

    String getLocaleString(int resourceId);

    String getLocaleResource(@NonNull String assetsNameFile);

    void buildAlphabet(@NonNull LinkedHashMap<String, String> map);

    boolean isLocaleChanged();
}
