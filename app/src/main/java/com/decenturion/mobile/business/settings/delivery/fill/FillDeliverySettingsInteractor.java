package com.decenturion.mobile.business.settings.delivery.fill;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.DeliveryPassportModel;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.FillDeliverySettingsModelView;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

public class FillDeliverySettingsInteractor implements IFillDeliverySettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public FillDeliverySettingsInteractor(IResidentClient IResidentClient,
                                          IResidentRepository iResidentRepository,
                                          IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<FillDeliverySettingsModelView> getDeliveryData() {
        Observable<FillDeliverySettingsModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new FillDeliverySettingsModelView(resident, passport, photo);
                })
                .onErrorReturn(throwable -> null);

        Observable<FillDeliverySettingsModelView> obs2 = Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                FillDeliverySettingsModelView::new);

        return Observable.concat(obs1, obs2, Observable.error(new NullPointerException()))
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<DeliveryPassportResult> setDeliveryData(@NonNull String phone,
                                                              @NonNull String address,
                                                              @NonNull String city,
                                                              @NonNull String state,
                                                              @NonNull String country,
                                                              @NonNull String zipcode) {
        DeliveryPassportModel model = new DeliveryPassportModel();
        model.setPhone(phone);
        model.setAddress(address);
        model.setCity(city);
        model.setState(state);
        model.setCountry(country);
        model.setZip(zipcode);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.delivery(m))
                .map(d -> {
                    DeliveryPassportResult result = d.getResult();
                    Resident resident = result.getResident();
                    int step = resident.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return result;
                });
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap) {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(OrmResidentModel::getUuid)
                .map(d -> {
                    byte[] bytes = ImageUtils.getBytes(bitmap);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    return MultipartBody.Part.createFormData("file", d + ".png", requestFile);
                })
                .flatMap(d -> mIResidentClient.uploadPhoto(d));
    }

}
