package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.type.CoinType;
import com.decenturion.mobile.database.type.StartupType;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.database.type.model.StartupTypeModel;
import com.decenturion.mobile.network.response.model.MarketCoinsToken;
import com.decenturion.mobile.network.response.model.Token;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_token_model")
public class OrmTokenModel {

    public final static String ID = "id";
    private final static String BALANCE = "balance";
    private final static String CATEGORY = "category";
    private final static String SELL_PRICE = "sell_price";
    private final static String BUY_PRICE = "buy_price";
    private final static String FREE_COOLDOWN = "free_cooldown";
    private final static String COIN = "coin";
    private static final String STARTUP = "startup";
    private static final String FULL_SYMBOL_NAME = "full_symbol_name";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(dataType = DataType.STRING, columnName = BALANCE)
    private String mBalance;

    @DatabaseField(dataType = DataType.STRING, columnName = CATEGORY)
    private String mCategory;

    @DatabaseField(dataType = DataType.STRING, columnName = BUY_PRICE)
    private String mBuyPrice;

    @DatabaseField(dataType = DataType.STRING, columnName = SELL_PRICE)
    private String mSellPrice;

    @DatabaseField(persisterClass = CoinType.class, columnName = COIN)
    private CoinTypeModel mCoinTypeModel;

    @DatabaseField(persisterClass = StartupType.class, columnName = STARTUP)
    private StartupTypeModel mStartupTypeModel;

    @DatabaseField(dataType = DataType.STRING, columnName = FREE_COOLDOWN)
    private String mFreeCooldown;

    @DatabaseField(dataType = DataType.STRING, columnName = FULL_SYMBOL_NAME)
    private String mFullSymbolName;

    public OrmTokenModel() {
    }

    public OrmTokenModel(@NonNull Token token) {
        mSellPrice = token.getAskPrice();
        mBuyPrice = token.getBidPrice();
        mBalance = token.getBalance();
        mCategory = token.getCategory();
        String freeCooldown = token.getFreeCooldown();
        mFreeCooldown = freeCooldown.replace("+0000 +0000", "+0000 UTC");
        mFullSymbolName = token.getFullSymbol();

        mStartupTypeModel = new StartupTypeModel(token.getStartup());
        mCoinTypeModel = new CoinTypeModel(token.getTokenCoin());
    }

    public OrmTokenModel(@NonNull MarketCoinsToken token) {
        mSellPrice = token.getAskPrice();
//        mBuyPrice = token.getBidPrice();
//        mBalance = token.getBalance();
        mCategory = token.getCategory();

        mStartupTypeModel = new StartupTypeModel(token.getStartup());
        mCoinTypeModel = new CoinTypeModel(token.getTokenCoin());
    }

    public int getId() {
        return mId;
    }

    public String getBuyPrice() {
        return mBuyPrice;
    }

    public String getSellPrice() {
        return mSellPrice;
    }

    public String getBalance() {
        return mBalance;
    }

    public String getCategory() {
        return mCategory;
    }

    public CoinTypeModel getCoinTypeModel() {
        return mCoinTypeModel;
    }

    public StartupTypeModel getStartupTypeModel() {
        return mStartupTypeModel;
    }

    public void setBuyPrice(String buyPrice) {
        mBuyPrice = buyPrice;
    }

    public void setSellPrice(String sellPrice) {
        mSellPrice = sellPrice;
    }

    public String getFreeCooldown() {
        return mFreeCooldown;
    }

    public String getFullSymbolName() {
        return mFullSymbolName;
    }
}
