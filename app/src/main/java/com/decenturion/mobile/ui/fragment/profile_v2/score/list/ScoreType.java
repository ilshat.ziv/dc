package com.decenturion.mobile.ui.fragment.profile_v2.score.list;

public enum ScoreType {
    MAIN,
    TRADE,
    FUTURES
}
