package com.decenturion.mobile.ui.fragment.profile_v2.transactions.presenter;

import com.decenturion.mobile.business.profile.transactions.ITransaionListInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.view.ITransactionListView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TransactionListPresenter extends Presenter<ITransactionListView> implements ITransactionListPresenter {

    private ITransaionListInteractor mITransaionListInteractor;
    private TransactionListModelView mTokenModelView;

    // TODO: 24.09.2018 костыль, поставить защиту от повторающихся запросов
    private boolean isLoadingPage;

    public TransactionListPresenter(ITransaionListInteractor iTokenInteractor) {
        mITransaionListInteractor = iTokenInteractor;
        mTokenModelView = new TransactionListModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTokenModelView = null;
    }

    @Override
    public void bindViewData() {
        if (isLoadingPage) {
            return;
        }

        showProgressView();

        Observable<TransactionListModelView> obs = mITransaionListInteractor.getTransactionsInfo(
                new TransactionListModelView()
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TransactionListModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TransactionListModelView model) {
        mTokenModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void onBottomScrolled() {
        if (mTokenModelView.getOffset() >= mTokenModelView.getTotal() || isLoadingPage) {
            return;
        }
        uploadData();
    }

    private void uploadData() {
        showProgressView();

        Observable<TransactionListModelView> obs = mITransaionListInteractor.getTransactionsInfo(
                mTokenModelView
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TransactionListModelView>) this::uploadDataSuccess,
                this::uploadDataFailure
        );
    }

    private void uploadDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadDataSuccess(TransactionListModelView model) {
        mTokenModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        isLoadingPage = true;
    }

    @Override
    public void hideProgressView() {
        super.hideProgressView();
        isLoadingPage = false;
    }
}
