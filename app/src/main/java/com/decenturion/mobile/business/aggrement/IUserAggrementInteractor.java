package com.decenturion.mobile.business.aggrement;

import com.decenturion.mobile.ui.fragment.disclaimer.model.UserAggrementModelView;

import rx.Observable;

public interface IUserAggrementInteractor {

    Observable<UserAggrementModelView> getTermsInfo();

}
