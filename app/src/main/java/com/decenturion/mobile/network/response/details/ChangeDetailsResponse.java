package com.decenturion.mobile.network.response.details;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeDetailsResponse extends BaseResponse {

    @JsonProperty("result")
    private ChangeDetailsResult mResult;

    public ChangeDetailsResponse() {
    }

    @JsonIgnore
    public ChangeDetailsResult getResult() {
        return mResult;
    }
}
