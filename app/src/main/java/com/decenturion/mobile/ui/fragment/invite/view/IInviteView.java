package com.decenturion.mobile.ui.fragment.invite.view;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.invite.model.InviteModelView;
import com.decenturion.mobile.ui.fragment.invite.model.TokenModelView;

public interface IInviteView extends IScreenFragmentView {

    void onBindViewData(InviteModelView model, TokenModelView model1);

    void onInvitationSend();

    void onInvitationSendFail();
}
