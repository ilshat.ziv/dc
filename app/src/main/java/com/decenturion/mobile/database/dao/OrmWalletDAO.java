package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmWalletModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmWalletDAO extends BaseDao<OrmWalletModel, Integer> {

    public OrmWalletDAO(ConnectionSource connectionSource,
                        Class<OrmWalletModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
