package com.decenturion.mobile.network.response.signup.skip;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignupSkipResponse extends BaseResponse {

    @JsonProperty("result")
    private SignupSkipResult mResult;

    public SignupSkipResponse() {
    }

    @JsonIgnore
    public SignupSkipResult getResult() {
        return mResult;
    }
}
