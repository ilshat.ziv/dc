package com.decenturion.mobile.network.client.score;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class ScoreClient extends DHttpClient<IScoreClient> {

    public ScoreClient(NetworkModule module) {
        super(module);
    }
}
