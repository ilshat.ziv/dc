package com.decenturion.mobile.utils;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;

public final class DisplayUtils {

    public static int getDisplayWidthPixel(final Resources resources) {
        return resources.getDisplayMetrics().widthPixels;
    }

    public static int GetDipsFromPixel(final Resources resources, float pixels) {
        final float scale = resources.getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }

    public static int getDp(Context context, float dps) {
        final float scale = context.getResources().getDisplayMetrics().density;
        //int pixels
        return (int) (dps * scale + 0.5f);
    }

    public static int[] getSizeView(final Resources resources, final View view) {
        int displaWidth = resources.getDisplayMetrics().widthPixels;
        int displaHeight = resources.getDisplayMetrics().heightPixels;

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(displaWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(displaHeight, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);

        return new int[] {view.getMeasuredWidth(), view.getMeasuredHeight()};
    }
}
