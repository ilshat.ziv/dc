package com.decenturion.mobile.business.send;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.fragment.token.send.model.SendTokenModelView;

import rx.Observable;

public interface ISendTokenInteractor {

    Observable<SendTokenModelView> getCoinInfo(@NonNull SendTokenModelView model);

    Observable<SendTokenResult> sendToken(@NonNull String coinUUID,
                                          @NonNull String coinCategory,
                                          @NonNull String dcntWallet,
                                          @NonNull String amount,
                                          @NonNull String twofa);
}
