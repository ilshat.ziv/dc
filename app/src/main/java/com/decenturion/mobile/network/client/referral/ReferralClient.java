package com.decenturion.mobile.network.client.referral;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class ReferralClient extends DHttpClient<IReferralClient> {

    public ReferralClient(NetworkModule module) {
        super(module);
    }
}
