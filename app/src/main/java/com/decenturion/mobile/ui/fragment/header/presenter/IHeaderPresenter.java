package com.decenturion.mobile.ui.fragment.header.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.header.view.IHeaderView;

public interface IHeaderPresenter extends IPresenter<IHeaderView> {

    void bindViewData();

}
