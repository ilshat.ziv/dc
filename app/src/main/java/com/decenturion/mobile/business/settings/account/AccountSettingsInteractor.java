package com.decenturion.mobile.business.settings.account;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.account.model.AccountSettingsModelView;

import rx.Observable;

public class AccountSettingsInteractor implements IAccountSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public AccountSettingsInteractor(IResidentClient IResidentClient,
                                     IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<AccountSettingsModelView> getProfileInfo() {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(AccountSettingsModelView::new);
    }
}
