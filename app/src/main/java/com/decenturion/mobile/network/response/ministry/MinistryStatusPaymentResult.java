package com.decenturion.mobile.network.response.ministry;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryStatusPaymentResult {

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("amount_left")
    private String mAmountLeft;

    public MinistryStatusPaymentResult() {
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getAddress() {
        return mAddress;
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getAmountLeft() {
        return mAmountLeft;
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoin = coin;
    }

    @JsonIgnore
    public void setAddress(String address) {
        mAddress = address;
    }

    @JsonIgnore
    public void setAmount(String amount) {
        mAmount = amount;
    }

    @JsonIgnore
    public void setAmountLeft(String amountLeft) {
        mAmountLeft = amountLeft;
    }
}
