package com.decenturion.mobile.ui.activity.sign;

import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.floating.keybord.IKeyboardVisionManager;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

public interface ISignActivity extends IScreenActivityView {

    IToolbarController getIToolbarController();

    INavigationManager getINavigationManager();

    IDialogManager getIDialogManager();

    IKeyboardVisionManager getIKeyboardVisionManager();
}
