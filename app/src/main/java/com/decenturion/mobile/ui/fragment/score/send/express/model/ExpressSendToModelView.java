package com.decenturion.mobile.ui.fragment.score.send.express.model;

import java.util.ArrayList;

public class ExpressSendToModelView {

    private ArrayList<ExpressSendToModel> mSendToData;

    public ExpressSendToModelView(ArrayList<ExpressSendToModel> sendToData) {
        mSendToData = sendToData;
    }

    public ArrayList<ExpressSendToModel> getSendToData() {
        return mSendToData;
    }
}
