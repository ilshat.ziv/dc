package com.decenturion.mobile.di.dagger.passport.activation;

import com.decenturion.mobile.ui.fragment.passport.activation.view.ActivationPassportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ActivationPassportModule.class
})

@ActivationPassportScope
public interface ActivationPassportComponent {

    void inject(ActivationPassportFragment view);
}
