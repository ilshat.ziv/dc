package com.decenturion.mobile.ui.fragment.settings.account.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.account.view.IAccountSettingsView;

public interface IAccountSettingsPresenter extends IPresenter<IAccountSettingsView> {

    void bindViewData();
}
