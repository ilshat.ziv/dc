package com.decenturion.mobile.app.toast;

public enum TypeMessage {
    Normal, Success, Info, Warning, Error
}