package com.decenturion.mobile.ui.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public interface INavigationManager<F> {

    void navigateTo(Class<F> clazz);

    void navigateTo(Class<F> clazz, String tag);

    void navigateTo(Class<F> clazz, boolean isAddBackStack);

    void navigateTo(Class<F> clazz, boolean isAddBackStack, String tag);

    void navigateTo(Class<F> clazz, boolean isAddBackStack, String tag, Bundle bundle);

    void navigateTo(Class<F> clazz, boolean isAddBackStack, Bundle bundle);

    void navigateTo(Class<F> clazz, Bundle bundle);

    void navigateTo(Class<F> clazz, boolean isAddBackStack, Fragment target, int actionCode);

    void navigateToBack();

    F getCurentFragment();

    void onSaveInstanceState(Bundle bundle);

    void restoreInstanceState(Bundle savedInstanceState);

    void onDestroy();

}
