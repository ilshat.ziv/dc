package com.decenturion.mobile.ui.fragment.seller.trade.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.seller.traids.SellerTraidsComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.seller.trade.SellerTraidsAdapter;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModel;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModelView;
import com.decenturion.mobile.ui.fragment.seller.trade.presenter.ISellerTraidsPresenter;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class SellerTraidsFragment extends BaseFragment implements ISellerTraidsView,
        SellerTraidsAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    public final String SELLER_UUID = "SELLER_UUID";

    private String mSellerUUID;

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.traidListView)
    RecyclerView mTraidListView;
    private SellerTraidsAdapter mSellerTraidsAdapter;

    /* DI */

    @Inject
    ISellerTraidsPresenter mISellerTraidsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mSellerUUID = arguments.getString(SELLER_UUID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SellerTraidsComponent sellerTraidsComponent = appDelegate
                .getIDIManager()
                .plusSellerTraidsComponent();
        sellerTraidsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_traids, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        mRefreshView.setOnRefreshListener(this);

        Context context = getContext();
        assert context != null;
        mTraidListView.setLayoutManager(new LinearLayoutManager(context));
        mSellerTraidsAdapter = new SellerTraidsAdapter(context, this);
        mTraidListView.setAdapter(mSellerTraidsAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISellerTraidsPresenter.bindView(this);
        mISellerTraidsPresenter.bindViewData(mSellerUUID);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISellerTraidsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISellerTraidsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull SellerTraideModelView model) {
        mSellerTraidsAdapter.replaceDataSet(model.getTokentModelList());
    }

    @Override
    public void onItemClick(SellerTraideModel model) {
//        Intent intent = new Intent(getActivity(), TraidActivity.class);
//        intent.putExtra(TraidActivity.TRAID_UUID, model.getTraidId());
//        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISellerTraidsPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mISellerTraidsPresenter.bindViewData(mSellerUUID);
    }
}
