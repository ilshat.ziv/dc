package com.decenturion.mobile.network.response.referral;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralSettingsResponse extends BaseResponse {

    @JsonProperty("result")
    private ReferralSettingsResult mResult;

    public ReferralSettingsResponse() {
    }

    @JsonIgnore
    public ReferralSettingsResult getResult() {
        return mResult;
    }
}
