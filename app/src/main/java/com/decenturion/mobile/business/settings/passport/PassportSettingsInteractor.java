package com.decenturion.mobile.business.settings.passport;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.UpdatePassportModel;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PassportSettingsModelView;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

public class PassportSettingsInteractor implements IPassportSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public PassportSettingsInteractor(IResidentClient IResidentClient,
                                      IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<PassportSettingsModelView> getPassportInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                PassportSettingsModelView::new);
    }

    @Override
    public Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName,
                                                          @NonNull String secondName,
                                                          long birth,
                                                          @NonNull String sex,
                                                          @NonNull String country,
                                                          @NonNull String city) {
        UpdatePassportModel model = new UpdatePassportModel();
        model.setFirstName(firstName);
        model.setLastName(secondName);
        model.setBirth(DateTimeUtils.getDateFormat(birth, DateTimeUtils.Format.SIMPLE_FORMAT_2));
        model.setSex(sex);
        model.setCountry(country);
        model.setCity(city);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.updatePassport(m))
                .doOnNext(d -> {
                    try {
                        UpdatePassportResult result = d.getResult();
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap) {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(OrmResidentModel::getUuid)
                .map(d -> {
                    byte[] bytes = ImageUtils.getBytes(bitmap);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    return MultipartBody.Part.createFormData("file", d + ".png", requestFile);
                })
                .flatMap(d -> mIResidentClient.uploadPhoto(d));
    }
}
