package com.decenturion.mobile.ui.architecture.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenView;

public interface IPresenter<T extends IScreenView> {

    void bindView(@NonNull T view);

    void unbindView();


    void showProgressView();

    void hideProgressView();


    void showMessage(@NonNull String message);

    void showSuccessView(@NonNull String message);

    void showErrorView(Throwable t);

    void showErrorView(@NonNull String m);

    void showWarningView(Throwable t);

    void showWarningView(@NonNull String m);


    void onDestroy();

}
