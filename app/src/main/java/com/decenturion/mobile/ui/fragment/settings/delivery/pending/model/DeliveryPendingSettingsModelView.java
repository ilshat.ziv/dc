package com.decenturion.mobile.ui.fragment.settings.delivery.pending.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;

public class DeliveryPendingSettingsModelView {

    private String mPhone;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mCountry;
    private String mZip;

    private boolean isDeliveryPaid;

    public DeliveryPendingSettingsModelView() {
    }

    public DeliveryPendingSettingsModelView(@NonNull OrmPassportModel m1) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
    }

    public DeliveryPendingSettingsModelView(@NonNull DeliveryPassportResult d) {
        Passport passport = d.getPassport();
        Delivery delivery = passport.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
    }

    public DeliveryPendingSettingsModelView(OrmPassportModel m1, boolean deliveryPaid) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
        this.isDeliveryPaid = deliveryPaid;
    }

    public DeliveryPendingSettingsModelView(OrmPassportModel m1, OrmResidentModel m0, Boolean deliveryPaid) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();

        this.isDeliveryPaid = m0.isActive() && !m0.isInvited() || deliveryPaid;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getState() {
        return mState;
    }

    public String getZip() {
        return mZip;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }
}
