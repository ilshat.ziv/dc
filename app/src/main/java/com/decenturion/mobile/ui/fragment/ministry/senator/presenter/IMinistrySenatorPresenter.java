package com.decenturion.mobile.ui.fragment.ministry.senator.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.ministry.product.view.IMinistryProductView;
import com.decenturion.mobile.ui.fragment.ministry.senator.view.IMinistrySenatorView;

import rx.Observable;

public interface IMinistrySenatorPresenter extends IPresenter<IMinistrySenatorView> {

    void bindViewData(@NonNull String category);

    void bindViewData(Observable<String> amount, Observable<String> coin);

    void getPaymentAddress(@NonNull String category, @NonNull String coin);
}
