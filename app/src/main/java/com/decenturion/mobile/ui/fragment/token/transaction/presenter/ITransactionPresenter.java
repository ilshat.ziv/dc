package com.decenturion.mobile.ui.fragment.token.transaction.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.transaction.view.ITransactionView;

public interface ITransactionPresenter extends IPresenter<ITransactionView> {

    void bindViewData(@NonNull String traideUUID);

    @Deprecated
    void confirmTraide(boolean checked, String receiveAddress, String refundAddress, String txid);

    void cancelTraide(String tradeUUID);
}
