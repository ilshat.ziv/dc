package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignoutResponse extends BaseResponse {

    public SignoutResponse() {
    }
}
