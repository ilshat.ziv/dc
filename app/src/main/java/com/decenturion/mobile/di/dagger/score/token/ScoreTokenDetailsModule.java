package com.decenturion.mobile.di.dagger.score.token;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.score.token.IScoreTokenDetailsInteractor;
import com.decenturion.mobile.business.score.token.ScoreTokenDetailsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.score.details.presenter.IScoreTokenDetailsPresenter;
import com.decenturion.mobile.ui.fragment.score.details.presenter.ScoreTokenDetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ScoreTokenDetailsModule {

    @Provides
    @ScoreTokenDetailsScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @ScoreTokenDetailsScope
    IScoreTokenDetailsInteractor provideIScoreTokenDetailsInteractor(IResidentClient IResidentClient,
                                                         ITokenRepository ITokenRepository,
                                                         ITokenClient iTokenClient,
                                                         ILocalistionManager iLocalistionManager) {

        return new ScoreTokenDetailsInteractor(IResidentClient, ITokenRepository, iTokenClient,
                iLocalistionManager);
    }

    @Provides
    @ScoreTokenDetailsScope
    IScoreTokenDetailsPresenter provideIScoreTokenDetailsPresenter(IScoreTokenDetailsInteractor iScoreTokenDetailsInteractor) {
        return new ScoreTokenDetailsPresenter(iScoreTokenDetailsInteractor);
    }
}
