package com.decenturion.mobile.ui.fragment.settings.delivery.pay.model;

import com.decenturion.mobile.network.response.model.Coin;

import java.util.ArrayList;

public class CoinModelView {

    private ArrayList<Coin> mCoinData;

    public CoinModelView() {
    }

    public CoinModelView(ArrayList<Coin> countryData) {
        mCoinData = countryData;
    }

    public ArrayList<Coin> getCoinData() {
        return mCoinData;
    }
}
