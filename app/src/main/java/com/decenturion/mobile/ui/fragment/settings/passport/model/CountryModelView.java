package com.decenturion.mobile.ui.fragment.settings.passport.model;

import com.decenturion.mobile.network.response.model.Country;

import java.util.ArrayList;

public class CountryModelView {

    private ArrayList<Country> mCountryData;

    public CountryModelView() {
    }

    public CountryModelView(ArrayList<Country> countryData) {
        mCountryData = countryData;
    }

    public ArrayList<Country> getCountryData() {
        return mCountryData;
    }
}
