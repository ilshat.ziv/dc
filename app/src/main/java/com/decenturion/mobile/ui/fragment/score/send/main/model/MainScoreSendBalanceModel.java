package com.decenturion.mobile.ui.fragment.score.send.main.model;

import android.support.annotation.NonNull;

public class MainScoreSendBalanceModel {

    private String mCoin;
    private double mBalance;
    private String mAddress;

    public MainScoreSendBalanceModel(@NonNull String coin, double balance, @NonNull String address) {
        mCoin = coin;
        mBalance = balance;
        mAddress = address;
    }

    public String getCoin() {
        return mCoin;
    }

    public double getBalance() {
        return mBalance;
    }

    public String getAddress() {
        return mAddress;
    }
}
