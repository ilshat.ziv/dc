package com.decenturion.mobile.ui.fragment.traide.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.traid.ITraidInfoInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.traide.model.TraidInfoModelView;
import com.decenturion.mobile.ui.fragment.traide.view.ITraidInfoView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TraidInfoPresenter extends Presenter<ITraidInfoView> implements ITraidInfoPresenter {

    private ITraidInfoInteractor mITraidInfoInteractor;
    private TraidInfoModelView mTraidInfoModelView;

    public TraidInfoPresenter(ITraidInfoInteractor iEiTraidInfoInteractor) {
        mITraidInfoInteractor = iEiTraidInfoInteractor;
    }

    @Override
    public void bindViewData(@NonNull String traidUUID) {
        showProgressView();

//        mTraidInfoModelView = new TransactionInfoModelView(traidId);

        Observable<TraidInfoModelView> obs = mITraidInfoInteractor.getTraidInfo(traidUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TraidInfoModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TraidInfoModelView model) {
        mTraidInfoModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());
                return;
            }
        }

        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTraidInfoModelView = null;
    }
}
