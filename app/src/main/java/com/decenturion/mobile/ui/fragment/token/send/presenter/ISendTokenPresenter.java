package com.decenturion.mobile.ui.fragment.token.send.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.send.view.ISendTokenView;

public interface ISendTokenPresenter extends IPresenter<ISendTokenView> {

    void bindViewData(int tokenId);

    void onSaveInstanceState(@NonNull Bundle outState,
                             String dcntWallet,
                             String amount,
                             String twofa);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    void sendToken(@NonNull String dcntWallet,
                   @NonNull String amount,
                   @NonNull String twofa);
}
