package com.decenturion.mobile.network.request.score.send;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendToExternalModel {

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCoinCategory;

    @JsonProperty("coin_uuid")
    private String mCoinUUID;

    @JsonProperty("code")
    private String mTwoFa;

    public SendToExternalModel() {
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoinUUID = coin;
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCoinCategory = category;
    }

    @JsonIgnore
    public void setAddress(String address) {
        mAddress = address;
    }

    @JsonIgnore
    public void setAmount(String amount) {
        mAmount = amount;
    }

    @JsonIgnore
    public void setTwoFa(String m2Fa) {
        this.mTwoFa = m2Fa;
    }
}
