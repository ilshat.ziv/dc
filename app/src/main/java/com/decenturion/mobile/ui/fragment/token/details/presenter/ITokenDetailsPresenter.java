package com.decenturion.mobile.ui.fragment.token.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.details.model.TokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.token.details.view.ITokenDetailsView;

public interface ITokenDetailsPresenter extends IPresenter<ITokenDetailsView> {

    void bindViewData(int tokenId);

    void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory);

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    TokenDetailsModelView getModelView();
}
