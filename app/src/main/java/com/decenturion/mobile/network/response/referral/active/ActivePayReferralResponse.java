package com.decenturion.mobile.network.response.referral.active;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivePayReferralResponse extends BaseResponse {

    @JsonProperty("result")
    private ActivePayReferralResult mResult;

    public ActivePayReferralResponse() {
    }

    @JsonIgnore
    public ActivePayReferralResult getResult() {
        return mResult;
    }
}
