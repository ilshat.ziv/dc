package com.decenturion.mobile.ui.architecture.view;

public interface IScreenFragmentView extends IScreenView {

    IScreenActivityView getIScreenActivityView();

}
