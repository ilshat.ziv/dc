package com.decenturion.mobile.network.response.resident.seller;

import com.decenturion.mobile.network.response.model.ReferralSettings;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerResidentResult {

    @JsonProperty("first_name")
    private String mFirstName;

    @JsonProperty("last_name")
    private String mLastName;

    @JsonProperty("passport")
    private String mPassport;

    @JsonProperty("photo")
    private String mPhoto;

    @JsonProperty("sex")
    private String mSex;

    @JsonProperty("dob")
    private String mBirth;

    @JsonProperty("country")
    private String mCountry;

    @JsonProperty("city")
    private String mCity;


    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("email")
    private String mEmail;

    @JsonProperty("phone")
    private String mPhone;

    @JsonProperty("balance")
    private String mBalance;

    @JsonProperty("position")
    private String mPosition;

    @JsonProperty("money")
    private String mMoney;

    @JsonProperty("power")
    private String mPower;

    @JsonProperty("glory")
    private String mGlory;

    @JsonProperty("about")
    private String mAbout;

    @JsonProperty("twitter")
    private String mTwitter;

    @JsonProperty("medium")
    private String mMedium;

    @JsonProperty("telegram")
    private String mTelegram;

    @JsonProperty("facebook")
    private String mFacebook;

    @JsonProperty("wechat")
    private String mWechat;

    @JsonProperty("whatsapp")
    private String mWhatsapp;

    @JsonProperty("viber")
    private String mViber;

    @JsonProperty("dcnt")
    private String mDcnt;

    @JsonProperty("deposit_price")
    private int mDepositPrice;

    @JsonProperty("referral_id")
    private String mReferralId;

    @JsonProperty("pay_setting")
    private ReferralSettings mPaySetting;

    @JsonProperty("status")
    private String mStatus;

    public SellerResidentResult() {
    }

    @JsonIgnore
    public String getPassport() {
        return mPassport;
    }

    @JsonIgnore
    public String getPhoto() {
        return mPhoto;
    }

    @JsonIgnore
    public String getFirstName() {
        return mFirstName;
    }

    @JsonIgnore
    public String getLastName() {
        return mLastName;
    }

    @JsonIgnore
    public String getSex() {
        return mSex;
    }

    @JsonIgnore
    public String getBirth() {
        return mBirth;
    }

    @JsonIgnore
    public String getCountry() {
        return mCountry;
    }

    @JsonIgnore
    public String getCity() {
        return mCity;
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getEmail() {
        return mEmail;
    }

    @JsonIgnore
    public String getPhone() {
        return mPhone;
    }

    @JsonIgnore
    public String getBalance() {
        return mBalance;
    }

    @JsonIgnore
    public String getPosition() {
        return mPosition;
    }

    @JsonIgnore
    public String getMoney() {
        return mMoney;
    }

    @JsonIgnore
    public String getPower() {
        return mPower;
    }

    @JsonIgnore
    public String getGlory() {
        return mGlory;
    }

    @JsonIgnore
    public String getAbout() {
        return mAbout;
    }

    @JsonIgnore
    public String getTwitter() {
        return mTwitter;
    }

    @JsonIgnore
    public String getMedium() {
        return mMedium;
    }

    @JsonIgnore
    public String getTelegram() {
        return mTelegram;
    }

    @JsonIgnore
    public String getFacebook() {
        return mFacebook;
    }

    @JsonIgnore
    public String getWechat() {
        return mWechat;
    }

    @JsonIgnore
    public String getWhatsapp() {
        return mWhatsapp;
    }

    @JsonIgnore
    public String getViber() {
        return mViber;
    }

    @JsonIgnore
    public String getDcnt() {
        return mDcnt;
    }

    @JsonIgnore
    public int getDepositPrice() {
        return mDepositPrice;
    }

    @JsonIgnore
    public ReferralSettings getPaySetting() {
        return mPaySetting;
    }

    @JsonIgnore
    public String getReferralId() {
        return mReferralId;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }
}
