package com.decenturion.mobile.ui.fragment.settings.delivery.pay.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class Coin implements ISpinnerItem {

    private String mName;

    public Coin(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String getItemName() {
        return mName;
    }

    @Override
    public String getItemKey() {
        return mName;
    }
}
