package com.decenturion.mobile.business.settings.delivery.delivered;

import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model.DeliveredSettingsModelView;

import rx.Observable;

public interface IDeliveredSettingsInteractor {

    Observable<DeliveredSettingsModelView> getPassportInfo();
}
