package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;

public class RestrictedException extends AppException {

    public RestrictedException() {
        super("Access to this resource is restricted in your region. " +
                "Please check your connection settings and make sure you are not using a proxy.");
    }

    public RestrictedException(Throwable throwable) {
        super(throwable.getMessage());
    }

    public RestrictedException(String message) {
        super(message);
    }
}
