package com.decenturion.mobile.network.response.payment.deliver;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliverPaymentResponse extends BaseResponse {

    @JsonProperty("result")
    private DeliverPaymentResult mResult;

    public DeliverPaymentResponse() {
    }

    @JsonIgnore
    public DeliverPaymentResult getResult() {
        return mResult;
    }
}
