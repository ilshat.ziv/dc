package com.decenturion.mobile.ui.fragment.profile_v2.deals.model;

import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class DealListModelView {

    private List<DealModel> mTokentModelList;
    private int mTotal;
    private long mOffset;
    public final int LIMIT = 10;

    public DealListModelView() {
    }

    public DealListModelView(List<DealModel> list, int total) {
        mTokentModelList = list;
        mTotal = total;
    }

    public DealListModelView(List<DealModel> d) {
        mTokentModelList = d;
    }

    public List<DealModel> getTokentModelList() {
        return mTokentModelList;
    }

    public void addDeals(List<DealModel> list) {
        if (CollectionUtils.isEmpty(mTokentModelList)) {
            mTokentModelList = new ArrayList<>();
        }

        if (!CollectionUtils.isEmpty(list)) {
            mTokentModelList.addAll(list);
        }
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }
}
