package com.decenturion.mobile.ui.fragment.seller.passport.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.response.model.ReferralSettings;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResult;

public class BannerModelView {

    private String mReferralPrice;
    private String mBasePrice;
    private String mReferralId;
    private String mTitle;

    public BannerModelView(@NonNull SellerResidentResult sellerResidentResult) {
        ReferralSettings model = sellerResidentResult.getPaySetting();
        int basePrice = sellerResidentResult.getDepositPrice();
        mReferralId = sellerResidentResult.getReferralId();
        if (TextUtils.equals(model.getFixedCost(), "0")) {
            int percent = model.getPercent();
            double d = (double) basePrice * ((double) (100 - percent) / 100);
            mReferralPrice = String.valueOf((d * 100) / 100);
            mTitle = model.getPercentComment();
        } else {
            mReferralPrice = model.getFixedCost();
            mTitle = model.getFixedComment();
        }

        mBasePrice = String.valueOf(basePrice);
    }

    public String getReferralPrice() {
        return mReferralPrice;
    }

    public String getBasePrice() {
        return mBasePrice;
    }

    public String getReferralId() {
        return mReferralId;
    }

    public String getTitle() {
        return mTitle;
    }
}
