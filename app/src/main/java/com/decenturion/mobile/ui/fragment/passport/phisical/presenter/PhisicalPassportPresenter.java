package com.decenturion.mobile.ui.fragment.passport.phisical.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.passport.phisical.IPhisicalPassportInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.delivery.skip.SkipDeliveryResult;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.DeliveryDataModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PhisicalPassportModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.view.IPhisicalPassportView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PhisicalPassportPresenter extends Presenter<IPhisicalPassportView> implements IPhisicalPassportPresenter {

    private Context mContext;
    private IPhisicalPassportInteractor mIPhisicalPassportInteractor;
    private ILocalistionManager mILocalistionManager;
    private IPrefsManager mIPrefsManager;

    private PhisicalPassportModelView mPhisicalPassportModelView;
    private CountryModelView mCountryModelView;
    private CoinModelView mCoinModelView;

    public PhisicalPassportPresenter(Context context,
                                     IPhisicalPassportInteractor iPhisicalPassportInteractor,
                                     IPrefsManager iPrefsManager,
                                     ILocalistionManager iLocalistionManager) {

        mIPrefsManager = iPrefsManager;
        mContext = context;
        mIPhisicalPassportInteractor = iPhisicalPassportInteractor;
        mILocalistionManager = iLocalistionManager;

        mPhisicalPassportModelView = new PhisicalPassportModelView();
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<PhisicalPassportModelView> obs = mIPhisicalPassportInteractor.getPhisicalPassportData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PhisicalPassportModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void setDeliveryData(@NonNull String phone,
                                @NonNull String address,
                                @NonNull String city,
                                @NonNull String state,
                                @NonNull String country,
                                @NonNull String zipcode) {

        if (mPhisicalPassportModelView.isInvited() && !mPhisicalPassportModelView.isActive()
                && TextUtils.isEmpty(address)) {

            hideProgressView();

//            Resident resident = result.getResident();
//            int step = resident.getStep();
            IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
            iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, 5);

//            mPhisicalPassportModelView.setActive(resident.isActive());
//            DeliveryDataModelView deliverDataModelView = mPhisicalPassportModelView.getDeliverDataModelView();
//            deliverDataModelView.setData(result);

            if (mView != null) {
                mView.onDeliveryPassportDataSeted(mPhisicalPassportModelView);
            }

            return;
        }

        showProgressView();

        Observable<DeliveryPassportResult> obs = mIPhisicalPassportInteractor.setDeliveryData(
                phone,
                address,
                city,
                state,
                country,
                zipcode
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliveryPassportResult>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(DeliveryPassportResult result) {
        hideProgressView();

        Resident resident = result.getResident();
        mPhisicalPassportModelView.setActive(resident.isActive());
        DeliveryDataModelView deliverDataModelView = mPhisicalPassportModelView.getDeliverDataModelView();
        deliverDataModelView.setData(result);

        if (mView != null) {
            mView.onDeliveryPassportDataSeted(mPhisicalPassportModelView);
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void bindViewDataSuccess(PhisicalPassportModelView model) {
        mPhisicalPassportModelView = model;

        ArrayList<Coin> list = initCoinData();
        mCoinModelView = new CoinModelView(list);

        ArrayList<Country> list1 = initCountryData();
        mCountryModelView = new CountryModelView(list1);
        if (mView != null) {
            mView.onBindViewData(mCoinModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    private ArrayList<Coin> initCoinData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/payment.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void getPaymentAddress(@NonNull String coin) {
        if (TextUtils.isEmpty(coin)) {
            return;
        }

        showProgressView();

        Observable<PhisicalPassportModelView> obs = mIPhisicalPassportInteractor.genPaymentAddress(coin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PhisicalPassportModelView>) this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void genPaymentAddressSuccess(PhisicalPassportModelView address) {
        if (mView != null) {
            mView.onBindPaymentViewData(address);
        }
        hideProgressView();
    }

    private void genPaymentAddressFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void skipDelivery() {
        showProgressView();

        Observable<SkipDeliveryResult> obs = mIPhisicalPassportInteractor.skipDelivery()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SkipDeliveryResult>) this::skipDeliverySuccess,
                this::skipDeliveryFailure
        );
    }

    private void skipDeliverySuccess(SkipDeliveryResult address) {
        if (mView != null) {
            mView.onSkipedDelivery();
        }
        hideProgressView();
    }

    private void skipDeliveryFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
