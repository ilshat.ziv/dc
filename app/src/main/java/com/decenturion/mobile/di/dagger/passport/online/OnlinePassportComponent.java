package com.decenturion.mobile.di.dagger.passport.online;

import com.decenturion.mobile.ui.fragment.passport.online.view.OnlinePassportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        OnlinePassportModule.class
})

@OnlinePassportScope
public interface OnlinePassportComponent {

    void inject(OnlinePassportFragment view);
}
