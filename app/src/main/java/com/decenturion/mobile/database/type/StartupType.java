package com.decenturion.mobile.database.type;

import com.decenturion.mobile.database.type.model.StartupTypeModel;

public class StartupType extends BaseFieldClassPersister<StartupTypeModel> {

    private static StartupType INSTANCE;

    protected StartupType(Class<StartupTypeModel> typeFieldClass) {
        super(typeFieldClass);
    }

    public static StartupType getSingleton() {
        if (INSTANCE == null) {
            return INSTANCE = new StartupType(StartupTypeModel.class);
        }
        return INSTANCE;
    }
}