package com.decenturion.mobile.ui.dialog.fullscreen.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.camera.Camera2Controller;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.dialog.fullscreen.FullDialog;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.controls.FlashSwitchView;
import com.decenturion.mobile.ui.dialog.fullscreen.camera.controls.RecordButton;
import com.decenturion.mobile.utils.LoggerUtils;

import com.github.florent37.camerafragment.CameraFragmentApi;
import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.configuration.ConfigurationProvider;
import com.github.florent37.camerafragment.configuration.ConfigurationProviderImpl;
import com.github.florent37.camerafragment.internal.controller.CameraController;
import com.github.florent37.camerafragment.internal.controller.view.CameraView;
import com.github.florent37.camerafragment.internal.enums.Camera;
import com.github.florent37.camerafragment.internal.enums.Flash;
import com.github.florent37.camerafragment.internal.enums.MediaAction;
import com.github.florent37.camerafragment.internal.enums.Record;
import com.github.florent37.camerafragment.internal.manager.CameraManager;
import com.github.florent37.camerafragment.internal.manager.listener.CameraCloseListener;
import com.github.florent37.camerafragment.internal.timer.CountdownTask;
import com.github.florent37.camerafragment.internal.timer.TimerTask;
import com.github.florent37.camerafragment.internal.timer.TimerTaskBase;
import com.github.florent37.camerafragment.internal.ui.model.PhotoQualityOption;
import com.github.florent37.camerafragment.internal.ui.model.VideoQualityOption;
import com.github.florent37.camerafragment.internal.ui.view.AspectFrameLayout;
import com.github.florent37.camerafragment.internal.utils.Size;
import com.github.florent37.camerafragment.internal.utils.Utils;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentVideoRecordTextAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentVideoRecordTextListener;
import com.github.florent37.camerafragment.widgets.CameraSwitchView;
import com.github.florent37.camerafragment.widgets.MediaActionSwitchView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import uk.co.senab.photoview.PhotoView;

public class CameraDialog extends FullDialog implements CameraFragmentApi {

    public static final int MIN_VERSION_ICECREAM = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;

    protected CharSequence[] mVideoQualities;
    protected CharSequence[] mPhotoQualities;
    protected AspectFrameLayout mPreviewContainer;

    protected ConfigurationProvider mConfigurationProvider;

    @Configuration.MediaQuality
    protected int mNewQuality = -1;

    private Configuration mConfiguration;
    private SensorManager mSensorManager = null;
    private CameraController mCameraController;
    private AlertDialog mSettingsDialog;
    private CameraFragmentControlsListener mCameraFragmentControlsListener;
    private CameraFragmentVideoRecordTextListener mCameraFragmentVideoRecordTextListener;

    final TimerTaskBase.Callback mTimerCallBack = new TimerTaskBase.Callback() {
        @Override
        public void setText(String text) {
            if (mCameraFragmentVideoRecordTextListener != null) {
                mCameraFragmentVideoRecordTextListener.setRecordDurationText(text);
            }
        }

        @Override
        public void setTextVisible(boolean visible) {
            if (mCameraFragmentVideoRecordTextListener != null) {
                mCameraFragmentVideoRecordTextListener.setRecordDurationTextVisible(visible);
            }
        }
    };
    private CameraFragmentStateListener mCameraFragmentStateListener;

    @Flash.FlashMode
    private int mCurrentFlashMode = Flash.FLASH_AUTO;

    @Camera.CameraType
    private int mCurrentCameraType = Camera.CAMERA_TYPE_REAR;

    @MediaAction.MediaActionState
    private int mCurrentMediaActionState = MediaAction.ACTION_PHOTO;

    @Record.RecordState
    private int mCurrentRecordState = Record.TAKE_PHOTO_STATE;

    private String mMediaFilePath;
    private FileObserver mFileObserver;
    private long mMaxVideoFileSize = 0;
    private TimerTaskBase mCountDownTimer;

    private SensorEventListener mSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            synchronized (this) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    if (sensorEvent.values[0] < 4 && sensorEvent.values[0] > -4) {
                        if (sensorEvent.values[1] > 0) {
                            // UP
                            mConfigurationProvider.setSensorPosition(Configuration.SENSOR_POSITION_UP);
                            mConfigurationProvider.setDegrees(mConfigurationProvider.getDeviceDefaultOrientation() == Configuration.ORIENTATION_PORTRAIT ? 0 : 90);
                        } else if (sensorEvent.values[1] < 0) {
                            // UP SIDE DOWN
                            mConfigurationProvider.setSensorPosition(Configuration.SENSOR_POSITION_UP_SIDE_DOWN);
                            mConfigurationProvider.setDegrees(mConfigurationProvider.getDeviceDefaultOrientation() == Configuration.ORIENTATION_PORTRAIT ? 180 : 270);
                        }
                    } else if (sensorEvent.values[1] < 4 && sensorEvent.values[1] > -4) {
                        if (sensorEvent.values[0] > 0) {
                            // LEFT
                            mConfigurationProvider.setSensorPosition(Configuration.SENSOR_POSITION_LEFT);
                            mConfigurationProvider.setDegrees(mConfigurationProvider.getDeviceDefaultOrientation() == Configuration.ORIENTATION_PORTRAIT ? 90 : 180);
                        } else if (sensorEvent.values[0] < 0) {
                            // RIGHT
                            mConfigurationProvider.setSensorPosition(Configuration.SENSOR_POSITION_RIGHT);
                            mConfigurationProvider.setDegrees(mConfigurationProvider.getDeviceDefaultOrientation() == Configuration.ORIENTATION_PORTRAIT ? 270 : 0);
                        }
                    }
                    onScreenRotation(mConfigurationProvider.getDegrees());
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };
    private CameraFragmentResultListener mCameraFragmentResultListener;

    /**
     * Camera controls
     */

    @BindView(R.id.cameraControlView)
    RelativeLayout mCameraControlView;

    /**
     * Preview controls
     */
    @BindView(R.id.previewControlPanelView)
    RelativeLayout mPreviewControlPanelView;

    @BindView(R.id.previewImageView)
    PhotoView mPreviewImageView;

   /* @BindView(R.id.settings_view)
    CameraSettingsView mSettingsView;*/

    @BindView(R.id.flash_switch_view)
    FlashSwitchView mFlashSwitchView;

    @BindView(R.id.front_back_camera_switcher)
    CameraSwitchView mCameraSwitchView;

    @BindView(R.id.record_button)
    RecordButton mRecordButton;

    @BindView(R.id.photo_video_camera_switcher)
    MediaActionSwitchView mMediaActionSwitchView;

    @BindView(R.id.record_duration_text)
    TextView mRecordDurationText;

    @BindView(R.id.record_size_mb_text)
    TextView mRecordSizeText;

    @BindView(R.id.cameraLayout)
    View mCameraLayout;

    @BindView(R.id.addCameraButton)
    View mAddCameraButton;

    private String mPathFile;
    private String mFileName;
    private int mRequestCode;

    private boolean isOnRecordingClicked = false;

    private boolean isLaunchCrop;

    public static void show(@NonNull FragmentManager fm, Configuration configuration, int requestCode) {
        final CameraDialog dialog = new CameraDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Args.ARG_CONFIGURATION, configuration);
        bundle.putInt(Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, CameraDialog.class.getName());
    }

    public static void show(@NonNull FragmentManager fm, Configuration configuration, int requestCode, Bundle bundle) {
        final CameraDialog dialog = new CameraDialog();

        bundle.putSerializable(Args.ARG_CONFIGURATION, configuration);
        bundle.putInt(Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, CameraDialog.class.getName());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        this.mConfigurationProvider = new ConfigurationProviderImpl();
        this.mConfigurationProvider.setupWithAnnaConfiguration(mConfiguration);

        this.mSensorManager = (SensorManager) getContext().getSystemService(Activity.SENSOR_SERVICE);

        final CameraView cameraView = new CameraView() {

            @Override
            public void updateCameraPreview(Size size, View cameraPreview) {
                if (mCameraFragmentControlsListener != null) {
                    mCameraFragmentControlsListener.unLockControls();
                    mCameraFragmentControlsListener.allowRecord(true);
                }

                setCameraPreview(cameraPreview, size);
            }

            @Override
            public void updateUiForMediaAction(@Configuration.MediaAction int mediaAction) {

            }

            @Override
            public void updateCameraSwitcher(int numberOfCameras) {
                if (mCameraFragmentControlsListener != null) {
                    mCameraFragmentControlsListener.allowCameraSwitching(numberOfCameras > 1);
                }
            }

            @Override
            public void onPhotoTaken(byte[] bytes, CameraFragmentResultListener callback) {
                final String filePath = mCameraController.getOutputFile().toString();
                if (mCameraFragmentResultListener != null) {
                    mCameraFragmentResultListener.onPhotoTaken(bytes, filePath);
                }
                if (callback != null) {
                    callback.onPhotoTaken(bytes, filePath);
                }
            }

            @Override
            public void onVideoRecordStart(int width, int height) {
                final File outputFile = mCameraController.getOutputFile();
                onStartVideoRecord(outputFile);
            }

            @Override
            public void onVideoRecordStop(@Nullable CameraFragmentResultListener callback) {
//                BaseAnncaFragment.this.onStopVideoRecord(callback);
            }

            @Override
            public void releaseCameraPreview() {
                clearCameraPreview();
            }
        };

//        if (CameraHelper.hasCamera2(getContext())) {
            mCameraController = new Camera2Controller(getContext(), cameraView, mConfigurationProvider);
//        } else {
//            mCameraController = new Camera1Controller(getContext(), cameraView, mConfigurationProvider);
//        }
        mCameraController.onCreate(savedInstanceState);

        //onProcessBundle
        mCurrentMediaActionState = mConfigurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_VIDEO ?
                MediaAction.ACTION_VIDEO : MediaAction.ACTION_PHOTO;
    }

    private void initArgs() {
        final Bundle arguments = getArguments();
        if (arguments != null) {
            mConfiguration = (Configuration) arguments.getSerializable(Args.ARG_CONFIGURATION);
            mRequestCode = arguments.getInt(Args.REQUEST_CODE);
            mFileName = arguments.getString(Args.FILE_NAME);
            mPathFile = arguments.getString(Args.PATH_FILE_SAVE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View decorView = getActivity().getWindow().getDecorView();
        if (Build.VERSION.SDK_INT > MIN_VERSION_ICECREAM) {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_camera);
        hideToolbar();
        setTitleText(CameraDialog.class.getSimpleName());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPreviewContainer = (AspectFrameLayout) view.findViewById(R.id.previewContainer);

        final int defaultOrientation = Utils.getDeviceDefaultOrientation(getContext());
        switch (defaultOrientation) {
            case android.content.res.Configuration.ORIENTATION_LANDSCAPE: {
                mConfigurationProvider.setDeviceDefaultOrientation(Configuration.ORIENTATION_LANDSCAPE);
                break;
            }
            default: {
                mConfigurationProvider.setDeviceDefaultOrientation(Configuration.ORIENTATION_PORTRAIT);
                break;
            }
        }

        switch (mConfigurationProvider.getFlashMode()) {
            case Configuration.FLASH_MODE_AUTO: {
                setFlashMode(Flash.FLASH_AUTO);
                break;
            }
            case Configuration.FLASH_MODE_ON: {
                setFlashMode(Flash.FLASH_ON);
                break;
            }
            case Configuration.FLASH_MODE_OFF: {
                setFlashMode(Flash.FLASH_OFF);
                break;
            }
        }

        if (mCameraFragmentControlsListener != null) {
            setMaxVideoDuration(mConfigurationProvider.getVideoDuration());
            setMaxVideoFileSize(mConfigurationProvider.getVideoFileSize());
        }

        setCameraTypeFrontBack(mConfigurationProvider.getCameraFace());

        notifyListeners();
        onAddCameraClicked();
    }

    public void notifyListeners() {
        onFlashModeChanged();
        onActionPhotoVideoChanged();
        onCameraTypeFrontBackChanged();
    }

    @Override
    public void takePhotoOrCaptureVideo(final CameraFragmentResultListener resultListener, @Nullable String directoryPath, @Nullable String fileName) {
        switch (mCurrentMediaActionState) {
            case MediaAction.ACTION_PHOTO: {
                takePhoto(resultListener, directoryPath, fileName);
                break;
            }
            case MediaAction.ACTION_VIDEO: {
                switch (mCurrentRecordState) {
                    case Record.RECORD_IN_PROGRESS_STATE: {
                        stopRecording(resultListener);
                        break;
                    }
                    default: {
                        startRecording(directoryPath, fileName);
                        break;
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mCameraController.onResume();
        mSensorManager.registerListener(mSensorEventListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.lockControls();
            mCameraFragmentControlsListener.allowRecord(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mCameraController.onPause();
        mSensorManager.unregisterListener(mSensorEventListener);

        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.lockControls();
            mCameraFragmentControlsListener.allowRecord(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mCameraController.onDestroy();
    }

    protected void setMaxVideoFileSize(long maxVideoFileSize) {
        this.mMaxVideoFileSize = maxVideoFileSize;
    }

    protected void setMaxVideoDuration(int maxVideoDurationInMillis) {
        if (maxVideoDurationInMillis > 0) {
            this.mCountDownTimer = new CountdownTask(mTimerCallBack, maxVideoDurationInMillis);
        } else {
            this.mCountDownTimer = new TimerTask(mTimerCallBack);
        }
    }

    @Override
    public void openSettingDialog() {
        final Context context = getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (mCurrentMediaActionState == MediaAction.ACTION_VIDEO) {
            builder.setSingleChoiceItems(mVideoQualities, getVideoOptionCheckedIndex(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int index) {
                    mNewQuality = ((VideoQualityOption) mVideoQualities[index]).getMediaQuality();
                }
            });
            if (mConfigurationProvider.getVideoFileSize() > 0)
                builder.setTitle(String.format(getString(R.string.settings_video_quality_title),
                        "(Max " + String.valueOf(mConfigurationProvider.getVideoFileSize() / (1024 * 1024) + " MB)")));
            else
                builder.setTitle(String.format(getString(R.string.settings_video_quality_title), ""));
        } else {
            builder.setSingleChoiceItems(mPhotoQualities, getPhotoOptionCheckedIndex(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int index) {
                    mNewQuality = ((PhotoQualityOption) mPhotoQualities[index]).getMediaQuality();
                }
            });
            builder.setTitle(R.string.settings_photo_quality_title);
        }

        builder.setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (mNewQuality > 0 && mNewQuality != mConfigurationProvider.getMediaQuality()) {
                    mConfigurationProvider.setMediaQuality(mNewQuality);
                    dialogInterface.dismiss();
                    if (mCameraFragmentControlsListener != null) {
                        mCameraFragmentControlsListener.lockControls();
                    }
                    mCameraController.switchQuality();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        mSettingsDialog = builder.create();
        mSettingsDialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(mSettingsDialog.getWindow().getAttributes());
        layoutParams.width = Utils.convertDipToPixels(context, 350);
        layoutParams.height = Utils.convertDipToPixels(context, 350);
        mSettingsDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void switchCameraTypeFrontBack() {
        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.lockControls();
            mCameraFragmentControlsListener.allowRecord(false);
        }

        int cameraFace = Configuration.CAMERA_FACE_REAR;
        switch (mCurrentCameraType) {
            case Camera.CAMERA_TYPE_FRONT: {
                mCurrentCameraType = Camera.CAMERA_TYPE_REAR;
                cameraFace = Configuration.CAMERA_FACE_REAR;
                break;
            }
            case Camera.CAMERA_TYPE_REAR: {
                mCurrentCameraType = Camera.CAMERA_TYPE_FRONT;
                cameraFace = Configuration.CAMERA_FACE_FRONT;
                break;
            }
        }

        onCameraTypeFrontBackChanged();
        this.mCameraController.switchCamera(cameraFace);

        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.unLockControls();
        }
    }

    protected void setCameraTypeFrontBack(@Configuration.CameraFace int cameraFace) {
        switch (cameraFace) {
            case Configuration.CAMERA_FACE_FRONT: {
                mCurrentCameraType = Camera.CAMERA_TYPE_FRONT;
                cameraFace = Configuration.CAMERA_FACE_FRONT;
                break;
            }
            case Configuration.CAMERA_FACE_REAR: {
                mCurrentCameraType = Camera.CAMERA_TYPE_REAR;
                cameraFace = Configuration.CAMERA_FACE_REAR;
                break;
            }
        }
        onCameraTypeFrontBackChanged();
        this.mCameraController.switchCamera(cameraFace);

    }

    protected void onCameraTypeFrontBackChanged() {
        if (mCameraFragmentStateListener != null) {
            switch (mCurrentCameraType) {
                case Camera.CAMERA_TYPE_REAR: {
                    mCameraFragmentStateListener.onCurrentCameraBack();
                    break;
                }
                case Camera.CAMERA_TYPE_FRONT: {
                    mCameraFragmentStateListener.onCurrentCameraFront();
                    break;
                }
            }
        }
    }

    @Override
    public void switchActionPhotoVideo() {
        switch (mCurrentMediaActionState) {
            case MediaAction.ACTION_PHOTO: {
                mCurrentMediaActionState = MediaAction.ACTION_VIDEO;
                break;
            }
            case MediaAction.ACTION_VIDEO: {
                mCurrentMediaActionState = MediaAction.ACTION_PHOTO;
                break;
            }
        }
        onActionPhotoVideoChanged();
    }

    protected void onActionPhotoVideoChanged() {
        if (mCameraFragmentStateListener != null) {
            switch (mCurrentMediaActionState) {
                case MediaAction.ACTION_VIDEO: {
                    mCameraFragmentStateListener.onCameraSetupForVideo();
                    break;
                }
                case MediaAction.ACTION_PHOTO: {
                    mCameraFragmentStateListener.onCameraSetupForPhoto();
                    break;
                }
            }
        }
    }

    @Override
    public void toggleFlashMode() {
        switch (mCurrentFlashMode) {
            case Flash.FLASH_AUTO: {
                mCurrentFlashMode = Flash.FLASH_OFF;
                break;
            }
            case Flash.FLASH_OFF: {
                mCurrentFlashMode = Flash.FLASH_ON;
                break;
            }
            case Flash.FLASH_ON: {
                mCurrentFlashMode = Flash.FLASH_AUTO;
                break;
            }
        }
        onFlashModeChanged();
    }

    private void onFlashModeChanged() {
        switch (mCurrentFlashMode) {
            case Flash.FLASH_AUTO: {
                if (mCameraFragmentStateListener != null)
                    mCameraFragmentStateListener.onFlashAuto();
                mConfigurationProvider.setFlashMode(Configuration.FLASH_MODE_AUTO);
                this.mCameraController.setFlashMode(Configuration.FLASH_MODE_AUTO);
                break;
            }
            case Flash.FLASH_ON: {
                if (mCameraFragmentStateListener != null) mCameraFragmentStateListener.onFlashOn();
                mConfigurationProvider.setFlashMode(Configuration.FLASH_MODE_ON);
                this.mCameraController.setFlashMode(Configuration.FLASH_MODE_ON);
                break;
            }
            case Flash.FLASH_OFF: {
                if (mCameraFragmentStateListener != null) mCameraFragmentStateListener.onFlashOff();
                mConfigurationProvider.setFlashMode(Configuration.FLASH_MODE_OFF);
                this.mCameraController.setFlashMode(Configuration.FLASH_MODE_OFF);
                break;
            }
        }
    }

    protected void onScreenRotation(int degrees) {
        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.shouldRotateControls(degrees);
        }
        rotateSettingsDialog(degrees);
    }

    protected void setRecordState(@Record.RecordState int recordState) {
        this.mCurrentRecordState = recordState;
    }

    //@Override
    //public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //    if (resultCode == Activity.RESULT_OK) {
    //        if (requestCode == REQUEST_PREVIEW_CODE) {
    //            final FragmentActivity activity = getActivity();
    //            if (activity != null) {
    //                if (PreviewActivity.isResultConfirm(data)) {
    //                    Intent resultIntent = new Intent();
    //                    resultIntent.putExtra(Configuration.Arguments.FILE_PATH,
    //                            PreviewActivity.getMediaFilePatch(data));
    //                    activity.setResult(Activity.RESULT_OK, resultIntent);
    //                    activity.finish();
    //                } else if (PreviewActivity.isResultCancel(data)) {
    //                    activity.setResult(Activity.RESULT_CANCELED);
    //                    activity.finish();
    //                } else if (PreviewActivity.isResultRetake(data)) {
    //                    //ignore, just proceed the camera
    //                }
    //            }
    //        }
    //    }
    //}

    protected void setFlashMode(@Flash.FlashMode int mode) {
        this.mCurrentFlashMode = mode;
        onFlashModeChanged();
    }

    protected void rotateSettingsDialog(int degrees) {
        if (mSettingsDialog != null && mSettingsDialog.isShowing() && Build.VERSION.SDK_INT > 10) {
            ViewGroup dialogView = (ViewGroup) mSettingsDialog.getWindow().getDecorView();
            for (int i = 0; i < dialogView.getChildCount(); i++) {
                dialogView.getChildAt(i).setRotation(degrees);
            }
        }
    }

    protected int getVideoOptionCheckedIndex() {
        int checkedIndex = -1;

        final int mediaQuality = mConfigurationProvider.getMediaQuality();
        final int passedMediaQuality = mConfigurationProvider.getPassedMediaQuality();

        if (mediaQuality == Configuration.MEDIA_QUALITY_AUTO) checkedIndex = 0;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_HIGH) checkedIndex = 1;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_MEDIUM) checkedIndex = 2;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_LOW) checkedIndex = 3;

        if (passedMediaQuality != Configuration.MEDIA_QUALITY_AUTO) checkedIndex--;

        return checkedIndex;
    }

    protected int getPhotoOptionCheckedIndex() {
        int checkedIndex = -1;

        final int mediaQuality = mConfigurationProvider.getMediaQuality();

        if (mediaQuality == Configuration.MEDIA_QUALITY_HIGHEST) checkedIndex = 0;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_HIGH) checkedIndex = 1;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_MEDIUM) checkedIndex = 2;
        else if (mediaQuality == Configuration.MEDIA_QUALITY_LOWEST) checkedIndex = 3;
        return checkedIndex;
    }

    protected void takePhoto(CameraFragmentResultListener callback, @Nullable String directoryPath, @Nullable String fileName) {
        if (Build.VERSION.SDK_INT > MIN_VERSION_ICECREAM) {
            new MediaActionSound().play(MediaActionSound.SHUTTER_CLICK);
        }
        setRecordState(Record.TAKE_PHOTO_STATE);
        this.mCameraController.takePhoto(callback, directoryPath, fileName);
        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.onRecordStatePhoto();
        }
    }

    protected void startRecording(@Nullable String directoryPath, @Nullable String fileName) {
        if (Build.VERSION.SDK_INT > MIN_VERSION_ICECREAM) {
            new MediaActionSound().play(MediaActionSound.START_VIDEO_RECORDING);
        }

        setRecordState(Record.RECORD_IN_PROGRESS_STATE);
        this.mCameraController.startVideoRecord(directoryPath, fileName);

        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.onRecordStateVideoInProgress();
        }
    }

    protected void stopRecording(CameraFragmentResultListener callback) {
        if (Build.VERSION.SDK_INT > MIN_VERSION_ICECREAM) {
            new MediaActionSound().play(MediaActionSound.STOP_VIDEO_RECORDING);
        }

        setRecordState(Record.READY_FOR_RECORD_STATE);
        this.mCameraController.stopVideoRecord(callback);

        this.onStopVideoRecord(callback);

        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.onRecordStateVideoReadyForRecord();
        }
    }

    protected void clearCameraPreview() {
        if (mPreviewContainer != null)
            mPreviewContainer.removeAllViews();
    }

    protected void setCameraPreview(View preview, Size previewSize) {
        //onCameraControllerReady()
        mVideoQualities = mCameraController.getVideoQualityOptions();
        mPhotoQualities = getPhotoQualityOptions();

        if (mPreviewContainer == null || preview == null) return;
        mPreviewContainer.removeAllViews();
        mPreviewContainer.addView(preview);

        mPreviewContainer.setAspectRatio(previewSize.getHeight() / (double) previewSize.getWidth());
    }

    protected void setMediaFilePath(final File mediaFile) {
        this.mMediaFilePath = mediaFile.toString();
    }

    protected void onStartVideoRecord(final File mediaFile) {
        setMediaFilePath(mediaFile);
        if (mMaxVideoFileSize > 0) {

            if (mCameraFragmentVideoRecordTextListener != null) {
                mCameraFragmentVideoRecordTextListener.setRecordSizeText(mMaxVideoFileSize, "1Mb" + " / " + mMaxVideoFileSize / (1024 * 1024) + "Mb");
                mCameraFragmentVideoRecordTextListener.setRecordSizeTextVisible(true);
            }
            try {
                mFileObserver = new FileObserver(this.mMediaFilePath) {
                    private long mLastUpdateSize = 0;

                    @Override
                    public void onEvent(int event, String path) {
                        final long fileSize = mediaFile.length() / (1024 * 1024);
                        if ((fileSize - mLastUpdateSize) >= 1) {
                            mLastUpdateSize = fileSize;
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mCameraFragmentVideoRecordTextListener != null) {
                                        mCameraFragmentVideoRecordTextListener.setRecordSizeText(mMaxVideoFileSize, fileSize + "Mb" + " / " + mMaxVideoFileSize / (1024 * 1024) + "Mb");
                                    }
                                }
                            });
                        }
                    }
                };
                mFileObserver.startWatching();
            } catch (Exception e) {
                LoggerUtils.exception(e);
            }
        }

        if (mCountDownTimer == null) {
            this.mCountDownTimer = new TimerTask(mTimerCallBack);
        }
        mCountDownTimer.start();

        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.onStartVideoRecord(mediaFile);
        }
    }

    protected void onStopVideoRecord(@Nullable CameraFragmentResultListener callback) {
        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.allowRecord(false);
        }
        if (mCameraFragmentStateListener != null) {
            mCameraFragmentStateListener.onStopVideoRecord();
        }
        setRecordState(Record.READY_FOR_RECORD_STATE);

        if (mFileObserver != null)
            mFileObserver.stopWatching();

        if (mCountDownTimer != null) {
            mCountDownTimer.stop();
        }

        final int mediaAction = mConfigurationProvider.getMediaAction();
        if (mCameraFragmentControlsListener != null) {
            if (mediaAction != Configuration.MEDIA_ACTION_UNSPECIFIED) {
                mCameraFragmentControlsListener.setMediaActionSwitchVisible(false);
            } else {
                mCameraFragmentControlsListener.setMediaActionSwitchVisible(true);
            }
        }

        final String filePath = this.mCameraController.getOutputFile().toString();
        if (mCameraFragmentResultListener != null) {
            mCameraFragmentResultListener.onVideoRecorded(filePath);
        }

        if (callback != null) {
            callback.onVideoRecorded(filePath);
        }
    }

    @Override
    public void setStateListener(CameraFragmentStateListener cameraFragmentStateListener) {
        this.mCameraFragmentStateListener = cameraFragmentStateListener;
    }

    @Override
    public void setTextListener(CameraFragmentVideoRecordTextListener cameraFragmentVideoRecordTextListener) {
        this.mCameraFragmentVideoRecordTextListener = cameraFragmentVideoRecordTextListener;
    }

    @Override
    public void setControlsListener(CameraFragmentControlsListener cameraFragmentControlsListener) {
        this.mCameraFragmentControlsListener = cameraFragmentControlsListener;
    }

    @Override
    public void setResultListener(CameraFragmentResultListener cameraFragmentResultListener) {
        this.mCameraFragmentResultListener = cameraFragmentResultListener;
    }

    @OnClick(R.id.flash_switch_view)
    public void onFlashSwitcClicked() {
        toggleFlashMode();
    }

    @OnClick(R.id.front_back_camera_switcher)
    public void onSwitchCameraClicked() {
        switchCameraTypeFrontBack();
    }

    @OnClick(R.id.record_button)
    public void onRecordButtonClicked() {
        if (isOnRecordingClicked) {
            return;
        }
        isOnRecordingClicked = true;

        File dir = getContext().getCacheDir();
        takePhotoOrCaptureVideo(new CameraFragmentResultAdapter() {
                                    @Override
                                    public void onVideoRecorded(String filePath) {
                                        showMessage("onVideoRecorded");
                                        isOnRecordingClicked = false;
                                    }

                                    @Override
                                    public void onPhotoTaken(byte[] bytes, String filePath) {
                                        mPathFile = filePath;
                                        mCameraControlView.setVisibility(View.GONE);

                                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                                        try {
                                            //https://stackoverflow.com/questions/3647993/android-bitmaps-loaded-from-gallery-are-rotated-in-imageview
                                            //http://sylvana.net/jpegcrop/exif_orientation.html
                                            ExifInterface exif = new ExifInterface(filePath);
                                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

                                            switch (orientation) {
                                                case 7 :
                                                case 8 : {
                                                    Matrix matrix = new Matrix();
                                                    matrix.postRotate(-90);
                                                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                                    break;
                                                }
                                                case 4 :
                                                case 3 : {
                                                    Matrix matrix = new Matrix();
                                                    matrix.postRotate(180);
                                                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                                    break;
                                                }
                                                case 5 :
                                                case 6 : {
                                                    Matrix matrix = new Matrix();
                                                    matrix.postRotate(90);
                                                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                                    break;
                                                }
                                            }
                                        } catch (IOException e) {
                                            LoggerUtils.exception(e);;
                                        }



//                                        BitmapFactory.Options options = new BitmapFactory.Options();
//                                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//                                        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

                                        if (mRequestCode == IMainActivity.ACTION_IMAGE_FROM_CAMERA_AND_CROP) {
                                            cropImage();
                                        } else {
                                            mPreviewImageView.setImageBitmap(bitmap);
                                            mPreviewControlPanelView.setVisibility(View.VISIBLE);
                                        }

                                        isOnRecordingClicked = false;
                                    }
                                },
                dir.getPath(),
                null);
    }

    /*@OnClick(R.id.settings_view)
    public void onSettingsClicked() {
        openSettingDialog();
    }*/

    @OnClick(R.id.photo_video_camera_switcher)
    public void onMediaActionSwitchClicked() {
        switchActionPhotoVideo();
    }

    @OnClick(R.id.addCameraButton)
    public void onAddCameraClicked() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.CAMERA,
//                    Manifest.permission.RECORD_AUDIO,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                    Manifest.permission.READ_EXTERNAL_STORAGE
            };

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                String[] permissions1 = permissionsToRequest.toArray(new String[permissionsToRequest.size()]);
                ActivityCompat.requestPermissions(getActivity(), permissions1, IMainActivity.REQUEST_CAMERA_PERMISSION);
            } else {
                addCamera();
            }

            addCamera();
        } else {
            addCamera();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length != 0) {
            addCamera();
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void addCamera() {
        mAddCameraButton.setVisibility(View.GONE);
        mCameraLayout.setVisibility(View.VISIBLE);

        setStateListener(new CameraFragmentStateAdapter() {

            @Override
            public void onCurrentCameraBack() {
                mCameraSwitchView.displayBackCamera();
            }

            @Override
            public void onCurrentCameraFront() {
                mCameraSwitchView.displayFrontCamera();
            }

            @Override
            public void onFlashAuto() {
                mFlashSwitchView.displayFlashAuto();
            }

            @Override
            public void onFlashOn() {
                mFlashSwitchView.displayFlashOn();
            }

            @Override
            public void onFlashOff() {
                mFlashSwitchView.displayFlashOff();
            }

            @Override
            public void onCameraSetupForPhoto() {
                mMediaActionSwitchView.displayActionWillSwitchVideo();

                mRecordButton.displayPhotoState();
                mFlashSwitchView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCameraSetupForVideo() {
                mMediaActionSwitchView.displayActionWillSwitchPhoto();

                mRecordButton.displayVideoRecordStateReady();
                mFlashSwitchView.setVisibility(View.GONE);
            }

            @Override
            public void shouldRotateControls(int degrees) {
                ViewCompat.setRotation(mCameraSwitchView, degrees);
                ViewCompat.setRotation(mMediaActionSwitchView, degrees);
                ViewCompat.setRotation(mFlashSwitchView, degrees);
                ViewCompat.setRotation(mRecordDurationText, degrees);
                ViewCompat.setRotation(mRecordSizeText, degrees);
            }

            @Override
            public void onRecordStateVideoReadyForRecord() {
                mRecordButton.displayVideoRecordStateReady();
            }

            @Override
            public void onRecordStateVideoInProgress() {
                mRecordButton.displayVideoRecordStateInProgress();
            }

            @Override
            public void onRecordStatePhoto() {
                mRecordButton.displayPhotoState();
            }

            @Override
            public void onStopVideoRecord() {
                mRecordSizeText.setVisibility(View.GONE);
                //mCameraSwitchView.setVisibility(View.VISIBLE);
//                mSettingsView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStartVideoRecord(File outputFile) {
            }
        });

        setControlsListener(new CameraFragmentControlsAdapter() {
            @Override
            public void lockControls() {
                mCameraSwitchView.setEnabled(false);
                mRecordButton.setEnabled(false);
//                mSettingsView.setEnabled(false);
                mFlashSwitchView.setEnabled(false);
            }

            @Override
            public void unLockControls() {
                mCameraSwitchView.setEnabled(true);
                mRecordButton.setEnabled(true);
//                mSettingsView.setEnabled(true);
                mFlashSwitchView.setEnabled(true);
            }

            @Override
            public void allowCameraSwitching(boolean allow) {
                mCameraSwitchView.setVisibility(allow ? View.VISIBLE : View.GONE);
            }

            @Override
            public void allowRecord(boolean allow) {
                mRecordButton.setEnabled(allow);
            }

            @Override
            public void setMediaActionSwitchVisible(boolean visible) {
                mMediaActionSwitchView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
            }
        });

        setTextListener(new CameraFragmentVideoRecordTextAdapter() {
            @Override
            public void setRecordSizeText(long size, String text) {
                mRecordSizeText.setText(text);
            }

            @Override
            public void setRecordSizeTextVisible(boolean visible) {
                mRecordSizeText.setVisibility(visible ? View.VISIBLE : View.GONE);
            }

            @Override
            public void setRecordDurationText(String text) {
                mRecordDurationText.setText(text);
            }

            @Override
            public void setRecordDurationTextVisible(boolean visible) {
                mRecordDurationText.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * Preview controls
     */

    @OnClick(R.id.cancel_media_action)
    public void cancelMediaAction() {
        dismiss();
    }

    @OnClick(R.id.crop_image)
    public void cropImage() {
        isLaunchCrop = true;

        File file = new File(mPathFile);
        Uri inputUri = Uri.fromFile(file);

        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yalantis.ucrop.OutputUri", inputUri);
        bundle.putParcelable("com.yalantis.ucrop.InputUri", inputUri);

//        bundle.putInt("com.yalantis.ucrop.StatusBarColor", ContextCompat.getColor(getContext(), R.color.primary_dark));
//        bundle.putInt("com.yalantis.ucrop.ToolbarColor", ContextCompat.getColor(getContext(), R.color.primary));
//        bundle.putInt("com.yalantis.ucrop.UcropColorWidgetActive", ContextCompat.getColor(getContext(), R.color.primary));

//        bundle.putString("com.yalantis.ucrop.CompressionFormatName", String.valueOf(Bitmap.CompressFormat.JPEG));
//        bundle.putString("com.yalantis.ucrop.CompressionFormatName", String.valueOf(Bitmap.CompressFormat.PNG));
        bundle.putInt("com.yalantis.ucrop.CompressionQuality", 100);

//        bundle.putInt("com.yalantis.ucrop.MaxSizeX", Preference.getParam(UtopiaSettings.Key.AVATAR_WIDTH, UtopiaSettings.Default.AVATAR_WIDTH));
        bundle.putInt("com.yalantis.ucrop.MaxSizeX", ClientPrefOptions.Defaults.AVATAR_WIDTH);
//        bundle.putInt("com.yalantis.ucrop.MaxSizeY", Preference.getParam(UtopiaSettings.Key.AVATAR_HEIGHT, UtopiaSettings.Default.AVATAR_HEIGHT));
        bundle.putInt("com.yalantis.ucrop.MaxSizeY", ClientPrefOptions.Defaults.AVATAR_HEIGHT);

        CropImageDialog.show(getFragmentManager(), bundle, mRequestCode);

        dismiss();
    }

    @OnClick(R.id.re_take_media)
    public void reTakePicture() {
        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.lockControls();
            mCameraFragmentControlsListener.allowRecord(false);
        }
        CameraManager cameraManager = mCameraController.getCameraManager();
        cameraManager.closeCamera((CameraCloseListener) mCameraController);
        if (mCameraFragmentControlsListener != null) {
            mCameraFragmentControlsListener.unLockControls();
        }

        mCameraControlView.setVisibility(View.VISIBLE);
        mPreviewControlPanelView.setVisibility(View.GONE);
        mPreviewImageView.setImageBitmap(null);
    }

    @OnClick(R.id.confirm_media_result)
    public void confirmResult() {
        onPositiveClickButton();
    }

    @Override
    public void onPositiveClickButton() {
        Intent intent = new Intent();
        intent.putExtra(Args.PATH_FILE_SAVE, mPathFile);

        IScreenActivityView iScreenActivityView = getIScreenActivityView();
        iScreenActivityView.onFragmentResult(mRequestCode, Activity.RESULT_OK, intent);

        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (!isLaunchCrop) {
            requireActivity().finish();
        }
    }

    private CharSequence[] getPhotoQualityOptions() {
        CharSequence[] options;

//        String key = this.getClass().getSimpleName() + "PhotoQualities";
////        String params = Preference.getParam(key, "");
//        if (!TextUtils.isEmpty(params)) {
//            String[] options1 = params.split(";");
//            options = new CharSequence[options1.length];
//
//            for (int i = 0; i < options1.length; i++) {
//                String option = options1[i];
//                String[] parametrs = option.split(":");
//                int quality = Integer.parseInt(parametrs[0]);
//
//                String[] qualities = parametrs[1].split(" x ");
//                Size size = new Size(Integer.parseInt(qualities[0]), Integer.parseInt(qualities[1]));
//                options[i] = new PhotoQualityOption(quality, size);
//            }
//        } else {
            options = mCameraController.getPhotoQualityOptions();
//            for (CharSequence option : options) {
//                PhotoQualityOption p = (PhotoQualityOption) option;
//                params += p.getMediaQuality() + ":" + p.toString() + ";";
//            }
//            Preference.setParam(key, params);
//        }

        return null;
    }

    public interface Args {
        String ARG_CONFIGURATION = "ARG_CONFIGURATION";
        String REQUEST_CODE = "REQUEST_CODE";

        String PATH_FILE_SAVE = "PATH_FILE_SAVE";
        String FILE_NAME = "FILE_NAME";
    }

    public interface Body {
        String TEMP_FILE_NAME = "TEMP_FILE_NAME.jpeg";
    }
}
