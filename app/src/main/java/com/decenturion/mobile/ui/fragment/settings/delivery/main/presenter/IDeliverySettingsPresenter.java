package com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.view.IDeliverySettingsView;

public interface IDeliverySettingsPresenter extends IPresenter<IDeliverySettingsView> {

    void bindViewData();

    void setDeliveryData(@NonNull String value,
                         @NonNull String value1,
                         @NonNull String value2,
                         @NonNull String value3,
                         @NonNull String selection,
                         @NonNull String value4);

    void getPaymentDate(@NonNull String coin);
}
