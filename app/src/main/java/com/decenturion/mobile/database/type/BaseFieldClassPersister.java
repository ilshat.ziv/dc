package com.decenturion.mobile.database.type;

import com.decenturion.mobile.utils.JsonUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

public abstract class BaseFieldClassPersister<T> extends StringType {

    private Class<T> mTypeFieldClass;

    protected BaseFieldClassPersister(Class<T> typeFieldClass) {
        super(SqlType.STRING, new Class<?>[] { typeFieldClass.getClass() });
        mTypeFieldClass = typeFieldClass;
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) {
        try {
            T myFieldClass = (T) javaObject;
            return myFieldClass != null ? getJsonFromMyFieldClass(myFieldClass) : null;
        } catch (Exception e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return sqlArg != null ? getMyFieldClassFromJson((String) sqlArg) : null;
    }

    private String getJsonFromMyFieldClass(T myFieldClass) {
        return JsonUtils.getJsonString(myFieldClass);
    }

    private T getMyFieldClassFromJson(String json) {
        return (T) JsonUtils.getObjectFromString(json, mTypeFieldClass);
    }
}
