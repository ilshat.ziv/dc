package com.decenturion.mobile.ui.fragment.token.info.v2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.token.info.v2.model.InfoParamsModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenInfoParamAdapter extends RecyclerView.Adapter<TokenInfoParamAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<InfoParamsModelView> mInfoParamsModelViews;

    public TokenInfoParamAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void replaceDataSet(List<InfoParamsModelView> list) {
        mInfoParamsModelViews = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mInfoParamsModelViews);
    }

    @NonNull
    @Override
    public TokenInfoParamAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_info_param_item2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TokenInfoParamAdapter.ViewHolder holder, int position) {
        final InfoParamsModelView param = mInfoParamsModelViews.get(position);
        holder.bind(param);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        @BindView(R.id.infoParamNameView)
        TextView textView;

        InfoParamsModelView infoModelView;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public void bind(@NonNull InfoParamsModelView model) {
            this.infoModelView = model;
            this.textView.setText(model.getName());
            if (!model.isActive()) {
                this.view.setAlpha(0.3f);
            } else {
                this.view.setAlpha(1f);
            }
        }
    }

}
