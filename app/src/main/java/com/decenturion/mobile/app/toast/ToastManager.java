package com.decenturion.mobile.app.toast;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class ToastManager implements IToastManager {

    private Context mContext;
    private Toast mToast;

    public ToastManager(@NonNull Context context) {
        mContext = context;
    }

    private Toast showNormal(@NonNull String message, int duration, Drawable drawable) {
        if (drawable != null) {
            mToast = Toasty.normal(mContext, message, duration, drawable);
        } else {
            mToast = Toasty.normal(mContext, message, duration);
        }
        mToast.show();
        return mToast;
    }

    private Toast showSuccess(@NonNull String message, int duration, boolean showIcon) {
        mToast = Toasty.success(mContext, message, duration, showIcon);
        mToast.show();
        return mToast;
    }

    private Toast showInfo(@NonNull String message, int duration, boolean showIcon) {
        mToast = Toasty.info(mContext, message, duration, showIcon);
        mToast.show();
        return mToast;
    }

    private Toast showWarning(@NonNull String message, int duration, boolean showIcon) {
        mToast = Toasty.warning(mContext, message, duration, showIcon);
        mToast.show();
        return mToast;
    }

    private Toast showError(@NonNull String message, int duration, boolean showIcon) {
        mToast = Toasty.error(mContext, message, duration, showIcon);
//        mToast.setGravity(Gravity.BOTTOM, 0, 0);
        mToast.show();
        return mToast;
    }


    @Override
    public void showShortToast(@NonNull TypeMessage type, @NonNull String message) {
        if (!isMessageValid(message)) {
            return;
        }

        cancelToast();

        switch (type) {
            case Normal : {
                mToast = showNormal(message, Toast.LENGTH_SHORT, null);
                break;
            }
            case Success : {
                mToast = showSuccess(message, Toast.LENGTH_SHORT, true);
                break;
            }
            case Info : {
                mToast = showInfo(message, Toast.LENGTH_SHORT, true);
                break;
            }
            case Warning : {
                mToast = showWarning(message, Toast.LENGTH_SHORT, true);
                break;
            }
            case Error : {
                mToast = showError(message, Toast.LENGTH_SHORT, true);
                break;
            }
        }
    }

    private boolean isMessageValid(String message) {
        return !TextUtils.isEmpty(message)
                && !TextUtils.equals(message, "thread interrupted");
    }

    @Override
    public void showLongToast(@NonNull TypeMessage type, @NonNull String message) {
        cancelToast();

        switch (type) {
            case Normal : {
                mToast = showNormal(message, Toast.LENGTH_LONG, null);
                break;
            }
            case Success : {
                mToast = showSuccess(message, Toast.LENGTH_LONG, true);
                break;
            }
            case Info : {
                mToast = showInfo(message, Toast.LENGTH_LONG, true);
                break;
            }
            case Warning : {
                mToast = showWarning(message, Toast.LENGTH_LONG, true);
                break;
            }
            case Error : {
                mToast = showError(message, Toast.LENGTH_LONG, true);
                break;
            }
        }
    }

    @Override
    public void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }
}
