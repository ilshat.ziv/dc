package com.decenturion.mobile.di.dagger.traids;

import com.decenturion.mobile.ui.fragment.profile_v2.deals.view.DealListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TraidsModule.class
})

@TraidsScope
public interface TraidsComponent {

    void inject(DealListFragment view);
}
