package com.decenturion.mobile.di.dagger.settings.delivery.delivered;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.delivered.DeliveredSettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.delivered.IDeliveredSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter.DeliveredSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter.IDeliveredSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DeliveredSettingsModule {

    @Provides
    @DeliveredSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @DeliveredSettingsScope
    IDeliveredSettingsInteractor provideIDeliveredSettingsInteractor(IResidentClient iResidentClient,
                                                                     IResidentRepository iResidentRepository) {
        return new DeliveredSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @DeliveredSettingsScope
    IDeliveredSettingsPresenter provideIDeliveredSettingsPresenter(IDeliveredSettingsInteractor iDeliverySettingsInteractor) {
        return new DeliveredSettingsPresenter(iDeliverySettingsInteractor);
    }
}
