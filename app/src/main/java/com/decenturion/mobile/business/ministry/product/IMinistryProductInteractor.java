package com.decenturion.mobile.business.ministry.product;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.ministry.product.model.PaymenDataModel;
import com.decenturion.mobile.ui.fragment.ministry.product.model.MinistryProductModelView;

import rx.Observable;

public interface IMinistryProductInteractor {

    Observable<MinistryProductModelView> getMinistryModelView(@NonNull String category);

    Observable<MinistryProductModelView> genPaymentAddress(@NonNull String category,
                                                           @NonNull String coin,
                                                           @NonNull String amount);

    Observable<MinistryProductModelView> genPaymentAddress(@NonNull PaymenDataModel model);

    Observable<MinistryProductModelView> genPaymentAddress(@NonNull String category, @NonNull String coin);
}
