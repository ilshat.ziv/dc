package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Guests {

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("count")
    private String mCount;

    public Guests() {
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getCount() {
        return mCount;
    }
}
