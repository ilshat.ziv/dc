package com.decenturion.mobile.network.response.photo;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadPhotoResponse extends BaseResponse {

    @JsonProperty("result")
    private UploadPhotoResult mResult;

    public UploadPhotoResponse() {
    }

    @JsonIgnore
    public UploadPhotoResult getResult() {
        return mResult;
    }
}
