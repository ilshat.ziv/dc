package com.decenturion.mobile.network.response.transactions;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsResponse extends BaseResponse {

    @JsonProperty("result")
    private TransactionsResult mResult;

    public TransactionsResponse() {
    }

    @JsonIgnore
    public TransactionsResult getResult() {
        return mResult;
    }
}
