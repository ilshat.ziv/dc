package com.decenturion.mobile.ui.fragment.referral.invite;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InviteListAdapter extends RecyclerView.Adapter<InviteListAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<InviteModel> mTraideModelList;

    private OnItemClickListener mOnItemClickListener;

    private InviteListAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public InviteListAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<InviteModel> traideModelList) {
        mTraideModelList = traideModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTraideModelList);
    }

    @NonNull
    @Override
    public InviteListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_invite_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InviteListAdapter.ViewHolder holder, int position) {
        final InviteModel model = mTraideModelList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        @BindView(R.id.dateView)
        TextView dateView;

        @BindView(R.id.urlInviteView)
        TextView urlInviteView;

        @BindView(R.id.priceView)
        TextView priceView;

        @BindView(R.id.removeInviteView)
        LocalizedTextView removeInviteView;

        InviteModel model;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

            this.view.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(model);
                }
            });

            this.removeInviteView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onRemoveInvite(model);
                }
            });
        }

        public void bind(@NonNull InviteModel param) {
            this.model = param;

            this.dateView.setText(model.getDate());
            this.urlInviteView.setText(model.getUrl());
            this.priceView.setText(model.getPrice());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull InviteModel model);
        void onRemoveInvite(@NonNull InviteModel model);
    }
}
