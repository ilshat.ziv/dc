package com.decenturion.mobile.business.sign;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.signin.gfa.SigninGfaResult;

import rx.Observable;

public interface IGfaSigninInteractor {

    Observable<SigninGfaResult> gfaSignin(@NonNull String gfaCode);
}
