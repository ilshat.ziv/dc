package com.decenturion.mobile.ui.activity.update;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.update.UpdateAppFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class UpdateAppActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            INavigationManager iNavigationManager = getINavigationManager();
            iNavigationManager.navigateTo(UpdateAppFragment.class);
        }
    }
}
