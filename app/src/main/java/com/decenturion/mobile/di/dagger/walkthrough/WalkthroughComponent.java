package com.decenturion.mobile.di.dagger.walkthrough;

import com.decenturion.mobile.ui.fragment.walkthrough.WalkthroughFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
})

@WalkthroughScope
public interface WalkthroughComponent {

    void inject(WalkthroughFragment view);
}
