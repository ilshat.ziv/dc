package com.decenturion.mobile.business.profile;

import com.decenturion.mobile.ui.fragment.profile_v2.main.model.ProfileModelView;

import rx.Observable;

public interface IProfileInteractor {

    Observable<ProfileModelView> getProfileInfo();
}
