package com.decenturion.mobile.ui.fragment.profile_v2.log.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.profile.actions.LogsComponent;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.log.LogsAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModel;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.log.presenter.ILogsPresenter;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LogsFragment extends BaseFragment implements ILogsView,
        LogsAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.actionListView)
    BaseRecyclerView mLogListView;
    private LogsAdapter mTraidsAdapter;

    /* DI */

    @Inject
    ILogsPresenter mILogsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        LogsComponent logsComponent = appDelegate
                .getIDIManager()
                .plusLogsComponent();
        logsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_action_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mRefreshView.setOnRefreshListener(this);
        mLogListView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mLogListView.setLayoutManager(new LinearLayoutManager(context));
        mTraidsAdapter = new LogsAdapter(context, this);
        mLogListView.setAdapter(mTraidsAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mILogsPresenter.bindView(this);
        mILogsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mILogsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mILogsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull LogModelView model) {
        mTraidsAdapter.replaceDataSet(model.getActionList());
    }

    @OnClick(R.id.killAllSessionsView)
    protected void onKillAllSessions() {
        mILogsPresenter.killAllSessions();
    }

    @Override
    public void logoutSuccess() {
        FragmentActivity activity = requireActivity();
        activity.startActivity(new Intent(activity, SignActivity.class));
        activity.finishAffinity();
    }

    @Override
    public void onItemClick(LogModel model) {
//        Intent intent = new Intent(getActivity(), TokenActivity.class);
//        intent.putExtra(TokenActivity.TOKEN_ID, model.getId());
//        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mILogsPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mILogsPresenter.bindViewData();
    }

    @Override
    public void onBottomScrolled() {
        mILogsPresenter.onBottomScrolled();
    }
}
