package com.decenturion.mobile.ui.fragment.sign.gfa.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.sign.gfa.model.GfaSigninModelView;

public interface IGfaSigninView extends IScreenFragmentView {

    void onSigninSuccess(int step);

    void onBindViewData(@NonNull GfaSigninModelView model);
}
