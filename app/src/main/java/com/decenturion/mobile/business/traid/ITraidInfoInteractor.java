package com.decenturion.mobile.business.traid;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.traide.model.TraidInfoModelView;

import rx.Observable;

public interface ITraidInfoInteractor {

    Observable<TraidInfoModelView> getTraidInfo(@NonNull String traidUUID);
}
