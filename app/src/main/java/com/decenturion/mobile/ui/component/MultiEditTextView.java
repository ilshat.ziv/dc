package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class MultiEditTextView extends UBaseView implements TextWatcher {

    @Nullable
    @BindView(R.id.valueView)
    EditText mValueView;

    private boolean isTextWatcherListen;

    public MultiEditTextView(Context context) {
        super(context);
    }

    public MultiEditTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiEditTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView()) {
            this.setOnClickListener(v -> {
                assert mValueView != null;
                mValueView.requestFocus();
                mValueView.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mValueView.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                mValueView.setCursorVisible(true);
            });

            assert mValueView != null;
            if (!isTextWatcherListen) {
                isTextWatcherListen = true;
                mValueView.addTextChangedListener(this);
            }
        }
    }

    public void setHint(String hint) {
        init();
        assert mValueView != null;
        mValueView.setHint(String.valueOf(hint));
    }

    public void setEnable(boolean isEnable) {
        init();
        assert mValueView != null;
        mValueView.setEnabled(isEnable);
    }

    public void setText(String text) {
        init();
        assert mValueView != null;
        mValueView.setText(text != null ? String.valueOf(text) : "");
    }

    public void setTypeInput(int inputType) {
        init();
        assert mValueView != null;
        mValueView.setInputType(inputType);
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {
        if (isInitView()) {
            super.editMode();
            assert mValueView != null;
            mValueView.setEnabled(true);
            mValueView.setClickable(true);
            mValueView.setEnabled(true);
            mValueView.setLongClickable(true);
            mValueView.setFocusable(true);
            mValueView.setFocusableInTouchMode(true);
            mValueView.setCursorVisible(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mValueView.setShowSoftInputOnFocus(true);
            }
        }
    }

    @Override
    public void simpleMode() {
        if (isInitView()) {
            super.simpleMode();
            assert mValueView != null;
            mValueView.setClickable(false);
            mValueView.setEnabled(false);
            mValueView.setLongClickable(false);
        }
    }

    public String getValue() {
        if (mValueView == null) {
            init();
        }
        assert mValueView != null;
        return mValueView.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mChangeComponentListener != null) {
            mChangeComponentListener.onChangeComponentData(this);
        }
    }
}