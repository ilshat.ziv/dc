package com.decenturion.mobile.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.di.dagger.DaggerAppComponent;
import com.decenturion.mobile.di.dagger.aggrement.UserAggrementComponent;
import com.decenturion.mobile.di.dagger.app.ApiModule;
import com.decenturion.mobile.di.dagger.app.AppModule;
import com.decenturion.mobile.di.dagger.app.DbModule;
import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.di.dagger.header.HeaderComponent;
import com.decenturion.mobile.di.dagger.invite.InviteComponent;
import com.decenturion.mobile.di.dagger.ministry.market.MinistryComponent;
import com.decenturion.mobile.di.dagger.ministry.product.MinistryProductComponent;
import com.decenturion.mobile.di.dagger.pass.confirm.email.ConfirmEmailRestorePassComponent;
import com.decenturion.mobile.di.dagger.pass.confirm.phone.ConfirmPhoneRestorePassComponent;
import com.decenturion.mobile.di.dagger.pass.edit.EditProfileComponent;
import com.decenturion.mobile.di.dagger.pass.reset.ResetPassComponent;
import com.decenturion.mobile.di.dagger.pass.restore.RestorePassComponent;
import com.decenturion.mobile.di.dagger.passport.PassportComponent;
import com.decenturion.mobile.di.dagger.passport.activation.ActivationPassportComponent;
import com.decenturion.mobile.di.dagger.passport.online.OnlinePassportComponent;
import com.decenturion.mobile.di.dagger.passport.phisical.PhisicalPassportComponent;
import com.decenturion.mobile.di.dagger.passport.welcome.WellcomeComponent;
import com.decenturion.mobile.di.dagger.profile.ProfileComponent;
import com.decenturion.mobile.di.dagger.profile.actions.LogsComponent;
import com.decenturion.mobile.di.dagger.profile.score.ScoreListComponent;
import com.decenturion.mobile.di.dagger.profile.transactions.TransactionListComponent;
import com.decenturion.mobile.di.dagger.referral.acquisition.AcquisitionComponent;
import com.decenturion.mobile.di.dagger.referral.invite.InviteListComponent;
import com.decenturion.mobile.di.dagger.referral.invite.create.CreateInviteComponent;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.di.dagger.referral.history.ReferralHistorylComponent;
import com.decenturion.mobile.di.dagger.score.ScoreTokenListComponent;
import com.decenturion.mobile.di.dagger.score.express.ExpressSendTokenComponent;
import com.decenturion.mobile.di.dagger.score.main.MainScoreSendTokenComponent;
import com.decenturion.mobile.di.dagger.score.token.ScoreTokenDetailsComponent;
import com.decenturion.mobile.di.dagger.score.trade.TradeScoreSendTokenComponent;
import com.decenturion.mobile.di.dagger.seller.passport.SellerPassportComponent;
import com.decenturion.mobile.di.dagger.seller.profile.SellerProfileComponent;
import com.decenturion.mobile.di.dagger.seller.token.SellerTokenComponent;
import com.decenturion.mobile.di.dagger.seller.traids.SellerTraidsComponent;
import com.decenturion.mobile.di.dagger.send.SendTokenComponent;
import com.decenturion.mobile.di.dagger.settings.account.AccountSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.action.ActionListSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.DeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.delivered.DeliveredSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.fill.FillDeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.pay.PayDeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.pending.DeliveryPendingSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.details.DetailsSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.mail.MailingSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.main.SettingsComponent;
import com.decenturion.mobile.di.dagger.settings.passport.PassportSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.password.PasswordSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.security.SecurityComponent;
import com.decenturion.mobile.di.dagger.settings.wallets.WalletsSettingsComponent;
import com.decenturion.mobile.di.dagger.sign.GfaSigninComponent;
import com.decenturion.mobile.di.dagger.signin.SigninComponent;
import com.decenturion.mobile.di.dagger.signup.SignupComponent;
import com.decenturion.mobile.di.dagger.splash.SplashComponent;
import com.decenturion.mobile.di.dagger.support.SupportComponent;
import com.decenturion.mobile.di.dagger.token.TokenComponent;
import com.decenturion.mobile.di.dagger.token.details.SellerTokenDetailsComponent;
import com.decenturion.mobile.di.dagger.token.info.TokenInfoComponent;
import com.decenturion.mobile.di.dagger.token.list.TokenListComponent;
import com.decenturion.mobile.di.dagger.tokendetails.buy.BuyTokenComponent;
import com.decenturion.mobile.di.dagger.tokendetails.details.TokenDetailsComponent;
import com.decenturion.mobile.di.dagger.tokendetails.edit.EditPriceTokenComponent;
import com.decenturion.mobile.di.dagger.tokendetails.transaction.TransactionComponent;
import com.decenturion.mobile.di.dagger.traid.TraidInfoComponent;
import com.decenturion.mobile.di.dagger.traids.TraidsComponent;
import com.decenturion.mobile.di.dagger.transaction.TransactionInfoComponent;
import com.decenturion.mobile.di.dagger.twofa.EnableTwoFAComponent;
import com.decenturion.mobile.di.dagger.validate.ValidateEmailComponent;
import com.decenturion.mobile.di.dagger.walkthrough.WalkthroughComponent;

public class DIManager implements IDIManager {

    private AppDelegate mAppDelegate;

    private AppComponent mAppComponent;

    public DIManager(@NonNull AppDelegate appDelegate) {
        mAppDelegate = appDelegate;
        mAppComponent = initAppComponent().build();
    }

    @NonNull
    private DaggerAppComponent.Builder initAppComponent() {
        Context context = mAppDelegate.getApplicationContext();

        AppModule appModule = new AppModule(context);
        NetworkModule networkModule = new NetworkModule(
                BuildConfig.API_HOST,
                appModule.provideIPrefsManager()
        );

        return DaggerAppComponent.builder()
                .appModule(appModule)
                .apiModule(new ApiModule())
                .networkModule(networkModule)
                .dbModule(new DbModule(context));
    }

    @Override
    public void onTerminate() {
        mAppDelegate = null;
        mAppComponent = null;
    }

    @Override
    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    @Override
    public SignupComponent plusSignupComponent() {
        return mAppComponent.plusSignupComponent();
    }

    @Override
    public SigninComponent plusSigninComponent() {
        return mAppComponent.plusSigninComponent();
    }

    @Override
    public SettingsComponent plusSettingsComponent() {
        return mAppComponent.plusSettingsComponent();
    }

    @Override
    public EditProfileComponent plusEditProfileComponent() {
        return mAppComponent.plusEditProfileComponent();
    }

    @Override
    public SecurityComponent plusSecurityComponent() {
        return mAppComponent.plusSecurityComponent();
    }

    @Override
    public PassportComponent plusPassportComponent() {
        return mAppComponent.plusPassportComponent();
    }

    @Override
    public TokenComponent plusTokenComponent() {
        return mAppComponent.plusTokenComponent();
    }

    @Override
    public TokenDetailsComponent plusTokenDetailsComponent() {
        return mAppComponent.plusTokenDetailsComponent();
    }

//    @Override
//    public TokenInfoComponent plusTokenInfoComponent() {
//        return mAppComponent.plusTokenInfoComponent();
//    }

    @Override
    public BuyTokenComponent plusBuyTokenComponent() {
        return mAppComponent.plusBuyTokenComponent();
    }

    @Override
    public TransactionComponent plusTransactionComponent() {
        return mAppComponent.plusTransactionComponent();
    }

    @Override
    public UserAggrementComponent plusUserAggrementComponent() {
        return mAppComponent.plusUserAggrementComponent();
    }

    @Override
    public ValidateEmailComponent plusValidateEmailComponent() {
        return mAppComponent.plusValidateEmailComponent();
    }

    @Override
    public OnlinePassportComponent plusOnlinePassportComponent() {
        return mAppComponent.plusOnlinePassportComponent();
    }

    @Override
    public PhisicalPassportComponent plusPhisicalPassportComponent() {
        return mAppComponent.plusPhisicalPassportComponent();
    }

    @Override
    public ActivationPassportComponent plusActivationPassportComponent() {
        return mAppComponent.plusActivationPassportComponent();
    }

    @Override
    public WellcomeComponent plusWellcomeComponent() {
        return mAppComponent.plusWellcomeComponent();
    }

    @Override
    public AccountSettingsComponent plusAccountSettingsComponent() {
        return mAppComponent.plusAccountSettingsComponent();
    }

    @Override
    public PasswordSettingsComponent plusPasswordSettingsComponent() {
        return mAppComponent.plusPasswordSettingsComponent();
    }

    @Override
    public PassportSettingsComponent plusPassportSettingsComponent() {
        return mAppComponent.plusPassportSettingsComponent();
    }

    @Override
    public DeliverySettingsComponent plusDeliverySettingsComponent() {
        return mAppComponent.plusDeliverySettingsComponent();
    }

    @Override
    public DetailsSettingsComponent plusDetailsSettingsComponent() {
        return mAppComponent.plusDetailsSettingsComponent();
    }

    @Override
    public MailingSettingsComponent plusMailingSettingsComponent() {
        return mAppComponent.plusMailingSettingsComponent();
    }

    @Override
    public ProfileComponent plusProfileComponent() {
        return mAppComponent.plusProfileComponent();
    }

    @Override
    public EditPriceTokenComponent plusEditPriceTokenComponent() {
        return mAppComponent.plusEditPriceTokenComponent();
    }

    @Override
    public InviteComponent plusInviteComponent() {
        return mAppComponent.plusInviteComponent();
    }

    @Override
    public SellerProfileComponent plusSellerProfileComponent() {
        return mAppComponent.plusSellerProfileComponent();
    }

    @Override
    public SellerTokenComponent plusSellerTokenComponent() {
        return mAppComponent.plusSellerTokenComponent();
    }

    @Override
    public SellerPassportComponent plusSellerPassportComponent() {
        return mAppComponent.plusSellerPassportComponent();
    }

    @Override
    public SendTokenComponent plusSendTokenComponent() {
        return mAppComponent.plusSendTokenComponent();
    }

    @Override
    public WalkthroughComponent plusWalkthroughComponent() {
        return mAppComponent.plusWalkthroughComponent();
    }

    @Override
    public TraidsComponent plusTraidsComponent() {
        return mAppComponent.plusTraidsComponent();
    }

    @Override
    public LogsComponent plusLogsComponent() {
        return mAppComponent.plusLogsComponent();
    }

    @Override
    public ActionListSettingsComponent plusActionListSettingsComponent() {
        return mAppComponent.plusActionListSettingsComponent();
    }

    @Override
    public TraidInfoComponent plusTraidInfoComponent() {
        return mAppComponent.plusTraidInfoComponent();
    }

    @Override
    public HeaderComponent plusHeaderComponent() {
        return mAppComponent.plusHeaderComponent();
    }

    @Override
    public RestorePassComponent plusRestorePassComponent() {
        return mAppComponent.plusRestorePassComponent();
    }

    @Override
    public ConfirmEmailRestorePassComponent plusConfirmEmailRestorePassComponent() {
        return mAppComponent.plusConfirmEmailRestorePassComponent();
    }

    @Override
    public ConfirmPhoneRestorePassComponent plusConfirmPhoneRestorePassComponent() {
        return mAppComponent.plusConfirmPhoneRestorePassComponent();
    }

    @Override
    public GfaSigninComponent plusGfaSigninComponent() {
        return mAppComponent.plusGfaSigninComponent();
    }

    @Override
    public SellerTraidsComponent plusSellerTraidsComponent() {
        return mAppComponent.plusSellerTraidsComponent();
    }

    @Override
    public ResetPassComponent plusResetPassComponent() {
        return mAppComponent.plusResetPassComponent();
    }

    @Override
    public WalletsSettingsComponent plusWalletsSettingsComponent() {
        return mAppComponent.plusWalletsSettingsComponent();
    }

    @Override
    public TransactionListComponent plusTransactionListComponent() {
        return mAppComponent.plusTransactionListComponent();
    }

    @Override
    public TokenInfoComponent plusTokenInfoComponent() {
        return mAppComponent.plusTokenInfoComponent();
    }

    @Override
    public SplashComponent plusSplashComponent() {
        return mAppComponent.plusSplashComponent();
    }

    @Override
    public DeliveredSettingsComponent plusDeliveredSettingsComponent() {
        return mAppComponent.plusDeliveredSettingsComponent();
    }

    @Override
    public DeliveryPendingSettingsComponent plusDeliveryPendingSettingsComponent() {
        return mAppComponent.plusDeliveryPendingSettingsComponent();
    }

    @Override
    public FillDeliverySettingsComponent plusFillDeliverySettingsComponent() {
        return mAppComponent.plusFillDeliverySettingsComponent();
    }

    @Override
    public PayDeliverySettingsComponent plusPayDeliverySettingsComponent() {
        return mAppComponent.plusPayDeliverySettingsComponent();
    }

    @Override
    public SellerTokenDetailsComponent plusSellerTokenDetailsComponent() {
        return mAppComponent.plusSellerTokenDetailsComponent();
    }

    @Override
    public TransactionInfoComponent plusTransactionInfoComponent() {
        return mAppComponent.plusTransactionInfoComponent();
    }

    @Override
    public EnableTwoFAComponent plusEnableTwoFAComponent() {
        return mAppComponent.plusEnableTwoFAComponent();
    }

    @Override
    public MinistryComponent plusMinistryComponent() {
        return mAppComponent.plusMinistryComponent();
    }

    @Override
    public MinistryProductComponent plusMinistryProductComponent() {
        return mAppComponent.plusMinistryProductComponent();
    }

    @Override
    public SupportComponent plusSupportComponent() {
        return mAppComponent.plusSupportComponent();
    }

    @Override
    public ReferralComponent plusReferallComponent() {
        return mAppComponent.plusReferallComponent();
    }

    @Override
    public ReferralHistorylComponent plusReferralHistorylComponent() {
        return mAppComponent.plusReferralHistorylComponent();
    }

    @Override
    public AcquisitionComponent plusAcquisitionComponent() {
        return mAppComponent.plusAcquisitionComponent();
    }

    @Override
    public CreateInviteComponent plusCreateInviteComponent() {
        return mAppComponent.plusCreateInviteComponent();
    }

    @Override
    public InviteListComponent plusInviteListComponent() {
        return mAppComponent.plusInviteListComponent();
    }

    @Override
    public ScoreListComponent plusScoreListComponent() {
        return mAppComponent.plusScoreListComponent();
    }

    @Override
    public TokenListComponent plusTokenListComponent() {
        return mAppComponent.plusTokenListComponent();
    }

    @Override
    public ScoreTokenListComponent plusScoreTokenListComponent() {
        return mAppComponent.plusScoreTokenListComponent();
    }

    @Override
    public ScoreTokenDetailsComponent plusScoreTokenDetailsComponent() {
        return mAppComponent.plusScoreTokenDetailsComponent();
    }

    @Override
    public MainScoreSendTokenComponent plusMainScoreSendTokenComponent() {
        return mAppComponent.plusMainScoreSendTokenComponent();
    }

    @Override
    public TradeScoreSendTokenComponent plusTradeScoreSendTokenComponent() {
        return mAppComponent.plusTradeScoreSendTokenComponent();
    }

    @Override
    public ExpressSendTokenComponent plusExpressSendTokenComponent() {
        return mAppComponent.plusExpressSendTokenComponent();
    }
}
