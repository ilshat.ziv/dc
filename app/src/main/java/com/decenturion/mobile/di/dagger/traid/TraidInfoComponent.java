package com.decenturion.mobile.di.dagger.traid;

import com.decenturion.mobile.ui.fragment.traide.view.TraidInfoFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TraidInfoModule.class
})

@TraidInfoScope
public interface TraidInfoComponent {

    void inject(TraidInfoFragment view);
}
