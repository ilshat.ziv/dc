package com.decenturion.mobile.network.response.traide.confirm;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmTraideResponse extends BaseResponse {

    @JsonProperty("result")
    private ConfirmTraideResult mResult;

    public ConfirmTraideResponse() {
    }

    @JsonIgnore
    public ConfirmTraideResult getResult() {
        return mResult;
    }
}
