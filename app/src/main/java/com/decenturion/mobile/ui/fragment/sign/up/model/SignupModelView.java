package com.decenturion.mobile.ui.fragment.sign.up.model;

import android.support.annotation.NonNull;

public class SignupModelView {

    private String mUsername;
    private String mPassword;
    private String mRetypePassword;
    private String mSource;
    private boolean isSubscribe;

    private boolean isAgreeTermsOfuse;
    private boolean isAgreeEmailConfirmation;

    private String mInviteCode;

    private String mReferralCode;
    private String mPayInviteCode;
    private String mBannerCode;

    public SignupModelView() {
    }

    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    public void setRetypePassword(@NonNull String retypePassword) {
        mRetypePassword = retypePassword;
    }

    public void setSource(@NonNull String source) {
        mSource = source;
    }

    public void setSubscribe(boolean subscribe) {
        isSubscribe = subscribe;
    }

    public void setAgreeTermsOfuse(boolean agreeTermsOfuse) {
        isAgreeTermsOfuse = agreeTermsOfuse;
    }

    public void setAgreeEmailConfirmation(boolean agreeEmailConfirmation) {
        isAgreeEmailConfirmation = agreeEmailConfirmation;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getRetypePassword() {
        return mRetypePassword;
    }

    public String getSource() {
        return mSource;
    }

    public boolean isSubscribe() {
        return isSubscribe;
    }

    public boolean isAgreeTermsOfuse() {
        return isAgreeTermsOfuse;
    }

    public boolean isAgreeEmailConfirmation() {
        return isAgreeEmailConfirmation;
    }

    public void setInviteCode(String inviteCode) {
        mInviteCode = inviteCode;
    }

    public void setReferralCode(String referralCode) {
        mReferralCode = referralCode;
    }

    public void setPayInviteCode(String payInviteCode) {
        mPayInviteCode = payInviteCode;
    }

    public void setBannerCode(String bannerCode) {
        mBannerCode = bannerCode;
    }

    public String getInviteCode() {
        return mInviteCode;
    }

    public String getReferralCode() {
        return mReferralCode;
    }

    public String getPayInviteCode() {
        return mPayInviteCode;
    }

    public String getBannerCode() {
        return mBannerCode;
    }
}
