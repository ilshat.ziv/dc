package com.decenturion.mobile.di.dagger.token.details;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface SellerTokenDetailsScope {
}
