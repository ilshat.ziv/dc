package com.decenturion.mobile.di.dagger.profile.score;

import com.decenturion.mobile.ui.fragment.profile_v2.score.list.view.ScoreListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ScoreListModule.class
})

@ScoreListScope
public interface ScoreListComponent {

    void inject(ScoreListFragment view);
}
