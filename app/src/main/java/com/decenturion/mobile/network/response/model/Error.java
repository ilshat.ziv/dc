package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Error {

    @JsonProperty("key")
    private String mKey;

    @JsonProperty("message")
    private String mMessage;

    public Error() {
    }

    @JsonIgnore
    public String getKey() {
        return mKey;
    }

    @JsonIgnore
    public String getMessage() {
        return mMessage;
    }
}
