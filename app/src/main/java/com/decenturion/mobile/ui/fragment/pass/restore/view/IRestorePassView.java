package com.decenturion.mobile.ui.fragment.pass.restore.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.restore.model.RestorePassModelView;

public interface IRestorePassView extends IScreenFragmentView {

    void onRestorePassSuccess();

    void onBindViewData(@NonNull RestorePassModelView model);
}
