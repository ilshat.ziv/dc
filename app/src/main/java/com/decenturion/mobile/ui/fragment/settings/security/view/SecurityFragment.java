package com.decenturion.mobile.ui.fragment.settings.security.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.di.dagger.settings.security.SecurityComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.security.model.GfaModel;
import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;
import com.decenturion.mobile.ui.fragment.settings.security.presenter.ISecurityPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SecurityFragment extends BaseFragment implements ISecurityView,
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.authGoogleAccountView)
    CheckBox mAuthGoogleAccountView;

    @BindView(R.id.enableG2faControlView)
    LinearLayout mEnableG2faControlView;

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;



    @BindView(R.id.showNameView)
    CheckBox mShowNameView;

    @BindView(R.id.showBirthView)
    CheckBox mShowBirthView;

    @BindView(R.id.showPhotoView)
    CheckBox mShowPhotoView;

    @BindView(R.id.showTokensView)
    CheckBox mShowTokensView;

    @BindView(R.id.showMultiplicatorAuthorityView)
    CheckBox mShowMultiplicatorAuthorityView;

    @BindView(R.id.showMultiplicatorGloryView)
    CheckBox mShowMultiplicatorGloryView;

    @BindView(R.id.showMultiplicatorView)
    CheckBox mShowMultiplicatorView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISecurityPresenter mISecurityPresenter;

    @Inject
    IPrefsManager mIPrefsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SecurityComponent securityComponent = appDelegate
                .getIDIManager()
                .plusSecurityComponent();
        securityComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fr_security, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_code_title));
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mQrCodeViewComponent.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_key));
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_security_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISecurityPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISecurityPresenter.bindViewData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mISecurityPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISecurityPresenter.unbindView();
        try {
            mISecurityPresenter.onSaveInstanceState(outState,
                    mShowNameView.isChecked(),
                    mShowBirthView.isChecked(),
                    mShowPhotoView.isChecked(),
                    mShowTokensView.isChecked(),
                    mShowMultiplicatorView.isChecked(),
                    mShowMultiplicatorAuthorityView.isChecked(),
                    mShowMultiplicatorGloryView.isChecked(),

                    (Boolean) mAuthGoogleAccountView.getTag(),
                    mAuthGoogleAccountView.isChecked(),
                    mQrCodeViewComponent.getAddress(),
                    mSecretCodeView.getValue()
            );
        } catch (Exception e) {
            // TODO: 04.10.2018 проследить в крашлитикс
            LoggerUtils.exception(e);
        }

        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISecurityPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISecurityPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mISecurityPresenter.saveChanges(
                mShowNameView.isChecked(),
                mShowBirthView.isChecked(),
                mShowPhotoView.isChecked(),
                mShowTokensView.isChecked(),
                mShowMultiplicatorView.isChecked(),
                mShowMultiplicatorAuthorityView.isChecked(),
                mShowMultiplicatorGloryView.isChecked(),

                (Boolean) mAuthGoogleAccountView.getTag(),
                mAuthGoogleAccountView.isChecked(),
                mQrCodeViewComponent.getAddress(),
                mSecretCodeView.getValue()
        );
    }

    @Override
    public void onBindViewData(@NonNull SecurityModelView model) {
        initG2faView(model);

        mShowNameView.setChecked(model.isShowName());
        mShowBirthView.setChecked(model.isShowBirth());
        mShowPhotoView.setChecked(model.isShowPhoto());
        mShowTokensView.setChecked(model.isShowTokens());
        mShowMultiplicatorAuthorityView.setChecked(model.isShowPower());
        mShowMultiplicatorGloryView.setChecked(model.isShowGlory());
        mShowMultiplicatorView.setChecked(model.isShowMoney());
    }

    @Override
    public void onChangesSaved(@NonNull SecurityModelView model) {
        mSecretCodeView.setVisibility(View.GONE);
        mEnableG2faControlView.setVisibility(View.GONE);
        initG2faView(model);

        mShowNameView.setChecked(model.isShowName());
        mShowBirthView.setChecked(model.isShowBirth());
        mShowPhotoView.setChecked(model.isShowPhoto());
        mShowTokensView.setChecked(model.isShowTokens());
        mShowMultiplicatorAuthorityView.setChecked(model.isShowPower());
        mShowMultiplicatorGloryView.setChecked(model.isShowGlory());
        mShowMultiplicatorView.setChecked(model.isShowMoney());
    }

    private void initG2faView(@NonNull SecurityModelView model) {
        GfaModel gfaModel = model.getGfaModel();

        mSecretCodeView.setText(gfaModel.getSecretCode());

        String secretString = gfaModel.getSecretString();
        mQrCodeViewComponent.setAddress(secretString);

        String authFormat = "otpauth://totp/{email}?secret={secret}&issuer=Decenturion";
        String string = authFormat.replace("{secret}", secretString);

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        String email = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, "");
        string = string.replace("{email}", email);

        mQrCodeViewComponent.setQrData(string);

        mAuthGoogleAccountView.setTag(gfaModel.isEnabledGfa());
        mAuthGoogleAccountView.setChecked(gfaModel.isEnableGfa());
        mAuthGoogleAccountView.setOnCheckedChangeListener(this);

        init2FaControlls(gfaModel.isEnableGfa(), gfaModel.isEnabledGfa());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Boolean state = (Boolean) buttonView.getTag();
        init2FaControlls(isChecked, state);
    }

    private void init2FaControlls(boolean isEnable, boolean isEnabled) {
        if (isEnabled) {
            if (isEnable) {
                mSecretCodeView.setVisibility(View.GONE);
            } else {
                mSecretCodeView.setVisibility(View.VISIBLE);
            }
        } else {
            if (isEnable) {
                mEnableG2faControlView.setVisibility(View.VISIBLE);
                mSecretCodeView.setVisibility(View.VISIBLE);
            } else {
                mEnableG2faControlView.setVisibility(View.GONE);
                mSecretCodeView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
