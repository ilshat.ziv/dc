package com.decenturion.mobile.network.response.payment;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResidencePaymentInfoResponse extends BaseResponse {

    @JsonProperty("result")
    private List<ResidencePaymentInfoResult> mResult;

    public ResidencePaymentInfoResponse() {
    }

    @JsonIgnore
    public List<ResidencePaymentInfoResult> getResult() {
        return mResult;
    }
}
