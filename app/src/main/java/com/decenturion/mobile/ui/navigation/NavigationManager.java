package com.decenturion.mobile.ui.navigation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.main.view.ProfileFragment;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

public class NavigationManager<F extends BaseFragment> implements INavigationManager<F>,
        FragmentManager.OnBackStackChangedListener {

    private final String FRAGMENT_TAGS = "FRAGMENT_TAGS";

    private FragmentActivity mFragmentActivity;
    private int mContentFrameId;

    private ArrayList<String> mFragmentTagList = new ArrayList<>();
    private int mCountStackFragments;

    private OnCustomBackStackChangedListener mOnCustomBackStackChangedListener;

    /* DI */

    public NavigationManager(@NonNull FragmentActivity activity, int contentFrameId) {
        mFragmentActivity = activity;
        mContentFrameId = contentFrameId;

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
    }

    private F getInstanceFragment(Class<F> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            LoggerUtils.exception(e);
        } catch (IllegalAccessException e) {
            LoggerUtils.exception(e);
        }

        return null;
    }

    @Override
    public void navigateTo(Class<F> clazz) {
        this.navigateTo(clazz, false, clazz.getSimpleName());
    }

    @Override
    public void navigateTo(Class<F> clazz, String tag) {
        this.navigateTo(clazz, false, tag);
    }

    @Override
    public void navigateTo(Class<F> clazz, boolean isAddBackStack) {
        this.navigateTo(clazz, isAddBackStack, clazz.getSimpleName());
    }

    @Override
    public void navigateTo(Class<F> clazz, boolean isAddBackStack, String tag) {
        this.navigateTo(clazz, isAddBackStack, tag, null);
    }

    @Override
    public void navigateTo(Class<F> clazz, boolean isAddBackStack, Bundle bundle) {
        this.navigateTo(clazz, isAddBackStack, clazz.getSimpleName(), bundle);
    }

    @Override
    public void navigateTo(Class<F> clazz, Bundle bundle) {
        this.navigateTo(clazz, false, clazz.getSimpleName(), bundle);
    }

    @Override
    public void navigateTo(Class<F> clazz, boolean isAddBackStack, Fragment target, int actionCode) {
        String tag = clazz.getSimpleName();
        F fragment = getInstanceFragment(clazz);
        assert fragment != null;

        if (isSingleFragment(tag)) {
            clearFragmentStack();
        }

        fragment.setTargetFragment(target, actionCode);

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (isAddBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(mContentFrameId, fragment, tag);
        ft.commit();

        mFragmentTagList.add(tag);
    }

    @Override
    public void navigateTo(Class<F> clazz, boolean isAddBackStack, String tag, Bundle bundle) {
        F fragment = getInstanceFragment(clazz);
        assert fragment != null;
        fragment.setArguments(bundle);

        if (isSingleFragment(tag)) {
            clearFragmentStack();
        }

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (isAddBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(mContentFrameId, fragment, tag);
        ft.commit();

        mFragmentTagList.add(tag);
    }

    private void clearFragmentStack() {
        if (CollectionUtils.isEmpty(mFragmentTagList)) {
            return;
        }

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        for (String tag : mFragmentTagList) {
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null) {
                fragmentTransaction.remove(fragment);
            }
        }
        fragmentTransaction.commit();
        mFragmentTagList.clear();
    }

    private boolean isSingleFragment(String tag) {
        return TextUtils.equals(tag, ProfileFragment.class.getSimpleName());
//                || TextUtils.equals(tag, UserAggrementFragment.class.getSimpleName());
    }

    @Override
    public void navigateToBack() {
        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            mFragmentActivity.onBackPressed();
        }
    }

    @Override
    public F getCurentFragment() {
        if (CollectionUtils.isEmpty(mFragmentTagList)) {
            return null;
        }

        int index = mFragmentTagList.size() - 1;
        String tag = mFragmentTagList.get(index);

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        return (F) fragmentManager.findFragmentByTag(tag);
    }

    @Override
    public void onBackStackChanged() {
        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();

        int newStackSize = fragmentManager.getBackStackEntryCount();
        if (mCountStackFragments > newStackSize) {
            if (!CollectionUtils.isEmpty(mFragmentTagList)) {
                F fragment = null;

                int size = CollectionUtils.size(mFragmentTagList);
                int index = size - 1;
                if (size > 0) {
                    mFragmentTagList.remove(index);
                }

                size = CollectionUtils.size(mFragmentTagList);
                if (size > 0) {
                    String key = mFragmentTagList.get(index - 1);
                    fragment = (F) fragmentManager.findFragmentByTag(key);
                }
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.show(fragment);
                    fragmentTransaction.commit();
                }
            }
        }
        mCountStackFragments = newStackSize;

        if (mOnCustomBackStackChangedListener != null) {
            mOnCustomBackStackChangedListener.onBackStackChange();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putSerializable(FRAGMENT_TAGS, mFragmentTagList);
    }

    @Override
    public void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        if (savedInstanceState.containsKey(FRAGMENT_TAGS)) {
            mFragmentTagList = (ArrayList<String>) savedInstanceState.getSerializable(FRAGMENT_TAGS);
            if (!CollectionUtils.isEmpty(mFragmentTagList)) {
                FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                int index = mFragmentTagList.size() - 1;
                String tag = mFragmentTagList.get(index);
                Fragment fragmentByTag = fragmentManager.findFragmentByTag(tag);
                ft.show(fragmentByTag);

//                if (mIAppLifecycleManager.isAppForeground()) {
                    ft.commit();
//                } else {
//                    mStopedFragmentTransaction = ft;
//                }
                mCountStackFragments = fragmentManager.getBackStackEntryCount();
            }
        }
    }

    @Override
    public void onDestroy() {
        mFragmentActivity = null;
    }

    public void setOnCustomBackStackChangedListener(OnCustomBackStackChangedListener listener) {
        mOnCustomBackStackChangedListener = listener;
    }

    public interface OnCustomBackStackChangedListener {
        void onBackStackChange();
    }

}
