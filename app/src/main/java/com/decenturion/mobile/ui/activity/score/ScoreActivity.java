package com.decenturion.mobile.ui.activity.score;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.view.ScoreTokenListFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class ScoreActivity extends SingleFragmentActivity {

    public static final String SCORE_TYPE = "SCORE_TYPE";
    private Bundle mBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
        } else {
            iNavigationManager.navigateTo(ScoreTokenListFragment.class, mBundle);
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mBundle = intent.getExtras();
        }
    }
}
