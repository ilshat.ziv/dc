package com.decenturion.mobile.ui.fragment.passport.activation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.passport.activation.ActivationPassportComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.passport.activation.model.ActivationModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.model.PassportModel;
import com.decenturion.mobile.ui.fragment.passport.activation.presenter.IActivationPassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.WellcomeFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ActivationPassportFragment extends BaseFragment implements IActivationPassportView,
        SpinnerView_v2.OnSpinnerItemClickListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    @BindView(R.id.summPayView)
    BuyTokenView mSummPayView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.payActionView)
    LinearLayout mPayActionView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IActivationPassportPresenter mIActivationPassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ActivationPassportComponent activationPassportComponent = appDelegate
                .getIDIManager()
                .plusActivationPassportComponent();
        activationPassportComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_activation_passport, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSummPayView.showLabel();
        mSummPayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_activation_payment_amount_title));
        mSummPayView.simpleMode();
        mSummPayView.setVisibleDivider(true);

        // TODO: 19.09.2018 костыль, слишком сложная структура перевода
        String localeString = mILocalistionManager.getLocaleString(R.string.app_activation_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();
        mSelectCoinView.setOnSpinnerItemClickListener(this);

        mQrCodeViewComponent.hideLabel();
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_activation_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIActivationPassportPresenter.bindView(this);
        mIActivationPassportPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIActivationPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIActivationPassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
        mIActivationPassportPresenter.unbindView();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_in);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_activation_signin));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_activation_passport, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_in : {
                FragmentActivity activity = requireActivity();
                Intent intent = new Intent(activity, SignActivity.class);
                startActivity(intent);
                activity.finish();

                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBindViewData(CoinModelView coinModelView, ActivationModelView model) {
        mSelectCoinView.setData(coinModelView.getCoinData());
        mSelectCoinView.selectItem(model.getTypePay());

        bindPayModelView(model);
        processPassportData(model);
    }

    @Override
    public void onBindPaymentViewData(@NonNull ActivationModelView modelView) {
        bindPayModelView(modelView);
        processPassportData(modelView);
    }

    private void bindPayModelView(ActivationModelView model) {
        if (!TextUtils.isEmpty(model.getCoinsPay())) {
            String summPay = mILocalistionManager.getLocaleString(R.string.app_activation_payment_amount_value);
            summPay = summPay.replace("{amount}", model.getCoinsPay());
            summPay = summPay.replace("{currency}", model.getTypePay());
            mSummPayView.setText(summPay);
            mSummPayView.setCoin(model.getCyrrencePay());

            mQrCodeViewComponent.setQrData(model.getQrCode());

            mPayActionView.setVisibility(View.VISIBLE);
        } else {
            mPayActionView.setVisibility(View.GONE);
        }

        mQrCodeViewComponent.setAddress(model.getAddress());
    }

    private void processPassportData(@NonNull ActivationModelView modelView) {
        PassportModel passportModel = modelView.getPassportModel();
        if (passportModel != null) {
            if (passportModel.isActive()) {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(WellcomeFragment.class);
            }
        }
    }

    @OnClick(R.id.shareButtonView)
    protected void onShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, BuildConfig.HOST);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, mILocalistionManager.getLocaleString(R.string.app_offlinepassport_share)));
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mIActivationPassportPresenter.getPaymentAddress(iSpinnerItem.getItemKey());
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
