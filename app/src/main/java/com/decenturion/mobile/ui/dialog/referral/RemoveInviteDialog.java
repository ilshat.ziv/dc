package com.decenturion.mobile.ui.dialog.referral;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class RemoveInviteDialog extends SingleDialogFragment implements IRemoveInviteView {

    public static String INVITE_ID = "INVITE_ID";
    private int mInviteId;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mInviteId = arguments.getInt(INVITE_ID, 0);
        }
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        ReferralComponent component = appDelegate
                .getIDIManager()
                .plusReferallComponent();
        component.inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_referral_invite_remove_title));
        setPositiveTextString(mILocalistionManager.getLocaleString(R.string.app_constants_remove));
        setNegativeTextString(mILocalistionManager.getLocaleString(R.string.app_constants_cancel));

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_referall_invite_delete);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        setupUi();

        return arbitrationRequestDialog;
    }

    private void setupUi() {
    }

    @Override
    public void onPositiveClickButton() {
        dismiss();

        Intent data = new Intent();
        data.putExtra(INVITE_ID, mInviteId);
        Fragment targetFragment = getTargetFragment();
        assert targetFragment != null;
        targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
    }

    @Override
    public void onNegativeClickButton() {
        dismiss();
    }

}
