package com.decenturion.mobile.ui.fragment.pass.restore.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.pass.restore.view.IRestorePassView;

public interface IRestorePassPresenter extends IPresenter<IRestorePassView> {

    void restorePass(@NonNull String data, String captcha);

    void bindViewData();
}
