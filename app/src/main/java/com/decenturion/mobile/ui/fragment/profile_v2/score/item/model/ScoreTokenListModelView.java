package com.decenturion.mobile.ui.fragment.profile_v2.score.item.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class ScoreTokenListModelView {

    public static final int LIMIT = 15;

    private ScoreType mScoreType;

    private int mTotal;
    private long mOffset;
    private List<ScoreTokenModelView> mTokentModelList;

    public ScoreTokenListModelView() {
    }

    public ScoreTokenListModelView(@NonNull ScoreType scoreType) {
        mScoreType = scoreType;
    }

    public ScoreTokenListModelView(@NonNull List<ScoreTokenModelView> list) {
        mTokentModelList = list;
    }

    public ScoreType getScoreType() {
        return mScoreType;
    }

    public void setTokentModelList(List<ScoreTokenModelView> tokentModelList) {
        mTokentModelList = tokentModelList;
    }

    public List<ScoreTokenModelView> getTokentModelList() {
        return mTokentModelList;
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }

    public void addTokenList(List<ScoreTokenModelView> list) {
        if (CollectionUtils.isEmpty(mTokentModelList)) {
            mTokentModelList = new ArrayList<>();
        }

        if (!CollectionUtils.isEmpty(list)) {
            mTokentModelList.addAll(list);
        }
    }
}
