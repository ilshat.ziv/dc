package com.decenturion.mobile.business.signin;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;
import com.decenturion.mobile.app.prefs.token.TokenPrefsOptions;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.request.SigninModel;
import com.decenturion.mobile.network.response.csrf.CsrfResult;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.signin.SigninResponse;
import com.decenturion.mobile.network.response.signin.SigninResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

import rx.Observable;

public class SigninInteractor implements ISigninInteractor {

    private IDeviceClient mIDeviceClient;
    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public SigninInteractor(IResidentClient IResidentClient,
                            IDeviceClient iDeviceClient,
                            IResidentRepository iResidentRepository,
                            IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIDeviceClient = iDeviceClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<SigninResult> signin(@NonNull String username,
                                           @NonNull String password,
                                           String captcha) {
        SigninModel model = new SigninModel();
        model.setUsername(username);
        model.setPassword(password);
        model.setReCaptcha(captcha);

        return Observable.just(model)
                .flatMap(m ->
                        Observable.zip(
                                mIDeviceClient.getCsrfToken()
                                        .doOnNext(d -> {
                                            CsrfResult result = d.getResult();
                                            ITokensPrefsManager iTokensPrefsManager = mIPrefsManager.getITokensPrefsManager();
                                            iTokensPrefsManager.setParam(TokenPrefsOptions.Keys.CSRF_TOKEN, result.getToken());
                                        })
//                                        .flatMap(d -> mIResidentClient.initCsrfToken())
                                ,
                                Observable.just(m),
                                (signinResponse, signinModel) -> signinModel)
                )
                .flatMap(m -> mIResidentClient.signin(m))
                .map(d -> {
                    SigninResult result = d.getResult();
                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    return result;
                })
                .doOnError(throwable -> {
                    if (throwable instanceof InputDataException) {
                        ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
                        if (!CollectionUtils.isEmpty(errorList)) {
                            Error error = errorList.get(0);
                            String key = error.getKey();
                            switch (key) {
                                case "username": {
                                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, username);
                                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, 0);
                                    return;
                                }
                            }
                        }
                    }
                });
    }
}
