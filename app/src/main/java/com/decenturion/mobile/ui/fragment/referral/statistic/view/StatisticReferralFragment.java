package com.decenturion.mobile.ui.fragment.referral.statistic.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.statistic.model.StatisticReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.statistic.presenter.IStatisticReferralPresenter;

import javax.inject.Inject;

import butterknife.BindView;

public class StatisticReferralFragment extends BaseFragment implements IStatisticReferralView {

    private int mInvited;
    private String mEarnedBtc;
    private String mEarnedEth;
    private String mPriceCitizen;

    /* UI */

    @BindView(R.id.priceCitizenView)
    TextView mPriceCitizenView;

    @BindView(R.id.earnedBtcView)
    TextView mEarnedBtcView;

    @BindView(R.id.earnedEthView)
    TextView mEarnedEthView;

    @BindView(R.id.invitedView)
    TextView mInvitedView;


    /* DI */

    @Inject
    IStatisticReferralPresenter mIStatisticReferralPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mInvited = arguments.getInt(Args.INVITED, 0);
            mEarnedBtc = arguments.getString(Args.EARNED_BTC, "0.0");
            mEarnedEth = arguments.getString(Args.EARNED_ETH, "0.0");
            mPriceCitizen = arguments.getString(Args.CITIZEN_PRICE, "0.0");
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ReferralComponent component = appDelegate
                .getIDIManager()
                .plusReferallComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_referral_statistic, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPriceCitizenView.setText(mPriceCitizen + " $");
        mEarnedBtcView.setText(mEarnedBtc);
        mEarnedEthView.setText(mEarnedEth);
        mInvitedView.setText(mInvited + " ppl");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIStatisticReferralPresenter.bindView(this);
        mIStatisticReferralPresenter.bindViewData();
    }

    @Override
    public void onBindViewData(@NonNull StatisticReferralModelView model) {
        mPriceCitizenView.setText(model.getCitizenPrice() + " $");
        mEarnedBtcView.setText(model.getEarnedBtc() + " btc");
        mEarnedEthView.setText(model.getEarnedEth() + " eth");
        mInvitedView.setText(String.valueOf(model.getInvited()));
    }

    @Override
    public void onResume() {
        super.onResume();
        mIStatisticReferralPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIStatisticReferralPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIStatisticReferralPresenter.unbindView();
    }

    public interface Args {
        String INVITED = "INVITED";
        String EARNED_ETH = "EARNED_ETH";
        String EARNED_BTC = "EARNED_BTC";
        String CITIZEN_PRICE = "CITIZEN_PRICE";
    }
}