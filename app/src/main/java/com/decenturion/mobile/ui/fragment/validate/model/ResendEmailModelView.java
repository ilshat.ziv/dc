package com.decenturion.mobile.ui.fragment.validate.model;

import android.support.annotation.NonNull;

public class ResendEmailModelView {

    private String mEmail;

    public ResendEmailModelView() {
    }

    public ResendEmailModelView(@NonNull String email) {
        mEmail = email;
    }

    public void setEmail(@NonNull String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }

}
