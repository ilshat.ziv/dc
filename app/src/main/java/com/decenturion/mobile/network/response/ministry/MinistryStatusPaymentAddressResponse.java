package com.decenturion.mobile.network.response.ministry;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryStatusPaymentAddressResponse extends BaseResponse {

    @JsonProperty("result")
    private MinistryStatusPaymentResult mResult;

    public MinistryStatusPaymentAddressResponse() {
    }

    @JsonIgnore
    public MinistryStatusPaymentResult getResult() {
        return mResult;
    }
}
