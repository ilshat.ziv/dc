package com.decenturion.mobile.network.response.payment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResidencePaymentInfoResult {

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("from")
    private String mFrom;

    @JsonProperty("to")
    private String mTo;

    @JsonProperty("txid")
    private String mTxid;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("usd")
    private String mUsd;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("contract")
    private String mContract;

    public ResidencePaymentInfoResult() {
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getFrom() {
        return mFrom;
    }

    @JsonIgnore
    public String getTo() {
        return mTo;
    }

    @JsonIgnore
    public String getTxid() {
        return mTxid;
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getUsd() {
        return mUsd;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getContract() {
        return mContract;
    }
}
