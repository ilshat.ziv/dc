package com.decenturion.mobile.network.response.password;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordResult {

    @JsonProperty("result")
    private boolean isChange;

    public ChangePasswordResult() {
    }

    @JsonIgnore
    public boolean isChange() {
        return isChange;
    }
}
