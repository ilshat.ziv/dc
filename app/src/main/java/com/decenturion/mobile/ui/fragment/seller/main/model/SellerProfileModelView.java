package com.decenturion.mobile.ui.fragment.seller.main.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResult;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PhotoModelView;

public class SellerProfileModelView {

    private String mUID;
    private String mStatus;
    private PhotoModelView mPhotoModelView;

    public SellerProfileModelView(@NonNull SellerResidentResult d,
                                  @NonNull ILocalistionManager iLocalistionManager) {
        mUID = d.getUuid();
        mStatus = initStatus(d.getStatus(), iLocalistionManager);

        String photo = d.getPhoto();
        if (photo != null) {
            mPhotoModelView = new PhotoModelView(photo);
        }
    }

    private String initStatus(@NonNull String status, @NonNull ILocalistionManager iLocalistionManager) {
        if (TextUtils.isEmpty(status)) {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_citizen);
        } else if (TextUtils.equals(status, "senator")) {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_senator);
        } else {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_honorary);
        }
    }

    public String getUID() {
        return mUID;
    }

    public String getStatus() {
        return mStatus;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }
}
