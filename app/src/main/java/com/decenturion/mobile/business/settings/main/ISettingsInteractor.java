package com.decenturion.mobile.business.settings.main;

import com.decenturion.mobile.network.response.SignoutResponse;

import rx.Observable;

public interface ISettingsInteractor {

    Observable<SignoutResponse> signout();
}
