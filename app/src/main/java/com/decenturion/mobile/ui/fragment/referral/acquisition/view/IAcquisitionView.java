package com.decenturion.mobile.ui.fragment.referral.acquisition.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.acquisition.model.AcquisitionModelView;

public interface IAcquisitionView extends IScreenFragmentView {

    void onBindViewData(@NonNull AcquisitionModelView model);

    void onSetPriceSuccess();
}
