package com.decenturion.mobile.network.response.traide.create;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTraideResponse extends BaseResponse {

    @JsonProperty("result")
    private CreateTraideResult mResult;

    public CreateTraideResponse() {
    }

    @JsonIgnore
    public CreateTraideResult getResult() {
        return mResult;
    }
}
