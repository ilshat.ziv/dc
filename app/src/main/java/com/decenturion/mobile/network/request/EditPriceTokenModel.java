package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EditPriceTokenModel {

    @JsonProperty("coin_id")
    private String mCoinId;

    @JsonProperty("ask_price")
    private String mAskPrice;

    @JsonProperty("category")
    private String mCategroy;

    public EditPriceTokenModel() {
    }

    @JsonIgnore
    public void setCoinId(String coinId) {
        mCoinId = coinId;
    }

    @JsonIgnore
    public void setAskPrice(String askPrice) {
        mAskPrice = askPrice;
    }

    @JsonIgnore
    public void setCategroy(String categroy) {
        mCategroy = categroy;
    }
}
