package com.decenturion.mobile.business.settings.passport;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PassportSettingsModelView;

import rx.Observable;

public interface IPassportSettingsInteractor {

    Observable<PassportSettingsModelView> getPassportInfo();

    Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName,
                                                   @NonNull String secondName,
                                                   long birth,
                                                   @NonNull String sex,
                                                   @NonNull String country,
                                                   @NonNull String city);

    Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap);
}
