package com.decenturion.mobile.network.response.referral.history;

import com.decenturion.mobile.network.response.model.ReferralHistoryItem;
import com.decenturion.mobile.network.response.model.Transaction;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralHistoryResult {

    @JsonProperty("total")
    private int mTotal;

    @JsonProperty("list")
    private List<ReferralHistoryItem> mTransactionList;

    public ReferralHistoryResult() {
    }

    @JsonIgnore
    public List<ReferralHistoryItem> getTransactionList() {
        return mTransactionList;
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }
}
