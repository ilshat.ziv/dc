package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo extends BaseResponse {

    @JsonProperty("uuid")
    private String mUuid;

    /**
     * https://s3.amazonaws.com/dc-photo-production/original/82add897-9918-43d9-b74b-fd3bdab9a6ab.png
     */
    @JsonProperty("original")
    private String mOriginal;

    /**
     * https://s3.amazonaws.com/dc-photo-production/300x400/82add897-9918-43d9-b74b-fd3bdab9a6ab.png
     */
    @JsonProperty("min")
    private String mMin;

    public Photo() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getOriginal() {
        return mOriginal;
    }

    @JsonIgnore
    public String getMin() {
        return mMin;
    }
}
