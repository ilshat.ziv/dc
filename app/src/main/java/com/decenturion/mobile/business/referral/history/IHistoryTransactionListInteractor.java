package com.decenturion.mobile.business.referral.history;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;

import rx.Observable;

public interface IHistoryTransactionListInteractor {

    Observable<HistoryModelView> getTransactionsInfo(@NonNull HistoryModelView model);
}
