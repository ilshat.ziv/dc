package com.decenturion.mobile.ui.fragment.referral.main.model;

public class ReferralModelView {

    private String mTotal;
    private String mFree;
    private String mHold;

    private String mReferralLink;
    private String mCitizenPrice;
    private String mEarnedBtc;
    private String mEarnedEth;
    private int mInvited;

    public ReferralModelView() {
    }

    public ReferralModelView(String referralLink) {
        mReferralLink = referralLink;
    }

    public String getReferralLink() {
        return mReferralLink;
    }

    public void setReferralLink(String referralLink) {
        mReferralLink = referralLink;
    }

    public String getCitizenPrice() {
        return mCitizenPrice;
    }

    public void setCitizenPrice(String citizenPrice) {
        mCitizenPrice = citizenPrice;
    }

    public String getEarnedBtc() {
        return mEarnedBtc;
    }

    public void setEarnedBtc(String earnedBtc) {
        mEarnedBtc = earnedBtc;
    }

    public String getEarnedEth() {
        return mEarnedEth;
    }

    public void setEarnedEth(String earnedEth) {
        mEarnedEth = earnedEth;
    }

    public int getInvited() {
        return mInvited;
    }

    public void setInvited(int invited) {
        mInvited = invited;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

    public void setFree(String free) {
        mFree = free;
    }

    public void setHold(String hold) {
        mHold = hold;
    }

    public String getTotal() {
        return mTotal;
    }

    public String getFree() {
        return mFree;
    }

    public String getHold() {
        return mHold;
    }
}
