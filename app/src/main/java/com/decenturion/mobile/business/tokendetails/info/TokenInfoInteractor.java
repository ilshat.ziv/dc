package com.decenturion.mobile.business.tokendetails.info;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.token.info.v1.model.TokenInfoModelView;

import rx.Observable;

public class TokenInfoInteractor implements ITokenInfoInteractor {

    private IResidentClient mIResidentClient;
    private ITokenRepository mITokenRepository;
    private ILocalistionManager mILocalistionManager;

    public TokenInfoInteractor(IResidentClient IResidentClient,
                               ITokenRepository ITokenRepository,
                               ILocalistionManager iLocalistionManager) {
        mIResidentClient = IResidentClient;
        mITokenRepository = ITokenRepository;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<TokenInfoModelView> getTokenInfo(int tokenId) {
        return Observable.just(mITokenRepository.getTokenInfoById(tokenId))
                .map(TokenInfoModelView::new);
    }

    @Override
    public Observable<com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView> getTokenCategoryInfo(@NonNull String tokenCategory) {
        return Observable.just(tokenCategory)
                .map(d -> new com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView(d, mILocalistionManager));
    }
}
