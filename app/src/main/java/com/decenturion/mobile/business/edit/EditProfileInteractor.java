package com.decenturion.mobile.business.edit;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.UpdatePassportModel;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.edit.model.EditProfileModelView;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

public class EditProfileInteractor implements IEditProfileInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public EditProfileInteractor(IResidentClient IResidentClient,
                                 IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<EditProfileModelView> getProfileInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                EditProfileModelView::new);
    }

    @Override
    public Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName, @NonNull String secondName,
                                                          long birth, @NonNull String sex, @NonNull String country,
                                                          @NonNull String city) {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(isVaildFirstName(firstName)),
                Observable.just(isVaildSecondName(secondName)),
                        Observable.just(isVaildBirth(birth)),
                                Observable.just(isVaildSex(sex)),
                                        Observable.just(isVaildCountry(country)),
                                                Observable.just(isVaildCity(city)),
                (s, s2, s3, s4, s5, s6, s7) -> {
                    UpdatePassportModel model = new UpdatePassportModel();
                    model.setFirstName(s2);
                    model.setLastName(s3);
                    model.setBirth(DateTimeUtils.getDateFormat(s4, DateTimeUtils.Format.SIMPLE_FORMAT_2));
                    model.setSex(s5);
                    model.setCountry(s6);
                    model.setCity(s7);

                    return model;
                })
                .flatMap(m -> mIResidentClient.updatePassport(m))
                .doOnNext(d -> {
                    try {
                        UpdatePassportResult result = d.getResult();
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    private String isVaildCity(String city) {
        if (TextUtils.isEmpty(city)) {
            throw new RuntimeException("City is empty");
        }
        return city;
    }

    private String isVaildCountry(String country) {
        if (TextUtils.isEmpty(country)) {
            throw new RuntimeException("Country is empty");
        }
        return country;
    }

    private String isVaildSex(String sex) {
        if (TextUtils.isEmpty(sex)) {
            throw new RuntimeException("Sex is empty");
        }
        return sex;
    }

    private long isVaildBirth(long birth) {
        if (birth == 0) {
            throw new RuntimeException("Birth is not 0");
        }
        return birth;
    }

    private String isVaildFirstName(String firstName) {
        if (TextUtils.isEmpty(firstName)) {
            throw new RuntimeException("FirstName is empty");
        }
        return firstName;
    }

    private String isVaildSecondName(String secondName) {
        if (TextUtils.isEmpty(secondName)) {
            throw new RuntimeException("SecondName is empty");
        }
        return secondName;
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap) {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(OrmResidentModel::getUuid)
                .map(d -> {
                    byte[] bytes = ImageUtils.getBytes(bitmap);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    return MultipartBody.Part.createFormData("file", d + ".png", requestFile);
                })
                .flatMap(d -> mIResidentClient.uploadPhoto(d));
    }
}
