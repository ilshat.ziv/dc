package com.decenturion.mobile.ui.fragment.passport.phisical.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;
import com.decenturion.mobile.utils.DateTimeUtils;

public class PhisicalPassportModelView {

    private String mName;
    private String mFirstname;
    private String mLastname;
    private String mBirth;
    private String mSex;
    private String mCountry;
    private String mCity;

    private boolean isInvited;
    private boolean isActive;

    private DeliveryDataModelView mDeliverDataModelView;

    private PaymentModelView mPaymentModelView;

    public PhisicalPassportModelView() {
    }

    public PhisicalPassportModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1, OrmPhotoModel m2) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.getStep() > 4;
        mName = m0.getEmail();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();

        long datetime = DateTimeUtils.getDatetime(m1.getBirth(), DateTimeUtils.Format.FORMAT_ISO_8601);
        datetime = DateTimeUtils.convertToUTC(datetime);
        mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);

        mSex = m1.getSex();
        mCountry = m1.getCountry();
        mCity = m1.getCity();

        Delivery delivery = m1.getDelivery();

        mDeliverDataModelView = new DeliveryDataModelView(delivery);
    }

    public PhisicalPassportModelView(Resident resident) {
        mName = resident.getEmail();
        this.isInvited = resident.isInvited();
        this.isActive = resident.isActive();

        DeliverPaymentResult deliverPaymentResult = new DeliverPaymentResult();
        deliverPaymentResult.setAddress(resident.getWalletNum());
        deliverPaymentResult.setCoin("dcnt");
        deliverPaymentResult.setAmount(1);
        deliverPaymentResult.setAmountLeft(1);
        mPaymentModelView = new PaymentModelView(deliverPaymentResult);
    }

    public PhisicalPassportModelView(DeliverPaymentResult deliverPaymentResult, Resident resident) {
        mName = resident.getEmail();
        this.isInvited = resident.isInvited();
        this.isActive = resident.isActive();

        mPaymentModelView = new PaymentModelView(deliverPaymentResult);
    }

    public PhisicalPassportModelView(Resident m0, Passport m1, Photo photo) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.isActive();
        mName = m0.getEmail();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();

        long datetime = DateTimeUtils.getDatetime(m1.getBirth(), DateTimeUtils.Format.FORMAT_ISO_8601);
        datetime = DateTimeUtils.convertToUTC(datetime);
        mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);

        mSex = m1.getSex();
        mCountry = m1.getCountry();
        mCity = m1.getCity();

        Delivery delivery = m1.getDelivery();
        mDeliverDataModelView = new DeliveryDataModelView(delivery);
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getBirth() {
        return mBirth;
    }

    public String getSex() {
        return mSex;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public boolean isInvited() {
        return this.isInvited;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public DeliveryDataModelView getDeliverDataModelView() {
        return mDeliverDataModelView;
    }

    public PaymentModelView getPaymentModelView() {
        return mPaymentModelView;
    }
}
