package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.support.model.SupportMessage;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportModel {

    @JsonProperty("subject")
    private String mSubject;

    @JsonProperty("message")
    private String mMessage;

    public SupportModel() {
    }

    @JsonIgnore
    public SupportModel(String subject,String message) {
        mSubject = subject;
        mMessage = message;
    }

    @JsonIgnore
    public SupportModel(@NonNull SupportMessage model) {
        mSubject = model.getTheme();
        mMessage = model.getMessage();
    }
}
