package com.decenturion.mobile.ui.dialog.bottomsheet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.view.View;

import com.decenturion.mobile.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SellerMoreActionDialogFragment extends BaseBottomSheetDialog {

    private OnMoreActionDialogListener mOnMoreActionDialogListener;

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fr_dialog_bottomsheet_seller_more_action, null);
        dialog.setContentView(contentView);

        ButterKnife.bind(this, contentView);

        setPeekHeight(contentView);
    }

    public void setOnMoreActionDialogListener(OnMoreActionDialogListener listener) {
        mOnMoreActionDialogListener = listener;
    }

    @OnClick(R.id.copyProfileLinkView)
    protected void onCopyProfileLink() {
        mOnMoreActionDialogListener.onCopyProfileLink();
        dismiss();
    }


    @OnClick(R.id.shareView)
    protected void onShare() {
        mOnMoreActionDialogListener.onShare();
        dismiss();
    }

    public interface OnMoreActionDialogListener {
        void onCopyProfileLink();
        void onShare();
    }
}