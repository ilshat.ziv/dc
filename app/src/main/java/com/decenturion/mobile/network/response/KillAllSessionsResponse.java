package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KillAllSessionsResponse extends BaseResponse {

    public KillAllSessionsResponse() {
    }
}
