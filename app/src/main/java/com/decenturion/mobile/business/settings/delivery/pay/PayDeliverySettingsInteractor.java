package com.decenturion.mobile.business.settings.delivery.pay;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.PaymentModel;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResponse;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PayDeliverySettingsModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class PayDeliverySettingsInteractor implements IPayDeliverySettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public PayDeliverySettingsInteractor(IResidentClient IResidentClient,
                                         IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<PayDeliverySettingsModelView> getDeliveryData() {
        Observable<PayDeliverySettingsModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new PayDeliverySettingsModelView(resident, passport);
                })
                .onErrorReturn(throwable -> null);

        Observable<PayDeliverySettingsModelView> obs2 = Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                PayDeliverySettingsModelView::new);

        return Observable.concat(obs1, obs2)
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<PayDeliverySettingsModelView> genPaymentAddress(@NonNull String coin) {
        Observable<DeliverPaymentResult> obs1 = Observable.just(new PaymentModel(coin))
                .flatMap(d -> mIResidentClient.paymentDeliver(d))
                .map(DeliverPaymentResponse::getResult);

        Observable<Resident> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(ResidentResult::getResident);

        if (TextUtils.equals(coin, "dcnt")) {
            return obs2.map(PayDeliverySettingsModelView::new);
        } else {
            return obs2.flatMap(d -> {
                if (d.isDeliveryPaid()) {
                    return Observable.just(new PayDeliverySettingsModelView(d));
                } else {
                    return Observable.zip(obs1, Observable.just(d), PayDeliverySettingsModelView::new);
                }
            });
        }
    }
}
