package com.decenturion.mobile.di.dagger.settings.details;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.details.DetailsSettingsInteractor;
import com.decenturion.mobile.business.settings.details.IDetailsSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.details.presenter.DetailsSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.details.presenter.IDetailsSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsSettingsModule {

    @Provides
    @DetailsSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @DetailsSettingsScope
    IDetailsSettingsInteractor provideIDetailsSettingsInteractor(IResidentClient iResidentClient,
                                                                  IResidentRepository iResidentRepository) {
        return new DetailsSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @DetailsSettingsScope
    IDetailsSettingsPresenter provideIDetailsSettingsPresenter(IDetailsSettingsInteractor iDetailsSettingsInteractor,
                                                               ILocalistionManager iLocalistionManager) {
        return new DetailsSettingsPresenter(iDetailsSettingsInteractor, iLocalistionManager);
    }
}
