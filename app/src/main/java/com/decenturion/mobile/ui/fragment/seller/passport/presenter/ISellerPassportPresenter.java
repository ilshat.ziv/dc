package com.decenturion.mobile.ui.fragment.seller.passport.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.seller.passport.view.ISellerPassportView;

public interface ISellerPassportPresenter extends IPresenter<ISellerPassportView> {

    void bindViewData(@NonNull String residentUuid);

    PassportModelView getProfileView();
}
