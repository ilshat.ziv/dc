package com.decenturion.mobile.ui.fragment.pass.edit.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.network.response.BaseResponse;

public class PhotoModelView extends BaseResponse {

    private String mOriginal;
    private String mMin;

    public PhotoModelView() {
    }

    PhotoModelView(@NonNull OrmPhotoModel model) {
        mOriginal = model.getOriginal();
        mMin = model.getMin();
    }

    public String getOriginal() {
        return mOriginal;
    }

    public String getMin() {
        return mMin;
    }
}
