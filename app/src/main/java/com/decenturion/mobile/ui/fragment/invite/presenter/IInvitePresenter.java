package com.decenturion.mobile.ui.fragment.invite.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.invite.view.IInviteView;

public interface IInvitePresenter extends IPresenter<IInviteView> {

    void bindViewData();

    void inviteUser(@NonNull String email, @NonNull String token);

    void onSaveInstanceState(@NonNull Bundle outState, String value, String selection);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
