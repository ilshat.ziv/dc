package com.decenturion.mobile.ui.fragment.score.send.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.score.main.IMainScoreSendTokenInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.request.score.send.SendToExternalModel;
import com.decenturion.mobile.network.request.score.send.SendToTradeModel;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.score.send.main.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendToModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendToModelView;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendTokenModelView;
import com.decenturion.mobile.ui.fragment.score.send.main.view.IMainScoreSendTokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainScoreSendTokenPresenter extends Presenter<IMainScoreSendTokenView> implements IMainScoreSendTokenPresenter {

    private IMainScoreSendTokenInteractor mIMainScoreSendTokenInteractor;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    private MainScoreSendTokenModelView mSendTokenModelView;

    public MainScoreSendTokenPresenter(IMainScoreSendTokenInteractor iMainScoreSendTokenInteractor,
                                       IPrefsManager iPrefsManager,
                                       ILocalistionManager iLocalistionManager) {
        mIPrefsManager = iPrefsManager;
        mIMainScoreSendTokenInteractor = iMainScoreSendTokenInteractor;
        mILocalistionManager = iLocalistionManager;

        mSendTokenModelView = new MainScoreSendTokenModelView();
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mSendTokenModelView = new MainScoreSendTokenModelView(tokenId);

        Observable<MainScoreSendTokenModelView> obs = mIMainScoreSendTokenInteractor.getCoinInfo(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MainScoreSendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    String dcntWallet,
                                    String amount,
                                    String twofa) {

        bundle.putString("coinUUID", mSendTokenModelView.getCoinUUID());
        bundle.putString("coinCategory", mSendTokenModelView.getCoinCategory());
        bundle.putString("dcntWallet", dcntWallet);
        bundle.putString("amount", amount);
        bundle.putString("twofa", twofa);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        String coinUUID = savedInstanceState.getString("coinUUID", "");
        mSendTokenModelView.setCoinUUID(coinUUID);

        String coinCategory = savedInstanceState.getString("coinCategory", "");
        mSendTokenModelView.setCoinCategory(coinCategory);

        String dcntWallet = savedInstanceState.getString("dcntWallet", "");
        mSendTokenModelView.setDcntWallet(dcntWallet);

        String amount = savedInstanceState.getString("amount", "");
        mSendTokenModelView.setAmount(amount);

        mSendTokenModelView.setTwoFA(savedInstanceState.getString("twofa", ""));

        Observable<MainScoreSendTokenModelView> obs = Observable.just(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MainScoreSendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(MainScoreSendTokenModelView model) {
        MainScoreSendToModelView model1 = initSendToData();
        if (mView != null) {
            mView.onBindViewData(model, model1);
        }
        hideProgressView();
    }

    private MainScoreSendToModelView initSendToData() {
        ArrayList<MainScoreSendToModel> list = new ArrayList<>();
        list.add(new MainScoreSendToModel(
                mILocalistionManager.getLocaleString(R.string.app_token_send_tocitizen),
                MainScoreSendToModel.Key.TOCITIZEN));
        list.add(new MainScoreSendToModel(
                mILocalistionManager.getLocaleString(R.string.app_token_send_toexternal),
                MainScoreSendToModel.Key.TOEXTERNAL));
        list.add(new MainScoreSendToModel(
                mILocalistionManager.getLocaleString(R.string.app_token_send_totrade),
                MainScoreSendToModel.Key.TOTRADE));

        return new MainScoreSendToModelView(list);
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void sendToken(@NonNull String dcntWallet,
                          @NonNull String amount,
                          MethodSendToken method,
                          @NonNull String twofa) {

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        Observable<SendTokenResult> obs = mIMainScoreSendTokenInteractor.sendToCitizen(
                mSendTokenModelView.getCoinUUID(),
                mSendTokenModelView.getCoinCategory(),
                dcntWallet,
                amount,
                method.name().toLowerCase(),
                twofa
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenResult>) this::sendTokenSuccess,
                this::sendTokenFailure
        );
    }

    @Override
    public void sendToExternal(@NonNull String address, @NonNull String amount, @NonNull String twofa) {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        SendToExternalModel model = new SendToExternalModel();
        model.setAddress(address);
        model.setCoin(mSendTokenModelView.getCoinUUID());
        model.setCategory(mSendTokenModelView.getCoinCategory());
        model.setAmount(amount);
        model.setTwoFa(twofa);

        Observable<SendTokenResult> obs = mIMainScoreSendTokenInteractor.sendToExternal(model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenResult>) this::sendTokenSuccess,
                this::sendTokenFailure
        );
    }

    @Override
    public void sendToTrade(@NonNull String amount, @NonNull MethodSendToken method, @NonNull String twofa) {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        SendToTradeModel model = new SendToTradeModel();
        model.setCoin(mSendTokenModelView.getCoinUUID());
        model.setCategory(mSendTokenModelView.getCoinCategory());
        model.setAmount(amount);
        model.setMethod(method.name().toLowerCase());
        model.setTwoFa(twofa);

        Observable<SendTokenResult> obs = mIMainScoreSendTokenInteractor.sendToTrade(model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenResult>) this::sendTokenSuccess,
                this::sendTokenFailure
        );
    }

    private void sendTokenFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                switch (error.getKey()) {
                    default: {
//                        showErrorView(error.getMessage());
                        if (mView != null) {
                            mView.onSendTokenFail();
                        }
                    }
                }
                return;
            }
        }

        showErrorView(throwable);
    }

    private void sendTokenSuccess(SendTokenResult result) {
        hideProgressView();
        if (mView != null) {
            mView.onSendTokenSuccess();
        }
    }
}
