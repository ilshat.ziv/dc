package com.decenturion.mobile.ui.fragment.referral.history.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.referral.history.ReferralHistorylComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.history.HistoryAdapter;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModel;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;
import com.decenturion.mobile.ui.fragment.referral.history.model.State;
import com.decenturion.mobile.ui.fragment.referral.history.presenter.IHistoryPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

public class HistoryFragment extends BaseFragment implements IHistoryView,
        HistoryAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener,
        View.OnClickListener, SpinnerView_v2.OnSpinnerItemClickListener {

    /* UI */

    @BindView(R.id.stateView)
    SpinnerView_v2 mStateView;

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.historyTransactionListView)
    BaseRecyclerView mhistoryTransactionListView;
    private HistoryAdapter mTraidsAdapter;

    /* DI */

    @Inject
    IHistoryPresenter mILogsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ReferralHistorylComponent component = appDelegate
                .getIDIManager()
                .plusReferralHistorylComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_history_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        initToolbar();

        String localeString = mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_title);
        localeString = localeString.replaceAll(":.*", ":");
        mStateView.setLabel(localeString);
        mStateView.showLabel();
        mStateView.setOnSpinnerItemClickListener(this);

        mRefreshView.setOnRefreshListener(this);
        mhistoryTransactionListView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mhistoryTransactionListView.setLayoutManager(new LinearLayoutManager(context));
        mTraidsAdapter = new HistoryAdapter(context, this);
        mhistoryTransactionListView.setAdapter(mTraidsAdapter);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_referral_transactions_title));
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        iToolbarController.setNavigationOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mILogsPresenter.bindView(this);
//        if (savedInstanceState == null ||
//                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mILogsPresenter.bindViewData("");
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mILogsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mILogsPresenter.unbindView();
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mILogsPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onBindViewData(@NonNull HistoryModelView model) {
        ArrayList<State> states = model.getStates();
        mStateView.setData(
                mILocalistionManager.getLocaleString(R.string.app_referral_transactions_type_all),
                states);

        mTraidsAdapter.replaceDataSet(model.getHistoryList());
    }

    @Override
    public void onItemClick(HistoryModel model) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mILogsPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        String selection = mStateView.getSelection();
        mILogsPresenter.bindViewData(selection);
    }

    @Override
    public void onBottomScrolled() {
        mILogsPresenter.onBottomScrolled();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mILogsPresenter.bindViewData(iSpinnerItem.getItemKey());
    }
}
