package com.decenturion.mobile.ui.fragment.token.info.v1.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.info.v1.view.ITokenInfoView;

public interface ITokenInfoPresenter extends IPresenter<ITokenInfoView> {

    void bindViewData(int tokenId);

    void bindViewData(String category);
}
