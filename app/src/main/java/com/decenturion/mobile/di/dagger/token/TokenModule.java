package com.decenturion.mobile.di.dagger.token;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.token.ITokenInteractor;
import com.decenturion.mobile.business.token.TokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.token.presenter.ITokenPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.presenter.TokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TokenModule {

    @Provides
    @TokenScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TokenScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @TokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TokenScope
    ITokenInteractor provideITokenInteractor(IResidentClient IResidentClient,
                                             ITokenClient iTokenClient,
                                             IScoreClient iScoreClient,
                                             IResidentRepository iResidentRepository,
                                             IWalletRepository iWalletRepository,
                                             ITokenRepository iTokenRepository,
                                             IPrefsManager iPrefsManager) {

        return new TokenInteractor(
                IResidentClient,
                iTokenClient,
                iScoreClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository,
                iPrefsManager
        );
    }

    @Provides
    @TokenScope
    ITokenPresenter provideITokenPresenter(ITokenInteractor iTokenInteractor) {
        return new TokenPresenter(iTokenInteractor);
    }
}
