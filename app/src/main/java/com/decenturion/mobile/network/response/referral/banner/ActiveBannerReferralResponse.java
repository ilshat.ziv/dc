package com.decenturion.mobile.network.response.referral.banner;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActiveBannerReferralResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean mResult;

    public ActiveBannerReferralResponse() {
    }

    @JsonIgnore
    public boolean getResult() {
        return mResult;
    }
}
