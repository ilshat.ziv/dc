//package com.decenturion.mobile.ui.component;
//
//import android.content.Context;
//import android.os.Parcelable;
//import android.support.annotation.DrawableRes;
//import android.support.annotation.Nullable;
//import android.util.AttributeSet;
//import android.util.SparseArray;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.decenturion.mobile.R;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//
//public abstract class BaseView implements Component {
//
//    @Nullable
//    @BindView(R.id.labelView)
//    TextView mLabelView;
//
//    @Nullable
//    @BindView(R.id.iconView)
//    ImageView mIconView;
//
//    @Nullable
//    @BindView(R.id.dividerView)
//    View mDividerView;
//
//    private Unbinder mUnbinder;
//
//    Mode mMode;
//
//    protected boolean isInitView() {
//        return (mLabelView != null && mDividerView != null && mIconView != null);
//    }
//
//    protected void init() {
//        if (isInitView()) {
//            return;
//        }
//        mUnbinder = ButterKnife.bind(this, this);
//
//        if (!isInitView()) {
//            return;
//        }
//    }
//
//    public void setIconView(@DrawableRes int resource) {
//        init();
//        mIconView.setImageResource(resource);
//        mIconView.setVisibility(View.VISIBLE);
//    }
//
//    @Nullable
//    @Override
//    public ImageView getIconView() {
//        return mIconView;
//    }
//
//    public void hideIconView() {
//        init();
//        mIconView.setVisibility(View.GONE);
//    }
//
//    public void setAlphaIconView(float alpha) {
//        mIconView.setAlpha(alpha);
//    }
//
//    public void setLabel(String label) {
//        init();
//        if (label != null) {
//            mLabelView.setText(String.valueOf(label));
//        }
//        showLabel();
//    }
//
//    @Override
//    public void hideLabel() {
//        init();
//        ViewGroup view = (ViewGroup) mLabelView.getParent();
//        view.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void showLabel() {
//        init();
//        ViewGroup view = (ViewGroup) mLabelView.getParent();
//        view.setVisibility(View.VISIBLE);
//    }
//
//    public void setVisibleDivider(boolean isVisible) {
//        init();
//        if (isVisible) {
//            mDividerView.setVisibility(View.VISIBLE);
//        } else {
//            mDividerView.setVisibility(View.GONE);
//        }
//    }
//
//    @Override
//    public void editMode() {
//        mMode = Mode.EDIT;
//    }
//
//    @Override
//    public void simpleMode() {
//        mMode = Mode.NORMAL;
//    }
//
//    public boolean isEditMode() {
//        return mMode == Mode.EDIT;
//    }
//
//    @Override
//    protected void onLayout(boolean changed, int l, int t, int r, int b) {
//        super.onLayout(changed, l, t, r, b);
//        init();
//    }
//
//    @Override
//    public Parcelable onSaveInstanceState() {
//        Parcelable superState = super.onSaveInstanceState();
//        SavedState ss = new SavedState(superState);
//        ss.setChildrenStates(new SparseArray());
//        for (int i = 0; i < getChildCount(); i++) {
//            getChildAt(i).saveHierarchyState(ss.getChildrenStates());
//        }
//        return ss;
//    }
//
//    @Override
//    public void onRestoreInstanceState(Parcelable state) {
//        SavedState ss = (SavedState) state;
//        super.onRestoreInstanceState(ss.getSuperState());
//        for (int i = 0; i < getChildCount(); i++) {
//            getChildAt(i).restoreHierarchyState(ss.getChildrenStates());
//        }
//    }
//
//    @Override
//    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
//        dispatchFreezeSelfOnly(container);
//    }
//
//    @Override
//    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
//        dispatchThawSelfOnly(container);
//    }
//
//    @Override
//    public void destroyDrawingCache() {
//        if (mUnbinder != null) {
//            mUnbinder.unbind();
//            mUnbinder = null;
//        }
//        super.destroyDrawingCache();
//    }
//}
