package com.decenturion.mobile.network.response.trades;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradesResponse extends BaseResponse {

    @JsonProperty("result")
    private TradesResult mResult;

    public TradesResponse() {
    }

    @JsonIgnore
    public TradesResult getResult() {
        return mResult;
    }
}
