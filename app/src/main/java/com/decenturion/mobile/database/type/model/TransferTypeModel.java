package com.decenturion.mobile.database.type.model;

import com.decenturion.mobile.network.response.model.Transfer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTypeModel {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("to")
    private String mTo;

    public TransferTypeModel() {
    }

    public TransferTypeModel(Transfer transfer) {
        mAmount = transfer.getAmount();
        mCoin = transfer.getCoin();
        mStatus = transfer.getStatus();
        mTo = transfer.getTo();
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getTo() {
        return mTo;
    }
}
