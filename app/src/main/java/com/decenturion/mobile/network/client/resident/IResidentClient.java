package com.decenturion.mobile.network.client.resident;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.request.ChangeDetailsModel;
import com.decenturion.mobile.network.request.ChangePasswordModel;
import com.decenturion.mobile.network.request.CheckDcntWalletModel;
import com.decenturion.mobile.network.request.CheckInviteModel;
import com.decenturion.mobile.network.request.ConfirmEmailModel;
import com.decenturion.mobile.network.request.DeliveryPassportModel;
import com.decenturion.mobile.network.request.DisableGfaModel;
import com.decenturion.mobile.network.request.EnableGfaModel;
import com.decenturion.mobile.network.request.InviteModel;
import com.decenturion.mobile.network.request.PaymentModel;
import com.decenturion.mobile.network.request.ResendEmailModel;
import com.decenturion.mobile.network.request.ResetPasswordModel;
import com.decenturion.mobile.network.request.RestorePassModel;
import com.decenturion.mobile.network.request.score.SendTokenModel;
import com.decenturion.mobile.network.request.SigninGfaModel;
import com.decenturion.mobile.network.request.SigninModel;
import com.decenturion.mobile.network.request.SignupModel;
import com.decenturion.mobile.network.request.SubscribeModel;
import com.decenturion.mobile.network.request.SupportModel;
import com.decenturion.mobile.network.request.UpdatePassportModel;
import com.decenturion.mobile.network.request.UpdateSettingsModel;
import com.decenturion.mobile.network.request.WalletModel;
import com.decenturion.mobile.network.response.EnableGfaResponse;
import com.decenturion.mobile.network.response.KillAllSessionsResponse;
import com.decenturion.mobile.network.response.actions.ActionsResponse;
import com.decenturion.mobile.network.response.delivery.skip.SkipDeliveryResponse;
import com.decenturion.mobile.network.response.details.ChangeDetailsResponse;
import com.decenturion.mobile.network.response.invite.CheckInviteTokenResponse;
import com.decenturion.mobile.network.response.invite.send.InviteResponse;
import com.decenturion.mobile.network.response.password.ChangePasswordResponse;
import com.decenturion.mobile.network.response.payment.ResidencePaymentInfoResponse;
import com.decenturion.mobile.network.response.payment.citizen.CitizenPaymentResponse;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResponse;
import com.decenturion.mobile.network.response.email.ConfirmEmailResponse;
import com.decenturion.mobile.network.response.ResendEmailResponse;
import com.decenturion.mobile.network.response.reset.ResetPasswordResponse;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResponse;
import com.decenturion.mobile.network.response.restore.RestorePassResponse;
import com.decenturion.mobile.network.response.send.SendTokenResponse;
import com.decenturion.mobile.network.response.signin.SigninResponse;
import com.decenturion.mobile.network.response.SignoutResponse;
import com.decenturion.mobile.network.response.signin.gfa.SigninGfaResponse;
import com.decenturion.mobile.network.response.signup.SignupResponse;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.settings.UpdateSettingsResponse;
import com.decenturion.mobile.network.response.signup.skip.SignupSkipResponse;
import com.decenturion.mobile.network.response.subscribe.SubscribeResponse;
import com.decenturion.mobile.network.response.support.SupportResponse;
import com.decenturion.mobile.network.response.transactions.TransactionsResponse;
import com.decenturion.mobile.network.response.wallets.CheckWalletResponse;
import com.decenturion.mobile.network.response.wallets.SaveWalletResponse;
import com.decenturion.mobile.network.response.wallets.WalletResponse;

import java.util.LinkedHashMap;

import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.OPTIONS;
import retrofit2.http.POST;

import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface IResidentClient {

    @GET(BuildConfig.API_V + "resident")
    Observable<ResidentResponse> getResident();

    @GET(BuildConfig.API_V + "public/resident/{residenId}")
    Observable<SellerResidentResponse> getSelletResident(@Path("residenId") String residentUUID);

    @GET(BuildConfig.API_V + "resident/wallet/trade")
    Observable<WalletResponse> getWallets();

    @GET(BuildConfig.API_V + "resident/transactions")
    Observable<TransactionsResponse> getTransations(@Query("offset") long offset, @Query("limit") long limit);

    @GET(BuildConfig.API_V + "resident/activity")
    Observable<ActionsResponse> getActions(@Query("offset") long offset, @Query("limit") long limit);

    /**
     * https://asset.decenturion.com/mobile/translations/
     * @param locale - ru-RU, en-US
     *
     * @return
     */
    @GET("https://asset.decenturion.com/mobile/translations/{locale}.json")
    Observable<LinkedHashMap<String, String>> getLocaleWords(@Path("locale") String locale);

    /**
     * Request for init resident
     *
     * @param model - request parametrs of new resident
     * @return request token
     */

//    @Multipart
    @POST(BuildConfig.API_V + "resident/signup")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SignupResponse> signup(@Body SignupModel model);

    @POST(BuildConfig.API_V + "resident/signup/skip")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SignupSkipResponse> skipSignup();

    @POST(BuildConfig.API_V + "resident/signup/skip-delivery")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SkipDeliveryResponse> skipDelivery();

    @OPTIONS(BuildConfig.API_V + "resident/signin")
    @Headers({
            "Access-Control-Request-Headers: x-csrf-token",
            "Access-Control-Request-Method: POST"
    })
    Observable<SigninResponse> initCsrfToken();

    @POST(BuildConfig.API_V + "resident/signin")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SigninResponse> signin(@Body SigninModel model);

    @POST(BuildConfig.API_V + "resident/resend")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ResendEmailResponse> resendEmail(@Body ResendEmailModel model);

    @POST(BuildConfig.API_V + "resident/confirm")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ConfirmEmailResponse> confirmEmail(@Body ConfirmEmailModel model);

    @POST(BuildConfig.API_V + "resident/g2fa/signin")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SigninGfaResponse> signin2gfa(@Body SigninGfaModel model);

    @POST(BuildConfig.API_V + "resident/g2fa/enable")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<EnableGfaResponse> enableG2fa(@Body EnableGfaModel model);

    @POST(BuildConfig.API_V + "resident/g2fa/disable")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<EnableGfaResponse> disableG2fa(@Body DisableGfaModel model);

    @POST(BuildConfig.API_V + "resident/signout")
    Observable<SignoutResponse> signout();

    @POST(BuildConfig.API_V + "resident/passport")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<UpdatePassportResponse> updatePassport(@Body UpdatePassportModel model);

    @Multipart
    @POST(BuildConfig.API_V + "resident/photo")
    Observable<UploadPhotoResponse> uploadPhoto(@Part MultipartBody.Part image);

    @POST(BuildConfig.API_V + "resident/settings")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<UpdateSettingsResponse> updateSettings(@Body UpdateSettingsModel model);

    @POST(BuildConfig.API_V + "resident/passport/delivery")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<DeliveryPassportResponse> delivery(@Body DeliveryPassportModel model);

    @POST(BuildConfig.API_V + "resident/address/deposit")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CitizenPaymentResponse> paymentCitizen(@Body PaymentModel model);

    @GET(BuildConfig.API_V + "resident/payment")
    Observable<ResidencePaymentInfoResponse> getResidencePaymentInfo();

//    @POST(BuildConfig.API_V1 + "resident/payment/address")
    @POST(BuildConfig.API_V + "resident/payment/address")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<DeliverPaymentResponse> paymentDeliver(@Body PaymentModel model);

//    50 баксов за доставку адреса
//
//    POST /resident/payment/address
//
//    параметры coin: string, ordered: bool
//
//===
//
//        1500 баксов за гражданство
//    POST /resident/address/deposit
//
//    параметры такие же как и выше

    @POST(BuildConfig.API_V + "resident/invite/check")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CheckInviteTokenResponse> checkTokenInvite(@Body CheckInviteModel model);



    @POST(BuildConfig.API_V + "resident/password")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ChangePasswordResponse> changePassword(@Body ChangePasswordModel model);

    @POST(BuildConfig.API_V + "resident/profile")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ChangeDetailsResponse> changeDetails(@Body ChangeDetailsModel model);

    @POST(BuildConfig.API_V + "resident/subscribe")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SubscribeResponse> subscribe(@Body SubscribeModel model);

    @POST(BuildConfig.API_V + "resident/invite")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<InviteResponse> invite(@Body InviteModel model);

    /**
     * Восстановление пароля
     *
     * @param model
     * @return
     */

    @POST(BuildConfig.API_V + "resident/password/request")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<RestorePassResponse> restorePass(@Body RestorePassModel model);

    @POST(BuildConfig.API_V + "resident/password/reset")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ResetPasswordResponse> resetPass(@Body ResetPasswordModel model);

    @POST(BuildConfig.API_V + "resident/wallet")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SaveWalletResponse> saveWallet(@Body WalletModel model);

    @POST(BuildConfig.API_V + "resident/signout/all")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<KillAllSessionsResponse> killAllSessions();

    @POST(BuildConfig.API_V + "contacts")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SupportResponse> sendSupport(@Body SupportModel model);

    /**
     * Отправка токенов другому гражданину
     *
     */

    @POST(BuildConfig.API_V + "resident/transfer/internal/create")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SendTokenResponse> sendToken(@Body SendTokenModel model);

    @POST(BuildConfig.API_V + "resident/transfer/internal/address/check")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CheckWalletResponse> checkDCNTWallet(@Body CheckDcntWalletModel model);

}
