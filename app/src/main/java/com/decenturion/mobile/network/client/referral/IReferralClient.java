package com.decenturion.mobile.network.client.referral;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.request.CheckReferallModel;
import com.decenturion.mobile.network.request.CreateInvite;
import com.decenturion.mobile.network.request.ReferralSettings;
import com.decenturion.mobile.network.request.RemoveInvite;
import com.decenturion.mobile.network.request.referral.ActiveBannerReferralModel;
import com.decenturion.mobile.network.request.referral.ActivePayReferralModel;
import com.decenturion.mobile.network.response.CheckReferallKeyResponse;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResponse;
import com.decenturion.mobile.network.response.referral.active.ActivePayReferralResponse;
import com.decenturion.mobile.network.response.referral.banner.ActiveBannerReferralResponse;
import com.decenturion.mobile.network.response.referral.history.ReferralHistoryResponse;
import com.decenturion.mobile.network.response.referral.invite.ReferralInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.create.CreateInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResponse;
import com.decenturion.mobile.network.response.referral.total.ReferralTotalResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface IReferralClient {

    @POST(BuildConfig.API_V + "resident/referral/settings")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ReferralSettingsResponse> setReferralSettings(@Body ReferralSettings model);

    @GET(BuildConfig.API_V + "resident/referral/settings")
    Observable<ReferralSettingsResponse> getReferralSettings();

    @GET(BuildConfig.API_V + "resident/referral/stats/total")
    Observable<ReferralTotalResponse> getReferralTotal();

    @GET(BuildConfig.API_V + "resident/referral/stats")
    Observable<ReferralHistoryResponse> getReferralHistory(
            @Query("offset") long offset,
            @Query("limit") long limit,
            @Query("type") String type
    );

    @POST(BuildConfig.API_V + "resident/referral/invite/generate")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CreateInviteResponse> createInvite(@Body CreateInvite model);

    @POST(BuildConfig.API_V + "resident/referral/invite/remove")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<RemoveInviteResponse> removeInvite(@Body RemoveInvite model);

    @POST(BuildConfig.API_V + "resident/referral/invite/list")
//    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ReferralInviteResponse> getReferralInviteList();



    @POST(BuildConfig.API_V + "resident/ref")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CheckReferallKeyResponse> checkReferallKey(@Body CheckReferallModel model);

    // Активация платного инвайта

    @POST(BuildConfig.API_V + "resident/referral/invite/activate")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
//    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    Observable<ActivePayReferralResponse> activePayReferкalKey(@Body ActivePayReferralModel model);

    // Активация кода из баннера

    @POST(BuildConfig.API_V + "resident/ref/paid")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ActiveBannerReferralResponse> activeBannerKey(@Body ActiveBannerReferralModel model);
}
