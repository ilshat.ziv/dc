package com.decenturion.mobile.app.prefs.token;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.BasePrefsManager;

import java.util.prefs.Preferences;

public class TokenPrefsManager extends BasePrefsManager implements ITokensPrefsManager {

    private static final String SHARED_PREFERENCES_NAME = "TOKEN_PREFS";

    public TokenPrefsManager(@NonNull Context context) {
        super(context, SHARED_PREFERENCES_NAME);
    }

}
