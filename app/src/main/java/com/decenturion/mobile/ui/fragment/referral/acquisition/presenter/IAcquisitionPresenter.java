package com.decenturion.mobile.ui.fragment.referral.acquisition.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.acquisition.view.IAcquisitionView;

public interface IAcquisitionPresenter extends IPresenter<IAcquisitionView> {

    void bindViewData();

    void setPrice(String type, String price, String amount, String bannerText);

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
