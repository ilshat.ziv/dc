package com.decenturion.mobile.business.score.express;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.CheckDcntWalletModel;
import com.decenturion.mobile.network.request.score.SendTokenModel;
import com.decenturion.mobile.network.response.send.SendTokenResponse;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.network.response.wallets.CheckWalletResponse;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.express.model.ExpressSendTokenModelView;

import rx.Observable;

public class ExpressSendTokenInteractor implements IExpressSendTokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenRepository mITokenRepository;

    public ExpressSendTokenInteractor(IResidentClient IResidentClient, ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mITokenRepository = iTokenRepository;
    }

    @Override
    public Observable<ExpressSendTokenModelView> getCoinInfo(@NonNull ExpressSendTokenModelView model) {
        Observable<OrmTokenModel> obs1 = Observable.just(model.getTokenId())
                .map(d -> mITokenRepository.getTokenById(d));

        Observable<ExpressSendTokenModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (ormTokenModel, sendTokenModelView) -> {
            sendTokenModelView.setCoinCategory(ormTokenModel.getCategory());
            CoinTypeModel coinTypeModel = ormTokenModel.getCoinTypeModel();
            sendTokenModelView.setCoinUUID(coinTypeModel.getUuid());
            sendTokenModelView.setSymbol(coinTypeModel.getSymbol());
            return sendTokenModelView;
        });
    }

    @Override
    public Observable<SendTokenResult> sendToken(
            @NonNull String coinUUID,
            @NonNull String twofa) {

        SendTokenModel model = new SendTokenModel();
        model.setCoin(coinUUID);
        model.setTwoFa(twofa);

        CheckDcntWalletModel model1 = new CheckDcntWalletModel(null);
        return mIResidentClient.checkDCNTWallet(model1)
                .map(CheckWalletResponse::getResult)
                .map(d -> {
                    if (!d.isPresence()) {
                        throw new RuntimeException("Address is not a valid ERC-20");
                    }

                    return true;
                })
                .flatMap(d -> mIResidentClient.sendToken(model))
                .map(SendTokenResponse::getResult);
    }
}
