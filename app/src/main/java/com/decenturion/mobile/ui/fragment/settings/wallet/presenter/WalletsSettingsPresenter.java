package com.decenturion.mobile.ui.fragment.settings.wallet.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.settings.wallets.IWalletsSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletModel;
import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletsSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.wallet.view.IWalletsSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class WalletsSettingsPresenter extends Presenter<IWalletsSettingsView> implements IWalletsSettingsPresenter {

    private IWalletsSettingsInteractor mIWalletsSettingsInteractor;
    private IPrefsManager mIPrefsManager;

    private WalletsSettingsModelView mPasswordSettingsModelView;

    public WalletsSettingsPresenter(IWalletsSettingsInteractor iWalletsSettingsInteractor,
                                    IPrefsManager iPrefsManager) {
        mIWalletsSettingsInteractor = iWalletsSettingsInteractor;
        mIPrefsManager = iPrefsManager;
        mPasswordSettingsModelView = new WalletsSettingsModelView();
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<WalletsSettingsModelView> obs = mIWalletsSettingsInteractor.getWalletsInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<WalletsSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(WalletsSettingsModelView model) {
        mPasswordSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    @NonNull String btcAddress,
                                    @NonNull String ethAddress,
                                    @NonNull String twofacode) {

        bundle.putString("btcAddress", btcAddress);
        bundle.putString("ethAddress", ethAddress);
        bundle.putString("twofacode", twofacode);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        String btcAddress = savedInstanceState.getString("btcAddress", "");
        mPasswordSettingsModelView.setBtcWallet(new WalletModel(btcAddress));

        String ethAddress = savedInstanceState.getString("ethAddress", "");
        mPasswordSettingsModelView.setEthWallet(new WalletModel(ethAddress));

        mPasswordSettingsModelView.setSecretCode(savedInstanceState.getString("twofacode", ""));

        Observable<WalletsSettingsModelView> obs = Observable.just(mPasswordSettingsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<WalletsSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void saveWallets(@NonNull String btcAddress,
                            @NonNull String ethAddress,
                            @NonNull String twofacode) {

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        Observable<Boolean> obs = mIWalletsSettingsInteractor.saveWallets(
                btcAddress,
                ethAddress,
                twofacode
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<Boolean>) this::saveWalletsSuccess,
                this::saveWalletsFailure
        );
    }

    private void saveWalletsFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                switch (error.getKey()) {
//                    case "form": {
//                        if (mView != null) {
//                            mView.onEnableTwoFA();
//                        }
//                        break;
//                    }
                    default: {
                        showErrorView(error.getMessage());
                    }
                }
                return;
            }
        }

        showErrorView(throwable);
    }

    private void saveWalletsSuccess(Boolean model) {
        hideProgressView();
    }

}
