package com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.delivery.delivered.DeliveredSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.UserDataView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model.DeliveredSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter.IDeliveredSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class DeliveredSettingsFragment extends BaseFragment implements IDeliveredSettingsView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.phoneView)
    UserDataView mPhoneView;

    @BindView(R.id.addressView)
    UserDataView mAddressView;

    @BindView(R.id.cityView)
    UserDataView mCityView;

    @BindView(R.id.stateView)
    UserDataView mStateView;

    @BindView(R.id.countyView)
    UserDataView mCountryView;

    @BindView(R.id.zipView)
    UserDataView mZipView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.proceedButtonView)
    Button mProceedButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IDeliveredSettingsPresenter mIDeliverySettingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        DeliveredSettingsComponent deliveredSettingsComponent = appDelegate
                .getIDIManager()
                .plusDeliveredSettingsComponent();
        deliveredSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_delivered_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_phone_title));
        mPhoneView.editMode();

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_address_title));
        mAddressView.editMode();

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_city_title));
        mCityView.editMode();

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_state_title));
        mStateView.editMode();

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_country_title));

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_zip_title));
        mZipView.editMode();

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDeliverySettingsPresenter.bindView(this);
        mIDeliverySettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDeliverySettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDeliverySettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
        mIDeliverySettingsPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onBindViewData(@NonNull DeliveredSettingsModelView model2) {
        mPhoneView.setText(model2.getPhone());
        mAddressView.setText(model2.getAddress());
        mCityView.setText(model2.getCity());
        mStateView.setText(model2.getState());
        mCountryView.setText(model2.getCountry());
        mZipView.setText(model2.getZip());
    }
}
