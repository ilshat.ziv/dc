package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmEmailModel {

    @JsonProperty("token")
    private String mToken;

    public ConfirmEmailModel() {
    }

    @JsonIgnore
    public ConfirmEmailModel(String token) {
        mToken = token;
    }

    @JsonIgnore
    public void setToken(@NonNull String token) {
        mToken = token;
    }

}
