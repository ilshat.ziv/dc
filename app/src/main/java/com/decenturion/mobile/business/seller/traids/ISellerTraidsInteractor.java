package com.decenturion.mobile.business.seller.traids;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModelView;

import rx.Observable;

public interface ISellerTraidsInteractor {

    Observable<SellerTraideModelView> getTraidsModelView(@NonNull String sellerUUID);
}
