package com.decenturion.mobile.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.dialog.DialogManager;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.floating.keybord.IKeyboardVisionManager;
import com.decenturion.mobile.ui.floating.keybord.KeyboardVisionManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.ministry.market.view.MinistryFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.main.view.ProfileFragment;
import com.decenturion.mobile.ui.fragment.referral.main.view.ReferralFragment;
import com.decenturion.mobile.ui.fragment.settings.main.view.SettingsFragment;
import com.decenturion.mobile.ui.fragment.support.view.SupportFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.navigation.NavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.toolbar.ToolbarController;

import butterknife.BindView;

public abstract class DrawerActivity extends BaseActivity implements IDrawerActivity,
        NavigationView.OnNavigationItemSelectedListener,
        NavigationManager.OnCustomBackStackChangedListener {

    private IToolbarController mIToolbarController;
    private INavigationManager mINavigationManager;
    private IDialogManager mIDialogManager;
    private IKeyboardVisionManager mIKeyboardVisionManager;

    /* UI */

    @BindView(R.id.toolbarView)
    Toolbar mToolbarView;

    @BindView(R.id.appBarView)
    AppBarLayout mAppBarLayoutView;

    @BindView(R.id.contentView)
    FrameLayout mContentFrameView;

    @BindView(R.id.drawerView)
    DrawerLayout mDrawerView;

    @BindView(R.id.navigationView)
    protected NavigationView mNavigationView;

    private ActionBarDrawerToggle mActionBarDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main_drawer);
        setupUi();
    }

    private void setupUi() {
        mIToolbarController = new ToolbarController(this, mAppBarLayoutView);
        mINavigationManager = new NavigationManager(this, R.id.contentView);
        ((NavigationManager) mINavigationManager).setOnCustomBackStackChangedListener(this);
        mIDialogManager = new DialogManager(getSupportFragmentManager());
        mIKeyboardVisionManager = new KeyboardVisionManager(this);

        initDrawerView();
    }

    private void initDrawerView() {
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerView,
                mToolbarView,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                // TODO: 10.08.2018 hideKeyBoard

            }
        };

        mDrawerView.post(() -> mActionBarDrawerToggle.syncState());
        mDrawerView.addDrawerListener(mActionBarDrawerToggle);

        initMenu();
    }

    private void initMenu() {
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mINavigationManager.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerView.isDrawerOpen(GravityCompat.START)) {
            mDrawerView.closeDrawer(GravityCompat.START);
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIDialogManager.onDestroy();
        mIToolbarController.onDestroy();
        mINavigationManager.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = (Fragment) mINavigationManager.getCurentFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public IToolbarController getIToolbarController() {
        return mIToolbarController;
    }

    @Override
    public INavigationManager getINavigationManager() {
        return mINavigationManager;
    }

    @Override
    public IKeyboardVisionManager getIKeyboardVisionManager() {
        return mIKeyboardVisionManager;
    }

    @Override
    public IDialogManager getIDialogManager() {
        return mIDialogManager;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) {
            onCloseDrawer();
            return true;
        }

        int id = item.getItemId();
        if (id == R.id.accountMenuDrawer) {
            mINavigationManager.navigateTo(ProfileFragment.class);
        } else if (id == R.id.settingsMenuDrawer) {
            mINavigationManager.navigateTo(SettingsFragment.class, true);
        } else if (id == R.id.supportMenuDrawer) {
            mINavigationManager.navigateTo(SupportFragment.class, true);
        } else if (id == R.id.ministryMenuDrawer) {
            mINavigationManager.navigateTo(MinistryFragment.class, true);
        } else if (id == R.id.referralMenuDrawer) {
            mINavigationManager.navigateTo(ReferralFragment.class, true);
        }

        onCloseDrawer();
        return true;
    }

    private void onCloseDrawer() {
        mDrawerView.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackStackChange() {
        BaseFragment fragment = (BaseFragment) mINavigationManager.getCurentFragment();
        if (fragment == null) {
            int id = getIdMenu(ProfileFragment.class.getSimpleName());
            MenuItem item = getMenuItem(id);
            item.setChecked(true);
            return;
        }

        String tag = fragment.getTag();
        if (tag == null) {
            return;
        }
        int id = getIdMenu(tag);
        if (id != -1) {
            MenuItem item = getMenuItem(id);
            item.setChecked(true);
        }
    }

    private MenuItem getMenuItem(int idMenu) {
        Menu menu = mNavigationView.getMenu();
        return menu.findItem(idMenu);
    }

    private int getIdMenu(String tag) {
        switch (tag) {
            case "ProfileFragment": {
                return R.id.accountMenuDrawer;
            }
            case "SettingsFragment": {
                return R.id.settingsMenuDrawer;
            }
            case "MinistryFragment": {
                return R.id.ministryMenuDrawer;
            }
            case "SupportFragment": {
                return R.id.supportMenuDrawer;
            }
            case "CreateInviteFragment": {
                return R.id.referralMenuDrawer;
            }
            default: {
                return -1;
            }
        }
    }

}
