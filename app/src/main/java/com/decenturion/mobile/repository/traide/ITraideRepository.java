package com.decenturion.mobile.repository.traide;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTraideModel;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;

public interface ITraideRepository {

    OrmTraideModel getTraide(@NonNull String traideUUID);

    void saveTraideInfo(@NonNull CreateTraideResult d) throws Exception;

    void deleteTraide(@NonNull String uuid) throws Exception;

    void saveTraideInfo(@NonNull WrapperTrade wrapperTrade) throws Exception;
}
