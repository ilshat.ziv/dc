package com.decenturion.mobile.ui.fragment.header.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.header.HeaderComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.header.model.HeaderModelView;
import com.decenturion.mobile.ui.fragment.header.presenter.IHeaderPresenter;

import javax.inject.Inject;

import butterknife.BindView;

public class HeaderFragment extends BaseFragment implements IHeaderView {

    /* UI */

    @BindView(R.id.userNameView)
    TextView mUserNameView;

    @BindView(R.id.statusView)
    TextView mStatusView;

    /* DI */

    @Inject
    IHeaderPresenter mIHeaderPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        HeaderComponent headerComponent = appDelegate
                .getIDIManager()
                .plusHeaderComponent();
        headerComponent.inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_header_navigation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIHeaderPresenter.bindView(this);
        mIHeaderPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIHeaderPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIHeaderPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIHeaderPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull HeaderModelView model) {
        mUserNameView.setText(model.getName());
        mStatusView.setText(model.getStatus());
    }
}
