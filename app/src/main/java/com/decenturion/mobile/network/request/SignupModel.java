package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignupModel {

    @JsonProperty("username")
    private String mUsername;

    @JsonProperty("password")
    private String mPassword;

    @JsonProperty("recaptcha")
    private String mReCaptcha;

    @JsonProperty("subscribe")
    private boolean isSubscribe;

    @JsonProperty("invite")
    private String mInviteCode;

    public SignupModel() {
    }

    @JsonIgnore
    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    @JsonIgnore
    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    @JsonIgnore
    public void setReCaptcha(@NonNull String reCaptcha) {
        mReCaptcha = reCaptcha;
    }

    @JsonIgnore
    public void setSubscribe(boolean subscribe) {
        isSubscribe = subscribe;
    }

    @JsonIgnore
    public void setInviteCode(String inviteCode) {
        mInviteCode = inviteCode;
    }
}
