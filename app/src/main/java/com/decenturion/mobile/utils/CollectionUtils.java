package com.decenturion.mobile.utils;

import java.util.Collection;

public final class CollectionUtils {

    public static boolean isEmpty(final Collection< ? > c ) {
        return c == null || c.isEmpty();
    }

    public static int size(final Collection< ? > c ) {
        return (c != null) ? c.size() : 0;
    }
}
