package com.decenturion.mobile.business.referral.invite;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.request.RemoveInvite;
import com.decenturion.mobile.network.response.model.Invite;
import com.decenturion.mobile.network.response.referral.invite.ReferralInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.ReferralInviteResult;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResult;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteListModelView;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;

import java.util.List;

import rx.Observable;
import rx.functions.Func2;

public class InviteListInteractor implements IInviteListInteractor {

    private IReferralClient mIReferralClient;
    private ILocalistionManager mILocalistionManager;

    public InviteListInteractor(IReferralClient iReferralClient,
                                ILocalistionManager iLocalistionManager) {
        mIReferralClient = iReferralClient;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<InviteListModelView> getInvitations() {
        return mIReferralClient.getReferralInviteList()
                .map(ReferralInviteResponse::getResult)
                .flatMap(d -> {
                    List<Invite> list = d.getList();
                    if (list == null) {
                        return Observable.just(new InviteListModelView(d));
                    }

                    Observable<List<InviteModel>> obs = Observable.just(list)
                            .flatMapIterable(list1 -> list1)
                            .map(d1 -> new InviteModel(d1, mILocalistionManager))
                            .toList();

                    return Observable.zip(
                            obs,
                            Observable.just(d.getTotal()),
                            Observable.just(d.getUsed()),
                            InviteListModelView::new
                    );
                });
    }

    @Override
    public Observable<RemoveInviteResult> removeInvite(int inviteId) {
        RemoveInvite model1 = new RemoveInvite(inviteId);
        return mIReferralClient.removeInvite(model1)
                .map(RemoveInviteResponse::getResult);
    }

}
