package com.decenturion.mobile.di.dagger.referral.main;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.referral.main.IReferralInteractor;
import com.decenturion.mobile.business.referral.main.ReferralInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.referral.balance.presenter.BalanceReferralPresenter;
import com.decenturion.mobile.ui.fragment.referral.balance.presenter.IBalanceReferralPresenter;
import com.decenturion.mobile.ui.fragment.referral.main.presenter.IReferralPresenter;
import com.decenturion.mobile.ui.fragment.referral.main.presenter.ReferralPresenter;
import com.decenturion.mobile.ui.fragment.referral.statistic.presenter.IStatisticReferralPresenter;
import com.decenturion.mobile.ui.fragment.referral.statistic.presenter.StatisticReferralPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ReferralModule {

    @Provides
    @ReferralScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ReferralScope
    IReferralInteractor provideIReferralInteractor(IResidentClient iResidentClient,
                                                   IReferralClient iReferralClient,
                                                   IResidentRepository iResidentRepository) {

        return new ReferralInteractor(iResidentClient, iReferralClient, iResidentRepository);
    }

    @Provides
    @ReferralScope
    IReferralPresenter provideIReferralPresenter(IReferralInteractor iReferralInteractor) {
        return new ReferralPresenter(iReferralInteractor);
    }

    @Provides
    @ReferralScope
    IStatisticReferralPresenter provideIStatisticReferralPresenter(IReferralInteractor iReferralInteractor) {
        return new StatisticReferralPresenter(iReferralInteractor);
    }

    @Provides
    @ReferralScope
    IBalanceReferralPresenter provideIBalanceReferralPresenter(IReferralInteractor iReferralInteractor) {
        return new BalanceReferralPresenter(iReferralInteractor);
    }
}
