package com.decenturion.mobile.business.referral.invite.create;

import com.decenturion.mobile.network.response.referral.invite.create.CreateInviteResult;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResult;

import rx.Observable;

public interface ICreateInviteInteractor {

    Observable<CreateInviteResult> createInvite(String price);

    Observable<RemoveInviteResult> removeInvite(int idInvite);
}
