package com.decenturion.mobile.ui.fragment.settings.delivery.pay.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PayDeliverySettingsModelView;

public interface IPayDeliverySettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull CoinModelView model2,
                        @NonNull PayDeliverySettingsModelView model);

    void onBindPaymentViewData(PayDeliverySettingsModelView address);
}
