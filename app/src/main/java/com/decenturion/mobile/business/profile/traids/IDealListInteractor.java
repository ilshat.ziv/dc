package com.decenturion.mobile.business.profile.traids;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealListModelView;

import rx.Observable;

public interface IDealListInteractor {

    Observable<DealListModelView> getDealsInfo(@NonNull DealListModelView model);
}
