package com.decenturion.mobile.di.dagger.pass.edit;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.edit.EditProfileInteractor;
import com.decenturion.mobile.business.edit.IEditProfileInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.edit.presenter.EditProfilePresenter;
import com.decenturion.mobile.ui.fragment.pass.edit.presenter.IEditProfilePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class EditProfileModule {

    @Provides
    @EditProfileScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @EditProfileScope
    IEditProfileInteractor provideIEditProfileInteractor(IResidentClient iResidentClient,
                                                    IResidentRepository iResidentRepository) {
        return new EditProfileInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @EditProfileScope
    IEditProfilePresenter provideIEditProfilePresenter(Context context,
                                                       IEditProfileInteractor iEditProfileInteractor,
                                                       ILocalistionManager iLocalistionManager) {
        return new EditProfilePresenter(context, iEditProfileInteractor, iLocalistionManager);
    }
}
