package com.decenturion.mobile.di.dagger.support;

import com.decenturion.mobile.ui.fragment.support.view.SupportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SupportModule.class
})

@SupportScope
public interface SupportComponent {

    void inject(SupportFragment view);
}
