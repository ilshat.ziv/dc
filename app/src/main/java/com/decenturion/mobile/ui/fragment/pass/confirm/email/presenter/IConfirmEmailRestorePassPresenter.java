package com.decenturion.mobile.ui.fragment.pass.confirm.email.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.view.IConfirmEmailRestorePassView;

public interface IConfirmEmailRestorePassPresenter extends IPresenter<IConfirmEmailRestorePassView> {

    void bindViewData();
}
