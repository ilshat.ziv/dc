package com.decenturion.mobile.ui.fragment.score.details.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.score.details.model.ScoreTokenDetailsModelView;

public interface IScoreTokenDetailsView extends IScreenFragmentView {

    void onBindViewData(@NonNull ScoreTokenDetailsModelView model);
}
