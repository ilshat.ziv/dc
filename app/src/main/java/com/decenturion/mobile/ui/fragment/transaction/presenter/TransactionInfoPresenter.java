package com.decenturion.mobile.ui.fragment.transaction.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.transaction.ITransactionInfoInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.transaction.model.TransactionInfoModelView;
import com.decenturion.mobile.ui.fragment.transaction.view.ITransactionInfoView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TransactionInfoPresenter extends Presenter<ITransactionInfoView> implements ITransactionInfoPresenter {

    private ITransactionInfoInteractor mITransactionInfoInteractor;
    private TransactionInfoModelView mTransactionInfoModelView;

    public TransactionInfoPresenter(ITransactionInfoInteractor iTransactionInfoInteractor) {
        mITransactionInfoInteractor = iTransactionInfoInteractor;
    }

    @Override
    public void bindViewData(@NonNull String transactionDate) {
        showProgressView();

        Observable<TransactionInfoModelView> obs = mITransactionInfoInteractor.getTransactionInfo(transactionDate)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TransactionInfoModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TransactionInfoModelView model) {
        mTransactionInfoModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());
                return;
            }
        }

        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTransactionInfoModelView = null;
    }
}
