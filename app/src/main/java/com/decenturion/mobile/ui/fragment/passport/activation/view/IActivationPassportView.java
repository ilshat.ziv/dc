package com.decenturion.mobile.ui.fragment.passport.activation.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.passport.activation.model.ActivationModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.model.PassportModel;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;

public interface IActivationPassportView extends IScreenFragmentView {

    void onBindViewData(CoinModelView coinModelView, ActivationModelView model);

    void onBindPaymentViewData(@NonNull ActivationModelView address);
}
