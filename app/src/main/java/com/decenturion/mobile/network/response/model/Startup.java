package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Startup {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("alias")
    private String mAlias;

    @JsonProperty("name")
    private String mName;

    @JsonProperty("logo")
    private String mLogo;

    @JsonProperty("description")
    private String mDescription;

    @JsonProperty("url")
    private String mUrl;

    @JsonProperty("whitepaper")
    private String mWhitepaper;

    @JsonProperty("presentation")
    private String mPresentation;

    @JsonProperty("smart_contract")
    private String mSmartContract;

    @JsonProperty("token_symbol")
    private String mTokenSymbol;

    @JsonProperty("token_name")
    private String mTokenName;

    @JsonProperty("token_amount")
    private String mTokenAmount;

    @JsonProperty("token_amount_text")
    private String mTokenAmountText;

    @JsonProperty("youtube_id")
    private String mYoutubeId;

    @JsonProperty("description_title")
    private String mDescriptionTitle;

    @JsonProperty("descriptions")
    private List<String> mDescriptions;

    public Startup() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getAlias() {
        return mAlias;
    }

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public String getLogo() {
        return mLogo;
    }

    @JsonIgnore
    public String getDescription() {
        return mDescription;
    }

    @JsonIgnore
    public String getUrl() {
        return mUrl;
    }

    @JsonIgnore
    public String getWhitepaper() {
        return mWhitepaper;
    }

    @JsonIgnore
    public String getPresentation() {
        return mPresentation;
    }

    @JsonIgnore
    public String getSmartContract() {
        return mSmartContract;
    }

    @JsonIgnore
    public String getTokenSymbol() {
        return mTokenSymbol;
    }

    @JsonIgnore
    public String getTokenName() {
        return mTokenName;
    }

    @JsonIgnore
    public String getTokenAmount() {
        return mTokenAmount;
    }

    @JsonIgnore
    public String getTokenAmountText() {
        return mTokenAmountText;
    }

    @JsonIgnore
    public String getYoutubeId() {
        return mYoutubeId;
    }

    @JsonIgnore
    public String getDescriptionTitle() {
        return mDescriptionTitle;
    }

    @JsonIgnore
    public List<String> getDescriptions() {
        return mDescriptions;
    }

    @JsonIgnore
    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    @JsonIgnore
    public void setAlias(String alias) {
        mAlias = alias;
    }

    @JsonIgnore
    public void setName(String name) {
        mName = name;
    }

    @JsonIgnore
    public void setLogo(String logo) {
        mLogo = logo;
    }

    @JsonIgnore
    public void setDescription(String description) {
        mDescription = description;
    }

    @JsonIgnore
    public void setUrl(String url) {
        mUrl = url;
    }

    @JsonIgnore
    public void setWhitepaper(String whitepaper) {
        mWhitepaper = whitepaper;
    }

    @JsonIgnore
    public void setPresentation(String presentation) {
        mPresentation = presentation;
    }

    @JsonIgnore
    public void setSmartContract(String smartContract) {
        mSmartContract = smartContract;
    }

    @JsonIgnore
    public void setTokenSymbol(String tokenSymbol) {
        mTokenSymbol = tokenSymbol;
    }

    @JsonIgnore
    public void setTokenName(String tokenName) {
        mTokenName = tokenName;
    }

    @JsonIgnore
    public void setTokenAmount(String tokenAmount) {
        mTokenAmount = tokenAmount;
    }

    @JsonIgnore
    public void setTokenAmountText(String tokenAmountText) {
        mTokenAmountText = tokenAmountText;
    }

    @JsonIgnore
    public void setYoutubeId(String youtubeId) {
        mYoutubeId = youtubeId;
    }

    @JsonIgnore
    public void setDescriptionTitle(String descriptionTitle) {
        mDescriptionTitle = descriptionTitle;
    }

    @JsonIgnore
    public void setDescriptions(List<String> descriptions) {
        mDescriptions = descriptions;
    }
}
