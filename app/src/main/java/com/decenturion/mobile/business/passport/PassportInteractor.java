package com.decenturion.mobile.business.passport;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class PassportInteractor implements IPassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public PassportInteractor(IResidentClient IResidentClient,
                              IResidentRepository iResidentRepository,
                              IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<PassportModelView> getProfileInfo() {
        //        Observable<PassportModelView> obs2 = Observable.zip(
//                Observable.just(mIResidentRepository.getResidentInfo()),
//                Observable.just(mIResidentRepository.getPassportInfo()),
//                Observable.just(mIResidentRepository.getPhotoInfo()),
//                PassportModelView::new);

        Observable<PassportModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new PassportModelView(d);
                });

        return obs1
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    OrmResidentModel m1 = mIResidentRepository.getResidentInfo();
                    OrmPassportModel m2 = mIResidentRepository.getPassportInfo();
                    OrmPhotoModel m3 = mIResidentRepository.getPhotoInfo();
                    return new PassportModelView(m1, m2, m3);
                });
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

}
