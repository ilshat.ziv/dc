package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletModel {

    @JsonProperty("category")
    private String mCategory = "trade";

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("code")
    private String mTwoFACode;

    public WalletModel() {
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCategory = category;
    }

    @JsonIgnore
    public void setAddress(String address) {
        mAddress = address;
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoin = coin;
    }

    @JsonIgnore
    public void setTwoFACode(String twoFACode) {
        mTwoFACode = twoFACode;
    }
}
