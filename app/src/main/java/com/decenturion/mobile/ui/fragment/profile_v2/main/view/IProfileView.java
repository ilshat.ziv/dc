package com.decenturion.mobile.ui.fragment.profile_v2.main.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.main.model.ProfileModelView;

public interface IProfileView extends IScreenFragmentView {

    void onShare(@NonNull String url);

    void onSignout();

    void onBindViewData(@NonNull ProfileModelView model);
}
