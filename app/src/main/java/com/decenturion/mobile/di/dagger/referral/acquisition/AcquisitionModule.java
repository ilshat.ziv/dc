package com.decenturion.mobile.di.dagger.referral.acquisition;

import com.decenturion.mobile.business.referral.acquisition.AcquisitionInteractor;
import com.decenturion.mobile.business.referral.acquisition.IAcquisitionInteractor;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.ui.fragment.referral.acquisition.presenter.AcquisitionPresenter;
import com.decenturion.mobile.ui.fragment.referral.acquisition.presenter.IAcquisitionPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AcquisitionModule {

    @Provides
    @AcquisitionScope
    IAcquisitionInteractor provideIAcquisitionInteractor(IReferralClient iReferralClient) {
        return new AcquisitionInteractor(iReferralClient);
    }

    @Provides
    @AcquisitionScope
    IAcquisitionPresenter provideIAcquisitionPresenter(IAcquisitionInteractor iAcquisitionInteractor) {
        return new AcquisitionPresenter(iAcquisitionInteractor);
    }
}
