package com.decenturion.mobile.network.client.token;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.request.ConfirmTraideModel;
import com.decenturion.mobile.network.request.CreateTraideModel;
import com.decenturion.mobile.network.request.DeleteTraidModel;
import com.decenturion.mobile.network.request.EditPriceTokenModel;
import com.decenturion.mobile.network.request.SaleModel;
import com.decenturion.mobile.network.response.EditPriceTokenResponse;
import com.decenturion.mobile.network.response.MinPriceResponse;
import com.decenturion.mobile.network.response.market.MarketTokensResponse;
import com.decenturion.mobile.network.response.trades.TradesResponse;
import com.decenturion.mobile.network.response.traide.cancel.CancelTraideResponse;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResponse;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResponse;
import com.decenturion.mobile.network.response.traide.sale.OnSaleResponse;
import com.decenturion.mobile.network.response.TokensResponse;
import com.decenturion.mobile.network.response.traide.state.StateTraideResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ITokenClient {

    @GET(BuildConfig.API_V + "public/coins/{residentUID}")
    Observable<TokensResponse> getTokens(@Path("residentUID") String residentUID);

    @POST(BuildConfig.API_V + "market/orderbook")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<EditPriceTokenResponse> editPriceToken(@Body EditPriceTokenModel model);

    // market

    @GET(BuildConfig.API_V + "market/coins/{residentUID}")
    Observable<MarketTokensResponse> getBuyTokens(@Path("residentUID") String residentUID);

    // buy

    @POST(BuildConfig.API_V + "market/trade/onsale")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<OnSaleResponse> getTraideParams(@Body SaleModel model);

    @POST(BuildConfig.API_V + "market/trade/create")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CreateTraideResponse> createTraide(@Body CreateTraideModel model);

    @POST(BuildConfig.API_V + "market/trade/cancel")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<CancelTraideResponse> cancelTraide(@Body DeleteTraidModel model);

    @Deprecated
    @POST(BuildConfig.API_V + "market/trade/confirm")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<ConfirmTraideResponse> confirmTraide(@Body ConfirmTraideModel model);

    // TODO: 10.09.2018 не реализовано на сервере
    @GET(BuildConfig.API_V + "market/trade/{tradeUUID}")
    Observable<StateTraideResponse> getTraide(@Path("tradeUUID") String tradeUID);

    /**
     * @param residentUUID
     * @param offset
     * @param limit
     *
     * @return - Список сделок
     */
    @GET(BuildConfig.API_V + "market/buyer/deals")
    Observable<TradesResponse> getDeals(
            @Query("resident_uuid") String residentUUID,
            @Query("offset") long offset,
            @Query("limit") long limit
    );

    /**
     *
     * @param traidUUID
     *
     * @return - информация по сделке
     */
    @GET(BuildConfig.API_V + "market/buyer/deals/{trade_uuid}")
    Observable<TradesResponse> getDealsByTradeUUID(@Path("trade_uuid") String traidUUID);

    @GET(BuildConfig.API_V + "market/orderbook/classic/minprice")
    Observable<MinPriceResponse> getMinPrice();
}
