package com.decenturion.mobile.ui.fragment.token.details.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.database.type.model.StartupTypeModel;
import com.decenturion.mobile.network.response.model.MarketCoinsToken;
import com.decenturion.mobile.network.response.model.Startup;
import com.decenturion.mobile.network.response.model.Token;
import com.decenturion.mobile.network.response.model.TokenCoin;

import java.util.ArrayList;
import java.util.List;

public class TokenDetailsModelView {

    private int mId;
    private String mTokenName;
    private List<TokenParamsModelView> mParamsModelViews;
    private String mSellPrice;
    private String mAmount;
    private String mCoin;
    private boolean isChangePrice = true;
    private String mResidentUUID;
    private String mCoinUUID;
    private String mCategory;

    public TokenDetailsModelView() {
    }

    public TokenDetailsModelView(@NonNull OrmTokenModel model, @NonNull ILocalistionManager iLocalistionManager) {
        mId = model.getId();
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        mCoin = coinTypeModel.getSymbol();
        mCategory = model.getCategory();
        mCoinUUID = coinTypeModel.getUuid();

        StartupTypeModel startupTypeModel = model.getStartupTypeModel();

        initName(model);

        mParamsModelViews = new ArrayList<>();
        String url = startupTypeModel.getUrl();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_website),
                    url,
                    R.drawable.ic_url_gray_24dp
            ));
        }
        url = startupTypeModel.getPresentation();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_presentation),
                    url,
                    R.drawable.ic_presentation_gray_24dp
            ));
        }
        url = startupTypeModel.getWhitepaper();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_whitepaper),
                    url,
                    R.drawable.ic_whitepaper_gray_24dp
            ));
        }
        url = startupTypeModel.getSmartContract();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_smartcontract),
                    url,
                    R.drawable.ic_smartcontract_gray_24dp
            ));
        }

        mSellPrice = model.getSellPrice();
        mAmount = model.getBalance();
        this.isChangePrice = !TextUtils.equals(model.getCategory(), "passport");
    }

    public TokenDetailsModelView(@NonNull MarketCoinsToken token, @NonNull ILocalistionManager iLocalistionManager) {
        TokenCoin coinTypeModel = token.getTokenCoin();
        mCoin = coinTypeModel.getSymbol();
        mCoinUUID = coinTypeModel.getUuid();

        Startup startupTypeModel = token.getStartup();

        mParamsModelViews = new ArrayList<>();
        String url = startupTypeModel.getUrl();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_website),
                    url,
                    R.drawable.ic_url_gray_24dp
            ));
        }
        url = startupTypeModel.getPresentation();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_presentation),
                    url,
                    R.drawable.ic_presentation_gray_24dp
            ));
        }
        url = startupTypeModel.getWhitepaper();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_whitepaper),
                    url,
                    R.drawable.ic_whitepaper_gray_24dp
            ));
        }
        url = startupTypeModel.getSmartContract();
        if (!TextUtils.isEmpty(url)) {
            mParamsModelViews.add(new TokenParamsModelView(
                    iLocalistionManager.getLocaleString(R.string.app_account_tokens_smartcontract),
                    url,
                    R.drawable.ic_smartcontract_gray_24dp
            ));
        }

        mSellPrice = token.getAskPrice();
        mAmount = token.getAskPrice();

        initName(token);
    }

    private void initName(MarketCoinsToken token) {
        TokenCoin coinTypeModel = token.getTokenCoin();
        if (TextUtils.equals(coinTypeModel.getName(), "DECENTURION")) {
            String category = token.getCategory();
            switch (category) {
                case "passport" : {
                    mTokenName = "DCNT Passport";
                    isChangePrice = false;
                    return;
                }
                case "internal" : {
                    mTokenName = "DCNT Classic";
                    return;
                }
                case "external" : {
                    mTokenName = "DCNT Liquid";
                    return;
                }
            }
        }

        mTokenName = coinTypeModel.getName();
    }

    private void initName(OrmTokenModel model) {
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        if (TextUtils.equals(coinTypeModel.getName(), "DECENTURION")) {
            String category = model.getCategory();
            switch (category) {
                case "passport" : {
                    mTokenName = "DCNT Passport";
                    isChangePrice = false;
                    return;
                }
                case "internal" : {
                    mTokenName = "DCNT Classic";
                    return;
                }
                case "external" : {
                    mTokenName = "DCNT Liquid";
                    return;
                }
            }
        }

        mTokenName = coinTypeModel.getName();
    }

    public TokenDetailsModelView(int tokenId) {
        mId = tokenId;
    }

    public int getId() {
        return mId;
    }

    public String getTokenName() {
        return mTokenName;
    }

    public List<TokenParamsModelView> getParamsModelViews() {
        return mParamsModelViews;
    }

    public String getSellPrice() {
        return mSellPrice;
    }

    public String getAmount() {
        return mAmount;
    }

    public boolean isAccept() {
        return false;
    }

    public boolean isChangePrice() {
        return this.isChangePrice;
    }

    public String getCoin() {
        return mCoin;
    }

    public void setResidentUUID(String residentUUID) {
        mResidentUUID = residentUUID;
    }

    public void setCoinUUID(String coinUUID) {
        mCoinUUID = coinUUID;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getResidentUUID() {
        return mResidentUUID;
    }

    public String getCoinUUID() {
        return mCoinUUID;
    }

    public String getCategory() {
        return mCategory;
    }
}
