package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferGas {

    @JsonProperty("fee")
    private String mGasValue;

    @JsonProperty("currency")
    private String mCurrency;

    public TransferGas() {
    }

    @JsonIgnore
    public String getGasValue() {
        return mGasValue;
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }
}
