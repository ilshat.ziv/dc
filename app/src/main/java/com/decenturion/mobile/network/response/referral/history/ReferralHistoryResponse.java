package com.decenturion.mobile.network.response.referral.history;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralHistoryResponse extends BaseResponse {

    @JsonProperty("result")
    private ReferralHistoryResult mResult;

    public ReferralHistoryResponse() {
    }

    @JsonIgnore
    public ReferralHistoryResult getResult() {
        return mResult;
    }
}
