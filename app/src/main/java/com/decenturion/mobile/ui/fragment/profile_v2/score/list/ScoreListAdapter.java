package com.decenturion.mobile.ui.fragment.profile_v2.score.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoreListAdapter extends RecyclerView.Adapter<ScoreListAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<ScoreModelView> mScoreModelList;

    private OnItemClickListener mOnItemClickListener;

    private ScoreListAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public ScoreListAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<ScoreModelView> tokenModelList) {
        mScoreModelList = tokenModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mScoreModelList);
    }

    @NonNull
    @Override
    public ScoreListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_score_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreListAdapter.ViewHolder holder, int position) {
        final ScoreModelView startup = mScoreModelList.get(position);
        holder.bind(startup);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.iconView)
        ImageView iconView;

        @Nullable
        @BindView(R.id.nameScoreView)
        TextView valueView;

        public View view;
        public ScoreModelView score;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(score);
                }
            });
        }

        public void bind(@NonNull ScoreModelView score) {
            this.score = score;

            assert this.iconView != null;
            this.iconView.setImageResource(score.getIcon());

            assert this.valueView != null;
            this.valueView.setText(score.getName());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull ScoreModelView model);
    }
}
