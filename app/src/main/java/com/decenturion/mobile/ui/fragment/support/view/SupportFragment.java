package com.decenturion.mobile.ui.fragment.support.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.support.SupportComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.MultiEditTextView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.support.presenter.ISupportPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SupportFragment extends BaseFragment implements ISupportView,
        View.OnClickListener,
        ChangeComponentListener {

    /* UI */

    @BindView(R.id.themeView)
    EditTextView mThemeView;

    @BindView(R.id.messageView)
    MultiEditTextView mMessageView;

    @BindView(R.id.sendButtonView)
    LocalizedButtonView mSendButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ISupportPresenter mISupportPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SupportComponent component = appDelegate
                .getIDIManager()
                .plusSupportComponent();
        component.inject(this);

        mISupportPresenter.bindView(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_support, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        initToolbar();

        mThemeView.showLabel();
        mThemeView.setLabel(mILocalistionManager.getLocaleString(R.string.app_support_theme_title));
        mThemeView.setHint(mILocalistionManager.getLocaleString(R.string.app_support_theme_placeholder));
        mThemeView.editMode();
        mThemeView.setVisibleDivider(true);
        mThemeView.setChangeDataModelListener(this);

        mMessageView.showLabel();
        mMessageView.setLabel(mILocalistionManager.getLocaleString(R.string.app_support_message_title));
        mMessageView.setHint(mILocalistionManager.getLocaleString(R.string.app_support_message_placeholder));
        mMessageView.editMode();
//        mMessageView.setVisibleDivider(true);
        mMessageView.setChangeDataModelListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_support_title));
        iToolbarController.setSubTitle("");

        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.GONE);

        FragmentManager supportFragmentManager = getFragmentManager();
        Fragment f = supportFragmentManager.findFragmentByTag("ProfileCollapsAppBarFragment");
        if (f != null) {
            FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
            fragmentTransaction.remove(f);
            fragmentTransaction.commit();
        }
    }

    @OnClick(R.id.sendButtonView)
    protected void onSendMessage() {
        String value = mThemeView.getValue();
        String value1 = mMessageView.getValue();
        if (TextUtils.isEmpty(value) || TextUtils.isEmpty(value1)) {
            showErrorMessage(mILocalistionManager.getLocaleString(R.string.app_alert_fillallthefields));
        } else {
            mISupportPresenter.sendMessage(value, value1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mISupportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mISupportPresenter.unbindView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onSendMessageSuccess() {
        showSuccessMessage(mILocalistionManager.getLocaleString(R.string.app_support_send_success));
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onChangeComponentData(View view) {
//        if (mThemeView != null && mMessageView != null
//                && !TextUtils.isEmpty(mThemeView.getValue()) && !TextUtils.isEmpty(mMessageView.getValue())) {
//            mSendButtonView.setEnabled(true);
//        } else {
//            mSendButtonView.setEnabled(false);
//        }
    }
}