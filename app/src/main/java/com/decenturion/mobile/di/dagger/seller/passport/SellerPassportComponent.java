package com.decenturion.mobile.di.dagger.seller.passport;

import com.decenturion.mobile.ui.fragment.seller.passport.view.SellerPassportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SellerPassportModule.class
})

@SellerPassportScope
public interface SellerPassportComponent {

    void inject(SellerPassportFragment view);
}
