package com.decenturion.mobile.di.dagger.tokendetails.edit;

import com.decenturion.mobile.ui.fragment.token.edit.view.EditPriceTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        EditPriceTokenModule.class
})

@EditPriceTokenScope
public interface EditPriceTokenComponent {

    void inject(EditPriceTokenFragment view);
}
