package com.decenturion.mobile.ui.fragment.settings.action;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModel;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogsAdapter extends RecyclerView.Adapter<LogsAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<LogModel> mActionModelList;

    private OnItemClickListener mOnItemClickListener;

    public LogsAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public LogsAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<LogModel> traideModelList) {
        mActionModelList = traideModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mActionModelList);
    }

    @NonNull
    @Override
    public LogsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_action_log_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogsAdapter.ViewHolder holder, int position) {
        final LogModel model = mActionModelList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        @BindView(R.id.labelView)
        TextView labelView;

        @BindView(R.id.valueView)
        TextView valueView;

        @BindView(R.id.stateView)
        TextView stateView;

        @BindView(R.id.actionView)
        TextView actionView;

        LogModel model;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public void bind(@NonNull LogModel param) {
            this.model = param;

            this.labelView.setText("IP " + model.getIp());
            this.valueView.setText(model.getDate());
            this.stateView.setText(model.getBrowser());
            this.actionView.setText(model.getAction());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(LogModel model);
    }
}
