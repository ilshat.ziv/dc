package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralSettings {

    @JsonProperty("costing_method")
    private String mCostingMethod;

    @JsonProperty("fixed_cost")
    private String mFixedCost;

    @JsonProperty("fixed_token_count")
    private String mFixedTokenCount;

    @JsonProperty("fixed_comment")
    private String mFixedComment;

    @JsonProperty("percent")
    private int mPercent;

    @JsonProperty("percent_token_count")
    private String mPercentTokenCount;

    @JsonProperty("percent_comment")
    private String mPercentComment;

    public ReferralSettings() {
    }

    @JsonIgnore
    public void setCostingMethod(String costingMethod) {
        mCostingMethod = costingMethod;
    }

    @JsonIgnore
    public void setFixedCost(String fixedCost) {
        mFixedCost = fixedCost;
    }

    @JsonIgnore
    public void setFixedTokenCount(String fixedTokenCount) {
        mFixedTokenCount = fixedTokenCount;
    }

    @JsonIgnore
    public void setFixedComment(String fixedComment) {
        mFixedComment = fixedComment;
    }

    @JsonIgnore
    public void setPercent(int percent) {
        mPercent = percent;
    }

    @JsonIgnore
    public void setPercentTokenCount(String percentTokenCount) {
        mPercentTokenCount = percentTokenCount;
    }

    @JsonIgnore
    public void setPercentComment(String percentComment) {
        mPercentComment = percentComment;
    }
}
