package com.decenturion.mobile.di.dagger.token.details;

import com.decenturion.mobile.ui.fragment.token.seller.details.view.SellerTokenDetailsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SellerTokenDetailsModule.class
})

@SellerTokenDetailsScope
public interface SellerTokenDetailsComponent {

    void inject(SellerTokenDetailsFragment view);
}
