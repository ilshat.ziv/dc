package com.decenturion.mobile.business.profile.main;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.KillAllSessionsResponse;
import com.decenturion.mobile.network.response.actions.ActionsResponse;
import com.decenturion.mobile.network.response.actions.ActionsResult;
import com.decenturion.mobile.network.response.model.Action;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModel;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.List;

import rx.Observable;

public class LogsInteractor implements ILogsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    public LogsInteractor(IResidentClient IResidentClient,
                          IResidentRepository iResidentRepository,
                          IPrefsManager iPrefsManager,
                          ILocalistionManager iLocalistionManager) {

        mIPrefsManager = iPrefsManager;
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;

        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<LogModelView> getLogsInfo() {
        return mIResidentClient.getActions(0, 99999)
                .map(ActionsResponse::getResult)
                .map(ActionsResult::getActionList)
                .flatMapIterable(d -> d)
                .map(d -> new LogModel(d, mILocalistionManager))
                .toList()
                .map(LogModelView::new);
    }

    @Override
    public Observable<LogModelView> getLogsInfo(@NonNull LogModelView model) {
        Observable<LogModelView> obs1 = mIResidentClient.getActions(model.getOffset(), model.LIMIT)
                .map(ActionsResponse::getResult)
                .flatMap(d -> {
                    List<Action> list = d.getActionList();
                    Observable<List<LogModel>> listObservable = Observable.just(list)
                            .flatMapIterable(list1 -> list1)
                            .map(d1 -> new LogModel(d1, mILocalistionManager))
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(d.getTotal()),
                            LogModelView::new
                    );
                })
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    throw new RuntimeException(throwable.getMessage());
                });

        Observable<LogModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (modelView, modelView2) -> {
            modelView2.setOffset(modelView2.getOffset() + modelView2.LIMIT);
            modelView2.addActions(modelView.getActionList());
            modelView2.setTotal(modelView.getTotal());
            return modelView2;
        });
    }

    @Override
    public Observable<KillAllSessionsResponse> killAllSessions() {
        return mIResidentClient.killAllSessions()
                .doOnNext(d -> clearDate())
                .doOnError(d -> clearDate());
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
