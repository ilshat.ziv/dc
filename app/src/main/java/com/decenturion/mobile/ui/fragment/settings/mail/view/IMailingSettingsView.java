package com.decenturion.mobile.ui.fragment.settings.mail.view;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.mail.model.MailingSettingsModelView;

public interface IMailingSettingsView extends IScreenFragmentView {

    void onBindViewData(MailingSettingsModelView model);
}
