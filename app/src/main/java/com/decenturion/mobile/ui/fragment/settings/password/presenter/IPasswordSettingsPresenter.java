package com.decenturion.mobile.ui.fragment.settings.password.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.password.view.IPasswordSettingsView;

public interface IPasswordSettingsPresenter extends IPresenter<IPasswordSettingsView> {

    void bindViewData();

    void saveNewPassword(@NonNull String currentPassword,
                         @NonNull String newPassword,
                         @NonNull String retypePassword);
}
