package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckDcntWalletModel {

    @JsonProperty("recipient")
    private String mDCNTWallet;

    public CheckDcntWalletModel() {
    }

    public CheckDcntWalletModel(String dcntWallet) {
        mDCNTWallet = dcntWallet;
    }
}
