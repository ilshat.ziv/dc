package com.decenturion.mobile.ui.fragment.profile_v2.log.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.model.Action;
import com.decenturion.mobile.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

public class LogModel {

    private String mAction;
    private String mBrowser;
    private String mDate;
    private String mIp;

    public LogModel(@NonNull Action d, ILocalistionManager iLocalistionManager) {
        mAction = d.getAction();
        mBrowser = d.getBrowser();

        String createdAt = d.getDate();
        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            String s = iLocalistionManager.getLocaleString(R.string.app_constants_today);
            mDate = s + " " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }

        mIp = d.getIp();
    }

    public String getAction() {
        return mAction;
    }

    public String getBrowser() {
        return mBrowser;
    }

    public String getDate() {
        return mDate;
    }

    public String getIp() {
        return mIp;
    }
}
