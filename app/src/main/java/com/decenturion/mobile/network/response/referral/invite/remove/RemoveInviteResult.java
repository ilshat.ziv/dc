package com.decenturion.mobile.network.response.referral.invite.remove;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoveInviteResult {

    @JsonProperty("id")
    private int mId;

    public RemoveInviteResult() {
    }

    @JsonIgnore
    public int getId() {
        return mId;
    }

}
