package com.decenturion.mobile.ui.fragment.profile.passport.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.network.response.model.Photo;

public class PhotoModelView{

    private String mOriginal;
    private String mMin;

    public PhotoModelView() {
    }

    public PhotoModelView(@NonNull OrmPhotoModel model) {
        mOriginal = model.getOriginal();
        mMin = model.getMin();
    }

    public PhotoModelView(Photo photo) {
        mOriginal = photo.getOriginal();
        mMin = photo.getMin();
    }

    public PhotoModelView(String photo) {
        mOriginal = photo;
        mMin = photo;
    }

    public String getOriginal() {
        return mOriginal;
    }

    public String getMin() {
        return mMin;
    }
}
