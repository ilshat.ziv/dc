package com.decenturion.mobile.di.dagger.score.express;

import com.decenturion.mobile.ui.fragment.score.send.express.view.ExpressSendTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ExpressSendTokenModule.class
})

@ExpressSendTokenScope
public interface ExpressSendTokenComponent {

    void inject(ExpressSendTokenFragment view);
}
