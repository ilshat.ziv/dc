package com.decenturion.mobile.di.dagger.traid;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.token.ITokenInteractor;
import com.decenturion.mobile.business.token.TokenInteractor;
import com.decenturion.mobile.business.traid.ITraidInfoInteractor;
import com.decenturion.mobile.business.traid.TraidInfoInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.traide.TraideRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.token.presenter.ITokenPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.presenter.TokenPresenter;
import com.decenturion.mobile.ui.fragment.traide.presenter.ITraidInfoPresenter;
import com.decenturion.mobile.ui.fragment.traide.presenter.TraidInfoPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TraidInfoModule {

    @Provides
    @TraidInfoScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TraidInfoScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @TraidInfoScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TraidInfoScope
    ITraideRepository provideITraideRepository(OrmHelper ormHelper) {
        return new TraideRepository(ormHelper);
    }

    @Provides
    @TraidInfoScope
    ITraidInfoInteractor provideITraidInfoInteractor(IResidentClient IResidentClient,
                                                 ITokenClient iTokenClient,
                                                 IResidentRepository iResidentRepository,
                                                 IWalletRepository iWalletRepository,
                                                 ITokenRepository iTokenRepository,
                                                     ITraideRepository iTraideRepository) {

        return new TraidInfoInteractor(
                IResidentClient,
                iTokenClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository,
                iTraideRepository
        );
    }

    @Provides
    @TraidInfoScope
    ITraidInfoPresenter provideITraidInfoPresenter(ITraidInfoInteractor iTraidInfoInteractor) {
        return new TraidInfoPresenter(iTraidInfoInteractor);
    }
}
