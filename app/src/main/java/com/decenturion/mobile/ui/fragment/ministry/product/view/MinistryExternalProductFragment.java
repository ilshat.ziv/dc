package com.decenturion.mobile.ui.fragment.ministry.product.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.ministry.product.MinistryProductComponent;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.dialog.ministry.DcntLiquidFeaturesDialog;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.ministry.info.MinistrySenatorInfoFragment;
import com.decenturion.mobile.ui.fragment.ministry.product.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.MinistryProductModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.presenter.IMinistryProductPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.LinkTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

public class MinistryExternalProductFragment extends BaseFragment implements IMinistryProductView,
        View.OnClickListener,
        SpinnerView_v2.OnSpinnerItemClickListener,
        LinkTextView.OnLinkTextViewListener,
        ChangeComponentListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    public static final String CATEGORY = "CATEGORY";

    private String mCategory;

    /* UI */

    @BindView(R.id.productFeaturesView)
    LinkTextView mProductFeaturesView;

    @BindView(R.id.priceProductView)
    TextView mPriceProductView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.summPayView)
    BuyTokenView mSummPayView;

    @BindView(R.id.priceOneView)
    TextView mPriceOneView;

    @BindView(R.id.amountPayView)
    TextView mAmountPayView;

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IMinistryProductPresenter mIMinistryProductPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mCategory = arguments.getString(CATEGORY);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        MinistryProductComponent component = appDelegate
                .getIDIManager()
                .plusMinistryProductComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_ministry_liquid_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSummPayView.showLabel();
        mSummPayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_ministry_product_payment_amount_title));
        mSummPayView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mSummPayView.setCoin("DCNT Liquid");
        mSummPayView.setText("1");
        mSummPayView.simpleMode();
        mSummPayView.setVisibleDivider(true);
        mSummPayView.setChangeDataModelListener(this);

        String localeString = mILocalistionManager.getLocaleString(R.string.app_ministry_product_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();
        mSelectCoinView.setOnSpinnerItemClickListener(this);

        mProductFeaturesView.setOnLinkClickListener(this);

        mQrCodeViewComponent.hideLabel();
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle("DCNT Liquid");
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIMinistryProductPresenter.bindView(this);
        mIMinistryProductPresenter.bindViewData(mCategory);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIMinistryProductPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIMinistryProductPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model1, @NonNull MinistryProductModelView model) {
        String summ = mILocalistionManager.getLocaleString(R.string.app_ministry_product_token_price);
        summ = summ.replace("{usdt_price}", model.getUsdtPrice());
        mPriceProductView.setText(summ);

        ArrayList<Coin> coinData = model1.getCoinData();
        Coin coin = coinData.get(0);
        mSelectCoinView.setData(coinData);
        mSelectCoinView.selectItem(coin.getItemKey());
    }

    @Override
    public void onBindPaymentViewData(@NonNull MinistryProductModelView modelView) {
        onBindPayModelView(modelView);
    }

    private void onBindPayModelView(MinistryProductModelView model) {
        PaymentModelView paymentModelView = model.getPaymentModelView();

        String string = mILocalistionManager.getLocaleString(R.string.app_ministry_product_payment_amount_value);

        String payForOne = string.replace("{amount}", paymentModelView.getPriceOne());
        payForOne = payForOne.replace("{currency}", paymentModelView.getTypePay());
        mPriceOneView.setText(payForOne);

        String summPay = string.replace("{amount}", paymentModelView.getCoinsPay());
        summPay = summPay.replace("{currency}", paymentModelView.getTypePay());
        mAmountPayView.setText(summPay);

        mQrCodeViewComponent.setAddress(paymentModelView.getAddress());
        mQrCodeViewComponent.setQrData(paymentModelView.getQrCode());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mIMinistryProductPresenter.getPaymentAddress(mCategory, mSelectCoinView.getSelection(), mSummPayView.getValue());
    }

    @Override
    public void onLinkClick() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(DcntLiquidFeaturesDialog.class);
    }

    @Override
    public void onTextClick() {

    }

    @Override
    public void onChangeComponentData(View view) {
        mIMinistryProductPresenter.getPaymentAddress(mCategory, mSelectCoinView.getSelection(), mSummPayView.getValue());
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
