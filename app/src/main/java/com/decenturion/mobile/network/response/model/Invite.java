package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Invite {

    @JsonProperty("url")
    private String mUrl;

    @JsonProperty("id")
    private int mId;

    @JsonProperty("price")
    private String mPrice;

    @JsonProperty("created_at")
    private String mCreatedAt;

    public Invite() {
    }

    @JsonIgnore
    public String getUrl() {
        return mUrl;
    }

    @JsonIgnore
    public int getId() {
        return mId;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }
}
