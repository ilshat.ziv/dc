package com.decenturion.mobile.network.response.referral.invite;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralInviteResponse extends BaseResponse {

    @JsonProperty("result")
    private ReferralInviteResult mResult;

    public ReferralInviteResponse() {
    }

    @JsonIgnore
    public ReferralInviteResult getResult() {
        return mResult;
    }
}
