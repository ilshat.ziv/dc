package com.decenturion.mobile.di.dagger.send;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.send.ISendTokenInteractor;
import com.decenturion.mobile.business.send.SendTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.token.send.presenter.ISendTokenPresenter;
import com.decenturion.mobile.ui.fragment.token.send.presenter.SendTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SendTokenModule {

    @Provides
    @SendTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @SendTokenScope
    ISendTokenInteractor provideISendTokenInteractor(IResidentClient iResidentClient,
                                                     ITokenRepository iTokenRepository) {
        return new SendTokenInteractor(iResidentClient, iTokenRepository);
    }

    @Provides
    @SendTokenScope
    ISendTokenPresenter provideISendTokenPresenter(ISendTokenInteractor iSendTokenInteractor,
                                                   IPrefsManager iPrefsManager,
                                                   ILocalistionManager iLocalistionManager) {
        return new SendTokenPresenter(iSendTokenInteractor, iPrefsManager, iLocalistionManager);
    }
}
