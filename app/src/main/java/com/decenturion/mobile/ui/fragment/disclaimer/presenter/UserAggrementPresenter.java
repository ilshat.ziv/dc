package com.decenturion.mobile.ui.fragment.disclaimer.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.aggrement.IUserAggrementInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.disclaimer.model.UserAggrementModelView;
import com.decenturion.mobile.ui.fragment.disclaimer.view.IUserAggrementView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class UserAggrementPresenter extends Presenter<IUserAggrementView> implements IUserAggrementPresenter {

    private IUserAggrementInteractor mIUserAggrementInteractor;

    private UserAggrementModelView mUserAggrementModelView;

    public UserAggrementPresenter(IUserAggrementInteractor iUserAggrementInteractor) {
        mIUserAggrementInteractor = iUserAggrementInteractor;

        mUserAggrementModelView = new UserAggrementModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        bundle.putString("terms", mUserAggrementModelView.getTerms());
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        String terms = savedInstanceState.getString("username", "");
        mUserAggrementModelView = new UserAggrementModelView(terms);

        Observable<UserAggrementModelView> obs = Observable.just(mUserAggrementModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UserAggrementModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<UserAggrementModelView> obs = mIUserAggrementInteractor.getTermsInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UserAggrementModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(UserAggrementModelView model) {
        mUserAggrementModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
