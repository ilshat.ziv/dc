package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WrapperTrade {

    @JsonProperty("trade")
    private Trade mTrade;

    @JsonProperty("transfer")
    private Transfer mTransfer;

    public WrapperTrade() {}

    @JsonIgnore
    public Trade getTrade() {
        return mTrade;
    }

    @JsonIgnore
    public Transfer getTransfer() {
        return mTransfer;
    }
}
