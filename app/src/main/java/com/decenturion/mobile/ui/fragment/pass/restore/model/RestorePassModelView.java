package com.decenturion.mobile.ui.fragment.pass.restore.model;

public class RestorePassModelView {

    private String mContact;

    public RestorePassModelView(String contact) {
        mContact = contact;
    }

    public String getContact() {
        return mContact;
    }
}
