package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.Settings;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_settings_model")
public class OrmSettingsModel {

    private final static String ID = "id";
    private final static String SHOW_NAME = "show_name";
    private final static String SHOW_DOB = "show_dob";
    private final static String SHOW_PHOTO = "show_photo";
    private final static String SHOW_TOKENS = "show_tokens";
    private final static String SHOW_MONEY = "show_money";
    private final static String SHOW_POWER = "show_power";
    private final static String SHOW_GLORY = "show_glory";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_NAME)
    private boolean isShowName;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_DOB)
    private boolean isShowBirth;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_PHOTO)
    private boolean isShowPhoto;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_TOKENS)
    private boolean isShowTokens;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_MONEY)
    private boolean isShowMoney;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_POWER)
    private boolean isShowPower;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SHOW_GLORY)
    private boolean isShowGlory;

    public OrmSettingsModel() {
    }

    public OrmSettingsModel(@NonNull Settings settings) {
        mId = 1;
        this.isShowName = settings.isShowName();
        this.isShowBirth = settings.isShowBirth();
        this.isShowPhoto = settings.isShowPhoto();
        this.isShowTokens = settings.isShowTokens();
        this.isShowMoney = settings.isShowMoney();
        this.isShowPower = settings.isShowPower();
        this.isShowGlory = settings.isShowGlory();
    }

    public boolean isShowName() {
        return this.isShowName;
    }

    public boolean isShowBirth() {
        return this.isShowBirth;
    }

    public boolean isShowPhoto() {
        return this.isShowPhoto;
    }

    public boolean isShowTokens() {
        return this.isShowTokens;
    }

    public boolean isShowMoney() {
        return this.isShowMoney;
    }

    public boolean isShowPower() {
        return this.isShowPower;
    }

    public boolean isShowGlory() {
        return this.isShowGlory;
    }

    public void setShowName(boolean showName) {
        isShowName = showName;
    }

    public void setShowBirth(boolean showBirth) {
        isShowBirth = showBirth;
    }

    public void setShowPhoto(boolean showPhoto) {
        isShowPhoto = showPhoto;
    }

    public void setShowTokens(boolean showTokens) {
        isShowTokens = showTokens;
    }

    public void setShowMoney(boolean showMoney) {
        isShowMoney = showMoney;
    }

    public void setShowPower(boolean showPower) {
        isShowPower = showPower;
    }

    public void setShowGlory(boolean showGlory) {
        isShowGlory = showGlory;
    }
}
