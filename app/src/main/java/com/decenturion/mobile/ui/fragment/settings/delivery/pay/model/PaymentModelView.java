package com.decenturion.mobile.ui.fragment.settings.delivery.pay.model;

import android.text.TextUtils;

import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;

public class PaymentModelView {

    private String mTypePay;
    private String mCoinsPay;
    private String mCyrrencePay;
    private String mQrCode;
    private String mAddress;

    public PaymentModelView() {
    }

    public PaymentModelView(DeliverPaymentResult d) {
        String coin1 = d.getCoin();
        if (TextUtils.equals(coin1, "btc")) {
            String s = "bitcoin:{address}?amount={amount}";
            mQrCode = s.replace("{address}", d.getAddress())
                    .replace("{amount}", String.valueOf(d.getAmount()));
        } else {
            mQrCode = d.getAddress();
        }

        mAddress = d.getAddress();
        mTypePay = d.getCoin();
        mCoinsPay = String.valueOf(d.getAmount());
        mCyrrencePay = "50 $";
    }

    public String getTypePay() {
        return mTypePay;
    }

    public String getCoinsPay() {
        return mCoinsPay;
    }

    public String getCyrrencePay() {
        return mCyrrencePay;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getQrCode() {
        return mQrCode;
    }

}
