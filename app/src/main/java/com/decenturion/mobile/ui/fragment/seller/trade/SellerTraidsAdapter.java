package com.decenturion.mobile.ui.fragment.seller.trade;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModel;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SellerTraidsAdapter extends RecyclerView.Adapter<SellerTraidsAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<SellerTraideModel> mTraideModelList;

    private OnItemClickListener mOnItemClickListener;

    private SellerTraidsAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public SellerTraidsAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<SellerTraideModel> traideModelList) {
        mTraideModelList = traideModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTraideModelList);
    }

    @NonNull
    @Override
    public SellerTraidsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_traid_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SellerTraidsAdapter.ViewHolder holder, int position) {
        final SellerTraideModel model = mTraideModelList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dateTraidView)
        TextView mDateTraidView;

        @BindView(R.id.idTraidView)
        TextView mIdTraidView;

        @BindView(R.id.amountTraidView)
        TextView mAmountTraidView;

        @BindView(R.id.iconStateTraidView)
        ImageView mIconStateTraidView;

        public final View view;
        public SellerTraideModel startup;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void bind(@NonNull SellerTraideModel model) {
            this.startup = model;

            String traidId = model.getTraidId();
            String t = TextUtils.substring(traidId, 0, 6);
            String t1 = TextUtils.substring(traidId, traidId.length() - 5, traidId.length());
            this.mIdTraidView.setText(t + "..." + t1);
            this.mDateTraidView.setText(model.getTraidDate());
            this.mAmountTraidView.setText(model.getTraidAmount() + " " + model.getCoin());

            String traidStatus = model.getTraidStatus();
            switch (traidStatus) {
                case "success" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_succes);
                    this.mAmountTraidView.setTextColor(Color.GREEN);
                    break;
                }
                case "fail" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_fail);
                    this.mAmountTraidView.setTextColor(Color.RED);
                    break;
                }
                case "fail-success" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traide_state_fail_success);
                    this.mAmountTraidView.setTextColor(Color.GREEN);
                    break;
                }
                case "done" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_done);
                    this.mAmountTraidView.setTextColor(Color.WHITE);
                    break;
                }
            }
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(this.startup);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SellerTraideModel model);
    }
}
