package com.decenturion.mobile.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.R;

import butterknife.ButterKnife;

public class TransactionFailFragmentDialog extends SingleDialogFragment {

    public static String TITLE = "TITLE";

    private String mTitle = "DEAL #";

    /* UI */


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString(TITLE, mTitle);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mTitle);
        setPositiveText(R.string.fr_dialog_transaction_fail_positive);
        setNegativeText(R.string.fr_dialog_transaction_fail_nagative);

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_transaction_fail);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        setupUi();

        return arbitrationRequestDialog;
    }

    private void setupUi() {
    }

    @Override
    public void onPositiveClickButton() {
        FragmentActivity activity = getActivity();
        assert activity != null;
        activity.finish();
    }

    @Override
    public void onNegativeClickButton() {
        dismiss();
    }
}
