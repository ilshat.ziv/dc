package com.decenturion.mobile.ui.fragment.token.info.v1.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.database.model.OrmTokenModel;

import java.util.ArrayList;
import java.util.List;

public class TokenInfoModelView {

    private int mTokenId;
    private String mCategory;
    private List<InfoModelView> mInfoModelViews;

    public TokenInfoModelView() {
    }

    public TokenInfoModelView(int tokenId) {
        mTokenId = tokenId;
    }

    public TokenInfoModelView(String category) {
        mCategory = category;
        initParams(mCategory);
    }

    public TokenInfoModelView(@NonNull OrmTokenModel model) {
        mTokenId = model.getId();
        mCategory = model.getCategory();
        initParams(mCategory);
    }

    private void initParams(String category) {
        mInfoModelViews = new ArrayList<>();

        boolean passport = TextUtils.equals(category, "passport");
        InfoModelView infoModelView = new InfoModelView("DCNT Passport", passport);
        List<InfoParamsModelView> list = new ArrayList<>();
        list.add(new InfoParamsModelView("Citizenship guarantee"));
        list.add(new InfoParamsModelView("Share in the startups token distribution"));
        list.add(new InfoParamsModelView("Right to participate in every voting"));
        list.add(new InfoParamsModelView("Lawmaking activity guarantee"));
        infoModelView.setInfoParamsModelViews(list);
        mInfoModelViews.add(infoModelView);

        boolean internal = TextUtils.equals(category, "internal");
        infoModelView = new InfoModelView("DCNT Classic", internal);
        list = new ArrayList<>();
        list.add(new InfoParamsModelView("Citizenship guarantee"));
        list.add(new InfoParamsModelView("Possibility to transfer to other users"));
        list.add(new InfoParamsModelView("Possibility to use as an invite"));
        list.add(new InfoParamsModelView("Share in the startups token distribution"));
        list.add(new InfoParamsModelView("Right to participate in every voting"));
        list.add(new InfoParamsModelView("Lawmaking activity guarantee"));
        infoModelView.setInfoParamsModelViews(list);
        mInfoModelViews.add(infoModelView);

        boolean external = TextUtils.equals(category, "external");
        infoModelView = new InfoModelView("DCNT Liquid", external);
        list = new ArrayList<>();
        list.add(new InfoParamsModelView("Citizenship guarantee"));
        list.add(new InfoParamsModelView("Possibility to transfer to other users"));
        list.add(new InfoParamsModelView("Possibility to use as an invite"));
        list.add(new InfoParamsModelView("Share in the startups token distribution"));
        list.add(new InfoParamsModelView("Right to participate in every voting"));
        list.add(new InfoParamsModelView("Lawmaking activity guarantee"));
        list.add(new InfoParamsModelView("Progressive model of multiplicators"));
        list.add(new InfoParamsModelView("TransferTypeModel out to any wallets and exchanges"));
        infoModelView.setInfoParamsModelViews(list);
        mInfoModelViews.add(infoModelView);
    }

    public List<InfoModelView> getInfoModelViews() {
        return mInfoModelViews;
    }

    public void setInfoModelViews(List<InfoModelView> infoModelViews) {
        mInfoModelViews = infoModelViews;
    }

    public void addInfoModelView(InfoModelView infoModelView) {
        if (mInfoModelViews == null) {
            mInfoModelViews = new ArrayList<>();
        }

        mInfoModelViews.add(infoModelView);
    }
}
