package com.decenturion.mobile.di.dagger.signin;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.signin.ISigninInteractor;
import com.decenturion.mobile.business.signin.SigninInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.sign.in.presenter.ISigninPresenter;
import com.decenturion.mobile.ui.fragment.sign.in.presenter.SigninPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SigninModule {

    @Provides
    @SigninScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SigninScope
    ISigninInteractor provideISigninInteractor(IResidentClient iResidentClient,
                                               IDeviceClient iDeviceClient,
                                               IResidentRepository iResidentRepository,
                                               IPrefsManager iPrefsManager) {
        return new SigninInteractor(
                iResidentClient,
                iDeviceClient,
                iResidentRepository,
                iPrefsManager);
    }

    @Provides
    @SigninScope
    ISigninPresenter provideISigninPresenter(ISigninInteractor iSigninInteractor,
                                             IPrefsManager iPrefsManager) {
        return new SigninPresenter(iSigninInteractor, iPrefsManager);
    }
}
