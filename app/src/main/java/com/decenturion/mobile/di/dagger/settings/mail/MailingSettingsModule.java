package com.decenturion.mobile.di.dagger.settings.mail;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.mail.IMailingSettingsInteractor;
import com.decenturion.mobile.business.settings.mail.MailingSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.mail.presenter.IMailingSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.mail.presenter.MailingSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MailingSettingsModule {

    @Provides
    @MailingSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @MailingSettingsScope
    IMailingSettingsInteractor provideIMailingSettingsInteractor(IResidentClient iResidentClient,
                                                                  IResidentRepository iResidentRepository) {
        return new MailingSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @MailingSettingsScope
    IMailingSettingsPresenter provideIMailingSettingsPresenter(IMailingSettingsInteractor iMailingSettingsInteractor,
                                                               ILocalistionManager iLocalistionManager) {
        return new MailingSettingsPresenter(iMailingSettingsInteractor, iLocalistionManager);
    }
}
