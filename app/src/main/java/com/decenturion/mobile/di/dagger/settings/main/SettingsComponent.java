package com.decenturion.mobile.di.dagger.settings.main;

import com.decenturion.mobile.ui.fragment.settings.main.view.SettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SettingsModule.class
})

@SettingsScope
public interface SettingsComponent {

    void inject(SettingsFragment view);
}
