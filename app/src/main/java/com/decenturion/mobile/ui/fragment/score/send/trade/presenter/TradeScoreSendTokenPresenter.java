package com.decenturion.mobile.ui.fragment.score.send.trade.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.score.trade.ITradeScoreSendTokenInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.request.score.send.SendToMainModel;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.score.send.trade.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendTokenModelView;
import com.decenturion.mobile.ui.fragment.score.send.trade.view.ITradeScoreSendTokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TradeScoreSendTokenPresenter extends Presenter<ITradeScoreSendTokenView> implements ITradeScoreSendTokenPresenter {

    private ITradeScoreSendTokenInteractor mITradeScoreSendTokenInteractor;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    private TradeScoreSendTokenModelView mSendTokenModelView;

    public TradeScoreSendTokenPresenter(ITradeScoreSendTokenInteractor iTradeScoreSendTokenInteractor,
                                        IPrefsManager iPrefsManager,
                                        ILocalistionManager iLocalistionManager) {
        mIPrefsManager = iPrefsManager;
        mITradeScoreSendTokenInteractor = iTradeScoreSendTokenInteractor;
        mILocalistionManager = iLocalistionManager;

        mSendTokenModelView = new TradeScoreSendTokenModelView();
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mSendTokenModelView = new TradeScoreSendTokenModelView(tokenId);

        Observable<TradeScoreSendTokenModelView> obs = mITradeScoreSendTokenInteractor.getCoinInfo(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TradeScoreSendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    String amount,
                                    String twofa) {

        bundle.putString("coinUUID", mSendTokenModelView.getCoinUUID());
        bundle.putString("coinCategory", mSendTokenModelView.getCoinCategory());
        bundle.putString("amount", amount);
        bundle.putString("twofa", twofa);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        String coinUUID = savedInstanceState.getString("coinUUID", "");
        mSendTokenModelView.setCoinUUID(coinUUID);

        String coinCategory = savedInstanceState.getString("coinCategory", "");
        mSendTokenModelView.setCoinCategory(coinCategory);

        String amount = savedInstanceState.getString("amount", "");
        mSendTokenModelView.setAmount(amount);

        mSendTokenModelView.setTwoFA(savedInstanceState.getString("twofa", ""));

        Observable<TradeScoreSendTokenModelView> obs = Observable.just(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TradeScoreSendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TradeScoreSendTokenModelView model) {
        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void sendToken(@NonNull String amount,
                          MethodSendToken method,
                          @NonNull String twofa) {

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        SendToMainModel model = new SendToMainModel();
        model.setCoin(mSendTokenModelView.getCoinUUID());
        model.setCategory(mSendTokenModelView.getCoinCategory());
        model.setAmount(amount);
        model.setMethod(method.name().toLowerCase());
        model.setTwoFa(twofa);

        Observable<SendTokenResult> obs = mITradeScoreSendTokenInteractor.sendToMain(model)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenResult>) this::sendTokenSuccess,
                this::sendTokenFailure
        );
    }

    private void sendTokenFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                switch (error.getKey()) {
                    default: {
//                        showErrorView(error.getMessage());
                        if (mView != null) {
                            mView.onSendTokenFail();
                        }
                    }
                }
                return;
            }
        }

        showErrorView(throwable);
    }

    private void sendTokenSuccess(SendTokenResult result) {
        hideProgressView();
        if (mView != null) {
            mView.onSendTokenSuccess();
        }
    }
}
