package com.decenturion.mobile.ui.fragment.settings.password.model;

public class PasswordSettingsModelView {

    private String mCurrentPassword;
    private String mNewPassword;
    private String mReypePassword;

    public PasswordSettingsModelView() {
    }

    public void setCurrentPassword(String currentPassword) {
        mCurrentPassword = currentPassword;
    }

    public void setNewPassword(String newPassword) {
        mNewPassword = newPassword;
    }

    public void setReypePassword(String reypePassword) {
        mReypePassword = reypePassword;
    }

    public String getCurrentPassword() {
        return mCurrentPassword;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public String getReypePassword() {
        return mReypePassword;
    }
}
