package com.decenturion.mobile.di.dagger.splash;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.splash.ISplashInteractor;
import com.decenturion.mobile.business.splash.SplashInteractor;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.ui.activity.splash.presenter.ISplashPresenter;
import com.decenturion.mobile.ui.activity.splash.presenter.SplashPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {

    @Provides
    @SplashScope
    ISplashInteractor provideISplashInteractor(IDeviceClient iDeviceClient,
                                               IResidentClient iResidentClient,
                                               ILocalistionManager iLocalistionManager,
                                               IPrefsManager iPrefsManager) {
        return new SplashInteractor(
                iDeviceClient,
                iResidentClient,
                iLocalistionManager,
                iPrefsManager
        );
    }

    @Provides
    @SplashScope
    ISplashPresenter provideISplashPresenter(ISplashInteractor iSplashInteractor,
            IPrefsManager iPrefsManager) {

        return new SplashPresenter(iSplashInteractor, iPrefsManager);
    }
}
