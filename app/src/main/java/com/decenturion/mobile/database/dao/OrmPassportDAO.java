package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmPassportDAO extends BaseDao<OrmPassportModel, Integer> {

    public OrmPassportDAO(ConnectionSource connectionSource,
                          Class<OrmPassportModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
