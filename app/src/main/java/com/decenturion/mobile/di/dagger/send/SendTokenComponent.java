package com.decenturion.mobile.di.dagger.send;

import com.decenturion.mobile.ui.dialog.token.send.SendTokenSuccessFragmentDialog;
import com.decenturion.mobile.ui.fragment.token.send.view.SendTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SendTokenModule.class
})

@SendTokenScope
public interface SendTokenComponent {

    void inject(SendTokenFragment view);

    void inject(SendTokenSuccessFragmentDialog view);
}
