package com.decenturion.mobile.di.dagger.settings.wallets;

import android.content.Context;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.passport.IPassportSettingsInteractor;
import com.decenturion.mobile.business.settings.passport.PassportSettingsInteractor;
import com.decenturion.mobile.business.settings.wallets.IWalletsSettingsInteractor;
import com.decenturion.mobile.business.settings.wallets.WalletsSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.passport.presenter.IPassportSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.passport.presenter.PassportSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.wallet.presenter.IWalletsSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.wallet.presenter.WalletsSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class WalletsSettingsModule {

    @Provides
    @WalletsSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @WalletsSettingsScope
    IWalletsSettingsInteractor provideIWalletsSettingsInteractor(IResidentClient iResidentClient,
                                                                  IResidentRepository iResidentRepository) {
        return new WalletsSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @WalletsSettingsScope
    IWalletsSettingsPresenter provideIWalletsSettingsPresenter(
            IWalletsSettingsInteractor iWalletsSettingsInteractor,
            IPrefsManager iPrefsManager) {
        return new WalletsSettingsPresenter(iWalletsSettingsInteractor, iPrefsManager);
    }
}
