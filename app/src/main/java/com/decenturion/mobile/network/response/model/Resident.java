package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Resident extends BaseResponse {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("email")
    private String mEmail;

    @JsonProperty("phone")
    private String mPhone;

    @JsonProperty("subscribed")
    private boolean mSubscribed;

    @JsonProperty("balance")
    private String mBalance;

    @JsonProperty("step")
    private int mStep;

    @JsonProperty("g2fa")
    private boolean isG2fa;

    @JsonProperty("position")
    private String mPosition;

    @JsonProperty("money")
    private String mMoney;

    @JsonProperty("power")
    private String mPower;

    @JsonProperty("glory")
    private String mGlory;

    @JsonProperty("about")
    private String mAbout;

    @JsonProperty("twitter")
    private String mTwitter;

    @JsonProperty("medium")
    private String mMedium;

    @JsonProperty("telegram")
    private String mTelegram;

    @JsonProperty("facebook")
    private String mFacebook;

    @JsonProperty("wechat")
    private String mWechat;

    @JsonProperty("whatsapp")
    private String mWhatsapp;

    @JsonProperty("viber")
    private String mViber;

    @JsonProperty("invited")
    private boolean isInvited;

    @JsonProperty("active")
    private boolean isActive;

    @JsonProperty("dcnt")
    private String mDcnt;

    @JsonProperty("is_free_account")
    private boolean isFreeAccount;

    @JsonProperty("delivery_paid")
    private boolean isDeliveryPaid;

    @JsonProperty("referral_id")
    private String mReferralId;

    @JsonProperty("deposit_price")
    private int mDepositPrice;

    @JsonProperty("passport_price")
    private int mPassportPrice;

    public Resident() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getEmail() {
        return mEmail;
    }

    @JsonIgnore
    public String getPhone() {
        return mPhone;
    }

    @JsonIgnore
    public boolean isSubscribed() {
        return mSubscribed;
    }

    @JsonIgnore
    public String getWalletNum() {
        return mDcnt;
    }

    @JsonIgnore
    public String getBalance() {
        return mBalance;
    }

    @JsonIgnore
    public int getStep() {
        return mStep;
    }

    @JsonIgnore
    public boolean isG2fa() {
        return this.isG2fa;
    }

    @JsonIgnore
    public String getPosition() {
        return mPosition;
    }

    @JsonIgnore
    public String getMoney() {
        return mMoney;
    }

    @JsonIgnore
    public String getPower() {
        return mPower;
    }

    @JsonIgnore
    public String getGlory() {
        return mGlory;
    }

    @JsonIgnore
    public String getAbout() {
        return mAbout;
    }

    @JsonIgnore
    public String getTwitter() {
        return mTwitter;
    }

    @JsonIgnore
    public String getMedium() {
        return mMedium;
    }

    @JsonIgnore
    public String getTelegram() {
        return mTelegram;
    }

    @JsonIgnore
    public String getFacebook() {
        return mFacebook;
    }

    @JsonIgnore
    public String getWechat() {
        return mWechat;
    }

    @JsonIgnore
    public String getWhatsapp() {
        return mWhatsapp;
    }

    @JsonIgnore
    public String getViber() {
        return mViber;
    }

    @JsonIgnore
    public boolean isInvited() {
        return isInvited;
    }

    @JsonIgnore
    public boolean isActive() {
        return this.isActive || mStep > 4;
    }

    @JsonIgnore
    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }

    @JsonIgnore
    public String getReferralId() {
        return mReferralId;
    }

    @JsonIgnore
    public int getDepositPrice() {
        return mDepositPrice;
    }

    @JsonIgnore
    public int getPassportPrice() {
        return mPassportPrice;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }
}
