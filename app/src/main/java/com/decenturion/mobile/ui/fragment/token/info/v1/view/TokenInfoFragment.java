package com.decenturion.mobile.ui.fragment.token.info.v1.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.info.v1.TokenInfoAdapter;
import com.decenturion.mobile.ui.fragment.token.info.v1.model.TokenInfoModelView;
import com.decenturion.mobile.ui.fragment.token.info.v1.presenter.ITokenInfoPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class TokenInfoFragment extends BaseFragment implements ITokenInfoView
        , View.OnClickListener {

    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_CATEGORY = "TOKEN_CATEGORY";

    public int mTokenId;
    private String mTokenCategory;

    /* UI */

    @BindView(R.id.tokenInfoListView)
    RecyclerView mTokenInfoListView;
    private TokenInfoAdapter mTokenInfoAdapter;


    /* DI */

    @Inject
    ITokenInfoPresenter mITokenInfoPresenter;

    @Inject
    IResourceManager mIResourceManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID, 0);
            mTokenCategory = arguments.getString(TOKEN_CATEGORY);
        }
    }

    private void injectToDagger() {
//        AppDelegate appDelegate = getApplication();
//        TokenInfoComponent tokenInfoComponent = appDelegate
//                .getIDIManager()
//                .plusTokenInfoComponent();
//        tokenInfoComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_token_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        Context context = getContext();
        assert context != null;
        mTokenInfoListView.setLayoutManager(new LinearLayoutManager(context));
        mTokenInfoAdapter = new TokenInfoAdapter(context, mIResourceManager);
        mTokenInfoListView.setAdapter(mTokenInfoAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITokenInfoPresenter.bindView(this);
        if (!TextUtils.isEmpty(mTokenCategory)) {
            mITokenInfoPresenter.bindViewData(mTokenCategory);
        } else {
            mITokenInfoPresenter.bindViewData(mTokenId);
        }
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle("Token info");
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onBindViewData(@NonNull TokenInfoModelView model) {
        mTokenInfoAdapter.replaceDataSet(model.getInfoModelViews());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

}
