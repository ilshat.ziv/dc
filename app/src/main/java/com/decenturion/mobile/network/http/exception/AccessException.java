package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;

public class AccessException extends AppException {

    public AccessException() {
        super("Not authorized");
    }

    public AccessException(Throwable throwable) {
        super(throwable.getMessage());
    }

    public AccessException(String message) {
        super(message);
    }
}
