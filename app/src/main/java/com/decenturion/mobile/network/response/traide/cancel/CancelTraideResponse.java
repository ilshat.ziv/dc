package com.decenturion.mobile.network.response.traide.cancel;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelTraideResponse extends BaseResponse {

    @JsonProperty("result")
    private CancelTraideResult mResult;

    public CancelTraideResponse() {
    }

    @JsonIgnore
    public CancelTraideResult getResult() {
        return mResult;
    }
}
