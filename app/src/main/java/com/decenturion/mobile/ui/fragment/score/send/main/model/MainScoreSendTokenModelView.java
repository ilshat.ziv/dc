package com.decenturion.mobile.ui.fragment.score.send.main.model;

import android.support.annotation.NonNull;

public class MainScoreSendTokenModelView {

    private int mTokenId;
    private String mCoinUUID;
    private String mCoinCategory;
    private String mDcntWallet;
    private String mAmount;
    private String mTwoFA;
    private String mSymbol;
    private String mTokenName;

    private MainScoreSendBalanceModel mMainScoreSendBalanceModel;
    private MainScoreSendOptionModel mMainScoreSendOptionModel;

    public MainScoreSendTokenModelView() {
    }

    public MainScoreSendTokenModelView(int tokenId) {
        mTokenId = tokenId;
    }

    public int getTokenId() {
        return mTokenId;
    }

    public String getCoinUUID() {
        return mCoinUUID;
    }

    public String getCoinCategory() {
        return mCoinCategory;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getTwoFA() {
        return mTwoFA;
    }

    public String getDcntWallet() {
        return mDcntWallet;
    }

    public void setCoinUUID(String coinUUID) {
        mCoinUUID = coinUUID;
    }

    public void setCoinCategory(String coinCategory) {
        mCoinCategory = coinCategory;
    }

    public void setTwoFA(String twoFA) {
        mTwoFA = twoFA;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public void setDcntWallet(String dcntWallet) {
        mDcntWallet = dcntWallet;
    }

    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    public String getSymbol() {
        return mSymbol;
    }

    public void setTokenName(String tokenName) {
        mTokenName = tokenName;
    }

    public String getTokenName() {
        return mTokenName;
    }

    public MainScoreSendOptionModel getMainScoreSendOptionModel() {
        return mMainScoreSendOptionModel;
    }

    public void setMainScoreSendOptionModel(@NonNull MainScoreSendOptionModel model) {
        mMainScoreSendOptionModel = model;
    }

    public MainScoreSendBalanceModel getMainScoreSendBalanceModel() {
        return mMainScoreSendBalanceModel;
    }

    public void setMainScoreSendBalanceModel(@NonNull MainScoreSendBalanceModel model) {
        mMainScoreSendBalanceModel = model;
    }
}
