package com.decenturion.mobile.ui.fragment.profile_v2.score.list.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreListModelView;

public interface IScoreListView extends IScreenFragmentView {

    void onBindViewData(@NonNull ScoreListModelView model);

    void onSignout();
}
