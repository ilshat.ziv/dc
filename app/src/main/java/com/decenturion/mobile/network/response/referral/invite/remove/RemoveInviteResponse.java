package com.decenturion.mobile.network.response.referral.invite.remove;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoveInviteResponse extends BaseResponse {

    @JsonProperty("result")
    private RemoveInviteResult mResult;

    public RemoveInviteResponse() {
    }

    @JsonIgnore
    public RemoveInviteResult getResult() {
        return mResult;
    }
}
