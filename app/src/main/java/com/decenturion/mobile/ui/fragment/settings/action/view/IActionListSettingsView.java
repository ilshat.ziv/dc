package com.decenturion.mobile.ui.fragment.settings.action.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;

public interface IActionListSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull LogModelView model);

    void logoutSuccess();
}
