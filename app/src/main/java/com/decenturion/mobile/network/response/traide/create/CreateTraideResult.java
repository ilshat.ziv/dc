package com.decenturion.mobile.network.response.traide.create;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTraideResult {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("price")
    private String mPrice;

    @JsonProperty("total_price")
    private String mTotalPrice;

    @JsonProperty("receive_address")
    private String mReceiveAddress;

    @JsonProperty("refund_address")
    private String mRefundAddress;

    @JsonProperty("txid")
    private String mTxid;

    @JsonProperty("trade_address")
    private String mTradeAddress;

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("frontend_id")
    private String mFrontendId;

    @JsonProperty("internal")
    private boolean isInternal;

    @JsonProperty("email")
    private String mEmail;

    public CreateTraideResult() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getTotalPrice() {
        return mTotalPrice;
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getReceiveAddress() {
        return mReceiveAddress;
    }

    @JsonIgnore
    public String getRefundAddress() {
        return mRefundAddress;
    }

    @JsonIgnore
    public String getTxid() {
        return mTxid;
    }

    @JsonIgnore
    public String getTradeAddress() {
        return mTradeAddress;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getFrontendId() {
        return mFrontendId;
    }

    @JsonIgnore
    public boolean isInternal() {
        return this.isInternal;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String isEmail() {
        return mEmail;
    }
}
