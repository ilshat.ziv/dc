package com.decenturion.mobile.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class RandomStringUtils {

    /**
     * http://qaru.site/questions/934/how-to-generate-a-random-alpha-numeric-string
     * Generate a mRandom string.
     */
    public String nextString() {
        for (int idx = 0; idx < mBuf.length; ++idx)
            mBuf[idx] = mSymbols[mRandom.nextInt(mSymbols.length)];
        return new String(mBuf);
    }

    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String LOWER = UPPER.toLowerCase(Locale.ROOT);

    public static final String DIGITS = "0123456789";

    public static final String ALPHANUM = UPPER + LOWER + DIGITS;

    private final Random mRandom;

    private final char[] mSymbols;

    private final char[] mBuf;

    public RandomStringUtils(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.mRandom = Objects.requireNonNull(random);
        this.mSymbols = symbols.toCharArray();
        this.mBuf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomStringUtils(int length, Random random) {
        this(length, random, ALPHANUM);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomStringUtils(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomStringUtils() {
        this(16);
    }

}