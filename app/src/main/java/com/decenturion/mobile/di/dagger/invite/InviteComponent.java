package com.decenturion.mobile.di.dagger.invite;

import com.decenturion.mobile.ui.fragment.invite.InviteFailFragment;
import com.decenturion.mobile.ui.fragment.invite.InviteSuccessFragment;
import com.decenturion.mobile.ui.fragment.invite.view.InviteFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        InviteModule.class
})

@InviteScope
public interface InviteComponent {

    void inject(InviteFragment view);

    void inject(InviteSuccessFragment view);

    void inject(InviteFailFragment view);
}
