package com.decenturion.mobile.ui.fragment.token.info.v1.presenter;

import com.decenturion.mobile.business.tokendetails.info.ITokenInfoInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.info.v1.model.TokenInfoModelView;
import com.decenturion.mobile.ui.fragment.token.info.v1.view.ITokenInfoView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TokenInfoPresenter extends Presenter<ITokenInfoView> implements ITokenInfoPresenter {

    private ITokenInfoInteractor mITokenInfoInteractor;
    private TokenInfoModelView mTokenInfoModelView;

    public TokenInfoPresenter(ITokenInfoInteractor iTokenInfoInteractor) {
        mITokenInfoInteractor = iTokenInfoInteractor;
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mTokenInfoModelView = new TokenInfoModelView(tokenId);
        Observable<TokenInfoModelView> obs = mITokenInfoInteractor.getTokenInfo(tokenId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenInfoModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(String category) {
        showProgressView();

        mTokenInfoModelView = new TokenInfoModelView(category);
        Observable<TokenInfoModelView> obs = Observable.just(mTokenInfoModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenInfoModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TokenInfoModelView model) {
        mTokenInfoModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTokenInfoModelView = null;
    }
}
