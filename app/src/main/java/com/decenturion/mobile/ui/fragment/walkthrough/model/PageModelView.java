package com.decenturion.mobile.ui.fragment.walkthrough.model;

public class PageModelView {

    private String mTitle;
    private String mText;

    public PageModelView(String title, String text) {
        mTitle = title;
        mText = text;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getText() {
        return mText;
    }
}
