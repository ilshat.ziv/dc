package com.decenturion.mobile.exception;

public class UnsupportedVersionAppException extends AppException {

    public UnsupportedVersionAppException() {
    }

    public UnsupportedVersionAppException(Throwable throwable) {
        super(throwable.getMessage());
    }

    public UnsupportedVersionAppException(String message) {
        super(message);
    }
}
