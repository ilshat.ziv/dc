package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;

public class CsrfAccessException extends AppException {

    public CsrfAccessException() {
        super("Tokein is not valid");
    }

    public CsrfAccessException(Throwable throwable) {
        super(throwable.getMessage());
    }

    public CsrfAccessException(String message) {
        super(message);
    }
}
