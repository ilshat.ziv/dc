package com.decenturion.mobile.network.response;

import com.decenturion.mobile.network.response.model.Error;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BadResponse extends BaseResponse {

    @JsonProperty("result")
    private ArrayList<Error> mResult;

    public BadResponse() {
    }

    @JsonIgnore
    public ArrayList<Error> getResult() {
        return mResult;
    }
}
