package com.decenturion.mobile.di.dagger.signup;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.signup.ISignupInteractor;
import com.decenturion.mobile.business.signup.SignupInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.sign.up.presenter.ISignupPresenter;
import com.decenturion.mobile.ui.fragment.sign.up.presenter.SignupPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SignupModule {

    @Provides
    @SignupScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SignupScope
    ISignupInteractor provideISignupInteractor(IResidentClient iResidentClient,
                                               IReferralClient iReferralClient,
                                               IResidentRepository iResidentRepository,
                                               IPrefsManager iPrefsManager) {
        return new SignupInteractor(iResidentClient, iReferralClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @SignupScope
    ISignupPresenter provideISignupPresenter(ISignupInteractor iSignupInteractor) {
        return new SignupPresenter(iSignupInteractor);
    }
}
