package com.decenturion.mobile.ui.fragment.settings.delivery.fill.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.FillDeliverySettingsModelView;

public interface IFillDeliverySettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull CountryModelView model1,
                        @NonNull FillDeliverySettingsModelView model);

    void onDeliveryPassportDataSeted(@NonNull FillDeliverySettingsModelView model);
}
