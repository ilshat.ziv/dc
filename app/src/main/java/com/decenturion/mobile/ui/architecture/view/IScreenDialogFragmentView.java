package com.decenturion.mobile.ui.architecture.view;

public interface IScreenDialogFragmentView extends IScreenFragmentView {

    IScreenActivityView getIScreenActivityView();

}
