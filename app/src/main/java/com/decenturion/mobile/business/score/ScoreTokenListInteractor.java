package com.decenturion.mobile.business.score;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.model.Token;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.score.FutureScoreTokenListResponse;
import com.decenturion.mobile.network.response.score.ScoreTokenListResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.List;

import rx.Observable;

public class ScoreTokenListInteractor implements IScoreTokenListInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IScoreClient mIScoreClient;

    private IResidentRepository mIResidentRepository;
    private IWalletRepository mIWalletRepository;
    private ITokenRepository mITokenRepository;

    private IPrefsManager mIPrefsManager;

    public ScoreTokenListInteractor(IResidentClient IResidentClient,
                                    ITokenClient iTokenClient,
                                    IScoreClient iScoreClient,
                                    IResidentRepository iResidentRepository,
                                    IWalletRepository iWalletRepository,
                                    ITokenRepository iTokenRepository,
                                    IPrefsManager iPrefsManager) {

        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIScoreClient = iScoreClient;
        mIResidentRepository = iResidentRepository;
        mIWalletRepository = iWalletRepository;
        mITokenRepository = iTokenRepository;
        mIPrefsManager = iPrefsManager;
    }

    private Observable<OrmResidentModel> getResidentIfon() {
        Observable<OrmResidentModel> obs1 = Observable.just(mIResidentRepository.getResidentInfo());
        Observable<OrmResidentModel> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    OrmResidentModel ormModel = new OrmResidentModel(d.getResident());
                    try {
                        mIResidentRepository.saveResidentInfo(ormModel);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return ormModel;
                });

        return Observable.concat(obs1, obs2, Observable.error(new RuntimeException("Oops, an error occurred")))
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<ScoreTokenListModelView> getTokenListModelView(@NonNull ScoreTokenListModelView model) {
        switch (model.getScoreType()) {
            case FUTURES: {
                return getFutures(model);
            }
            case TRADE: {
                return getTrade(model);
            }
            case MAIN:
            default: {
                return getMain(model);
            }
        }
    }

    private Observable<ScoreTokenListModelView> getFutures(@NonNull ScoreTokenListModelView model) {
        Observable<ScoreTokenListModelView> obs1 = mIScoreClient.getFutures(
                model.getOffset(),
                ScoreTokenListModelView.LIMIT
        )
                .map(FutureScoreTokenListResponse::getResult)
                .flatMap(r -> {
                    ScoreTokenListModelView model1 = new ScoreTokenListModelView(ScoreType.FUTURES);
                    model1.setTotal(r.getTotal());


                    List<Token> tokenList = r.getTokenList();
                    if (CollectionUtils.isEmpty(tokenList)) {
                        return Observable.just(model1);
                    } else {
                        Observable<List<ScoreTokenModelView>> listObservable = Observable.just(tokenList)
                                .flatMapIterable(list1 -> list1)
                                .map(t -> {
                                    OrmTokenModel tm = new OrmTokenModel(t);
                                    try {
                                        mITokenRepository.saveTokenInfo(tm);
                                    } catch (Exception e) {
                                        LoggerUtils.exception(e);
                                    }
                                    return new ScoreTokenModelView(tm, t.getStartup().getLogo());
                                })
                                .toList();

                        return Observable.zip(
                                listObservable,
                                Observable.just(model1), (scoreTokenModelViews, scoreTokenListModelView) -> {
                                    scoreTokenListModelView.setTokentModelList(scoreTokenModelViews);
                                    return scoreTokenListModelView;
                                }
                        );
                    }
                });

        Observable<ScoreTokenListModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (t1, t2) -> {
            t2.setOffset(t2.getOffset() + ScoreTokenListModelView.LIMIT);
            t2.addTokenList(t1.getTokentModelList());
            t2.setTotal(t1.getTotal());
            return t2;
        });
    }

    private Observable<ScoreTokenListModelView> getTrade(@NonNull ScoreTokenListModelView model) {
        Observable<ScoreTokenListModelView> obs1 = mIScoreClient.getTrade(
                model.getOffset(),
                ScoreTokenListModelView.LIMIT
        )
                .map(ScoreTokenListResponse::getResult)
                .flatMap(r -> {
                    ScoreTokenListModelView model1 = new ScoreTokenListModelView(ScoreType.TRADE);
                    model1.setTotal(r.getTotal());

                    Observable<List<ScoreTokenModelView>> listObservable = Observable.just(r.getTokenList())
                            .flatMapIterable(list1 -> list1)
                            .map(t -> {
                                OrmTokenModel tm = new OrmTokenModel(t);
                                try {
                                    mITokenRepository.saveTokenInfo(tm);
                                } catch (Exception e) {
                                    LoggerUtils.exception(e);
                                }
                                return new ScoreTokenModelView(tm, t.getStartup().getLogo());
                            })
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(model1), (scoreTokenModelViews, scoreTokenListModelView) -> {
                                scoreTokenListModelView.setTokentModelList(scoreTokenModelViews);
                                return scoreTokenListModelView;
                            }
                    );
                });

        Observable<ScoreTokenListModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (t1, t2) -> {
            t2.setOffset(t2.getOffset() + ScoreTokenListModelView.LIMIT);
            t2.addTokenList(t1.getTokentModelList());
            t2.setTotal(t1.getTotal());
            return t2;
        });
    }

    private Observable<ScoreTokenListModelView> getMain(@NonNull ScoreTokenListModelView model) {
        Observable<ScoreTokenListModelView> obs1 = mIScoreClient.getMain(
                model.getOffset(),
                ScoreTokenListModelView.LIMIT
        )
                .map(ScoreTokenListResponse::getResult)
                .flatMap(r -> {
                    ScoreTokenListModelView model1 = new ScoreTokenListModelView(ScoreType.MAIN);
                    model1.setTotal(r.getTotal());

                    Observable<List<ScoreTokenModelView>> listObservable = Observable.just(r.getTokenList())
                            .flatMapIterable(list1 -> list1)
                            .map(t -> {
                                OrmTokenModel tm = new OrmTokenModel(t);
                                try {
                                    mITokenRepository.saveTokenInfo(tm);
                                } catch (Exception e) {
                                    LoggerUtils.exception(e);
                                }
                                return new ScoreTokenModelView(tm, t.getStartup().getLogo());
                            })
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(model1), (scoreTokenModelViews, scoreTokenListModelView) -> {
                                scoreTokenListModelView.setTokentModelList(scoreTokenModelViews);
                                return scoreTokenListModelView;
                            }
                    );
                });

        Observable<ScoreTokenListModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (t1, t2) -> {
            t2.setOffset(t2.getOffset() + ScoreTokenListModelView.LIMIT);
            t2.addTokenList(t1.getTokentModelList());
            t2.setTotal(t1.getTotal());
            return t2;
        });
    }
}
