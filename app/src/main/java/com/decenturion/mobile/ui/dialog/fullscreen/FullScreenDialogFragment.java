package com.decenturion.mobile.ui.dialog.fullscreen;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.decenturion.mobile.R;

import butterknife.BindView;

public abstract class FullScreenDialogFragment extends DialogFragment implements Toolbar.OnMenuItemClickListener {

    public static final String TITLE = "TITLE";

    /* UI */

    private View mDialogView;

    @Nullable
    @BindView(R.id.mainContent)
    protected FrameLayout mMainContent;

    @Nullable
    @BindView(R.id.waitContent)
    protected LinearLayout mWaitContent;

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    @BindView(R.id.containerToolbarView)
    protected LinearLayout mContainerToolbarView;

    protected enum Mode {
        WAIT, EDIT
    }

    private int mPositiveText = R.string.ok;
    private int mNegativeText = R.string.cancel;
    private String mTitle;

    public void setPositiveText(int positiveText) {
        mPositiveText = positiveText;
    }

    public void setNegativeText(int negativeText) {
        mNegativeText = negativeText;
    }

    public void setTitleText(@StringRes int titleText) {
        initView();
        mToolbar.setTitle(titleText);
    }

    public void setTitleText(String titleText) {
        initView();
        mToolbar.setTitle(titleText);
    }

    public void setSubTitle(String titleText) {
        initView();
        mToolbar.setSubtitle(titleText);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
        initArgs();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString(TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mDialogView = inflater.inflate(R.layout.fr_fd, container);
        return mDialogView;
    }

    private void initView() {
        if (mDialogView == null || mMainContent != null && mWaitContent != null) {
            return;
        }

//        mAppBarLayout = (AppBarLayout) mDialogView.findViewById(appBarLayout);
        mToolbar = (Toolbar) mDialogView.findViewById(R.id.toolbar);
        mMainContent = (FrameLayout) mDialogView.findViewById(R.id.mainContent);
        mWaitContent = (LinearLayout) mDialogView.findViewById(R.id.waitContent);

        if (!TextUtils.isEmpty(mTitle)) {
            mToolbar.setTitle(mTitle);
        }

        mToolbar.setNavigationOnClickListener(v -> onNegativeClickButton());
        mToolbar.setNavigationIcon(R.drawable.ic_close_24dp);

        mToolbar.inflateMenu(R.menu.full_screen_dialog_menu);
        mToolbar.setOnMenuItemClickListener(this);

//        mContainerToolbarView = (LinearLayout) mToolbar.findViewById(R.id.containerToolbarView);
        mContainerToolbarView = (LinearLayout) mDialogView.findViewById(R.id.containerToolbarView);
    }

    public void setContentView(int layout) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(layout, null);
        initView();
        if (mMainContent != null) {
            mMainContent.addView(view);
        }
    }

    public void setContentToolbarView(int layout) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(layout, null);
        initView();
        if (mContainerToolbarView != null) {
            mContainerToolbarView.addView(view);
        }
    }

    public void hideShadowToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//            mAppBarLayout.setElevation(0);
//            mAppBarLayout.invalidate();
            mToolbar.setElevation(0);

//            mAppBarLayout.invalidate();
        }
    }

    public void restoreToolbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//            mAppBarLayout.setElevation(8);
            mToolbar.setElevation(8);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done : {
                onPositiveClickButton();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    private void waitMode() {
        setVisibilityActions(false);
        initView();
        mWaitContent.setVisibility(View.VISIBLE);
        mMainContent.setVisibility(View.GONE);
    }

    private void editMode() {
        setVisibilityActions(true);
        initView();
        mWaitContent.setVisibility(View.GONE);
        mMainContent.setVisibility(View.VISIBLE);
    }

    protected void setDialogMode(Mode modeDialog) {
        switch (modeDialog) {
            case WAIT : {
                waitMode();
                break;
            }
            case EDIT: default: {
                editMode();
                break;
            }
        }
    }

    public void setFullVisual(boolean isFull) {
        initView();
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mMainContent.getLayoutParams();
        if (isFull) {
            params.setBehavior(null);
            mToolbar.setBackgroundResource(R.color.transparent_black);
//            mAppBarLayout.setBackgroundResource(R.color.transparent_black);
            hideShadowToolbar();
        } else {
            params.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        }
        mMainContent.requestLayout();
    }

    public void hideToolbar() {
        initView();
        mToolbar.setVisibility(View.GONE);
//        mAppBarLayout.setVisibility(View.GONE);
        hideShadowToolbar();
        mMainContent.requestLayout();
    }

    public void onPositiveClickButton() {
        setDialogMode(Mode.WAIT);
    }

    public void onNegativeClickButton() {
        dismiss();
    }

    protected void setEnabledPositiveClickButton(boolean isEnabled) {
        Menu menu = mToolbar.getMenu();
        if (menu != null) {
            menu.clear();
        }
    }

    protected void setVisibilityNegativeClickButton(boolean isVisible) {
        if (isVisible) {
//            mDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.VISIBLE);
        } else {
//            mDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
        }
    }

    protected void setVisibilityPositiveClickButton(boolean isVisible) {
        if (isVisible) {
//            mDialog.getActionButton(DialogAction.POSITIVE).setVisibility(View.VISIBLE);
        } else {
//            mDialog.getActionButton(DialogAction.POSITIVE).setVisibility(View.GONE);
        }
    }

    private void setVisibilityActions(boolean isVisible) {
        setVisibilityPositiveClickButton(isVisible);
        setVisibilityNegativeClickButton(isVisible);
    }

//    protected void registerOtto() {
//        try {
//            OttoBus.getInstance().register(this);
//        } catch (Exception e) {
//            LoggerUtils.exception(e);;
//        }
//    }
//
//    protected void unregisterOtto() {
//        try {
//            OttoBus.getInstance().unregister(this);
//        } catch (Exception e) {
//            LoggerUtils.exception(e);;
//        }
//    }

    @Override
    public void onDestroyView() {
        restoreToolbar();
        super.onDestroyView();
    }
}