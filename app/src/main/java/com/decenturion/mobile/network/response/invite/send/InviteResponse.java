package com.decenturion.mobile.network.response.invite.send;

import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.invite.CheckInviteTokenResult;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteResponse extends BaseResponse {

    @JsonProperty("result")
    private InviteResult mResult;

    public InviteResponse() {
    }

    @JsonIgnore
    public InviteResult getResult() {
        return mResult;
    }
}
