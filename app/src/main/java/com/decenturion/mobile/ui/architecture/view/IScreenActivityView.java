package com.decenturion.mobile.ui.architecture.view;

import android.content.Intent;

public interface IScreenActivityView extends IScreenView {

    void onFragmentResult(int requestCode, int resultCode, Intent data);
}
