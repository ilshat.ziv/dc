package com.decenturion.mobile.ui.fragment.passport.wellcome.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.IWellcomePassportView;

public interface IWellcomePresenter extends IPresenter<IWellcomePassportView> {

    void bindViewData();

    void skipWellcomeMessage();
}
