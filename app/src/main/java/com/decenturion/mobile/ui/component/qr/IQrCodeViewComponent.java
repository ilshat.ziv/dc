package com.decenturion.mobile.ui.component.qr;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.component.Component;

public interface IQrCodeViewComponent extends Component {

    void setAddress(@NonNull String address);

    String getAddress();

    void setQrData(@NonNull String qrData);
}
