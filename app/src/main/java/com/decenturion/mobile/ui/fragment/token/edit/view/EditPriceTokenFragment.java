package com.decenturion.mobile.ui.fragment.token.edit.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.tokendetails.edit.EditPriceTokenComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.edit.model.EditPriceModelView;
import com.decenturion.mobile.ui.fragment.token.edit.presenter.IEditPriceTokenPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class EditPriceTokenFragment extends BaseFragment implements IEditPriceTokenView,
        View.OnClickListener,
        ChangeComponentListener {

    public static final String TOKEN_ID = "TOKEN_ID";

    public int mTokenId;

    /* UI */

    @BindView(R.id.sellPriceView)
    BuyTokenView mSellPriceView;

    @BindView(R.id.minSellPriceLabelView)
    LocalizedTextView mMinSellPriceLabelView;

    @BindView(R.id.buyPriceView)
    BuyTokenView mBuyPriceView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.saveChangesButtonView)
    LinearLayout mSaveChangesButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IEditPriceTokenPresenter mIEditPriceTokenPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = getActivity();
        assert activity != null;
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID, 0);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        EditPriceTokenComponent editPriceTokenComponent = appDelegate
                .getIDIManager()
                .plusEditPriceTokenComponent();
        editPriceTokenComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_edit_price_token, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        mSaveChangesButtonView.setEnabled(false);

        mSellPriceView.showLabel();
        mSellPriceView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_sellprice_title));
        mSellPriceView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_sellprice_placeholder));
        mSellPriceView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mSellPriceView.editMode();
        mSellPriceView.setVisibleDivider(true);
        mSellPriceView.setChangeDataModelListener(this);

        mBuyPriceView.showLabel();
        mBuyPriceView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_buyprice_title));
        mBuyPriceView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_buyprice_placeholder));
        mBuyPriceView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mBuyPriceView.editMode();
        mBuyPriceView.setVisibleDivider(true);
        mBuyPriceView.setChangeDataModelListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIEditPriceTokenPresenter.bindView(this);
        mIEditPriceTokenPresenter.bindViewData(mTokenId);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIEditPriceTokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIEditPriceTokenPresenter.unbindView();
        mIEditPriceTokenPresenter.onSaveInstanceState(outState,
                mTokenId,
                mBuyPriceView.getValue(), mSellPriceView.getValue()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIEditPriceTokenPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIEditPriceTokenPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull EditPriceModelView model) {
        mSellPriceView.setText(model.getSell());
        mSellPriceView.setCoin("USDT");

        if (TextUtils.equals(model.getCoin(), "DCNT") && TextUtils.equals(model.getCategory(), "internal")) {
            String string = mILocalistionManager.getLocaleString(R.string.app_account_tokens_editprices_sell_minprice);
            string = string.replace("{min_price}", model.getSellMinPrice());
            mMinSellPriceLabelView.setText(string);
            mMinSellPriceLabelView.setVisibility(View.VISIBLE);
        } else {
            mMinSellPriceLabelView.setVisibility(View.GONE);
        }

        mBuyPriceView.setText(model.getBuy());
        mBuyPriceView.setCoin("USDT");
    }

    @Override
    public void onChangesSaved() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onBuyTokens() {
        mIEditPriceTokenPresenter.saveChanges(mTokenId, mSellPriceView.getValue());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onChangeComponentData(View view) {
        String value = mSellPriceView.getValue();
//        String value1 = mBuyPriceView.getValue();
        double v = proccessPrice(value);
//        double v1 = proccessPrice(value1);
        boolean b = !TextUtils.isEmpty(value) &&
//                !TextUtils.isEmpty(value1) &&
                v > 0
//                && v1 > 0
                ;

        mSaveChangesButtonView.setEnabled(b);
    }

    private double proccessPrice(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            LoggerUtils.exception(e);
        }

        return 0;
    }
}
