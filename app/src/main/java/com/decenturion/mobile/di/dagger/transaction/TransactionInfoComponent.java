package com.decenturion.mobile.di.dagger.transaction;

import com.decenturion.mobile.ui.fragment.transaction.view.TransactionInfoFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TransactionInfoModule.class
})

@TransactionInfoScope
public interface TransactionInfoComponent {

    void inject(TransactionInfoFragment view);
}
