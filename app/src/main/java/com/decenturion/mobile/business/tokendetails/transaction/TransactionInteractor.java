package com.decenturion.mobile.business.tokendetails.transaction;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.request.ConfirmTraideModel;
import com.decenturion.mobile.network.request.DeleteTraidModel;
import com.decenturion.mobile.network.response.traide.cancel.CancelTraideResponse;
import com.decenturion.mobile.network.response.traide.cancel.CancelTraideResult;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResponse;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResult;
import com.decenturion.mobile.network.response.traide.state.StateTraideResponse;
import com.decenturion.mobile.network.response.traide.state.StateTraideResult;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class TransactionInteractor implements ITransactionInteractor {

    private IResidentClient mIResidentClient;
    private ITokenRepository mITokenRepository;
    private ITraideRepository mITraideRepository;
    private ITokenClient mITokenClient;

    public TransactionInteractor(IResidentClient IResidentClient,
                                 ITokenRepository ITokenRepository,
                                 ITokenClient iTokenClient,
                                 ITraideRepository iTraideRepository) {
        mIResidentClient = IResidentClient;
        mITokenRepository = ITokenRepository;
        mITraideRepository = iTraideRepository;
        mITokenClient = iTokenClient;
    }

    @Override
    public Observable<TransactionModelView> getTokenInfo(@NonNull String traideUUID) {
        return Observable.just(mITraideRepository.getTraide(traideUUID))
                .map(TransactionModelView::new);
    }

    @Override
    public Observable<ConfirmTraideResult> confirmTraide(boolean isInternal,
                                                         String receivedAddress,
                                                         String refundAddress,
                                                         String trideUUID,
                                                         String txid) {
//        {
//            "status": "ok",
//                "result": {
//            "uuid": "f72b19d6-6347-44c9-79fc-27073b04bce7",
//                    "status": "new",
//                    "category": "external",
//                    "amount": "1",
//                    "currency": "btc",
//                    "price": "0.2126661",
//                    "total_price": "0.2126661",
//                    "receive_address": "",
//                    "refund_address": "",
//                    "txid": "",
//                    "trade_address": "mfjLbA8qbpi6dKByJpUo5zAJpRRwXc2R55",
//                    "expired_at": "2018-08-29T05:08:12.517323698Z",
//                    "frontend_id": "1535519112516",
//                    "internal": false
//        }
//        }
        ConfirmTraideModel model = new ConfirmTraideModel();
        model.setInternal(isInternal);
        model.setReceive(receivedAddress);
        model.setRefund(refundAddress);
        model.setTraideUuid(trideUUID);
        model.setTxid(txid);


        return mITokenClient.confirmTraide(model)
                .map(ConfirmTraideResponse::getResult);
    }

    @Override
    public Observable<StateTraideResult> getStateTraide(String tradeUUID) {
        return mITokenClient.getTraide(tradeUUID)
                .map(StateTraideResponse::getResult);
    }

    @Override
    public Observable<CancelTraideResult> cancelTraide(String traidUUID) {
        DeleteTraidModel model = new DeleteTraidModel(traidUUID);
        return mITokenClient.cancelTraide(model)
                .map(CancelTraideResponse::getResult)
                .doOnNext(d -> {
                    try {
                        mITraideRepository.deleteTraide(d.getUuid());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }
}
