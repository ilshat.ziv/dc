package com.decenturion.mobile.network.response.password;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean isChange;

    public ChangePasswordResponse() {
    }

    @JsonIgnore
    public boolean isChange() {
        return isChange;
    }
}
