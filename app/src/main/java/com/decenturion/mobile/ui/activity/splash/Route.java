package com.decenturion.mobile.ui.activity.splash;

import android.os.Bundle;

public class Route {

    private TypeRoute mTypeRoute;
    private Bundle mBundle;

    Route(TypeRoute typeRoute) {
        mTypeRoute = typeRoute;
    }

    public TypeRoute getType() {
        return mTypeRoute;
    }

    public void setBundle(Bundle bundle) {
        mBundle = bundle;
    }

    public Bundle getBundle() {
        return mBundle;
    }

    public enum TypeRoute {
        RESET_PASSWORD,
        SINGLE_INVITE,
        REFERRAL_KEY,
        BANNER_REFERRAL_KEY,
        PAY_REFERRAL_KEY,
        CITIZEN,
        CONFIRM_EMAIL
    }
}
