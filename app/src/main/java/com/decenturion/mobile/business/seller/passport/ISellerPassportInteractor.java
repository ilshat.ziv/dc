package com.decenturion.mobile.business.seller.passport;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public interface ISellerPassportInteractor {

    Observable<PassportModelView> getProfileInfo(@NonNull String residentUuid);
}
