package com.decenturion.mobile.ui.fragment.sign.up.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.sign.up.view.ISignupView;

public interface ISignupPresenter extends IPresenter<ISignupView> {

    void bindViewData(@Nullable String inviteToken);

    // TODO: 21.08.2018 это временное решение надо биндится к вьюхам
    void saveViewData(@NonNull String value, @NonNull String value1, @NonNull String value2,
                      boolean checked, boolean checked1);

    void signup(@NonNull String username,
                @NonNull String password,
                @NonNull String retypePassword,
                @NonNull String captcha,
                boolean isAgreeTerms,
                boolean isSubscribe,
                @Nullable String inviteCode,
                String referralKey,
                String payReferralKey,
                String bannerReferralKey);

    void onSaveInstanceState(@NonNull Bundle outState,
                             @NonNull String username,
                             @NonNull String password,
                             @NonNull String retypePassword,
                             @NonNull String captcha,
                             boolean isAgreeTerms,
                             boolean isSubscribe,
                             @Nullable String inviteCode,
                             @Nullable String referralKey,
                             @Nullable String payReferralKey,
                             @Nullable String bannerReferralKey
    );

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
