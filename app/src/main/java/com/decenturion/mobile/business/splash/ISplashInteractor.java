package com.decenturion.mobile.business.splash;

import rx.Observable;

public interface ISplashInteractor {

    Observable<Boolean> buildApp();
}
