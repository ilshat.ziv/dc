package com.decenturion.mobile.ui.dialog.fullscreen.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.toast.IToastManager;
import com.decenturion.mobile.app.toast.TypeMessage;
import com.decenturion.mobile.ui.dialog.fullscreen.FullDialog;

import com.github.florent37.camerafragment.CameraFragment;
import com.github.florent37.camerafragment.CameraFragmentApi;
import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateAdapter;
import com.github.florent37.camerafragment.listeners.CameraFragmentVideoRecordTextAdapter;
import com.github.florent37.camerafragment.widgets.CameraSettingsView;
import com.github.florent37.camerafragment.widgets.CameraSwitchView;
import com.github.florent37.camerafragment.widgets.FlashSwitchView;
import com.github.florent37.camerafragment.widgets.MediaActionSwitchView;
import com.github.florent37.camerafragment.widgets.RecordButton;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

import static com.github.florent37.camerafragment.internal.ui.BaseAnncaFragment.ARG_CONFIGURATION;

public class UCameraDialog extends FullDialog {

    public static final String FRAGMENT_TAG = "camera";
    private static final int REQUEST_CAMERA_PERMISSIONS = 931;
    private static final int REQUEST_PREVIEW_CODE = 1001;

    /* UI */

    /*@BindView(R.id.settings_view)*/
    CameraSettingsView mSettingsView;

    @BindView(R.id.flash_switch_view)
    FlashSwitchView mFlashSwitchView;

    @BindView(R.id.front_back_camera_switcher)
    CameraSwitchView mCameraSwitchView;

    @BindView(R.id.record_button)
    RecordButton mRecordButton;

    @BindView(R.id.photo_video_camera_switcher)
    MediaActionSwitchView mMediaActionSwitchView;

    @BindView(R.id.record_duration_text)
    TextView mRecordDurationText;

    @BindView(R.id.record_size_mb_text)
    TextView mRecordSizeText;

    @BindView(R.id.cameraLayout)
    View mCameraLayout;

    @BindView(R.id.addCameraButton)
    View mAddCameraButton;

    /* DI */

    IToastManager mIToastManager;

    public static void show(@NonNull FragmentManager fm, Configuration configuration, int requestCode) {
        final UCameraDialog dialog = new UCameraDialog();

        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_CONFIGURATION, configuration);
        bundle.putInt(Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, UCameraDialog.class.getName());
    }

    public static void show(@NonNull FragmentManager fm, Configuration configuration, int requestCode, Bundle bundle) {
        final UCameraDialog dialog = new UCameraDialog();

        bundle.putSerializable(ARG_CONFIGURATION, configuration);
        bundle.putInt(Args.REQUEST_CODE, requestCode);
        dialog.setArguments(bundle);

        dialog.show(fm, UCameraDialog.class.getName());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        setContentView(R.layout.fr_dialog_camera);
        setFullVisual(false);
        setTitleText(UCameraDialog.class.getSimpleName());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.flash_switch_view)
    public void onFlashSwitcClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.toggleFlashMode();
        }
    }

    @OnClick(R.id.front_back_camera_switcher)
    public void onSwitchCameraClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.switchCameraTypeFrontBack();
        }
    }

    @OnClick(R.id.record_button)
    public void onRecordButtonClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.takePhotoOrCaptureVideo(new CameraFragmentResultAdapter() {
                                                       @Override
                                                       public void onVideoRecorded(String filePath) {
                                                           mIToastManager.showShortToast(TypeMessage.Success,"onVideoRecorded");
                                                       }

                                                       @Override
                                                       public void onPhotoTaken(byte[] bytes, String filePath) {
                                                           mIToastManager.showShortToast(TypeMessage.Success,"onPhotoTaken");
                                                       }
                                                   },
                    "/storage/self/primary",
                    "photo0");
        }
    }

    /*@OnClick(R.id.settings_view)
    public void onSettingsClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.openSettingDialog();
        }
    }*/

    @OnClick(R.id.photo_video_camera_switcher)
    public void onMediaActionSwitchClicked() {
        final CameraFragmentApi cameraFragment = getCameraFragment();
        if (cameraFragment != null) {
            cameraFragment.switchActionPhotoVideo();
        }
    }

    @SuppressLint("MissingPermission")
    @OnClick(R.id.addCameraButton)
    public void onAddCameraClicked() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {
                    Manifest.permission.CAMERA,
//                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            /*final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CAMERA_PERMISSIONS);
            } else {
                addCamera();
            }*/

            addCamera();
        } else {
            addCamera();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length != 0) {
            addCamera();
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void addCamera() {
        mAddCameraButton.setVisibility(View.GONE);
        mCameraLayout.setVisibility(View.VISIBLE);

//        final CameraFragment cameraFragment = CameraFragment.newInstance(new Configuration.Builder().setCamera(Configuration.CAMERA_FACE_REAR).build());
//        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        fragmentTransaction
//                .replace(R.id.content, cameraFragment, FRAGMENT_TAG)
//                .commitAllowingStateLoss();
//
//        if (cameraFragment != null) {
            //cameraFragment.setResultListener(new CameraFragmentResultListener() {
            //    @Override
            //    public void onVideoRecorded(String filePath) {
            //        Intent intent = PreviewActivity.newIntentVideo(SignActivity.this, filePath);
            //        startActivityForResult(intent, REQUEST_PREVIEW_CODE);
            //    }
//
            //    @Override
            //    public void onPhotoTaken(byte[] bytes, String filePath) {
            //        Intent intent = PreviewActivity.newIntentPhoto(SignActivity.this, filePath);
            //        startActivityForResult(intent, REQUEST_PREVIEW_CODE);
            //    }
            //});

            /*cameraFragment.setStateListener(new CameraFragmentStateAdapter() {

                @Override
                public void onCurrentCameraBack() {
                    mCameraSwitchView.displayBackCamera();
                }

                @Override
                public void onCurrentCameraFront() {
                    mCameraSwitchView.displayFrontCamera();
                }

                @Override
                public void onFlashAuto() {
                    mFlashSwitchView.displayFlashAuto();
                }

                @Override
                public void onFlashOn() {
                    mFlashSwitchView.displayFlashOn();
                }

                @Override
                public void onFlashOff() {
                    mFlashSwitchView.displayFlashOff();
                }

                @Override
                public void onCameraSetupForPhoto() {
                    mMediaActionSwitchView.displayActionWillSwitchVideo();

                    mRecordButton.displayPhotoState();
                    mFlashSwitchView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCameraSetupForVideo() {
                    mMediaActionSwitchView.displayActionWillSwitchPhoto();

                    mRecordButton.displayVideoRecordStateReady();
                    mFlashSwitchView.setVisibility(View.GONE);
                }

                @Override
                public void shouldRotateControls(int degrees) {
                    ViewCompat.setRotation(mCameraSwitchView, degrees);
                    ViewCompat.setRotation(mMediaActionSwitchView, degrees);
                    ViewCompat.setRotation(mFlashSwitchView, degrees);
                    ViewCompat.setRotation(mRecordDurationText, degrees);
                    ViewCompat.setRotation(mRecordSizeText, degrees);
                }

                @Override
                public void onRecordStateVideoReadyForRecord() {
                    mRecordButton.displayVideoRecordStateReady();
                }

                @Override
                public void onRecordStateVideoInProgress() {
                    mRecordButton.displayVideoRecordStateInProgress();
                }

                @Override
                public void onRecordStatePhoto() {
                    mRecordButton.displayPhotoState();
                }

                @Override
                public void onStopVideoRecord() {
                    mRecordSizeText.setVisibility(View.GONE);
                    //mCameraSwitchView.setVisibility(View.VISIBLE);
                    mSettingsView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onStartVideoRecord(File outputFile) {
                }
            });

            cameraFragment.setControlsListener(new CameraFragmentControlsAdapter() {
                @Override
                public void lockControls() {
                    mCameraSwitchView.setEnabled(false);
                    mRecordButton.setEnabled(false);
                    mSettingsView.setEnabled(false);
                    mFlashSwitchView.setEnabled(false);
                }

                @Override
                public void unLockControls() {
                    mCameraSwitchView.setEnabled(true);
                    mRecordButton.setEnabled(true);
                    mSettingsView.setEnabled(true);
                    mFlashSwitchView.setEnabled(true);
                }

                @Override
                public void allowCameraSwitching(boolean allow) {
                    mCameraSwitchView.setVisibility(allow ? View.VISIBLE : View.GONE);
                }

                @Override
                public void allowRecord(boolean allow) {
                    mRecordButton.setEnabled(allow);
                }

                @Override
                public void setMediaActionSwitchVisible(boolean visible) {
                    mMediaActionSwitchView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
                }
            });

            cameraFragment.setTextListener(new CameraFragmentVideoRecordTextAdapter() {
                @Override
                public void setRecordSizeText(long size, String text) {
                    mRecordSizeText.setText(text);
                }

                @Override
                public void setRecordSizeTextVisible(boolean visible) {
                    mRecordSizeText.setVisibility(visible ? View.VISIBLE : View.GONE);
                }

                @Override
                public void setRecordDurationText(String text) {
                    mRecordDurationText.setText(text);
                }

                @Override
                public void setRecordDurationTextVisible(boolean visible) {
                    mRecordDurationText.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });*/
//        }
    }

    private CameraFragmentApi getCameraFragment() {
        return (CameraFragmentApi) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
    }


    public interface Args {
        String REQUEST_CODE = "REQUEST_CODE";
    }
}
