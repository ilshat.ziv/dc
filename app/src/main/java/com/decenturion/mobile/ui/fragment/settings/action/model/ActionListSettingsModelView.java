package com.decenturion.mobile.ui.fragment.settings.action.model;

import java.util.List;

public class ActionListSettingsModelView {

    private List<ActionModel> mActionList;

    public ActionListSettingsModelView() {
    }

    public ActionListSettingsModelView(List<ActionModel> d) {
        mActionList = d;
    }

    public List<ActionModel> getActionList() {
        return mActionList;
    }

}
