package com.decenturion.mobile.business.settings.delivery.fill;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.FillDeliverySettingsModelView;

import rx.Observable;

public interface IFillDeliverySettingsInteractor {

    Observable<FillDeliverySettingsModelView> getDeliveryData();

    Observable<DeliveryPassportResult> setDeliveryData(@NonNull String phone,
                                                       @NonNull String address,
                                                       @NonNull String city,
                                                       @NonNull String state,
                                                       @NonNull String country,
                                                       @NonNull String zipcode);

    Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap photo);
}
