package com.decenturion.mobile.ui.fragment.validate.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.validate.ValidateEmailComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.sign.ISignActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.sign.in.view.SigninFragment;
import com.decenturion.mobile.ui.fragment.validate.model.ResendEmailModelView;
import com.decenturion.mobile.ui.fragment.validate.presenter.IValidEmailPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ValidateEmailFragment extends BaseFragment implements IValidEmailView,
        View.OnClickListener {

    public static final String EMAIL = "EMAIL";
    private String mTokenConfirm;
    private String mEmail;

    /* UI */

    @BindView(R.id.textView)
    TextView mValidateText;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IValidEmailPresenter mIValidEmailPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
        setHasOptionsMenu(true);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenConfirm = arguments.getString(SignActivity.CONFIRM_EMAIL_TOKEN, "");
            mEmail = arguments.getString(EMAIL, "");
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ValidateEmailComponent validateEmailComponent = appDelegate
                .getIDIManager()
                .plusValidateEmailComponent();
        validateEmailComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_validate, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_confirmation_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIValidEmailPresenter.bindView(this);
        mIValidEmailPresenter.bindViewData(mEmail);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIValidEmailPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIValidEmailPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIValidEmailPresenter.unbindView();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_in);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_confirmation_signin));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_confirm_email, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_in : {
                ISignActivity activity = (ISignActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(SigninFragment.class);

                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @OnClick(R.id.resendButtonView)
    protected void onResendEmail() {
        mIValidEmailPresenter.resendEmail();
    }

    @Override
    public void onBindViewData(@NonNull ResendEmailModelView model) {
        String localeString = mILocalistionManager.getLocaleString(R.string.app_confirmation_text);
        String username = model.getEmail();
        localeString = localeString.replace("{email}", username);
        Spanned text = Html.fromHtml(localeString);
        mValidateText.setText(text);

        if (!TextUtils.isEmpty(mTokenConfirm)) {
            mIValidEmailPresenter.confirmEmail(mTokenConfirm);
        }
    }

    @Override
    public void onConfirmedEmail() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, PassportActivity.class);
        intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
        startActivity(intent);
        activity.finish();
    }

    @Override
    public void onSignin() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(SigninFragment.class);
    }

    @Override
    public void onClick(View v) {
        try {
            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity.getINavigationManager();
            iNavigationManager.navigateToBack();
        } catch (Exception e) {
            // TODO: 25.09.2018 надо ловить в Crashlytics и чинить
            LoggerUtils.exception(e);
        }
    }
}
