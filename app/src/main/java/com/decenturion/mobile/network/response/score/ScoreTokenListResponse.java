package com.decenturion.mobile.network.response.score;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScoreTokenListResponse extends BaseResponse {

    @JsonProperty("result")
    private ScoreTokenListResult mResult;

    public ScoreTokenListResponse() {
    }

    @JsonIgnore
    public ScoreTokenListResult getResult() {
        return mResult;
    }
}
