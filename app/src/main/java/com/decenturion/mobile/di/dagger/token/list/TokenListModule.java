package com.decenturion.mobile.di.dagger.token.list;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.token.ITokenInteractor;
import com.decenturion.mobile.business.token.TokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter.IScoreTokenListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter.ScoreTokenListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TokenListModule {

    @Provides
    @TokenListScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TokenListScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @TokenListScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TokenListScope
    ITokenInteractor provideITokenInteractor(IResidentClient IResidentClient,
                                             ITokenClient iTokenClient,
                                             IScoreClient iScoreClient,
                                             IResidentRepository iResidentRepository,
                                             IWalletRepository iWalletRepository,
                                             ITokenRepository iTokenRepository,
                                             IPrefsManager iPrefsManager) {

        return new TokenInteractor(
                IResidentClient,
                iTokenClient,
                iScoreClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository,
                iPrefsManager
        );
    }

//    @Provides
//    @TokenListScope
//    ITokenListPresenter provideITokenListPresenter(ITokenInteractor iTokenInteractor) {
//        return new TokenListPresenter(iTokenInteractor);
//    }
}
