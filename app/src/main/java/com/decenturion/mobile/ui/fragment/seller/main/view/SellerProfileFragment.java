package com.decenturion.mobile.ui.fragment.seller.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.di.dagger.seller.profile.SellerProfileComponent;
import com.decenturion.mobile.ui.activity.ISimpleActivity;
import com.decenturion.mobile.ui.dialog.bottomsheet.SellerMoreActionDialogFragment;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.collaps.ProfileCollapsAppBarFragment;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.seller.SellerProfileAdapter;
import com.decenturion.mobile.ui.fragment.seller.main.model.SellerProfileModelView;
import com.decenturion.mobile.ui.fragment.seller.main.presenter.ISellerProfilePresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class SellerProfileFragment extends BaseFragment implements ISellerProfileView,
        SellerMoreActionDialogFragment.OnMoreActionDialogListener,
        View.OnClickListener {

    public static final String SELLER_UUID = "SELLER_UUID";

    private String mResidentUuid;

    /* UI */

    @BindView(R.id.pagerView)
    ViewPager mPagerView;

    /* DI */

    @Inject
    IPrefsManager mIPrefsManager;

    @Inject
    IResourceManager mIResourceManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISellerProfilePresenter mIProfilePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mResidentUuid = arguments.getString(SELLER_UUID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SellerProfileComponent sellerProfileComponent = appDelegate
                .getIDIManager()
                .plusSellerProfileComponent();
        sellerProfileComponent.inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_profile_v2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        FragmentManager fm = getChildFragmentManager();
        SellerProfileAdapter adapter = new SellerProfileAdapter(fm, mILocalistionManager, getArguments());
        mPagerView.setAdapter(adapter);
    }

    private void initToolbar() {
        ISimpleActivity activity = (ISimpleActivity) requireActivity();
        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.VISIBLE);
        tabLayoutView.setupWithViewPager(mPagerView);
    }

    @Override
    public void onBindViewData(@NonNull SellerProfileModelView model) {
        ISimpleActivity activity = (ISimpleActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setSubTitle(model.getStatus());

        initPhotoUi(model);
    }

    private void initPhotoUi(@NonNull SellerProfileModelView model) {
        ProfileCollapsAppBarFragment fragment = new ProfileCollapsAppBarFragment();

        Bundle bundle = new Bundle();
        PhotoModelView photoModelView = model.getPhotoModelView();
        if (photoModelView == null) {
            bundle.putString(ProfileCollapsAppBarFragment.URL_PHOTO, "");
            bundle.putString(ProfileCollapsAppBarFragment.URL_MIN_PHOTO, "");
        } else {
            bundle.putString(ProfileCollapsAppBarFragment.URL_PHOTO, photoModelView.getOriginal());
            bundle.putString(ProfileCollapsAppBarFragment.URL_MIN_PHOTO, photoModelView.getMin());
        }
        fragment.setArguments(bundle);

        ISimpleActivity activity = (ISimpleActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setCollapseContentView(fragment, "ProfileCollapsAppBarFragment");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIProfilePresenter.bindView(this);
        mIProfilePresenter.bindViewData(mResidentUuid);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIProfilePresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIProfilePresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIProfilePresenter.unbindView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fr_seller_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_more : {
                SellerMoreActionDialogFragment dialog = new SellerMoreActionDialogFragment();
                dialog.setOnMoreActionDialogListener(this);
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                dialog.show(fragmentManager, dialog.getTag());
                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onCopyProfileLink() {
        mIProfilePresenter.onCopyProfileLink(mResidentUuid);
    }

    @Override
    public void onShare() {
        mIProfilePresenter.onShare(mResidentUuid);
    }

    @Override
    public void onShare(@NonNull String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    @Override
    public void onClick(View v) {
        ISimpleActivity activity = (ISimpleActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
