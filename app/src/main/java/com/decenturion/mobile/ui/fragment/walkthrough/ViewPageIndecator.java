package com.decenturion.mobile.ui.fragment.walkthrough;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.decenturion.mobile.R;

import java.util.ArrayList;
import java.util.List;

public class ViewPageIndecator implements ViewPager.OnPageChangeListener {

    private final PagerAdapter mPagerAdapter;
    private final LinearLayout mPageIndecator;

    private final List<ImageView> mDotViewList;
    private final Context mContext;
    private final ViewPager mViewPager;
    private int mCurentItem;

    public ViewPageIndecator(@NonNull Context context,
                             @NonNull ViewPager ViewPager,
                             @NonNull LinearLayout pagerIndicator) {

        this.mContext = context;
        this.mViewPager = ViewPager;
        this.mPagerAdapter = ViewPager.getAdapter();
        pagerIndicator.removeAllViews();
        this.mPageIndecator = pagerIndicator;
        assert mPagerAdapter != null;
        int dotsCount = mPagerAdapter.getCount();
        mDotViewList = new ArrayList<>();
        for (int i = 0; i < dotsCount; i++) {
            ImageView imageView = AddDot(context);
            mDotViewList.add(imageView);

        }

        if (dotsCount > 0) {
            int currentItem = mViewPager.getCurrentItem();
            setCurentItem(currentItem);
        }
        mViewPager.addOnPageChangeListener(this);
    }

    @NonNull
    private ImageView AddDot(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.pager_dot_item_nonselected));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(8, 0, 8, 0);
        mPageIndecator.addView(imageView, params);
        return imageView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        setCurentItem(position);
    }

    private void setCurentItem(int position) {

        ImageView imageView = mDotViewList.get(mCurentItem);
        imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.pager_dot_item_nonselected));

        mDotViewList.get(position).setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.pager_dot_item_selected));
        this.mCurentItem = position;
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void next() {
        mViewPager.setCurrentItem((mViewPager.getCurrentItem() < mDotViewList.size())
                ? mViewPager.getCurrentItem() + 1 : 0);
    }

    public void addNew() {
        ImageView imageView = AddDot(mContext);
        mDotViewList.add(imageView);
    }

    public void show(boolean show) {
        mPageIndecator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void removeItem(int postition) {
        if (mCurentItem == postition && postition != 0) {
            setCurentItem(postition - 1);
        }
        mDotViewList.remove(postition);
    }
}
