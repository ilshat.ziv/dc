package com.decenturion.mobile.ui.fragment.settings.delivery.pay.model;

import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;

public class DeliveryDataModelView {

    private String mPhone;
    private String mAddress;
    private String mCountry;
    private String mCity;
    private String mState;
    private String mZip;

    public DeliveryDataModelView() {
    }

    public DeliveryDataModelView(Delivery delivery) {
        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCountry = delivery.getCountry();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mZip = delivery.getZip();
    }

    public String getPhone() {
        return mPhone;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    public String getZip() {
        return mZip;
    }

    public void setData(DeliveryPassportResult data) {
        Passport passport = data.getPassport();
        Delivery deliveryObject = passport.getDelivery();
        mPhone = deliveryObject.getPhone();
        mAddress = deliveryObject.getAddress();
        mCountry = deliveryObject.getCountry();
        mCity = deliveryObject.getCity();
        mState = deliveryObject.getState();
        mZip = deliveryObject.getZip();
    }
}
