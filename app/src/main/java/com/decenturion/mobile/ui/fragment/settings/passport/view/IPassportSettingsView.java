package com.decenturion.mobile.ui.fragment.settings.passport.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PassportSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.SexModelView;

public interface IPassportSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull SexModelView model,
                        @NonNull CountryModelView model1,
                        @NonNull PassportSettingsModelView model2);

    void onSavedChanges();
}
