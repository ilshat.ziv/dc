package com.decenturion.mobile.business.score.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.score.details.model.ScoreTokenDetailsModelView;

import rx.Observable;

public interface IScoreTokenDetailsInteractor {

    Observable<ScoreTokenDetailsModelView> getTokenDetails(int tokenId);

    Observable<ScoreTokenDetailsModelView> getTokenDetails(@NonNull ScoreTokenDetailsModelView modelView);
}
