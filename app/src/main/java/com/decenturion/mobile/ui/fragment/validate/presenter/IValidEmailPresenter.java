package com.decenturion.mobile.ui.fragment.validate.presenter;

import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.validate.view.IValidEmailView;

public interface IValidEmailPresenter extends IPresenter<IValidEmailView> {

    void resendEmail();

    void confirmEmail(String token);

    void bindViewData(@Nullable String email);
}
