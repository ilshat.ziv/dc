package com.decenturion.mobile.ui.fragment.invite.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class Token implements ISpinnerItem {

    private String mName;
    private String mCode;

    public Token() {}

    public Token(String name, String code) {
        mName = name;
        mCode = code;
    }

    @Override
    public String getItemName() {
        return mName;
    }

    @Override
    public String getItemKey() {
        return mCode;
    }
}