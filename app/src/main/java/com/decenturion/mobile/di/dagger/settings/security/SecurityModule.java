package com.decenturion.mobile.di.dagger.settings.security;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.security.ISecurityInteractor;
import com.decenturion.mobile.business.settings.security.SecurityInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.security.presenter.ISecurityPresenter;
import com.decenturion.mobile.ui.fragment.settings.security.presenter.SecurityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SecurityModule {

    @Provides
    @SecurityScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SecurityScope
    ISecurityInteractor provideISecurityInteractor(IResidentClient iResidentClient,
                                                   IResidentRepository iResidentRepository) {
        return new SecurityInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @SecurityScope
    ISecurityPresenter provideISecurityPresenter(ISecurityInteractor iSecurityInteractor,
                                                 ILocalistionManager iLocalistionManager) {
        return new SecurityPresenter(iSecurityInteractor, iLocalistionManager);
    }
}
