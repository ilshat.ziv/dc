package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmPhotoDAO extends BaseDao<OrmPhotoModel, Integer> {

    public OrmPhotoDAO(ConnectionSource connectionSource,
                       Class<OrmPhotoModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
