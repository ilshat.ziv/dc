package com.decenturion.mobile.ui.architecture.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.exception.AppException;
import com.decenturion.mobile.rx.DCompositeSubscription;
import com.decenturion.mobile.rx.IDCompositeSubscription;
import com.decenturion.mobile.ui.architecture.view.IScreenView;
import com.decenturion.mobile.utils.LoggerUtils;

public abstract class Presenter<T extends IScreenView> implements IPresenter<T> {

    protected T mView;

    protected final IDCompositeSubscription mIDCompositeSubscription = new DCompositeSubscription();

    @Override
    public void bindView(@NonNull T view) {
        mView = view;
    }

    @Override
    public void unbindView() {
        mIDCompositeSubscription.clear();
        mView = null;
    }

    @Override
    public void showProgressView() {
        if (mView != null) {
            mView.showProgressView();
        }
    }

    @Override
    public void hideProgressView() {
        if (mView != null) {
            mView.hideProgressView();
        }
    }

    @Override
    public void showMessage(@NonNull String message) {
        if (mView != null) {
            mView.showMessage(message);
        }
    }

    @Override
    public void showSuccessView(@NonNull String message) {
        if (mView != null) {
            mView.showSuccessMessage(message);
        }
    }

    @Override
    public void showErrorView(Throwable t) {
        LoggerUtils.exception(t);

        if (mView != null
                && t instanceof AppException) {
            mView.showErrorMessage(t.getMessage());
        }
    }

    @Override
    public void showErrorView(@NonNull String m) {
        LoggerUtils.exception(new RuntimeException(m));

        if (mView != null) {
            mView.showErrorMessage(m);
        }
    }

    @Override
    public void showWarningView(Throwable t) {
        if (mView != null) {
            mView.showWarningMessage(t.getMessage());
        }
    }

    @Override
    public void showWarningView(@NonNull String m) {
        if (mView != null) {
            mView.showWarningMessage(m);
        }
    }

    @Override
    public void onDestroy() {
        mIDCompositeSubscription.clear();
    }
}
