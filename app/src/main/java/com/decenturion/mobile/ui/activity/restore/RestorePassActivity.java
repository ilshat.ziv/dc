package com.decenturion.mobile.ui.activity.restore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.pass.restore.view.RestorePassFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class RestorePassActivity extends SingleFragmentActivity {

    public static final String CONTACT = "CONTACT";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
            return;
        }
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        iNavigationManager.navigateTo(RestorePassFragment.class, bundle);
    }
}
