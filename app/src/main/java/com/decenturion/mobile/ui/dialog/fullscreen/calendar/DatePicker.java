package com.decenturion.mobile.ui.dialog.fullscreen.calendar;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DatePicker extends LinearLayout implements android.widget.DatePicker.OnDateChangedListener {

    @Nullable
    @BindView(R.id.yearView)
    TextView mYearView;

    @Nullable
    @BindView(R.id.dayView)
    TextView mDayView;

    @Nullable
    @BindView(R.id.datePickerView)
    android.widget.DatePicker mDatePickerView;

    private Unbinder mUnbinder;

    private int mDay;
    private int mMonth;
    private int mYear;

    private OnChangeDateListener mOnDateChangeListener;

    public DatePicker(Context context) {
        super(context);
    }

    public DatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected boolean isInitView() {
        return  (mYearView != null && mDayView != null && mDatePickerView != null);
    }

    protected void init() {
        if (isInitView()) {
            mDatePickerView.invalidate();
            return;
        }
        bindView();
        if (!isInitView()) {
            return;
        }

        if (mDay == 0) {
            mDay = mDatePickerView.getDayOfMonth();
            mMonth = mDatePickerView.getMonth() + 1;
            mYear = mDatePickerView.getYear();
        }
        mDatePickerView.init(mYear, mMonth - 1, mDay, this);
        mDatePickerView.invalidate();

        hideAndroidHeader();
    }

    private boolean isBuildLayout;

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();

        // TODO not call onDateChange
        if (isInitView() && !isBuildLayout) {
            if (mDay == 0) {
                mDay = mDatePickerView.getDayOfMonth();
            }

            if (mMonth == 0) {
                mMonth = mDatePickerView.getMonth();
                mMonth = (mDay == 0) ? mMonth : mMonth + 1 ;
            }

            if (mYear == 0) {
                mYear = mDatePickerView.getYear();
            }

            mDatePickerView.updateDate(mYear, mMonth - 1, mDay);
            isBuildLayout = true;
        }
    }

    @Override
    public void onDateChanged(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mYear = year;
        mMonth = monthOfYear + 1;
        mDay = dayOfMonth;

        mDatePickerView.invalidate();

        try {
            rebuildLabel();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    private void bindView() {
        mUnbinder = ButterKnife.bind(this, this);
    }

    private void unBindView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    private void hideAndroidHeader() {
        init();

        try {
            ViewGroup viewGroup = (ViewGroup) mDatePickerView.getChildAt(0);
            View view = viewGroup.getChildAt(0);
            view.setVisibility(GONE);

            rebuildLabel();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    private void rebuildLabel() throws ParseException {
        mYearView.setText(String.valueOf(mYear));

        SimpleDateFormat format = new SimpleDateFormat(DateTimeUtils.Format.SIMPLE_FORMAT);
        Date date = format.parse(mDay + "." + mMonth + "." + mYear);

        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtils.Format.TITLE_DATE_PICKER);
        String sdate = dateFormat.format(date);
        mDayView.setText(sdate);

        if (mOnDateChangeListener != null) {
            long time = date.getTime();
            time = DateTimeUtils.convertToGMT(time);
            mOnDateChangeListener.changeData(time);
        }
    }

    public long getDate() {
        SimpleDateFormat format = new SimpleDateFormat(DateTimeUtils.Format.SIMPLE_FORMAT);
        try {
            Date date = format.parse(mDay + "." + mMonth + "." + mYear);
            return date.getTime();
        } catch (ParseException e) {
            LoggerUtils.exception(e);
            return 0;
        }
    }

    public void setOnDateChangeListener(OnChangeDateListener onDateChangeListener) {
        mOnDateChangeListener = onDateChangeListener;
    }

    public void setDateTime(long date) {
        if (date > 0) {
            Date dates = new Date(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dates);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            mMonth = calendar.get(Calendar.MONTH) + 1;
            mYear = calendar.get(Calendar.YEAR);
        }
    }

    public interface OnChangeDateListener {

        /**
         * @param date
         *
         * Данную дату надо перед отправкой конвертировать в дату по GMT
         *
         */

        void changeData(long date);
    }
}
