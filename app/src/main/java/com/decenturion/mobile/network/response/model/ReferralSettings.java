package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralSettings {

    @JsonProperty("available_tokens")
    private String mAvailableTokens;

    @JsonProperty("enabled")
    private boolean isEnable;

    @JsonProperty("costing_method")
    private String mCostingMethod;

    @JsonProperty("fixed_cost")
    private String mFixedCost;

    @JsonProperty("fixed_token_count")
    private int mFixedTokenCount;

    @JsonProperty("fixed_comment")
    private String mFixedComment;

    @JsonProperty("percent")
    private int mPercent;

    @JsonProperty("percent_token_count")
    private int mPercentTokenCount;

    @JsonProperty("percent_comment")
    private String mPercentComment;

    public ReferralSettings() {
    }

    @JsonIgnore
    public void setCostingMethod(String costingMethod) {
        mCostingMethod = costingMethod;
    }

    @JsonIgnore
    public void setFixedCost(String fixedCost) {
        mFixedCost = fixedCost;
    }

    @JsonIgnore
    public void setFixedTokenCount(int fixedTokenCount) {
        mFixedTokenCount = fixedTokenCount;
    }

    @JsonIgnore
    public void setFixedComment(String fixedComment) {
        mFixedComment = fixedComment;
    }

    @JsonIgnore
    public void setPercent(int percent) {
        mPercent = percent;
    }

    @JsonIgnore
    public void setPercentTokenCount(int percentTokenCount) {
        mPercentTokenCount = percentTokenCount;
    }

    @JsonIgnore
    public void setPercentComment(String percentComment) {
        mPercentComment = percentComment;
    }

    @JsonIgnore
    public String getAvailableTokens() {
        return mAvailableTokens;
    }

    @JsonIgnore
    public boolean isEnable() {
        return isEnable;
    }

    @JsonIgnore
    public String getCostingMethod() {
        return mCostingMethod;
    }

    @JsonIgnore
    public String getFixedCost() {
        return mFixedCost;
    }

    @JsonIgnore
    public int getFixedTokenCount() {
        return mFixedTokenCount;
    }

    @JsonIgnore
    public String getFixedComment() {
        return mFixedComment;
    }

    @JsonIgnore
    public int getPercent() {
        return mPercent;
    }

    @JsonIgnore
    public int getPercentTokenCount() {
        return mPercentTokenCount;
    }

    @JsonIgnore
    public String getPercentComment() {
        return mPercentComment;
    }
}
