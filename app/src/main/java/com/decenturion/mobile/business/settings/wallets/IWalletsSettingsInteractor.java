package com.decenturion.mobile.business.settings.wallets;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletsSettingsModelView;

import rx.Observable;

public interface IWalletsSettingsInteractor {

    Observable<Boolean> saveWallets(@NonNull String btcAddress,
                                    @NonNull String ethAddress,
                                    @NonNull String twofacode);

    Observable<WalletsSettingsModelView> getWalletsInfo();
}
