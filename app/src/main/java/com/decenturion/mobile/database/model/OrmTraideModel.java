package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.type.TransferType;
import com.decenturion.mobile.database.type.model.TransferTypeModel;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_traide_model")
public class OrmTraideModel {

    private final static String ID = "id";
    public final static String UUID = "uuid";

    private final static String AMOUNT = "amount";
    private final static String CATEGORY = "category";
    private final static String CURRENCY = "currency";
    private final static String EXPIRED_AT = "expired_at";
    private final static String FRONTEND_ID = "frontend_id";
    private final static String INTERNAL = "internal";
    private final static String PRICE = "price";
    private final static String RECEIVE_ADDRESS = "receive_address";
    private final static String REFUND_ADDRESS = "refund_address";
    private final static String STATUS = "status";
    private final static String TOTAL_PRICE = "total_price";
    private final static String TRADE_ADDRESS = "trade_address";
    private final static String TXID = "txid";
    private final static String COIN = "coin";

    private static final String TRANSFER = "transfer";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = UUID)
    private String mUuid;

    @DatabaseField(dataType = DataType.STRING, columnName = AMOUNT)
    private String mAmount;

    @DatabaseField(dataType = DataType.STRING, columnName = CATEGORY)
    private String mCategory;

    @DatabaseField(dataType = DataType.STRING, columnName = CURRENCY)
    private String mCurrency;

    @DatabaseField(dataType = DataType.STRING, columnName = EXPIRED_AT)
    private String mExpiredAt;

    @DatabaseField(dataType = DataType.STRING, columnName = FRONTEND_ID)
    private String mFrontendId;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = INTERNAL)
    private boolean isInternal;

    @DatabaseField(dataType = DataType.STRING, columnName = PRICE)
    private String mPrice;

    @DatabaseField(dataType = DataType.STRING, columnName = RECEIVE_ADDRESS)
    private String mReceiveAddress;

    @DatabaseField(dataType = DataType.STRING, columnName = REFUND_ADDRESS)
    private String mRefundAddress;

    @DatabaseField(dataType = DataType.STRING, columnName = STATUS)
    private String mStatus;

    @DatabaseField(dataType = DataType.STRING, columnName = TOTAL_PRICE)
    private String mTotalPrice;

    @DatabaseField(dataType = DataType.STRING, columnName = TRADE_ADDRESS)
    private String mTradeAddress;

    @DatabaseField(dataType = DataType.STRING, columnName = TXID)
    private String mTxid;

    @DatabaseField(dataType = DataType.STRING, columnName = COIN)
    private String mCoin;

    @DatabaseField(persisterClass = TransferType.class, columnName = TRANSFER)
    private TransferTypeModel mTransferTypeModel;

    public OrmTraideModel() {
    }

    public OrmTraideModel(CreateTraideResult d) {
        mUuid = d.getUuid();
        mAmount = d.getAmount();
        mCategory = d.getCategory();
        mCurrency = d.getCurrency();
        mExpiredAt = d.getCreatedAt();
        mFrontendId = d.getFrontendId();
        isInternal = d.isInternal();
        mPrice = d.getPrice();
        mReceiveAddress = d.getReceiveAddress();
        mRefundAddress = d.getRefundAddress();
        mStatus = d.getStatus();
        mTotalPrice = d.getTotalPrice();
        mTradeAddress = d.getTradeAddress();
        mTxid = d.getTxid();
        mCoin = d.getCoin();
    }

    public OrmTraideModel(@NonNull WrapperTrade model) {
        Trade d = model.getTrade();
        mUuid = d.getUuid();
        mAmount = d.getAmount();
        mCategory = d.getCategory();
        mCurrency = d.getCurrency();
        mExpiredAt = d.getCreatedAt();
        mFrontendId = d.getFrontendId();
        isInternal = d.isInternal();
        mPrice = d.getPrice();
        mReceiveAddress = d.getReceiveAddress();
        mRefundAddress = d.getRefundAddress();
        mStatus = d.getStatus();
        mTotalPrice = d.getTotalPrice();
        mTradeAddress = d.getTradeAddress();
        mTxid = d.getTxid();
        mCoin = d.getCoin();

        mTransferTypeModel = new TransferTypeModel(model.getTransfer());
    }

    public String getUuid() {
        return mUuid;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public String getExpiredAt() {
        return mExpiredAt;
    }

    public String getFrontendId() {
        return mFrontendId;
    }

    public boolean isInternal() {
        return isInternal;
    }

    public String getPrice() {
        return mPrice;
    }

    public String getReceiveAddress() {
        return mReceiveAddress;
    }

    public String getRefundAddress() {
        return mRefundAddress;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getTotalPrice() {
        return mTotalPrice;
    }

    public String getTradeAddress() {
        return mTradeAddress;
    }

    public String getTxid() {
        return mTxid;
    }

    public TransferTypeModel getTransfer() {
        return mTransferTypeModel;
    }

    public String getCoin() {
        return mCoin;
    }
}
