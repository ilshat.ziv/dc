package com.decenturion.mobile.ui.fragment.score.details.model;

public class ScoreTokenParamsModelView {

    private String mName;
    private String mValue;
    private int mIconRes;

    public ScoreTokenParamsModelView(String name, String value, int iconRes) {
        mName = name;
        mValue = value;
        mIconRes = iconRes;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }

    public int getIconRes() {
        return mIconRes;
    }
}
