package com.decenturion.mobile.di.dagger.profile;

import com.decenturion.mobile.ui.fragment.profile_v2.main.view.ProfileFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ProfileModule.class
})

@ProfileScope
public interface ProfileComponent {

    void inject(ProfileFragment view);
}
