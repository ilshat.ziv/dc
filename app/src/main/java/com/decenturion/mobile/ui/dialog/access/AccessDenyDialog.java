package com.decenturion.mobile.ui.dialog.access;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;
import com.decenturion.mobile.ui.widget.LinkTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccessDenyDialog extends SingleDialogFragment implements LinkTextView.OnLinkTextViewListener {

    @BindView(R.id.messageView)
    LinkTextView mMessageView;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        DIManager idiManager = (DIManager) appDelegate.getIDIManager();
        idiManager.getAppComponent().inject(this);
    }


    public static void show(@NonNull FragmentManager fragmentManager) {
        AccessDenyDialog dialog = new AccessDenyDialog();
        dialog.show(fragmentManager, AccessDenyDialog.class.getSimpleName());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_access_deny_title));
        setNegativeTextString(mILocalistionManager.getLocaleString(R.string.app_access_deny_settings));
        setPositiveTextString(mILocalistionManager.getLocaleString(R.string.app_constants_close));

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_access_deny);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());

        setupUi();

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        return arbitrationRequestDialog;
    }

    private void setupUi() {
        String localeString = mILocalistionManager.getLocaleString(R.string.app_access_deny_text);
        localeString = localeString.replace("{host}", BuildConfig.HOST);
        mMessageView.setLinkText(localeString);

        mMessageView.setOnLinkClickListener(this);
    }

    @Override
    public void onPositiveClickButton() {
        dismiss();
    }

    @Override
    public void onNegativeClickButton() {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onLinkClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.HOST));
        startActivity(browserIntent);
        dismiss();
    }

    @Override
    public void onTextClick() {

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Process.killProcess(Process.myPid());
    }
}
