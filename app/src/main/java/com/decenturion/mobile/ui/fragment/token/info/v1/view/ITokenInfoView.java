package com.decenturion.mobile.ui.fragment.token.info.v1.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.info.v1.model.TokenInfoModelView;

public interface ITokenInfoView extends IScreenFragmentView {

    void onBindViewData(@NonNull TokenInfoModelView model);
}
