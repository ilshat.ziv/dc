package com.decenturion.mobile.app.lifecycle;

public interface IAppLifecycleManager {

    boolean isAppForeground();

    void addOnAppLifecycleStateListener(OnAppLifecycleStateListener listener);

    void removeOnAppLifecycleStateListener(OnAppLifecycleStateListener listener);

}
