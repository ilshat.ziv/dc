package com.decenturion.mobile.network.response.reset;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetPasswordResponse extends BaseResponse {

    @JsonProperty("result")
    private ResetPasswordResult mResult;

    public ResetPasswordResponse() {
    }

    @JsonIgnore
    public ResetPasswordResult getResult() {
        return mResult;
    }
}
