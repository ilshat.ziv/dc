package com.decenturion.mobile.network.response.support;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportResponse extends BaseResponse {

    @JsonProperty("result")
    private SupportResult mResult;

    public SupportResponse() {
    }

    @JsonIgnore
    public SupportResult getResult() {
        return mResult;
    }
}
