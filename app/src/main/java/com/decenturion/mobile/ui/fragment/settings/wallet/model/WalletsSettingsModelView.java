package com.decenturion.mobile.ui.fragment.settings.wallet.model;

import android.text.TextUtils;

import com.decenturion.mobile.network.response.model.Wallet;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

public class WalletsSettingsModelView {

    private WalletModel mBtcWallet;
    private WalletModel mEthWallet;
    private String mSecretCode;

    public WalletsSettingsModelView() {
    }

    public WalletsSettingsModelView(List<Wallet> list) {
        if (!CollectionUtils.isEmpty(list)) {
            for (Wallet wallet : list) {
                String coin = wallet.getCoin();
                if (TextUtils.equals(coin, "btc")) {
                    mBtcWallet = new WalletModel(wallet.getAddress(), coin);
                } else if (TextUtils.equals(coin, "eth")) {
                    mEthWallet = new WalletModel(wallet.getAddress(), coin);
                }
            }
        }

    }

    public void setBtcWallet(WalletModel btcWallet) {
        mBtcWallet = btcWallet;
    }

    public void setEthWallet(WalletModel ethWallet) {
        mEthWallet = ethWallet;
    }

    public void setSecretCode(String secretCode) {
        mSecretCode = secretCode;
    }

    public String getBtcWaller() {
        return mBtcWallet == null ? "" : mBtcWallet.getAddress();
    }

    public String getEthWallet() {
        return mEthWallet == null ? "" : mEthWallet.getAddress();
    }

    public String getSecretCode() {
        return mSecretCode;
    }
}
