package com.decenturion.mobile.ui.fragment.ministry.market.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.ministry.market.view.IMinistryView;

public interface IMinistryPresenter extends IPresenter<IMinistryView> {

    void bindViewData();

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
