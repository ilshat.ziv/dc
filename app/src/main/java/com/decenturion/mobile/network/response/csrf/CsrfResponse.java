package com.decenturion.mobile.network.response.csrf;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CsrfResponse extends BaseResponse {

    @JsonProperty("result")
    private CsrfResult mResult;

    public CsrfResponse() {
    }

    @JsonIgnore
    public CsrfResult getResult() {
        return mResult;
    }
}
