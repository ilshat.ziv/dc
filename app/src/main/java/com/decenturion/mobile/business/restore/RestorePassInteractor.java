package com.decenturion.mobile.business.restore;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.ResetPasswordModel;
import com.decenturion.mobile.network.request.RestorePassModel;
import com.decenturion.mobile.network.response.reset.ResetPasswordResult;
import com.decenturion.mobile.network.response.restore.RestorePassResponse;
import com.decenturion.mobile.network.response.signin.SigninResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class RestorePassInteractor implements IRestorePassInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public RestorePassInteractor(IResidentClient IResidentClient,
                                 IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<RestorePassResponse> restorePass(@NonNull String contact, String captcha) {
        RestorePassModel model = new RestorePassModel();
        model.setUsername(contact);
        model.setCaptcha(captcha);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.restorePass(m));
    }

    @Override
    public Observable<ResetPasswordResult> resetPass(@NonNull String token,
                                        @NonNull String newPassword,
                                        @NonNull String retypePassword) {
        ResetPasswordModel m = new ResetPasswordModel();
        m.setPassword(newPassword);
        m.setToken(token);
        return mIResidentClient.resetPass(m)
                .map(d -> {
                    ResetPasswordResult result = d.getResult();
                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    return result;
                });
    }

    private Observable<String> isVaildUserName(@NonNull String username) {
        return Observable.just(username)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("User name is empty");
                    }
                    return s;
                });
    }
}
