package com.decenturion.mobile.app.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.client.ClientPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.app.prefs.device.DevicePrefsManager;
import com.decenturion.mobile.app.prefs.device.IDevicePrefsManager;
import com.decenturion.mobile.app.prefs.locale.ILocalePrefsManager;
import com.decenturion.mobile.app.prefs.locale.LocalePrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;
import com.decenturion.mobile.app.prefs.token.TokenPrefsManager;

public class PrefsManager implements IPrefsManager {

    private Context mContext;
    private IDevicePrefsManager mIDevicePrefsManager;
    private IClientPrefsManager mIClientPrefsManager;
    private ITokensPrefsManager mITokensPrefsManager;
    private ILocalePrefsManager mILocalePrefsManager;

    public PrefsManager(@NonNull Context context) {
        mContext = context;
    }

    public SharedPreferences from(@NonNull String nameSharedPreference) {
        return mContext.getSharedPreferences(nameSharedPreference, Context.MODE_PRIVATE);
    }

    @Override
    public IDevicePrefsManager getIDevicePrefsManager() {
        if (mIDevicePrefsManager == null) {
            mIDevicePrefsManager = new DevicePrefsManager(mContext);
        }
        return mIDevicePrefsManager;
    }

    @Override
    public IClientPrefsManager getIClientPrefsManager() {
        if (mIClientPrefsManager == null) {
            mIClientPrefsManager = new ClientPrefsManager(mContext);
        }
        return mIClientPrefsManager;
    }

    @Override
    public ITokensPrefsManager getITokensPrefsManager() {
        if (mITokensPrefsManager == null) {
            mITokensPrefsManager = new TokenPrefsManager(mContext);
        }
        return mITokensPrefsManager;
    }

    @Override
    public ILocalePrefsManager getILocalePrefsManager() {
        if (mILocalePrefsManager == null) {
            mILocalePrefsManager = new LocalePrefsManager(mContext);
        }
        return mILocalePrefsManager;
    }

}
