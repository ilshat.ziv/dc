package com.decenturion.mobile.business.signin;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.signin.SigninResult;

import rx.Observable;

public interface ISigninInteractor {

    Observable<SigninResult> signin(@NonNull String username, @NonNull String password, String captcha);
}
