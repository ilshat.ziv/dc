package com.decenturion.mobile.ui.fragment.referral.main.presenter;

import com.decenturion.mobile.business.referral.main.IReferralInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.main.view.IReferralView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ReferralPresenter extends Presenter<IReferralView> implements IReferralPresenter {

    private IReferralInteractor mIReferralInteractor;
    private ReferralModelView mReferralModelView;

    public ReferralPresenter(IReferralInteractor iReferralInteractor) {
        mIReferralInteractor = iReferralInteractor;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<ReferralModelView> obs = mIReferralInteractor.getReferralInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ReferralModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);

    }

    private void bindViewDataSuccess(ReferralModelView model) {
        mReferralModelView = model;

        hideProgressView();

        if (mView != null) {
            mView.onBindViewData(model);
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
