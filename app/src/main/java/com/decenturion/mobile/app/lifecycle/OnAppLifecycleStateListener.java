package com.decenturion.mobile.app.lifecycle;

public interface OnAppLifecycleStateListener {

    void onAppForeground();

    void onAppBackground();
}
