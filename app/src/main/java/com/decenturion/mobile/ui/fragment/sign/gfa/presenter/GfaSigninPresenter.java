package com.decenturion.mobile.ui.fragment.sign.gfa.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.sign.IGfaSigninInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.signin.gfa.SigninGfaResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.sign.gfa.model.GfaSigninModelView;
import com.decenturion.mobile.ui.fragment.sign.gfa.view.IGfaSigninView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class GfaSigninPresenter extends Presenter<IGfaSigninView> implements IGfaSigninPresenter {

    private IGfaSigninInteractor mIGfaSigninInteractor;
    private GfaSigninModelView mSigninModelView;
    private IPrefsManager mIPrefsManager;

    public GfaSigninPresenter(IGfaSigninInteractor iGfaSigninInteractor, IPrefsManager iPrefsManager) {
        mIGfaSigninInteractor = iGfaSigninInteractor;
        mIPrefsManager = iPrefsManager;

        mSigninModelView = new GfaSigninModelView();
    }

    @Override
    public void bindViewData() {
        Observable<GfaSigninModelView> obs = Observable.just(mSigninModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<GfaSigninModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(GfaSigninModelView model) {
        mSigninModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void gfaSignin(@NonNull String gfaCode) {
        mSigninModelView = new GfaSigninModelView();
        mSigninModelView.setGfaCode(gfaCode);

        showProgressView();

        Observable<SigninGfaResult> obs = mIGfaSigninInteractor.gfaSignin(gfaCode)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SigninGfaResult>) this::gfaSigninSuccess,
                this::gfaSigninFailure
        );
    }

    private void gfaSigninSuccess(SigninGfaResult result) {
        hideProgressView();
        Resident resident = result.getResident();

        int step = resident.getStep();
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

        if (mView != null) {
            mView.onSigninSuccess(step);
        }
    }

    private void gfaSigninFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());
                return;
            }
        }
        showErrorView(throwable);
    }
}
