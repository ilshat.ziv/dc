package com.decenturion.mobile.ui.fragment.token.edit.model;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.network.response.model.Token;

public class EditPriceModelView {

    private int mTokenId;
    private String mCategory;

    private String mCoinId;
    private String mCoin;

    private String mSell;
    private String mSellMinPrice;

    private String mBuy;

    public EditPriceModelView(int tokenId) {
        mTokenId = tokenId;
    }

    public EditPriceModelView(OrmTokenModel model) {
        mTokenId = model.getId();

        mBuy = model.getBuyPrice();
        mSell = model.getSellPrice();
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        mCoin = coinTypeModel.getSymbol();
        mCoinId = coinTypeModel.getUuid();
        mCategory = model.getCategory();
    }

    public EditPriceModelView(Token token) {
        mBuy = token.getBidPrice();
        mSell = token.getAskPrice();
    }

    public void setTokenId(int tokenId) {
        mTokenId = tokenId;
    }

    public String getCoinId() {
        return mCoinId;
    }

    public void setSell(String sell) {
        mSell = sell;
    }

    public void setBuy(String buy) {
        mBuy = buy;
    }

    public String getSell() {
        return mSell;
    }

    public String getBuy() {
        return mBuy;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setSellMinPrice(String sellMinPrice) {
        mSellMinPrice = sellMinPrice;
    }

    public String getSellMinPrice() {
        return mSellMinPrice;
    }
}
