package com.decenturion.mobile.network.response.send;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendTokenResult {

    @JsonProperty("recipient")
    private String mDCNTWallet;

    @JsonProperty("Amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("coin")
    private String mCoin;

    public SendTokenResult() {
    }

    @JsonIgnore
    public String getDCNTWallet() {
        return mDCNTWallet;
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }
}
