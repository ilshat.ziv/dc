package com.decenturion.mobile.ui.fragment.sign.gfa.model;

import android.support.annotation.NonNull;

public class GfaSigninModelView {

    private String mGfaCode;

    public GfaSigninModelView() {
    }

    public void setGfaCode(@NonNull String gfaCode) {
        mGfaCode = gfaCode;
    }

    public String getGfaCode() {
        return mGfaCode;
    }

}
