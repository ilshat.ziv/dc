package com.decenturion.mobile.ui.fragment.profile_v2.score.list.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.profile.score.ScoreListComponent;
import com.decenturion.mobile.ui.activity.score.ScoreActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreListAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.model.ScoreListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.presenter.IScoreListPresenter;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class ScoreListFragment extends BaseFragment implements IScoreListView,
        ScoreListAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.scoreListView)
    RecyclerView mScoreListView;
    private ScoreListAdapter mScoreListAdapter;

    /* DI */

    @Inject
    IScoreListPresenter mIScoreListPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ScoreListComponent component = appDelegate
                .getIDIManager()
                .plusScoreListComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_score_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mRefreshView.setOnRefreshListener(this);

        Context context = requireContext();
        mScoreListView.setLayoutManager(new LinearLayoutManager(context));
        mScoreListAdapter = new ScoreListAdapter(context, this);
        mScoreListView.setAdapter(mScoreListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIScoreListPresenter.bindView(this);
        mIScoreListPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIScoreListPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIScoreListPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull ScoreListModelView model) {
        mScoreListAdapter.replaceDataSet(model.getTokentModelList());
    }

    @Override
    public void onSignout() {
    }

    @Override
    public void onItemClick(@NonNull ScoreModelView model) {
        Intent intent = new Intent(getActivity(), ScoreActivity.class);
        intent.putExtra(ScoreActivity.SCORE_TYPE, model.getTypeScore());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIScoreListPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mIScoreListPresenter.bindViewData();
    }
}
