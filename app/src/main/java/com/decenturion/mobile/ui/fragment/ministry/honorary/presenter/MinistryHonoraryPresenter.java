package com.decenturion.mobile.ui.fragment.ministry.honorary.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.business.ministry.product.IMinistryProductInteractor;
import com.decenturion.mobile.business.ministry.product.model.PaymenDataModel;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.ministry.honorary.view.IMinistryHonoraryView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.MinistryProductModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MinistryHonoraryPresenter extends Presenter<IMinistryHonoraryView> implements IMinistryHonoraryPresenter {

    private Context mContext;
    private IMinistryProductInteractor mIMinistryProductInteractor;
    private MinistryProductModelView mMinistryProductModelView;
    private CoinModelView mCoinModelView;

    public MinistryHonoraryPresenter(Context context,
                                     IMinistryProductInteractor iMinistryProductInteractor) {
        mContext = context;
        mIMinistryProductInteractor = iMinistryProductInteractor;

        mMinistryProductModelView = new MinistryProductModelView();
    }

    @Override
    public void bindViewData(@NonNull String category) {
        showProgressView();

        Observable<MinistryProductModelView> obs = mIMinistryProductInteractor.getMinistryModelView(category)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MinistryProductModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(Observable<String> amount, Observable<String> coin) {
        Observable<String> obs1 = amount.filter(d -> {
                    try {
                        Double.parseDouble(d);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }
        )
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread());

        Observable<String> obs2 = coin
                .onErrorReturn(throwable -> null)
                .filter(d -> !TextUtils.isEmpty(d))
                .subscribeOn(AndroidSchedulers.mainThread());

        Observable<MinistryProductModelView> obs = Observable.zip(
                obs1,
                obs2,
                (s, s2) -> {
                    showProgressView();
                    return new PaymenDataModel(s2, s, "internal");
                }
        )
                .flatMap(d -> mIMinistryProductInteractor.genPaymentAddress(d))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        obs.subscribe(
                this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void bindViewDataSuccess(MinistryProductModelView model) {
        mMinistryProductModelView = model;

        if (mView != null) {
            ArrayList<Coin> list = initCoinData();
            mCoinModelView = new CoinModelView(list);

            mView.onBindViewData(mCoinModelView, model);
        }
        hideProgressView();
    }

    private ArrayList<Coin> initCoinData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/payment.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void getPaymentAddress(@NonNull String category,
                                  @NonNull String coin) {
        if (TextUtils.isEmpty(category) || TextUtils.isEmpty(coin)) {
            return;
        }

        showProgressView();

        Observable<MinistryProductModelView> obs = mIMinistryProductInteractor.genPaymentAddress(category, coin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MinistryProductModelView>) this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void genPaymentAddressSuccess(MinistryProductModelView address) {
        if (mView != null) {
            mView.onBindPaymentViewData(address);
        }
        hideProgressView();
    }

    private void genPaymentAddressFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mMinistryProductModelView = null;
    }
}
