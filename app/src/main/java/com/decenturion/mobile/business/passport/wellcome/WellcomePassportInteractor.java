package com.decenturion.mobile.business.passport.wellcome;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.signup.skip.SignupSkipResponse;
import com.decenturion.mobile.network.response.signup.skip.SignupSkipResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.wellcome.model.WellcomePassportModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class WellcomePassportInteractor implements IWellcomPassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public WellcomePassportInteractor(IResidentClient IResidentClient,
                                      IResidentRepository iResidentRepository,
                                      IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<WellcomePassportModelView> getResidentInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                WellcomePassportModelView::new);
    }

    @Override
    public Observable<SignupSkipResult> skipWellcomeMessage() {
        return mIResidentClient.skipSignup()
                .map(SignupSkipResponse::getResult)
                .doOnNext(d -> {
                    Resident resident = d.getResident();
                    saveData(d);

                    int step = resident.getStep();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);
                });
    }

    private void saveData(@NonNull SignupSkipResult d) {
        Resident resident = d.getResident();
        try {
            mIResidentRepository.saveResidentInfo(resident);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
