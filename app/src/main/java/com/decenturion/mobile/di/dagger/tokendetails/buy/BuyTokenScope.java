package com.decenturion.mobile.di.dagger.tokendetails.buy;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface BuyTokenScope {
}
