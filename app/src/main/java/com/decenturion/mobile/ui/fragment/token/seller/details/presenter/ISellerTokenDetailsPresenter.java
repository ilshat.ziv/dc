package com.decenturion.mobile.ui.fragment.token.seller.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.seller.details.view.ISellerTokenDetailsView;

public interface ISellerTokenDetailsPresenter extends IPresenter<ISellerTokenDetailsView> {

    void bindViewData(int tokenId);

    void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory);

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
