package com.decenturion.mobile.ui.fragment.referral.statistic.model;

public class StatisticReferralModelView {

    private String mCitizenPrice;
    private String mEarnedBtc;
    private String mEarnedEth;
    private int mInvited;

    public StatisticReferralModelView(String citizenPrice,
                                      String earnedBtc,
                                      String earnedEth,
                                      int invited) {
        mCitizenPrice = citizenPrice;
        mEarnedBtc = earnedBtc;
        mEarnedEth = earnedEth;
        mInvited = invited;
    }

    public String getEarnedBtc() {
        return mEarnedBtc;
    }

    public String getEarnedEth() {
        return mEarnedEth;
    }

    public int getInvited() {
        return mInvited;
    }

    public String getCitizenPrice() {
        return mCitizenPrice;
    }
}
