package com.decenturion.mobile.ui.fragment.referral.balance.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.balance.model.BalanceReferralModelView;

public interface IBalanceReferralView extends IScreenFragmentView {

    void onBindViewData(@NonNull BalanceReferralModelView model);

}
