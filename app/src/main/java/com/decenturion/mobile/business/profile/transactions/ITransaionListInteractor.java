package com.decenturion.mobile.business.profile.transactions;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;

import rx.Observable;

public interface ITransaionListInteractor {

    Observable<TransactionListModelView> getTransactionsInfo();

    Observable<TransactionListModelView> getTransactionsInfo(@NonNull TransactionListModelView model);
}
