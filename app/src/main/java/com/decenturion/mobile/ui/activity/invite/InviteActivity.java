package com.decenturion.mobile.ui.activity.invite;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.invite.view.InviteFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class InviteActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
        } else {
            iNavigationManager.navigateTo(InviteFragment.class);
        }
    }
}
