package com.decenturion.mobile.di.dagger.pass.reset;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.business.restore.RestorePassInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.reset.presenter.IResetPasswordPresenter;
import com.decenturion.mobile.ui.fragment.pass.reset.presenter.ResetPasswordPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ResetPassModule {

    @Provides
    @ResetPassScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ResetPassScope
    IRestorePassInteractor provideIRestorePassInteractor(IResidentClient iResidentClient,
                                                         IResidentRepository iResidentRepository) {
        return new RestorePassInteractor(iResidentClient, iResidentRepository);
    }


    @Provides
    @ResetPassScope
    IResetPasswordPresenter provideIResetPasswordPresenter(IRestorePassInteractor iRestorePassInteractor,
                                                         IPrefsManager iPrefsManager) {
        return new ResetPasswordPresenter(iRestorePassInteractor, iPrefsManager);
    }
}
