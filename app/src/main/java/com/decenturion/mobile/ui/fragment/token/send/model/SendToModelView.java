package com.decenturion.mobile.ui.fragment.token.send.model;

import java.util.ArrayList;

public class SendToModelView {

    private ArrayList<SendToModel> mSendToData;

    public SendToModelView(ArrayList<SendToModel> sendToData) {
        mSendToData = sendToData;
    }

    public ArrayList<SendToModel> getSendToData() {
        return mSendToData;
    }
}
