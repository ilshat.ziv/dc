package com.decenturion.mobile.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.decenturion.mobile.app.localisation.view.LocalizedTextView;

public class LinkTextView extends LocalizedTextView {

    private OnLinkTextViewListener mOnLinkClickListener;

    public LinkTextView(Context context) {
        super(context);
    }

    public LinkTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LinkTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initLocaleText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super.initLocaleText(context, attrs);
        CharSequence text = getText();
        if (!TextUtils.isEmpty(text)) {
            initLinkText(String.valueOf(text));
        }
    }

    private void initLinkText(@NonNull String text) {
        if (text.contains("<u>")) {
            Spanned label = Html.fromHtml(text);
            setText(label);
            this.setOnClickListener(v -> {
                if (mOnLinkClickListener != null) {
                    mOnLinkClickListener.onLinkClick();
                }
            });
        } else {
            setText(text);
        }
    }

    public void setOnLinkClickListener(OnLinkTextViewListener listener) {
        mOnLinkClickListener = listener;
    }

    public void setLinkText(@NonNull String text) {
        initLinkText(text);
    }

    public interface OnLinkTextViewListener {

        void onLinkClick();

        void onTextClick();
    }
}
