package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.Wallet;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_wallet_model")
public class OrmWalletModel {

    private final static String ID = "id";
    private final static String UUID = "uuid";
    private final static String COIN = "coin";
    private final static String ADDRESS = "address";
    private final static String BALANCE = "balance";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(dataType = DataType.STRING, columnName = UUID)
    private String mUuid;

    @DatabaseField(dataType = DataType.STRING, columnName = COIN)
    private String mCoin;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = ADDRESS)
    private String mAddress;

    @DatabaseField(dataType = DataType.STRING, columnName = BALANCE)
    private String mBalance;

    public OrmWalletModel() {
    }

    public OrmWalletModel(@NonNull Wallet wallet) {
        mUuid = wallet.getUuid();
        mCoin = wallet.getCoin();
        mAddress = wallet.getAddress();
        mBalance = wallet.getBalance();
    }

    public String getUuid() {
        return mUuid;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getBalance() {
        return mBalance;
    }
}
