package com.decenturion.mobile.di.dagger.referral.history;

import com.decenturion.mobile.ui.fragment.referral.history.view.HistoryFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ReferralHistoryModule.class
})

@ReferralHistorylScope
public interface ReferralHistorylComponent {

    void inject(HistoryFragment view);
}
