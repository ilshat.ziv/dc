package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class BuyTokenView extends UBaseView implements TextWatcher {

    @Nullable
    @BindView(R.id.valueView)
    EditText mValueView;

    @Nullable
    @BindView(R.id.coinView)
    TextView mCoinView;

    private boolean isAddedTextWatcher;

    public BuyTokenView(Context context) {
        super(context);
    }

    public BuyTokenView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BuyTokenView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null && mCoinView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView() && !isAddedTextWatcher) {
            assert mValueView != null;
            mValueView.addTextChangedListener(this);
            isAddedTextWatcher = true;
        }
    }

    public void setTypeInput(int inputType) {
        init();
        assert mValueView != null;
        mValueView.setInputType(inputType);
    }

    @Override
    public void editMode() {
        super.editMode();
        assert mValueView != null;
        mValueView.setEnabled(true);
    }

    @Override
    public void simpleMode() {
        super.simpleMode();
        assert mValueView != null;
        mValueView.setEnabled(false);
    }

    public void setHint(String hint) {
        init();
        assert mValueView != null;
        mValueView.setHint(String.valueOf(hint));
    }

    public void setText(String text) {
        init();
        assert mValueView != null;
        mValueView.setText(text != null ? String.valueOf(text) : "");
    }

    public void setCoin(String text) {
        init();
        assert mCoinView != null;
        mCoinView.setText(text != null ? String.valueOf(text) : "");
    }

    public String getValue() {
        assert mValueView != null;
        CharSequence text = mValueView.getText();
        return text.toString();
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mChangeComponentListener != null) {
            mChangeComponentListener.onChangeComponentData(this);
        }
    }
}