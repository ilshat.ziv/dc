package com.decenturion.mobile.app.toast;

import android.support.annotation.NonNull;

public interface IToastManager {

    void showShortToast(@NonNull TypeMessage type, @NonNull String message);

    void showLongToast(@NonNull TypeMessage type, @NonNull String message);

    void cancelToast();
}
