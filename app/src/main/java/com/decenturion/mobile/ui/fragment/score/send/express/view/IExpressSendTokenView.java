package com.decenturion.mobile.ui.fragment.score.send.express.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.score.send.express.model.ExpressSendToModelView;
import com.decenturion.mobile.ui.fragment.score.send.express.model.ExpressSendTokenModelView;

public interface IExpressSendTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull ExpressSendTokenModelView model, @NonNull ExpressSendToModelView model1);

    void onEnableTwoFA();

    void onSendTokenSuccess();
}
