package com.decenturion.mobile.ui.fragment.referral.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.decenturion.mobile.ui.fragment.referral.balance.view.BalanceReferralFragment;
import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.statistic.view.StatisticReferralFragment;

public class ReferralAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private SparseArray<Fragment> mHashMap;

    private ReferralModelView mModel;

    public ReferralAdapter(FragmentManager fm, ReferralModelView model) {
        super(fm);
        this.mNumOfTabs = 2;
        mHashMap = new SparseArray<>();
        mModel = model;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = mHashMap.get(position);
        if (fragment != null) {
            return fragment;
        }
        switch (position) {
            case 0: {
                fragment = new StatisticReferralFragment();
                if (mModel != null) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(StatisticReferralFragment.Args.INVITED, mModel.getInvited());
                    bundle.putString(StatisticReferralFragment.Args.EARNED_BTC, mModel.getEarnedBtc());
                    bundle.putString(StatisticReferralFragment.Args.EARNED_ETH, mModel.getEarnedEth());
                    bundle.putString(StatisticReferralFragment.Args.CITIZEN_PRICE, mModel.getCitizenPrice());
                    fragment.setArguments(bundle);
                }
                break;
            }
            case 1: {
                fragment = new BalanceReferralFragment();
                if (mModel != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(BalanceReferralFragment.Args.TOTAL, mModel.getTotal());
                    bundle.putString(BalanceReferralFragment.Args.AVAILABLE, mModel.getFree());
                    bundle.putString(BalanceReferralFragment.Args.HOLD, mModel.getHold());
                    fragment.setArguments(bundle);
                }
                break;
            }
            default: {
                return null;
            }
        }
        mHashMap.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}