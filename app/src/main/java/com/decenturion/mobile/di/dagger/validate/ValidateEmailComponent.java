package com.decenturion.mobile.di.dagger.validate;

import com.decenturion.mobile.ui.fragment.validate.view.ValidateEmailFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ValidateEmailModule.class
})

@ValidateEmailScope
public interface ValidateEmailComponent {

    void inject(ValidateEmailFragment view);
}
