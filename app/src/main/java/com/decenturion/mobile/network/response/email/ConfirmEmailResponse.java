package com.decenturion.mobile.network.response.email;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmEmailResponse extends BaseResponse {

    @JsonProperty("result")
    private ConfirmEmailResult mResult;

    public ConfirmEmailResponse() {
    }

    @JsonIgnore
    public ConfirmEmailResult getResult() {
        return mResult;
    }
}
