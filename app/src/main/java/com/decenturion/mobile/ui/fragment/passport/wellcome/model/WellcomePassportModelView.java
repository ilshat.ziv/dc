package com.decenturion.mobile.ui.fragment.passport.wellcome.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;

public class WellcomePassportModelView {

    private String mName;
    private String mFirstname;
    private String mLastname;

    public WellcomePassportModelView() {
    }

    public WellcomePassportModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1, OrmPhotoModel m2) {
        mName = m1.getName();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }
}
