package com.decenturion.mobile.ui.fragment.validate.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.validate.model.ResendEmailModelView;

public interface IValidEmailView extends IScreenFragmentView {

    void onBindViewData(@NonNull ResendEmailModelView model);

    void onConfirmedEmail();

    void onSignin();
}
