package com.decenturion.mobile.app.resource;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.AnimRes;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class ResourceManager implements IResourceManager {
    
    private Context mContext;

    public ResourceManager(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public int getColor(@ColorRes int idResource) {
        return mContext.getResources().getColor(idResource);
    }

    @Override
    public ColorStateList getColorStateList(@ColorRes int idResource) {
        return mContext.getResources().getColorStateList(idResource);
    }

    @Override
    public float getDimension(@DimenRes int idResource) {
        return mContext.getResources().getDimension(idResource);
    }

    @Override
    public String getString(@StringRes int idResource) {
        return mContext.getResources().getString(idResource);
    }

    @Override
    public int getInteger(@IntegerRes int idResource) {
        return mContext.getResources().getInteger(idResource);
    }

    @Override
    public Drawable getDrawable(@DrawableRes int idResource) {
        return mContext.getResources().getDrawable(idResource);
    }

    @Override
    public Bitmap getBitmapFDrawer(@DrawableRes int idResource) {
        return BitmapFactory.decodeResource(mContext.getResources(), idResource);
    }

    @Override
    public Animation getAnimation(@AnimRes int idResource) {
        return AnimationUtils.loadAnimation(mContext, idResource);
    }

    @Override
    public String getResourceName(int resourceId) {
        Resources resources = mContext.getResources();
        return resources.getResourceName(resourceId);
    }
}
