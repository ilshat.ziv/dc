package com.decenturion.mobile.ui.activity.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.transaction.view.TransactionInfoFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class TransactionActivity extends SingleFragmentActivity {

    public static final String TRANSACTIONS_ID = "TRANSACTIONS_ID";

    private Bundle mBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        iNavigationManager.navigateTo(TransactionInfoFragment.class, mBundle);
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mBundle = intent.getExtras();
        }
    }
}
