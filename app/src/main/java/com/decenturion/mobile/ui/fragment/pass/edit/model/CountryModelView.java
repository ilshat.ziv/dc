package com.decenturion.mobile.ui.fragment.pass.edit.model;

import java.util.ArrayList;
import com.decenturion.mobile.network.response.model.Country;

public class CountryModelView {

    private ArrayList<Country> mCountryData;

    public CountryModelView() {
    }

    public CountryModelView(ArrayList<Country> countryData) {
        mCountryData = countryData;
    }

    public ArrayList<Country> getCountryData() {
        return mCountryData;
    }
}
