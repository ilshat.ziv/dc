package com.decenturion.mobile.business.invite;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.invite.send.InviteResult;

import rx.Observable;

public interface IInviteInteractor {

    Observable<InviteResult> invite(@NonNull String email, @NonNull String token);
}
