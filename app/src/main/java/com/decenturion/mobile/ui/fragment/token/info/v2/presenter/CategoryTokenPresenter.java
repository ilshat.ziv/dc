package com.decenturion.mobile.ui.fragment.token.info.v2.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.tokendetails.info.ITokenInfoInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView;
import com.decenturion.mobile.ui.fragment.token.info.v2.view.ICategoryTokenView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CategoryTokenPresenter extends Presenter<ICategoryTokenView> implements ICategoryTokenPresenter {

    private ITokenInfoInteractor mITokenInfoInteractor;
    private TokenInfoModelView mTokenInfoModelView;
    private ILocalistionManager mILocalistionManager;

    public CategoryTokenPresenter(ITokenInfoInteractor iTokenInfoInteractor,
                                  ILocalistionManager iLocalistionManager) {
        mITokenInfoInteractor = iTokenInfoInteractor;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public void bindViewData(@NonNull String tokenCategory) {
//        showProgressView();

        mTokenInfoModelView = new TokenInfoModelView(tokenCategory, mILocalistionManager);
        Observable<TokenInfoModelView> obs = mITokenInfoInteractor.getTokenCategoryInfo(tokenCategory)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenInfoModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TokenInfoModelView model) {
        mTokenInfoModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTokenInfoModelView = null;
    }
}
