package com.decenturion.mobile.business.settings.details;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.ChangeDetailsModel;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.details.ChangeDetailsResponse;
import com.decenturion.mobile.network.response.details.ChangeDetailsResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.details.model.DetailsSettingsModelView;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

public class DetailsSettingsInteractor implements IDetailsSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public DetailsSettingsInteractor(IResidentClient IResidentClient,
                                     IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<DetailsSettingsModelView> getPassportInfo() {
        return Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                DetailsSettingsModelView::new);
    }

    @Override
    public Observable<ChangeDetailsResponse> saveChanges(@NonNull String bio,
                                                         @NonNull String telegram,
                                                         @NonNull String whatsApp,
                                                         @NonNull String viber,
                                                         @NonNull String wechat) {
        ChangeDetailsModel model = new ChangeDetailsModel();
        model.setAbout(bio);
        model.setTelegram(telegram);
        model.setWhatsapp(whatsApp);
        model.setViber(viber);
        model.setWechat(wechat);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.changeDetails(m))
                .doOnNext(d -> {
                    try {
                        ChangeDetailsResult result = d.getResult();
                        mIResidentRepository.saveResidentInfo(result.getResident());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap) {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(OrmResidentModel::getUuid)
                .map(d -> {
                    byte[] bytes = ImageUtils.getBytes(bitmap);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    return MultipartBody.Part.createFormData("file", d + ".png", requestFile);
                })
                .flatMap(d -> mIResidentClient.uploadPhoto(d));
    }
}
