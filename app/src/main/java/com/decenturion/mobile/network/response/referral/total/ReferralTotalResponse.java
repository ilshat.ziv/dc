package com.decenturion.mobile.network.response.referral.total;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralTotalResponse extends BaseResponse {

    @JsonProperty("result")
    private ReferralTotalResult mResult;

    public ReferralTotalResponse() {
    }

    @JsonIgnore
    public ReferralTotalResult getResult() {
        return mResult;
    }
}
