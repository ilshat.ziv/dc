package com.decenturion.mobile.ui.fragment.referral.history.model;

import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class HistoryModelView {

    private String mType = "";
    private List<HistoryModel> mHistoryModelList;
    private int mTotal;
    private long mOffset;
    public final int LIMIT = 15;

    private ArrayList<State> mStates;

    public HistoryModelView() {
    }

    public HistoryModelView(List<HistoryModel> list, int total) {
        mHistoryModelList = list;
        mTotal = total;
    }

    public HistoryModelView(List<HistoryModel> d) {
        mHistoryModelList = d;
    }

    public List<HistoryModel> getHistoryList() {
        return mHistoryModelList;
    }

    public void addHistoryTransactions(List<HistoryModel> list) {
        if (CollectionUtils.isEmpty(mHistoryModelList)) {
            mHistoryModelList = new ArrayList<>();
        }

        if (!CollectionUtils.isEmpty(list)) {
            mHistoryModelList.addAll(list);
        }
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }

    public void setStates(ArrayList<State> states) {
        mStates = states;
    }

    public ArrayList<State> getStates() {
        return mStates;
    }
}
