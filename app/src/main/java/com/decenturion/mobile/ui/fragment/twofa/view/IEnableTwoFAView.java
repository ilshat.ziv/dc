package com.decenturion.mobile.ui.fragment.twofa.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.twofa.model.EnableTwoFAModelView;

public interface IEnableTwoFAView extends IScreenFragmentView {

    void onBindViewData(@NonNull EnableTwoFAModelView model);

    void onChangesSaved();
}
