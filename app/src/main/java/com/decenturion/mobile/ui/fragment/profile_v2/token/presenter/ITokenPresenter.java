package com.decenturion.mobile.ui.fragment.profile_v2.token.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.view.ITokenView;

public interface ITokenPresenter extends IPresenter<ITokenView> {

    void bindViewData();
}
