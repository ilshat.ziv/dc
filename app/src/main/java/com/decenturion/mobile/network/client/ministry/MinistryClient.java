package com.decenturion.mobile.network.client.ministry;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class MinistryClient extends DHttpClient<IMinistryClient> {

    public MinistryClient(NetworkModule module) {
        super(module);
    }
}
