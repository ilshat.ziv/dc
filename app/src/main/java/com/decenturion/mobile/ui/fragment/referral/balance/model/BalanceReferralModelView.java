package com.decenturion.mobile.ui.fragment.referral.balance.model;

public class BalanceReferralModelView {

    private String mCitizenPrice;
    private String mEarnedBtc;
    private String mEarnedEth;
    private int mInvited;

    public BalanceReferralModelView(String citizenPrice,
                                    String earnedBtc,
                                    String earnedEth,
                                    int invited) {
        mCitizenPrice = citizenPrice;
        mEarnedBtc = earnedBtc;
        mEarnedEth = earnedEth;
        mInvited = invited;
    }

    public String getEarnedBtc() {
        return mEarnedBtc;
    }

    public String getEarnedEth() {
        return mEarnedEth;
    }

    public int getInvited() {
        return mInvited;
    }

    public String getCitizenPrice() {
        return mCitizenPrice;
    }
}
