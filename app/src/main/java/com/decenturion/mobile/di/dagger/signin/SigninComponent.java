package com.decenturion.mobile.di.dagger.signin;

import com.decenturion.mobile.ui.fragment.sign.in.view.SigninFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SigninModule.class
})

@SigninScope
public interface SigninComponent {

    void inject(SigninFragment view);
}
