package com.decenturion.mobile.ui.fragment.invite.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.invite.IInviteInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.invite.send.InviteResult;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.invite.model.InviteModelView;
import com.decenturion.mobile.ui.fragment.invite.model.Token;
import com.decenturion.mobile.ui.fragment.invite.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.invite.view.IInviteView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class InvitePresenter extends Presenter<IInviteView> implements IInvitePresenter {

    private IInviteInteractor mIInviteInteractor;
    private InviteModelView mInviteModelView;
    private TokenModelView mTokenModelView;

    public InvitePresenter(IInviteInteractor iInviteInteractor) {
        mIInviteInteractor = iInviteInteractor;

        mInviteModelView = new InviteModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mInviteModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<InviteModelView> obs = Observable.just(mInviteModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<InviteModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void inviteUser(@NonNull String email, @NonNull String token) {
        mInviteModelView.setInviteEmail(email);
        mInviteModelView.setToken(token);

        showProgressView();

        Observable<InviteResult> obs = mIInviteInteractor.invite(email, token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<InviteResult>) this::inviteUserSuccess,
                this::inviteUserFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle, String email, String selection) {
        bundle.putString("email", email);
        bundle.putString("token", selection);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        // TODO: 14.11.2018 дописать восстановление данных
//        mTokenModelView.setUsername(savedInstanceState.getString("username", ""));
//        mTokenModelView.setPassword(savedInstanceState.getString("password", ""));
//
//        Observable<SigninModelView> obs = Observable.just(mSigninModelView)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<SigninModelView>) this::bindViewDataSuccess,
//                this::bindViewDataFailure
//        );
    }

    private void bindViewDataSuccess(InviteModelView model) {
        mInviteModelView = model;

        if (mView != null) {
            mTokenModelView = initTokenModelView();
            mView.onBindViewData(model, mTokenModelView);
        }
        hideProgressView();
    }

    private TokenModelView initTokenModelView() {
        ArrayList<Token> list = new ArrayList<>();
        list.add(new Token("DCNT Classic", "classic"));
        list.add(new Token("DCNT Liquid", "liquid"));
        return new TokenModelView(list);
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    private void inviteUserSuccess(InviteResult result) {
        hideProgressView();

        if (mView != null) {
            mView.onInvitationSend();
        }
    }

    private void inviteUserFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                switch (error.getKey()) {
                    case "token" : {
                        if (mView != null) {
                            mView.onInvitationSendFail();
                        }
                        break;
                    }
                    default : {
                        showErrorView(error.getMessage());
                    }
                }

                return;
            }
        }
        showErrorView(throwable);
    }

}
