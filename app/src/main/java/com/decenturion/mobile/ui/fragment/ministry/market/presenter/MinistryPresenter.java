package com.decenturion.mobile.ui.fragment.ministry.market.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.ministry.market.IMinistryInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryModelView;
import com.decenturion.mobile.ui.fragment.ministry.market.view.IMinistryView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MinistryPresenter extends Presenter<IMinistryView> implements IMinistryPresenter {

    private IMinistryInteractor mIMinistryInteractor;

    private MinistryModelView mMinistryModelView;

    public MinistryPresenter(IMinistryInteractor iMinistryInteractor) {
        mIMinistryInteractor = iMinistryInteractor;

        mMinistryModelView = new MinistryModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mMinistryModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<MinistryModelView> obs = mIMinistryInteractor.getMinistryModelView()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MinistryModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        Observable<MinistryModelView> obs = Observable.just(mMinistryModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<MinistryModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(MinistryModelView model) {
        mMinistryModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
