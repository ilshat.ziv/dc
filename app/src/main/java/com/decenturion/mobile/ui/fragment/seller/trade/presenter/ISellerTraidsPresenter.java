package com.decenturion.mobile.ui.fragment.seller.trade.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.seller.trade.view.ISellerTraidsView;

public interface ISellerTraidsPresenter extends IPresenter<ISellerTraidsView> {

    void bindViewData(@NonNull String sellerUUID);
}
