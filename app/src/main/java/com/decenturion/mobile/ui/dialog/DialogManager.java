package com.decenturion.mobile.ui.dialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.logging.Logger;

public class DialogManager<D extends BaseDialogFragment> implements IDialogManager<D> {

    private FragmentManager mFragmentManager;
    private FragmentLifecycleManager mFragmentLifecycleManager;

    public DialogManager(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
//        mFragmentLifecycleManager = new FragmentLifecycleManager(this);
//        mFragmentManager.registerFragmentLifecycleCallbacks(mFragmentLifecycleManager, true);
    }

    @Override
    public void showDialog(Class<D> clazz) {
        this.showDialog(clazz, null);
    }

    @Override
    public void showDialog(Class<D> clazz, Bundle bundle) {
        String tag = clazz.getSimpleName();
        Fragment f = mFragmentManager.findFragmentByTag(tag);
        if (f != null && f instanceof BaseDialogFragment) {
            try {
                ((BaseDialogFragment) f).dismiss();
            } catch (Exception e) {
                LoggerUtils.exception(e);
            }
        }

        D dialogFragment = getInstanceDialogFragment(clazz);
        if (bundle != null) {
            assert dialogFragment != null;
            dialogFragment.setArguments(bundle);
        }

        assert dialogFragment != null;
        try {
            dialogFragment.show(mFragmentManager, tag);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void showDialog(Class<D> clazz, Bundle bundle, Fragment target, int requestCode) {
        String tag = clazz.getSimpleName();
        Fragment f = mFragmentManager.findFragmentByTag(tag);
        if (f != null && f instanceof BaseDialogFragment) {
            try {
                ((BaseDialogFragment) f).dismiss();
            } catch (Exception e) {
                LoggerUtils.exception(e);
            }
        }

        D dialogFragment = getInstanceDialogFragment(clazz);
        dialogFragment.setTargetFragment(target, requestCode);
        if (bundle != null) {
            assert dialogFragment != null;
            dialogFragment.setArguments(bundle);
        }

        assert dialogFragment != null;
        try {
            dialogFragment.show(mFragmentManager, tag);
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void hideDialog(Class<D> clazz) {
        String tag = clazz.getSimpleName();
        Fragment f = mFragmentManager.findFragmentByTag(tag);
        if (f != null && f instanceof BaseDialogFragment) {
            try {
                ((BaseDialogFragment) f).dismiss();
            } catch (Exception e) {
                LoggerUtils.exception(e);
            }
        }
    }

    private D getInstanceDialogFragment(Class<D> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            LoggerUtils.exception(e);
        } catch (IllegalAccessException e) {
            LoggerUtils.exception(e);
        }

        return null;
    }

    @Override
    public void onDestroy() {
//        mFragmentManager.unregisterFragmentLifecycleCallbacks(mFragmentLifecycleManager);
//        mFragmentManager = null;
    }

    private static class FragmentLifecycleManager extends FragmentManager.FragmentLifecycleCallbacks {

        @Override
        public void onFragmentCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
            super.onFragmentCreated(fm, f, savedInstanceState);
//            mOnFragmentStateListener.onAddFragment();
        }

        @Override
        public void onFragmentDestroyed(FragmentManager fm, Fragment f) {
            super.onFragmentDestroyed(fm, f);
//            mOnFragmentStateListener.onRemoveFragment();
        }
    }
}
