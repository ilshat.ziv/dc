package com.decenturion.mobile.business.referral.main;

import android.text.TextUtils;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.model.ClassicBalance;
import com.decenturion.mobile.network.response.model.Earned;
import com.decenturion.mobile.network.response.model.Guests;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResponse;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;
import com.decenturion.mobile.network.response.referral.total.ReferralTotalResponse;
import com.decenturion.mobile.network.response.referral.total.ReferralTotalResult;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.List;

import rx.Observable;

public class ReferralInteractor implements IReferralInteractor {

    private IResidentClient mIResidentClient;
    private IReferralClient mIReferralClient;
    private IResidentRepository mIResidentRepository;

    public ReferralInteractor(IResidentClient iResidentClient,
                              IReferralClient iReferralClient,
                              IResidentRepository iResidentRepository) {
        mIResidentClient = iResidentClient;
        mIReferralClient = iReferralClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<ReferralModelView> getReferralInfo() {
        Observable<ResidentResult> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return d;
                });


        Observable<ReferralSettingsResult> obs2 = mIReferralClient.getReferralSettings()
                .map(ReferralSettingsResponse::getResult);

        Observable<ReferralTotalResult> obs3 = mIReferralClient.getReferralTotal()
                .map(ReferralTotalResponse::getResult);

        return Observable.zip(obs1, obs2, obs3, (residentResult, referralSettingsResult, referralTotalResult) -> {
            Resident resident = residentResult.getResident();
            String url = BuildConfig.HOST + "/signup/" + resident.getReferralId();
            ReferralModelView model = new ReferralModelView(url);

            ClassicBalance classicBalance = residentResult.getClassicBalance();
            model.setTotal(classicBalance.getTotal());
            model.setFree(classicBalance.getAvailable());
            model.setHold(classicBalance.getHold());

            switch (referralSettingsResult.getCostingMethod()) {
                case "fixed" : {
                    model.setCitizenPrice(String.valueOf(referralSettingsResult.getFixedCost()));
                    break;
                }
                default : {
                    model.setCitizenPrice(String.valueOf(referralSettingsResult.getPercent()));
                }
            }

            model.setEarnedBtc("0.0");
            model.setEarnedEth("0.0");
            List<Earned> earnedList = referralTotalResult.getEarnedList();
            if (!CollectionUtils.isEmpty(earnedList)) {
                for (Earned earned : earnedList) {
                    String currency = earned.getCurrency();
                    if (TextUtils.equals(currency, "eth")) {
                        model.setEarnedEth(earned.getEarned());
                    } else if (TextUtils.equals(currency, "btc")) {
                        model.setEarnedBtc(earned.getEarned());
                    }
                }
            }

            int invited = 0;
            List<Guests> guestsList = referralTotalResult.getGuestsList();
            if (!CollectionUtils.isEmpty(guestsList)) {
                for (Guests guests : guestsList) {
                    invited += Integer.parseInt(guests.getCount());
                }
            }
            model.setInvited(invited);

            return model;
        });
    }
}
