package com.decenturion.mobile.ui.fragment.disclaimer.view;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Layout;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.aggrement.UserAggrementComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.agreement.IUserAggrementActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.disclaimer.model.UserAggrementModelView;
import com.decenturion.mobile.ui.fragment.disclaimer.presenter.IUserAggrementPresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class UserAggrementFragment extends BaseFragment implements IUserAggrementView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.textView)
    TextView mTextView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IUserAggrementPresenter mIUserAggrementPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        UserAggrementComponent userAggrementComponent = appDelegate
                .getIDIManager()
                .plusUserAggrementComponent();
        userAggrementComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_user_aggrements, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mTextView.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_termsofuse_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @OnClick(R.id.agreeButtonView)
    protected void onAgree() {
        IUserAggrementActivity activity = (IUserAggrementActivity) requireActivity();
        activity.aggree();
    }

    @Override
    public void onClick(View v) {
        IUserAggrementActivity activity = (IUserAggrementActivity) requireActivity();
        activity.disaggree();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIUserAggrementPresenter.bindView(this);
        mIUserAggrementPresenter.bindViewData();
    }

    @Override
    public void onBindViewData(@NonNull UserAggrementModelView model) {
        Spanned s = convertToSpanned(model.getTerms());
        mTextView.setText(s);
    }

    private Spanned convertToSpanned(String html) {
        Spanned result = null;
        if (!TextUtils.isEmpty(html)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
            } else {
                result = Html.fromHtml(html);
            }
        }
        return result;
    }
}
