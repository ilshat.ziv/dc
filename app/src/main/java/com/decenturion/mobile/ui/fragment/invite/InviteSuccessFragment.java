package com.decenturion.mobile.ui.fragment.invite;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.invite.InviteComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class InviteSuccessFragment extends BaseFragment implements View.OnClickListener {

    /* UI */

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        InviteComponent inviteComponent= appDelegate
                .getIDIManager()
                .plusInviteComponent();
        inviteComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_invite_success, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_share_invite_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.proccedButtonView)
    protected void onProcced() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
