package com.decenturion.mobile.ui.fragment.sign.gfa.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.sign.GfaSigninComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.sign.ISignActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.sign.gfa.model.GfaSigninModelView;
import com.decenturion.mobile.ui.fragment.sign.gfa.presenter.IGfaSigninPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class GfaSigninFragment extends BaseFragment implements IGfaSigninView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.codeView)
    EditTextView mCodeView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IGfaSigninPresenter mIGfaSigninPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        GfaSigninComponent gfaSigninComponent = appDelegate
                .getIDIManager()
                .plusGfaSigninComponent();
        gfaSigninComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_signin_gfa, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();

    }

    private void setupUi() {
        initToolbar();

        mCodeView.showLabel();
        mCodeView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signin_g2fa_code_title));
        mCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_signin_g2fa_code_placeholder));
        mCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mCodeView.editMode();
        mCodeView.setVisibleDivider(true);
    }

    private void initToolbar() {
        ISignActivity activity = (ISignActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signin_g2fa_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIGfaSigninPresenter.bindView(this);
        mIGfaSigninPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIGfaSigninPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIGfaSigninPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.sendCodeButtonView)
    protected void onSignInG2fa() {
        mIGfaSigninPresenter.gfaSignin(mCodeView.getValue());
    }

    @Override
    public void onSigninSuccess(int step) {
        FragmentActivity activity = requireActivity();
        switch (step) {
            case 0 : {
                break;
            }
            case 2 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 3 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.PHISICAL_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 4 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ACTIVATION_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 5 : {
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.WELLCOME);
                startActivity(intent);
                activity.finish();
                break;
            }
            case 6 : {
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
                break;
            }
        }
    }

    @Override
    public void onBindViewData(@NonNull GfaSigninModelView model) {
        mCodeView.setText(model.getGfaCode());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIGfaSigninPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
