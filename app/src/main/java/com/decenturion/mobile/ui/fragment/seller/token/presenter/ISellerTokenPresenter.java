package com.decenturion.mobile.ui.fragment.seller.token.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.seller.token.view.ISellerTokenView;

public interface ISellerTokenPresenter extends IPresenter<ISellerTokenView> {

    void bindViewData(@NonNull String residentUUID);
}
