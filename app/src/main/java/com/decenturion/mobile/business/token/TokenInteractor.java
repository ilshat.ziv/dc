package com.decenturion.mobile.business.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.TokensResponse;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.transactions.TransactionsResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModel;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;
import rx.functions.Func2;

public class TokenInteractor implements ITokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IScoreClient mIScoreClient;

    private IResidentRepository mIResidentRepository;
    private IWalletRepository mIWalletRepository;
    private ITokenRepository mITokenRepository;

    private IPrefsManager mIPrefsManager;

    public TokenInteractor(IResidentClient IResidentClient,
                           ITokenClient iTokenClient,
                           IScoreClient iScoreClient,
                           IResidentRepository iResidentRepository,
                           IWalletRepository iWalletRepository,
                           ITokenRepository iTokenRepository,
                           IPrefsManager iPrefsManager) {

        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIScoreClient = iScoreClient;
        mIResidentRepository = iResidentRepository;
        mIWalletRepository = iWalletRepository;
        mITokenRepository = iTokenRepository;
        mIPrefsManager = iPrefsManager;
    }

    private Observable<OrmResidentModel> getResidentIfon() {
        Observable<OrmResidentModel> obs1 = Observable.just(mIResidentRepository.getResidentInfo());
        Observable<OrmResidentModel> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    OrmResidentModel ormModel = new OrmResidentModel(d.getResident());
                    try {
                        mIResidentRepository.saveResidentInfo(ormModel);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return ormModel;
                });

        return Observable.concat(obs1, obs2, Observable.error(new RuntimeException("Oops, an error occurred")))
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<TokenModelView> getTokenModelView() {
        return getResidentIfon()
                .map(OrmResidentModel::getUuid)
                .onErrorReturn(throwable -> {
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    return iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_UUID, "");
                })
                .flatMap(d -> mITokenClient.getTokens(d))
                .map(TokensResponse::getResult)
                .flatMapIterable(d -> d)
                .map(d -> {
                    OrmTokenModel model = new OrmTokenModel(d);
                    try {
                        mITokenRepository.saveTokenInfo(model);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new TokenModel(model, d.getStartup().getLogo());
                })
                .toList()
                .map(TokenModelView::new)
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    return new TokenModelView();
                });
    }

    @Override
    public Observable<ScoreTokenListModelView> getTokenListModelView(@NonNull ScoreTokenListModelView model) {
        switch (model.getScoreType()) {
            case FUTURES: {
                return getFutures(model)
                        .map(d -> new ScoreTokenListModelView());
            }
            case TRADE:
            case MAIN:
            default: {
                return null;
//                getResidentIfon()
//                        .map(OrmResidentModel::getUuid)
//                        .onErrorReturn(throwable -> {
//                            IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
//                            return iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_UUID, "");
//                        })
//                        .flatMap(d -> mITokenClient.getTokens(d))
//                        .map(TokensResponse::getResult)
//                        .flatMapIterable(d -> d)
//                        .map(d -> {
//                            OrmTokenModel model = new OrmTokenModel(d);
//                            try {
//                                mITokenRepository.saveTokenInfo(model);
//                            } catch (Exception e) {
//                                LoggerUtils.exception(e);
//                            }
//
//                            return new com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.TokenModelView(model, d.getStartup().getLogo());
//                        })
//                        .toList()
//                        .map(ScoreTokenListModelView::new)
//                        .onErrorReturn(throwable -> {
//                            LoggerUtils.exception(throwable);
//
//                            if (throwable instanceof AccessException) {
//                                clearDate();
//                                throw new AccessException(throwable);
//                            }
//
//                            return new ScoreTokenListModelView();
//                        });
            }
        }
    }

    private Observable<ScoreTokenListModelView> getFutures(@NonNull ScoreTokenListModelView model) {
        return null;
//        Observable<ScoreTokenListModelView> obs1 = mIScoreClient.getFutures(model.getOffset(), model.getTotal())
//                .map(TransactionsResponse::getResult)
//                .map(r -> {
////                    List<Transaction> list = r.getTransactionList();
////                    Observable<List<TokenModelView>> listObservable = Observable.just(list)
////                            .flatMapIterable(list1 -> list1)
////                            .map(d1 -> new TokenModelView(d1))
////                            .toList();
////
////                    return Observable.zip(
////                            listObservable,
////                            Observable.just(d.getTotal()),
////                            LogModelView::new
////                    );
////
////                    ScoreTokenListModelView model1 = new ScoreTokenListModelView(ScoreType.FUTURES);
////                    model1.setTotal(r.getTotal());
////                    model1.setOffset();
//                    return null;
//                });
//
//        Observable<ScoreTokenListModelView> obs2 = Observable.just(model);
//
//        return Observable.zip(obs1, obs2, new Func2<ScoreTokenListModelView, ScoreTokenListModelView, ScoreTokenListModelView>() {
//            @Override
//            public ScoreTokenListModelView call(ScoreTokenListModelView t1, ScoreTokenListModelView t2) {
//                t2.setTotal(t1.getTotal());
//                t2.setOffset(t1.getOffset());
////                List<TokenModelView> list = t2.getTokentModelList();
////                list.addAll(t1.getTokentModelList());
////                t2.setTokentModelList(list);
//                return t2;
//            }
//        });
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
