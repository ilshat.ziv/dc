package com.decenturion.mobile.di.dagger.settings.wallets;

import com.decenturion.mobile.ui.fragment.settings.wallet.view.WalletsSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        WalletsSettingsModule.class
})

@WalletsSettingsScope
public interface WalletsSettingsComponent {

    void inject(WalletsSettingsFragment view);
}
