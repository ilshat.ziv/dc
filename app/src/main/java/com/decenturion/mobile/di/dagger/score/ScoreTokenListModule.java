package com.decenturion.mobile.di.dagger.score;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.score.IScoreTokenListInteractor;
import com.decenturion.mobile.business.score.ScoreTokenListInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter.IScoreTokenListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter.ScoreTokenListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ScoreTokenListModule {

    @Provides
    @ScoreTokenListScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ScoreTokenListScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @ScoreTokenListScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @ScoreTokenListScope
    IScoreTokenListInteractor provideIScoreTokenListInteractor(IResidentClient IResidentClient,
                                                               ITokenClient iTokenClient,
                                                               IScoreClient iScoreClient,
                                                               IResidentRepository iResidentRepository,
                                                               IWalletRepository iWalletRepository,
                                                               ITokenRepository iTokenRepository,
                                                               IPrefsManager iPrefsManager) {

        return new ScoreTokenListInteractor(
                IResidentClient,
                iTokenClient,
                iScoreClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository,
                iPrefsManager
        );
    }

    @Provides
    @ScoreTokenListScope
    IScoreTokenListPresenter provideIScoreTokenListPresenter(IScoreTokenListInteractor iScoreTokenListInteractor) {
        return new ScoreTokenListPresenter(iScoreTokenListInteractor);
    }
}
