package com.decenturion.mobile.business.profile;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.main.model.ProfileModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class ProfileInteractor implements IProfileInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private ILocalistionManager mILocalistionManager;

    public ProfileInteractor(IResidentClient IResidentClient,
                             IResidentRepository iResidentRepository,
                             ILocalistionManager iLocalistionManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<ProfileModelView> getProfileInfo() {
        Observable<ProfileModelView> obs1 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new ProfileModelView(d, mILocalistionManager);
                })
                .onErrorReturn(throwable -> {
                    if (throwable instanceof AccessException) {
                        throw new AccessException();
                    }

                    return null;
                });

        Observable<OrmResidentModel> obs2 = Observable.just(mIResidentRepository.getResidentInfo());
        Observable<OrmPhotoModel> obs3 = Observable.just(mIResidentRepository.getPhotoInfo());

        Observable<ProfileModelView> obs4 = Observable.zip(
                obs2,
                obs3,
                (ormResidentModel, ormPhotoModel) -> new ProfileModelView(ormResidentModel, ormPhotoModel, mILocalistionManager))
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(new Exception("Database exception, entity is null"));
                    throw new RuntimeException("Oops, an error occurred");
                });

        return Observable.concat(obs1, obs4, Observable.error(new RuntimeException("Oops, an error occurred")))
                .takeFirst(d -> d != null);
    }

}
