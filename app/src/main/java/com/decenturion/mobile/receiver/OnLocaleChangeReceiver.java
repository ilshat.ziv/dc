package com.decenturion.mobile.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Process;

public class OnLocaleChangeReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: 30.08.2018 плохое решение для динамического изменения локализации
        Process.killProcess(Process.myPid());
    }
}
