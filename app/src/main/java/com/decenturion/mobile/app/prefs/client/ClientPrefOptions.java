package com.decenturion.mobile.app.prefs.client;

public class ClientPrefOptions {

    public interface Keys {
        String LOCALE = "LOCALE";
        String IS_SKIP_WALKTHROUGH = "IS_SKIP_WALKTHROUGH";
        String SIGN_STEP = "SIGN_STEP";
        String RESIDENT_EMAIL = "RESIDENT_EMAIL";
        String RESIDENT_UUID = "RESIDENT_UUID";
        String RESIDENT_INVITED = "RESIDENT_INVITED";
        String RESIDENT_DELIVERY_PAID = "RESIDENT_DELIVERY_PAID";
        String RESIDENT_DELIVERY_FILL_DATA = "RESIDENT_DELIVERY_FILL_DATA";
        String IS_ACEPT_USER_AGREEMENT = "IS_ACEPT_USER_AGREEMENT";
        String RESIDENT_TWOFA_ENABLED = "RESIDENT_TWOFA_ENABLED";
    }

    public interface Defaults {
        String VALUE = "VALUE";
        String LOCALE = "en_US";
        int AVATAR_WIDTH = 500;
        int AVATAR_HEIGHT = 500;
    }
}
