package com.decenturion.mobile.di.dagger.passport.online;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.passport.online.IOnlinePassportInteractor;
import com.decenturion.mobile.business.passport.online.OnlinePassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.online.presenter.IOnlinePassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.online.presenter.OnlinePassportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class OnlinePassportModule {

    @Provides
    @OnlinePassportScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @OnlinePassportScope
    IOnlinePassportInteractor provideIOnlinePassportInteractor(IResidentClient iResidentClient,
                                                               IResidentRepository iResidentRepository,
                                                               IPrefsManager iPrefsManager) {
        return new OnlinePassportInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @OnlinePassportScope
    IOnlinePassportPresenter provideIOnlinePassportPresenter(Context context,
                                                             IOnlinePassportInteractor iOnlinePassportInteractor,
                                                             ILocalistionManager iLocalistionManager) {
        return new OnlinePassportPresenter(context, iOnlinePassportInteractor, iLocalistionManager);
    }
}
