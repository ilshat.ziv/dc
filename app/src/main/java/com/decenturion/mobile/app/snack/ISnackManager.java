package com.decenturion.mobile.app.snack;

import android.support.annotation.NonNull;

public interface ISnackManager {

    void showShortSnack(@NonNull TypeMessage type, @NonNull String message);

    void showLongSnack(@NonNull TypeMessage type, @NonNull String message);

    void dismissSnackbar();
}
