package com.decenturion.mobile.di.dagger.twofa;

import com.decenturion.mobile.ui.fragment.twofa.view.EnableTwoFAFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        EnableTwoFAModule.class
})

@EnableTwoFAScope
public interface EnableTwoFAComponent {

    void inject(EnableTwoFAFragment view);
}
