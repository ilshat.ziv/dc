package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryItemPrice {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("price")
    private String mPrice;

    @JsonProperty("total_price")
    private String mTotalPrice;

    public MinistryItemPrice() {}

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getTotalPrice() {
        return mTotalPrice;
    }
}
