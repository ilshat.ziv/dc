package com.decenturion.mobile.ui.activity.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SimpleActivity;
import com.decenturion.mobile.ui.fragment.seller.main.view.SellerProfileFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class SellerActivity extends SimpleActivity implements ISellerActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        INavigationManager iNavigationManager = getINavigationManager();
        iNavigationManager.restoreInstanceState(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle extras = null;
            Intent intent = getIntent();
            if (intent != null) {
                extras = intent.getExtras();
            }

            iNavigationManager.navigateTo(SellerProfileFragment.class, extras);
        }
    }


}
