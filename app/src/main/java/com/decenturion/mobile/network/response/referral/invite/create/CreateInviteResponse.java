package com.decenturion.mobile.network.response.referral.invite.create;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateInviteResponse extends BaseResponse {

    @JsonProperty("result")
    private CreateInviteResult mResult;

    public CreateInviteResponse() {
    }

    @JsonIgnore
    public CreateInviteResult getResult() {
        return mResult;
    }
}
