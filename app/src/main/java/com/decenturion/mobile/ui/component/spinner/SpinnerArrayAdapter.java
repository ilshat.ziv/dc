package com.decenturion.mobile.ui.component.spinner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class SpinnerArrayAdapter<I extends ISpinnerItem> extends ArrayAdapter<I> {

    private List<I> mItemList;

    public SpinnerArrayAdapter(@NonNull Context context, int resource, @NonNull List<I> objects) {
        super(context, resource, objects);
        mItemList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        I item = getItem(position);
        assert item != null;
        label.setText(String.valueOf(item.getItemName()));
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        I item = getItem(position);
        assert item != null;
        label.setText(String.valueOf(item.getItemName()));
        return label;
    }

    public int getPositionByItemName(@NonNull String item) {
        for (I i : mItemList) {
            String itemKey = i.getItemKey();
            if (TextUtils.equals(itemKey, item)) {
                return getPosition(i);
            }
        }
        return 0;
    }
}
