package com.decenturion.mobile.ui.fragment.profile.passport.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.passport.IPassportInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.Sex;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.view.IPassportView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PassportPresenter extends Presenter<IPassportView> implements IPassportPresenter {

    private Context mContext;
    private IPassportInteractor mIPassportInteractor;
    private ILocalistionManager mILocalistionManager;

    private PassportModelView mPassportModelView;
    private CountryModelView mCountryModelView;
    private SexModelView mSexModelView;

    public PassportPresenter(Context context, IPassportInteractor iPassportInteractor,
                             ILocalistionManager iLocalistionManager) {
        mContext = context;
        mIPassportInteractor = iPassportInteractor;
        mILocalistionManager = iLocalistionManager;

        mPassportModelView = new PassportModelView();
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<PassportModelView> obs = mIPassportInteractor.getProfileInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PassportModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public PassportModelView getProfileView() {
        return mPassportModelView;
    }

    private void bindViewDataSuccess(PassportModelView model) {
        mPassportModelView = model;

        ArrayList<Sex> sexData = initGenderData();
        mSexModelView = new SexModelView(sexData);

        ArrayList<Country> list = initCountryData();
        mCountryModelView = new CountryModelView(list);

        if (mView != null) {
            mView.onBindViewData(mSexModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    @NonNull
    private ArrayList<Sex> initGenderData() {
        ArrayList<Sex> sexData = new ArrayList<>();
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_male), "male"));
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_female), "female"));
        return sexData;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
