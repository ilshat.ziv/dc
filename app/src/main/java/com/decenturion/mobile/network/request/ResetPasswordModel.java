package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetPasswordModel {

    @JsonProperty("password")
    private String mPassword;

    @JsonProperty("token")
    private String mToken;

    public ResetPasswordModel() {
    }

    @JsonIgnore
    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    @JsonIgnore
    public void setToken(@NonNull String token) {
        mToken = token;
    }
}
