package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaleModel {
    //    ResidentUuid string `form:"resident_uuid" json:"resident_uuid"`
//    CoinID       string `form:"coin_id" json:"coin_id"`
//    Currency     string `form:"currency" json:"currency"`
//    Amount       string `form:"amount" json:"amount"`
//    Category     string `form:"category" json:"category"`
//    coin         *models.Coin


    //    Coin:         *f.coin,
//    Currency:     f.Currency,
//    Category:     f.Category,
//    Amount:       amountText,
//    OnsaleAmount: onsaleAmount,
//    Price:        priceText,
//    TotalPrice:   totalPriceText

    @JsonProperty("resident_uuid")
    private String mResidentUuid;

    @JsonProperty("coin_id")
    private String mCoinId;

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCategory;

    public SaleModel() {
    }

    @JsonIgnore
    public void setResidentUuid(String residentUuid) {
        mResidentUuid = residentUuid;
    }

    @JsonIgnore
    public void setCoinId(String coinId) {
        mCoinId = coinId;
    }

    @JsonIgnore
    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    @JsonIgnore
    public void setAmount(String amount) {
        mAmount = amount;
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCategory = category;
    }
}
