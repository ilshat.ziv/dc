package com.decenturion.mobile.network.response.wallets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckWalletResult {

    @JsonProperty("presence")
    private boolean mPresence;

    public CheckWalletResult() {
    }

    @JsonIgnore
    public boolean isPresence() {
        return mPresence;
    }
}
