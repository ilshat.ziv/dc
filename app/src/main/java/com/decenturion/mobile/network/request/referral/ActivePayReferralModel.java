package com.decenturion.mobile.network.request.referral;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivePayReferralModel {

    @JsonProperty("uuid")
    private String mReferralKey;

    public ActivePayReferralModel() {
    }

    @JsonIgnore
    public ActivePayReferralModel(String referralKey) {
        mReferralKey = referralKey;
    }

    @JsonIgnore
    public void setReferralKey(@NonNull String referralKey) {
        mReferralKey = referralKey;
    }
}
