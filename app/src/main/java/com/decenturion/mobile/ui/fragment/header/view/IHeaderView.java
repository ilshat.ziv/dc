package com.decenturion.mobile.ui.fragment.header.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.header.model.HeaderModelView;

public interface IHeaderView extends IScreenFragmentView {

    void onBindViewData(@NonNull HeaderModelView model);
}
