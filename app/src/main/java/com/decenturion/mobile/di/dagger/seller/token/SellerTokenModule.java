package com.decenturion.mobile.di.dagger.seller.token;

import android.content.Context;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.seller.token.ISellerTokenInteractor;
import com.decenturion.mobile.business.seller.token.SellerTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.wallet.IWalletRepository;
import com.decenturion.mobile.repository.wallet.WalletRepository;
import com.decenturion.mobile.ui.fragment.seller.token.presenter.ISellerTokenPresenter;
import com.decenturion.mobile.ui.fragment.seller.token.presenter.SellerTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SellerTokenModule {

    @Provides
    @SellerTokenScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SellerTokenScope
    IWalletRepository provideIWalletRepository(OrmHelper ormHelper) {
        return new WalletRepository(ormHelper);
    }

    @Provides
    @SellerTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @SellerTokenScope
    ISellerTokenInteractor provideISellerTokenInteractor(IResidentClient IResidentClient,
                                                   ITokenClient iTokenClient,
                                                   IResidentRepository iResidentRepository,
                                                   IWalletRepository iWalletRepository,
                                                   ITokenRepository iTokenRepository) {

        return new SellerTokenInteractor(IResidentClient,
                iTokenClient,
                iResidentRepository,
                iWalletRepository,
                iTokenRepository);
    }

    @Provides
    @SellerTokenScope
    ISellerTokenPresenter provideISellerTokenPresenter(Context context, ISellerTokenInteractor iSellerTokenInteractor) {
        return new SellerTokenPresenter(context, iSellerTokenInteractor);
    }
}
