package com.decenturion.mobile.di.dagger.pass.confirm.email;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.business.restore.RestorePassInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.presenter.ConfirmEmailRestorePassPresenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.email.presenter.IConfirmEmailRestorePassPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ConfirmEmailRestorePassModule {

    @Provides
    @ConfirmEmailRestorePassScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ConfirmEmailRestorePassScope
    IRestorePassInteractor provideIRestorePassInteractor(IResidentClient iResidentClient,
                                                         IResidentRepository iResidentRepository) {
        return new RestorePassInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @ConfirmEmailRestorePassScope
    IConfirmEmailRestorePassPresenter provideIConfirmEmailRestorePassPresenter(
            IRestorePassInteractor iRestorePassInteractor,
            IPrefsManager iPrefsManager) {

        return new ConfirmEmailRestorePassPresenter(iRestorePassInteractor, iPrefsManager);
    }
}
