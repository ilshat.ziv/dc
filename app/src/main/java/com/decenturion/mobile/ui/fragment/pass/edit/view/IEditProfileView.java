package com.decenturion.mobile.ui.fragment.pass.edit.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.EditProfileModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;

public interface IEditProfileView extends IScreenFragmentView {

    void onBindViewData(@NonNull SexModelView model,
                        @NonNull CountryModelView model1,
                        @NonNull EditProfileModelView model2);

    void saveChangesSuccess();
}
