package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangeDetailsModel {

    @JsonProperty("about")
    private String mAbour;

    @JsonProperty("telegram")
    private String mTelegram;

    @JsonProperty("viber")
    private String mViber;

    @JsonProperty("wechat")
    private String mWechat;

    @JsonProperty("whatsapp")
    private String mWhatsapp;

    public ChangeDetailsModel() {
    }

    @JsonIgnore
    public String getAbour() {
        return mAbour;
    }

    @JsonIgnore
    public String getTelegram() {
        return mTelegram;
    }

    @JsonIgnore
    public String getViber() {
        return mViber;
    }

    @JsonIgnore
    public String getWechat() {
        return mWechat;
    }

    @JsonIgnore
    public String getWhatsapp() {
        return mWhatsapp;
    }

    @JsonIgnore
    public void setAbout(String abour) {
        mAbour = abour;
    }

    @JsonIgnore
    public void setTelegram(String telegram) {
        mTelegram = telegram;
    }

    @JsonIgnore
    public void setViber(String viber) {
        mViber = viber;
    }

    @JsonIgnore
    public void setWechat(String wechat) {
        mWechat = wechat;
    }

    @JsonIgnore
    public void setWhatsapp(String whatsapp) {
        mWhatsapp = whatsapp;
    }
}
