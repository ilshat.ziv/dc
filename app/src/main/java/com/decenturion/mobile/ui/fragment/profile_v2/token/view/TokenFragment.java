package com.decenturion.mobile.ui.fragment.profile_v2.token.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.token.TokenComponent;
import com.decenturion.mobile.ui.activity.token.TokenActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile.wallet.StartupAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModel;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.presenter.ITokenPresenter;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class TokenFragment extends BaseFragment implements ITokenView,
        StartupAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.startupListView)
    RecyclerView mStartupListView;
    private StartupAdapter mStartupAdapter;

    /* DI */

    @Inject
    ITokenPresenter mITokenPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TokenComponent tokenComponent = appDelegate
                .getIDIManager()
                .plusTokenComponent();
        tokenComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_token, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mRefreshView.setOnRefreshListener(this);

        Context context = requireContext();
        mStartupListView.setLayoutManager(new LinearLayoutManager(context));
        mStartupAdapter = new StartupAdapter(context, this);
        mStartupListView.setAdapter(mStartupAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITokenPresenter.bindView(this);
        mITokenPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mITokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mITokenPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull TokenModelView model) {
        mStartupAdapter.replaceDataSet(model.getTokentModelList());
    }

    @Override
    public void onSignout() {
    }

    @Override
    public void onItemClick(TokenModel model) {
        Intent intent = new Intent(getActivity(), TokenActivity.class);
        intent.putExtra(TokenActivity.TOKEN_ID, model.getId());
        intent.putExtra(TokenActivity.TOKEN_CATEGORY, model.getTokenCategory());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mITokenPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mITokenPresenter.bindViewData();
    }
}
