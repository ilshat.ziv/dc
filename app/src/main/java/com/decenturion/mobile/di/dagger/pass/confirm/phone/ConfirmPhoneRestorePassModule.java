package com.decenturion.mobile.di.dagger.pass.confirm.phone;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.business.restore.RestorePassInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.presenter.ConfirmPhoneRestorePassPresenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.presenter.IConfirmPhoneRestorePassPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ConfirmPhoneRestorePassModule {

    @Provides
    @ConfirmPhoneRestorePassScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ConfirmPhoneRestorePassScope
    IRestorePassInteractor provideIRestorePassInteractor(IResidentClient iResidentClient,
                                                         IResidentRepository iResidentRepository) {
        return new RestorePassInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @ConfirmPhoneRestorePassScope
    IConfirmPhoneRestorePassPresenter provideIConfirmPhoneRestorePassPresenter(
            IRestorePassInteractor iRestorePassInteractor,
            IPrefsManager iPrefsManager) {

        return new ConfirmPhoneRestorePassPresenter(iRestorePassInteractor, iPrefsManager);
    }
}
