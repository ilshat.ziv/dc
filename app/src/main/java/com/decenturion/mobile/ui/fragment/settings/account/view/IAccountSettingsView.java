package com.decenturion.mobile.ui.fragment.settings.account.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.account.model.AccountSettingsModelView;

public interface IAccountSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull AccountSettingsModelView model);
}
