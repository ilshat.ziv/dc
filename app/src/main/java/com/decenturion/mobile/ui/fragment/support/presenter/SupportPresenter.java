package com.decenturion.mobile.ui.fragment.support.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.support.ISupportInteractor;
import com.decenturion.mobile.business.support.model.SupportMessage;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.support.view.ISupportView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SupportPresenter extends Presenter<ISupportView> implements ISupportPresenter {

    private ISupportInteractor mISupportInteractor;

    public SupportPresenter(ISupportInteractor iSupportInteractor) {
        mISupportInteractor = iSupportInteractor;
    }

    @Override
    public void sendMessage(@NonNull String theme, @NonNull String message) {
        showProgressView();

        SupportMessage supportMessage = new SupportMessage(theme, message);

        Observable<Boolean> obs = mISupportInteractor.sendMessage(supportMessage)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<Boolean>) this::sendMessageSuccess,
                this::sendMessageFailure
        );
    }

    private void sendMessageSuccess(Boolean b) {
        if (mView != null) {
            mView.onSendMessageSuccess();
        }
        hideProgressView();
    }

    private void sendMessageFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
