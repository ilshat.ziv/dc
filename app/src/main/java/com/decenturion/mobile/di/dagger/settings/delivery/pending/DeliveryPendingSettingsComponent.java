package com.decenturion.mobile.di.dagger.settings.delivery.pending;

import com.decenturion.mobile.ui.fragment.settings.delivery.pending.view.DeliveryPendingSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DeliveryPendingSettingsModule.class
})

@DeliveryPendingSettingsScope
public interface DeliveryPendingSettingsComponent {

    void inject(DeliveryPendingSettingsFragment view);
}
