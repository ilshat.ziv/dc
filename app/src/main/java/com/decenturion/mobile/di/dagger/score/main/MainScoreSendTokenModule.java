package com.decenturion.mobile.di.dagger.score.main;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.score.main.IMainScoreSendTokenInteractor;
import com.decenturion.mobile.business.score.main.MainScoreSendTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.main.presenter.IMainScoreSendTokenPresenter;
import com.decenturion.mobile.ui.fragment.score.send.main.presenter.MainScoreSendTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainScoreSendTokenModule {

    @Provides
    @MainScoreSendTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @MainScoreSendTokenScope
    IMainScoreSendTokenInteractor provideIMainScoreSendTokenInteractor(
            IResidentClient iResidentClient,
            IScoreClient iScoreClient,
            ITokenRepository iTokenRepository) {

        return new MainScoreSendTokenInteractor(iResidentClient, iScoreClient, iTokenRepository);
    }

    @Provides
    @MainScoreSendTokenScope
    IMainScoreSendTokenPresenter provideIMainScoreSendTokenPresenter(
            IMainScoreSendTokenInteractor iMainScoreSendTokenInteractor,
            IPrefsManager iPrefsManager,
            ILocalistionManager iLocalistionManager) {

        return new MainScoreSendTokenPresenter(
                iMainScoreSendTokenInteractor,
                iPrefsManager,
                iLocalistionManager);
    }
}
