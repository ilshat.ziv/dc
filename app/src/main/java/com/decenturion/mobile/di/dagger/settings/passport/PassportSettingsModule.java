package com.decenturion.mobile.di.dagger.settings.passport;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.passport.IPassportSettingsInteractor;
import com.decenturion.mobile.business.settings.passport.PassportSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.passport.presenter.IPassportSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.passport.presenter.PassportSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PassportSettingsModule {

    @Provides
    @PassportSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @PassportSettingsScope
    IPassportSettingsInteractor provideIPassportSettingsInteractor(IResidentClient iResidentClient,
                                                                   IResidentRepository iResidentRepository) {
        return new PassportSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @PassportSettingsScope
    IPassportSettingsPresenter provideIPassportSettingsPresenter(Context context,
                                                                 IPassportSettingsInteractor iPassportSettingsInteractor,
                                                                 ILocalistionManager iLocalistionManager) {
        return new PassportSettingsPresenter(context, iPassportSettingsInteractor, iLocalistionManager);
    }
}
