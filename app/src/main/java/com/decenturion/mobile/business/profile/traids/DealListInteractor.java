package com.decenturion.mobile.business.profile.traids;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.trades.TradesResponse;
import com.decenturion.mobile.network.response.trades.TradesResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealModel;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModel;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.List;

import rx.Observable;

public class DealListInteractor implements IDealListInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private IResidentRepository mIResidentRepository;
    private ITokenRepository mITokenRepository;
    private ITraideRepository mITraideRepository;
    private IPrefsManager mIPrefsManager;

    private ILocalistionManager mILocalistionManager;

    public DealListInteractor(IResidentClient IResidentClient,
                              ITokenClient iTokenClient,
                              IResidentRepository iResidentRepository,
                              ITokenRepository iTokenRepository,
                              ITraideRepository iTraideRepository,
                              IPrefsManager iPrefsManager,
                              ILocalistionManager iLocalistionManager) {

        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mIResidentRepository = iResidentRepository;
        mITokenRepository = iTokenRepository;
        mITraideRepository = iTraideRepository;
        mIPrefsManager = iPrefsManager;

        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<DealListModelView> getDealsInfo(@NonNull DealListModelView model) {
        Observable<DealListModelView> obs1 = mITokenClient.getDeals(
                "",
                model.getOffset(),
                model.LIMIT
        )
                .map(TradesResponse::getResult)
                .flatMap(d -> {
                    List<WrapperTrade> list = d.getWrapperTradeList();
                    Observable<List<DealModel>> listObservable = Observable.just(list)
                            .flatMapIterable(list1 -> list1)
                            .map(d1 -> new DealModel(d1, mILocalistionManager))
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(d.getTotal()),
                            DealListModelView::new
                    );
                })
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    throw new RuntimeException(throwable.getMessage());
                });

        Observable<DealListModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (modelView, modelView2) -> {
            modelView2.setOffset(modelView2.getOffset() + modelView2.LIMIT);
            modelView2.addDeals(modelView.getTokentModelList());
            modelView2.setTotal(modelView.getTotal());
            return modelView2;
        });
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
