package com.decenturion.mobile.ui.fragment.seller.token.model;

import java.util.ArrayList;

import com.decenturion.mobile.network.response.model.Coin;

public class CoinModelView {

    private ArrayList<Coin> mCoinData;

    public CoinModelView() {
    }

    public CoinModelView(ArrayList<Coin> countryData) {
        mCoinData = countryData;
    }

    public ArrayList<Coin> getCoinData() {
        return mCoinData;
    }
}
