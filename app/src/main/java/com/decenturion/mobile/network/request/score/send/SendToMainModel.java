package com.decenturion.mobile.network.request.score.send;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendToMainModel {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCoinCategory;

    @JsonProperty("coin")
    private String mCoinUUID;

    @JsonProperty("source_acc")
    private String mSource = "trade";

    @JsonProperty("target_acc")
    private String mTarget = "main";

    @JsonProperty("type")
    private String mMethod;

    @JsonProperty("code")
    private String mTwoFa;

    public SendToMainModel() {
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoinUUID = coin;
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCoinCategory = category;
    }

    @JsonIgnore
    public void setMethod(String method) {
        mMethod = method;
    }

    @JsonIgnore
    public void setAmount(String amount) {
        mAmount = amount;
    }

//    @JsonIgnore
//    public void setSource(String source) {
//        mSource = source;
//    }
//
//    @JsonIgnore
//    public void setTarget(String target) {
//        mTarget = target;
//    }

    @JsonIgnore
    public void setTwoFa(String m2Fa) {
        this.mTwoFa = m2Fa;
    }
}
