package com.decenturion.mobile.business.signup;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.request.CheckInviteModel;
import com.decenturion.mobile.network.request.CheckReferallModel;
import com.decenturion.mobile.network.request.SignupModel;
import com.decenturion.mobile.network.request.referral.ActiveBannerReferralModel;
import com.decenturion.mobile.network.request.referral.ActivePayReferralModel;
import com.decenturion.mobile.network.response.CheckReferallKeyResponse;
import com.decenturion.mobile.network.response.invite.CheckInviteTokenResponse;
import com.decenturion.mobile.network.response.invite.CheckInviteTokenResult;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.referral.active.ActivePayReferralResponse;
import com.decenturion.mobile.network.response.referral.banner.ActiveBannerReferralResponse;
import com.decenturion.mobile.network.response.signup.SignupResponse;
import com.decenturion.mobile.network.response.signup.SignupResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

import rx.Observable;

public class SignupInteractor implements ISignupInteractor {

    private IResidentClient mIResidentClient;
    private IReferralClient mIReferralClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public SignupInteractor(IResidentClient IResidentClient,
                            IReferralClient iReferralClient,
                            IResidentRepository iResidentRepository,
                            IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIReferralClient = iReferralClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<SignupResult> signup(@NonNull String username,
                                           @NonNull String password,
                                           @NonNull String retypePassword,
                                           @NonNull String captcha,
                                           boolean isSubscribe,
                                            @Nullable String inviteCode) {

        SignupModel model = new SignupModel();
        model.setUsername(username);
        model.setPassword(password);
        model.setReCaptcha(captcha);
        model.setSubscribe(isSubscribe);
        model.setInviteCode(inviteCode);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.signup(m))
                .map(SignupResponse::getSigninResult)
                .doOnNext(d -> {
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, d.getStep());

                    try {
                        mIResidentRepository.saveResidentInfo(d);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                })
                .doOnError(throwable -> proccessError(username, throwable));
    }

    private void proccessError(@NonNull String username, Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                String key = error.getKey();
                switch (key) {
                    case "username" : {
                        try {
                            OrmResidentModel resident = new OrmResidentModel();
                            resident.setEmail(username);
                            mIResidentRepository.saveResidentInfo(resident);
                        } catch (Exception e) {
                            LoggerUtils.exception(e);
                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public Observable<String> checkInviteToken(@NonNull String inviteToken) {
        return Observable.just(new CheckInviteModel(inviteToken))
                .flatMap(mIResidentClient::checkTokenInvite)
                .map(CheckInviteTokenResponse::getResult)
                .map(CheckInviteTokenResult::getEmail);
    }

    @Override
    public Observable<Boolean> activeReferallCode(@NonNull String referallKey) {
        return Observable.just(new CheckReferallModel(referallKey))
                .flatMap(mIReferralClient::checkReferallKey)
                .map(CheckReferallKeyResponse::getResult);
    }

    @Override
    public Observable<Boolean> activePayReferralCode(@NonNull String referralKey) {
        return Observable.just(new ActivePayReferralModel(referralKey))
                .flatMap(mIReferralClient::activePayReferкalKey)
                .map(ActivePayReferralResponse::getResult)
                .map(d -> true);
    }

    @Override
    public Observable<Boolean> activeBannerReferralCode(@NonNull String referralKey) {
        return Observable.just(new ActiveBannerReferralModel(referralKey))
                .flatMap(mIReferralClient::activeBannerKey)
                .map(ActiveBannerReferralResponse::getResult);
    }
}
