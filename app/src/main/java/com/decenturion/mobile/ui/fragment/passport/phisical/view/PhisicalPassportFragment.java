package com.decenturion.mobile.ui.fragment.passport.phisical.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.DecenturionConstants;
import com.decenturion.mobile.di.dagger.passport.phisical.PhisicalPassportComponent;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.MainActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.view.ActivationPassportFragment;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.DeliveryDataModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PhisicalPassportModelView;
import com.decenturion.mobile.ui.fragment.passport.phisical.presenter.IPhisicalPassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.WellcomeFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PhisicalPassportFragment extends BaseFragment implements IPhisicalPassportView,
        SpinnerView_v2.OnSpinnerItemClickListener,
        ChangeComponentListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.titleView)
    TextView mTitleView;


    @BindView(R.id.payPassportDataView)
    LinearLayout mPayPassportDataView;

    @BindView(R.id.payStateView)
    LocalizedTextView mPayStateView;

    @BindView(R.id.summPayView)
    BuyTokenView mSummPayView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    @BindView(R.id.payActionView)
    LinearLayout mPayActionView;


    @BindView(R.id.deliveryPassportDataView)
    LinearLayout mDeliveryPassportDataView;


    @BindView(R.id.pasportDataView)
    LinearLayout mPasportDataView;

    @BindView(R.id.phoneView)
    EditTextView mPhoneView;

    @BindView(R.id.addressView)
    EditTextView mAddressView;

    @BindView(R.id.cityView)
    EditTextView mCityView;

    @BindView(R.id.stateView)
    EditTextView mStateView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.zipView)
    EditTextView mZipView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mProceedButtonView;

    @BindView(R.id.skipButtonView)
    LocalizedButtonView mSkipButtonView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IPrefsManager mIPrefsManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IPhisicalPassportPresenter mIPhisicalPassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        PhisicalPassportComponent phisicalPassportComponent = appDelegate
                .getIDIManager()
                .plusPhisicalPassportComponent();
        phisicalPassportComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_phisical_passport, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mProceedButtonView.setEnabled(false);

        String localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_cost_title);
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean param = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_INVITED, false);
        if (param) {
            mTitleView.setText(localeString.replace("{passportCost}", DecenturionConstants.DELIVERY_PAY));
        } else {
            mTitleView.setText(localeString.replace("{passportCost}", "0"));
        }

        mSummPayView.showLabel();
        mSummPayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_amount_title));
        mSummPayView.simpleMode();
        mSummPayView.setVisibleDivider(true);

        // TODO: 19.09.2018 костыль, слишком сложная структура перевода
        localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();
        mSelectCoinView.setOnSpinnerItemClickListener(this);

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_phone_title));
        mPhoneView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_phone_placeholder));
        mPhoneView.editMode();
        mPhoneView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mPhoneView.setVisibleDivider(true);
        mPhoneView.setChangeDataModelListener(this);

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_address_title));
        mAddressView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_address_placeholder));
        mAddressView.editMode();
        mAddressView.setVisibleDivider(true);
        mAddressView.setChangeDataModelListener(this);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_city_title));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_city_placeholder));
        mCityView.editMode();
        mCityView.setVisibleDivider(true);
        mCityView.setChangeDataModelListener(this);

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_region_title));
        mStateView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_region_placeholder));
        mStateView.editMode();
        mStateView.setVisibleDivider(true);
        mStateView.setChangeDataModelListener(this);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_country_title));
        mCountryView.setVisibleDivider(true);

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_zip_title));
        mZipView.setHint(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_zip_placeholder));
        mZipView.editMode();
        mZipView.setVisibleDivider(true);
        mZipView.setChangeDataModelListener(this);

        mQrCodeViewComponent.hideLabel();
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPhisicalPassportPresenter.bindView(this);
        mIPhisicalPassportPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPhisicalPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIPhisicalPassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_in);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_signin));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_offline_passport, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_in: {
                FragmentActivity activity = requireActivity();
                Intent intent = new Intent(activity, SignActivity.class);
                startActivity(intent);
                activity.finish();

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
        mIPhisicalPassportPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model2,
                               @NonNull CountryModelView model1,
                               @NonNull PhisicalPassportModelView model) {
        if (model.isInvited()) {
            mSkipButtonView.setVisibility(View.VISIBLE);
        } else {
            mProceedButtonView.setVisibility(View.VISIBLE);
        }

        mCountryView.setData(
                mILocalistionManager.getLocaleString(R.string.app_offlinepassport_delivery_country_placeholder),
                model1.getCountryData());

        if (model.isInvited()) {
            ArrayList<Coin> coinData = model2.getCoinData();
            Coin coin = coinData.get(0);
            mSelectCoinView.setData(coinData);
            mSelectCoinView.selectItem(coin.getItemKey());

            // по мере оплаты
            if (model.isActive()) {
                mSkipButtonView.setVisibility(View.GONE);
                mProceedButtonView.setVisibility(View.VISIBLE);

                mPayStateView.setText(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_text_success));
                mPayActionView.setVisibility(View.GONE);
                mDeliveryPassportDataView.setVisibility(View.VISIBLE);
                mPasportDataView.setVisibility(View.VISIBLE);
            } else {
                mPayStateView.setText(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_text_pay));
                mPayActionView.setVisibility(View.VISIBLE);
                mDeliveryPassportDataView.setVisibility(View.GONE);
                mPasportDataView.setVisibility(View.GONE);
            }
        } else {
            mPayStateView.setText(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_text_free));
            mDeliveryPassportDataView.setVisibility(View.VISIBLE);
            mPasportDataView.setVisibility(View.VISIBLE);

            DeliveryDataModelView deliverDataModelView = model.getDeliverDataModelView();
            mPhoneView.setText(deliverDataModelView.getPhone());
            mAddressView.setText(deliverDataModelView.getAddress());
            mCityView.setText(deliverDataModelView.getCity());
            mStateView.setText(deliverDataModelView.getState());
            mCountryView.selectItem(deliverDataModelView.getCountry());
            mZipView.setText(deliverDataModelView.getZip());

            mProceedButtonView.setVisibility(View.VISIBLE);
        }

        if (model.isActive()) {
            FragmentActivity activity = requireActivity();
            Intent intent = new Intent(activity, MainActivity.class);
            startActivity(intent);
            activity.finish();
        }
    }

    private void onBindPayModelView(PhisicalPassportModelView model) {
        PaymentModelView paymentModelView = model.getPaymentModelView();

        if (model.isActive()) {
            mPayStateView.setText(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_text_success));
            mProceedButtonView.setVisibility(View.VISIBLE);
            mDeliveryPassportDataView.setVisibility(View.VISIBLE);
            mPasportDataView.setVisibility(View.VISIBLE);
        }

        if (model.isInvited()) {
            if (!model.isActive()) {
                String summPay = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_amount_value);
                summPay = summPay.replace("{amount}", paymentModelView.getCoinsPay());
                summPay = summPay.replace("{currency}", paymentModelView.getTypePay());
                mSummPayView.setText(summPay);
                mSummPayView.setCoin(paymentModelView.getCyrrencePay());

                mQrCodeViewComponent.setAddress(paymentModelView.getAddress());
                mQrCodeViewComponent.setQrData(paymentModelView.getQrCode());

                mPayActionView.setVisibility(View.VISIBLE);
            } else {
                mPayActionView.setVisibility(View.GONE);
            }
        } else {
            mPayActionView.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        mIPhisicalPassportPresenter.setDeliveryData(
                mPhoneView.getValue(),
                mAddressView.getValue(),
                mCityView.getValue(),
                mStateView.getValue(),
                mCountryView.getSelection(),
                mZipView.getValue()
        );
    }

    @OnClick(R.id.shareButtonView)
    protected void onShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, BuildConfig.HOST);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, mILocalistionManager.getLocaleString(R.string.app_offlinepassport_share)));
    }

    @OnClick(R.id.skipButtonView)
    protected void onSkip() {
        mIPhisicalPassportPresenter.skipDelivery();
    }

    @Override
    public void onDeliveryPassportDataSeted(@NonNull PhisicalPassportModelView model) {
        if (!model.isActive() && !model.isInvited()) {
            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity.getINavigationManager();
            iNavigationManager.navigateTo(ActivationPassportFragment.class);
        } else if (model.isInvited() || model.isActive()) {
            mPayPassportDataView.setVisibility(View.GONE);
            mDeliveryPassportDataView.setVisibility(View.VISIBLE);
            mPasportDataView.setVisibility(View.VISIBLE);
            mProceedButtonView.setVisibility(View.VISIBLE);

            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity.getINavigationManager();
            iNavigationManager.navigateTo(WellcomeFragment.class);
        }
    }

    @Override
    public void onBindPaymentViewData(PhisicalPassportModelView modelView) {
        onBindPayModelView(modelView);
    }

    @Override
    public void onSkipedDelivery() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(WellcomeFragment.class);
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mIPhisicalPassportPresenter.getPaymentAddress(iSpinnerItem.getItemKey());
    }

    @Override
    public void onChangeComponentData(View view) {
        boolean b = !TextUtils.isEmpty(mPhoneView.getValue()) &&
                !TextUtils.isEmpty(mAddressView.getValue()) &&
                !TextUtils.isEmpty(mCityView.getValue()) &&
                !TextUtils.isEmpty(mStateView.getValue()) &&
                !TextUtils.isEmpty(mZipView.getValue()) &&
                !TextUtils.isEmpty(mCountryView.getSelection());

        mProceedButtonView.setEnabled(b);
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
