package com.decenturion.mobile.di.dagger.settings.delivery.pending;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.delivered.DeliveredSettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.delivered.IDeliveredSettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.panding.DeliveryPendingSettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.panding.IDeliveryPendingSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter.DeliveredSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter.IDeliveredSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.presenter.DeliveryPendingSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.presenter.IDeliveryPendingSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DeliveryPendingSettingsModule {

    @Provides
    @DeliveryPendingSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @DeliveryPendingSettingsScope
    IDeliveryPendingSettingsInteractor provideIDeliveryPendingSettingsInteractor(IResidentClient iResidentClient,
                                                                           IResidentRepository iResidentRepository) {
        return new DeliveryPendingSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @DeliveryPendingSettingsScope
    IDeliveryPendingSettingsPresenter provideIDeliveryPendingSettingsPresenter(IDeliveryPendingSettingsInteractor iDeliveryPendingSettingsInteractor) {
        return new DeliveryPendingSettingsPresenter(iDeliveryPendingSettingsInteractor);
    }
}
