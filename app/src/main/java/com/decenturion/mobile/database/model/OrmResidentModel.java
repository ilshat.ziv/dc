package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.Resident;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_resident_model")
public class OrmResidentModel {

    private final static String ID = "id";
    private final static String UUID = "uuid";
    private final static String STATUS = "status";
    private final static String EMAIL = "email";
    private final static String PHONE = "phone";
    private final static String SUBSCRIBED = "subscribed";
    private final static String DCNT = "dcnt";
    private final static String BALANCE = "balance";
    private final static String STEP = "step";
    private final static String G2FA = "g2fa";
    private final static String POSITION = "position";
    private final static String MONEY = "money";
    private final static String POWER = "power";
    private final static String GLORY = "glory";
    private final static String ABOUT = "about";
    private final static String TWITTER = "twitter";
    private final static String MEDIUM = "medium";
    private final static String TELEGRAM = "telegram";
    private final static String FACEBOOK = "facebook";
    private final static String WECHAT = "wechat";
    private final static String WHATSAPP = "whatsapp";
    private final static String VIBER = "viber";

    private final static String INVITED = "invited";
    private final static String ACTIVE = "active";
    private final static String DELIVERY_PAID = "delivery_paid";

    private final static String REFERRAL_ID = "referral_id";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = UUID)
    private String mUuid;

    @DatabaseField(dataType = DataType.STRING, columnName = STATUS)
    private String mStatus;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = EMAIL)
    private String mEmail;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = PHONE)
    private String mPhone;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = SUBSCRIBED)
    private boolean isSubscribed;

    @DatabaseField(dataType = DataType.STRING, columnName = DCNT)
    private String mWalletNum;

    @DatabaseField(dataType = DataType.STRING, columnName = BALANCE)
    private String mBalance;

    @DatabaseField(dataType = DataType.DOUBLE, columnName = STEP)
    private double mStep;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = G2FA)
    private boolean isG2fa;

    @DatabaseField(dataType = DataType.STRING, columnName = POSITION)
    private String mPosition;

    @DatabaseField(dataType = DataType.STRING, columnName = MONEY)
    private String mMoney;

    @DatabaseField(dataType = DataType.STRING, columnName = POWER)
    private String mPower;

    @DatabaseField(dataType = DataType.STRING, columnName = GLORY)
    private String mGlory;

    @DatabaseField(dataType = DataType.STRING, columnName = ABOUT)
    private String mAbout;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = TWITTER)
    private String mTwitter;

    @DatabaseField(dataType = DataType.STRING, columnName = MEDIUM)
    private String mMedium;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = TELEGRAM)
    private String mTelegram;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = FACEBOOK)
    private String mFacebook;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = INVITED)
    private boolean isInvited;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = ACTIVE)
    private boolean isActive;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = DELIVERY_PAID)
    private boolean isDeliveryPaid;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = VIBER)
    private String mViber;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = WECHAT)
    private String mWechat;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = WHATSAPP)
    private String mWhatsApp;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = REFERRAL_ID)
    private String mReferralId;

    public OrmResidentModel() {
        mId = 1;
    }

    public OrmResidentModel(@NonNull Resident resident) {
        mId = 1;
        mUuid = resident.getUuid();
        mStatus = resident.getStatus();
        mEmail = resident.getEmail();
        mPhone = resident.getPhone();
        this.isSubscribed = resident.isSubscribed();
        mWalletNum = resident.getWalletNum();
        mBalance = resident.getBalance();
        mStep = resident.getStep();
        this.isG2fa = resident.isG2fa();
        mPosition = resident.getPosition();
        mMoney = resident.getMoney();
        mPower = resident.getPower();
        mGlory = resident.getGlory();
        mAbout = resident.getAbout();
        mTwitter = resident.getTwitter();
        mMedium = resident.getMedium();
        mTelegram = resident.getTelegram();
        mFacebook = resident.getFacebook();
        mWechat = resident.getWechat();
        mWhatsApp = resident.getWhatsapp();
        mViber = resident.getViber();

        this.isActive = resident.isActive();
        this.isInvited = resident.isInvited();
        this.isDeliveryPaid = resident.isDeliveryPaid();
        mReferralId = resident.getReferralId();
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getUuid() {
        return mUuid;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public boolean isSubscribed() {
        return this.isSubscribed;
    }

    public String getWalletNum() {
        return mWalletNum;
    }

    public String getBalance() {
        return mBalance;
    }

    public double getStep() {
        return mStep;
    }

    public boolean isG2fa() {
        return isG2fa;
    }

    public String getPosition() {
        return mPosition;
    }

    public String getMoney() {
        return mMoney;
    }

    public String getPower() {
        return mPower;
    }

    public String getGlory() {
        return mGlory;
    }

    public String getAbout() {
        return mAbout;
    }

    public String getTwitter() {
        return mTwitter;
    }

    public String getMedium() {
        return mMedium;
    }

    public String getTelegram() {
        return mTelegram;
    }

    public String getFacebook() {
        return mFacebook;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public String getViber() {
        return mViber;
    }

    public String getWechat() {
        return mWechat;
    }

    public String getWhatsApp() {
        return mWhatsApp;
    }

    public void setSubscribed(boolean subscribed) {
        this.isSubscribed = subscribed;
    }

    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }

    public void setG2fa(boolean g2fa) {
        this.isG2fa = g2fa;
    }

    public String getReferralId() {
        return mReferralId;
    }

    public String getStatus() {
        return mStatus;
    }
}
