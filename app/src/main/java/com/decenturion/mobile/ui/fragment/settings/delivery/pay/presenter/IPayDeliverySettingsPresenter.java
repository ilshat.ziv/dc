package com.decenturion.mobile.ui.fragment.settings.delivery.pay.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.view.IPayDeliverySettingsView;

public interface IPayDeliverySettingsPresenter extends IPresenter<IPayDeliverySettingsView> {

    void bindViewData();

    void getPaymentAddress(@NonNull String coin);
}
