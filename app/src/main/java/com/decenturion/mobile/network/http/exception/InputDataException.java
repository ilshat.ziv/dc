package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;
import com.decenturion.mobile.network.response.BadResponse;
import com.decenturion.mobile.network.response.model.Error;

import java.util.ArrayList;

public class InputDataException extends AppException {

    private BadResponse mErrorResponse;

    public InputDataException(BadResponse response) {
        mErrorResponse = response;
    }

    public InputDataException(String message) {
        super(message);
    }

    public ArrayList<Error> getErrorList() {
        if (mErrorResponse != null) {
            return mErrorResponse.getResult();
        } else {
            return null;
        }
    }

    public String getStatus() {
        if (mErrorResponse != null) {
            return mErrorResponse.getStatus();
        } else {
            return null;
        }
    }

    @Override
    public String getMessage() {
        ArrayList<Error> result = mErrorResponse.getResult();
        Error error = result.get(0);
        return error.getMessage();
    }
}
