package com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.delivery.IDeliverySettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.view.IDeliverySettingsView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DeliverySettingsPresenter extends Presenter<IDeliverySettingsView> implements IDeliverySettingsPresenter {

    private Context mContext;
    private IDeliverySettingsInteractor mIDeliverySettingsInteractor;
    private ILocalistionManager mILocalistionManager;

    private DeliverySettingsModelView mDeliverySettingsModelView;
    private CountryModelView mCountryModelView;
    private CoinModelView mCoinModelView;

    public DeliverySettingsPresenter(Context context,
                                     IDeliverySettingsInteractor iDeliverySettingsInteractor,
                                     ILocalistionManager iLocalistionManager) {
        mContext = context;
        mIDeliverySettingsInteractor = iDeliverySettingsInteractor;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDeliverySettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<DeliverySettingsModelView> obs = mIDeliverySettingsInteractor.getPassportInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliverySettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void setDeliveryData(@NonNull String value,
                                @NonNull String value1,
                                @NonNull String value2,
                                @NonNull String value3,
                                @NonNull String selection,
                                @NonNull String value4) {

        showProgressView();

        Observable<DeliverySettingsModelView> obs = mIDeliverySettingsInteractor.setDeliveryData(
                value,
                value1,
                value2,
                value3,
                selection,
                value4
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliverySettingsModelView>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(DeliverySettingsModelView result) {
        hideProgressView();

        if (mView != null) {
            mView.onDeliveryPassportDataSeted();
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    private void bindViewDataSuccess(DeliverySettingsModelView model) {
        mDeliverySettingsModelView = model;

        ArrayList<Coin> list = initCoinData();
        mCoinModelView = new CoinModelView(list);

        ArrayList<Country> list2 = initCountryData();
        mCountryModelView = new CountryModelView(list2);

        if (mView != null) {
            mView.onBindViewData(mCoinModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    private ArrayList<Coin> initCoinData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/payment.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }

    @Override
    public void getPaymentDate(@NonNull String coin) {
        if (TextUtils.isEmpty(coin)) {
            return;
        }

        showProgressView();

        Observable<PaymentModelView> obs = mIDeliverySettingsInteractor.getPaymentAddress(coin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PaymentModelView>) this::genPaymentAddressSuccess,
                this::genPaymentAddressFailure
        );
    }

    private void genPaymentAddressSuccess(PaymentModelView address) {
        if (mView != null) {
            mView.onBindPayModelView(address);
        }
        hideProgressView();
    }

    private void genPaymentAddressFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

}
