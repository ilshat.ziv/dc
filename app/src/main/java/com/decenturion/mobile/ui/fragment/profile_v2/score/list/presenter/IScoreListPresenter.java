package com.decenturion.mobile.ui.fragment.profile_v2.score.list.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.view.IScoreListView;

public interface IScoreListPresenter extends IPresenter<IScoreListView> {

    void bindViewData();
}
