package com.decenturion.mobile.di.dagger.settings.delivery.delivered;

import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view.DeliveredSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DeliveredSettingsModule.class
})

@DeliveredSettingsScope
public interface DeliveredSettingsComponent {

    void inject(DeliveredSettingsFragment view);
}
