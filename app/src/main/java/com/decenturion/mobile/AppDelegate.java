package com.decenturion.mobile;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.di.IDIManager;
import com.decenturion.mobile.utils.LoggerUtils;

public class AppDelegate extends Application {

    private IDIManager mIDIManager;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            StrictMode.enableDefaults();
            Timber.plant(new Timber.DebugTree());
        } else {
            Fabric.with(this, new Crashlytics());
        }

        mIDIManager = new DIManager(this);
    }

    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        try {
            MultiDex.install(this);
        } catch (RuntimeException e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mIDIManager.onTerminate();
        mIDIManager = null;
    }

    public IDIManager getIDIManager() {
        return mIDIManager;
    }
}
