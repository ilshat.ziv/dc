package com.decenturion.mobile.di.dagger.passport.phisical;

import com.decenturion.mobile.ui.fragment.passport.phisical.view.PhisicalPassportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PhisicalPassportModule.class
})

@PhisicalPassportScope
public interface PhisicalPassportComponent {

    void inject(PhisicalPassportFragment view);
}
