package com.decenturion.mobile.network.response.support;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportResult {

    @JsonProperty("subject")
    private String mSubject;

    @JsonProperty("message")
    private String mMessage;

    public SupportResult() {
    }

    @JsonIgnore
    public String getSubject() {
        return mSubject;
    }

    @JsonIgnore
    public String getMessage() {
        return mMessage;
    }
}
