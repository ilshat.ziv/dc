package com.decenturion.mobile.di.dagger.passport;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.edit.IEditProfileInteractor;
import com.decenturion.mobile.business.passport.IPassportInteractor;
import com.decenturion.mobile.business.passport.PassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.profile.passport.presenter.IPassportPresenter;
import com.decenturion.mobile.ui.fragment.profile.passport.presenter.PassportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PassportModule {

    @Provides
    @PassportScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @PassportScope
    IPassportInteractor provideIPassportInteractor(IResidentClient iResidentClient,
                                                   IResidentRepository iResidentRepository,
                                                   IPrefsManager iPrefsManager) {
        return new PassportInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @PassportScope
    IPassportPresenter provideIPassportPresenter(Context context,
                                                 IPassportInteractor iPassportInteractor,
                                                 ILocalistionManager iLocalistionManager) {
        return new PassportPresenter(context, iPassportInteractor, iLocalistionManager);
    }
}
