package com.decenturion.mobile.ui.fragment.seller.trade.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.seller.traids.ISellerTraidsInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModelView;
import com.decenturion.mobile.ui.fragment.seller.trade.view.ISellerTraidsView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SellerTraidsPresenter extends Presenter<ISellerTraidsView> implements ISellerTraidsPresenter {

    private ISellerTraidsInteractor mISellerTraidsInteractor;
    private SellerTraideModelView mSellerTraideModelView;

    public SellerTraidsPresenter(ISellerTraidsInteractor iSellerTraidsInteractor) {
        mISellerTraidsInteractor = iSellerTraidsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mSellerTraideModelView = null;
    }

    @Override
    public void bindViewData(@NonNull String sellerUUID) {
        Observable<SellerTraideModelView> obs = mISellerTraidsInteractor.getTraidsModelView(sellerUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerTraideModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SellerTraideModelView model) {
        mSellerTraideModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
