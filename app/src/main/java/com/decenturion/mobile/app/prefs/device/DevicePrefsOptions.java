package com.decenturion.mobile.app.prefs.device;

public class DevicePrefsOptions {

    public interface Keys {
        String REMOTE_SERVER_TIME = "remoteServerTime";
        String LOCAL_TIME = "LocalTime";
        String APP_VERSION_CODE = "APP_VERSION_CODE";
        String APP_LOCALE = "APP_LOCALE";
        String SUPPORT_MIN_VERSION = "SUPPORT_MIN_VERSION";
    }

    public interface Defaults {
    }
}
