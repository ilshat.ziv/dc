package com.decenturion.mobile.di.dagger.settings.delivery;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.DeliverySettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.IDeliverySettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter.DeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter.IDeliverySettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DeliverySettingsModule {

    @Provides
    @DeliverySettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @DeliverySettingsScope
    IDeliverySettingsInteractor provideIDeliverySettingsInteractor(IResidentClient iResidentClient,
                                                                   IResidentRepository iResidentRepository) {
        return new DeliverySettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @DeliverySettingsScope
    IDeliverySettingsPresenter provideIDeliverySettingsPresenter(Context context,
                                                                 IDeliverySettingsInteractor iDeliverySettingsInteractor,
                                                                 ILocalistionManager iLocalistionManager) {
        return new DeliverySettingsPresenter(context, iDeliverySettingsInteractor, iLocalistionManager);
    }
}
