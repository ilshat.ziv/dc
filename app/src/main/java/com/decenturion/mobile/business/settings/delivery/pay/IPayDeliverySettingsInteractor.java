package com.decenturion.mobile.business.settings.delivery.pay;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PayDeliverySettingsModelView;

import rx.Observable;

public interface IPayDeliverySettingsInteractor {

    Observable<PayDeliverySettingsModelView> getDeliveryData();

    Observable<PayDeliverySettingsModelView> genPaymentAddress(@NonNull String coin);
}
