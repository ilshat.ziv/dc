package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryItem {

    @JsonProperty("bch")
    private MinistryItemPrice mBch;

    @JsonProperty("btc")
    private MinistryItemPrice mBtc;

    @JsonProperty("eth")
    private MinistryItemPrice mEth;

    @JsonProperty("usdt")
    private MinistryItemPrice mUsdt;

    public MinistryItem() {}

    @JsonIgnore
    public MinistryItemPrice getBch() {
        return mBch;
    }

    @JsonIgnore
    public MinistryItemPrice getBtc() {
        return mBtc;
    }

    @JsonIgnore
    public MinistryItemPrice getEth() {
        return mEth;
    }

    @JsonIgnore
    public MinistryItemPrice getUsdt() {
        return mUsdt;
    }
}
