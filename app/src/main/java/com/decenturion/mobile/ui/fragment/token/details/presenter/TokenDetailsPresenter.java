package com.decenturion.mobile.ui.fragment.token.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.tokendetails.details.ITokenDetailsInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.details.model.TokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.token.details.view.ITokenDetailsView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TokenDetailsPresenter extends Presenter<ITokenDetailsView> implements ITokenDetailsPresenter {

    private ITokenDetailsInteractor mITokenDetailsInteractor;
    private TokenDetailsModelView mTokenDetailsModelView;

    public TokenDetailsPresenter(ITokenDetailsInteractor iTokenDetailsInteractor) {
        mITokenDetailsInteractor = iTokenDetailsInteractor;
        mTokenDetailsModelView = new TokenDetailsModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {

    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    @Override
    public TokenDetailsModelView getModelView() {
        return mTokenDetailsModelView;
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mTokenDetailsModelView = new TokenDetailsModelView(tokenId);
        Observable<TokenDetailsModelView> obs = mITokenDetailsInteractor.getTokenDetails(tokenId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory) {
        showProgressView();

        mTokenDetailsModelView = new TokenDetailsModelView(tokenId);
        mTokenDetailsModelView.setResidentUUID(residentUuid);
        mTokenDetailsModelView.setCoinUUID(tokenCoinUuid);
        mTokenDetailsModelView.setCategory(tokenCategory);

        Observable<TokenDetailsModelView> obs = mITokenDetailsInteractor.getTokenDetails(mTokenDetailsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TokenDetailsModelView model) {
        mTokenDetailsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
