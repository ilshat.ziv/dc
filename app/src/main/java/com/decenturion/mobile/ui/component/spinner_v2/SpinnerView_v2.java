package com.decenturion.mobile.ui.component.spinner_v2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.component.UBaseView;
import com.decenturion.mobile.ui.component.spinner.EmptyItem;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner.SpinnerArrayAdapter;

import java.util.ArrayList;

import butterknife.BindView;

public class SpinnerView_v2<I extends ISpinnerItem> extends UBaseView {

    /* UI */

    @Nullable
    @BindView(R.id.spinnerView)
    Spinner mSpinnerView;

    private SpinnerArrayAdapter<I> mSpinnerArrayAdapter;
    private ArrayList<I> mData = new ArrayList<>();

    private OnSpinnerItemClickListener mOnSpinnerItemClickListener;

    public SpinnerView_v2(Context context) {
        super(context);
    }

    public SpinnerView_v2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerView_v2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnSpinnerItemClickListener(OnSpinnerItemClickListener listener) {
        mOnSpinnerItemClickListener = listener;
    }

    @Override
    protected boolean isInitView() {
        return mLabelView != null && mSpinnerView != null;
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {
        init();
        this.setOnClickListener(v -> {
            assert mSpinnerView != null;
            mSpinnerView.performClick();
        });
        assert mSpinnerView != null;
        mSpinnerView.setEnabled(true);
    }

    @Override
    public void simpleMode() {
        init();
        this.setOnClickListener(null);
        assert mSpinnerView != null;
        mSpinnerView.setEnabled(false);
    }

    public void setData(String hint, ArrayList<I> data) {
        init();

        if (!isInitView()) {
            return;
        }

        if (hint != null) {
            data.add(0, (I) new EmptyItem(hint));
        }

        if (mSpinnerArrayAdapter == null) {
            mSpinnerArrayAdapter = new SpinnerArrayAdapter<>(getContext(), R.layout.view_spinner_item, mData);
            mSpinnerArrayAdapter.setDropDownViewResource(R.layout.view_spinner_dropdown_item);
            assert mSpinnerView != null;
            mSpinnerView.setAdapter(mSpinnerArrayAdapter);
            mSpinnerView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mOnSpinnerItemClickListener != null) {
                        mOnSpinnerItemClickListener.onSpinnerItemClick(mData.get(position));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        mData.clear();
        mData.addAll(data);
        mSpinnerArrayAdapter.notifyDataSetChanged();
    }


    public void setData(ArrayList<I> data) {
        setData(null, data);
    }

    public void selectItem(@NonNull String item) {
        int position = mSpinnerArrayAdapter.getPositionByItemName(item);
        assert mSpinnerView != null;
        mSpinnerView.setSelection(position);
    }

    public void selectItem(@NonNull I item) {
        int position = mSpinnerArrayAdapter.getPositionByItemName(item.getItemKey());
        assert mSpinnerView != null;
        mSpinnerView.setSelection(position);
    }

    public String getSelection() {
        assert mSpinnerView != null;
        Object selectedItem1 = mSpinnerView.getSelectedItem();
        if (selectedItem1 == null) {
            return null;
        }
        I selectedItem = (I) selectedItem1;
        return selectedItem.getItemKey();
    }

    public interface OnSpinnerItemClickListener<I extends ISpinnerItem> {

        void onSpinnerItemClick(I i);
    }
}