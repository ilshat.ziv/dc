package com.decenturion.mobile.ui.fragment.settings.password.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.password.IPasswordSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.password.model.PasswordSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.password.view.IPasswordSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class PasswordSettingsPresenter extends Presenter<IPasswordSettingsView> implements IPasswordSettingsPresenter {

    private IPasswordSettingsInteractor mIPasswordSettingsInteractor;
    private ILocalistionManager mILocalistionManager;

    private PasswordSettingsModelView mPasswordSettingsModelView;

    public PasswordSettingsPresenter(IPasswordSettingsInteractor iPasswordSettingsInteractor,
                                     ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mIPasswordSettingsInteractor = iPasswordSettingsInteractor;
        mPasswordSettingsModelView = new PasswordSettingsModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mPasswordSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
//        showProgressView();

        Observable<PasswordSettingsModelView> obs = Observable.just(mPasswordSettingsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PasswordSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(PasswordSettingsModelView model) {
        mPasswordSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveNewPassword(@NonNull String currentPassword,
                                @NonNull String newPassword,
                                @NonNull String retypePassword) {

        showProgressView();

        Observable<Boolean> obs = mIPasswordSettingsInteractor.saveNewPassword(
                currentPassword,
                newPassword,
                retypePassword
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<Boolean>) this::saveNewPasswordSuccess,
                this::saveNewPasswordFailure
        );
    }

    private void saveNewPasswordFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }

        showErrorView(throwable);
    }

    private void saveNewPasswordSuccess(Boolean model) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));
    }

}
