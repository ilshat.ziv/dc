package com.decenturion.mobile.ui.fragment.passport.online.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.di.dagger.passport.online.OnlinePassportComponent;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.CalendarView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.ProfileImageView;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.dialog.SpinnerDateDialog;
import com.decenturion.mobile.ui.dialog.bottomsheet.PhotoDialogFragment;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.passport.activation.view.ActivationPassportFragment;
import com.decenturion.mobile.ui.fragment.passport.online.model.OnlinePassportModelView;
import com.decenturion.mobile.ui.fragment.passport.online.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.passport.online.presenter.IOnlinePassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.phisical.view.PhisicalPassportFragment;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.WellcomeFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class OnlinePassportFragment extends BaseFragment implements IOnlinePassportView,
        ProfileImageView.OnPhotoViewListener,
        ChangeComponentListener,
        SpinnerView.OnSpinnerItemClickListener {

    /* UI */

    @BindView(R.id.firstNameView)
    EditTextView mFirstNameView;

    @BindView(R.id.secondNameView)
    EditTextView mSecondNameView;

    @BindView(R.id.birthdayView)
    CalendarView mBirthdayView;

    @BindView(R.id.sexView)
    SpinnerView mSexView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.cityView)
    EditTextView mCityView;


    @BindView(R.id.photoView)
    ProfileImageView mProfileImageView;

    @BindView(R.id.photoLabelView)
    LocalizedTextView mPhotoLabelView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mProceedButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IPrefsManager mIPrefsManager;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IOnlinePassportPresenter mIOnlinePassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        setHasOptionsMenu(true);

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        OnlinePassportComponent onlinePassportComponent = appDelegate
                .getIDIManager()
                .plusOnlinePassportComponent();
        onlinePassportComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_online_passport, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mFirstNameView.showLabel();
        mFirstNameView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_firstname_title));
        mFirstNameView.setHint(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_firstname_placeholder));
        mFirstNameView.editMode();
        mFirstNameView.setVisibleDivider(true);
        mFirstNameView.setChangeDataModelListener(this);

        mSecondNameView.showLabel();
        mSecondNameView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_lastname_title));
        mSecondNameView.setHint(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_lastname_placeholder));
        mSecondNameView.editMode();
        mSecondNameView.setVisibleDivider(true);
        mSecondNameView.setChangeDataModelListener(this);

        mBirthdayView.showLabel();
        mBirthdayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_dateofbirth_title));
        mBirthdayView.setHint(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_dateofbirth_placeholder));
        mBirthdayView.simpleMode();
        mBirthdayView.setVisibleDivider(true);
        mBirthdayView.setChangeDataModelListener(this);

        mSexView.showLabel();
        mSexView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_sex_title));
        mSexView.setVisibleDivider(true);
        mSexView.setOnSpinnerItemClickListener(this);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_country_title));
        mCountryView.setVisibleDivider(true);
        mCountryView.setOnSpinnerItemClickListener(this);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_city_title));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_city_placeholder));
        mCityView.editMode();
        mCityView.setVisibleDivider(true);
        mCityView.setChangeDataModelListener(this);

        mProfileImageView.setOnPhotoViewListener(this);
        mProfileImageView.editMode();

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnlinePassportPresenter.bindView(this);
        mIOnlinePassportPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIOnlinePassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIOnlinePassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_in);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_signin));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_online_passport, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_in : {
                FragmentActivity activity = requireActivity();
                Intent intent = new Intent(activity, SignActivity.class);
                startActivity(intent);
                activity.finish();

                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIOnlinePassportPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mProfileImageView.getResult(requestCode, resultCode, data);
    }

    @Override
    public void onBindViewData(@NonNull SexModelView model2,
                               @NonNull CountryModelView model1,
                               @NonNull OnlinePassportModelView model) {
        mProceedButtonView.setEnabled(false);

        mSexView.setData(
                mILocalistionManager.getLocaleString(R.string.app_onlinepassport_sex_placeholder),
                model2.getSexData()
        );
        mCountryView.setData(
                mILocalistionManager.getLocaleString(R.string.app_onlinepassport_country_placeholder),
                model1.getCountryData()
        );

        mFirstNameView.setText(model.getFirstname());
        mSecondNameView.setText(model.getLastname());

        String birth = model.getBirth();
        if (!TextUtils.isEmpty(birth)) {
            long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.SIMPLE_FORMAT);
            mBirthdayView.changeData(datetime);
        }

        mSexView.selectItem(model.getSex());
        mCountryView.selectItem(model.getCountry());
        mCityView.setText(model.getCity());

        PhotoModelView photoModelView = model.getPhotoModelView();
        if (photoModelView != null) {
            mProfileImageView.addUrlImage(photoModelView.getOriginal());
        }
    }

    @Override
    public void onSavedChanges(UpdatePassportResult result) {
        Resident resident = result.getResident();
        int step = resident.getStep();
        switch (step) {
            case -1 :
            case 1 :
            case 2 :{
                break;
            }
            case 3 : {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(PhisicalPassportFragment.class);
                break;
            }
            case 4 : {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(ActivationPassportFragment.class);
                break;
            }
            case 5 :
            case 6 :
            default : {
                ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(WellcomeFragment.class);
                break;
            }
        }
    }

    @OnClick(R.id.birthdayView)
    public void onBirth() {
        long date = DateTimeUtils.convertToLocalGMT(mBirthdayView.getDate());
        String title = mILocalistionManager.getLocaleString(R.string.app_onlinepassport_dateofbirth_title);
        SpinnerDateDialog dateDialog = new SpinnerDateDialog(mBirthdayView, date, title);
        FragmentManager fragmentManager = requireFragmentManager();
        dateDialog.show(fragmentManager, SpinnerDateDialog.class.getSimpleName());
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        mIOnlinePassportPresenter.setPassportData(
                mProfileImageView.getImage() != null
                        ? mProfileImageView.getImage().toString()
                        : mProfileImageView.getUrlImage(),

                mFirstNameView.getValue(),
                mSecondNameView.getValue(),
                mBirthdayView.getDate(),
                mSexView.getSelection(),
                mCountryView.getSelection(),
                mCityView.getValue());
    }

    @OnClick(R.id.shareButtonView)
    protected void onShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, BuildConfig.HOST);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, mILocalistionManager.getLocaleString(R.string.app_onlinepassport_share)));
    }

    @Override
    public void onUploadPhoto() {
        BottomSheetDialogFragment bottomSheetDialogFragment = new PhotoDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(PhotoDialogFragment.Args.IS_CROP_IMAGE, true);
        bottomSheetDialogFragment.setArguments(bundle);

        FragmentManager fragmentManager = requireFragmentManager();
        bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onShowPhoto(Bitmap bitmap) {

    }

    @Override
    public void onShowPhoto(String urlImage) {

    }

    @Override
    public void onPhotoUploaded(@NonNull Bitmap bitmap) {
        mIOnlinePassportPresenter.uploadPhoto(bitmap);
        mPhotoLabelView.setText(mILocalistionManager.getLocaleString(R.string.app_onlinepassport_editphoto));
        mPhotoLabelView.setOnClickListener(v -> {
            if (mProfileImageView.isEditable()) {
                onUploadPhoto();
            }
        });
        onChangeComponentData(null);
    }

    @Override
    public void onChangeComponentData(View view) {
        try {
            boolean b = !TextUtils.isEmpty(mFirstNameView.getValue()) &&
                    !TextUtils.isEmpty(mSecondNameView.getValue()) &&
                    !TextUtils.isEmpty(mCityView.getValue()) &&
                    !TextUtils.isEmpty(mSexView.getSelection()) &&
                    !TextUtils.isEmpty(mCountryView.getSelection()) &&
                    mBirthdayView.getDate() > 0;
//                    && (mProfileImageView.getImage() != null || !TextUtils.isEmpty(mProfileImageView.getUrlImage()));

            mProceedButtonView.setEnabled(b);
        } catch (Exception e) {
            // TODO: 26.09.2018 проследить в крашлитикс
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        onChangeComponentData(null);
    }
}
