package com.decenturion.mobile.ui.activity.traid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.traide.view.TraidInfoFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class TraidActivity extends SingleFragmentActivity {

    public static final String TRAID_UUID = "TRAID_UUID";

    private String mTraidUUID;
    private Bundle mBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        iNavigationManager.navigateTo(TraidInfoFragment.class, mBundle);
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mBundle = intent.getExtras();
//            mTransactionID = intent.getIntExtra(TRAID_UUID, 0);
        }

//        if (mTransactionID == 0) {
//            LoggerUtils.exception(new Exception(TRAID_UUID + " is not be 0"));
//            finish();
//        }
    }
}
