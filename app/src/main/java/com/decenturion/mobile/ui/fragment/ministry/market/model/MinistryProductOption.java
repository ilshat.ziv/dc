package com.decenturion.mobile.ui.fragment.ministry.market.model;

import android.support.annotation.NonNull;

public class MinistryProductOption {

    private String mName;

    public MinistryProductOption(@NonNull String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
