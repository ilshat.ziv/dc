package com.decenturion.mobile.ui.fragment.profile_v2.transactions.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;

public interface ITransactionListView extends IScreenFragmentView {

    void onBindViewData(@NonNull TransactionListModelView model);

    void onSignout();
}
