package com.decenturion.mobile.ui.fragment.settings.security.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.security.ISecurityInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.security.model.GfaModel;
import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;
import com.decenturion.mobile.ui.fragment.settings.security.view.ISecurityView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SecurityPresenter extends Presenter<ISecurityView> implements ISecurityPresenter {

    private ISecurityInteractor mISecurityInteractor;
    private ILocalistionManager mILocalistionManager;

    private SecurityModelView mSecurityModelView;

    public SecurityPresenter(ISecurityInteractor iSecurityInteractor,
                             ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mISecurityInteractor = iSecurityInteractor;
        mSecurityModelView = new SecurityModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    boolean isShowName, boolean isShowBirth, boolean isShowPhoto,
                                    boolean isShowTokens, boolean isShowMoney,
                                    boolean isShowPower, boolean isShowGlory,

                                    boolean isEnabledGfa, boolean isEnableGfa,
                                    String secretString, String secretCode) {

        bundle.putBoolean("isShowName", isShowName);
        bundle.putBoolean("isShowBirth", isShowBirth);
        bundle.putBoolean("isShowPhoto", isShowPhoto);
        bundle.putBoolean("isShowTokens", isShowTokens);
        bundle.putBoolean("isShowPower", isShowPower);
        bundle.putBoolean("isShowMoney", isShowMoney);
        bundle.putBoolean("isShowGlory", isShowGlory);

        bundle.putBoolean("isEnabledGfa", isEnabledGfa);
        bundle.putBoolean("isEnableGfa", isEnableGfa);
        bundle.putString("secretString", secretString);
        bundle.putString("secretCode", secretCode);
//        bundle.putBoolean("isEnableGfa", isEnableGfa);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mSecurityModelView.setShowName(savedInstanceState.getBoolean("isShowName", false));
        mSecurityModelView.setShowBirth(savedInstanceState.getBoolean("isShowBirth", false));
        mSecurityModelView.setShowPhoto(savedInstanceState.getBoolean("isShowPhoto", false));
        mSecurityModelView.setShowTokens(savedInstanceState.getBoolean("isShowTokens", false));
        mSecurityModelView.setShowPower(savedInstanceState.getBoolean("isShowPower", false));
        mSecurityModelView.setShowMoney(savedInstanceState.getBoolean("isShowMoney", false));
        mSecurityModelView.setShowGlory(savedInstanceState.getBoolean("isShowGlory", false));

        GfaModel gfaModel = new GfaModel(
                savedInstanceState.getString("secretString", ""),
                savedInstanceState.getString("secretCode", ""),
                savedInstanceState.getBoolean("isEnableGfa", false),
                savedInstanceState.getBoolean("isEnabledGfa", false));
        mSecurityModelView.setGfaModel(gfaModel);

        Observable<SecurityModelView> obs = Observable.just(mSecurityModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SecurityModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<SecurityModelView> obs = mISecurityInteractor.getSettingsInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SecurityModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SecurityModelView model) {
        mSecurityModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveChanges(boolean isShowName, boolean isShowBirth, boolean isShowPhoto,
                            boolean isShowTokens, boolean isShowMoney,
                            boolean isShowPower, boolean isShowGlory,

                            boolean isEnabledGfa, boolean isEnableGfa,
                            String secretString, String secretCode) {
        showProgressView();

        Observable<SecurityModelView> obs = mISecurityInteractor.saveChanges(
                isShowName, isShowBirth, isShowPhoto, isShowTokens, isShowMoney,
                isShowPower, isShowGlory,

                isEnabledGfa, isEnableGfa, secretString, secretCode
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SecurityModelView>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(SecurityModelView modelView) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));

        mSecurityModelView = modelView;

        if (mView != null) {
            mView.onChangesSaved(modelView);
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                LoggerUtils.exception(new RuntimeException(error.getMessage()));

                return;
            }
        }
        showErrorView(throwable);

        LoggerUtils.exception(throwable);
    }
}
