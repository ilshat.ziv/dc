package com.decenturion.mobile.di.dagger.pass.restore;

import com.decenturion.mobile.ui.fragment.pass.restore.view.RestorePassFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        RestorePassModule.class
})

@RestorePassScope
public interface RestorePassComponent {

    void inject(RestorePassFragment view);
}
