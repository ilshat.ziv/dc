package com.decenturion.mobile.ui.fragment.token.transaction.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.tokendetails.transaction.TransactionComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.TransactionFailFragmentDialog;
import com.decenturion.mobile.ui.dialog.TransactionSuccessFragmentDialog;
import com.decenturion.mobile.ui.dialog.token.DeleteDealDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;
import com.decenturion.mobile.ui.fragment.token.transaction.presenter.ITransactionPresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class TransactionFragment extends BaseFragment implements ITransactionView,
        View.OnClickListener {

    public static final String TRAIDE_UUID = "TRAIDE_UUID";

    public String mTraidUUID;

    /* UI */

    @BindView(R.id.dealNumberView)
    TextView mDealNumberView;

    @BindView(R.id.dealDateView)
    TextView mDealDateView;

    @BindView(R.id.addressView)
    TextView mAddressView;

    @BindView(R.id.totalView)
    TextView mTotalView;

    @BindView(R.id.simpleReviesAddressView)
    SimpleTextView mSimpleReviesAddressView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    ITransactionPresenter mITransactionPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTraidUUID = arguments.getString(TRAIDE_UUID, "");
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TransactionComponent transactionComponent = appDelegate
                .getIDIManager()
                .plusTransactionComponent();
        transactionComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_transaction, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

//        mSimpleReviesAddressView.hideLabel();
//        mSimpleReviesAddressView.setText(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_refundaddress_title));
//        mSimpleReviesAddressView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_refundaddress_placeholder));
//        mSimpleReviesAddressView.simpleMode();
//        mSimpleReviesAddressView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITransactionPresenter.bindView(this);
        mITransactionPresenter.bindViewData(mTraidUUID);
    }

    @Override
    public void onResume() {
        super.onResume();
        mITransactionPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mITransactionPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mITransactionPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull TransactionModelView model) {
        String localeString = mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_dealnumber);
        localeString = localeString.replace("{dealnumber}", model.getFrontendId());
//        localeString = localeString.replace("{dealnumber}", "34215");
        mDealNumberView.setText(localeString);
        mAddressView.setText(model.getTradeAddress());
        mTotalView.setText(model.getTotalPrice());

        String mexpiredAt = model.getMexpiredAt();
        mexpiredAt = mexpiredAt.replaceAll("\\.\\d*","");
        long datetime = DateTimeUtils.getDatetime(mexpiredAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        mexpiredAt = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.LABEL_BUTTON_TIME);
        mDealDateView.setText(mexpiredAt);
    }

    @Override
    public void onTransactionConfirmed() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        Bundle bundle = new Bundle();
        bundle.putString(TransactionFailFragmentDialog.TITLE, mDealNumberView.getText().toString());
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(TransactionSuccessFragmentDialog.class, bundle);
    }

    @Override
    public void onErrorTransaction() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        Bundle bundle = new Bundle();
        bundle.putString(TransactionFailFragmentDialog.TITLE, mDealNumberView.getText().toString());
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(TransactionFailFragmentDialog.class, bundle);
    }

    @Override
    public void onTransactionCanceled() {
        FragmentActivity activity = requireActivity();
        activity.finish();
    }

    @Override
    public void onClick(View v) {
        FragmentActivity activity = requireActivity();
        activity.finish();
    }

    @OnClick(R.id.deleteDealButtonView)
    protected void onDeleteDeal() {
        Bundle bundle = new Bundle();
        bundle.putString(DeleteDealDialog.TRAIDE_UUID, mTraidUUID);
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(DeleteDealDialog.class, bundle);
    }

    @OnClick(R.id.addressView)
    protected void onCopyAddres() {
        Context context = requireContext();
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("AddressWalletView6", mAddressView.getText());
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
        }
    }
}
