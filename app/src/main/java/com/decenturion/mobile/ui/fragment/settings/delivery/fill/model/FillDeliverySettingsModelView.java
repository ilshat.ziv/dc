package com.decenturion.mobile.ui.fragment.settings.delivery.fill.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.ui.fragment.passport.online.model.PhotoModelView;

public class FillDeliverySettingsModelView {

    private boolean isInvited;
    private boolean isActive;
    private boolean isDeliveryPaid;

    private DeliveryDataModelView mDeliverDataModelView;
    private PhotoModelView mPhotoModelView;

    public FillDeliverySettingsModelView() {
    }

    public FillDeliverySettingsModelView(@NonNull OrmResidentModel m0,
                                         @NonNull OrmPassportModel m1,
                                         OrmPhotoModel m2) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.isActive() || m0.getStep() > 4;
        this.isDeliveryPaid = m0.isDeliveryPaid();

        Delivery delivery = m1.getDelivery();
        mDeliverDataModelView = new DeliveryDataModelView(delivery);

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public FillDeliverySettingsModelView(Resident m0, Passport m1, Photo m2) {
        this.isInvited = m0.isInvited();
        this.isActive = m0.isActive();
        this.isDeliveryPaid = m0.isDeliveryPaid();

        Delivery delivery = m1.getDelivery();
        mDeliverDataModelView = new DeliveryDataModelView(delivery);

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isInvited() {
        return this.isInvited;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }

    public DeliveryDataModelView getDeliverDataModelView() {
        return mDeliverDataModelView;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }
}
