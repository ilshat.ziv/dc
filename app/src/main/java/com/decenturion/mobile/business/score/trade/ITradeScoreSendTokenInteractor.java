package com.decenturion.mobile.business.score.trade;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.request.score.send.SendToMainModel;
import com.decenturion.mobile.network.request.score.send.SendToTradeModel;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendTokenModelView;

import rx.Observable;

public interface ITradeScoreSendTokenInteractor {

    Observable<TradeScoreSendTokenModelView> getCoinInfo(@NonNull TradeScoreSendTokenModelView model);

//    Observable<SendTokenResult> sendToken(@NonNull String coinUUID,
//                                          @NonNull String coinCategory,
//                                          @NonNull String amount,
//                                          @NonNull String twofa);

    Observable<SendTokenResult> sendToMain(@NonNull SendToMainModel model);
}
