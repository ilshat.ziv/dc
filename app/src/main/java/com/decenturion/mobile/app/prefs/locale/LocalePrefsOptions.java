package com.decenturion.mobile.app.prefs.locale;

public class LocalePrefsOptions {

    public interface Keys {
        String LOCAL = "Locale";
        String VERSION_DEFAULT_LOCALISATION = "VERSION_DEFAULT_LOCALISATION";
    }

    public interface Defaults {
        String LOCAL = "en_EN";
    }
}
