package com.decenturion.mobile.ui.fragment.score.send.trade.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.TransferGas;

public class TradeScoreSendOptionModel {

    // Дата следующей отпраки токенов обычным способом
    private long mFreeCoolDown;
    private double mGas;
    private String mCurrence;

    public TradeScoreSendOptionModel(long freeCoolDown, double gas) {
        mFreeCoolDown = freeCoolDown;
        mGas = gas;
    }

    public TradeScoreSendOptionModel(long freeCoolDown, @NonNull TransferGas transferGas) {
        this(freeCoolDown, Double.parseDouble(transferGas.getGasValue()));
        mCurrence = transferGas.getCurrency();
    }

    public long getFreeCoolDown() {
        return mFreeCoolDown;
    }

    public double getGas() {
        return mGas;
    }

    public String getCurrence() {
        return mCurrence;
    }
}
