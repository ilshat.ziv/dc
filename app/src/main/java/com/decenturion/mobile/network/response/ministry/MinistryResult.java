package com.decenturion.mobile.network.response.ministry;

import com.decenturion.mobile.network.response.model.MinistryItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryResult {

    @JsonProperty("external")
    private MinistryItem mExternal;

    @JsonProperty("internal")
    private MinistryItem mInternal;

    @JsonProperty("honorary")
    private MinistryItem mHonorary;

    @JsonProperty("senator")
    private MinistryItem mSenator;

    public MinistryResult() {
    }

    @JsonIgnore
    public MinistryItem getExternal() {
        return mExternal;
    }

    @JsonIgnore
    public MinistryItem getInternal() {
        return mInternal;
    }

    @JsonIgnore
    public MinistryItem getHonorary() {
        return mHonorary;
    }

    @JsonIgnore
    public MinistryItem getSenator() {
        return mSenator;
    }
}
