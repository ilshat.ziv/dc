package com.decenturion.mobile.ui.fragment.settings.account.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.account.AccountSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.NextView;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.account.model.AccountSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.account.presenter.IAccountSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.password.view.PasswordSettingsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountSettingsFragment extends BaseFragment implements IAccountSettingsView
        , View.OnClickListener {

    /* UI */

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.emailView)
    SimpleTextView mEmailView;

    @BindView(R.id.passwordView)
    NextView mPasswordView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;


    /* DI */

    @Inject
    IAccountSettingsPresenter mIAccountSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        AccountSettingsComponent accountSettingsComponent = appDelegate
                .getIDIManager()
                .plusAccountSettingsComponent();
        accountSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_account_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mEmailView.showLabel();
        mEmailView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_account_email));
        mEmailView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_account_email));
        mEmailView.setVisibleDivider(true);

        mPasswordView.setText(mILocalistionManager.getLocaleString(R.string.app_settings_account_password));
        mPasswordView.setVisibleDivider(true);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_account_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIAccountSettingsPresenter.bindView(this);
        mIAccountSettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIAccountSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIAccountSettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIAccountSettingsPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull AccountSettingsModelView model) {
        mEmailView.setText(model.getUsername());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.passwordView)
    protected void onPasswordSettings() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(PasswordSettingsFragment.class, true);
    }

    @OnClick(R.id.deleteAccountButtonView)
    protected void onDeleteAccount() {
        // TODO: 26.08.2018 реализовать удаление аккаунта
    }

}
