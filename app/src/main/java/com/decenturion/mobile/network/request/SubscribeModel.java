package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscribeModel {

    @JsonProperty("subscribe")
    private boolean isSubscribe;

    public SubscribeModel() {
    }

    @JsonIgnore
    public SubscribeModel(boolean isSubscribe) {
        this.isSubscribe = isSubscribe;
    }
}
