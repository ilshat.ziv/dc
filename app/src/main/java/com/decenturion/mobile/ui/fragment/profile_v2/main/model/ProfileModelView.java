package com.decenturion.mobile.ui.fragment.profile_v2.main.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PhotoModelView;

public class ProfileModelView {

    private String mResidentUUID;
    private String mWalletNum;
    private String mReferralId;
    private String mStatus;

    private PhotoModelView mPhotoModelView;

    public ProfileModelView(@NonNull OrmResidentModel orm1, OrmPhotoModel orm2,
                            @NonNull ILocalistionManager iLocalistionManager) {

        mResidentUUID = orm1.getUuid();
        mStatus = initStatus(orm1.getStatus(), iLocalistionManager);
        mWalletNum = orm1.getWalletNum();

        if (orm2 != null) {
            mPhotoModelView = new PhotoModelView(orm2);
        }
    }

    public ProfileModelView(@NonNull ResidentResult d, @NonNull ILocalistionManager iLocalistionManager) {
        Resident resident = d.getResident();
        mResidentUUID = resident.getUuid();
        mStatus = initStatus(resident.getStatus(), iLocalistionManager);
        mWalletNum = resident.getWalletNum();
        mReferralId = resident.getReferralId();

        Photo photo = d.getPhoto();
        if (photo != null) {
            mPhotoModelView = new PhotoModelView(photo);
        }
    }

    private String initStatus(@NonNull String status, @NonNull ILocalistionManager iLocalistionManager) {
        if (TextUtils.isEmpty(status)) {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_citizen);
        } else if (TextUtils.equals(status, "senator")) {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_senator);
        } else {
            return iLocalistionManager.getLocaleString(R.string.app_resident_status_honorary);
        }
    }

    public String getUID() {
        return mResidentUUID;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getWalletNum() {
        return mWalletNum;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }

    public String getReferralId() {
        return mReferralId;
    }
}
