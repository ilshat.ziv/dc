package com.decenturion.mobile.utils;

import android.text.TextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public final class JsonUtils {

    public static String getJsonString(Object object) {
        final ObjectMapper mObjMapper = new ObjectMapper();
        try {
            return mObjMapper.writeValueAsString(object);
        } catch (OutOfMemoryError | Exception e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    public static Object getObjectFromString(String jsonString, Class cls) {
        if (TextUtils.isEmpty(jsonString)) {
            return null;
        }
        final ObjectMapper mObjMapper = new ObjectMapper();

        try {
            return mObjMapper.readValue(jsonString, cls);
        } catch (IOException e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    public static Object convertLinkedMap(Object o, Class TClass) {
        if (o == null) {
            return null;
        }

        final ObjectMapper mObjMapper = new ObjectMapper();

        try {
            return mObjMapper.convertValue(o, TClass);
        } catch (Exception e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    /**
     * http://www.baeldung.com/jackson-collection-array
     */
    public static Collection getCollectionFromJson(String json, Class Clazz) {
        ObjectMapper mapper = new ObjectMapper();

        CollectionType javaType = mapper.getTypeFactory()
                .constructCollectionType(List.class, Clazz);
        try {
            return mapper.readValue(json, javaType);
        } catch (IOException e) {
            LoggerUtils.exception(e);
        }

        return null;
    }
}