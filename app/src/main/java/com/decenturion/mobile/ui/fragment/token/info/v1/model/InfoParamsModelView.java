package com.decenturion.mobile.ui.fragment.token.info.v1.model;

public class InfoParamsModelView {

    private String mName;

    public InfoParamsModelView(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

}
