package com.decenturion.mobile.ui.fragment.settings.delivery.pending.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.delivery.pending.DeliveryPendingSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.model.DeliveryPendingSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.presenter.IDeliveryPendingSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class DeliveryPendingSettingsFragment extends BaseFragment implements IDeliveryPendingSettingsView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.trackIdView)
    TextView mTrackIdView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IDeliveryPendingSettingsPresenter mIDeliverySettingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        DeliveryPendingSettingsComponent component = appDelegate
                .getIDIManager()
                .plusDeliveryPendingSettingsComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_delivery_pending_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDeliverySettingsPresenter.bindView(this);
        mIDeliverySettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDeliverySettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDeliverySettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIDeliverySettingsPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onBindViewData(@NonNull DeliveryPendingSettingsModelView model2) {
        mTrackIdView.setText(model2.getPhone());
    }
}
