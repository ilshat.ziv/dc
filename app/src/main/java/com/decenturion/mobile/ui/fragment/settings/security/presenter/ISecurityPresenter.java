package com.decenturion.mobile.ui.fragment.settings.security.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.security.view.ISecurityView;

public interface ISecurityPresenter extends IPresenter<ISecurityView> {

    void bindViewData();

    void saveChanges(
            boolean isShowName,
            boolean isShowBirth,
            boolean isShowPhoto,
            boolean isShowTokens,
            boolean isShowMoney,
            boolean isShowPower,
            boolean isShowGlory,
            boolean isEnabledGfa,
            boolean isEnableGfa,
            String secretString,
            String secretCode
    );

    void onSaveInstanceState(
            @NonNull Bundle bundle,

            boolean isShowName,
            boolean isShowBirth,
            boolean isShowPhoto,
            boolean isShowTokens,
            boolean isShowMoney,
            boolean isShowPower,
            boolean isShowGlory,
            boolean isEnabledGfa,
            boolean isEnableGfa,
            String secretString,
            String secretCode
    );

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
