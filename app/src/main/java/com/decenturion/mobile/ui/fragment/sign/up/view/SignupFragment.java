package com.decenturion.mobile.ui.fragment.sign.up.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.signup.SignupComponent;
import com.decenturion.mobile.ui.activity.agreement.UserAggrementActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.passport.PassportActivity;
import com.decenturion.mobile.ui.activity.sign.ISignActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.PasswordView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.sign.in.view.SigninFragment;
import com.decenturion.mobile.ui.fragment.sign.up.model.SignupModelView;
import com.decenturion.mobile.ui.fragment.sign.up.presenter.ISignupPresenter;
import com.decenturion.mobile.ui.fragment.validate.view.ValidateEmailFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.LinkCheckBoxView;
import com.decenturion.mobile.ui.widget.LinkTextView;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SignupFragment extends BaseFragment implements ISignupView,
        LinkTextView.OnLinkTextViewListener,
        CompoundButton.OnCheckedChangeListener,
        ChangeComponentListener {

    /* UI */

    @BindView(R.id.emailView)
    EditTextView mEmailView;

    @BindView(R.id.passwordView)
    PasswordView mPasswordView;

    @BindView(R.id.retypePasswordView)
    PasswordView mRetypePasswordView;

    @BindView(R.id.agreemetView)
    LinkCheckBoxView mAgreemetView;

    @BindView(R.id.agreemeGetEmailtView)
    LinkCheckBoxView mAgreemeGetEmailtView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mSignupButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    private String mInviteToken;
    private String mReferallKey;
    private String mPayReferallKey;
    private String mBannerReferallKey;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISignupPresenter mISignupPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
        setHasOptionsMenu(true);

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mInviteToken = arguments.getString(SignActivity.INVITE_TOKEN, "");
            mReferallKey = arguments.getString(SignActivity.REFERALL_KEY, "");
            mPayReferallKey = arguments.getString(SignActivity.PAY_REFERALL_KEY, "");
            mBannerReferallKey = arguments.getString(SignActivity.BANNER_REFERALL_KEY, "");
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SignupComponent signupComponent = appDelegate
                .getIDIManager()
                .plusSignupComponent();
        signupComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_signup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mEmailView.showLabel();
        mEmailView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signup_email_title));
        mEmailView.setHint(mILocalistionManager.getLocaleString(R.string.app_signup_email_placeholder));
        mEmailView.setTypeInput(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        if (TextUtils.isEmpty(mInviteToken)) {
            mEmailView.editMode();
        } else {
            mEmailView.simpleMode();
        }
        mEmailView.setVisibleDivider(true);

        mPasswordView.showLabel();
        mPasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signup_password_title));
        mPasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_signup_password_placeholder));
        mPasswordView.editMode();
        mPasswordView.setVisibleDivider(true);

        mRetypePasswordView.showLabel();
        mRetypePasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_signup_repeatpassword_title));
        mRetypePasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_signup_repeatpassword_placeholder));
        mRetypePasswordView.editMode();
        mRetypePasswordView.setVisibleDivider(true);

        mAgreemetView.setChecked(false);
        mAgreemetView.setText(mILocalistionManager.getLocaleString(R.string.app_signup_termsofuse));
        mAgreemetView.setOnLinkClickListener(this);
        mAgreemetView.setOnCheckedChangeListener(this);

        mAgreemeGetEmailtView.setChecked(true);
        mAgreemeGetEmailtView.setText(mILocalistionManager.getLocaleString(R.string.app_signup_emailconfirmation));

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISignActivity activity = (ISignActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_signup_title));
        iToolbarController.removeNavigationButton();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISignupPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISignupPresenter.bindViewData(mInviteToken);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mISignupPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISignupPresenter.unbindView();
        try {
            mISignupPresenter.onSaveInstanceState(outState,
                    mEmailView.getValue(),
                    mPasswordView.getValue(),
                    mRetypePasswordView.getValue(),
                    "",
                    mAgreemetView.isChecked(),
                    mAgreemeGetEmailtView.isChecked(),
                    mInviteToken,
                    mReferallKey,
                    mPayReferallKey,
                    mBannerReferallKey
            );
        } catch (Exception e) {
            // TODO: 25.09.2018 ловить в crashlytics и чинить
            LoggerUtils.exception(e);
        }

        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISignupPresenter.onViewStateRestored(savedInstanceState);
            mInviteToken = savedInstanceState.getString("inviteCode", "");
            mReferallKey = savedInstanceState.getString("referallKey", "");
            mPayReferallKey = savedInstanceState.getString("payReferallKey", "");
            mBannerReferallKey = savedInstanceState.getString("bannerReferralKey", "");
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_sign_in);
        if (item != null) {
            item.setTitle(mILocalistionManager.getLocaleString(R.string.app_signup_signin));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fr_signup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_in: {
                ISignActivity activity = (ISignActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();
                iNavigationManager.navigateTo(SigninFragment.class);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISignupPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull SignupModelView model) {
        mEmailView.setText(model.getUsername());
        mPasswordView.setText(model.getPassword());
        mRetypePasswordView.setText(model.getRetypePassword());

        // TODO: 24.09.2018 костыль
        if (!mAgreemetView.isChecked()) {
            mAgreemetView.setChecked(model.isAgreeTermsOfuse());
        }
        mAgreemeGetEmailtView.setChecked(model.isAgreeEmailConfirmation());
        mSignupButtonView.setEnabled(mAgreemetView.isChecked());
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        String email = mEmailView.getValue();
        String password = mPasswordView.getValue();
        String retypePassword = mRetypePasswordView.getValue();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(retypePassword)) {
            showErrorMessage(mILocalistionManager.getLocaleString(R.string.app_alert_fillallthefields));
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            String localeString = mILocalistionManager.getLocaleString(R.string.app_alert_failed_email);
            showErrorMessage(localeString.replace("{email}", email));
            return;
        }

        if (password.length() < 6) {
            showErrorMessage(mILocalistionManager.getLocaleString(R.string.app_error_password_length));
            return;
        }

        if (!TextUtils.equals(password, retypePassword)) {
            showErrorMessage(mILocalistionManager.getLocaleString(R.string.app_error_password_match));
            return;
        }

        mISignupPresenter.signup(
                mEmailView.getValue(),
                mPasswordView.getValue(),
                mRetypePasswordView.getValue(),
                "",
                mAgreemetView.isChecked(),
                mAgreemeGetEmailtView.isChecked(),
                mInviteToken,
                mReferallKey,
                mPayReferallKey,
                mBannerReferallKey
        );
    }

    @Override
    public void onSignupSuccess(@NonNull SignupModelView model, int step) {
        switch (step) {
            case 0 : {
                ISignActivity activity = (ISignActivity) requireActivity();
                INavigationManager iNavigationManager = activity.getINavigationManager();

                Bundle bundle = new Bundle();
                bundle.putString(ValidateEmailFragment.EMAIL, model.getUsername());
                iNavigationManager.navigateTo(ValidateEmailFragment.class, true, bundle);
                break;
            }
            case 2 : {
                FragmentActivity activity = requireActivity();
                Intent intent = new Intent(activity, PassportActivity.class);
                intent.putExtra(PassportActivity.TYPE_PASSPORT, PassportActivity.ONLINE_PASSPORT);
                startActivity(intent);
                activity.finish();
                break;
            }
        }
    }

    @Override
    public void onResendEmail(@NonNull SignupModelView model) {
        onSignupSuccess(model,0);
    }

    @Override
    public void onLinkClick() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, UserAggrementActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_USER_AGGREEMENT);
    }

    @Override
    public void onTextClick() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_USER_AGGREEMENT) {
            mAgreemetView.setChecked(resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        onChangeComponentData(buttonView);
    }

    @Override
    public void onChangeComponentData(View view) {
        boolean b = mAgreemetView.isChecked();
        mSignupButtonView.setEnabled(b);
    }
}