package com.decenturion.mobile.ui.fragment.settings.main.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.main.view.ISettingsView;

public interface ISettingsPresenter extends IPresenter<ISettingsView> {

    void logout();
}
