package com.decenturion.mobile.business.token.details;

import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.response.market.MarketTokensResponse;
import com.decenturion.mobile.network.response.model.MarketCoinsToken;
import com.decenturion.mobile.network.response.model.TokenCoin;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.token.seller.details.model.SellerTokenDetailsModelView;

import rx.Observable;

public class SellerTokenDetailsInteractor implements ISellerTokenDetailsInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private ITokenRepository mITokenRepository;
    private ILocalistionManager mILocalistionManager;

    public SellerTokenDetailsInteractor(IResidentClient IResidentClient,
                                        ITokenRepository ITokenRepository,
                                        ITokenClient iTokenClient,
                                        ILocalistionManager iLocalistionManager) {
        mIResidentClient = IResidentClient;
        mITokenRepository = ITokenRepository;
        mITokenClient = iTokenClient;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<SellerTokenDetailsModelView> getTokenDetails(int tokenId) {
        return Observable.just(mITokenRepository.getTokenById(tokenId))
                .map(d -> new SellerTokenDetailsModelView(d, mILocalistionManager));
    }

    @Override
    public Observable<SellerTokenDetailsModelView> getTokenDetails(SellerTokenDetailsModelView modelView) {
        return mITokenClient.getBuyTokens(modelView.getResidentUUID())
                .map(MarketTokensResponse::getResult)
                .map(d -> {
                    for (MarketCoinsToken token : d) {
                        TokenCoin tokenCoin = token.getTokenCoin();
                        if (TextUtils.equals(tokenCoin.getUuid(), modelView.getCoinUUID())
                                && TextUtils.equals(token.getCategory(), modelView.getCategory())) {
                            return new SellerTokenDetailsModelView(token, mILocalistionManager);
                        }

                    }
                    return null;
                });
    }
}
