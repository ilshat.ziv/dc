package com.decenturion.mobile.ui.fragment.settings.details.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;

public class DetailsSettingsModelView {

    private String mAbout;
    private String mTelegram;
    private String mWhatsApp;
    private String mWechat;
    private String mViber;

    private PhotoModelView mPhotoModelView;
    private String mEmail;

    public DetailsSettingsModelView() {
    }

    public DetailsSettingsModelView(@NonNull OrmResidentModel m0, OrmPhotoModel m2) {
        mAbout = m0.getAbout();
        mEmail = m0.getEmail();
        mTelegram = m0.getTelegram();
        mWhatsApp = m0.getWhatsApp();
        mWechat = m0.getWechat();
        mViber = m0.getViber();

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public String getAbout() {
        return mAbout;
    }

    public String getTelegram() {
        return mTelegram;
    }

    public String getWhatsApp() {
        return mWhatsApp;
    }

    public String getWechat() {
        return mWechat;
    }

    public String getViber() {
        return mViber;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }

    public String getEmail() {
        return mEmail;
    }
}
