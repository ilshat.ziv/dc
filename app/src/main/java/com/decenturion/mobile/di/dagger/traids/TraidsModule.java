package com.decenturion.mobile.di.dagger.traids;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.profile.traids.IDealListInteractor;
import com.decenturion.mobile.business.profile.traids.DealListInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.repository.traide.ITraideRepository;
import com.decenturion.mobile.repository.traide.TraideRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.presenter.IDealListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.presenter.DealListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TraidsModule {

    @Provides
    @TraidsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TraidsScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TraidsScope
    ITraideRepository provideITraideRepository(OrmHelper ormHelper) {
        return new TraideRepository(ormHelper);
    }


    @Provides
    @TraidsScope
    IDealListInteractor provideITraidsInteractor(IResidentClient IResidentClient,
                                                 ITokenClient iTokenClient,
                                                 IResidentRepository iResidentRepository,
                                                 ITokenRepository iTokenRepository,
                                                 ITraideRepository iTraideRepository,
                                                 IPrefsManager iPrefsManager,
                                                 ILocalistionManager iLocalistionManager) {

        return new DealListInteractor(IResidentClient,
                iTokenClient,
                iResidentRepository,
                iTokenRepository,
                iTraideRepository,
                iPrefsManager,
                iLocalistionManager
        );
    }

    @Provides
    @TraidsScope
    IDealListPresenter provideITraidsPresenter(IDealListInteractor iTraidsInteractor) {
        return new DealListPresenter(iTraidsInteractor);
    }
}
