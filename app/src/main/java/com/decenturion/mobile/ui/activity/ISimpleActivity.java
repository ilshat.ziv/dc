package com.decenturion.mobile.ui.activity;

import android.support.design.widget.TabLayout;

import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.floating.keybord.IKeyboardVisionManager;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

public interface ISimpleActivity extends IScreenActivityView {

    IToolbarController getIToolbarController();

    INavigationManager getINavigationManager();

    IKeyboardVisionManager getIKeyboardVisionManager();

    TabLayout getTabLayoutView();
}
