package com.decenturion.mobile.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import com.squareup.picasso.Transformation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public final class ImageUtils {

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap loadBitmap(File file) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
//        bitmap = Bitmap.createScaledBitmap(bitmap, parent.getWidth(), parent.getHeight(), true);
        return bitmap;
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int maxSideSize) {
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        int maxSide = Math.max(height, width);
        if (maxSide < maxSideSize) {
            return bitmap;
        }

        if (maxSide == height) {
            float k = (float) height / (float) maxSideSize;
            height = maxSideSize;
            width = (int) (width / k);
        } else {
            float k = (float) width / (float) maxSideSize;
            width = maxSideSize;
            height = (int) (height / k);
        }

        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static Bitmap compressBitmap(Bitmap bitmap, int percent) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, percent, out);
        Bitmap bitmap1 = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return bitmap1;
    }

    /**
     * Использовать только для преобрахования фото с камеры
     */
    public static Bitmap fixRotateBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            Matrix matrix = new Matrix();
            matrix.preRotate(-90);

            Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
            if (rotated != bitmap) {
                bitmap.recycle();
            }
            return rotated;
        } else {
            return bitmap;
        }
    }

    public static boolean isImageLigth(Bitmap bitmap) {
        int pixelCount = bitmap.getWidth() * bitmap.getHeight();
        int[] histogram = new int[]{} ;

        for (int x = 0; x < bitmap.getWidth(); x++) {
            for (int y = 0; y < bitmap.getHeight(); y++) {
                int pixel = bitmap.getPixel(x, y);

                int r = Color.red(pixel);
                int g = Color.green(pixel);
                int b = Color.blue(pixel);

                int brightness = (int) (0.2126 * r + 0.7152 * g + 0.0722 * b);
                histogram[brightness]++;
            }
        }

        // Count pixels with brightness less then 10
        int darkPixelCount = 0;
        for (int i = 0; i < 10; i++) {
            darkPixelCount += histogram[i];
        }

        return darkPixelCount > pixelCount * 0.25;
//        if (darkPixelCount > pixelCount * 0.25); // Dark picture. Play with a percentage
//        else // Light picture.
    }

    public static Bitmap fixRotateBitmap2(Bitmap bitmap, String filePath) {
        try {
            //https://stackoverflow.com/questions/3647993/android-bitmaps-loaded-from-gallery-are-rotated-in-imageview
            //http://sylvana.net/jpegcrop/exif_orientation.html
            ExifInterface exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            switch (orientation) {
                case 7 :
                case 8 : {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(-90);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    break;
                }
                case 4 :
                case 3 : {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    break;
                }
                case 5 :
                case 6 : {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    break;
                }
            }
        } catch (IOException e) {
            LoggerUtils.exception(e);;
        }
        return bitmap;
    }

    public static String getPathFromMediaUri(@NonNull Context context,  Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri,
                null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }
}
