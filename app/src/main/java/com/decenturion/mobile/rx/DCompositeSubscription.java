package com.decenturion.mobile.rx;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class DCompositeSubscription implements IDCompositeSubscription {

    private CompositeSubscription mCompositeSubscription;
    private Map<String, Subscription> mSubscriptionSet = new HashMap<>();

    public DCompositeSubscription() {
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void clear() {
        mCompositeSubscription.clear();
        mSubscriptionSet.clear();
    }

    public Subscription subscribe(Observable<?> observable, Action1<?> successAction, Action1<Throwable> failAction, Action0 compliteAction) {
        String keySubscription = observable.toString();

        Subscription subscription = observable
                    .doOnCompleted(getCompliteAction(keySubscription))
                    .doOnError(getErrorAction(keySubscription))
                    .subscribe(successAction, failAction, compliteAction);

        mCompositeSubscription.add(subscription);
        mSubscriptionSet.put(keySubscription, subscription);

        return subscription;
    }

    @Override
    public Subscription subscribe(Observable<?> observable, Action1<?> successAction, Action1<Throwable> failAction) {
        String keySubscription = observable.toString();

        Subscription subscription = observable
                .doOnCompleted(getCompliteAction(keySubscription))
                .doOnError(getErrorAction(keySubscription))
                .subscribe(successAction, failAction);

        mCompositeSubscription.add(subscription);
        mSubscriptionSet.put(keySubscription, subscription);

        return subscription;
    }

    @Override
    public Subscription subscribe(Observable<?> observable, Action1<?> successAction) {
        String keySubscription = observable.toString();

        Subscription subscription = observable
                .doOnCompleted(getCompliteAction(keySubscription))
                .doOnError(getErrorAction(keySubscription))
                .subscribe(successAction);

        mCompositeSubscription.add(subscription);
        mSubscriptionSet.put(keySubscription, subscription);

        return subscription;
    }

    private Action0 getCompliteAction(final String keySubscription) {
        return () -> {
            Subscription subscription1 = mSubscriptionSet.get(keySubscription);
            if (subscription1 != null) {
                mCompositeSubscription.remove(subscription1);
                mSubscriptionSet.remove(keySubscription);
            }
        };
    }

    private Action1 getErrorAction(final String keySubscription) {
        return (Action1<Throwable>) throwable -> {
            Subscription subscription1 = mSubscriptionSet.get(keySubscription);
            if (subscription1 != null) {
                mCompositeSubscription.remove(subscription1);
                mSubscriptionSet.remove(keySubscription);
            }
        };
    }


}
