package com.decenturion.mobile.ui.fragment.settings.main.view;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;

public interface ISettingsView extends IScreenFragmentView {

    void logoutSuccess();
}
