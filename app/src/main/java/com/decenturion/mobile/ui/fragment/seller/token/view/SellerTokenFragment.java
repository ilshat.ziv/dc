package com.decenturion.mobile.ui.fragment.seller.token.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.seller.token.SellerTokenComponent;
import com.decenturion.mobile.ui.activity.token.seller.SellerTokenActivity;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile.wallet.StartupAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModel;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.seller.token.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.seller.token.presenter.ISellerTokenPresenter;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class SellerTokenFragment extends BaseFragment implements ISellerTokenView,
        StartupAdapter.OnItemClickListener,
        SpinnerView_v2.OnSpinnerItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    private String mResidentUuid;

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.startupListView)
    RecyclerView mStartupListView;
    private StartupAdapter mStartupAdapter;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ISellerTokenPresenter mISellerTokenPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mResidentUuid = arguments.getString(RESIDENT_UUID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        SellerTokenComponent sellerTokenComponent = appDelegate
                .getIDIManager()
                .plusSellerTokenComponent();
        sellerTokenComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_seller_token, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        mRefreshView.setOnRefreshListener(this);

        // TODO: 19.09.2018 костыль, слишком сложная структура перевода
        String localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();
        mSelectCoinView.setOnSpinnerItemClickListener(this);

        Context context = getContext();
        assert context != null;
        mStartupListView.setLayoutManager(new LinearLayoutManager(context));
        mStartupAdapter = new StartupAdapter(context, this);
        mStartupListView.setAdapter(mStartupAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISellerTokenPresenter.bindView(this);
        mISellerTokenPresenter.bindViewData(mResidentUuid);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISellerTokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISellerTokenPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model, @NonNull TokenModelView model2) {
        mSelectCoinView.setData(model.getCoinData());
        mSelectCoinView.selectItem(model2.getTypePay());

        mStartupAdapter.replaceDataSet(model2.getTokentModelList());
    }

    @Override
    public void onItemClick(TokenModel model) {
        Intent intent = new Intent(getActivity(), SellerTokenActivity.class);
        intent.putExtra(SellerTokenActivity.TOKEN_ID, model.getId());
        intent.putExtra(SellerTokenActivity.TOKEN_COIN_UUID, model.getCoinUUID());
        intent.putExtra(SellerTokenActivity.TOKEN_CATEGORY, model.getCategory());
        intent.putExtra(SellerTokenActivity.RESIDENT_UUID, mResidentUuid);
        intent.putExtra(SellerTokenActivity.PAYMENT_METHOD, mSelectCoinView.getSelection());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISellerTokenPresenter.unbindView();
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mISellerTokenPresenter.bindViewData(mResidentUuid);
    }
}
