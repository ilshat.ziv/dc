package com.decenturion.mobile.di.dagger.passport;

import com.decenturion.mobile.ui.fragment.profile_v2.passport.view.PassportFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PassportModule.class
})

@PassportScope
public interface PassportComponent {

    void inject(PassportFragment view);
}
