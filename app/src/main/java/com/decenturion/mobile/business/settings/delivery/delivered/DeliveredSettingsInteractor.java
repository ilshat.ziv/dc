package com.decenturion.mobile.business.settings.delivery.delivered;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model.DeliveredSettingsModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import rx.Observable;

public class DeliveredSettingsInteractor implements IDeliveredSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public DeliveredSettingsInteractor(IResidentClient IResidentClient,
                                       IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<DeliveredSettingsModelView> getPassportInfo() {
        Observable<Boolean> obs2 = mIResidentClient.getResidencePaymentInfo()
                .map(d -> !CollectionUtils.isEmpty(d.getResult()));

        return Observable.zip(
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getResidentInfo()),
                obs2,
                DeliveredSettingsModelView::new
        );
    }

}
