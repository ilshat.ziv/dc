package com.decenturion.mobile.ui.fragment.settings.delivery.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.delivery.DeliverySettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.DeliverySettingsPayFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter.IDeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class DeliverySettingsFragment extends BaseFragment implements IDeliverySettingsView,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.textView)
    TextView mLabelView;

    @BindView(R.id.phoneView)
    EditTextView mPhoneView;

    @BindView(R.id.addressView)
    EditTextView mAddressView;

    @BindView(R.id.cityView)
    EditTextView mCityView;

    @BindView(R.id.stateView)
    EditTextView mStateView;

    @BindView(R.id.countyView)
    SpinnerView mCountryView;

    @BindView(R.id.zipView)
    EditTextView mZipView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.proceedButtonView)
    Button mProceedButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IDeliverySettingsPresenter mIDeliverySettingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        DeliverySettingsComponent deliverySettingsComponent = appDelegate
                .getIDIManager()
                .plusDeliverySettingsComponent();
        deliverySettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_delivery_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_phone_title));
        mPhoneView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_phone_placeholder));
        mPhoneView.editMode();
        mPhoneView.setVisibleDivider(true);

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_address_title));
        mAddressView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_address_placeholder));
        mAddressView.editMode();
        mAddressView.setVisibleDivider(true);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_city_title));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_city_placeholder));
        mCityView.editMode();
        mCityView.setVisibleDivider(true);

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_state_title));
        mStateView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_state_placeholder));
        mStateView.editMode();
        mStateView.setVisibleDivider(true);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_country_title));
        mCountryView.setVisibleDivider(true);

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_zip_title));
        mZipView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_zip_placeholder));
        mZipView.editMode();
        mZipView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDeliverySettingsPresenter.bindView(this);
        mIDeliverySettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDeliverySettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDeliverySettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
        mIDeliverySettingsPresenter.unbindView();
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        mIDeliverySettingsPresenter.setDeliveryData(
                mPhoneView.getValue(),
                mAddressView.getValue(),
                mCityView.getValue(),
                mStateView.getValue(),
                mCountryView.getSelection(),
                mZipView.getValue()
        );
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model3,
                               @NonNull CountryModelView model1,
                               @NonNull DeliverySettingsModelView model2) {
        mCountryView.setData(
                mILocalistionManager.getLocaleString(R.string.app_settings_delivery_country_placeholder),
                model1.getCountryData());

        mPhoneView.setText(model2.getPhone());
        mAddressView.setText(model2.getAddress());
        mCityView.setText(model2.getCity());
        mStateView.setText(model2.getState());
        mCountryView.selectItem(model2.getCountry());
        mZipView.setText(model2.getZip());

        if (model2.isDeliveryPaid()) {
            mCountryView.simpleMode();
            mPhoneView.simpleMode();
            mAddressView.simpleMode();
            mCityView.simpleMode();
            mStateView.simpleMode();
            mCountryView.simpleMode();
            mZipView.simpleMode();

//            onDeliveryPassportDataSeted();
        } else {
            mProceedButtonView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBindPayModelView(@NonNull PaymentModelView model) {

    }

    @Override
    public void onDeliveryPassportDataSeted() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(DeliverySettingsPayFragment.class);
    }
}
