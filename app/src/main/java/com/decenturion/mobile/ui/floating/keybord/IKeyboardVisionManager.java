package com.decenturion.mobile.ui.floating.keybord;

public interface IKeyboardVisionManager {

    int KEYBOARD_VISIBLE_THRESHOLD_DP = 100;

    void addKeyboardListener(OnKeyboardVisionListener listener);

    void removeKeyboardListener(OnKeyboardVisionListener listener);
}
