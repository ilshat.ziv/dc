package com.decenturion.mobile.ui.fragment.twofa.model;

import com.decenturion.mobile.utils.RandomStringUtils;

import java.security.SecureRandom;

public class EnableTwoFAModelView {

    private String mSecretString;
    private String mSecretCode;

    public EnableTwoFAModelView() {
        RandomStringUtils randomStringUtils = new RandomStringUtils(16,
                new SecureRandom(), RandomStringUtils.UPPER);
        mSecretString = randomStringUtils.nextString();
    }

    public void setSecretCode(String secretCode) {
        mSecretCode = secretCode;
    }

    public void setSecretString(String secretString) {
        mSecretString = secretString;
    }

    public String getSecretString() {
        return mSecretString;
    }

    public String getSecretCode() {
        return mSecretCode;
    }
}
