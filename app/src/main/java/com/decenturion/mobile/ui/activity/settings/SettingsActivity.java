package com.decenturion.mobile.ui.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.PrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view.DeliveredSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.view.FillDeliverySettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.view.PayDeliverySettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.security.view.SecurityFragment;
import com.decenturion.mobile.ui.fragment.settings.account.view.AccountSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.action.view.ActionListSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.details.view.DetailsSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.mail.view.MailingSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.passport.view.PassportSettingsFragment;
import com.decenturion.mobile.ui.fragment.settings.wallet.view.WalletsSettingsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class SettingsActivity extends SingleFragmentActivity {

    public static final String TYPE_SETTINGS = "TYPE_SETTINGS";
    public static final int ACCOUNT_SETTINGS = 1;
    public static final int PASSPORT_SETTINGS = 2;
    public static final int DELIVERY_SETTINGS = 3;
    public static final int DETAILS_SETTINGS = 4;
    public static final int MAILING_SETTINGS = 5;
    public static final int SECYRITY_SETTINGS = 6;
    public static final int ACTION_LOG_SETTINGS = 7;
    public static final int WALLET_SETTINGS = 8;

    private int mTypeSettings;

    private IPrefsManager mIPrefsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIPrefsManager = new PrefsManager(getApplicationContext());
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
            return;
        }

        switch (mTypeSettings) {
            case ACCOUNT_SETTINGS: {
                iNavigationManager.navigateTo(AccountSettingsFragment.class);
                break;
            }
            case PASSPORT_SETTINGS: {
                iNavigationManager.navigateTo(PassportSettingsFragment.class);
                break;
            }
            case DELIVERY_SETTINGS: {
                boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_INVITED, false);
                boolean b1 = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_PAID, false);
                boolean b2 = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_DELIVERY_FILL_DATA, false);
                if (b) {
                    if (b1) {
                        iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
                    } else if (b2) {
                        iNavigationManager.navigateTo(PayDeliverySettingsFragment.class);
                    } else {
                        iNavigationManager.navigateTo(FillDeliverySettingsFragment.class);
                    }
                } else {
                    if (b1 || b2) {
                        iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
                    } else {
                        iNavigationManager.navigateTo(FillDeliverySettingsFragment.class);
                    }
                }

                break;
            }
            case DETAILS_SETTINGS: {
                iNavigationManager.navigateTo(DetailsSettingsFragment.class);
                break;
            }
            case MAILING_SETTINGS: {
                iNavigationManager.navigateTo(MailingSettingsFragment.class);
                break;
            }
            case WALLET_SETTINGS : {
                iNavigationManager.navigateTo(WalletsSettingsFragment.class);
                break;
            }
            case SECYRITY_SETTINGS : {
                iNavigationManager.navigateTo(SecurityFragment.class);
                break;
            }
            case ACTION_LOG_SETTINGS : {
                iNavigationManager.navigateTo(ActionListSettingsFragment.class);
                break;
            }
            default : {
                break;
            }
        }

    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mTypeSettings = intent.getIntExtra(TYPE_SETTINGS, ACCOUNT_SETTINGS);
        }
    }
}
