package com.decenturion.mobile.di.dagger.sign;

import com.decenturion.mobile.ui.fragment.sign.gfa.view.GfaSigninFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        GfaSigninModule.class
})

@GfaSigninScope
public interface GfaSigninComponent {

    void inject(GfaSigninFragment view);
}
