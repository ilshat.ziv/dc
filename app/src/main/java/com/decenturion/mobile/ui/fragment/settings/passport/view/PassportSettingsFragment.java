package com.decenturion.mobile.ui.fragment.settings.passport.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.passport.PassportSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.CalendarView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.ProfileImageView;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.dialog.SpinnerDateDialog;
import com.decenturion.mobile.ui.dialog.bottomsheet.PhotoDialogFragment;
import com.decenturion.mobile.ui.dialog.fullscreen.DateDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PassportSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.SexModelView;
import com.decenturion.mobile.ui.fragment.settings.passport.presenter.IPassportSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PassportSettingsFragment extends BaseFragment implements IPassportSettingsView,
        ProfileImageView.OnPhotoViewListener,
        View.OnClickListener,
        ChangeComponentListener,
        SpinnerView.OnSpinnerItemClickListener {

    /* UI */

    @BindView(R.id.firstNameView)
    EditTextView mFirstNameView;

    @BindView(R.id.secondNameView)
    EditTextView mSecondNameView;

    @BindView(R.id.birthdayView)
    CalendarView mBirthdayView;

    @BindView(R.id.sexView)
    SpinnerView mSexView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.cityView)
    EditTextView mCityView;

    @BindView(R.id.photoView)
    ProfileImageView mProfileImageView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScrollView;

    @BindView(R.id.saveChangesButtonView)
    FloatView mSaveChangesButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    IPassportSettingsPresenter mIPassportSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        PassportSettingsComponent passportSettingsComponent = appDelegate
                .getIDIManager()
                .plusPassportSettingsComponent();
        passportSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_passport_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSaveChangesButtonView.setEnabled(false);

        mFirstNameView.showLabel();
        mFirstNameView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_firstname_title));
        mFirstNameView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_passport_firstname_placeholder));
        mFirstNameView.editMode();
        mFirstNameView.setVisibleDivider(true);


        mSecondNameView.showLabel();
        mSecondNameView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_lastname_title));
        mSecondNameView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_passport_lastname_placeholder));
        mSecondNameView.editMode();
        mSecondNameView.setVisibleDivider(true);


        mBirthdayView.showLabel();
        mBirthdayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_dob_title));
        mBirthdayView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_passport_dob_placeholder));
        mBirthdayView.simpleMode();
        mBirthdayView.setVisibleDivider(true);


        mSexView.showLabel();
        mSexView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_sex_title));
        mSexView.setVisibleDivider(true);


        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_country_title));
        mCountryView.setVisibleDivider(true);


        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_passport_city_title));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_passport_city_placeholder));
        mCityView.editMode();
        mCityView.setVisibleDivider(true);


        mProfileImageView.setOnPhotoViewListener(this);
        mProfileImageView.editMode();

        mIFloatViewManager.bindIFloatView(mSaveChangesButtonView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_passport_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPassportSettingsPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIPassportSettingsPresenter.bindViewData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPassportSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIPassportSettingsPresenter.unbindView();
        mIPassportSettingsPresenter.onSaveInstanceState(outState);
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIPassportSettingsPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mProfileImageView.getResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIPassportSettingsPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mSaveChangesButtonView);
    }

    @OnClick(R.id.birthdayView)
    public void onBirth() {
        long date = DateTimeUtils.convertToLocalGMT(mBirthdayView.getDate());
        SpinnerDateDialog dateDialog = new SpinnerDateDialog(mBirthdayView, date,
                mILocalistionManager.getLocaleString(R.string.app_settings_passport_dob_placeholder));
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        dateDialog.show(fragmentManager, DateDialog.class.getSimpleName());
    }

    @Override
    public void onBindViewData(@NonNull SexModelView model2,
                               @NonNull CountryModelView model1,
                               @NonNull PassportSettingsModelView model) {

        mFirstNameView.setChangeDataModelListener(this);
        mSecondNameView.setChangeDataModelListener(this);
        mBirthdayView.setChangeDataModelListener(this);
        mSexView.setOnSpinnerItemClickListener(this);
        mCountryView.setOnSpinnerItemClickListener(this);
        mCityView.setChangeDataModelListener(this);

        mSexView.setData(
                mILocalistionManager.getLocaleString(R.string.app_settings_passport_sex_placeholder),
                model2.getSexData());

        mCountryView.setData(
                mILocalistionManager.getLocaleString(R.string.app_settings_passport_country_placeholder),
                model1.getCountryData());

        mFirstNameView.setText(model.getFirstname());
        mSecondNameView.setText(model.getLastname());

        String birth = model.getBirth();
        long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.SIMPLE_FORMAT);
        mBirthdayView.changeData(datetime);

        mSexView.selectItem(model.getSex());
        mCountryView.selectItem(model.getCountry());
        mCityView.setText(model.getCity());

        PhotoModelView photoModelView = model.getPhotoModelView();
        if (photoModelView != null) {
            mProfileImageView.addUrlImage(photoModelView.getOriginal());
        }
    }

    @Override
    public void onSavedChanges() {
//        IMainActivity activity = (IMainActivity) requireActivity();
//        INavigationManager iNavigationManager = activity.getINavigationManager();
//        iNavigationManager.navigateToBack();
    }

    @Override
    public void onUploadPhoto() {
        BottomSheetDialogFragment bottomSheetDialogFragment = new PhotoDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putBoolean(PhotoDialogFragment.Args.IS_CROP_IMAGE, true);
        bottomSheetDialogFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onShowPhoto(Bitmap bitmap) {
    }

    @Override
    public void onShowPhoto(String urlImage) {
    }

    @Override
    public void onPhotoUploaded(@NonNull Bitmap bitmap) {
        mIPassportSettingsPresenter.uploadPhoto(bitmap);
        onChangeComponentData(null);
    }

    @OnClick(R.id.saveChangesButtonView)
    public void onSaveChanges() {
        mIPassportSettingsPresenter.saveChanges(mFirstNameView.getValue(),
                mSecondNameView.getValue(),
                mBirthdayView.getDate(),
                mSexView.getSelection(),
                mCountryView.getSelection(),
                mCityView.getValue());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onChangeComponentData(View view) {
        try {
            boolean b = !TextUtils.isEmpty(mFirstNameView.getValue()) &&
                    !TextUtils.isEmpty(mSecondNameView.getValue()) &&
                    !TextUtils.isEmpty(mCityView.getValue()) &&
                    !TextUtils.isEmpty(mSexView.getSelection()) &&
                    !TextUtils.isEmpty(mCountryView.getSelection()) &&
                    mBirthdayView.getDate() > 0 &&
                    (mProfileImageView.getImage() != null || !TextUtils.isEmpty(mProfileImageView.getUrlImage()));

            mSaveChangesButtonView.setEnabled(b);
        } catch (Exception e) {
            // TODO: 26.09.2018 проследить в крашлитикс
            LoggerUtils.exception(e);
        }
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        onChangeComponentData(null);
    }
}
