package com.decenturion.mobile.business.seller.profile;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.seller.main.model.SellerProfileModelView;

import rx.Observable;

public interface ISellerProfileInteractor {

    Observable<SellerProfileModelView> getProfileInfo(@NonNull String residentUuid);
}
