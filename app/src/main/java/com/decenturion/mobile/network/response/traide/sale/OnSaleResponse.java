package com.decenturion.mobile.network.response.traide.sale;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnSaleResponse extends BaseResponse {

    @JsonProperty("result")
    private OnSaleResultResult mResult;

    public OnSaleResponse() {
    }

    @JsonIgnore
    public OnSaleResultResult getResult() {
        return mResult;
    }
}
