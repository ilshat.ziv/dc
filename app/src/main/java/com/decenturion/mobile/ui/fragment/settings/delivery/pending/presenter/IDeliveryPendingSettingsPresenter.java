package com.decenturion.mobile.ui.fragment.settings.delivery.pending.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.view.IDeliveryPendingSettingsView;

public interface IDeliveryPendingSettingsPresenter extends IPresenter<IDeliveryPendingSettingsView> {

    void bindViewData();
}
