package com.decenturion.mobile.ui.fragment.score.send.trade.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.score.send.trade.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.trade.view.ITradeScoreSendTokenView;

public interface ITradeScoreSendTokenPresenter extends IPresenter<ITradeScoreSendTokenView> {

    void bindViewData(int tokenId);

    void onSaveInstanceState(@NonNull Bundle outState,
                             String amount,
                             String twofa);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    void sendToken(@NonNull String amount,
                   MethodSendToken method,
                   @NonNull String twofa
    );
}
