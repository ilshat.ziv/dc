package com.decenturion.mobile.ui.component.spinner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.component.UBaseView;

import java.util.ArrayList;

import butterknife.BindView;

public class SpinnerView<I extends ISpinnerItem> extends UBaseView {

    /* UI */

    @Nullable
    @BindView(R.id.spinnerView)
    Spinner mSpinnerView;

    private SpinnerArrayAdapter<I> mSpinnerArrayAdapter;
    private ArrayList<I> mData = new ArrayList<>();

    private OnSpinnerItemClickListener mOnSpinnerItemClickListener;

    public SpinnerView(Context context) {
        super(context);
    }

    public SpinnerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mSpinnerView != null;
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {
        init();
        this.setOnClickListener(v -> {
            assert mSpinnerView != null;
            mSpinnerView.performClick();
        });
        assert mSpinnerView != null;
        mSpinnerView.setEnabled(true);
    }

    @Override
    public void simpleMode() {
        init();
        this.setOnClickListener(null);
        assert mSpinnerView != null;
        mSpinnerView.setEnabled(false);
    }

    public void setData(ArrayList<I> data) {
        setData(null, data);
    }

    public void setData(String hint, ArrayList<I> data) {
        init();

        if (!isInitView()) {
            return;
        }

        if (hint != null) {
            data.add(0, (I) new EmptyItem(hint));
        }

        if (mSpinnerArrayAdapter == null) {
            mSpinnerArrayAdapter = new SpinnerArrayAdapter<>(getContext(), R.layout.view_spinner_item, mData);
            mSpinnerArrayAdapter.setDropDownViewResource(R.layout.view_spinner_dropdown_item);
            assert mSpinnerView != null;
            mSpinnerView.setAdapter(mSpinnerArrayAdapter);
            mSpinnerView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mOnSpinnerItemClickListener != null) {
                        mOnSpinnerItemClickListener.onSpinnerItemClick(mData.get(position));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        mData.clear();
        mData.addAll(data);
        mSpinnerArrayAdapter.notifyDataSetChanged();
    }

    public void selectItem(@NonNull String item) {
        int position = mSpinnerArrayAdapter.getPositionByItemName(item);
        assert mSpinnerView != null;
        mSpinnerView.setSelection(position);
    }

    public String getSelection() {
        assert mSpinnerView != null;
        Object selectedItem1 = mSpinnerView.getSelectedItem();
        if (selectedItem1 == null) {
            return null;
        }
        I selectedItem = (I) selectedItem1;
        return selectedItem.getItemKey();
    }

    public void setOnSpinnerItemClickListener(OnSpinnerItemClickListener listener) {
        mOnSpinnerItemClickListener = listener;
    }

    public interface OnSpinnerItemClickListener<I extends ISpinnerItem> {

        void onSpinnerItemClick(I i);
    }
}