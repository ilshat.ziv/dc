package com.decenturion.mobile.business.settings.password;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.ChangePasswordModel;
import com.decenturion.mobile.network.response.password.ChangePasswordResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;

import rx.Observable;

public class PasswordSettingsInteractor implements IPasswordSettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public PasswordSettingsInteractor(IResidentClient IResidentClient,
                                      IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<Boolean> saveNewPassword(
            @NonNull String currentPassword,
            @NonNull String newPassword,
            @NonNull String retypePassword) {

        return Observable.zip(
                Observable.just(currentPassword),
                isValidNewPassword(newPassword, retypePassword),
                (s, s2) -> {
                    ChangePasswordModel model = new ChangePasswordModel();
                    model.setPassword(s2);
                    model.setOldPassword(s);

                    return model;
                })
                .flatMap(m -> mIResidentClient.changePassword(m))
                .map(ChangePasswordResponse::isChange);
    }

    private Observable<String> isValidNewPassword(String newPassword, String retypePassword) {
        Observable<String> obs1 = Observable.just(newPassword)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("New password is empty");
                    }
                    return s;
                });

        Observable<String> obs2 = Observable.just(retypePassword)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("Retype password is empty");
                    }
                    return s;
                });

        return Observable.zip(obs1, obs2, (s, s2) -> {
            if (!TextUtils.equals(s, s2)) {
                throw new RuntimeException("Passwords should be equal");
            }
            return s;
        });
    }

    private Observable<String> isValidCurrentPassword(String currentPassword) {
        return Observable.just(currentPassword)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("Current password is not be empty");
                    }
                    return s;
                });
    }
}
