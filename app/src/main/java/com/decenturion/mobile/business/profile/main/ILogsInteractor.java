package com.decenturion.mobile.business.profile.main;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.KillAllSessionsResponse;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;

import rx.Observable;

public interface ILogsInteractor {

    Observable<LogModelView> getLogsInfo();

    Observable<LogModelView> getLogsInfo(@NonNull LogModelView model);

    Observable<KillAllSessionsResponse> killAllSessions();
}
