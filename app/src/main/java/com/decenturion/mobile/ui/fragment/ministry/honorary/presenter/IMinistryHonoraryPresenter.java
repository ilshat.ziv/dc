package com.decenturion.mobile.ui.fragment.ministry.honorary.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.ministry.honorary.view.IMinistryHonoraryView;

import rx.Observable;

public interface IMinistryHonoraryPresenter extends IPresenter<IMinistryHonoraryView> {

    void bindViewData(@NonNull String category);

    void bindViewData(Observable<String> amount, Observable<String> coin);

    void getPaymentAddress(@NonNull String category, @NonNull String coin);
}
