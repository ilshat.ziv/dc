package com.decenturion.mobile.ui.fragment.pass.confirm.phone.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.model.ConfirmPhoneRestorePassModelView;

public interface IConfirmPhoneRestorePassView extends IScreenFragmentView {

    void onRestorePassSuccess();

    void onBindViewData(@NonNull ConfirmPhoneRestorePassModelView model);
}
