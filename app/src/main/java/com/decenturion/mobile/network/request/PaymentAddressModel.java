package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentAddressModel {

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("currency")
    private String mCurrency;

    public PaymentAddressModel() {
    }

    @JsonIgnore
    public PaymentAddressModel(String category, String currency) {
        mCategory = category;
        mCurrency = currency;
    }

    @JsonIgnore
    public void setCategory(String category) {
        mCategory = category;
    }

    @JsonIgnore
    public void setCurrency(String currency) {
        mCurrency = currency;
    }
}
