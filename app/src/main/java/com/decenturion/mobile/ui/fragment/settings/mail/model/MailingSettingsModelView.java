package com.decenturion.mobile.ui.fragment.settings.mail.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmResidentModel;

public class MailingSettingsModelView {

    private boolean isSubscribe;

    public MailingSettingsModelView() {
    }

    public MailingSettingsModelView(@NonNull OrmResidentModel m0) {
        this.isSubscribe = m0.isSubscribed();
    }

    public boolean isSubscribe() {
        return this.isSubscribe;
    }
}
