package com.decenturion.mobile.ui.fragment.settings.action.presenter;

import com.decenturion.mobile.business.profile.main.ILogsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.KillAllSessionsResponse;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModelView;
import com.decenturion.mobile.ui.fragment.settings.action.view.IActionListSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ActionListSettingsPresenter extends Presenter<IActionListSettingsView> implements IActionListSettingsPresenter {

    private ILogsInteractor mILogsInteractor;
    private LogModelView mActionListModelView;

    // TODO: 24.09.2018 костыль, поставить защиту от повторающихся запросов
    private boolean isLoadingPage;

    public ActionListSettingsPresenter(ILogsInteractor iLogsInteractor) {
        mILogsInteractor = iLogsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mActionListSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        if (isLoadingPage) {
            return;
        }

        showProgressView();

        Observable<LogModelView> obs = mILogsInteractor.getLogsInfo(
                new LogModelView()
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<LogModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(LogModelView model) {
        mActionListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void onBottomScrolled() {
        if (mActionListModelView.getOffset() >= mActionListModelView.getTotal() || isLoadingPage) {
            return;
        }
        uploadData();
    }

    private void uploadData() {
        showProgressView();

        Observable<LogModelView> obs = mILogsInteractor.getLogsInfo(
                mActionListModelView
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<LogModelView>) this::uploadDataSuccess,
                this::uploadDataFailure
        );
    }

    private void uploadDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void uploadDataSuccess(LogModelView model) {
        mActionListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    @Override
    public void killAllSessions() {
        showProgressView();

        Observable<KillAllSessionsResponse> obs = mILogsInteractor.killAllSessions()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<KillAllSessionsResponse>) this::killAllSessionsSuccess,
                this::killAllSessionsFailure
        );
    }

    private void killAllSessionsFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void killAllSessionsSuccess(KillAllSessionsResponse response) {
        hideProgressView();
        if (mView != null) {
            mView.logoutSuccess();
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        isLoadingPage = true;
    }

    @Override
    public void hideProgressView() {
        super.hideProgressView();
        isLoadingPage = false;
    }
}
