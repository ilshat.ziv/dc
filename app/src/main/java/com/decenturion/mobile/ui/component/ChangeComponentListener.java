package com.decenturion.mobile.ui.component;

import android.view.View;

public interface ChangeComponentListener {

    void onChangeComponentData(View view);
}