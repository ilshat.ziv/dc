package com.decenturion.mobile.ui.fragment.settings.details.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.details.view.IDetailsSettingsView;

public interface IDetailsSettingsPresenter extends IPresenter<IDetailsSettingsView> {

    void bindViewData();

    void uploadPhoto(@NonNull Bitmap photo);

    void saveChanges(@NonNull String bio,
                     @NonNull String telegram,
                     @NonNull String whatsApp,
                     @NonNull String viber,
                     @NonNull String wechat);
}
