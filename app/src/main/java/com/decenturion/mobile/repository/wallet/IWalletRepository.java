package com.decenturion.mobile.repository.wallet;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmWalletModel;
import com.decenturion.mobile.network.response.model.Wallet;

import java.util.List;

public interface IWalletRepository {

    void saveWalletInfo(@NonNull Wallet wallet) throws Exception;

    List<OrmWalletModel> getWalletInfo();
}
