package com.decenturion.mobile.network.response.ministry;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinistryResponse extends BaseResponse {

    @JsonProperty("result")
    private MinistryResult mResult;

    public MinistryResponse() {
    }

    @JsonIgnore
    public MinistryResult getResult() {
        return mResult;
    }
}
