package com.decenturion.mobile.ui.dialog.token.send;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.send.SendTokenComponent;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SendTokenSuccessFragmentDialog extends SingleDialogFragment {

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        SendTokenComponent sendTokenComponent = appDelegate
                .getIDIManager()
                .plusSendTokenComponent();
        sendTokenComponent.inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_account_send_success_title));

        MaterialDialog dialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_send_token_success);

        ButterKnife.bind(this, dialog.getView());
        setVisibilityPositiveClickButton(false);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }

}
