package com.decenturion.mobile.ui.fragment.pass.reset.model;

public class ResetPasswordModelView {

    private String mNewPassword;
    private String mReypePassword;

    public ResetPasswordModelView() {
    }

    public void setNewPassword(String newPassword) {
        mNewPassword = newPassword;
    }

    public void setReypePassword(String reypePassword) {
        mReypePassword = reypePassword;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public String getReypePassword() {
        return mReypePassword;
    }
}
