package com.decenturion.mobile.ui.fragment.sign.in.model;

import android.support.annotation.NonNull;

public class SigninModelView {

    private String mUsername;
    private String mPassword;

    public SigninModelView() {
    }

    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    public void setPassword(@NonNull String password) {
        mPassword = password;
    }

    public String getEmail() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

}
