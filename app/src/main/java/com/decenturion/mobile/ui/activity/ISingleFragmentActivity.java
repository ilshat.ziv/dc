package com.decenturion.mobile.ui.activity;

import com.decenturion.mobile.ui.dialog.IDialogManager;

public interface ISingleFragmentActivity extends ISimpleActivity {

    IDialogManager getIDialogManager();
}
