package com.decenturion.mobile.ui.floating;

import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;

public interface IFloatViewManager {

    void bindIFloatView(@NonNull IFloatView iFloatView, @NonNull NestedScrollView scrollView);

    void bindIFloatView(@NonNull IFloatView iFloatView);

    void unbindIFloatView(@NonNull IFloatView iFloatView);
}
