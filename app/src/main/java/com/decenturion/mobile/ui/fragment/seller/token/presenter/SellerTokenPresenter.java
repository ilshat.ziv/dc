package com.decenturion.mobile.ui.fragment.seller.token.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.seller.token.ISellerTokenInteractor;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.seller.token.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.seller.token.view.ISellerTokenView;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SellerTokenPresenter extends Presenter<ISellerTokenView> implements ISellerTokenPresenter {

    private Context mContext;
    private ISellerTokenInteractor mISellerTokenInteractor;
    private TokenModelView mTokenModelView;
    private CoinModelView mCoinModelView;

    public SellerTokenPresenter(Context context, ISellerTokenInteractor iTokenInteractor) {
        mContext = context;
        mISellerTokenInteractor = iTokenInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTokenModelView = null;
    }

    @Override
    public void bindViewData(@NonNull String residentUUID) {
        Observable<TokenModelView> obs = mISellerTokenInteractor.getTokenModelView(residentUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TokenModelView model) {
        mTokenModelView = model;

        ArrayList<Coin> list = initPaymentData();
        mCoinModelView = new CoinModelView(list);

        if (mView != null) {
            mView.onBindViewData(mCoinModelView, model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    private ArrayList<Coin> initPaymentData() {
        String json = FileUtils.loadJsonFromAssets(mContext, "data/payment.json");
        Object obj = JsonUtils.getCollectionFromJson(json, Coin.class);
        return (ArrayList<Coin>) obj;
    }
}
