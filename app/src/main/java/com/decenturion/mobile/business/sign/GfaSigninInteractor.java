package com.decenturion.mobile.business.sign;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.SigninGfaModel;
import com.decenturion.mobile.network.request.SigninModel;
import com.decenturion.mobile.network.response.signin.SigninResult;
import com.decenturion.mobile.network.response.signin.gfa.SigninGfaResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class GfaSigninInteractor implements IGfaSigninInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public GfaSigninInteractor(IResidentClient IResidentClient,
                               IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<SigninGfaResult> gfaSignin(@NonNull String gfaCode) {
        SigninGfaModel model = new SigninGfaModel();
        model.setCode(gfaCode);

        return Observable.just(model)
                .flatMap(m -> mIResidentClient.signin2gfa(m))
                .map(d -> {
                    SigninGfaResult result = d.getResult();
                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(result.getResident());
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    return result;
                });
    }

    private Observable<String> isValidPassword(@NonNull String password) {
        return Observable.just(password)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("Password is empty");
                    }
                    return s;
                });
    }

    private Observable<String> isVaildUserName(@NonNull String username) {
        return Observable.just(username)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("User name is empty");
                    }
                    return s;
                });
    }
}
