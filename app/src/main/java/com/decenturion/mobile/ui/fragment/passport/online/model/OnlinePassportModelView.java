package com.decenturion.mobile.ui.fragment.passport.online.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmPhotoModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.utils.DateTimeUtils;

public class OnlinePassportModelView {

    private String mName;
    private String mFirstname;
    private String mLastname;
    private String mBirth;
    private String mSex;
    private String mCountry;
    private String mCity;
    private PhotoModelView mPhotoModelView;

    public OnlinePassportModelView() {
    }

    public OnlinePassportModelView(@NonNull OrmResidentModel m0) {
        mName = m0.getEmail();
    }

    public OnlinePassportModelView(@NonNull OrmResidentModel m0, @NonNull OrmPassportModel m1, OrmPhotoModel m2) {
        mName = m0.getEmail();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();

        String birth = m1.getBirth();
        if (!TextUtils.isEmpty(birth)) {
            long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.FORMAT_ISO_8601);
            mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);
        }

        String sex = m1.getSex();
        mSex = sex == null ? "male" : sex;
        String country = m1.getCountry();
        mCountry = country == null ? "USA" : country;
        mCity = m1.getCity();

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public OnlinePassportModelView(@NonNull Resident m0, @NonNull Passport m1, Photo m2) {
        mName = m0.getEmail();
        mFirstname = m1.getFirstname();
        mLastname = m1.getLastname();

        String birth = m1.getBirth();
        if (!TextUtils.isEmpty(birth)) {
            long datetime = DateTimeUtils.getDatetime(birth, DateTimeUtils.Format.FORMAT_ISO_8601);
            mBirth = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.SIMPLE_FORMAT);
        }

        String sex = m1.getSex();
        mSex = sex == null ? "male" : sex;
        String country = m1.getCountry();
        mCountry = country == null ? "USA" : country;
        mCity = m1.getCity();

        if (m2 != null) {
            mPhotoModelView = new PhotoModelView(m2);
        }
    }

    public String getFirstname() {
        return mFirstname;
    }

    public String getLastname() {
        return mLastname;
    }

    public String getBirth() {
        return mBirth;
    }

    public String getSex() {
        return mSex;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public PhotoModelView getPhotoModelView() {
        return mPhotoModelView;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setFirstname(String firstname) {
        mFirstname = firstname;
    }

    public void setLastname(String lastname) {
        mLastname = lastname;
    }

    public void setBirth(String birth) {
        mBirth = birth;
    }

    public void setSex(String sex) {
        mSex = sex;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public void setPhotoModelView(PhotoModelView photoModelView) {
        mPhotoModelView = photoModelView;
    }
}
