package com.decenturion.mobile.ui.fragment.score.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.ui.fragment.score.details.model.ScoreTokenParamsModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoreTokenParamsAdapter extends RecyclerView.Adapter<ScoreTokenParamsAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<ScoreTokenParamsModelView> mTokenParamsList;

    private OnItemClickListener mOnItemClickListener;

    private ScoreTokenParamsAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public ScoreTokenParamsAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<ScoreTokenParamsModelView> list) {
        mTokenParamsList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTokenParamsList);
    }

    @NonNull
    @Override
    public ScoreTokenParamsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_token_param_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreTokenParamsAdapter.ViewHolder holder, int position) {
        final ScoreTokenParamsModelView param = mTokenParamsList.get(position);
        holder.bind(param);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final View view;

        @BindView(R.id.valueView)
        TextView valueView;

        @BindView(R.id.labelView)
        LocalizedTextView labelView;

        @BindView(R.id.iconView)
        ImageView iconView;

        ScoreTokenParamsModelView tokenParam;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void bind(@NonNull ScoreTokenParamsModelView param) {
            this.tokenParam = param;

            this.valueView.setText(param.getValue());
            this.labelView.setText(param.getName());
            this.iconView.setImageResource(param.getIconRes());
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(this.tokenParam);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ScoreTokenParamsModelView model);
    }

}
