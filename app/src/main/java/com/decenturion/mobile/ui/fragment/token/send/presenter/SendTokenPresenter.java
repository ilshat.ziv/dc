package com.decenturion.mobile.ui.fragment.token.send.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.send.ISendTokenInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.send.model.SendToModel;
import com.decenturion.mobile.ui.fragment.token.send.model.SendToModelView;
import com.decenturion.mobile.ui.fragment.token.send.model.SendTokenModelView;
import com.decenturion.mobile.ui.fragment.token.send.view.ISendTokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SendTokenPresenter extends Presenter<ISendTokenView> implements ISendTokenPresenter {

    private ISendTokenInteractor mISendTokenInteractor;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    private SendTokenModelView mSendTokenModelView;

    public SendTokenPresenter(ISendTokenInteractor iSendTokenInteractor,
                              IPrefsManager iPrefsManager,
                              ILocalistionManager iLocalistionManager) {
        mIPrefsManager = iPrefsManager;
        mISendTokenInteractor = iSendTokenInteractor;
        mILocalistionManager = iLocalistionManager;

        mSendTokenModelView = new SendTokenModelView();
    }

    @Override
    public void bindViewData(int tokenId) {
        mSendTokenModelView = new SendTokenModelView(tokenId);

        Observable<SendTokenModelView> obs = mISendTokenInteractor.getCoinInfo(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    String dcntWallet,
                                    String amount,
                                    String twofa) {

        bundle.putString("coinUUID", mSendTokenModelView.getCoinUUID());
        bundle.putString("coinCategory", mSendTokenModelView.getCoinCategory());
        bundle.putString("dcntWallet", dcntWallet);
        bundle.putString("amount", amount);
        bundle.putString("twofa", twofa);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        String coinUUID = savedInstanceState.getString("coinUUID", "");
        mSendTokenModelView.setCoinUUID(coinUUID);

        String coinCategory = savedInstanceState.getString("coinCategory", "");
        mSendTokenModelView.setCoinCategory(coinCategory);

        String dcntWallet = savedInstanceState.getString("dcntWallet", "");
        mSendTokenModelView.setDcntWallet(dcntWallet);

        String amount = savedInstanceState.getString("amount", "");
        mSendTokenModelView.setAmount(amount);

        mSendTokenModelView.setTwoFA(savedInstanceState.getString("twofa", ""));

        Observable<SendTokenModelView> obs = Observable.just(mSendTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SendTokenModelView model) {
        SendToModelView model1 = initSendToData();
        if (mView != null) {
            mView.onBindViewData(model, model1);
        }
        hideProgressView();
    }

    private SendToModelView initSendToData() {
        ArrayList<SendToModel> list = new ArrayList<>();
        list.add(new SendToModel(mILocalistionManager.getLocaleString(R.string.app_token_send_tocitizen), "0"));
        list.add(new SendToModel(mILocalistionManager.getLocaleString(R.string.app_token_send_toexternal), "1"));
        list.add(new SendToModel(mILocalistionManager.getLocaleString(R.string.app_token_send_totrade), "2"));

        return new SendToModelView(list);
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void sendToken(@NonNull String dcntWallet,
                          @NonNull String amount,
                          @NonNull String twofa) {

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        boolean b = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_TWOFA_ENABLED, false);
        if (!b) {
            if (mView != null) {
                mView.onEnableTwoFA();
            }
            return;
        }

        showProgressView();

        Observable<SendTokenResult> obs = mISendTokenInteractor.sendToken(
                mSendTokenModelView.getCoinUUID(),
                mSendTokenModelView.getCoinCategory(),
                dcntWallet,
                amount,
                twofa
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SendTokenResult>) this::sendTokenSuccess,
                this::sendTokenFailure
        );
    }

    private void sendTokenFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                switch (error.getKey()) {
                    default: {
                        showErrorView(error.getMessage());
                    }
                }
                return;
            }
        }

        showErrorView(throwable);
    }

    private void sendTokenSuccess(SendTokenResult result) {
        hideProgressView();
        if (mView != null) {
            mView.onSendTokenSuccess();
        }
    }
}
