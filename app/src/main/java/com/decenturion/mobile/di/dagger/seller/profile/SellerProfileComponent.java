package com.decenturion.mobile.di.dagger.seller.profile;

import com.decenturion.mobile.ui.fragment.seller.main.view.SellerProfileFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SellerProfileModule.class
})

@SellerProfileScope
public interface SellerProfileComponent {

    void inject(SellerProfileFragment view);
}
