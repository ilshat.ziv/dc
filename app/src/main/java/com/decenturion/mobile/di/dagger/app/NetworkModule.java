package com.decenturion.mobile.di.dagger.app;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;
import com.decenturion.mobile.app.prefs.token.TokenPrefsOptions;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.http.AddCookiesInterceptor;
import com.decenturion.mobile.network.http.ReceivedCookiesInterceptor;
import com.decenturion.mobile.network.http.adapter.RxJavaCallAdapterFactory;
import com.decenturion.mobile.network.http.converter.JacksonConverterFactory;

import com.decenturion.mobile.network.response.csrf.CsrfResult;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

@Module
public class NetworkModule {

    private String mBaseUrl;
    private File mCacheDir;
    protected IPrefsManager mIPrefsManager;

    private IDeviceClient mIDeviceClient;
    private Observable<Boolean> mRefreshCsrfTokenObs;

    private RxJavaCallAdapterFactory mRxJavaCallAdapterFactory;
    private Retrofit mRetrofit;

    private AddCookiesInterceptor mAddCookiesInterceptor;
    private ReceivedCookiesInterceptor mReceivedCookiesInterceptor;

    public NetworkModule(@NonNull String baseUrl, @NonNull IPrefsManager iPrefsManager) {
        mBaseUrl = baseUrl;
        mIPrefsManager = iPrefsManager;

        mAddCookiesInterceptor = new AddCookiesInterceptor(iPrefsManager);
        mReceivedCookiesInterceptor = new ReceivedCookiesInterceptor(iPrefsManager);

        mRxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create(mRefreshCsrfTokenObs);
        initCsrfTokenObs();
        mRetrofit = initRetrofit();
    }

    public NetworkModule(@NonNull String baseUrl, @NonNull IPrefsManager iPrefsManager, File cacheDir) {
        mBaseUrl = baseUrl;
        mCacheDir = cacheDir;

        mAddCookiesInterceptor = new AddCookiesInterceptor(iPrefsManager);
        mReceivedCookiesInterceptor = new ReceivedCookiesInterceptor(iPrefsManager);

        mRxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();
        mRetrofit = initRetrofit();
    }

    private void initCsrfTokenObs() {
        if (mRefreshCsrfTokenObs != null) {
            return;
        }

        if (mIDeviceClient == null) {
            mIDeviceClient = new Retrofit.Builder()
                    .baseUrl(mBaseUrl)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create(null))
                    .addConverterFactory(JacksonConverterFactory.create(getObjectMapper()))
                    .client(getHttpClient())
                    .build()
                    .create(IDeviceClient.class);
        }

        rx.Observable<Boolean> obs = mIDeviceClient.getCsrfToken()
                .subscribeOn(Schedulers.newThread())
                .map(response -> {
                    CsrfResult result = response.getResult();
                    ITokensPrefsManager iTokensPrefsManager = mIPrefsManager.getITokensPrefsManager();
                    iTokensPrefsManager.setParam(TokenPrefsOptions.Keys.CSRF_TOKEN, result.getToken());
                    return true;
                });


        mRefreshCsrfTokenObs = obs
                .doOnCompleted(() -> {
                    mRefreshCsrfTokenObs = null;
                    initCsrfTokenObs();
                })
                .doOnError(throwable -> {
                    mRefreshCsrfTokenObs = null;
                    initCsrfTokenObs();
                });

        mRxJavaCallAdapterFactory.setObservableCsrfAccessException(mRefreshCsrfTokenObs);
    }

    private Retrofit initRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(mRxJavaCallAdapterFactory)
                .addConverterFactory(JacksonConverterFactory.create(getObjectMapper()))
                .client(getHttpClient())
                .build();
    }

    private Cache cache(File cacheDir) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(cacheDir, cacheSize);
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(20, TimeUnit.SECONDS);
        httpClient.readTimeout(20, TimeUnit.SECONDS);
        if (mCacheDir != null) {
            httpClient.cache(cache(mCacheDir));
        }

        httpClient.addInterceptor(mAddCookiesInterceptor);
        httpClient.addInterceptor(mReceivedCookiesInterceptor);

        return httpClient.build();
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
        objectMapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);

        return objectMapper;
    }

    @Singleton
    @Provides
    public NetworkModule provideNetworkModule() {
        return this;
    }

    @Singleton
    @Provides
    public Retrofit provideRetrofit() {
        return mRetrofit;
    }
}
