package com.decenturion.mobile.ui.fragment.sign.in.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.sign.in.view.ISigninView;

public interface ISigninPresenter extends IPresenter<ISigninView> {

    void signin(@NonNull String username, @NonNull String password, String captcha);

    void bindViewData();

    void onSaveInstanceState(@NonNull Bundle outState);

    void onSaveInstanceState(@NonNull Bundle outState, @NonNull String username, @NonNull String password);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
