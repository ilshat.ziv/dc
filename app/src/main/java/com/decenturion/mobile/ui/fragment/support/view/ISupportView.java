package com.decenturion.mobile.ui.fragment.support.view;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;

public interface ISupportView extends IScreenFragmentView {

    void onSendMessageSuccess();
}
