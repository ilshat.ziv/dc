package com.decenturion.mobile.ui.fragment.referral.acquisition.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.referral.acquisition.IAcquisitionInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.acquisition.model.AcquisitionModelView;
import com.decenturion.mobile.ui.fragment.referral.acquisition.view.IAcquisitionView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class AcquisitionPresenter extends Presenter<IAcquisitionView> implements IAcquisitionPresenter {

    private IAcquisitionInteractor mIAcquisitionInteractor;
    private AcquisitionModelView mAcquisitionModelView;

    public AcquisitionPresenter(IAcquisitionInteractor iAcquisitionInteractor) {
        mIAcquisitionInteractor = iAcquisitionInteractor;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<AcquisitionModelView> obs = mIAcquisitionInteractor.getReferralInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<AcquisitionModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);

    }

    private void bindViewDataSuccess(AcquisitionModelView model) {
        mAcquisitionModelView = model;

        hideProgressView();

        if (mView != null) {
            mView.onBindViewData(model);
        }
    }

    @Override
    public void setPrice(String type, String price, String amount, String bannerText) {
        showProgressView();

        Observable<ReferralSettingsResult> obs = mIAcquisitionInteractor.setPrice(
                type,
                price,
                proccessCount(amount),
                bannerText
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ReferralSettingsResult>) this::setPriceSuccess,
                this::setPriceFailure
        );
    }

    private int proccessCount(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            LoggerUtils.exception(e);
        }

        return 0;
    }

    private void setPriceFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);

    }

    private void setPriceSuccess(ReferralSettingsResult model) {
        hideProgressView();

        if (mView != null) {
            mView.onSetPriceSuccess();
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
