package com.decenturion.mobile.network.response.resident;

import com.decenturion.mobile.network.response.model.ClassicBalance;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.Settings;
import com.decenturion.mobile.network.response.model.Wallet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResidentResult {

    @JsonProperty("locale")
    private String mLocale;

    @JsonProperty("passport")
    private Passport mPassport;

    @JsonProperty("photo")
    private Photo mPhoto;

    @JsonProperty("resident")
    private Resident mResident;

    @JsonProperty("settings")
    private Settings mSettings;

    @JsonProperty("wallets")
    private List<Wallet> mWalletList;

    @JsonProperty("classic_balances")
    private ClassicBalance mClassicBalance;


    // Требуется при отправке токена на внешний кошелек

    @JsonProperty("passport_coin")
    private String mPassportCoin;

    @JsonProperty("eth_balance")
    private String mEthBalance;



    public ResidentResult() {
    }

    @JsonIgnore
    public String getLocale() {
        return mLocale;
    }

    @JsonIgnore
    public Passport getPassport() {
        return mPassport;
    }

    @JsonIgnore
    public Photo getPhoto() {
        return mPhoto;
    }

    @JsonIgnore
    public Resident getResident() {
        return mResident;
    }

    @JsonIgnore
    public Settings getSettings() {
        return mSettings;
    }

    @JsonIgnore
    public List<Wallet> getWalletList() {
        return mWalletList;
    }

    @JsonIgnore
    public ClassicBalance getClassicBalance() {
        return mClassicBalance;
    }


    @JsonIgnore
    public String getPassportCoin() {
        return mPassportCoin;
    }

    @JsonIgnore
    public String getEthBalance() {
        return mEthBalance;
    }
}
