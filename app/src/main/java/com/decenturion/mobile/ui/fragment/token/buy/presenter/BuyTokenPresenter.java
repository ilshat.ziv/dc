package com.decenturion.mobile.ui.fragment.token.buy.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.business.tokendetails.buy.IBuyTokenInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.buy.model.BuyTokenModelView;
import com.decenturion.mobile.ui.fragment.token.buy.view.IBuyTokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class BuyTokenPresenter extends Presenter<IBuyTokenView> implements IBuyTokenPresenter {

    private IBuyTokenInteractor mIBuyTokenInteractor;

    private BuyTokenModelView mBuyTokenModelView;

    public BuyTokenPresenter(IBuyTokenInteractor iBuyTokenInteractor) {
        mIBuyTokenInteractor = iBuyTokenInteractor;
        mBuyTokenModelView = new BuyTokenModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    @NonNull String receiveAddress,
                                    boolean isInternal,
                                    boolean isAgreeTerms,
                                    boolean isGas) {

        bundle.putString("OnSale", mBuyTokenModelView.getOnSale());
        bundle.putString("PriceFor1", mBuyTokenModelView.getPriceFor1());
        bundle.putString("Total", mBuyTokenModelView.getTotal());
        bundle.putString("NumberOfTokens", mBuyTokenModelView.getNumberOfTokens());

        bundle.putString("ResidentUUID", mBuyTokenModelView.getResidentUUID());
        bundle.putString("UUID", mBuyTokenModelView.getUUID());
        bundle.putString("Category", mBuyTokenModelView.getCategory());
        bundle.putString("Cyrrency", mBuyTokenModelView.getCyrrency());
        bundle.putString("Coin", mBuyTokenModelView.getCoin());

        bundle.putString("receiveAddress", receiveAddress);
        bundle.putBoolean("isInternal", isInternal);
        bundle.putBoolean("isAgreeTerms", isAgreeTerms);
        bundle.putBoolean("isGas", isGas);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mBuyTokenModelView.setOnSale(savedInstanceState.getString("OnSale", ""));
        mBuyTokenModelView.setPriceFor1(savedInstanceState.getString("PriceFor1", ""));
        mBuyTokenModelView.setTotal(savedInstanceState.getString("Total", ""));
        mBuyTokenModelView.setNumberOfTokens(savedInstanceState.getString("NumberOfTokens", ""));

        mBuyTokenModelView.setResidentUUID(savedInstanceState.getString("ResidentUUID", ""));
        mBuyTokenModelView.setUUID(savedInstanceState.getString("UUID", ""));
        mBuyTokenModelView.setCategory(savedInstanceState.getString("Category", ""));
        mBuyTokenModelView.setCyrrency(savedInstanceState.getString("Cyrrency", ""));
        mBuyTokenModelView.setCoin(savedInstanceState.getString("Coin", ""));

        mBuyTokenModelView.setReceiveAddress(savedInstanceState.getString("receiveAddress", ""));
        mBuyTokenModelView.setInternalTransaction(savedInstanceState.getBoolean("isInternal", false));
        mBuyTokenModelView.setAgreeTermsOfuse(savedInstanceState.getBoolean("isAgreeTerms", false));
        mBuyTokenModelView.setIsGas(savedInstanceState.getBoolean("isGas", false));

        Observable<BuyTokenModelView> obs = Observable.just(mBuyTokenModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<BuyTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(@NonNull String sellerUUID,
                             int tokenId,
                             @NonNull String paymentMethod,
                             @NonNull String category,
                             @NonNull String coinId) {

        Observable<BuyTokenModelView> obs = mIBuyTokenInteractor.getInfoSale(
                1,
                category,
                coinId,
                paymentMethod,
                sellerUUID
        )
                .map(BuyTokenModelView::new)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

//        Observable<StateTraideResult> obs = mIBuyTokenInteractor.getTraide()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());

//        Observable<ConfirmTraideResult> obs = mIBuyTokenInteractor.confirmTraide()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());

        mBuyTokenModelView = new BuyTokenModelView(sellerUUID, tokenId, paymentMethod);

        mIDCompositeSubscription.subscribe(obs,
                (Action1<BuyTokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(BuyTokenModelView model) {
        mBuyTokenModelView.setCoin(model.getCoin());
        mBuyTokenModelView.setCategory(model.getCategory());
        mBuyTokenModelView.setNumberOfTokens(model.getNumberOfTokens());
        mBuyTokenModelView.setOnSale(model.getOnSale());
        mBuyTokenModelView.setPriceFor1(model.getPriceFor1());
        mBuyTokenModelView.setTotal(model.getTotal());
        mBuyTokenModelView.setUUID(model.getUUID());

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void createTraide(@Nullable String receiveAddress, boolean isInternal) {
        showProgressView();

        Observable<CreateTraideResult> obs = mIBuyTokenInteractor.createTraide(
                receiveAddress,
                isInternal,
                mBuyTokenModelView.getCategory(),
                mBuyTokenModelView.getUUID(),
                mBuyTokenModelView.getCyrrency(),
                mBuyTokenModelView.getResidentUUID())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<CreateTraideResult>) this::createTraideSuccess,
                this::createTraideFailure
        );
    }

    private void createTraideFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void createTraideSuccess(CreateTraideResult createTraideResult) {
        hideProgressView();
        if (mView != null) {
            mView.onTraideCreated(createTraideResult);
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
