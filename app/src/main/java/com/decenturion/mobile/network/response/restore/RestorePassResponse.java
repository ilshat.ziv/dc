package com.decenturion.mobile.network.response.restore;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestorePassResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean mResult;

    public RestorePassResponse() {
    }

    @JsonIgnore
    public boolean getResult() {
        return mResult;
    }
}
