package com.decenturion.mobile.business.passport.wellcome;

import com.decenturion.mobile.network.response.signup.skip.SignupSkipResult;
import com.decenturion.mobile.ui.fragment.passport.wellcome.model.WellcomePassportModelView;

import rx.Observable;

public interface IWellcomPassportInteractor {

    Observable<WellcomePassportModelView> getResidentInfo();

    Observable<SignupSkipResult> skipWellcomeMessage();
}
