package com.decenturion.mobile.ui.fragment.settings.delivery.fill.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.fill.IFillDeliverySettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.DeliveryDataModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.model.FillDeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.fill.view.IFillDeliverySettingsView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class FillDeliverySettingsPresenter extends Presenter<IFillDeliverySettingsView> implements IFillDeliverySettingsPresenter {

    private IFillDeliverySettingsInteractor mIFillDeliverySettingsInteractor;
    private ILocalistionManager mILocalistionManager;

    private FillDeliverySettingsModelView mPhisicalPassportModelView;
    private CountryModelView mCountryModelView;

    public FillDeliverySettingsPresenter(Context context,
                                         IFillDeliverySettingsInteractor iFillDeliverySettingsInteractor,
                                         IPrefsManager iPrefsManager,
                                         ILocalistionManager iLocalistionManager) {

        mIFillDeliverySettingsInteractor = iFillDeliverySettingsInteractor;
        mILocalistionManager = iLocalistionManager;

        mPhisicalPassportModelView = new FillDeliverySettingsModelView();
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<FillDeliverySettingsModelView> obs = mIFillDeliverySettingsInteractor.getDeliveryData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<FillDeliverySettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void setDeliveryData(
            @NonNull String phone,
            @NonNull String address,
            @NonNull String city,
            @NonNull String state,
            @NonNull String country,
            @NonNull String zipcode) {

        if (mPhisicalPassportModelView.isInvited() && !mPhisicalPassportModelView.isActive()
                && TextUtils.isEmpty(address)) {

            hideProgressView();
            if (mView != null) {
                mView.onDeliveryPassportDataSeted(mPhisicalPassportModelView);
            }
            return;
        }

        showProgressView();

        Observable<DeliveryPassportResult> obs = mIFillDeliverySettingsInteractor.setDeliveryData(
                phone,
                address,
                city,
                state,
                country,
                zipcode
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliveryPassportResult>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(DeliveryPassportResult result) {
        hideProgressView();

        Resident resident = result.getResident();
        mPhisicalPassportModelView.setActive(resident.isActive());
        DeliveryDataModelView deliverDataModelView = mPhisicalPassportModelView.getDeliverDataModelView();
        deliverDataModelView.setData(result);

        if (mView != null) {
            mView.onDeliveryPassportDataSeted(mPhisicalPassportModelView);
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void uploadPhoto(@NonNull Bitmap photo) {
        showProgressView();

        Observable<UploadPhotoResponse> obs = mIFillDeliverySettingsInteractor.uploadPhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UploadPhotoResponse>) this::uploadPhotoSuccess,
                this::uploadPhotoFailure
        );
    }

    private void uploadPhotoSuccess(UploadPhotoResponse uploadPhotoResponse) {
        hideProgressView();
    }

    private void uploadPhotoFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void bindViewDataSuccess(FillDeliverySettingsModelView model) {
        mPhisicalPassportModelView = model;

        ArrayList<Country> list1 = initCountryData();
        mCountryModelView = new CountryModelView(list1);
        if (mView != null) {
            mView.onBindViewData(mCountryModelView, model);
        }
        hideProgressView();
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
