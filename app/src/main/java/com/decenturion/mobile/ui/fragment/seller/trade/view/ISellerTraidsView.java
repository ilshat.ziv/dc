package com.decenturion.mobile.ui.fragment.seller.trade.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.seller.trade.model.SellerTraideModelView;

public interface ISellerTraidsView extends IScreenFragmentView {

    void onBindViewData(@NonNull SellerTraideModelView model);
}
