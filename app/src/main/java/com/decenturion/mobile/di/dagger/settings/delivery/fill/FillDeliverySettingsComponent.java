package com.decenturion.mobile.di.dagger.settings.delivery.fill;

import com.decenturion.mobile.ui.fragment.settings.delivery.fill.view.FillDeliverySettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        FillDeliverySettingsModule.class
})

@FillDeliverySettingsScope
public interface FillDeliverySettingsComponent {

    void inject(FillDeliverySettingsFragment view);
}
