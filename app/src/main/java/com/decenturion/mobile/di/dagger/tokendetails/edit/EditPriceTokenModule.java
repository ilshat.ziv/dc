package com.decenturion.mobile.di.dagger.tokendetails.edit;

import com.decenturion.mobile.business.token.edit.EditPriceTokenInteractor;
import com.decenturion.mobile.business.token.edit.IEditPriceTokenInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.token.edit.presenter.EditPriceTokenPresenter;
import com.decenturion.mobile.ui.fragment.token.edit.presenter.IEditPriceTokenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class EditPriceTokenModule {

    @Provides
    @EditPriceTokenScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @EditPriceTokenScope
    IEditPriceTokenInteractor provideIEditPriceTokenInteractor(IResidentClient IResidentClient,
                                                               ITokenClient iTokenClient,
                                                               ITokenRepository ITokenRepository) {

        return new EditPriceTokenInteractor(IResidentClient, iTokenClient, ITokenRepository);
    }

    @Provides
    @EditPriceTokenScope
    IEditPriceTokenPresenter provideIEditPriceTokenPresenter(IEditPriceTokenInteractor iEditPriceTokenInteractor) {
        return new EditPriceTokenPresenter(iEditPriceTokenInteractor);
    }
}
