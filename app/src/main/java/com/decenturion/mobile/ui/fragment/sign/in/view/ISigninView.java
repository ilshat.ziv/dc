package com.decenturion.mobile.ui.fragment.sign.in.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.sign.in.model.SigninModelView;

public interface ISigninView extends IScreenFragmentView {

    void onSigninSuccess(int step);

    void onBindViewData(@NonNull SigninModelView model);

    void onResendEmail();

    void onResendEmail(@NonNull String email);

    void onSignByG2fa();

    void onAccessRestricted();
}
