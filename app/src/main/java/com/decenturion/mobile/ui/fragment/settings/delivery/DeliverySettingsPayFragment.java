package com.decenturion.mobile.ui.fragment.settings.delivery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.settings.delivery.DeliverySettingsComponent;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.UserDataView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.presenter.IDeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.view.IDeliverySettingsView;
import com.decenturion.mobile.ui.fragment.settings.passport.model.CountryModelView;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class DeliverySettingsPayFragment extends BaseFragment implements IDeliverySettingsView,
        View.OnClickListener,
        SpinnerView_v2.OnSpinnerItemClickListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.payPassportDataView)
    LinearLayout mPayPassportDataView;

    @BindView(R.id.summPayView)
    BuyTokenView mSummPayView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    @BindView(R.id.payActionView)
    LinearLayout mPayActionView;



    @BindView(R.id.textView)
    LocalizedTextView mLabelView;

    @BindView(R.id.phoneView)
    UserDataView mPhoneView;

    @BindView(R.id.addressView)
    UserDataView mAddressView;

    @BindView(R.id.cityView)
    UserDataView mCityView;

    @BindView(R.id.stateView)
    UserDataView mStateView;

    @BindView(R.id.countryView)
    UserDataView mCountryView;

    @BindView(R.id.zipView)
    UserDataView mZipView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mProceedButtonView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IDeliverySettingsPresenter mIDeliverySettingsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        DeliverySettingsComponent deliverySettingsComponent = appDelegate
                .getIDIManager()
                .plusDeliverySettingsComponent();
        deliverySettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_delivery_passport_pay, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_phone));
        mPhoneView.editMode();

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_address));
        mAddressView.editMode();

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_city));
        mCityView.editMode();

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_state));
        mStateView.editMode();

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_country));

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_zip));
        mZipView.editMode();

        // TODO: 19.09.2018 костыль, слишком сложная структура перевода
        String localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();

        mQrCodeViewComponent.hideLabel();
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_order_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIDeliverySettingsPresenter.bindView(this);
        mIDeliverySettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIDeliverySettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIDeliverySettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIDeliverySettingsPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(DeliverySettingsPayCompleteFragment.class, true);
    }


    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model3,
                               @NonNull CountryModelView model1,
                               @NonNull DeliverySettingsModelView model2) {

        mPhoneView.setText(model2.getPhone());
        mAddressView.setText(model2.getAddress());
        mCityView.setText(model2.getCity());
        mStateView.setText(model2.getState());
        mCountryView.setText(model2.getCountry());
        mZipView.setText(model2.getZip());

        if (!model2.isDeliveryPaid()) {

            ArrayList<Coin> coinData = model3.getCoinData();
            Coin coin = coinData.get(0);
            mSelectCoinView.setData(coinData);
            mSelectCoinView.setOnSpinnerItemClickListener(this);
            mSelectCoinView.selectItem(coin.getItemKey());
            mPayPassportDataView.setVisibility(View.VISIBLE);
        } else {
            mPayPassportDataView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBindPayModelView(@NonNull PaymentModelView model) {
        if (!TextUtils.isEmpty(model.getCoinsPay())) {
            String summPay = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_amount_value);
            summPay = summPay.replace("{amount}", model.getCoinsPay());
            summPay = summPay.replace("{currency}", model.getTypePay());
            mSummPayView.setText(summPay);
            mSummPayView.setCoin(model.getCyrrencePay());

            mQrCodeViewComponent.setAddress(model.getAddress());
            mQrCodeViewComponent.setQrData(model.getQrCode());

            mPayActionView.setVisibility(View.VISIBLE);
        } else {
            mPayActionView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDeliveryPassportDataSeted() {
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mIDeliverySettingsPresenter.getPaymentDate(iSpinnerItem.getItemKey());
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
