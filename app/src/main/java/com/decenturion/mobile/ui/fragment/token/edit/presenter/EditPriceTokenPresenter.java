package com.decenturion.mobile.ui.fragment.token.edit.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.token.edit.IEditPriceTokenInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.edit.model.EditPriceModelView;
import com.decenturion.mobile.ui.fragment.token.edit.view.IEditPriceTokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class EditPriceTokenPresenter extends Presenter<IEditPriceTokenView> implements IEditPriceTokenPresenter {

    private IEditPriceTokenInteractor mIEditPriceTokenInteractor;
    private EditPriceModelView mEditPriceModelView;

    public EditPriceTokenPresenter(IEditPriceTokenInteractor iEditPriceTokenInteractor) {
        mIEditPriceTokenInteractor = iEditPriceTokenInteractor;
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mEditPriceModelView = new EditPriceModelView(tokenId);

        Observable<EditPriceModelView> obs = mIEditPriceTokenInteractor.getTokenDetailsModule(tokenId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EditPriceModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(EditPriceModelView model) {
        mEditPriceModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void saveChanges(int tokenId, @NonNull String sell) {
        showProgressView();

        Observable<EditPriceModelView> obs = mIEditPriceTokenInteractor.saveChanges(
                tokenId,
                mEditPriceModelView.getCoinId(),
                sell,
                mEditPriceModelView.getCategory())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EditPriceModelView>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    int tokenId,
                                    @NonNull String buy,
                                    @NonNull String sell) {
        bundle.putInt("tokenId", tokenId);
        bundle.putString("buy", buy);
        bundle.putString("sell", sell);
        bundle.putString("min_sell_price", mEditPriceModelView.getSellMinPrice());
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mEditPriceModelView.setBuy(savedInstanceState.getString("buy", ""));
        mEditPriceModelView.setSell(savedInstanceState.getString("sell", ""));
        mEditPriceModelView.setTokenId(savedInstanceState.getInt("tokenId", 0));
        mEditPriceModelView.setSellMinPrice(savedInstanceState.getString("min_sell_price", ""));

        Observable<EditPriceModelView> obs = Observable.just(mEditPriceModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EditPriceModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void saveChangesSuccess(EditPriceModelView model) {
        if (mView != null) {
            mView.onChangesSaved();
        }
        hideProgressView();
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEditPriceModelView = null;
    }
}
