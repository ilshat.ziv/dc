package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateInvite {

    @JsonProperty("price")
    private String mPrice;


    public CreateInvite() {
    }

    @JsonIgnore
    public CreateInvite(String price) {
        mPrice = price;
    }

    @JsonIgnore
    public void setPrice(String price) {
        mPrice = price;
    }
}
