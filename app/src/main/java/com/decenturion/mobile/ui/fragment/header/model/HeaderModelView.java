package com.decenturion.mobile.ui.fragment.header.model;

import android.support.annotation.NonNull;

public class HeaderModelView {

    private String mName;
    private String mStatus;

    public HeaderModelView(@NonNull String name, @NonNull String status) {
        mName = name;
        mStatus = status;
    }

    public String getName() {
        return mName;
    }

    public String getStatus() {
        return mStatus;
    }
}
