package com.decenturion.mobile.ui.fragment.twofa.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.di.dagger.twofa.EnableTwoFAComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.twofa.model.EnableTwoFAModelView;
import com.decenturion.mobile.ui.fragment.twofa.presenter.IEnableTwoFAPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class EnableTwoFAFragment extends BaseFragment implements IEnableTwoFAView,
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.qrCodeComponentView)
    QrCodeViewComponent mQrCodeView;

    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IEnableTwoFAPresenter mISecurityPresenter;

    @Inject
    IPrefsManager mIPrefsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        EnableTwoFAComponent component = appDelegate
                .getIDIManager()
                .plusEnableTwoFAComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fr_security_twofa, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_code_title));
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mQrCodeView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_key));
        mQrCodeView.setOnQrCodeViewClickListener(this);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_enable));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISecurityPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISecurityPresenter.bindViewData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mISecurityPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISecurityPresenter.unbindView();
        mISecurityPresenter.onSaveInstanceState(outState,
                mQrCodeView.getAddress(), mSecretCodeView.getValue());
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISecurityPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISecurityPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mISecurityPresenter.saveChanges(mQrCodeView.getAddress(), mSecretCodeView.getValue());
    }

    @Override
    public void onBindViewData(@NonNull EnableTwoFAModelView model) {
        mSecretCodeView.setText(model.getSecretCode());
        String secretString = model.getSecretString();
        mQrCodeView.setAddress(secretString);

        String authFormat = "otpauth://totp/{email}?secret={secret}&issuer=Decenturion";
        String string = authFormat.replace("{secret}", secretString);

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        String email = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, "");
        string = string.replace("{email}", email);

        mQrCodeView.setQrData(string);
    }

    @Override
    public void onChangesSaved() {
        FragmentActivity activity = requireActivity();
        activity.setResult(Activity.RESULT_OK);
        activity.finish();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Boolean state = (Boolean) buttonView.getTag();
        if (state) {
            if (isChecked) {
                mSecretCodeView.setVisibility(View.GONE);
            } else {
                mSecretCodeView.setVisibility(View.VISIBLE);
            }
        } else {
            if (isChecked) {
                mSecretCodeView.setVisibility(View.VISIBLE);
            } else {
                mSecretCodeView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
