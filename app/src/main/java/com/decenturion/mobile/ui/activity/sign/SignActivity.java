package com.decenturion.mobile.ui.activity.sign;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.pass.reset.view.ResetPasswordFragment;
import com.decenturion.mobile.ui.fragment.sign.in.view.SigninFragment;
import com.decenturion.mobile.ui.fragment.sign.up.view.SignupFragment;
import com.decenturion.mobile.ui.fragment.validate.view.ValidateEmailFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class SignActivity extends SingleFragmentActivity implements ISignActivity {

    public static final String CONFIRM_EMAIL_TOKEN = "CONFIRM_EMAIL_TOKEN";
    public static final String INVITE_TOKEN = "INVITE_TOKEN";
    public static final String RESET_TOKEN = "RESET_TOKEN";
    public static final String REFERALL_KEY = "REFERALL_KEY";
    public static final String PAY_REFERALL_KEY = "PAY_REFERALL_KEY";
    public static final String BANNER_REFERALL_KEY = "BANNER_REFERALL_KEY";

    private String mInviteToken;
    private String mReferallKey;
    private String mPayReferallKey;
    private String mBannerReferallKey;
    private String mConfirmToken;
    private String mResetToken;

    private Bundle mBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iniNavigation(savedInstanceState, getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        iniNavigation(null, intent);
    }

    private void iniNavigation(@Nullable Bundle savedInstanceState, Intent intent) {
        initArgs(intent);
        if (savedInstanceState != null) {
            getINavigationManager().restoreInstanceState(savedInstanceState);
            return;
        }

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        int step = iClientPrefsManager.getParam(ClientPrefOptions.Keys.SIGN_STEP, -1);
        INavigationManager iNavigationManager = getINavigationManager();
        switch (step) {
            case -1: {
                if (!TextUtils.isEmpty(mInviteToken)
                        || !TextUtils.isEmpty(mReferallKey)
                        || !TextUtils.isEmpty(mPayReferallKey)
                        || !TextUtils.isEmpty(mBannerReferallKey)) {
                    iNavigationManager.navigateTo(SignupFragment.class, mBundle);
                } else if (!TextUtils.isEmpty(mResetToken)) {
                    iNavigationManager.navigateTo(ResetPasswordFragment.class, mBundle);
                } else {
                    iNavigationManager.navigateTo(SigninFragment.class);
                }
                break;
            }
            case 0: {
                iNavigationManager.navigateTo(ValidateEmailFragment.class, mBundle);
                break;
            }
            default: {
                iNavigationManager.navigateTo(SigninFragment.class);
            }
        }
    }

    private void initArgs(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            mBundle = intent.getExtras();

            mInviteToken = intent.getStringExtra(INVITE_TOKEN);
            mConfirmToken = intent.getStringExtra(CONFIRM_EMAIL_TOKEN);
            mResetToken = intent.getStringExtra(RESET_TOKEN);

            mReferallKey = intent.getStringExtra(REFERALL_KEY);
            mPayReferallKey = intent.getStringExtra(PAY_REFERALL_KEY);
            mBannerReferallKey = intent.getStringExtra(BANNER_REFERALL_KEY);
        }
    }
}
