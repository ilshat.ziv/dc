package com.decenturion.mobile.rx;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;

public interface IDCompositeSubscription {

    Subscription subscribe(Observable<?> observable, Action1<?> successAction, Action1<Throwable> failAction, Action0 compliteAction);

    Subscription subscribe(Observable<?> observable, Action1<?> successAction, Action1<Throwable> failAction);

    Subscription subscribe(Observable<?> observable, Action1<?> successAction);

    void clear();
}
