package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseResponse {

    @JsonProperty("status")
    private String mStatus;

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }
}
