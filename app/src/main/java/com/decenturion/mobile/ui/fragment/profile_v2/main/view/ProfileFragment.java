package com.decenturion.mobile.ui.fragment.profile_v2.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.profile.ProfileComponent;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.dialog.bottomsheet.MoreActionDialogFragment;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.collaps.ProfileCollapsAppBarFragment;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PhotoModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.ProfileAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.main.model.ProfileModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.main.presenter.IProfilePresenter;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;

public class ProfileFragment extends BaseFragment implements IProfileView,
        MoreActionDialogFragment.OnMoreActionDialogListener {

    /* UI */

    @BindView(R.id.pagerView)
    ViewPager mPagerView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IProfilePresenter mIProfilePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        ProfileComponent profileComponent = appDelegate
                .getIDIManager()
                .plusProfileComponent();
        profileComponent.inject(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fr_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        FragmentManager fragmentManager = requireFragmentManager();
        String tag = MoreActionDialogFragment.class.getSimpleName();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            ((MoreActionDialogFragment) fragment).setOnMoreActionDialogListener(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_more : {
                MoreActionDialogFragment dialog = new MoreActionDialogFragment();
                dialog.setOnMoreActionDialogListener(this);
                FragmentManager fragmentManager = requireFragmentManager();
                dialog.show(fragmentManager, MoreActionDialogFragment.class.getSimpleName());
                return true;
            }
            default : {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_profile_v2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        FragmentManager fm = getChildFragmentManager();
        ProfileAdapter adapter = new ProfileAdapter(fm, mILocalistionManager);
        mPagerView.setAdapter(adapter);
    }

    private void initToolbar() {
        IMainActivity activity = (IMainActivity) requireActivity();
        TabLayout tabLayoutView = activity.getTabLayoutView();
        tabLayoutView.setVisibility(View.VISIBLE);
        tabLayoutView.setupWithViewPager(mPagerView);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIProfilePresenter.bindView(this);
        mIProfilePresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIProfilePresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIProfilePresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIProfilePresenter.unbindView();
    }

    @Override
    public void onCopyWalletAddress() {
        mIProfilePresenter.onCopyWalletAddress();
    }

    @Override
    public void onCopyProfileLink() {
        mIProfilePresenter.onCopyProfileLink();
    }

    @Override
    public void onCopyReferalLink() {
        mIProfilePresenter.onCopyReferalLinc();
    }

    @Override
    public void onShare() {
        mIProfilePresenter.onShare();
    }

    @Override
    public void onShare(@NonNull String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }

    @Override
    public void onSignout() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, SignActivity.class);
        startActivity(intent);
        activity.finish();
    }

    @Override
    public void onBindViewData(@NonNull ProfileModelView model) {
        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setSubTitle(model.getStatus());

        initPhotoUi(model);
    }

    private void initPhotoUi(@NonNull ProfileModelView model) {
        ProfileCollapsAppBarFragment fragment = new ProfileCollapsAppBarFragment();

        Bundle bundle = new Bundle();
        PhotoModelView photoModelView = model.getPhotoModelView();
        if (photoModelView == null) {
            bundle.putString(ProfileCollapsAppBarFragment.URL_PHOTO, "");
            bundle.putString(ProfileCollapsAppBarFragment.URL_MIN_PHOTO, "");
        } else {
            bundle.putString(ProfileCollapsAppBarFragment.URL_PHOTO, photoModelView.getOriginal());
            bundle.putString(ProfileCollapsAppBarFragment.URL_MIN_PHOTO, photoModelView.getMin());
        }
        fragment.setArguments(bundle);

        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setCollapseContentView(fragment, "ProfileCollapsAppBarFragment");
    }
}
