package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralHistoryItem {

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("type")
    private String mType;

    public ReferralHistoryItem() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }

    @JsonIgnore
    public String getType() {
        return mType;
    }
}
