package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Country implements ISpinnerItem {

    @JsonProperty("name")
    private String mName;

    @JsonProperty("code")
    private String mCode;

    @JsonProperty("country-code")
    private String mCountryCode;

    public Country() {}

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public String getCode() {
        return mCode;
    }

    @JsonIgnore
    public String getCountryCode() {
        return mCountryCode;
    }

    @Override
    public String getItemName() {
        return mName;
    }

    @Override
    public String getItemKey() {
        return mCode;
    }
}
