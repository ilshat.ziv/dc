package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class PasswordView extends EditTextView {

    public PasswordView(Context context) {
        super(context);
    }

    public PasswordView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PasswordView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}