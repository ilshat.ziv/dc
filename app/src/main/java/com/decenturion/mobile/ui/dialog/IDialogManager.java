package com.decenturion.mobile.ui.dialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public interface IDialogManager<D> {

    void showDialog(Class<D> clazz);

    void showDialog(Class<D> clazz, Bundle bundle);

    void showDialog(Class<D> clazz, Bundle bundle, Fragment target, int requestCode);

    void hideDialog(Class<D> clazz);

    void onDestroy();
}
