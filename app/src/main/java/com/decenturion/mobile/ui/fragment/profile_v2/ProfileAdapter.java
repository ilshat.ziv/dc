package com.decenturion.mobile.ui.fragment.profile_v2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.ui.fragment.profile_v2.log.view.LogsFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.passport.view.PassportFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.view.ScoreListFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.view.DealListFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.view.TransactionListFragment;

public class ProfileAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private SparseArray<Fragment> mHashMap;

    private ILocalistionManager mILocalistionManager;

    public ProfileAdapter(FragmentManager fm, ILocalistionManager iLocalistionManager) {
        super(fm);
        this.mNumOfTabs = 5;
        mHashMap = new SparseArray<>();
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Fragment getItem(int position) {
        if (mHashMap.get(position) != null) {
            return mHashMap.get(position);
        }
        Fragment fragment = null;
        switch (position) {
            case 0: {
                fragment = new PassportFragment();
                break;
            }
            case 1: {
                fragment = new ScoreListFragment();
                break;
            }
            case 2: {
                fragment = new DealListFragment();
                break;
            }
            case 3: {
                fragment = new TransactionListFragment();
                break;
            }
            case 4 : {
                fragment = new LogsFragment();
                break;
            }
            default: {
                return null;
            }
        }
        mHashMap.put(position, fragment);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_passport);
            }
            case 1: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_myscores);
            }
            case 2: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_deals);
            }
            case 3: {
                return mILocalistionManager.getLocaleString(R.string.app_account_tabbar_transactions);//"История транзакций";
            }
            case 4 : {
                return mILocalistionManager.getLocaleString(R.string.app_settings_actionlog);
            }
        }
        throw new RuntimeException();
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}