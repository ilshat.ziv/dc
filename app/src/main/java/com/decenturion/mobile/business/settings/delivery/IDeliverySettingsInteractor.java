package com.decenturion.mobile.business.settings.delivery;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.DeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.main.model.PaymentModelView;

import rx.Observable;

public interface IDeliverySettingsInteractor {

    Observable<DeliverySettingsModelView> getPassportInfo();

    Observable<UpdatePassportResponse> saveChanges(@NonNull String firstName,
                                                   @NonNull String secondName,
                                                   long birth,
                                                   @NonNull String sex,
                                                   @NonNull String country,
                                                   @NonNull String city);

    Observable<DeliverySettingsModelView> setDeliveryData(@NonNull String phone,
                                                       @NonNull String address,
                                                       @NonNull String city,
                                                       @NonNull String state,
                                                       @NonNull String country,
                                                       @NonNull String zipcode);

    Observable<PaymentModelView> getPaymentAddress(@NonNull String coin);
}
