package com.decenturion.mobile.ui.fragment.token.seller.details.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.seller.details.model.SellerTokenDetailsModelView;

public interface ISellerTokenDetailsView extends IScreenFragmentView {

    void onBindViewData(@NonNull SellerTokenDetailsModelView model);
}
