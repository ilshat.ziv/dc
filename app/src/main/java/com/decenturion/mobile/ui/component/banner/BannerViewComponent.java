package com.decenturion.mobile.ui.component.banner;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.component.SavedState;
import com.decenturion.mobile.ui.widget.LinkTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BannerViewComponent extends LinearLayout implements IBannerViewComponent {

    @Nullable
    @BindView(R.id.referralBannerTitleView)
    TextView mReferralBannerTitleView;

    @Nullable
    @BindView(R.id.bannerPriceView)
    TextView mBannerPriceView;

    @Nullable
    @BindView(R.id.citizenPriceView)
    TextView mCitizenPriceView;

    @Nullable
    @BindView(R.id.getCitizenButtonView)
    ConstraintLayout mGetCitizenButtonView;

    @Nullable
    @BindView(R.id.bannerInfoView)
    LinkTextView mBannerInfoView;

    @Nullable
    @BindView(R.id.bannerInfo2View)
    LinkTextView mBannerInfo2View;

    private Unbinder mUnbinder;

    private OnBannerViewListener mOnBannerViewListener;

    public BannerViewComponent(Context context) {
        super(context);
        init();
    }

    public BannerViewComponent(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BannerViewComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected boolean isInitView() {
        return (mBannerPriceView != null
                && mCitizenPriceView != null
                && mGetCitizenButtonView != null
                && mBannerInfoView != null
                && mBannerInfo2View != null);
    }

    protected void init() {
        if (isInitView()) {
            return;
        }
        mUnbinder = ButterKnife.bind(this, this);

        if (isInitView()) {
            assert mGetCitizenButtonView != null;
            mGetCitizenButtonView.setOnClickListener(v -> {
                if (mOnBannerViewListener != null) {
                    mOnBannerViewListener.onGetCitizen();
                }
            });

            assert mBannerInfoView != null;
            mBannerInfoView.setOnLinkClickListener(new LinkTextView.OnLinkTextViewListener() {
                @Override
                public void onLinkClick() {
                    if (mOnBannerViewListener != null) {
                        mOnBannerViewListener.onGetCitizenInfo();
                    }
                }

                @Override
                public void onTextClick() {

                }
            });

            assert mBannerInfo2View != null;
            mBannerInfo2View.setOnLinkClickListener(new LinkTextView.OnLinkTextViewListener() {
                @Override
                public void onLinkClick() {
                    if (mOnBannerViewListener != null) {
                        mOnBannerViewListener.onGetCitizenInfo2();
                    }
                }

                @Override
                public void onTextClick() {

                }
            });
        }
    }

    public void setOnBannerViewListener(OnBannerViewListener listener) {
        mOnBannerViewListener = listener;
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.setChildrenStates(new SparseArray());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).saveHierarchyState(ss.getChildrenStates());
        }
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).restoreHierarchyState(ss.getChildrenStates());
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    @Override
    public void destroyDrawingCache() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.destroyDrawingCache();
    }


    @Override
    public void setTitle(@NonNull String title) {
        init();
        assert mReferralBannerTitleView != null;
        mReferralBannerTitleView.setText(title);
    }

    @Override
    public void setReferralPrice(@NonNull String referralPrice) {
        init();
        assert mBannerPriceView != null;
        mBannerPriceView.setText(referralPrice);
    }

    @Override
    public void setBasePrice(@NonNull String basePrice) {
        init();
        assert mCitizenPriceView != null;
        mCitizenPriceView.setText(basePrice);
    }

    @Override
    public ImageView getIconView() {
        return null;
    }

    @Override
    public void hideLabel() {

    }

    @Override
    public void showLabel() {

    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {

    }

    @Override
    public void simpleMode() {

    }

    public interface OnBannerViewListener {
        void onGetCitizen();
        void onGetCitizenInfo();
        void onGetCitizenInfo2();
    }
}
