package com.decenturion.mobile.di.dagger.settings.delivery.pay;

import android.content.Context;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.delivery.pay.IPayDeliverySettingsInteractor;
import com.decenturion.mobile.business.settings.delivery.pay.PayDeliverySettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.presenter.IPayDeliverySettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.presenter.PayDeliverySettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PayDeliverySettingsModule {

    @Provides
    @PayDeliverySettingsSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @PayDeliverySettingsSettingsScope
    IPayDeliverySettingsInteractor provideIPayDeliverySettingsInteractor(IResidentClient iResidentClient,
                                                                             IResidentRepository iResidentRepository) {
        return new PayDeliverySettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @PayDeliverySettingsSettingsScope
    IPayDeliverySettingsPresenter provideIPayDeliverySettingsPresenter(Context context,
                                                                       IPayDeliverySettingsInteractor iPayDeliverySettingsInteractor) {
        return new PayDeliverySettingsPresenter(context, iPayDeliverySettingsInteractor);
    }
}
