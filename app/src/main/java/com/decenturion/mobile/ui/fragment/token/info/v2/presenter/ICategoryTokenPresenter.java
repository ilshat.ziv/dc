package com.decenturion.mobile.ui.fragment.token.info.v2.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.info.v2.view.ICategoryTokenView;

public interface ICategoryTokenPresenter extends IPresenter<ICategoryTokenView> {

    void bindViewData(@NonNull String tokenCategory);
}
