package com.decenturion.mobile.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.dialog.DialogManager;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.floating.keybord.IKeyboardVisionManager;
import com.decenturion.mobile.ui.floating.keybord.KeyboardVisionManager;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.navigation.NavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.toolbar.ToolbarController;
import com.decenturion.mobile.ui.widget.BaseTabLayout;

import butterknife.BindView;

public abstract class SimpleActivity extends BaseActivity implements ISingleFragmentActivity {

    private IToolbarController mIToolbarController;
    private INavigationManager mINavigationManager;
    private IDialogManager mIDialogManager;
    private IKeyboardVisionManager mIKeyboardVisionManager;

    /* UI */

    @BindView(R.id.tabLayoutView)
    BaseTabLayout mTabView;

    @BindView(R.id.appBarView)
    AppBarLayout mAppBarLayoutView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main_simple);
        setupUi();
    }

    private void setupUi() {
        mIToolbarController = new ToolbarController(this, mAppBarLayoutView);
        mINavigationManager = new NavigationManager(this, R.id.contentView);
        mIDialogManager = new DialogManager(getSupportFragmentManager());
        mIKeyboardVisionManager = new KeyboardVisionManager(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mINavigationManager.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIDialogManager.onDestroy();
        mIToolbarController.onDestroy();
        mINavigationManager.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = (Fragment) mINavigationManager.getCurentFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public IToolbarController getIToolbarController() {
        return mIToolbarController;
    }

    @Override
    public INavigationManager getINavigationManager() {
        return mINavigationManager;
    }

    @Override
    public IKeyboardVisionManager getIKeyboardVisionManager() {
        return mIKeyboardVisionManager;
    }

    @Override
    public TabLayout getTabLayoutView() {
        return mTabView;
    }

    @Override
    public IDialogManager getIDialogManager() {
        return mIDialogManager;
    }
}
