package com.decenturion.mobile.ui.dialog.bottomsheet;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.utils.DisplayUtils;

import butterknife.BindView;

public abstract class BaseBottomSheetDialog extends BottomSheetDialogFragment {

    @Nullable
    @BindView(R.id.titleView)
    protected TextView mTitleView;

    protected BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    protected void setPeekHeight(View contentView) {
        // TODO: 05.08.2018 убрать этот костыль
        int[] sizeConten = DisplayUtils.getSizeView(getResources(), contentView);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            ((BottomSheetBehavior) behavior).setPeekHeight(sizeConten[1]);
        }
    }

    protected void setPeekHeight(int height, View contentView) {
        // TODO: 05.08.2018 убрать этот костыль
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            ((BottomSheetBehavior) behavior).setPeekHeight(height);
        }
    }

    protected void setTitle(String title) {
        mTitleView.setText(String.valueOf(title));
    }
}
