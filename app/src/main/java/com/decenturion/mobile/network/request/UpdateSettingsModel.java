package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateSettingsModel {

    @JsonProperty("show_name")
    private String isShowName;

    @JsonProperty("show_dob")
    private String isShowBirth;

    @JsonProperty("show_photo")
    private String isShowPhoto;

    @JsonProperty("show_tokens")
    private String isShowTokens;

    @JsonProperty("show_money")
    private String isShowMoney;

    @JsonProperty("show_power")
    private String isShowPower;

    @JsonProperty("show_glory")
    private String isShowGlory;

    public UpdateSettingsModel() {
    }

    @JsonIgnore
    public void setShowName(boolean showName) {
        this.isShowName = String.valueOf(showName);
    }

    @JsonIgnore
    public void setShowBirth(boolean showBirth) {
        this.isShowBirth = String.valueOf(showBirth);
    }

    @JsonIgnore
    public void setShowPhoto(boolean showPhoto) {
        this.isShowPhoto = String.valueOf(showPhoto);
    }

    @JsonIgnore
    public void setShowTokens(boolean showTokens) {
        this.isShowTokens = String.valueOf(showTokens);
    }

    @JsonIgnore
    public void setShowMoney(boolean showMoney) {
        this.isShowMoney = String.valueOf(showMoney);
    }

    @JsonIgnore
    public void setShowPower(boolean showPower) {
        this.isShowPower = String.valueOf(showPower);
    }

    @JsonIgnore
    public void setShowGlory(boolean showGlory) {
        this.isShowGlory = String.valueOf(showGlory);
    }
}
