package com.decenturion.mobile.ui.fragment.twofa.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.twofa.IEnableTwoFAInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.twofa.model.EnableTwoFAModelView;
import com.decenturion.mobile.ui.fragment.twofa.view.IEnableTwoFAView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class EnableTwoFAPresenter extends Presenter<IEnableTwoFAView> implements IEnableTwoFAPresenter {

    private IEnableTwoFAInteractor mIEnableTwoFAInteractor;
    private ILocalistionManager mILocalistionManager;

    private EnableTwoFAModelView mEnableTwoFAModelView;

    public EnableTwoFAPresenter(IEnableTwoFAInteractor iSecurityInteractor,
                                ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mIEnableTwoFAInteractor = iSecurityInteractor;
        mEnableTwoFAModelView = new EnableTwoFAModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mEnableTwoFAModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<EnableTwoFAModelView> obs = mIEnableTwoFAInteractor.getInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EnableTwoFAModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(EnableTwoFAModelView model) {
        mEnableTwoFAModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    @NonNull String secretString, @NonNull String secretCode) {
        bundle.putString("secretString", secretString);
        bundle.putString("secretCode", secretCode);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mEnableTwoFAModelView.setSecretString(savedInstanceState.getString("secretString", ""));
        mEnableTwoFAModelView.setSecretCode(savedInstanceState.getString("secretCode", ""));

        Observable<EnableTwoFAModelView> obs = Observable.just(mEnableTwoFAModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<EnableTwoFAModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void saveChanges(@NonNull String secretString, @NonNull String secretCode) {
        showProgressView();

        Observable<Boolean> obs = mIEnableTwoFAInteractor.saveChanges(secretString, secretCode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<Boolean>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(Boolean b) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));

        if (mView != null) {
            mView.onChangesSaved();
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                LoggerUtils.exception(new RuntimeException(error.getMessage()));

                return;
            }
        }
        showErrorView(throwable);

        LoggerUtils.exception(throwable);
    }
}
