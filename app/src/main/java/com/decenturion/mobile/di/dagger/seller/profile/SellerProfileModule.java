package com.decenturion.mobile.di.dagger.seller.profile;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.seller.profile.ISellerProfileInteractor;
import com.decenturion.mobile.business.seller.profile.SellerProfileInteractor;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.ui.fragment.seller.main.presenter.ISellerProfilePresenter;
import com.decenturion.mobile.ui.fragment.seller.main.presenter.SellerProfilePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SellerProfileModule {

    @Provides
    @SellerProfileScope
    ISellerProfileInteractor provideISellerProfileInteractor(IResidentClient iResidentClient,
                                                             ILocalistionManager ilocalisationManager) {
        return new SellerProfileInteractor(iResidentClient, ilocalisationManager);
    }

    @Provides
    @SellerProfileScope
    ISellerProfilePresenter provideISellerProfilePresenter(ISellerProfileInteractor iProfileInteractor,
                                                           Context context,
                                                           ILocalistionManager iLocalistionManager) {
        return new SellerProfilePresenter(iProfileInteractor, context, iLocalistionManager);
    }
}
