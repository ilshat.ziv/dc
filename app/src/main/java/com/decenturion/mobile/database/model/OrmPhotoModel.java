package com.decenturion.mobile.database.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.Photo;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "orm_photo_model")
public class OrmPhotoModel {

    private final static String ID = "id";
    private final static String UUID = "uuid";
    private final static String ORIGINAL = "originalImage";
    private final static String MIN = "minImage";

    @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = ID)
    private int mId;

    @DatabaseField(dataType = DataType.STRING, columnName = UUID)
    private String mUuid;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = ORIGINAL)
    private String mOriginal;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = MIN)
    private String mMin;

    public OrmPhotoModel() {
    }

    public OrmPhotoModel(@NonNull Photo photo) {
        mId = 1;
        mUuid = photo.getUuid();
        mOriginal = photo.getOriginal();
        mMin = photo.getMin();
    }

    public String getUuid() {
        return mUuid;
    }

    public String getOriginal() {
        return mOriginal;
    }

    public String getMin() {
        return mMin;
    }
}
