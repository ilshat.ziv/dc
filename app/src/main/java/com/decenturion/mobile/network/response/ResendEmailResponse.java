package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResendEmailResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean isSuccess;

    public ResendEmailResponse() {
    }

    @JsonIgnore
    public boolean isSuccess() {
        return isSuccess;
    }
}
