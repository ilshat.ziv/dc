package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnableGfaModel {

    @JsonProperty("secret")
    private String mSecret;

    @JsonProperty("code")
    private String mCode;

    public EnableGfaModel() {
    }

    @JsonIgnore
    public void setCode(@NonNull String code) {
        mCode = code;
    }

    @JsonIgnore
    public void setSecret(String secret) {
        mSecret = secret;
    }
}
