package com.decenturion.mobile.ui.toolbar;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.decenturion.mobile.ui.fragment.collaps.ProfileCollapsAppBarFragment;

public interface IToolbarController {

    void setTitle(@NonNull String title);

    void setSubTitle(@NonNull String subtitle);

    void onDestroy();

    void setNavigationOnClickListener(View.OnClickListener listener);

    void setNavigationIcon(@DrawableRes int resourceId);

    void setNavigationIcon(@Nullable Drawable icon);

    void removeNavigationButton();

    void setCollapseContentView(Fragment fragment, String tag);
}
