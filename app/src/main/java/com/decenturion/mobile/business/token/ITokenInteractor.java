package com.decenturion.mobile.business.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;

import rx.Observable;

public interface ITokenInteractor {

    Observable<TokenModelView> getTokenModelView();

    Observable<ScoreTokenListModelView> getTokenListModelView(@NonNull ScoreTokenListModelView model);
}
