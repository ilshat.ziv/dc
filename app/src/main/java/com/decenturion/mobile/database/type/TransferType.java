package com.decenturion.mobile.database.type;

import com.decenturion.mobile.database.type.model.TransferTypeModel;

public class TransferType extends BaseFieldClassPersister<TransferTypeModel> {

    private static TransferType INSTANCE;

    protected TransferType(Class<TransferTypeModel> typeFieldClass) {
        super(typeFieldClass);
    }

    public static TransferType getSingleton() {
        if (INSTANCE == null) {
            return INSTANCE = new TransferType(TransferTypeModel.class);
        }
        return INSTANCE;
    }
}