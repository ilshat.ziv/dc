package com.decenturion.mobile.ui.fragment.token.transaction.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.tokendetails.transaction.ITransactionInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.traide.cancel.CancelTraideResult;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;
import com.decenturion.mobile.ui.fragment.token.transaction.view.ITransactionView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TransactionPresenter extends Presenter<ITransactionView> implements ITransactionPresenter {

    private ITransactionInteractor mITransactionInteractor;
    private TransactionModelView mTransactionModelView;

    public TransactionPresenter(ITransactionInteractor iTransactionInteractor) {
        mITransactionInteractor = iTransactionInteractor;
        mTransactionModelView = new TransactionModelView();
    }

    @Override
    public void bindViewData(@NonNull String traideUUID) {
        showProgressView();

        mTransactionModelView.setTraideUUID(traideUUID);
        Observable<TransactionModelView> obs = mITransactionInteractor.getTokenInfo(traideUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TransactionModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(TransactionModelView model) {
        mTransactionModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    @Override
    public void confirmTraide(boolean checked,
                              String receiveAddress,
                              String refundAddress,
                              String txid) {

        Observable<ConfirmTraideResult> obs = mITransactionInteractor.confirmTraide(checked,
                receiveAddress,
                refundAddress,
                mTransactionModelView.getTraideUUID(),
                txid
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TransactionModelView>) this::confirmTraideSuccess,
                this::confirmTraideFailure
        );
    }

    @Override
    public void cancelTraide(String tradeUUID) {
        Observable<CancelTraideResult> obs = mITransactionInteractor.cancelTraide(tradeUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<CancelTraideResult>) this::cancelTraideSuccess,
                this::confirmTraideFailure
        );
    }

    private void cancelTraideSuccess(CancelTraideResult cancelTraideResult) {
        if (mView != null) {
            mView.onTransactionCanceled();
        }
    }

    private void confirmTraideFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
//                Error error = errorList.get(0);
//                showErrorView(error.getMessage());
                if (mView != null) {
                    mView.onErrorTransaction();
                }
                return;
            }
        }

        showErrorView(throwable);
    }

    private void confirmTraideSuccess(TransactionModelView transactionModelView) {
        hideProgressView();
        if (mView != null) {
            mView.onTransactionConfirmed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTransactionModelView = null;
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
