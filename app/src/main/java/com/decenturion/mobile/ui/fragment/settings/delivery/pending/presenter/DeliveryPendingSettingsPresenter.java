package com.decenturion.mobile.ui.fragment.settings.delivery.pending.presenter;

import com.decenturion.mobile.business.settings.delivery.panding.IDeliveryPendingSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.model.DeliveryPendingSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pending.view.IDeliveryPendingSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DeliveryPendingSettingsPresenter extends Presenter<IDeliveryPendingSettingsView> implements IDeliveryPendingSettingsPresenter {

    private IDeliveryPendingSettingsInteractor mIDeliveryPendingSettingsInteractor;

    private DeliveryPendingSettingsModelView mDeliveredSettingsModelView;

    public DeliveryPendingSettingsPresenter(IDeliveryPendingSettingsInteractor iDeliveryPendingSettingsInteractor) {
        mIDeliveryPendingSettingsInteractor = iDeliveryPendingSettingsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDeliveredSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<DeliveryPendingSettingsModelView> obs = mIDeliveryPendingSettingsInteractor.getPassportInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliveryPendingSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(DeliveryPendingSettingsModelView model) {
        mDeliveredSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
