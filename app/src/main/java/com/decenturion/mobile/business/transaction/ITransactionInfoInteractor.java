package com.decenturion.mobile.business.transaction;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.fragment.traide.model.TraidInfoModelView;
import com.decenturion.mobile.ui.fragment.transaction.model.TransactionInfoModelView;

import rx.Observable;

public interface ITransactionInfoInteractor {

    Observable<TransactionInfoModelView> getTransactionInfo(@NonNull String traidUUID);
}
