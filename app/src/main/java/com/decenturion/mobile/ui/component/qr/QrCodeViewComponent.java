package com.decenturion.mobile.ui.component.qr;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.SavedState;
import com.decenturion.mobile.utils.LoggerUtils;
import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.Context.WINDOW_SERVICE;

public class QrCodeViewComponent extends LinearLayout implements IQrCodeViewComponent {

    @Nullable
    @BindView(R.id.qrCodeDataView)
    LinearLayout mQrCodeDataView;

    @Nullable
    @BindView(R.id.qrCodeView)
    ImageView mQrCodeView;

    @Nullable
    @BindView(R.id.labelView)
    LocalizedTextView mLabelView;

    @Nullable
    @BindView(R.id.addressQrView)
    TextView mAddressQrView;

    @Nullable
    @BindView(R.id.dividerView)
    View mDividerView;

    private Unbinder mUnbinder;

    protected Mode mMode;
    protected ChangeComponentListener mChangeComponentListener;

    private OnQrCodeViewClickListener mOnQrCodeViewClickListener;

    public QrCodeViewComponent(Context context) {
        super(context);
        init();
    }

    public QrCodeViewComponent(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public QrCodeViewComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected boolean isInitView() {
        return (mLabelView != null && mDividerView != null
                && mAddressQrView != null && mQrCodeView != null);
    }

    protected void init() {
        if (isInitView()) {
            return;
        }
        mUnbinder = ButterKnife.bind(this, this);

        if (isInitView()) {
            assert mQrCodeDataView != null;
            mQrCodeDataView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCopyAddres();
                }
            });
        }
    }

    public void setIconView(@DrawableRes int resource) {
    }

    public void setIconView(@NonNull String url) {
    }

    @Nullable
    @Override
    public ImageView getIconView() {
        return null;
    }

    public void hideIconView() {
    }

    public void setAlphaIconView(float alpha) {
    }

    public void setLabel(String label) {
        init();
        if (label != null) {
            assert mLabelView != null;
            mLabelView.setText(String.valueOf(label));
        }
        showLabel();
    }

    @Override
    public void hideLabel() {
        init();
        assert mLabelView != null;
        mLabelView.setVisibility(GONE);
    }

    @Override
    public void showLabel() {
        init();
        ViewGroup view = (ViewGroup) mLabelView.getParent();
        view.setVisibility(VISIBLE);
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    public void setVisibleDivider(boolean isVisible) {
        init();
        if (isVisible) {
            mDividerView.setVisibility(VISIBLE);
        } else {
            mDividerView.setVisibility(GONE);
        }
    }

    @Override
    public void editMode() {
        mMode = Mode.EDIT;
    }

    @Override
    public void simpleMode() {
        mMode = Mode.SIMPLE;
    }

    public boolean isEditMode() {
        return mMode == Mode.EDIT;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();

    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.setChildrenStates(new SparseArray());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).saveHierarchyState(ss.getChildrenStates());
        }
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).restoreHierarchyState(ss.getChildrenStates());
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    @Override
    public void destroyDrawingCache() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.destroyDrawingCache();
    }

    private void onCopyAddres() {
        Context context = getContext();
        assert context != null;
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        assert mAddressQrView != null;
        ClipData clip = ClipData.newPlainText("QrCodeAddress", mAddressQrView.getText());
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            if (mOnQrCodeViewClickListener != null) {
                mOnQrCodeViewClickListener.onQrCodeDataCopied();
            }
        }
    }

    public void setChangeDataModelListener(ChangeComponentListener listener) {
        mChangeComponentListener = listener;
    }

    public void setOnQrCodeViewClickListener(OnQrCodeViewClickListener listener) {
        mOnQrCodeViewClickListener = listener;
    }

    @Override
    public void setAddress(@NonNull String address) {
        init();
        assert mAddressQrView != null;
        mAddressQrView.setText(address);
        this.setVisibility(VISIBLE);
    }

    @Override
    public void setQrData(@NonNull String string) {
        init();
        assert mQrCodeView != null;

        Context context = getContext();
        WindowManager manager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        assert manager != null;

        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 2 / 4;

        QRGEncoder qrgEncoder = new QRGEncoder(
                string,
                null,
                QRGContents.Type.TEXT,
                smallerDimension);
        try {
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();

            mQrCodeView.setImageBitmap(bitmap);

            mQrCodeView.setOnClickListener(v -> {
                if (mOnQrCodeViewClickListener != null) {
                    mOnQrCodeViewClickListener.onQrCodeViewClick(string);
                }
            });
        } catch (WriterException e) {
            LoggerUtils.exception(e);
            mQrCodeView.setOnClickListener(null);
        }

        this.setVisibility(VISIBLE);
    }

    @Override
    public String getAddress() {
        init();
        assert mAddressQrView != null;
        return mAddressQrView.getText().toString();
    }

    public interface OnQrCodeViewClickListener {

        void onQrCodeViewClick(@NonNull String qrCodeData);

        void onQrCodeDataCopied();
    }
}
