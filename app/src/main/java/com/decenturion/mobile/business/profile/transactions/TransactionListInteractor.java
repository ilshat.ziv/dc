package com.decenturion.mobile.business.profile.transactions;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.response.model.Transaction;
import com.decenturion.mobile.network.response.transactions.TransactionsResponse;
import com.decenturion.mobile.network.response.transactions.TransactionsResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.log.model.LogModel;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionModel;
import com.decenturion.mobile.utils.LoggerUtils;

import java.util.List;

import rx.Observable;

public class TransactionListInteractor implements ITransaionListInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;
    private ILocalistionManager mILocalistionManager;

    public TransactionListInteractor(IResidentClient IResidentClient,
                                     IResidentRepository iResidentRepositor,
                                     IPrefsManager iPrefsManager,
                                     ILocalistionManager iLocalistionManager) {

        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepositor;
        mIPrefsManager = iPrefsManager;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<TransactionListModelView> getTransactionsInfo() {
        return mIResidentClient.getTransations(0, 99999)
                .map(TransactionsResponse::getResult)
                .map(TransactionsResult::getTransactionList)
                .flatMapIterable(list -> list)
                .map(d -> new TransactionModel(d, mILocalistionManager))
                .toList()
                .map(TransactionListModelView::new)
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    return null;
                });
    }

    @Override
    public Observable<TransactionListModelView> getTransactionsInfo(@NonNull TransactionListModelView model) {
        Observable<TransactionListModelView> obs1 = mIResidentClient.getTransations(model.getOffset(), model.LIMIT)
                .map(TransactionsResponse::getResult)
                .flatMap(d -> {
                    List<Transaction> list = d.getTransactionList();
                    Observable<List<TransactionModel>> listObservable = Observable.just(list)
                            .flatMapIterable(list1 -> list1)
                            .map(d1 -> new TransactionModel(d1, mILocalistionManager))
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(d.getTotal()),
                            TransactionListModelView::new
                    );
                })
                .onErrorReturn(throwable -> {
                    LoggerUtils.exception(throwable);

                    if (throwable instanceof AccessException) {
                        clearDate();
                        throw new AccessException(throwable);
                    }

                    throw new RuntimeException(throwable.getMessage());
                });

        Observable<TransactionListModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (modelView, modelView2) -> {
            modelView2.setOffset(modelView2.getOffset() + modelView2.LIMIT);
            modelView2.addTransactions(modelView.getTransactionList());
            modelView2.setTotal(modelView.getTotal());
            return modelView2;
        });
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
