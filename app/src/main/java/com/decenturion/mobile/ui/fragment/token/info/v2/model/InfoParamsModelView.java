package com.decenturion.mobile.ui.fragment.token.info.v2.model;

public class InfoParamsModelView {

    private String mName;
    private boolean isActive;

    public InfoParamsModelView(String name) {
        mName = name;
    }

    public InfoParamsModelView(String name, boolean b) {
        this(name);
        this.isActive = b;
    }

    public String getName() {
        return mName;
    }

    public boolean isActive() {
        return isActive;
    }
}
