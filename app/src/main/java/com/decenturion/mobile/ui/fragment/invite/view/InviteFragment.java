package com.decenturion.mobile.ui.fragment.invite.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.invite.InviteComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.invite.InviteFailFragment;
import com.decenturion.mobile.ui.fragment.invite.InviteSuccessFragment;
import com.decenturion.mobile.ui.fragment.invite.model.InviteModelView;
import com.decenturion.mobile.ui.fragment.invite.model.Token;
import com.decenturion.mobile.ui.fragment.invite.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.invite.presenter.IInvitePresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class InviteFragment extends BaseFragment implements IInviteView
        , View.OnClickListener {

    /* UI */

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.emailView)
    EditTextView mEmailView;

    @BindView(R.id.tokenView)
    SpinnerView mTokenView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    /* DI */

    @Inject
    IInvitePresenter mIInvitePresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        InviteComponent inviteComponent= appDelegate
                .getIDIManager()
                .plusInviteComponent();
        inviteComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_invite, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mEmailView.showLabel();
        mEmailView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_share_invite_email_title));
        mEmailView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_share_invite_email_placeholder));
        mEmailView.setTypeInput(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mEmailView.editMode();
        mEmailView.setVisibleDivider(true);

        mTokenView.showLabel();
        mTokenView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_share_invite_token_title));
        mTokenView.setVisibleDivider(true);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_share_invite_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIInvitePresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIInvitePresenter.bindViewData();
        }
    }

    @Override
    public void onBindViewData(InviteModelView model, TokenModelView model1) {
        mEmailView.setText(model.getInviteEmail());

        ArrayList<Token> tokenData = model1.getTokenData();
        Token token = tokenData.get(0);
        mTokenView.setData(
                mILocalistionManager.getLocaleString(R.string.app_account_share_invite_token_placeholder),
                tokenData
        );
        mTokenView.selectItem(token.getItemKey());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIInvitePresenter.unbindView();
        mIInvitePresenter.onSaveInstanceState(outState,
                mEmailView.getValue(),
                mTokenView.getSelection()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        mIInvitePresenter.onViewStateRestored(savedInstanceState);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onInvitationSend() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(InviteSuccessFragment.class);
    }

    @Override
    public void onInvitationSendFail() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(InviteFailFragment.class);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.inviteButtonView)
    protected void onInvite() {
        mIInvitePresenter.inviteUser(mEmailView.getValue(), mTokenView.getSelection());
    }
}
