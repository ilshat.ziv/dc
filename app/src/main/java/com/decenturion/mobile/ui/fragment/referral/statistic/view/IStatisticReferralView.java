package com.decenturion.mobile.ui.fragment.referral.statistic.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.statistic.model.StatisticReferralModelView;

public interface IStatisticReferralView extends IScreenFragmentView {

    void onBindViewData(@NonNull StatisticReferralModelView model);

}
