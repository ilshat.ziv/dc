package com.decenturion.mobile.ui.fragment.token.seller.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.fragment.token.seller.details.model.TokenParamsModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenParamsAdapter extends RecyclerView.Adapter<TokenParamsAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<TokenParamsModelView> mTokenParamsList;

    private OnItemClickListener mOnItemClickListener;

    private TokenParamsAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public TokenParamsAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<TokenParamsModelView> list) {
        mTokenParamsList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTokenParamsList);
    }

    @NonNull
    @Override
    public TokenParamsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_token_param_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TokenParamsAdapter.ViewHolder holder, int position) {
        final TokenParamsModelView param = mTokenParamsList.get(position);
        holder.bind(param);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final View view;

        @BindView(R.id.valueView)
        TextView valueView;

        @BindView(R.id.labelView)
        LocalizedTextView labelView;

        @BindView(R.id.iconView)
        ImageView iconView;

        TokenParamsModelView tokenParam;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void bind(@NonNull TokenParamsModelView param) {
            this.tokenParam = param;

            this.valueView.setText(param.getValue());
            this.labelView.setText(param.getName());
            this.iconView.setImageResource(param.getIconRes());
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(this.tokenParam);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(TokenParamsModelView model);
    }

}
