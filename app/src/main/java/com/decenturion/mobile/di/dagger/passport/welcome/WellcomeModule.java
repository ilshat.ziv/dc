package com.decenturion.mobile.di.dagger.passport.welcome;


import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.passport.wellcome.IWellcomPassportInteractor;
import com.decenturion.mobile.business.passport.wellcome.WellcomePassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.wellcome.presenter.IWellcomePresenter;
import com.decenturion.mobile.ui.fragment.passport.wellcome.presenter.WellcomePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class WellcomeModule {

    @Provides
    @WellcomeScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @WellcomeScope
    IWellcomPassportInteractor provideIWellcomPassportInteractor(IResidentClient iResidentClient,
                                                                 IResidentRepository iResidentRepository,
                                                                 IPrefsManager iPrefsManager) {
        return new WellcomePassportInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @WellcomeScope
    IWellcomePresenter provideIWellcomePresenter(IWellcomPassportInteractor iPassportInteractor) {
        return new WellcomePresenter(iPassportInteractor);
    }
}
