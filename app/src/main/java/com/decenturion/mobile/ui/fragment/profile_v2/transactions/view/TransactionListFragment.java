package com.decenturion.mobile.ui.fragment.profile_v2.transactions.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.di.dagger.profile.transactions.TransactionListComponent;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.TransactionAdapter;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionModel;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.presenter.ITransactionListPresenter;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;

public class TransactionListFragment extends BaseFragment implements ITransactionListView,
        TransactionAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.transactionListView)
    BaseRecyclerView mTransactionListView;
    private TransactionAdapter mTransactionAdapter;

    /* DI */

    @Inject
    ITransactionListPresenter mITransactionListPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TransactionListComponent transactionListComponent = appDelegate
                .getIDIManager()
                .plusTransactionListComponent();
        transactionListComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_transaction_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mRefreshView.setOnRefreshListener(this);
        mTransactionListView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mTransactionListView.setLayoutManager(new LinearLayoutManager(context));
        mTransactionAdapter = new TransactionAdapter(context, this);
        mTransactionListView.setAdapter(mTransactionAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITransactionListPresenter.bindView(this);
        mITransactionListPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mITransactionListPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mITransactionListPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull TransactionListModelView model) {
        mTransactionAdapter.replaceDataSet(model.getTransactionList());
    }

    @Override
    public void onSignout() {
//        FragmentActivity activity = getActivity();
//        assert activity != null;
//        Intent intent = new Intent(activity, SignActivity.class);
//        startActivity(intent);
//        activity.finish();
    }

    @Override
    public void onItemClick(TransactionModel model) {
//        Intent intent = new Intent(getActivity(), TransactionActivity.class);
//        intent.putExtra(TransactionActivity.TRANSACTIONS_ID, model.getTraidId());
//        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mITransactionListPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mITransactionListPresenter.bindViewData();
    }

    @Override
    public void onBottomScrolled() {
        mITransactionListPresenter.onBottomScrolled();
    }
}
