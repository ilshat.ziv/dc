package com.decenturion.mobile.ui.fragment.score.send.trade.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.score.send.trade.model.TradeScoreSendTokenModelView;

public interface ITradeScoreSendTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull TradeScoreSendTokenModelView model);

    void onEnableTwoFA();

    void onSendTokenSuccess();

    void onSendTokenFail();
}
