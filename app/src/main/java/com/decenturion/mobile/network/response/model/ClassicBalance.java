package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClassicBalance {

    @JsonProperty("total")
    private String mTotal;

    @JsonProperty("available")
    private String mAvailable;

    @JsonProperty("hold")
    private String mHold;

    public ClassicBalance() {
    }

    @JsonIgnore
    public String getTotal() {
        return mTotal;
    }

    @JsonIgnore
    public String getAvailable() {
        return mAvailable;
    }

    @JsonIgnore
    public String getHold() {
        return mHold;
    }
}
