package com.decenturion.mobile.ui.fragment.seller.main.presenter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.seller.profile.ISellerProfileInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.seller.main.model.SellerProfileModelView;
import com.decenturion.mobile.ui.fragment.seller.main.view.ISellerProfileView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SellerProfilePresenter extends Presenter<ISellerProfileView> implements ISellerProfilePresenter {

    private Context mContext;
    private ISellerProfileInteractor mISellerProfileInteractor;
    private ILocalistionManager mILocalistionManager;

    private SellerProfileModelView mProfileModelView;

    public SellerProfilePresenter(ISellerProfileInteractor iProfileInteractor, Context context,
                                  ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mContext = context;
        mISellerProfileInteractor = iProfileInteractor;
    }

    @Override
    public void bindViewData(@NonNull String mUUID) {
        showProgressView();

        Observable<SellerProfileModelView> obs = mISellerProfileInteractor.getProfileInfo(mUUID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerProfileModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SellerProfileModelView model) {
        mProfileModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void onCopyProfileLink(@NonNull String residentUuid) {
        Observable<SellerProfileModelView> obs = getProfileSellerObs(residentUuid);

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerProfileModelView>) this::copyProfileLinkSuccess,
                this::copyProfileLinkFailure
        );
    }

    private void copyProfileLinkFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void copyProfileLinkSuccess(SellerProfileModelView profileModelView) {
        mProfileModelView = profileModelView;

        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        String url = BuildConfig.HOST + "/" + profileModelView.getUID();
        ClipData clip = ClipData.newPlainText("DecenturionUserLink", url);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }

    @Override
    public void onShare(@NonNull String residentUuid) {
        Observable<SellerProfileModelView> obs = getProfileSellerObs(residentUuid);

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SellerProfileModelView>) this::getShareDataSuccess,
                this::getShareDataFailure
        );
    }

    private void getShareDataFailure(Throwable throwable) {
        showErrorView(throwable);
    }

    private void getShareDataSuccess(SellerProfileModelView profileModelView) {
        if (mView != null) {
            String url = BuildConfig.HOST + "/" + profileModelView.getUID();
            mView.onShare(url);
        }
    }

    private Observable<SellerProfileModelView> getProfileSellerObs(@NonNull String residentUuid) {
        return Observable.concat(
                Observable.just(mProfileModelView),
                mISellerProfileInteractor.getProfileInfo(residentUuid)
        )
                .takeFirst(d -> d != null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
