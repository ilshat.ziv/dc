package com.decenturion.mobile.ui.fragment.traide.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.traide.view.ITraidInfoView;

public interface ITraidInfoPresenter extends IPresenter<ITraidInfoView> {

    void bindViewData(@NonNull String traidUUID);
}
