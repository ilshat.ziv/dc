package com.decenturion.mobile.network.http.exception;

public class NotModifyException extends RuntimeException {

    public NotModifyException() {
        this("Not modify exception 304");
    }

    public NotModifyException(Throwable throwable) {
        this(throwable.getMessage());
    }

    public NotModifyException(String message) {
        super(message);
    }
}
