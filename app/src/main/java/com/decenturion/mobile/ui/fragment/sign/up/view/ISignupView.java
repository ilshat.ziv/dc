package com.decenturion.mobile.ui.fragment.sign.up.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.sign.up.model.SignupModelView;

public interface ISignupView extends IScreenFragmentView {

    void onBindViewData(@NonNull SignupModelView model);

    void onSignupSuccess(@NonNull SignupModelView model, int step);

    void onResendEmail(@NonNull SignupModelView model);
}
