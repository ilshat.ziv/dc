package com.decenturion.mobile.network.response.score;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FutureScoreTokenListResponse extends BaseResponse {

    @JsonProperty("result")
    private FutureScoreTokenListResult mResult;

    public FutureScoreTokenListResponse() {
    }

    @JsonIgnore
    public FutureScoreTokenListResult getResult() {
        return mResult;
    }
}
