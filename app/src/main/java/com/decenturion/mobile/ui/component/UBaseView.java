package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class UBaseView extends LinearLayout implements Component {

    @Nullable
    @BindView(R.id.labelView)
    protected LocalizedTextView mLabelView;

    @Nullable
    @BindView(R.id.iconView)
    protected ImageView mIconView;

    @Nullable
    @BindView(R.id.dividerView)
    protected View mDividerView;

    private Unbinder mUnbinder;

    protected Mode mMode;
    protected ChangeComponentListener mChangeComponentListener;

    public UBaseView(Context context) {
        super(context);
        init();
    }

    public UBaseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UBaseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected boolean isInitView() {
        return (mLabelView != null && mDividerView != null && mIconView != null);
    }

    protected void init() {
        if (isInitView()) {
            return;
        }
        mUnbinder = ButterKnife.bind(this, this);
    }

    public void setIconView(@DrawableRes int resource) {
        init();
        assert mIconView != null;
        mIconView.setImageResource(resource);
        mIconView.setVisibility(VISIBLE);
    }

    public void setIconView(@NonNull String url) {
        init();
        assert mIconView != null;
        Picasso.with(getContext())
                .load(url)
                .resize(192, 192)
                .onlyScaleDown()
                .into(mIconView);
        mIconView.setVisibility(VISIBLE);
    }

    @Nullable
    @Override
    public ImageView getIconView() {
        return mIconView;
    }

    public void hideIconView() {
        init();
        assert mIconView != null;
        mIconView.setVisibility(GONE);
    }

    public void setAlphaIconView(float alpha) {
        assert mIconView != null;
        mIconView.setAlpha(alpha);
    }

    public void setLabel(String label) {
        init();
        if (label != null) {
            assert mLabelView != null;
            mLabelView.setText(String.valueOf(label));
        }
        showLabel();
    }

    @Override
    public void hideLabel() {
        init();
        ViewGroup view = (ViewGroup) mLabelView.getParent();
        view.setVisibility(GONE);
    }

    @Override
    public void showLabel() {
        init();
        ViewGroup view = (ViewGroup) mLabelView.getParent();
        view.setVisibility(VISIBLE);
    }

    public void setVisibleDivider(boolean isVisible) {
        init();
        if (isVisible) {
            mDividerView.setVisibility(VISIBLE);
        } else {
            mDividerView.setVisibility(GONE);
        }
    }

    @Override
    public void editMode() {
        mMode = Mode.EDIT;
    }

    @Override
    public void simpleMode() {
        mMode = Mode.SIMPLE;
    }

    public boolean isEditMode() {
        return mMode == Mode.EDIT;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.setChildrenStates(new SparseArray());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).saveHierarchyState(ss.getChildrenStates());
        }
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).restoreHierarchyState(ss.getChildrenStates());
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    @Override
    public void destroyDrawingCache() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.destroyDrawingCache();
    }

    public void setChangeDataModelListener(ChangeComponentListener listener) {
        mChangeComponentListener = listener;
    }
}
