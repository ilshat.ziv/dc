package com.decenturion.mobile.network.response.transactions;

import com.decenturion.mobile.network.response.model.Transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsResult {

    @JsonProperty("total")
    private int mTotal;

    @JsonProperty("list")
    private List<Transaction> mTransactionList;

    public TransactionsResult() {
    }

    @JsonIgnore
    public List<Transaction> getTransactionList() {
        return mTransactionList;
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }
}
