package com.decenturion.mobile.ui.fragment.score.send.main.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendToModelView;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendTokenModelView;

public interface IMainScoreSendTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull MainScoreSendTokenModelView model, @NonNull MainScoreSendToModelView model1);

    void onEnableTwoFA();

    void onSendTokenSuccess();

    void onSendTokenFail();
}
