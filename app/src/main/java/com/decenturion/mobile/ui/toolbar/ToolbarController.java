package com.decenturion.mobile.ui.toolbar;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;
import butterknife.Unbinder;

public class ToolbarController implements IToolbarController {

    private AppCompatActivity mFragmentActivity;

    /* UI */

    @BindView(R.id.labelView)
    TextView mLabelIconView;

    @BindView(R.id.iconView)
    ImageView mIconView;

    @BindView(R.id.loginView)
    LinearLayout mLoginView;

    @BindView(R.id.titleView)
    TextView mTitleView;

    private AppBarLayout mAppBarLayoutView;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private TextView mCollapsingSubTitleView;

    private Toolbar mToolbar;

    private Unbinder mUnbinder;

    public ToolbarController(@NonNull AppCompatActivity activity, @NonNull Toolbar toolbar) {
        mFragmentActivity = activity;
        mToolbar = toolbar;
        activity.setSupportActionBar(toolbar);

        bindView();
        initUI();
    }

    public ToolbarController(@NonNull AppCompatActivity activity,
                             @NonNull AppBarLayout appBarLayoutView) {

        mFragmentActivity = activity;
        mAppBarLayoutView = appBarLayoutView;
        mToolbar = mAppBarLayoutView.findViewById(R.id.toolbarView);
        activity.setSupportActionBar(mToolbar);

        mCollapsingToolbarLayout = mAppBarLayoutView.findViewById(R.id.collapsedToolbarLayoutView);
        mCollapsingSubTitleView = mCollapsingToolbarLayout.findViewById(R.id.subTitle);

        bindView();
        initUI();
    }

    private void initUI() {
        if (mCollapsingToolbarLayout != null) {
            AssetManager assets = mFragmentActivity.getAssets();
            Typeface typeface = Typeface.createFromAsset(assets, "font/Canada Type - VoxRound-Bold.otf");
            mCollapsingToolbarLayout.setCollapsedTitleTypeface(typeface);
            mCollapsingToolbarLayout.setExpandedTitleTypeface(typeface);
            mAppBarLayoutView.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    // Collapsed
                    mCollapsingToolbarLayout.setTitleEnabled(false);
                } else {
                    // Expanded
                    mCollapsingToolbarLayout.setTitleEnabled(true);
                }
            });
        }
    }

    @Override
    public void setTitle(@NonNull String title) {
        if (mCollapsingToolbarLayout != null) {
            mCollapsingToolbarLayout.setTitleEnabled(false);
            mCollapsingToolbarLayout.setTitle(title);
        }

        ActionBar actionBar = mFragmentActivity.getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(title);
    }

    @Override
    public void setSubTitle(@NonNull String subtitle) {
        if (mCollapsingToolbarLayout != null) {
            mCollapsingToolbarLayout.setTitleEnabled(false);
            mCollapsingSubTitleView.setText(subtitle);
        }

        ActionBar actionBar = mFragmentActivity.getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle(subtitle);
    }

    @Override
    public void onDestroy() {
        unBindView();
        mFragmentActivity = null;
    }

    @Override
    public void setNavigationOnClickListener(View.OnClickListener listener) {
        mToolbar.setNavigationOnClickListener(listener);
    }

    @Override
    public void setNavigationIcon(int resourceId) {
        mToolbar.setNavigationIcon(resourceId);
    }

    @Override
    public void setNavigationIcon(@Nullable Drawable icon) {
        mToolbar.setNavigationIcon(icon);
    }

    @Override
    public void removeNavigationButton() {
        mToolbar.setNavigationOnClickListener(null);
        mToolbar.setNavigationIcon(null);
    }

    @Override
    public void setCollapseContentView(Fragment fragment, String tag) {
        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.collapsedContentView, fragment, tag);
        // TODO: 29.08.2018 Can not perform this action after onSaveInstanceState
        fragmentTransaction.commit();
    }

    private void bindView() {
//        mUnbinder = ButterKnife.bind(this, mToolbar);
    }

    private void unBindView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }
}
