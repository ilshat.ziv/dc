package com.decenturion.mobile.ui.fragment.referral.invite.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.model.Invite;
import com.decenturion.mobile.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

public class InviteModel {

    private int mId;
    private String mDate;
    private String mUrl;
    private String mPrice;

    public InviteModel(@NonNull Invite d, ILocalistionManager iLocalistionManager) {
        mId = d.getId();
        String createdAt = d.getCreatedAt();
        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            String s = iLocalistionManager.getLocaleString(R.string.app_constants_today);
            mDate = s + " " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }

        mUrl = d.getUrl();
        mPrice = d.getPrice() + " $";
    }

    public int getId() {
        return mId;
    }

    public String getDate() {
        return mDate;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getPrice() {
        return mPrice;
    }
}
