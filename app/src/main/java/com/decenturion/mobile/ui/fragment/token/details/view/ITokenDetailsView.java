package com.decenturion.mobile.ui.fragment.token.details.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.details.model.TokenDetailsModelView;

public interface ITokenDetailsView extends IScreenFragmentView {

    void onBindViewData(@NonNull TokenDetailsModelView model);
}
