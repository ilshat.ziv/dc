package com.decenturion.mobile.ui.fragment.profile_v2.transactions;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.model.TransactionModel;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<TransactionModel> mTransactionModelList;

    private OnItemClickListener mOnItemClickListener;

    public TransactionAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public TransactionAdapter(@NonNull Context context, OnItemClickListener listener) {
        this(context);
        mOnItemClickListener = listener;
    }

    public void replaceDataSet(List<TransactionModel> tokenModelList) {
        mTransactionModelList = tokenModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mTransactionModelList);
    }

    @NonNull
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_traid_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.ViewHolder holder, int position) {
        final TransactionModel model = mTransactionModelList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dateTraidView)
        TextView mDateTraidView;

        @BindView(R.id.idTraidView)
        TextView mIdTraidView;

        @BindView(R.id.amountTraidView)
        TextView mAmountTraidView;

        @BindView(R.id.iconStateTraidView)
        ImageView mIconStateTraidView;

        public final View view;
        public TransactionModel startup;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        public void bind(@NonNull TransactionModel model) {
            this.startup = model;

//            String traidId = model.getTraidId();
//            String t = TextUtils.substring(traidId, 0, 6);
//            String t1 = TextUtils.substring(traidId, traidId.length() - 5, traidId.length());
//            this.mIdTraidView.setText(t + "..." + t1);
            this.mIdTraidView.setText(model.getCategory());

            this.mDateTraidView.setText(model.getTraidDate());
            String traidAmount = model.getTraidAmount();
            this.mAmountTraidView.setText(traidAmount + " " + model.getCoin());

            String traidStatus = model.getTraidStatus();
            if (traidAmount.contains("-")) {
                traidStatus = "fail";
            } else {
                traidStatus = "success";
            }

            switch (traidStatus) {
                case "success" : {
                    this.mAmountTraidView.setTextColor(Color.GREEN);
                    break;
                }
                case "fail" : {
                    this.mAmountTraidView.setTextColor(Color.RED);
                    break;
                }
                case "fail-success" : {
                    this.mAmountTraidView.setTextColor(Color.GREEN);
                    break;
                }
                case "done" : {
                    this.mAmountTraidView.setTextColor(Color.WHITE);
                    break;
                }
            }

            switch (traidStatus) {
                case "success" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_succes);
                    break;
                }
                case "fail" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_fail);
                    break;
                }
                case "fail-success" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traide_state_fail_success);
                    break;
                }
                case "done" : {
                    mIconStateTraidView.setImageResource(R.drawable.bg_traid_state_done);
                    break;
                }
            }
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(this.startup);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(TransactionModel model);
    }
}
