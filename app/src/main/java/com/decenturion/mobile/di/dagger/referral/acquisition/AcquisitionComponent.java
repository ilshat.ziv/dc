package com.decenturion.mobile.di.dagger.referral.acquisition;

import com.decenturion.mobile.ui.fragment.referral.acquisition.view.AcquisitionFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        AcquisitionModule.class
})

@AcquisitionScope
public interface AcquisitionComponent {

    void inject(AcquisitionFragment view);
}
