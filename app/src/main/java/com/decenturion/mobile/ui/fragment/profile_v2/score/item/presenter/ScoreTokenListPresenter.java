package com.decenturion.mobile.ui.fragment.profile_v2.score.item.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.score.IScoreTokenListInteractor;
import com.decenturion.mobile.business.token.ITokenInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.view.IScoreTokenListView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.list.ScoreType;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ScoreTokenListPresenter extends Presenter<IScoreTokenListView> implements IScoreTokenListPresenter {

    private IScoreTokenListInteractor mIScoreTokenListInteractor;
    private ScoreTokenListModelView mTokenListModelView;

    public ScoreTokenListPresenter(IScoreTokenListInteractor iScoreTokenListInteractor) {
        mIScoreTokenListInteractor = iScoreTokenListInteractor;
        mTokenListModelView = new ScoreTokenListModelView();
    }

    @Override
    public void bindViewData(ScoreType type) {
        mTokenListModelView = new ScoreTokenListModelView(type);
        showProgressView();

        Observable<ScoreTokenListModelView> obs = mIScoreTokenListInteractor.getTokenListModelView(mTokenListModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ScoreTokenListModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(@NonNull ScoreTokenListModelView model) {
        mTokenListModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }
}
