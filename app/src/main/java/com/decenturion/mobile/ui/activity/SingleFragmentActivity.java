package com.decenturion.mobile.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.PrefsManager;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.app.resource.ResourceManager;
import com.decenturion.mobile.ui.dialog.DialogManager;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.floating.keybord.IKeyboardVisionManager;
import com.decenturion.mobile.ui.floating.keybord.KeyboardVisionManager;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.navigation.NavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.toolbar.ToolbarController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class SingleFragmentActivity extends BaseActivity implements ISingleFragmentActivity {

    private IToolbarController mIToolbarController;
    private INavigationManager mINavigationManager;
    private IDialogManager mIDialogManager;
    private IKeyboardVisionManager mIKeyboardVisionManager;

    /* DI */

    IResourceManager mIResourceManager;
    protected IPrefsManager mIPrefsManager;

    /* UI */

    @BindView(R.id.toolbarView)
    Toolbar mToolbar;

    private Unbinder mUnbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main_single);

        mUnbinder = ButterKnife.bind(this);

        mIResourceManager = new ResourceManager(getApplicationContext());
        mIPrefsManager = new PrefsManager(getApplicationContext());
        mIKeyboardVisionManager = new KeyboardVisionManager(this);

        setSupportActionBar(mToolbar);
        setupUi();
    }

    private void setupUi() {
        mIToolbarController = new ToolbarController(this, mToolbar);
        mINavigationManager = new NavigationManager(this, R.id.contentView);
        mIDialogManager = new DialogManager(getSupportFragmentManager());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mINavigationManager.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        mIDialogManager.onDestroy();
        mIToolbarController.onDestroy();
        mINavigationManager.onDestroy();
    }

    @Override
    public IToolbarController getIToolbarController() {
        return mIToolbarController;
    }

    @Override
    public INavigationManager getINavigationManager() {
        return mINavigationManager;
    }

    @Override
    public IKeyboardVisionManager getIKeyboardVisionManager() {
        return mIKeyboardVisionManager;
    }

    @Override
    public IDialogManager getIDialogManager() {
        return mIDialogManager;
    }

    @Override
    public void showMessage(@NonNull String message) {
    }

    @Override
    public void showSuccessMessage(@NonNull String message) {

    }

    @Override
    public void showErrorMessage(@NonNull String message) {
    }

    @Override
    public void showWarningMessage(@NonNull String message) {
    }

    @Override
    public void showProgressView() {
    }

    @Override
    public void hideProgressView() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = (Fragment) mINavigationManager.getCurentFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public TabLayout getTabLayoutView() {
        return null;
    }
}