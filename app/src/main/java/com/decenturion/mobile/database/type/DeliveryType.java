package com.decenturion.mobile.database.type;

import com.decenturion.mobile.database.type.model.DeliverTypeModel;

public class DeliveryType extends BaseFieldClassPersister<DeliverTypeModel> {

    private static DeliveryType INSTANCE;

    protected DeliveryType(Class<DeliverTypeModel> typeFieldClass) {
        super(typeFieldClass);
    }

    public static DeliveryType getSingleton() {
        if (INSTANCE == null) {
            return INSTANCE = new DeliveryType(DeliverTypeModel.class);
        }
        return INSTANCE;
    }
}