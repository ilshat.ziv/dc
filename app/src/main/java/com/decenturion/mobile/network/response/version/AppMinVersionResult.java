package com.decenturion.mobile.network.response.version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppMinVersionResult {

    @JsonProperty("platform")
    private String mPlatform;

    @JsonProperty("min_version")
    private int mMinVersion;

    public AppMinVersionResult() {
    }

    @JsonIgnore
    public String getPlatform() {
        return mPlatform;
    }

    @JsonIgnore
    public int getMinVersion() {
        return mMinVersion;
    }
}
