package com.decenturion.mobile.ui.fragment.referral.main.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.main.view.IReferralView;

public interface IReferralPresenter extends IPresenter<IReferralView> {

    void bindViewData();
}
