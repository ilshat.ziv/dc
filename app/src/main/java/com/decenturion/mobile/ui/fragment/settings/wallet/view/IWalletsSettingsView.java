package com.decenturion.mobile.ui.fragment.settings.wallet.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletsSettingsModelView;

public interface IWalletsSettingsView extends IScreenFragmentView {

    void onBindViewData(@NonNull WalletsSettingsModelView modelView);

    void onEnableTwoFA();
}
