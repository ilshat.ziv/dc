package com.decenturion.mobile.ui.fragment.referral.statistic.presenter;

import com.decenturion.mobile.business.referral.main.IReferralInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.referral.statistic.model.StatisticReferralModelView;
import com.decenturion.mobile.ui.fragment.referral.statistic.view.IStatisticReferralView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class StatisticReferralPresenter extends Presenter<IStatisticReferralView> implements IStatisticReferralPresenter {

    private IReferralInteractor mIReferralInteractor;
    private StatisticReferralModelView mReferralModelView;

    public StatisticReferralPresenter(IReferralInteractor iReferralInteractor) {
        mIReferralInteractor = iReferralInteractor;
    }

    @Override
    public void bindViewData() {
//        showProgressView();
//
//        Observable<BalanceReferralModelView> obs = mIReferralInteractor.getReferralInfo()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());
//
//        mIDCompositeSubscription.subscribe(obs,
//                (Action1<BalanceReferralModelView>) this::bindViewDataSuccess,
//                this::bindViewDataFailure
//        );
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);

    }

    private void bindViewDataSuccess(StatisticReferralModelView model) {
        mReferralModelView = model;

        hideProgressView();

        if (mView != null) {
            mView.onBindViewData(model);
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
