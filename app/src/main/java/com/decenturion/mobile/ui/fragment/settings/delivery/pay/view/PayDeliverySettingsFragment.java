package com.decenturion.mobile.ui.fragment.settings.delivery.pay.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.di.dagger.settings.delivery.pay.PayDeliverySettingsComponent;
import com.decenturion.mobile.network.response.model.Coin;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.UserDataView;
import com.decenturion.mobile.ui.component.qr.QrCodeViewComponent;
import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.decenturion.mobile.ui.component.spinner_v2.SpinnerView_v2;
import com.decenturion.mobile.ui.dialog.fullscreen.QrCodeDialog;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.DeliveryDataModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PaymentModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.model.PayDeliverySettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.pay.presenter.IPayDeliverySettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PayDeliverySettingsFragment extends BaseFragment implements IPayDeliverySettingsView,
        SpinnerView_v2.OnSpinnerItemClickListener,
        ChangeComponentListener,
        View.OnClickListener,
        QrCodeViewComponent.OnQrCodeViewClickListener {

    /* UI */

    @BindView(R.id.phoneView)
    UserDataView mPhoneView;

    @BindView(R.id.addressView)
    UserDataView mAddressView;

    @BindView(R.id.cityView)
    UserDataView mCityView;

    @BindView(R.id.stateView)
    UserDataView mStateView;

    @BindView(R.id.countyView)
    UserDataView mCountryView;

    @BindView(R.id.zipView)
    UserDataView mZipView;


    @BindView(R.id.summPayView)
    BuyTokenView mSummPayView;

    @BindView(R.id.selectCoinView)
    SpinnerView_v2 mSelectCoinView;

    @BindView(R.id.addressPayView)
    QrCodeViewComponent mQrCodeViewComponent;

    @BindView(R.id.payActionView)
    LinearLayout mPayActionView;

    @BindView(R.id.proceedButtonView)
    LocalizedButtonView mProceedButtonView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IPayDeliverySettingsPresenter mIPhisicalPassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        PayDeliverySettingsComponent component = appDelegate
                .getIDIManager()
                .plusPayDeliverySettingsComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_pay_delivery_passport, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mPhoneView.showLabel();
        mPhoneView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_phone_title));
        mPhoneView.editMode();

        mAddressView.showLabel();
        mAddressView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_address_title));
        mAddressView.editMode();

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_city_title));
        mCityView.editMode();

        mStateView.showLabel();
        mStateView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_state_title));
        mStateView.editMode();

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_country_title));

        mZipView.showLabel();
        mZipView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_delivery_zip_title));
        mZipView.editMode();

        mSummPayView.showLabel();
        mSummPayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_amount_title));
        mSummPayView.simpleMode();
        mSummPayView.setVisibleDivider(true);

        // TODO: 19.09.2018 костыль, слишком сложная структура перевода
        String localeString = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_currency);
        localeString = localeString.replaceAll(":.*", ":");
        mSelectCoinView.setLabel(localeString);
        mSelectCoinView.showLabel();
        mSelectCoinView.setOnSpinnerItemClickListener(this);

        mQrCodeViewComponent.hideLabel();
        mQrCodeViewComponent.setOnQrCodeViewClickListener(this);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_offlinepassport_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPhisicalPassportPresenter.bindView(this);
        mIPhisicalPassportPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPhisicalPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIPhisicalPassportPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIPhisicalPassportPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull CoinModelView model1,
                               @NonNull PayDeliverySettingsModelView model) {

//        if (model.isDeliveryPaid()) {
//            ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
//            assert activity != null;
//            INavigationManager iNavigationManager = activity.getINavigationManager();
//            iNavigationManager.navigateTo(DeliveredSettingsFragment.class);
//            return;
//        }

        DeliveryDataModelView model2 = model.getDeliverDataModelView();
        mPhoneView.setText(model2.getPhone());
        mAddressView.setText(model2.getAddress());
        mCityView.setText(model2.getCity());
        mStateView.setText(model2.getState());
        mCountryView.setText(model2.getCountry());
        mZipView.setText(model2.getZip());

        ArrayList<Coin> coinData = model1.getCoinData();
        Coin coin = coinData.get(0);
        mSelectCoinView.setData(coinData);
        mSelectCoinView.selectItem(coin.getItemKey());
    }

    private void onBindPayModelView(PayDeliverySettingsModelView model) {
        PaymentModelView paymentModelView = model.getPaymentModelView();

        String summPay = mILocalistionManager.getLocaleString(R.string.app_offlinepassport_payment_amount_value);
        summPay = summPay.replace("{amount}", paymentModelView.getCoinsPay());
        summPay = summPay.replace("{currency}", paymentModelView.getTypePay());
        mSummPayView.setText(summPay);
        mSummPayView.setCoin(paymentModelView.getCyrrencePay());

        mQrCodeViewComponent.setAddress(paymentModelView.getAddress());
        mQrCodeViewComponent.setQrData(paymentModelView.getQrCode());
    }

    @OnClick(R.id.proceedButtonView)
    protected void onProceed() {

    }

    @Override
    public void onBindPaymentViewData(PayDeliverySettingsModelView modelView) {
        onBindPayModelView(modelView);
    }

    @Override
    public void onSpinnerItemClick(ISpinnerItem iSpinnerItem) {
        mIPhisicalPassportPresenter.getPaymentAddress(iSpinnerItem.getItemKey());
    }

    @Override
    public void onChangeComponentData(View view) {
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @Override
    public void onQrCodeViewClick(@NonNull String qrCodeData) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        String tag = QrCodeDialog.class.getSimpleName();
        QrCodeDialog.show(fragmentManager, tag, qrCodeData);
    }

    @Override
    public void onQrCodeDataCopied() {
        showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
    }
}
