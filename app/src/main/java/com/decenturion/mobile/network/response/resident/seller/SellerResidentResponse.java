package com.decenturion.mobile.network.response.resident.seller;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerResidentResponse extends BaseResponse {

    @JsonProperty("result")
    private SellerResidentResult mResult;

    public SellerResidentResponse() {
    }

    @JsonIgnore
    public SellerResidentResult getResult() {
        return mResult;
    }
}
