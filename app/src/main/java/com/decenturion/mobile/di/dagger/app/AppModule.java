package com.decenturion.mobile.di.dagger.app;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.app.lifecycle.IAppLifecycleManager;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.LocalisationManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.PrefsManager;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.app.resource.ResourceManager;
import com.decenturion.mobile.app.toast.IToastManager;
import com.decenturion.mobile.app.toast.ToastManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Context mAppContext;
    private IAppLifecycleManager mIAppLifecycleManager;

    private IPrefsManager mIPrefsManager;
    private IToastManager mIToastManager;
    private IResourceManager mIResourceManager;

    private ILocalistionManager mILocalistionManager;

    public AppModule(@NonNull Context context) {
        mAppContext = context;
    }

    public AppModule(@NonNull Context context, @NonNull IAppLifecycleManager iAppLifecycleManager) {
        this(context);
        mIAppLifecycleManager = iAppLifecycleManager;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mAppContext;
    }

    @Provides
    @Singleton
    IAppLifecycleManager provideIAppLifecycleManager() {
        return mIAppLifecycleManager;
    }

    @Provides
    @Singleton
    IResourceManager provideIResourceManager() {
        if (mIResourceManager == null) {
            mIResourceManager = new ResourceManager(mAppContext);
        }
        return mIResourceManager;
    }

    @Provides
    @Singleton
    IToastManager provideIToastManager() {
        if (mIToastManager == null) {
            mIToastManager = new ToastManager(mAppContext);
        }
        return mIToastManager;
    }

    @Provides
    @Singleton
    public IPrefsManager provideIPrefsManager() {
        if (mIPrefsManager == null) {
            mIPrefsManager = new PrefsManager(mAppContext);
        }
        return mIPrefsManager;
    }

    @Provides
    @Singleton
    public ILocalistionManager provideILocalistionManager() {
        if (mILocalistionManager == null) {
            mILocalistionManager = new LocalisationManager(
                    mAppContext,
                    provideIResourceManager(),
                    provideIPrefsManager()
            );
        }
        return mILocalistionManager;
    }

}
