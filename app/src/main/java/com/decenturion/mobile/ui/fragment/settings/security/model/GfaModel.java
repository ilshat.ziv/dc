package com.decenturion.mobile.ui.fragment.settings.security.model;

import com.decenturion.mobile.utils.RandomStringUtils;

import java.security.SecureRandom;

public class GfaModel {

    private String mSecretString;
    private String mSecretCode;

    private boolean isEnableGfa;
    private boolean isEnabledGfa;

    GfaModel() {
        RandomStringUtils randomStringUtils = new RandomStringUtils(16,
                new SecureRandom(), RandomStringUtils.UPPER);
        mSecretString = randomStringUtils.nextString();
    }

    public GfaModel(String secretString, String secretCode, boolean isEnableGfa) {
        mSecretString = secretString;
        mSecretCode = secretCode;
        this.isEnableGfa = isEnableGfa;
    }

    public GfaModel(String secretString, String secretCode, boolean isEnableGfa, boolean isEnabledGfa) {
        this(secretString, secretCode, isEnableGfa);
        this.isEnabledGfa = isEnabledGfa;
    }

    public void setSecretCode(String secretCode) {
        mSecretCode = secretCode;
    }

    public void setEnableGfa(boolean enableGfa) {
        this.isEnableGfa = enableGfa;
    }

    public void setEnabledGfa(boolean enabledGfa) {
        this.isEnabledGfa = enabledGfa;
    }

    public String getSecretString() {
        return mSecretString;
    }

    public String getSecretCode() {
        return mSecretCode;
    }

    public boolean isEnableGfa() {
        return this.isEnableGfa;
    }

    public boolean isEnabledGfa() {
        return this.isEnabledGfa;
    }
}
