package com.decenturion.mobile.network.response.model;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Coin implements ISpinnerItem {

    @JsonProperty("id")
    private String mId;

    @JsonProperty("label")
    private String mLabel;

    public Coin() {}

    @JsonIgnore
    public String getId() {
        return mId;
    }

    @JsonIgnore
    public String getLabel() {
        return mLabel;
    }

    @Override
    public String getItemName() {
        return mLabel;
    }

    @Override
    public String getItemKey() {
        return mId;
    }
}
