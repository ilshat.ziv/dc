package com.decenturion.mobile.repository.token;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.database.dao.OrmTokenDAO;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.response.model.Token;
import com.decenturion.mobile.utils.LoggerUtils;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;
import java.util.List;

public class TokenRepository implements ITokenRepository {

    private OrmHelper mOrmHelper;

    public TokenRepository(@NonNull OrmHelper ormHelper) {
        mOrmHelper = ormHelper;
    }

//    @Override
//    public List<OrmTokenModel> getTokenInfo() {
//        try {
//            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
//            OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
//            return dao.queryForAll();
//        } catch (Exception e) {
//            LoggerUtils.exception(e);
//        }
//        return new ArrayList<>();
//    }

    @Override
    public List<OrmTokenModel> getTokens() {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
            return dao.queryForAll();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
        return new ArrayList<>();
    }

    @Override
    public OrmTokenModel getTokenById(int tokenId) {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
            QueryBuilder<OrmTokenModel, Integer> builder = dao.queryBuilder();
            return builder.where()
                    .eq(OrmTokenModel.ID, tokenId)
                    .queryForFirst();
        } catch (Exception e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    @Override
    public OrmTokenModel getTokenInfoById(int tokenId) {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
            QueryBuilder<OrmTokenModel, Integer> builder = dao.queryBuilder();
            return builder.where()
                    .eq(OrmTokenModel.ID, tokenId)
                    .queryForFirst();
        } catch (Exception e) {
            LoggerUtils.exception(e);
            return null;
        }
    }

    @Override
    public void saveTokenInfo(@NonNull Token token) throws Exception {
        OrmTokenModel model = new OrmTokenModel(token);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
        dao.createOrUpdate(model);
    }

    @Override
    public void saveTokenInfo(@NonNull OrmTokenModel model) throws Exception {
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmTokenDAO dao = new OrmTokenDAO(connectionSource, OrmTokenModel.class);
        dao.createOrUpdate(model);
    }
}
