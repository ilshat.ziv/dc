package com.decenturion.mobile.ui.dialog.token;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.tokendetails.transaction.TransactionComponent;
import com.decenturion.mobile.ui.dialog.SingleDialogFragment;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;
import com.decenturion.mobile.ui.fragment.token.transaction.presenter.ITransactionPresenter;
import com.decenturion.mobile.ui.fragment.token.transaction.view.ITransactionView;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class DeleteDealDialog extends SingleDialogFragment implements ITransactionView {

    public static String TRAIDE_UUID = "TRAIDE_UUID";
    private String mTraideUUID;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ITransactionPresenter mITransactionPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTraideUUID = arguments.getString(TRAIDE_UUID, "");
        }
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        TransactionComponent transactionComponent = appDelegate
                .getIDIManager()
                .plusTransactionComponent();
        transactionComponent.inject(this);
    }


    public static void show(@NonNull FragmentManager fragmentManager) {
        DeleteDealDialog dialog = new DeleteDealDialog();
        dialog.show(fragmentManager, DeleteDealDialog.class.getSimpleName());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setTitleText(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_deletedeal_title));
        setPositiveTextString(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_deletedeal_delete));
        setNegativeTextString(mILocalistionManager.getLocaleString(R.string.app_account_buytokens_transaction_close));

        MaterialDialog arbitrationRequestDialog = (MaterialDialog) super.onCreateDialog(savedInstanceState);
        setContentView(R.layout.fr_dialog_delete_deal);

        ButterKnife.bind(this, arbitrationRequestDialog.getView());

        arbitrationRequestDialog.setCancelable(false);
        arbitrationRequestDialog.setCanceledOnTouchOutside(false);
        arbitrationRequestDialog.show();

        setupUi();

        return arbitrationRequestDialog;
    }

    private void setupUi() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mITransactionPresenter.bindView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mITransactionPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mITransactionPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mITransactionPresenter.unbindView();
    }

    @Override
    public void onPositiveClickButton() {
        dismiss();
    }

    @Override
    public void onNegativeClickButton() {
        mITransactionPresenter.cancelTraide(mTraideUUID);
    }

    @Override
    public void onBindViewData(@NonNull TransactionModelView model) {

    }

    @Override
    public void onTransactionConfirmed() {

    }

    @Override
    public void onErrorTransaction() {

    }

    @Override
    public void onTransactionCanceled() {
        FragmentActivity activity = requireActivity();
        activity.finish();
    }
}
