package com.decenturion.mobile.di.dagger.referral.main;

import com.decenturion.mobile.ui.dialog.referral.RemoveInviteDialog;
import com.decenturion.mobile.ui.dialog.referral.invite.NewInviteDialog;
import com.decenturion.mobile.ui.fragment.referral.balance.view.BalanceReferralFragment;
import com.decenturion.mobile.ui.fragment.referral.main.view.ReferralFragment;
import com.decenturion.mobile.ui.fragment.referral.statistic.view.StatisticReferralFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        ReferralModule.class
})

@ReferralScope
public interface ReferralComponent {

    void inject(ReferralFragment view);

    void inject(StatisticReferralFragment view);

    void inject(BalanceReferralFragment view);

    void inject(RemoveInviteDialog view);

    void inject(NewInviteDialog view);
}
