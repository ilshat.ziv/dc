package com.decenturion.mobile.network.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MinPriceResponse extends BaseResponse {

    @JsonProperty("result")
    private String mResult;

    public MinPriceResponse() {
    }

    @JsonIgnore
    public String getResult() {
        return mResult;
    }
}
