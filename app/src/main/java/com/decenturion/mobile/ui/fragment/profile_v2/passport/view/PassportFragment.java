package com.decenturion.mobile.ui.fragment.profile_v2.passport.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.passport.PassportComponent;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.component.SimpleTextView;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.presenter.IPassportPresenter;
import com.decenturion.mobile.ui.fragment.profile.passport.view.IPassportView;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.RefreshView;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PassportFragment extends BaseFragment implements IPassportView,
        SwipeRefreshLayout.OnRefreshListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    // Balance

    @BindView(R.id.balanceValueView)
    TextView mBalanceValueView;

    @BindView(R.id.balanceCoinView)
    TextView mBalanceCoinView;

    @BindView(R.id.moneyView)
    TextView mMoneyView;

    @BindView(R.id.powerView)
    TextView mPowerView;

    @BindView(R.id.gloryView)
    TextView mGloryView;

    // Bio

    @BindView(R.id.birthdayView)
    SimpleTextView mBirthdayView;

    @BindView(R.id.sexView)
    SpinnerView mSexView;

    @BindView(R.id.countryView)
    SpinnerView mCountryView;

    @BindView(R.id.cityView)
    SimpleTextView mCityView;

    @BindView(R.id.localWorldwideView)
    SimpleTextView mLocalWorldwideView;

    @BindView(R.id.topWorldwideView)
    SimpleTextView mTopWorldwideView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    IPassportPresenter mIPassportPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        PassportComponent passportComponent = appDelegate
                .getIDIManager()
                .plusPassportComponent();
        passportComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_passport_v2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        mRefreshView.setOnRefreshListener(this);

        mBirthdayView.showLabel();
        mBirthdayView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_dob));
        mBirthdayView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_dob));
        mBirthdayView.simpleMode();
        mBirthdayView.setVisibleDivider(true);

        mSexView.showLabel();
        mSexView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_sex));
        mSexView.simpleMode();
        mSexView.setVisibleDivider(true);

        mCountryView.showLabel();
        mCountryView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_country));
        mCountryView.simpleMode();
        mCountryView.setVisibleDivider(true);

        mCityView.showLabel();
        mCityView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_city));
        mCityView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_city));
        mCityView.simpleMode();
        mCityView.setVisibleDivider(true);

        mLocalWorldwideView.showLabel();
        mLocalWorldwideView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_toplocal));
        mLocalWorldwideView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_toplocal));
        mLocalWorldwideView.simpleMode();
        mLocalWorldwideView.setVisibleDivider(true);

        mTopWorldwideView.showLabel();
        mTopWorldwideView.setLabel(mILocalistionManager.getLocaleString(R.string.app_account_passport_topworldwide));
        mTopWorldwideView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_passport_topworldwide));
        mTopWorldwideView.simpleMode();
        mTopWorldwideView.setVisibleDivider(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPassportPresenter.bindView(this);
        mIPassportPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPassportPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mIPassportPresenter.unbindView();
    }

    @Override
    public void onBindViewData(@NonNull SexModelView model,
                               @NonNull CountryModelView model1,
                               @NonNull PassportModelView model2) {

        IMainActivity activity = (IMainActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(model2.getFirstname() + " " + model2.getLastname());

        // Balance
        mBalanceCoinView.setText(model2.getCoin());
        mBalanceValueView.setText(model2.getBalance());


        mMoneyView.setText("X " + model2.getMoney());
        mPowerView.setText("X " + model2.getPower());
        mGloryView.setText("X " + model2.getGlory());

        mSexView.setData(model.getSexData());
        mCountryView.setData(model1.getCountryData());

        mBirthdayView.setText(model2.getBirth());
        mSexView.selectItem(model2.getSex());
        mCountryView.selectItem(model2.getCountry());
        mCityView.setText(model2.getCity());

//        mLocalWorldwideView.setText("1440");
//        mTopWorldwideView.setText("5496");
    }

    @Override
    public void onSignout() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, SignActivity.class);
        startActivity(intent);
        activity.finish();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mIPassportPresenter.bindViewData();
        mRefreshView.hide();
    }

    @OnClick(R.id.telegramContactView)
    protected void onTelegramContact() {
        PassportModelView model = mIPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String telegramContact = model.getTelegramContact();
        if (!TextUtils.isEmpty(telegramContact)) {
            if (isAppInstalled("org.telegram.messenger")) {
                String url = "https://t.me/" + telegramContact.replace("@", "");
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    startActivity(browserIntent);
                } catch (Exception e) {
                    LoggerUtils.exception(e);
                }
            } else {
                launchMarket("org.telegram.messenger");
            }
        }
    }

    @OnClick(R.id.whatsappContactView)
    protected void onWhatsappContact() {
        PassportModelView model = mIPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String whatsappContact = model.getWhatsappContact();
        if (!TextUtils.isEmpty(whatsappContact)) {
            if (isAppInstalled("com.whatsapp")) {
                String url = "https://wa.me/" + whatsappContact;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    startActivity(browserIntent);
                } catch (Exception e) {
                    LoggerUtils.exception(e);
                }
            } else {
                launchMarket("com.whatsapp");
            }
        }
    }

    @OnClick(R.id.wechatContactView)
    protected void onWechatContact() {
        PassportModelView model = mIPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String wechatContact = model.getWechatContact();
        if (!TextUtils.isEmpty(wechatContact)) {
            if (isAppInstalled("com.tencent.mm")) {
                String url = "weixin://dl/chat?" + wechatContact;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    startActivity(browserIntent);
                } catch (Exception e) {
                    LoggerUtils.exception(e);
                }
            } else {
                launchMarket("com.tencent.mm");
            }
        }
    }

    @OnClick(R.id.viberContactView)
    protected void onViberContact() {
        PassportModelView model = mIPassportPresenter.getProfileView();
        if (model == null) {
            return;
        }
        String viberContact = model.getViberContact();
        if (!TextUtils.isEmpty(viberContact)) {
            if (isAppInstalled("com.viber.voip")) {
                Uri uri = Uri.parse("tel:" + Uri.encode(viberContact));
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
                intent.setData(uri);
                startActivity(intent);
            } else {
                launchMarket("com.viber.voip");
            }
        }
    }

    private boolean isAppInstalled(String uri) {
        Context context = getContext();
        assert context != null;
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }

        return false;
    }

    private void launchMarket(String packageName) {
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
        } catch (Exception e) {
            LoggerUtils.exception(e);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
        }
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIPassportPresenter.unbindView();
    }
}
