package com.decenturion.mobile.ui.fragment.ministry.product.model;

import android.text.TextUtils;

import com.decenturion.mobile.network.response.payment.deliver.DeliverPaymentResult;

public class PaymentModelView {

    private String mTypePay;
    private String mPriceOne;
    private String mCoinsPay;
    private String mCyrrencePay;
    private String mQrCode;
    private String mAddress;

    public PaymentModelView() {
    }

    public String getTypePay() {
        return mTypePay;
    }

    public String getCoinsPay() {
        return mCoinsPay;
    }

    public String getCyrrencePay() {
        return mCyrrencePay;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getQrCode() {
        return mQrCode;
    }

    public void setTypePay(String typePay) {
        mTypePay = typePay;
    }

    public String getPriceOne() {
        return mPriceOne;
    }

    public void setPriceOne(String priceOne) {
        mPriceOne = priceOne;
    }

    public void setCoinsPay(String coinsPay) {
        mCoinsPay = coinsPay;
    }

    public void setCyrrencePay(String cyrrencePay) {
        mCyrrencePay = cyrrencePay;
    }

    public void setAddress(String address) {
        if (TextUtils.equals(mTypePay, "btc")) {
            String s = "bitcoin:{address}?amount={amount}";
            mQrCode = s.replace("{address}", address)
                    .replace("{amount}", mCoinsPay);
        } else {
            mQrCode = address;
        }
        mAddress = address;
    }
}
