package com.decenturion.mobile.ui.fragment.score.send.express.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.score.send.express.view.IExpressSendTokenView;

public interface IExpressSendTokenPresenter extends IPresenter<IExpressSendTokenView> {

    void bindViewData(int tokenId);

    void onSaveInstanceState(@NonNull Bundle outState, String twofa);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    void sendToken(@NonNull String twofa);
}
