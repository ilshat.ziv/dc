package com.decenturion.mobile.database.type;

import com.decenturion.mobile.database.type.model.CoinTypeModel;

public class CoinType extends BaseFieldClassPersister<CoinTypeModel> {

    private static CoinType INSTANCE;

    protected CoinType(Class<CoinTypeModel> typeFieldClass) {
        super(typeFieldClass);
    }

    public static CoinType getSingleton() {
        if (INSTANCE == null) {
            return INSTANCE = new CoinType(CoinTypeModel.class);
        }
        return INSTANCE;
    }
}