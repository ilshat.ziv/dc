package com.decenturion.mobile.ui.fragment.score.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.business.score.token.IScoreTokenDetailsInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.score.details.model.ScoreTokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.score.details.view.IScoreTokenDetailsView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ScoreTokenDetailsPresenter extends Presenter<IScoreTokenDetailsView> implements IScoreTokenDetailsPresenter {

    private IScoreTokenDetailsInteractor mIScoreTokenDetailsInteractor;
    private ScoreTokenDetailsModelView mTokenDetailsModelView;

    public ScoreTokenDetailsPresenter(IScoreTokenDetailsInteractor iScoreTokenDetailsInteractor) {
        mIScoreTokenDetailsInteractor = iScoreTokenDetailsInteractor;
        mTokenDetailsModelView = new ScoreTokenDetailsModelView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {

    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {

    }

    @Override
    public ScoreTokenDetailsModelView getModelView() {
        return mTokenDetailsModelView;
    }

    @Override
    public void bindViewData(int tokenId) {
        showProgressView();

        mTokenDetailsModelView = new ScoreTokenDetailsModelView(tokenId);
        Observable<ScoreTokenDetailsModelView> obs = mIScoreTokenDetailsInteractor.getTokenDetails(tokenId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ScoreTokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory) {
        showProgressView();

        mTokenDetailsModelView = new ScoreTokenDetailsModelView(tokenId);
        mTokenDetailsModelView.setResidentUUID(residentUuid);
        mTokenDetailsModelView.setCoinUUID(tokenCoinUuid);
        mTokenDetailsModelView.setCategory(tokenCategory);

        Observable<ScoreTokenDetailsModelView> obs = mIScoreTokenDetailsInteractor.getTokenDetails(mTokenDetailsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ScoreTokenDetailsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ScoreTokenDetailsModelView model) {
        mTokenDetailsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }
}
