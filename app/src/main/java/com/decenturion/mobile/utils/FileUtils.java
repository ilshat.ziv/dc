package com.decenturion.mobile.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;

public final class FileUtils {

    public static String loadJsonFromAssets(@NonNull Context context, @NonNull String path) {
        String json = null;
        try {
            AssetManager assets = context.getAssets();
            InputStream is = assets.open(path);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            LoggerUtils.exception(e);
            return null;
        }
        return json;
    }
}
