package com.decenturion.mobile.di;

import com.decenturion.mobile.di.dagger.AppComponent;
import com.decenturion.mobile.di.dagger.aggrement.UserAggrementComponent;
import com.decenturion.mobile.di.dagger.header.HeaderComponent;
import com.decenturion.mobile.di.dagger.invite.InviteComponent;
import com.decenturion.mobile.di.dagger.ministry.market.MinistryComponent;
import com.decenturion.mobile.di.dagger.ministry.product.MinistryProductComponent;
import com.decenturion.mobile.di.dagger.pass.confirm.email.ConfirmEmailRestorePassComponent;
import com.decenturion.mobile.di.dagger.pass.confirm.phone.ConfirmPhoneRestorePassComponent;
import com.decenturion.mobile.di.dagger.pass.edit.EditProfileComponent;
import com.decenturion.mobile.di.dagger.pass.reset.ResetPassComponent;
import com.decenturion.mobile.di.dagger.pass.restore.RestorePassComponent;
import com.decenturion.mobile.di.dagger.passport.PassportComponent;
import com.decenturion.mobile.di.dagger.passport.activation.ActivationPassportComponent;
import com.decenturion.mobile.di.dagger.passport.online.OnlinePassportComponent;
import com.decenturion.mobile.di.dagger.passport.phisical.PhisicalPassportComponent;
import com.decenturion.mobile.di.dagger.passport.welcome.WellcomeComponent;
import com.decenturion.mobile.di.dagger.profile.ProfileComponent;
import com.decenturion.mobile.di.dagger.profile.actions.LogsComponent;
import com.decenturion.mobile.di.dagger.profile.score.ScoreListComponent;
import com.decenturion.mobile.di.dagger.profile.transactions.TransactionListComponent;
import com.decenturion.mobile.di.dagger.referral.acquisition.AcquisitionComponent;
import com.decenturion.mobile.di.dagger.referral.invite.InviteListComponent;
import com.decenturion.mobile.di.dagger.referral.invite.create.CreateInviteComponent;
import com.decenturion.mobile.di.dagger.referral.main.ReferralComponent;
import com.decenturion.mobile.di.dagger.referral.history.ReferralHistorylComponent;
import com.decenturion.mobile.di.dagger.score.ScoreTokenListComponent;
import com.decenturion.mobile.di.dagger.score.express.ExpressSendTokenComponent;
import com.decenturion.mobile.di.dagger.score.main.MainScoreSendTokenComponent;
import com.decenturion.mobile.di.dagger.score.token.ScoreTokenDetailsComponent;
import com.decenturion.mobile.di.dagger.score.trade.TradeScoreSendTokenComponent;
import com.decenturion.mobile.di.dagger.seller.passport.SellerPassportComponent;
import com.decenturion.mobile.di.dagger.seller.profile.SellerProfileComponent;
import com.decenturion.mobile.di.dagger.seller.token.SellerTokenComponent;
import com.decenturion.mobile.di.dagger.seller.traids.SellerTraidsComponent;
import com.decenturion.mobile.di.dagger.send.SendTokenComponent;
import com.decenturion.mobile.di.dagger.settings.account.AccountSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.action.ActionListSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.DeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.delivered.DeliveredSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.fill.FillDeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.pay.PayDeliverySettingsComponent;
import com.decenturion.mobile.di.dagger.settings.delivery.pending.DeliveryPendingSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.details.DetailsSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.mail.MailingSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.main.SettingsComponent;
import com.decenturion.mobile.di.dagger.settings.passport.PassportSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.password.PasswordSettingsComponent;
import com.decenturion.mobile.di.dagger.settings.security.SecurityComponent;
import com.decenturion.mobile.di.dagger.settings.wallets.WalletsSettingsComponent;
import com.decenturion.mobile.di.dagger.sign.GfaSigninComponent;
import com.decenturion.mobile.di.dagger.signin.SigninComponent;
import com.decenturion.mobile.di.dagger.signup.SignupComponent;
import com.decenturion.mobile.di.dagger.splash.SplashComponent;
import com.decenturion.mobile.di.dagger.support.SupportComponent;
import com.decenturion.mobile.di.dagger.token.TokenComponent;
import com.decenturion.mobile.di.dagger.token.details.SellerTokenDetailsComponent;
import com.decenturion.mobile.di.dagger.token.info.TokenInfoComponent;
import com.decenturion.mobile.di.dagger.token.list.TokenListComponent;
import com.decenturion.mobile.di.dagger.tokendetails.buy.BuyTokenComponent;
import com.decenturion.mobile.di.dagger.tokendetails.details.TokenDetailsComponent;
import com.decenturion.mobile.di.dagger.tokendetails.edit.EditPriceTokenComponent;
import com.decenturion.mobile.di.dagger.tokendetails.transaction.TransactionComponent;
import com.decenturion.mobile.di.dagger.traid.TraidInfoComponent;
import com.decenturion.mobile.di.dagger.traids.TraidsComponent;
import com.decenturion.mobile.di.dagger.transaction.TransactionInfoComponent;
import com.decenturion.mobile.di.dagger.twofa.EnableTwoFAComponent;
import com.decenturion.mobile.di.dagger.validate.ValidateEmailComponent;
import com.decenturion.mobile.di.dagger.walkthrough.WalkthroughComponent;

public interface IDIManager {

    void onTerminate();

    AppComponent getAppComponent();

    SignupComponent plusSignupComponent();

    SigninComponent plusSigninComponent();

    SettingsComponent plusSettingsComponent();

    EditProfileComponent plusEditProfileComponent();

    SecurityComponent plusSecurityComponent();

    PassportComponent plusPassportComponent();

    TokenComponent plusTokenComponent();

    TokenDetailsComponent plusTokenDetailsComponent();

//    TokenInfoComponent plusTokenInfoComponent();

    BuyTokenComponent plusBuyTokenComponent();

    TransactionComponent plusTransactionComponent();

    UserAggrementComponent plusUserAggrementComponent();

    ValidateEmailComponent plusValidateEmailComponent();

    OnlinePassportComponent plusOnlinePassportComponent();

    PhisicalPassportComponent plusPhisicalPassportComponent();

    ActivationPassportComponent plusActivationPassportComponent();

    WellcomeComponent plusWellcomeComponent();

    AccountSettingsComponent plusAccountSettingsComponent();

    PasswordSettingsComponent plusPasswordSettingsComponent();

    PassportSettingsComponent plusPassportSettingsComponent();

    DeliverySettingsComponent plusDeliverySettingsComponent();

    DetailsSettingsComponent plusDetailsSettingsComponent();

    MailingSettingsComponent plusMailingSettingsComponent();

    ProfileComponent plusProfileComponent();

    EditPriceTokenComponent plusEditPriceTokenComponent();

    InviteComponent plusInviteComponent();

    SellerProfileComponent plusSellerProfileComponent();

    SellerTokenComponent plusSellerTokenComponent();

    SellerPassportComponent plusSellerPassportComponent();

    SendTokenComponent plusSendTokenComponent();

    WalkthroughComponent plusWalkthroughComponent();

    TraidsComponent plusTraidsComponent();

    LogsComponent plusLogsComponent();

    ActionListSettingsComponent plusActionListSettingsComponent();

    TraidInfoComponent plusTraidInfoComponent();

    HeaderComponent plusHeaderComponent();

    RestorePassComponent plusRestorePassComponent();

    ConfirmEmailRestorePassComponent plusConfirmEmailRestorePassComponent();

    ConfirmPhoneRestorePassComponent plusConfirmPhoneRestorePassComponent();

    GfaSigninComponent plusGfaSigninComponent();

    SellerTraidsComponent plusSellerTraidsComponent();

    ResetPassComponent plusResetPassComponent();

    WalletsSettingsComponent plusWalletsSettingsComponent();

    TransactionListComponent plusTransactionListComponent();

    TokenInfoComponent plusTokenInfoComponent();

    SplashComponent plusSplashComponent();

    DeliveredSettingsComponent plusDeliveredSettingsComponent();

    DeliveryPendingSettingsComponent plusDeliveryPendingSettingsComponent();

    FillDeliverySettingsComponent plusFillDeliverySettingsComponent();

    PayDeliverySettingsComponent plusPayDeliverySettingsComponent();

    SellerTokenDetailsComponent plusSellerTokenDetailsComponent();

    TransactionInfoComponent plusTransactionInfoComponent();

    EnableTwoFAComponent plusEnableTwoFAComponent();

    MinistryComponent plusMinistryComponent();

    MinistryProductComponent plusMinistryProductComponent();

    SupportComponent plusSupportComponent();

    ReferralComponent plusReferallComponent();

    ReferralHistorylComponent plusReferralHistorylComponent();

    AcquisitionComponent plusAcquisitionComponent();

    CreateInviteComponent plusCreateInviteComponent();

    InviteListComponent plusInviteListComponent();

    ScoreListComponent plusScoreListComponent();

    TokenListComponent plusTokenListComponent();

    ScoreTokenListComponent plusScoreTokenListComponent();

    ScoreTokenDetailsComponent plusScoreTokenDetailsComponent();

    MainScoreSendTokenComponent plusMainScoreSendTokenComponent();

    TradeScoreSendTokenComponent plusTradeScoreSendTokenComponent();

    ExpressSendTokenComponent plusExpressSendTokenComponent();
}
