package com.decenturion.mobile.network.response.model.score;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transfer {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("coin_uuid")
    private String mCoinUuid;

    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("from_address")
    private String mFromAddress;

    @JsonProperty("id")
    private int mId;

    @JsonProperty("process_time")
    private String mProcessTime;

    @JsonProperty("to_address")
    private String mToAddress;

    @JsonProperty("uuid")
    private String mUuid;

    public Transfer() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getCoinUuid() {
        return mCoinUuid;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getFromAddress() {
        return mFromAddress;
    }

    @JsonIgnore
    public int getId() {
        return mId;
    }

    @JsonIgnore
    public String getProcessTime() {
        return mProcessTime;
    }

    @JsonIgnore
    public String getToAddress() {
        return mToAddress;
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }
}
