package com.decenturion.mobile.di.dagger.settings.passport;

import com.decenturion.mobile.ui.fragment.settings.passport.view.PassportSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PassportSettingsModule.class
})

@PassportSettingsScope
public interface PassportSettingsComponent {

    void inject(PassportSettingsFragment view);
}
