package com.decenturion.mobile.di.dagger.signup;

import com.decenturion.mobile.ui.fragment.sign.up.view.SignupFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        SignupModule.class
})

@SignupScope
public interface SignupComponent {

    void inject(SignupFragment view);
}
