package com.decenturion.mobile.ui.fragment.score.details.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.score.details.model.ScoreTokenDetailsModelView;
import com.decenturion.mobile.ui.fragment.score.details.view.IScoreTokenDetailsView;

public interface IScoreTokenDetailsPresenter extends IPresenter<IScoreTokenDetailsView> {

    void bindViewData(int tokenId);

    void bindViewData(String residentUuid, int tokenId, String tokenCoinUuid, String tokenCategory);

    void onSaveInstanceState(@NonNull Bundle outState);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);

    ScoreTokenDetailsModelView getModelView();
}
