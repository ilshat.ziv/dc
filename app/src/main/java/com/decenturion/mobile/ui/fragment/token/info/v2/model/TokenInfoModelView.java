package com.decenturion.mobile.ui.fragment.token.info.v2.model;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;

import java.util.ArrayList;
import java.util.List;

public class TokenInfoModelView {

    private String mCategory;
    private List<InfoParamsModelView> mInfoModelViews;
    private ILocalistionManager mILocalistionManager;


    public TokenInfoModelView() {
    }

    public TokenInfoModelView(String category) {
        mCategory = category;
        initParams(mCategory);
    }

    public TokenInfoModelView(String category, ILocalistionManager iLocalistionManager) {
        mCategory = category;
        mILocalistionManager = iLocalistionManager;
        initParams(mCategory);
    }

    private void initParams(String category) {
        switch (category) {
            default:
            case "passport" : {
                mInfoModelViews = new ArrayList<>();
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_1), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_2), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_3), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_4), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_5), false));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_6), false));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_7), false));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_8), false));
                break;
            }
            case "classic" : {
                mInfoModelViews = new ArrayList<>();
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_1), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_2), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_3), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_4), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_5), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_6), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_7), false));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_8), false));
                break;
            }
            case "liquid" : {
                mInfoModelViews = new ArrayList<>();
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_1), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_2), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_3), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_4), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_5), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_6), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_7), true));
                mInfoModelViews.add(new InfoParamsModelView(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_feature_8), true));
                break;
            }
        }
    }

    public List<InfoParamsModelView> getInfoModelViews() {
        return mInfoModelViews;
    }

}
