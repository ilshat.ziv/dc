package com.decenturion.mobile.ui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.DIManager;
import com.decenturion.mobile.di.IDIManager;
import com.decenturion.mobile.di.dagger.tokendetails.buy.BuyTokenComponent;

import javax.inject.Inject;

import butterknife.BindView;

public class ThrooberFragmentDialog extends BaseDialogFragment {

    public static String TITLE = "TITLE";
    private String mTitle = "Please wait...";

    /* UI */

    @BindView(R.id.titleView)
    TextView mTitleView;

    @BindView(R.id.progressView)
    ProgressBar mProgressView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    public static ThrooberFragmentDialog show(@NonNull FragmentManager fragmentManager,
                            @NonNull String tag,
                            @NonNull String title) {
        ThrooberFragmentDialog dialog = new ThrooberFragmentDialog();

        Bundle bundle = new Bundle();
        bundle.putString(TITLE, title);
        dialog.setArguments(bundle);

        dialog.show(fragmentManager, tag);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.ThrooberDialogFragmetTheme);
    }

    private void injectToDagger() {
        FragmentActivity activity = requireActivity();
        AppDelegate appDelegate = (AppDelegate) activity.getApplication();
        DIManager diManager = (DIManager) appDelegate.getIDIManager();
        diManager.getAppComponent().inject(this);
    }

    private void initArgs() {
//        Bundle arguments = getArguments();
//        if (arguments != null) {
//            mTitle = arguments.getString(TITLE, mTitle);
//        }

        mTitle = mILocalistionManager.getLocaleString(R.string.app_constants_wait);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_dialog_throober, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mTitleView.setText(mTitle);
    }

}
