package com.decenturion.mobile.business.settings.main;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.SignoutResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class SettingsInteractor implements ISettingsInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public SettingsInteractor(IResidentClient IResidentClient,
                              IResidentRepository iResidentRepository,
                              IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<SignoutResponse> signout() {
        return mIResidentClient.signout()
                .doOnNext(d -> clearDate())
                .doOnError(d -> clearDate());
    }

    private void clearDate() {
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.clear();
        try {
            mIResidentRepository.clearData();
        } catch (Exception e) {
            LoggerUtils.exception(e);
        }
    }
}
