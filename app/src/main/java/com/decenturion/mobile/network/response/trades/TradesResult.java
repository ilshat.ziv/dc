package com.decenturion.mobile.network.response.trades;

import com.decenturion.mobile.network.response.model.WrapperTrade;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradesResult {

    @JsonProperty("list")
    private List<WrapperTrade> mWrapperTradeList;

    @JsonProperty("total")
    private int mTotal;

    public TradesResult() {
    }

    @JsonIgnore
    public List<WrapperTrade> getWrapperTradeList() {
        return mWrapperTradeList;
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }
}
