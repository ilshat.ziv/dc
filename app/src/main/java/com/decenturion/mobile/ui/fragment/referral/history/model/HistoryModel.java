package com.decenturion.mobile.ui.fragment.referral.history.model;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.model.ReferralHistoryItem;
import com.decenturion.mobile.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

public class HistoryModel {

    private String mAction;
    private String mDate;
    private String mAmount;

    public HistoryModel(ReferralHistoryItem d, ILocalistionManager iLocalistionManager) {
        String createdAt = d.getCreatedAt();
        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            String s = iLocalistionManager.getLocaleString(R.string.app_constants_today);
            mDate = s + " " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }

        mAction = iLocalistionManager.getLocaleString("app.referral.transactions.type."
                + d.getType().replace("_", ".")
                + ".label");
        mAmount = d.getAmount() + " " + d.getCurrency();
    }

    public String getAction() {
        return mAction;
    }

    public String getDate() {
        return mDate;
    }

    public String getAmount() {
        return mAmount;
    }
}
