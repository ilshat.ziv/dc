package com.decenturion.mobile.network.response.referral.total;

import com.decenturion.mobile.network.response.model.Earned;
import com.decenturion.mobile.network.response.model.Guests;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralTotalResult {

//    "result":{"earned":[],"guests":[{"category":"base","count":"2"}]}}

    @JsonProperty("earned")
    private List<Earned> mEarnedList;

    @JsonProperty("guests")
    private List<Guests> mGuestsList;

    public ReferralTotalResult() {
    }

    @JsonIgnore
    public List<Earned> getEarnedList() {
        return mEarnedList;
    }

    @JsonIgnore
    public List<Guests> getGuestsList() {
        return mGuestsList;
    }
}
