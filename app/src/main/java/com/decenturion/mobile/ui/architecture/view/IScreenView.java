package com.decenturion.mobile.ui.architecture.view;

import android.support.annotation.NonNull;

public interface IScreenView {

    void showMessage(@NonNull String message);

    void showSuccessMessage(@NonNull String message);

    void showErrorMessage(@NonNull String message);

    void showWarningMessage(@NonNull String message);

    void showProgressView();

    void hideProgressView();
}
