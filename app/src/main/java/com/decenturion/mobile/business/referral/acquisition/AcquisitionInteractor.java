package com.decenturion.mobile.business.referral.acquisition;

import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.request.ReferralSettings;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResponse;
import com.decenturion.mobile.network.response.referral.ReferralSettingsResult;
import com.decenturion.mobile.ui.fragment.referral.acquisition.model.AcquisitionModelView;

import rx.Observable;

public class AcquisitionInteractor implements IAcquisitionInteractor {

    private IReferralClient mIReferralClient;

    public AcquisitionInteractor(IReferralClient iReferralClient) {
        mIReferralClient = iReferralClient;
    }

    @Override
    public Observable<AcquisitionModelView> getReferralInfo() {
        return mIReferralClient.getReferralSettings()
                .map(ReferralSettingsResponse::getResult)
                .map(AcquisitionModelView::new);
    }

    @Override
    public Observable<ReferralSettingsResult> setPrice(String type,
                                                       String price, int amount, String bannerText) {
        ReferralSettings model = new ReferralSettings();
        model.setCostingMethod(type);
        switch (type) {
            case "fixed" : {
                model.setFixedCost(price);
                model.setFixedTokenCount(String.valueOf(amount));
                model.setFixedComment(bannerText);
                break;
            }
            case "percent" : {
                model.setFixedCost("0");
                model.setFixedTokenCount(String.valueOf(0));
                model.setPercent(Integer.parseInt(price));
                model.setPercentTokenCount(String.valueOf(amount));
                model.setPercentComment(bannerText);
                break;
            }
        }

        return mIReferralClient.setReferralSettings(model)
                .map(ReferralSettingsResponse::getResult);
    }
}
