package com.decenturion.mobile.utils;

import android.annotation.SuppressLint;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@SuppressLint("SimpleDateFormat")
public final class DateTimeUtils {

    public static int getGMT() {
        TimeZone timezone = TimeZone.getDefault();
        return (int) (timezone.getRawOffset() / DateUtils.HOUR_IN_MILLIS);
    }

    public static long getDatetime(String datetime, String format) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        try {
            Date d = f.parse(datetime);
            return d.getTime();
        } catch (ParseException | NullPointerException e) {
            LoggerUtils.exception(e);
        }
        return -1;
    }

    public static String getDateFormat(long dateUTC, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
//        if (DateTimeUtils.getGMT() != 0) {
//            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
//        }
        return formatter.format(new Date(dateUTC));
    }

    public static String getDateFormat(long dateUTC, String format, Locale locale) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, locale);
        if (DateTimeUtils.getGMT() != 0) {
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        String format1 = formatter.format(new Date(dateUTC));
        return format1.replace("UTC", "GMT");
    }

    public static String getDateFormatLocale(long dateByGMT, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        if (DateTimeUtils.getGMT() != 0) {
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        return formatter.format(new Date(dateByGMT));
    }

    public static long convertToLocalGMT(long date) {
        date += TimeUnit.HOURS.toMillis(getGMT());
        return date;
    }

    public static long convertToUTC(long date) {
        date += TimeUnit.HOURS.toMillis(getGMT());
        return date;
    }

    public static long convertToGMT(long date) {
        date -= TimeUnit.HOURS.toMillis(getGMT());
        return date;
    }


    @SuppressLint("WrongConstant")
    public static int getWeekDayIndex() {
        Calendar c = Calendar.getInstance();
        Date date = new Date(System.currentTimeMillis());
        c.setTime(date);
        int i = c.get(Calendar.DAY_OF_WEEK);

        return (i == 1) ? 7 : i - 1;
    }

    public static long getTimePart(long date) {
        long milDays = TimeUnit.MILLISECONDS.toDays(date);
        return date - TimeUnit.DAYS.toMillis(milDays);
    }

    public static String getTime(long interval) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(interval);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(interval);
        if (minutes == 0) {
            return String.format("%02d sec", seconds);
        } else {
            return String.format("%02d min, %02d sec",
                    minutes,
                    seconds - TimeUnit.MINUTES.toSeconds(minutes)
            );
        }
    }

    public interface Format {
        //2018-12-13 06:30:52 +0000 +0000
        //0001-01-01 00:00:00 +0000 UTC
        String FORMAT_UTC = "yyyy-MM-dd HH:mm:ss +0000 'UTC'";
//        2018-08-29T04:59:44.008619422Z
        String FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

        String SIMPLE_FORMAT = "dd.MM.yyyy";
        String SIMPLE_FORMAT_2 = "dd-MM-yyyy";
        String TITLE_DATE_PICKER = "EEE, d MMM";
        String DATE = "EEE, d MMM yyyy";
        String LABEL_BUTTON_DATE = SIMPLE_FORMAT;
        String LABEL_BUTTON_TIME = "HH:mm";
        String TIME = "HH:mm:ss";
        String DEAL_DATETIME = "d MMMM HH:mm:ss";
        String LABEL_BUTTON_DATETIME = "EEE, d MMM yyyy HH:mm";
        String TITLE_TRAID_INFO_DATETIME = "d MMMM yyyy, HH:mm:ss";
//        String LAST_MODIFY = "EEE, d MMM yyyy HH:mm:ss";//Sat, 29 Oct 2016 18:10:50 GMT// EEE MMM dd HH:mm:ss z yyyy
        String LAST_MODIFY = "EEE, dd MMM yyyy HH:mm:ss zzz";
    }
}