package com.decenturion.mobile.network.response.reset;

import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.Settings;
import com.decenturion.mobile.network.response.model.Wallet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetPasswordResult {

    @JsonProperty("locale")
    private String mLocale;

    @JsonProperty("passport")
    private Passport mPassport;

    @JsonProperty("photo")
    private Photo mPhoto;

    @JsonProperty("resident")
    private Resident mResident;

    @JsonProperty("settings")
    private Settings mSettings;

    @JsonProperty("wallets")
    private List<Wallet> mWalletList;

    public ResetPasswordResult() {
    }

    @JsonIgnore
    public String getLocale() {
        return mLocale;
    }

    @JsonIgnore
    public Passport getPassport() {
        return mPassport;
    }

    @JsonIgnore
    public Photo getPhoto() {
        return mPhoto;
    }

    @JsonIgnore
    public Resident getResident() {
        return mResident;
    }

    @JsonIgnore
    public Settings getSettings() {
        return mSettings;
    }

    @JsonIgnore
    public List<Wallet> getWalletList() {
        return mWalletList;
    }
}
