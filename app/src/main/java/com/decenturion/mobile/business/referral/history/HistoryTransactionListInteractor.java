package com.decenturion.mobile.business.referral.history;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.response.model.ReferralHistoryItem;
import com.decenturion.mobile.network.response.referral.history.ReferralHistoryResponse;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModel;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;

import java.util.List;

import rx.Observable;

public class HistoryTransactionListInteractor implements IHistoryTransactionListInteractor {

    private IReferralClient mIReferralClient;
    private ILocalistionManager mILocalistionManager;

    public HistoryTransactionListInteractor(IReferralClient iReferralClient,
                                            ILocalistionManager iLocalistionManager) {
        mIReferralClient = iReferralClient;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<HistoryModelView> getTransactionsInfo(@NonNull HistoryModelView model) {
        Observable<HistoryModelView> obs1 = mIReferralClient.getReferralHistory(
                model.getOffset(),
                model.LIMIT,
                model.getType()
        )
                .map(ReferralHistoryResponse::getResult)
                .flatMap(d -> {
                    List<ReferralHistoryItem> list = d.getTransactionList();
                    Observable<List<HistoryModel>> listObservable = Observable.just(list)
                            .flatMapIterable(list1 -> list1)
                            .map(d1 -> new HistoryModel(d1, mILocalistionManager))
                            .toList();

                    return Observable.zip(
                            listObservable,
                            Observable.just(d.getTotal()),
                            HistoryModelView::new
                    );
                })
                .onErrorReturn(throwable -> {
                    throw new RuntimeException(throwable.getMessage());
                });

        Observable<HistoryModelView> obs2 = Observable.just(model);

        return Observable.zip(obs1, obs2, (modelView, modelView2) -> {
            modelView2.setOffset(modelView2.getOffset() + modelView2.LIMIT);
            modelView2.addHistoryTransactions(modelView.getHistoryList());
            modelView2.setTotal(modelView.getTotal());
            return modelView2;
        });
    }

}
