package com.decenturion.mobile.ui.fragment.passport.online.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.passport.online.IOnlinePassportInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.Sex;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.passport.online.model.OnlinePassportModelView;
import com.decenturion.mobile.ui.fragment.passport.online.view.IOnlinePassportView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class OnlinePassportPresenter extends Presenter<IOnlinePassportView> implements IOnlinePassportPresenter {

    private Context mContext;
    private IOnlinePassportInteractor mIOnlinePassportInteractor;
    private ILocalistionManager mILocalistionManager;

    private OnlinePassportModelView mOnlinePassportModelView;
    private CountryModelView mCountryModelView;
    private SexModelView mSexModelView;

    public OnlinePassportPresenter(Context context,
                                   IOnlinePassportInteractor iOnlinePassportInteractor,
                                   ILocalistionManager iLocalistionManager) {
        mContext = context;
        mIOnlinePassportInteractor = iOnlinePassportInteractor;
        mILocalistionManager = iLocalistionManager;

        mOnlinePassportModelView = new OnlinePassportModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mOnlinePassportModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<OnlinePassportModelView> obs = mIOnlinePassportInteractor.getProfileInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<OnlinePassportModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(OnlinePassportModelView model) {
        mOnlinePassportModelView = model;

        ArrayList<Sex> sexData = initGenderData();
        mSexModelView = new SexModelView(sexData);

        ArrayList<Country> list = initCountryData();
        mCountryModelView = new CountryModelView(list);

        if (mView != null) {
            mView.onBindViewData(mSexModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    @NonNull
    private ArrayList<Sex> initGenderData() {
        ArrayList<Sex> sexData = new ArrayList<>();
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_male), "male"));
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_female), "female"));
        return sexData;
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void setPassportData(
            @NonNull String photo,
            @NonNull String firstName,
            @NonNull String secondName,
            long birth,
            @NonNull String sex,
            @NonNull String country,
            @NonNull String city) {
        showProgressView();

        Observable<UpdatePassportResult> obs = mIOnlinePassportInteractor.setPassportData(
                photo,
                firstName,
                secondName,
                birth,
                sex,
                country,
                city)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UpdatePassportResult>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(UpdatePassportResult result) {
        hideProgressView();
        if (mView != null) {
            mView.onSavedChanges(result);
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    @Override
    public void uploadPhoto(@NonNull Bitmap photo) {
        showProgressView();

        Observable<UploadPhotoResponse> obs = mIOnlinePassportInteractor.uploadPhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UploadPhotoResponse>) this::uploadPhotoSuccess,
                this::uploadPhotoFailure
        );
    }

    private void uploadPhotoSuccess(UploadPhotoResponse uploadPhotoResponse) {
        hideProgressView();
    }

    private void uploadPhotoFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

    @Override
    public void saveViewData(@NonNull String value, @NonNull String value1, @NonNull String value2,
                             boolean checked, boolean checked1) {
//        mOnlinePassportModelView.setToken(value);
//        mOnlinePassportModelView.setPassword(value1);
//        mOnlinePassportModelView.setRetypePassword(value2);
//        mOnlinePassportModelView.setAgreeTermsOfuse(checked);
//        mOnlinePassportModelView.setAgreeEmailConfirmation(checked1);
    }
}
