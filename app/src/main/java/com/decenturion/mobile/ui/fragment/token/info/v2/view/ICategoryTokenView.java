package com.decenturion.mobile.ui.fragment.token.info.v2.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView;

public interface ICategoryTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull TokenInfoModelView model);
}
