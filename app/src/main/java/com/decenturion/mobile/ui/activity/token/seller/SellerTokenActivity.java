package com.decenturion.mobile.ui.activity.token.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.token.buy.view.BuyTokenFragment;
import com.decenturion.mobile.ui.fragment.token.seller.details.view.SellerTokenDetailsFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.utils.LoggerUtils;

public class SellerTokenActivity extends SingleFragmentActivity {

    public static final String RESIDENT_UUID = "SELLER_UUID";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_COIN_UUID = "TOKEN_COIN_UUID";
    public static final String TOKEN_CATEGORY = "TOKEN_CATEGORY";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";

    public static final String ACTION_BY_TOKEN = "ACTION_BY_TOKEN";
    public static final int ACTION_BUY_TOKEN = 1;

    private int mActionByToken;

    private String mResidentUuid;
    private String mTokenCategory;
    private String mTokenCoinUuid;
    private int mTokenId;
    private String mPaymentMethod;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            getINavigationManager().restoreInstanceState(savedInstanceState);
            return;
        }
        initArgs();

        Bundle bundle = new Bundle();
        bundle.putInt(TOKEN_ID, mTokenId);
        bundle.putString(TOKEN_COIN_UUID, mTokenCoinUuid);
        bundle.putString(TOKEN_CATEGORY, mTokenCategory);
        bundle.putString(RESIDENT_UUID, mResidentUuid);
        bundle.putString(PAYMENT_METHOD, mPaymentMethod);
        INavigationManager iNavigationManager = getINavigationManager();

        switch (mActionByToken) {
            case ACTION_BUY_TOKEN : {
                iNavigationManager.navigateTo(BuyTokenFragment.class, bundle);
                break;
            }
            default : {
                iNavigationManager.navigateTo(SellerTokenDetailsFragment.class, bundle);
                break;
            }
        }
    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mTokenId = intent.getIntExtra(TOKEN_ID, 0);
            mActionByToken = intent.getIntExtra(ACTION_BY_TOKEN, 0);
            mTokenCoinUuid = intent.getStringExtra(TOKEN_COIN_UUID);
            mTokenCategory = intent.getStringExtra(TOKEN_CATEGORY);
            mResidentUuid = intent.getStringExtra(RESIDENT_UUID);
            mPaymentMethod = intent.getStringExtra(PAYMENT_METHOD);
        }

        if (mTokenId == 0 && TextUtils.isEmpty(mTokenCoinUuid)) {
            LoggerUtils.exception(new Exception("TOKEN_ID is not be 0 or TOKEN_COIN_UUID is not be emty"));
            finish();
        }
    }
}
