package com.decenturion.mobile.repository.traide;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.database.dao.OrmTraideDAO;
import com.decenturion.mobile.database.model.OrmTraideModel;
import com.decenturion.mobile.network.response.model.WrapperTrade;
import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class TraideRepository implements ITraideRepository {

    private OrmHelper mOrmHelper;

    public TraideRepository(@NonNull OrmHelper ormHelper) {
        mOrmHelper = ormHelper;
    }

    @Override
    public OrmTraideModel getTraide(@NonNull String traideUUID) {
        try {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmTraideDAO dao = new OrmTraideDAO(connectionSource, OrmTraideModel.class);
            QueryBuilder<OrmTraideModel, Integer> builder = dao.queryBuilder();
            return builder
                    .where()
                    .eq(OrmTraideModel.UUID, traideUUID)
                    .queryForFirst();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveTraideInfo(@NonNull CreateTraideResult d) throws Exception {
        OrmTraideModel model = new OrmTraideModel(d);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmTraideDAO dao = new OrmTraideDAO(connectionSource, OrmTraideModel.class);
        dao.createOrUpdate(model);
    }

    @Override
    public void saveTraideInfo(@NonNull WrapperTrade wrapperTrade) throws Exception {
        OrmTraideModel model = new OrmTraideModel(wrapperTrade);
        ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
        OrmTraideDAO dao = new OrmTraideDAO(connectionSource, OrmTraideModel.class);
        dao.createOrUpdate(model);
    }

    @Override
    public void deleteTraide(@NonNull String uuid) throws Exception {
            ConnectionSource connectionSource = mOrmHelper.getConnectionSource();
            OrmTraideDAO dao = new OrmTraideDAO(connectionSource, OrmTraideModel.class);
            DeleteBuilder<OrmTraideModel, Integer> builder = dao.deleteBuilder();
            builder.where().eq(OrmTraideModel.UUID, uuid);
            builder.delete();
    }

}
