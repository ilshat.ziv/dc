package com.decenturion.mobile.network.response.referral.invite;

import com.decenturion.mobile.network.response.model.Invite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralInviteResult {

    @JsonProperty("list")
    private List<Invite> mList;

    @JsonProperty("total")
    private int mTotal;

    @JsonProperty("used")
    private int mUsed;

    public ReferralInviteResult() {
    }

    @JsonIgnore
    public List<Invite> getList() {
        return mList;
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }

    @JsonIgnore
    public int getUsed() {
        return mUsed;
    }
}
