package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmTokenDAO extends BaseDao<OrmTokenModel, Integer> {

    public OrmTokenDAO(ConnectionSource connectionSource,
                       Class<OrmTokenModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
