package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class UserDataView extends LinearLayout implements Component {

    @Nullable
    @BindView(R.id.labelView)
    protected TextView mLabelView;

    @Nullable
    @BindView(R.id.valueView)
    TextView mValueView;

    private Unbinder mUnbinder;

    public UserDataView(Context context) {
        super(context);
        init();
    }

    public UserDataView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserDataView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected boolean isInitView() {
        return (mLabelView != null && mValueView != null);
    }

    protected void init() {
        if (isInitView()) {
            return;
        }
        mUnbinder = ButterKnife.bind(this, this);
    }

    public void setIconView(@DrawableRes int resource) {
        init();
    }

    public void setLabel(String label) {
        init();
        if (label != null) {
            mLabelView.setText(String.valueOf(label));
        }
        showLabel();
    }

    @Override
    public ImageView getIconView() {
        return null;
    }

    @Override
    public void hideLabel() {
    }

    @Override
    public void showLabel() {
    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    public void editMode() {

    }

    @Override
    public void simpleMode() {

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.setChildrenStates(new SparseArray());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).saveHierarchyState(ss.getChildrenStates());
        }
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).restoreHierarchyState(ss.getChildrenStates());
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    @Override
    public void destroyDrawingCache() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.destroyDrawingCache();
    }

    public void setText(String text) {
        mValueView.setText(text);
    }
}
