package com.decenturion.mobile.ui.fragment.token.info.v1.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class InfoModelView {

    private String mName;
    private boolean isSelected;
    private List<InfoParamsModelView> mInfoParamsModelViews;

    public InfoModelView() {
    }

    public InfoModelView(@NonNull String name) {
        mName = name;
    }

    public InfoModelView(@NonNull String name, boolean b) {
        mName = name;
        isSelected = b;
    }

    public String getName() {
        return mName;
    }

    public List<InfoParamsModelView> getInfoParamsModelViews() {
        return mInfoParamsModelViews;
    }

    public void setInfoParamsModelViews(List<InfoParamsModelView> infoParamsModelViews) {
        mInfoParamsModelViews = infoParamsModelViews;
    }

    public void addInfoParamsModelView(InfoParamsModelView infoParamsModelView) {
        if (mInfoParamsModelViews == null) {
            mInfoParamsModelViews = new ArrayList<>();
        }

        mInfoParamsModelViews.add(infoParamsModelView);
    }

    public boolean isSelected() {
        return this.isSelected;
    }
}
