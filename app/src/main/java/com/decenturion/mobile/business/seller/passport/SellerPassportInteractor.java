package com.decenturion.mobile.business.seller.passport;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public class SellerPassportInteractor implements ISellerPassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;

    public SellerPassportInteractor(IResidentClient IResidentClient,
                                    IResidentRepository iResidentRepository) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
    }

    @Override
    public Observable<PassportModelView> getProfileInfo(@NonNull String residentUuid) {
        return mIResidentClient.getSelletResident(residentUuid)
                .map(SellerResidentResponse::getResult)
                .map(PassportModelView::new);
    }

}
