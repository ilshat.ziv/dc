package com.decenturion.mobile.business.header;

import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.header.model.HeaderModelView;

import rx.Observable;

public class HeaderInteractor implements IHeaderInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private ILocalistionManager mILocalistionManager;

    public HeaderInteractor(IResidentClient IResidentClient,
                            IResidentRepository iResidentRepository,
                            ILocalistionManager iLocalistionManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<HeaderModelView> getProfileInfo() {
        Observable<OrmPassportModel> obs1 = Observable.just(mIResidentRepository.getPassportInfo());
        Observable<String> obs2 = Observable.just(mIResidentRepository.getResidentInfo())
                .map(d -> {
                    String status = d.getStatus();
                    if (TextUtils.isEmpty(status)) {
                        return mILocalistionManager.getLocaleString(R.string.app_resident_status_citizen);
                    } else if (TextUtils.equals(status, "senator")) {
                        return mILocalistionManager.getLocaleString(R.string.app_resident_status_senator);
                    } else {
                        return mILocalistionManager.getLocaleString(R.string.app_resident_status_honorary);
                    }
                });

        return Observable.zip(obs1, obs2, (ormPassportModel, s) ->
                new HeaderModelView(ormPassportModel.getName(), s));
    }

}
