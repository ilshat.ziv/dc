package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trade {

    @JsonProperty("uuid")
    private String mUuid;

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("price")
    private String mPrice;

    @JsonProperty("total_price")
    private String mTotalPrice;

    @JsonProperty("receive_address")
    private String mReceiveAddress;

    @JsonProperty("refund_address")
    private String mRefundAddress;

    @JsonProperty("txid")
    private String mTxid;

    @JsonProperty("trade_address")
    private String mTradeAddress;

    @JsonProperty("frontend_id")
    private String mFrontendId;

    @JsonProperty("internal")
    private boolean isInternal;

    public Trade() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getTotalPrice() {
        return mTotalPrice;
    }

    @JsonIgnore
    public String getReceiveAddress() {
        return mReceiveAddress;
    }

    @JsonIgnore
    public String getRefundAddress() {
        return mRefundAddress;
    }

    @JsonIgnore
    public String getTxid() {
        return mTxid;
    }

    @JsonIgnore
    public String getTradeAddress() {
        return mTradeAddress;
    }

    @JsonIgnore
    public String getFrontendId() {
        return mFrontendId;
    }

    @JsonIgnore
    public boolean isInternal() {
        return isInternal;
    }
}
