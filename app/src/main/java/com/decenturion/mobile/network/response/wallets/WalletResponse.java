package com.decenturion.mobile.network.response.wallets;

import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.model.Wallet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletResponse extends BaseResponse {

    @JsonProperty("result")
    private List<Wallet> mResult;

    public WalletResponse() {
    }

    @JsonIgnore
    public List<Wallet> getResult() {
        return mResult;
    }
}
