package com.decenturion.mobile.ui.fragment.profile_v2.score.item.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.score.item.model.ScoreTokenListModelView;

public interface IScoreTokenListView extends IScreenFragmentView {

    void onBindViewData(@NonNull ScoreTokenListModelView model);

    void onSignout();
}
