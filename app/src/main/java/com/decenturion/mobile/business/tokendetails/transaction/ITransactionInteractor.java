package com.decenturion.mobile.business.tokendetails.transaction;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.traide.cancel.CancelTraideResult;
import com.decenturion.mobile.network.response.traide.confirm.ConfirmTraideResult;
import com.decenturion.mobile.network.response.traide.state.StateTraideResult;
import com.decenturion.mobile.ui.fragment.token.transaction.model.TransactionModelView;

import rx.Observable;

public interface ITransactionInteractor {

    Observable<TransactionModelView> getTokenInfo(@NonNull String traideUUID);

    @Deprecated
    Observable<ConfirmTraideResult> confirmTraide(boolean isInternal,
                                                  String receivedAddress,
                                                  String refundAddress,
                                                  String trideUUID,
                                                  String txid);

    Observable<StateTraideResult> getStateTraide(String tradeUUID);

    Observable<CancelTraideResult> cancelTraide(String tradeUUID);
}
