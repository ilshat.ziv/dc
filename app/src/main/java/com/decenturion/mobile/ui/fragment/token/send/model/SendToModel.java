package com.decenturion.mobile.ui.fragment.token.send.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.component.spinner.ISpinnerItem;

public class SendToModel implements ISpinnerItem {

    private String mTo;
    private String mKey;

    public SendToModel(@NonNull String to, @NonNull String key) {
        mTo = to;
        mKey = key;
    }

    @Override
    public String getItemName() {
        return mTo;
    }

    @Override
    public String getItemKey() {
        return mKey;
    }
}
