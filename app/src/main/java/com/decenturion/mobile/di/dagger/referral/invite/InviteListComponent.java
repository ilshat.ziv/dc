package com.decenturion.mobile.di.dagger.referral.invite;

import com.decenturion.mobile.ui.fragment.referral.invite.view.InviteListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        InviteListModule.class
})

@InviteListlScope
public interface InviteListComponent {

    void inject(InviteListFragment view);
}
