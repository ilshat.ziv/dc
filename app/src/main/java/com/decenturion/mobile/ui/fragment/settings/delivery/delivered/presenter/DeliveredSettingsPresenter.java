package com.decenturion.mobile.ui.fragment.settings.delivery.delivered.presenter;

import com.decenturion.mobile.business.settings.delivery.delivered.IDeliveredSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model.DeliveredSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.delivery.delivered.view.IDeliveredSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DeliveredSettingsPresenter extends Presenter<IDeliveredSettingsView> implements IDeliveredSettingsPresenter {

    private IDeliveredSettingsInteractor mIDeliveredSettingsInteractor;

    private DeliveredSettingsModelView mDeliveredSettingsModelView;

    public DeliveredSettingsPresenter(IDeliveredSettingsInteractor iDeliveredSettingsInteractor) {
        mIDeliveredSettingsInteractor = iDeliveredSettingsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDeliveredSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<DeliveredSettingsModelView> obs = mIDeliveredSettingsInteractor.getPassportInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DeliveredSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(DeliveredSettingsModelView model) {
        mDeliveredSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
