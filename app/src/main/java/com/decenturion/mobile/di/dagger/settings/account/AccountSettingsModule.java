package com.decenturion.mobile.di.dagger.settings.account;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.settings.account.AccountSettingsInteractor;
import com.decenturion.mobile.business.settings.account.IAccountSettingsInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.settings.account.presenter.AccountSettingsPresenter;
import com.decenturion.mobile.ui.fragment.settings.account.presenter.IAccountSettingsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountSettingsModule {

    @Provides
    @AccountSettingsScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @AccountSettingsScope
    IAccountSettingsInteractor provideIAccountSettingsInteractor(IResidentClient iResidentClient,
                                                          IResidentRepository iResidentRepository) {
        return new AccountSettingsInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @AccountSettingsScope
    IAccountSettingsPresenter provideIAccountSettingsPresenter(IAccountSettingsInteractor iAccountSettingsInteractor) {
        return new AccountSettingsPresenter(iAccountSettingsInteractor);
    }
}
