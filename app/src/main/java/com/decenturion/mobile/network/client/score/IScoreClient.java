package com.decenturion.mobile.network.client.score;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.request.score.SendTokenModel;
import com.decenturion.mobile.network.request.score.send.SendToExternalModel;
import com.decenturion.mobile.network.request.score.send.SendToMainModel;
import com.decenturion.mobile.network.request.score.send.SendToTradeModel;
import com.decenturion.mobile.network.response.score.FutureScoreTokenListResponse;
import com.decenturion.mobile.network.response.score.ScoreTokenListResponse;
import com.decenturion.mobile.network.response.score.gas.TransferGasResponse;
import com.decenturion.mobile.network.response.send.SendTokenResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface IScoreClient {

    @GET(BuildConfig.API_V + "resident/futures")
    Observable<FutureScoreTokenListResponse> getFutures(@Query("offset") long offset, @Query("limit") long limit);

    @GET(BuildConfig.API_V + "resident/coins/main")
    Observable<ScoreTokenListResponse> getMain(@Query("offset") long offset, @Query("limit") long limit);

    @GET(BuildConfig.API_V + "resident/coins/trade")
    Observable<ScoreTokenListResponse> getTrade(@Query("offset") long offset, @Query("limit") long limit);


    /**
     * Метод получения газа для отправки токенов
     *
     * @return
     */
    @GET(BuildConfig.API_V + "resident/transfer/external/fee")
    Observable<TransferGasResponse> getTransferGas();

    /**
     * Метод отправки токенов на внешний кошелек
     *
     * @param model - SendToExternalModel.java
     * @return
     */

    @POST(BuildConfig.API_V + "resident/transfer/external/create")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SendTokenResponse> sendToExternal(@Body SendToExternalModel model);

    /**
     * Метод отправки токенов на торговый счет
     *
     * @param model - SendToTradeModel.java
     * @return
     */

    @POST(BuildConfig.API_V + "resident/transfer/own/create")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SendTokenResponse> sendToTrade(@Body SendToTradeModel model);

    @POST(BuildConfig.API_V + "resident/transfer/own/create")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Observable<SendTokenResponse> sendToMain(@Body SendToMainModel model);
}
