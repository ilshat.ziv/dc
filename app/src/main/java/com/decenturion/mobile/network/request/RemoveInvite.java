package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoveInvite {

    @JsonProperty("id")
    private int mId;

    public RemoveInvite() {
    }

    @JsonIgnore
    public RemoveInvite(int id) {
        mId = id;
    }

    @JsonIgnore
    public void setId(int id) {
        mId = id;
    }
}
