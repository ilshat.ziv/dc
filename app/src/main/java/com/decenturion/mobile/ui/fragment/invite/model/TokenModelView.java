package com.decenturion.mobile.ui.fragment.invite.model;

import java.util.ArrayList;

public class TokenModelView {

    private ArrayList<Token> mTokenData;

    public TokenModelView() {
    }

    public TokenModelView(ArrayList<Token> tokenData) {
        mTokenData = tokenData;
    }

    public ArrayList<Token> getTokenData() {
        return mTokenData;
    }
}
