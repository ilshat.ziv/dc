package com.decenturion.mobile.ui.fragment.settings.mail.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.mail.MailingSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.mail.model.MailingSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.mail.presenter.IMailingSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MailingSettingsFragment extends BaseFragment implements IMailingSettingsView
        , View.OnClickListener {

    /* UI */

    @BindView(R.id.isMailingView)
    CheckBox mMailingView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    @BindView(R.id.saveChangesButtonView)
    FloatView mSaveChangesButtonView;


    /* DI */

    @Inject
    IMailingSettingsPresenter mIMailingSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        MailingSettingsComponent mailingSettingsComponent= appDelegate
                .getIDIManager()
                .plusMailingSettingsComponent();
        mailingSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_mailing_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        initToolbar();
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_mailinglists_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIMailingSettingsPresenter.bindView(this);
        mIMailingSettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIMailingSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIMailingSettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIMailingSettingsPresenter.unbindView();
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mIMailingSettingsPresenter.saveChanges(mMailingView.isChecked());
    }

    @Override
    public void onBindViewData(MailingSettingsModelView model) {
        mMailingView.setChecked(model.isSubscribe());
    }
}
