package com.decenturion.mobile.business.passport.phisical;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.delivery.skip.SkipDeliveryResult;
import com.decenturion.mobile.ui.fragment.passport.phisical.model.PhisicalPassportModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public interface IPhisicalPassportInteractor {

    Observable<PassportModelView> getProfileInfo();

    Observable<PhisicalPassportModelView> getPhisicalPassportData();

    Observable<DeliveryPassportResult> setDeliveryData(@NonNull String phone,
                                                       @NonNull String address,
                                                       @NonNull String city,
                                                       @NonNull String state,
                                                       @NonNull String country,
                                                       @NonNull String zipcode);

    Observable<PhisicalPassportModelView> genPaymentAddress(@NonNull String coin);

    Observable<SkipDeliveryResult> skipDelivery();
}
