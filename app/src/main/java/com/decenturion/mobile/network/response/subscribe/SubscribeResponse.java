package com.decenturion.mobile.network.response.subscribe;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscribeResponse extends BaseResponse {

    @JsonProperty("result")
    private boolean mResult;

    public SubscribeResponse() {
    }

    @JsonIgnore
    public boolean getResult() {
        return mResult;
    }
}
