package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdatePassportModel {

    @JsonProperty("firstname")
    private String mFirstName;

    @JsonProperty("lastname")
    private String mLastName;

    @JsonProperty("dob")
    private String mBirth;

    @JsonProperty("country")
    private String mCountry;

    @JsonProperty("city")
    private String mCity;

    @JsonProperty("sex")
    private String mSex;

    public UpdatePassportModel() {
    }

    @JsonIgnore
    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    @JsonIgnore
    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    @JsonIgnore
    public void setBirth(String birth) {
        mBirth = birth;
    }

    @JsonIgnore
    public void setCountry(String country) {
        mCountry = country;
    }

    @JsonIgnore
    public void setCity(String city) {
        mCity = city;
    }

    @JsonIgnore
    public void setSex(String sex) {
        mSex = sex;
    }

    @JsonIgnore
    public String getFirstName() {
        return mFirstName;
    }

    @JsonIgnore
    public String getLastName() {
        return mLastName;
    }

    @JsonIgnore
    public String getBirth() {
        return mBirth;
    }

    @JsonIgnore
    public String getCountry() {
        return mCountry;
    }

    @JsonIgnore
    public String getCity() {
        return mCity;
    }

    @JsonIgnore
    public String getSex() {
        return mSex;
    }
}
