package com.decenturion.mobile.network.response.csrf;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CsrfResult {

    @JsonProperty("token")
    private String mToken;

    public CsrfResult() {
    }

    @JsonIgnore
    public String getToken() {
        return mToken;
    }
}
