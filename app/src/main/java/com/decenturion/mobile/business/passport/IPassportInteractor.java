package com.decenturion.mobile.business.passport;

import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

import rx.Observable;

public interface IPassportInteractor {

    Observable<PassportModelView> getProfileInfo();
}
