package com.decenturion.mobile.ui.fragment.walkthrough;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class WalkthroughAdapter extends PagerAdapter {

    private int[] mResources;
    private List<View> mViews = new ArrayList<>();
    private LayoutInflater mLayoutInflater;

    private WalkthroughAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }

    WalkthroughAdapter(Context context, int[] resources) {
        this(context);
        mResources = resources;
    }
 
    @Override
    public int getCount() {
        return (mResources == null) ? 0 :  mResources.length;
    }
 
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ( object);
    }
 
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = (mViews.size() - 1 >= position) ? mViews.get(position) : null;
        if (itemView == null) {
            itemView = mLayoutInflater.inflate(mResources[position], container, false);
            mViews.add(position, itemView);
            container.addView(itemView);
        }
        return itemView;
    }
 
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        container.removeView((ImageView) object);
    }

    @Nullable
    @Override
    public Parcelable saveState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.saveState());
        bundle.putInt("size", getCount());
        return bundle;
    }

    @Override
    public void restoreState(@Nullable Parcelable state, @Nullable ClassLoader loader) {
        super.restoreState(state, loader);
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            int size = bundle.getInt("size");
            for (int i = 0; i < size; i++) {
                mViews.add(null);
            }
            super.restoreState(bundle.getParcelable("instanceState"), loader);
        } else {
            super.restoreState(state, loader);
        }
    }
}