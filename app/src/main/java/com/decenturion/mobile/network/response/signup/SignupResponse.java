package com.decenturion.mobile.network.response.signup;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignupResponse extends BaseResponse {

    @JsonProperty("result")
    private SignupResult mSigninResult;

    public SignupResponse() {
    }

    @JsonIgnore
    public SignupResult getSigninResult() {
        return mSigninResult;
    }
}
