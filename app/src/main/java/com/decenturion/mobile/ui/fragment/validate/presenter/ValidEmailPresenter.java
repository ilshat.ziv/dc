package com.decenturion.mobile.ui.fragment.validate.presenter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.decenturion.mobile.business.validate.IValidEmailInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.email.ConfirmEmailResponse;
import com.decenturion.mobile.network.response.ResendEmailResponse;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.validate.model.ResendEmailModelView;
import com.decenturion.mobile.ui.fragment.validate.view.IValidEmailView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ValidEmailPresenter extends Presenter<IValidEmailView> implements IValidEmailPresenter {

    private IValidEmailInteractor mIValidEmailInteractor;
    private ResendEmailModelView mResendEmailModelView;

    public ValidEmailPresenter(IValidEmailInteractor iValidEmailInteractor) {
        mIValidEmailInteractor = iValidEmailInteractor;

        mResendEmailModelView = new ResendEmailModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mResendEmailModelView = null;
    }

    @Override
    public void bindViewData(@Nullable String email) {
        showProgressView();

        mResendEmailModelView.setEmail(email);

        Observable<ResendEmailModelView> obs = Observable.concat(
                Observable.just(mResendEmailModelView),
                mIValidEmailInteractor.getEmailInfo()
        )
                .takeFirst(d -> !TextUtils.isEmpty(d.getEmail()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ResendEmailModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ResendEmailModelView model) {
        mResendEmailModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);

        if (mView != null) {
            mView.onSignin();
        }
    }

    @Override
    public void confirmEmail(String token) {
        showProgressView();

        Observable<ConfirmEmailResponse> obs = mIValidEmailInteractor.confirmEmail(token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ConfirmEmailResponse>) this::confirmEmailSuccess,
                this::confirmEmailFailure
        );
    }

    private void confirmEmailSuccess(ConfirmEmailResponse response) {
        if (mView != null) {
            mView.onConfirmedEmail();
        }
        hideProgressView();
    }

    private void confirmEmailFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void resendEmail() {
        showProgressView();

        String username = mResendEmailModelView.getEmail();

        Observable<ResendEmailResponse> obs = mIValidEmailInteractor.resendEmail(username)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ResendEmailResponse>) this::resendEmailSuccess,
                this::resendEmailFailure
        );
    }

    private void resendEmailSuccess(ResendEmailResponse resendEmailResponse) {
        hideProgressView();
    }

    private void resendEmailFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

}
