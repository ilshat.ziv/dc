package com.decenturion.mobile.ui.fragment.settings.action.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.action.view.IActionListSettingsView;

public interface IActionListSettingsPresenter extends IPresenter<IActionListSettingsView> {

    void bindViewData();

    void onBottomScrolled();

    void killAllSessions();
}
