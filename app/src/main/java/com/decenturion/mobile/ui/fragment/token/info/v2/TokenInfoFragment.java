package com.decenturion.mobile.ui.fragment.token.info.v2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.token.info.TokenInfoComponent;
import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.BaseTabLayout;

import javax.inject.Inject;

import butterknife.BindView;

public class TokenInfoFragment extends BaseFragment {

    public static final String CATEGORY = "CATEGORY";
    private String mTokenCategory;

    /* UI */

    @BindView(R.id.tabLayoutView)
    BaseTabLayout mTabView;

    @BindView(R.id.pagerView)
    ViewPager mPagerView;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenCategory = arguments.getString(CATEGORY);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TokenInfoComponent tokenInfoComponent = appDelegate
                .getIDIManager()
                .plusTokenInfoComponent();
        tokenInfoComponent.inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_token_info2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mTabView.setupWithViewPager(mPagerView);

        FragmentManager fm = getChildFragmentManager();
        TokensInfoAdapter adapter = new TokensInfoAdapter(fm, mILocalistionManager);
        mPagerView.setAdapter(adapter);

        switch (mTokenCategory) {
            case "internal" :
            case "classic": {
                mPagerView.setCurrentItem(1);
                break;
            }
            case "external" :
            case "liquid": {
                mPagerView.setCurrentItem(2);
                break;
            }
            default: {
                mPagerView.setCurrentItem(0);
            }
        }
    }

    private void initToolbar() {
        SingleFragmentActivity activity = (SingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_account_tokens_tokeninfo_title));
    }

}
