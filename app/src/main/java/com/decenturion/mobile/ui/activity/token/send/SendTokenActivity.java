package com.decenturion.mobile.ui.activity.token.send;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.token.send.view.SendTokenFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class SendTokenActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = null;
        Intent intent = getIntent();
        if (intent != null) {
            extras = intent.getExtras();
        }

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
        } else {
            iNavigationManager.navigateTo(SendTokenFragment.class, extras);
        }
    }
}
