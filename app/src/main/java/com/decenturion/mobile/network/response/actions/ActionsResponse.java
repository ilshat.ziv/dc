package com.decenturion.mobile.network.response.actions;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActionsResponse extends BaseResponse {

    @JsonProperty("result")
    private ActionsResult mResult;

    public ActionsResponse() {
    }

    @JsonIgnore
    public ActionsResult getResult() {
        return mResult;
    }
}
