package com.decenturion.mobile.business.splash;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.device.DevicePrefsOptions;
import com.decenturion.mobile.app.prefs.device.IDevicePrefsManager;
import com.decenturion.mobile.exception.UnsupportedVersionAppException;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.version.AppMinVersionResponse;
import com.decenturion.mobile.network.response.version.AppMinVersionResult;

import rx.Observable;
import rx.functions.Func1;

public class SplashInteractor implements ISplashInteractor {

    private IDeviceClient mIDeviceClient;
    private IResidentClient mIResidentClient;
    private ILocalistionManager mILocalistionManager;
    private IPrefsManager mIPrefsManager;

    public SplashInteractor(IDeviceClient iDeviceClient,
                            IResidentClient iResidentClient,
                            ILocalistionManager iLocalistionManager,
                            IPrefsManager iPrefsManager) {
        mIDeviceClient = iDeviceClient;
        mIResidentClient = iResidentClient;
        mILocalistionManager = iLocalistionManager;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<Boolean> buildApp() {
        Observable<Boolean> obs1 = Observable.just(mILocalistionManager.getLocale())
                .map(d -> d.replace("_", "-"))
                .flatMap(d -> mIResidentClient.getLocaleWords(d))
                .doOnNext(d -> mILocalistionManager.buildAlphabet(d))
                .map(d -> true);

        return mIDeviceClient.getAppMinVersion()
                .map(AppMinVersionResponse::getResult)
                .doOnNext(d -> {
                    int minVersion = d.getMinVersion();
                    IDevicePrefsManager iDevicePrefsManager = mIPrefsManager.getIDevicePrefsManager();
                    iDevicePrefsManager.setParam(DevicePrefsOptions.Keys.SUPPORT_MIN_VERSION, minVersion);

                    if (BuildConfig.VERSION_CODE < minVersion) {
                        throw new UnsupportedVersionAppException();
                    }
                })
                .onErrorReturn(throwable -> {
                    if (throwable instanceof UnsupportedVersionAppException) {
                        throw new UnsupportedVersionAppException();
                    }
                    return null;
                })
                .flatMap(d -> obs1);
    }
}
