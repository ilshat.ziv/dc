package com.decenturion.mobile.business.referral.invite.create;

import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.request.CreateInvite;
import com.decenturion.mobile.network.request.RemoveInvite;
import com.decenturion.mobile.network.response.referral.invite.create.CreateInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.create.CreateInviteResult;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResponse;
import com.decenturion.mobile.network.response.referral.invite.remove.RemoveInviteResult;

import rx.Observable;

public class CreateInviteInteractor implements ICreateInviteInteractor {

    private IReferralClient mIReferralClient;

    public CreateInviteInteractor(IReferralClient iReferralClient) {
        mIReferralClient = iReferralClient;
    }

    @Override
    public Observable<CreateInviteResult> createInvite(String price) {
        CreateInvite model = new CreateInvite(price);
        return mIReferralClient.createInvite(model)
                .map(CreateInviteResponse::getResult);
    }

    @Override
    public Observable<RemoveInviteResult> removeInvite(int idInvite) {
        RemoveInvite model = new RemoveInvite(idInvite);
        return mIReferralClient.removeInvite(model)
                .map(RemoveInviteResponse::getResult);
    }

}
