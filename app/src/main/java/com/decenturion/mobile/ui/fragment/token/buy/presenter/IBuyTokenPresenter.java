package com.decenturion.mobile.ui.fragment.token.buy.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.token.buy.view.IBuyTokenView;

public interface IBuyTokenPresenter extends IPresenter<IBuyTokenView> {

    void bindViewData(@NonNull String sellerUUID,
                      int tokenId,
                      @NonNull String paymentMethod,
                      @NonNull String category,
                      @NonNull String coinId);

    void createTraide(@Nullable String receiveAddress, boolean isInternal);

    void onSaveInstanceState(@NonNull Bundle outState,
                             @NonNull String receiveAddress,
                             boolean isInternal,
                             boolean isAgreeTerms,
                             boolean isGas);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
