package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmResidentModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmResidentDAO extends BaseDao<OrmResidentModel, Integer> {

    public OrmResidentDAO(ConnectionSource connectionSource,
            Class<OrmResidentModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
