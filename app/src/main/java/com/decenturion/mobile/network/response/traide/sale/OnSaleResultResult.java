package com.decenturion.mobile.network.response.traide.sale;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.model.TokenCoin;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnSaleResultResult {

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("currency")
    private String mCurrency;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("overflow")
    private boolean isOverflow;

    @JsonProperty("onsale_amount")
    private String mOnsaleAmount;

    @JsonProperty("price")
    private String mPrice;

    @JsonProperty("total_price")
    private String mTotalPrice;

    @JsonProperty("coin")
    private TokenCoin mCoin;

    public OnSaleResultResult() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCurrency() {
        return mCurrency;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getOnsaleAmount() {
        return mOnsaleAmount;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getTotalPrice() {
        return mTotalPrice;
    }

    @JsonIgnore
    public TokenCoin getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public boolean isOverflow() {
        return isOverflow;
    }

    @JsonIgnore
    public void setOnsaleAmount(@NonNull String onsaleAmount) {
        mOnsaleAmount = onsaleAmount;
    }
}
