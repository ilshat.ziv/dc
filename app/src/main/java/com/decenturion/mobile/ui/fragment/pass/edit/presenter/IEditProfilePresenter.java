package com.decenturion.mobile.ui.fragment.pass.edit.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.pass.edit.view.IEditProfileView;

public interface IEditProfilePresenter extends IPresenter<IEditProfileView> {

    void bindViewData();

    void saveChanges(@NonNull String firstName,
                     @NonNull String secondName,
                     long birth,
                     @NonNull String sex,
                     @NonNull String country,
                     @NonNull String city);

    void uploadPhoto(@NonNull Bitmap photo);
}
