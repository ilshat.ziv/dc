package com.decenturion.mobile.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.decenturion.mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LinkCheckBoxView extends LinearLayout {

    @Nullable
    @BindView(R.id.textView)
    LinkTextView mTextView;

    @Nullable
    @BindView(R.id.checkboxView)
    CheckBox mCheckBox;

    private Unbinder mUnbinder;

    public LinkCheckBoxView(Context context) {
        super(context);
        init();
    }

    public LinkCheckBoxView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkCheckBoxView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init() {
        if (isInitView()) {
            return;
        }
        mUnbinder = ButterKnife.bind(this, this);
    }

    protected boolean isInitView() {
        return (mTextView != null && mCheckBox != null);
    }

    public void setOnLinkClickListener(LinkTextView.OnLinkTextViewListener listener) {
        init();
        mTextView.setOnLinkClickListener(listener);
    }

    public void setText(@NonNull String text) {
        init();
        mTextView.setLinkText(text);
    }

    @Override
    public void destroyDrawingCache() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.destroyDrawingCache();
    }

    public void setChecked(boolean checked) {
        init();
        mCheckBox.setChecked(checked);

    }

    public boolean isChecked() {
        init();
        return mCheckBox.isChecked();
    }

    public void setOnCheckedChangeListener(@Nullable CompoundButton.OnCheckedChangeListener listener) {
        init();
        mCheckBox.setOnCheckedChangeListener(listener);
    }
}
