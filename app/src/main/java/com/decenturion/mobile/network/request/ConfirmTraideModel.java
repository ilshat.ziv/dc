package com.decenturion.mobile.network.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmTraideModel {
//    заполняются атрибуты, которые пришли в запросе, trade переходит в статус 'processing'

    @JsonProperty("uuid")
    private String mTraideUuid;

    @JsonProperty("internal")
    private boolean isInternal;

    @JsonProperty("receive")
    private String mReceive;

    @JsonProperty("refund")
    private String mRefund;

    @JsonProperty("txid")
    private String mTxid;

    public ConfirmTraideModel() {
    }

    @JsonIgnore
    public void setTraideUuid(String traideUuid) {
        mTraideUuid = traideUuid;
    }

    @JsonIgnore
    public void setInternal(boolean internal) {
        isInternal = internal;
    }

    @JsonIgnore
    public void setReceive(String receive) {
        mReceive = receive;
    }

    @JsonIgnore
    public void setRefund(String refund) {
        mRefund = refund;
    }

    @JsonIgnore
    public void setTxid(String txid) {
        mTxid = txid;
    }
}
