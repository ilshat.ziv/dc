package com.decenturion.mobile.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.app.snack.ISnackManager;
import com.decenturion.mobile.app.snack.SnackManager;
import com.decenturion.mobile.app.snack.TypeMessage;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.ThrooberFragmentDialog;
import com.decenturion.mobile.utils.LoggerUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements IScreenFragmentView {

    private Unbinder mUnbinder;

    private ISnackManager mISnackManager;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mISnackManager = new SnackManager(view);
        bindView(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindView();
    }

    private void bindView(@NonNull View view) {
        mUnbinder = ButterKnife.bind(this, view);
    }

    private void unbindView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

    protected AppDelegate getApplication() {
        return (AppDelegate) requireActivity().getApplication();
    }

    @Override
    public void showMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Normal, String.valueOf(message));
    }

    @Override
    public void showSuccessMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Success, String.valueOf(message));
    }

    @Override
    public void showErrorMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Error, String.valueOf(message));
    }

    @Override
    public void showWarningMessage(@NonNull String message) {
        mISnackManager.showLongSnack(TypeMessage.Warning, String.valueOf(message));
    }

    @Override
    public void showProgressView() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        Bundle bundle = new Bundle();
        bundle.putString(ThrooberFragmentDialog.TITLE, "Please wait...");
        iDialogManager.showDialog(ThrooberFragmentDialog.class, bundle);
    }

    @Override
    public void hideProgressView() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        try {
            IDialogManager iDialogManager = activity.getIDialogManager();
            iDialogManager.hideDialog(ThrooberFragmentDialog.class);
        } catch (NullPointerException e) {
            LoggerUtils.exception(e);
        }
    }

    @Override
    public IScreenActivityView getIScreenActivityView() {
        return (IScreenActivityView) getActivity();
    }
}
