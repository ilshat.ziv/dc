package com.decenturion.mobile.ui.fragment.ministry.market;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryProductOption;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MinistryOptionAdapter extends RecyclerView.Adapter<MinistryOptionAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<MinistryProductOption> mMinistryProductOptionList;

    MinistryOptionAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void replaceDataSet(List<MinistryProductOption> list) {
        mMinistryProductOptionList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mMinistryProductOptionList);
    }

    @NonNull
    @Override
    public MinistryOptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_ministry_option_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MinistryOptionAdapter.ViewHolder holder, int position) {
        final MinistryProductOption model = mMinistryProductOptionList.get(position);
        holder.bind(model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.optionNameView)
        TextView optionNameView;

        public final View view;
        public MinistryProductOption model;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public void bind(@NonNull MinistryProductOption model) {
            this.model = model;
            this.optionNameView.setText(model.getName());
        }
    }
}
