package com.decenturion.mobile.ui.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.decenturion.mobile.R;

public class BaseTabLayout extends TabLayout {

    public BaseTabLayout(Context context) {
        super(context);
        init();
    }

    public BaseTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            for (int i = 0; i < BaseTabLayout.this.getTabCount(); i++) {
                Tab tab = BaseTabLayout.this.getTabAt(i);
                assert tab != null;
                CharSequence text = tab.getText();

                View customView = tab.getCustomView();
                if (customView == null) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    TextView cutomText = (TextView) inflater.inflate(R.layout.view_tab_custom, null);
                    cutomText.setText(text);

                    tab.setCustomView(cutomText);
                } else {
                    AssetManager assets = getContext().getAssets();
                    Typeface typeface = Typeface.createFromAsset(assets, "font/Canada Type - VoxRound-Medium.otf");
                    ((TextView) customView).setTypeface(typeface);
                }
            }
        });
    }


}
