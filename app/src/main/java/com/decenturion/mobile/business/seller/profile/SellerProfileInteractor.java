package com.decenturion.mobile.business.seller.profile;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.resident.seller.SellerResidentResponse;
import com.decenturion.mobile.ui.fragment.seller.main.model.SellerProfileModelView;

import rx.Observable;

public class SellerProfileInteractor implements ISellerProfileInteractor {

    private IResidentClient mIResidentClient;
    private ILocalistionManager mIlocalisationManager;

    public SellerProfileInteractor(IResidentClient IResidentClient, ILocalistionManager ilocalisationManager) {
        mIResidentClient = IResidentClient;
        mIlocalisationManager = ilocalisationManager;
    }

    @Override
    public Observable<SellerProfileModelView> getProfileInfo(@NonNull String residentUuid) {
        return mIResidentClient.getSelletResident(residentUuid)
                .map(SellerResidentResponse::getResult)
                .map(d -> new SellerProfileModelView(d, mIlocalisationManager));
    }

}
