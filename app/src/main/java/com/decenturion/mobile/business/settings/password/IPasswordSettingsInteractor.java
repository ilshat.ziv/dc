package com.decenturion.mobile.business.settings.password;

import android.support.annotation.NonNull;

import rx.Observable;

public interface IPasswordSettingsInteractor {

    Observable<Boolean> saveNewPassword(@NonNull String currentPassword,
                                                          @NonNull String newPassword,
                                                          @NonNull String retypePassword);
}
