package com.decenturion.mobile.business.ministry.market;

import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.client.ministry.IMinistryClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.response.ministry.MinistryResponse;
import com.decenturion.mobile.network.response.ministry.MinistryResult;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.resident.ResidentResult;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryProduct;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryModelView;
import com.decenturion.mobile.ui.fragment.ministry.market.model.MinistryProductOption;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class MinistryInteractor implements IMinistryInteractor {

    private IResidentClient mIResidentClient;
    private IMinistryClient mIMinistryClient;
    private IResidentRepository mIResidentRepository;
    private ILocalistionManager mILocalistionManager;

    public MinistryInteractor(IResidentClient iResidentClient,
                              IMinistryClient iMinistryClient,
                              IResidentRepository iResidentRepository,
                              ILocalistionManager iLocalistionManager) {
        mIResidentClient = iResidentClient;
        mIMinistryClient = iMinistryClient;
        mIResidentRepository = iResidentRepository;
        mILocalistionManager = iLocalistionManager;
    }

    @Override
    public Observable<MinistryModelView> getMinistryModelView() {
        Observable<MinistryResult> obs1 = mIMinistryClient.getPriceProduct()
                .map(MinistryResponse::getResult);

        Observable<String> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(ResidentResult::getResident)
                .map(Resident::getStatus);

        return Observable.zip(obs1, obs2, (d, s) -> {
            List<MinistryProduct> list = new ArrayList<>();
            MinistryProduct p = new MinistryProduct(d.getInternal(), mILocalistionManager);
            p.setType(mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_type));
            p.setCategory("internal");
            p.setName(mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_name));
            p.setDescription(mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_note));
            list.add(p);

            p = new MinistryProduct(d.getExternal(), mILocalistionManager);
            p.setType(mILocalistionManager.getLocaleString(R.string.app_ministry_product_internal_type));
            p.setCategory("external");
            p.setName(mILocalistionManager.getLocaleString(R.string.app_ministry_product_liquid_name));
            p.setDescription(mILocalistionManager.getLocaleString(R.string.app_ministry_product_liquid_note));
            list.add(p);

            if (TextUtils.equals(s, "senator")) {
                return new MinistryModelView(list);

            } else if (!TextUtils.isEmpty(s)) {
                p = new MinistryProduct(d.getSenator(), mILocalistionManager);
                p.setType(mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_type));
                p.setCategory("senator");
                p.setName(mILocalistionManager.getLocaleString(R.string.app_ministry_product_senator_name));
                p.setDescription(mILocalistionManager.getLocaleString(R.string.app_ministry_product_senator_note));
                list.add(p);

                return new MinistryModelView(list);
            } else {
                p = new MinistryProduct(d.getHonorary(), mILocalistionManager);
                p.setType(mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_type));
                p.setCategory("honorary");
                p.setName(mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_name));
                p.setDescription(mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_note));
                list.add(p);

                p = new MinistryProduct(d.getSenator(), mILocalistionManager);
                p.setType(mILocalistionManager.getLocaleString(R.string.app_ministry_product_honorary_type));
                p.setCategory("senator");
                p.setName(mILocalistionManager.getLocaleString(R.string.app_ministry_product_senator_name));
                p.setDescription(mILocalistionManager.getLocaleString(R.string.app_ministry_product_senator_note));
                list.add(p);
            }
            return new MinistryModelView(list);
        });
    }
}
