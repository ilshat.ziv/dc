package com.decenturion.mobile.ui.fragment.profile_v2.token.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;

public interface ITokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull TokenModelView model);

    void onSignout();
}
