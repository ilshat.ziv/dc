package com.decenturion.mobile.ui.fragment.settings.wallet.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.wallets.WalletsSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.twofa.EnableTwoFAActivity;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.wallet.model.WalletsSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.wallet.presenter.IWalletsSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletsSettingsFragment extends BaseFragment implements IWalletsSettingsView
        , View.OnClickListener {

    /* UI */

    @BindView(R.id.ethWalletView)
    EditTextView mEthWalletView;

    @BindView(R.id.btcWalletView)
    EditTextView mBtcWalletView;


    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;



    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    IWalletsSettingsPresenter mIWalletsSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        WalletsSettingsComponent walletsSettingsComponent = appDelegate
                .getIDIManager()
                .plusWalletsSettingsComponent();
        walletsSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_settings_wallets, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        mBtcWalletView.showLabel();
        mBtcWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_btc_title));
        mBtcWalletView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_btc_placeholder));
        mBtcWalletView.setVisibleDivider(true);

        mEthWalletView.showLabel();
        mEthWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_eth_title));
        mEthWalletView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_eth_placeholder));
        mEthWalletView.setVisibleDivider(true);

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel("Google Authenticator");
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_security_g2fa_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_wallets_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIWalletsSettingsPresenter.bindView(this);
        if (savedInstanceState == null ||
                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIWalletsSettingsPresenter.bindViewData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mIWalletsSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIWalletsSettingsPresenter.unbindView();
        mIWalletsSettingsPresenter.onSaveInstanceState(outState,
                mBtcWalletView.getValue(),
                mEthWalletView.getValue(),
                mSecretCodeView.getValue()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIWalletsSettingsPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIWalletsSettingsPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull WalletsSettingsModelView model) {
        mBtcWalletView.setText(model.getBtcWaller());
        mEthWalletView.setText(model.getEthWallet());

        mSecretCodeView.setText(model.getSecretCode());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mIWalletsSettingsPresenter.saveWallets(
                mBtcWalletView.getValue(),
                mEthWalletView.getValue(),
                mSecretCodeView.getValue()
        );
    }

    @Override
    public void onEnableTwoFA() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, EnableTwoFAActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_ENABLE_TWOFA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_ENABLE_TWOFA && requestCode == Activity.RESULT_OK) {
            onSaveChanges();
        }
    }
}