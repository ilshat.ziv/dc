package com.decenturion.mobile.ui.fragment.settings.main.presenter;

import com.decenturion.mobile.business.settings.main.ISettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.SignoutResponse;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.main.view.ISettingsView;
import com.decenturion.mobile.ui.fragment.sign.in.model.SigninModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SettingsPresenter extends Presenter<ISettingsView> implements ISettingsPresenter {

    private ISettingsInteractor mISettingsInteractor;
    private SigninModelView mSigninModelView;

    public SettingsPresenter(ISettingsInteractor iSettingsInteractor) {
        mISettingsInteractor = iSettingsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSigninModelView = null;
    }

    @Override
    public void logout() {
        showProgressView();

        Observable<SignoutResponse> obs = mISettingsInteractor.signout()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SignoutResponse>) this::logoutSuccess,
                this::logoutFailure
        );
    }

    private void logoutFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
        if (mView != null) {
            mView.logoutSuccess();
        }
    }

    private void logoutSuccess(SignoutResponse signoutResponse) {
        hideProgressView();
        if (mView != null) {
            mView.logoutSuccess();
        }
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
