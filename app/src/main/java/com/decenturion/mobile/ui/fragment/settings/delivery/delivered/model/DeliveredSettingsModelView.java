package com.decenturion.mobile.ui.fragment.settings.delivery.delivered.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.database.model.OrmPassportModel;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.response.delivery.DeliveryPassportResult;
import com.decenturion.mobile.network.response.model.Delivery;
import com.decenturion.mobile.network.response.model.Passport;

public class DeliveredSettingsModelView {

    private String mPhone;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mCountry;
    private String mZip;

    private boolean isDeliveryPaid;

    public DeliveredSettingsModelView() {
    }

    public DeliveredSettingsModelView(@NonNull OrmPassportModel m1) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
    }

    public DeliveredSettingsModelView(@NonNull DeliveryPassportResult d) {
        Passport passport = d.getPassport();
        Delivery delivery = passport.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
    }

    public DeliveredSettingsModelView(OrmPassportModel m1, boolean deliveryPaid) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();
        this.isDeliveryPaid = deliveryPaid;
    }

    public DeliveredSettingsModelView(OrmPassportModel m1, OrmResidentModel m0, Boolean deliveryPaid) {
        Delivery delivery = m1.getDelivery();

        mPhone = delivery.getPhone();
        mAddress = delivery.getAddress();
        mCity = delivery.getCity();
        mState = delivery.getState();
        mCountry = delivery.getCountry();
        mZip = delivery.getZip();

        mPhone = validate(mPhone);
        mAddress = validate(mAddress);
        mCity = validate(mCity);
        mState = validate(mState);
        mCountry = validate(mCountry);
        mZip = validate(mZip);

        this.isDeliveryPaid = m0.isActive() && !m0.isInvited() || deliveryPaid;
    }

    private String validate(String param) {
        return TextUtils.isEmpty(param) ? "-" : param;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getState() {
        return mState;
    }

    public String getZip() {
        return mZip;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public boolean isDeliveryPaid() {
        return this.isDeliveryPaid;
    }
}
