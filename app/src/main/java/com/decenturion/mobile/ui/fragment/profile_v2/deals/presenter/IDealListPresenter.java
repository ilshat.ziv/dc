package com.decenturion.mobile.ui.fragment.profile_v2.deals.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.view.IDealListView;

public interface IDealListPresenter extends IPresenter<IDealListView> {

    void bindViewData();

    void onBottomScrolled();
}
