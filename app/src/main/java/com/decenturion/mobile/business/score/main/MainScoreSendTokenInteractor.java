package com.decenturion.mobile.business.score.main;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.request.CheckDcntWalletModel;
import com.decenturion.mobile.network.request.score.SendTokenModel;
import com.decenturion.mobile.network.request.score.send.SendToExternalModel;
import com.decenturion.mobile.network.request.score.send.SendToTradeModel;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.model.TransferGas;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.network.response.score.gas.TransferGasResponse;
import com.decenturion.mobile.network.response.send.SendTokenResponse;
import com.decenturion.mobile.network.response.send.SendTokenResult;
import com.decenturion.mobile.network.response.wallets.CheckWalletResponse;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendBalanceModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendOptionModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendTokenModelView;
import com.decenturion.mobile.utils.DateTimeUtils;

import rx.Observable;

public class MainScoreSendTokenInteractor implements IMainScoreSendTokenInteractor {

    private IResidentClient mIResidentClient;
    private IScoreClient mIScoreClient;
    private ITokenRepository mITokenRepository;

    public MainScoreSendTokenInteractor(IResidentClient IResidentClient,
                                        IScoreClient iScoreClient,
                                        ITokenRepository iTokenRepository) {
        mIResidentClient = IResidentClient;
        mIScoreClient = iScoreClient;
        mITokenRepository = iTokenRepository;
    }

    @Override
    public Observable<MainScoreSendTokenModelView> getCoinInfo(@NonNull MainScoreSendTokenModelView model) {
        Observable<OrmTokenModel> obs1 = Observable.just(model.getTokenId())
                .map(d -> mITokenRepository.getTokenById(d));

        Observable<MainScoreSendTokenModelView> obs2 = Observable.just(model);

        Observable<TransferGas> obs3 = mIScoreClient.getTransferGas()
                .map(TransferGasResponse::getResult);

        Observable<MainScoreSendBalanceModel> obs4 = getBalance();

        return Observable.zip(obs1, obs2, obs3, obs4,
                (ormTokenModel, sendTokenModelView, transferGas, balance) -> {
                    sendTokenModelView.setCoinCategory(ormTokenModel.getCategory());
                    sendTokenModelView.setTokenName(ormTokenModel.getFullSymbolName());
                    CoinTypeModel coinTypeModel = ormTokenModel.getCoinTypeModel();
                    sendTokenModelView.setCoinUUID(coinTypeModel.getUuid());
                    sendTokenModelView.setSymbol(coinTypeModel.getSymbol());

                    sendTokenModelView.setMainScoreSendBalanceModel(balance);

                    String freeCooldown = ormTokenModel.getFreeCooldown();
                    long datetime = DateTimeUtils.getDatetime(freeCooldown, DateTimeUtils.Format.FORMAT_UTC);
                    MainScoreSendOptionModel option = new MainScoreSendOptionModel(datetime, transferGas);
                    sendTokenModelView.setMainScoreSendOptionModel(option);

                    return sendTokenModelView;
                });
    }

    private Observable<MainScoreSendBalanceModel> getBalance() {
        return mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    double balance = Double.parseDouble(d.getEthBalance());
                    return new MainScoreSendBalanceModel(d.getPassportCoin(), balance, resident.getWalletNum());
                });
    }

    @Override
    public Observable<SendTokenResult> sendToCitizen(
            @NonNull String coinUUID,
            @NonNull String coinCategory,
            @NonNull String dcntWallet,
            @NonNull String amount,
            @NonNull String method,
            @NonNull String twofa) {

        SendTokenModel model = new SendTokenModel();
        model.setCoin(coinUUID);
        model.setCategory(coinCategory);
        model.setDCNTWallet(dcntWallet);
        model.setAmount(amount);
        model.setType(method);
        model.setTwoFa(twofa);

        CheckDcntWalletModel model1 = new CheckDcntWalletModel(dcntWallet);
        return mIResidentClient.checkDCNTWallet(model1)
                .map(CheckWalletResponse::getResult)
                .map(d -> {
                    if (!d.isPresence()) {
                        throw new RuntimeException("Address is not a valid ERC-20");
                    }

                    return true;
                })
                .flatMap(d -> mIResidentClient.sendToken(model))
                .map(SendTokenResponse::getResult);
    }

    @Override
    public Observable<SendTokenResult> sendToExternal(@NonNull SendToExternalModel model) {
        return mIScoreClient.sendToExternal(model)
                .map(SendTokenResponse::getResult);
    }

    @Override
    public Observable<SendTokenResult> sendToTrade(@NonNull SendToTradeModel model) {
        return mIScoreClient.sendToTrade(model)
                .map(SendTokenResponse::getResult);
    }
}
