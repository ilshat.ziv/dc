package com.decenturion.mobile.business.support;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.support.model.SupportMessage;

import rx.Observable;

public interface ISupportInteractor {

    Observable<Boolean> sendMessage(@NonNull SupportMessage model);
}
