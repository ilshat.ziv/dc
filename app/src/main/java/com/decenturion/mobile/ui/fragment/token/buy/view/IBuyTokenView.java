package com.decenturion.mobile.ui.fragment.token.buy.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.traide.create.CreateTraideResult;
import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.token.buy.model.BuyTokenModelView;

public interface IBuyTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull BuyTokenModelView model);

    void onTraideCreated(@NonNull CreateTraideResult result);
}
