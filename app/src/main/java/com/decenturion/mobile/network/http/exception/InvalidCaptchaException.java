package com.decenturion.mobile.network.http.exception;

import com.decenturion.mobile.exception.AppException;

public class InvalidCaptchaException extends AppException {

    public InvalidCaptchaException() {
        super("Captcha is not valid");
    }
}
