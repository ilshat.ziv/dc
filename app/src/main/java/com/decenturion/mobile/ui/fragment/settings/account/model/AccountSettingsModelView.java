package com.decenturion.mobile.ui.fragment.settings.account.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmResidentModel;

public class AccountSettingsModelView {

    private String mUsername;

    public AccountSettingsModelView() {
    }

    public AccountSettingsModelView(OrmResidentModel d) {
        mUsername = d.getEmail();
    }

    public void setUsername(@NonNull String username) {
        mUsername = username;
    }

    public String getUsername() {
        return mUsername;
    }

}
