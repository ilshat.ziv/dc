package com.decenturion.mobile.app.localisation;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.device.DevicePrefsOptions;
import com.decenturion.mobile.app.prefs.device.IDevicePrefsManager;
import com.decenturion.mobile.app.prefs.locale.ILocalePrefsManager;
import com.decenturion.mobile.app.prefs.locale.LocalePrefsOptions;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class LocalisationManager implements ILocalistionManager {

    private final String PREFIX_DEFAULT = "default.";

    private Context mContext;
    private IResourceManager mIResourceManager;
    private ILocalePrefsManager mILocalePrefsManager;
    private IDevicePrefsManager mIDevicePrefsManager;

    public LocalisationManager(@NonNull Context context,
                               @NonNull IResourceManager iResourceManager,
                               @NonNull IPrefsManager iPrefsManager) {
        mContext = context;
        mIResourceManager = iResourceManager;
        mILocalePrefsManager = iPrefsManager.getILocalePrefsManager();
        mIDevicePrefsManager = iPrefsManager.getIDevicePrefsManager();

        initArgs(context);
    }

    private void initArgs(@NonNull Context context) {
        int lastVersion = mILocalePrefsManager.getParam(LocalePrefsOptions.Keys.VERSION_DEFAULT_LOCALISATION, 0);
        if (lastVersion == BuildConfig.VERSION_CODE) {
            return;
        }
        String localeKey = ILocalistionManager.DEFAULT_LOCALISATION;
        String json = FileUtils.loadJsonFromAssets(context, "data/text_" + localeKey + ".json");
        try {
            JSONObject data = new JSONObject(json);

            mILocalePrefsManager.clear();
            String packageName = mContext.getPackageName();

            Iterator<?> keys = data.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = (String) data.get(key);
                value = proccessValue(value);

                mILocalePrefsManager.setParam(packageName + ":string/" + PREFIX_DEFAULT + key, value);
            }
            mILocalePrefsManager.setParam(LocalePrefsOptions.Keys.VERSION_DEFAULT_LOCALISATION, BuildConfig.VERSION_CODE);
        } catch (JSONException e) {
            LoggerUtils.exception(e);
        }
    }

    private String getLocale(Context context) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale locale = configuration.locale;
        return locale.toString();
    }

    @Override
    public String getLocaleString(@NonNull String name) {
        name = BuildConfig.APPLICATION_ID + ":string/" + name;
        String result = mILocalePrefsManager.getParam(name, "");
        if (TextUtils.isEmpty(result)) {
            name = name.replace(":string/", ":string/" + PREFIX_DEFAULT);
            result = mILocalePrefsManager.getParam(name, "");
        }
        return result;
    }

    @Override
    public String getLocaleString(int resourceId) {
        String name = mIResourceManager.getResourceName(resourceId);
        String result = mILocalePrefsManager.getParam(name, "");
        if (TextUtils.isEmpty(result)) {
            name = name.replace(":string/", ":string/" + PREFIX_DEFAULT);
            result = mILocalePrefsManager.getParam(name, "");
        }
        return result;
    }

    @Override
    public String getLocaleResource(@NonNull String assetsNameFile) {
        String locale = getLocale();
        String s = FileUtils.loadJsonFromAssets(mContext, assetsNameFile + "_" + locale + ".json");
        if (TextUtils.isEmpty(s)) {
            s = FileUtils.loadJsonFromAssets(mContext, assetsNameFile + "_" + ILocalistionManager.DEFAULT_LOCALISATION + ".json");
        }
        return s;
    }

    @Override
    public void buildAlphabet(@NonNull LinkedHashMap<String, String> hm) {
        String packageName = mContext.getPackageName();
        for (Map.Entry<String, String> entry : hm.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            value = proccessValue(value);

            mILocalePrefsManager.setParam(packageName + ":string/" + key, value);
        }
        mIDevicePrefsManager.setParam(DevicePrefsOptions.Keys.APP_LOCALE, getLocale());
    }

    private String proccessValue(@NonNull String value) {
        if (value.contains("</")){
            value = value.replace("\n", "<br/>");

            value = value.replace("<h>", "<font color='#ffffff'>");
            value = value.replace("</h>", "</font>");

            value = value.replace("<l>", "<u>");
            value = value.replace("</l>", "</u>");
        }
        return value;
    }

    @Override
    public boolean isLocaleChanged() {
        String devLocale = getLocale();
        String appLocale = mIDevicePrefsManager.getParam(DevicePrefsOptions.Keys.APP_LOCALE, "");
        return !TextUtils.equals(devLocale, appLocale);
    }

    @Override
    public String getLocale() {
        return getLocale(mContext);
    }
}
