package com.decenturion.mobile.ui.fragment.referral.invite.create.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.referral.invite.create.CreateInviteComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.component.ChangeComponentListener;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.invite.create.presenter.ICreateInvitePresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.LoggerUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateInviteFragment extends BaseFragment implements ICreateInviteView,
        View.OnClickListener,
        ChangeComponentListener {

    public static final String INVITE_URL = "INVITE_URL";

    /* UI */

    @BindView(R.id.priceView)
    EditTextView mPriceView;

    @BindView(R.id.recomendationPriceTextView)
    LocalizedTextView mRecomendationPriceTextView;

    @BindView(R.id.createInviteButtonView)
    LinearLayout mCreateInviteButtonView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;

    /* DI */

    @Inject
    ILocalistionManager mILocalistionManager;

    @Inject
    ICreateInvitePresenter mIAcquisitionPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        CreateInviteComponent component = appDelegate
                .getIDIManager()
                .plusCreateInviteComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_create_invite, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mCreateInviteButtonView.setEnabled(false);

        mPriceView.setLabel(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_price_title));
        mPriceView.setHint(mILocalistionManager.getLocaleString(R.string.app_referral_acquisition_price_placeholder));
        mPriceView.setTypeInput(InputType.TYPE_CLASS_PHONE);
        mPriceView.setChangeDataModelListener(this);

        mRecomendationPriceTextView.setText(mILocalistionManager.getLocaleString(R.string.app_referral_invite_recomendation));

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_referral_invite_title));
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        iToolbarController.setNavigationOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIAcquisitionPresenter.bindView(this);
    }

    @Override
    public void onCreateInviteSuccess(@NonNull String url) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();

        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            Intent data = new Intent();
            data.putExtra(INVITE_URL, url);
            targetFragment.onActivityResult(IMainActivity.ACTION_CREATE_INVITE, Activity.RESULT_OK, data);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIAcquisitionPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIAcquisitionPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIAcquisitionPresenter.unbindView();
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIAcquisitionPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onChangeComponentData(View view) {
        String value = mPriceView.getValue();
        double v = proccessPrice(value);
        boolean b = !TextUtils.isEmpty(value) && v > 0;

        mCreateInviteButtonView.setEnabled(b);
    }

    private double proccessPrice(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            LoggerUtils.exception(e);
        }

        return 0;
    }

    @OnClick(R.id.createInviteButtonView)
    protected void onCreateInvite() {
        mIAcquisitionPresenter.createInvite(mPriceView.getValue());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
