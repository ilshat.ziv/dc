package com.decenturion.mobile.ui.fragment.profile_v2.token.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.business.token.ITokenInteractor;
import com.decenturion.mobile.network.http.exception.AccessException;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.view.ITokenView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TokenPresenter extends Presenter<ITokenView> implements ITokenPresenter {

    private ITokenInteractor mITokenInteractor;
    private TokenModelView mTokenModelView;

    public TokenPresenter(ITokenInteractor iTokenInteractor) {
        mITokenInteractor = iTokenInteractor;
        mTokenModelView = new TokenModelView();
    }

    @Override
    public void bindViewData() {
        Observable<TokenModelView> obs = mITokenInteractor.getTokenModelView()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<TokenModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(@NonNull TokenModelView model) {
        mTokenModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        } else if (throwable instanceof AccessException) {
            if (mView != null) {
                mView.onSignout();
            }
        }
        showErrorView(throwable);
    }
}
