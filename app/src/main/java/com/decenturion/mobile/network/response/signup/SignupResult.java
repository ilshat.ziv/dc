package com.decenturion.mobile.network.response.signup;

import com.decenturion.mobile.network.response.model.Resident;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignupResult extends Resident {

    public SignupResult() {
    }
}
