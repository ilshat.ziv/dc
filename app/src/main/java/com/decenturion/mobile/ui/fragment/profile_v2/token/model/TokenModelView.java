package com.decenturion.mobile.ui.fragment.profile_v2.token.model;

import java.util.List;

public class TokenModelView {

    private String mTypePay = "btc";
    private List<TokenModel> mTokentModelList;

    public TokenModelView() {
    }

    public TokenModelView(List<TokenModel> d) {
        mTokentModelList = d;
    }

    public List<TokenModel> getTokentModelList() {
        return mTokentModelList;
    }

    public void setTypePay(String typePay) {
        mTypePay = typePay;
    }

    public String getTypePay() {
        return mTypePay;
    }
}
