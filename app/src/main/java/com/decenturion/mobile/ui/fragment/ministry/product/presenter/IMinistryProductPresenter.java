package com.decenturion.mobile.ui.fragment.ministry.product.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.ministry.product.view.IMinistryProductView;

import rx.Observable;

public interface IMinistryProductPresenter extends IPresenter<IMinistryProductView> {

    void bindViewData(@NonNull String category);

    void bindViewData(Observable<String> amount, Observable<String> coin);

    void getPaymentAddress(@NonNull String category, @NonNull String coin, @NonNull String amount);
}
