package com.decenturion.mobile.di.dagger.passport.activation;

import android.content.Context;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.passport.activation.ActivationPassportInteractor;
import com.decenturion.mobile.business.passport.activation.IActivationPassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.activation.presenter.ActivationPassportPresenter;
import com.decenturion.mobile.ui.fragment.passport.activation.presenter.IActivationPassportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivationPassportModule {

    @Provides
    @ActivationPassportScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @ActivationPassportScope
    IActivationPassportInteractor provideIActivationPassportInteractor(IResidentClient iResidentClient,
                                                                       IResidentRepository iResidentRepository,
                                                                       IPrefsManager iPrefsManager) {
        return new ActivationPassportInteractor(iResidentClient, iResidentRepository, iPrefsManager);
    }

    @Provides
    @ActivationPassportScope
    IActivationPassportPresenter provideIActivationPassportPresenter(Context context,
                                                                     IActivationPassportInteractor iActivationPassportInteractor) {
        return new ActivationPassportPresenter(context, iActivationPassportInteractor);
    }
}
