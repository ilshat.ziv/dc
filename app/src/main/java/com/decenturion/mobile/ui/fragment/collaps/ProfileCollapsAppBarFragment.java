package com.decenturion.mobile.ui.fragment.collaps;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.component.ProfileImageView;
import com.decenturion.mobile.ui.dialog.fullscreen.ImageDialog;
import com.decenturion.mobile.ui.fragment.BaseFragment;

import butterknife.BindView;

public class ProfileCollapsAppBarFragment extends BaseFragment implements ProfileImageView.OnPhotoViewListener {

    public static final String URL_PHOTO = "URL_PHOTO";
    public static final String URL_MIN_PHOTO = "URL_MIN_PHOTO";

    /* UI */

    @BindView(R.id.photoView)
    ProfileImageView mProfileImageView;

    private String mUrlPhoto;
    private String mUrlMinPhoto;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
        initArgs();
    }

    private void initArgs() {
        final Bundle arguments = getArguments();
        if (arguments != null) {
            mUrlPhoto = arguments.getString(URL_PHOTO);
            mUrlMinPhoto = arguments.getString(URL_MIN_PHOTO);
        }
    }

    private void injectToDagger() {
//        AppDelegate appDelegate = getApplication();
//        PassportComponent passportComponent = appDelegate
//                .getIDIManager()
//                .plusPassportComponent();
//        passportComponent.inject(this);

//        setIToastManager(mIToastManager);
//        mIPassportPresenter.bindView(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_collapsed_content_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
        if (TextUtils.isEmpty(mUrlMinPhoto)) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_spider_man);
            mProfileImageView.addBitmap(bitmap);
        } else {
            mProfileImageView.addUrlImage(mUrlMinPhoto);
        }
    }

    private void initUi() {
        mProfileImageView.setOnPhotoViewListener(this);
        mProfileImageView.simpleMode();
    }

    @Override
    public void onUploadPhoto() {
    }

    @Override
    public void onShowPhoto(Bitmap bitmap) {
        FragmentManager fragmentManager = requireFragmentManager();
        ImageDialog.show(fragmentManager, bitmap, null);
    }

    @Override
    public void onShowPhoto(String urlImage) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        ImageDialog.show(fragmentManager, null, urlImage);
    }

    @Override
    public void onPhotoUploaded(@NonNull Bitmap bitmap) {
    }
}
