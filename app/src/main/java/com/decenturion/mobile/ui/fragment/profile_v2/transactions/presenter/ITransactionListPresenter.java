package com.decenturion.mobile.ui.fragment.profile_v2.transactions.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.view.ITransactionListView;

public interface ITransactionListPresenter extends IPresenter<ITransactionListView> {

    void bindViewData();

    void onBottomScrolled();
}
