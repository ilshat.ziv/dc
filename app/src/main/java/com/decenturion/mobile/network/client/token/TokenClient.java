package com.decenturion.mobile.network.client.token;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class TokenClient extends DHttpClient<ITokenClient> {

    public TokenClient(NetworkModule module) {
        super(module);
    }
}
