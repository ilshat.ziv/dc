package com.decenturion.mobile.ui.component;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.View;

public class SavedState extends View.BaseSavedState {

    private SparseArray mChildrenStates;

    public static final Parcelable.ClassLoaderCreator<SavedState> CREATOR
            = new Parcelable.ClassLoaderCreator<SavedState>() {
        @Override
        public SavedState createFromParcel(Parcel source, ClassLoader loader) {
            return new SavedState(source, loader);
        }

        @Override
        public SavedState createFromParcel(Parcel source) {
            return createFromParcel(source, null);
        }

        public SavedState[] newArray(int size) {
            return new SavedState[size];
        }
    };

    public SavedState(Parcelable superState) {
        super(superState);
    }

    private SavedState(Parcel in, ClassLoader classLoader) {
        super(in);
        mChildrenStates = in.readSparseArray(classLoader);
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeSparseArray(mChildrenStates);
    }

    public SparseArray getChildrenStates() {
        return mChildrenStates;
    }

    public void setChildrenStates(SparseArray childrenStates) {
        mChildrenStates = childrenStates;
    }

}
