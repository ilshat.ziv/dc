package com.decenturion.mobile.ui.fragment.ministry.market.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.model.MinistryItem;
import com.decenturion.mobile.network.response.model.MinistryItemPrice;

import java.util.List;

public class MinistryProduct {

    private String mCategory;
    private String mType;
    private String mName;
    private String mDescription;

    private List<MinistryProductOption> mOptionList;

    private String mPrice;

    public MinistryProduct(@NonNull MinistryItem d, @NonNull ILocalistionManager iLocalistionManager) {
        MinistryItemPrice usdt = d.getUsdt();
        String string = iLocalistionManager.getLocaleString(R.string.app_ministry_product_internal_pricetoday);
        mPrice = string.replace("{price}", usdt.getPrice());
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getPrice() {
        return mPrice;
    }

    public List<MinistryProductOption> getOptionList() {
        return mOptionList;
    }

    public void setOptionList(List<MinistryProductOption> optionList) {
        mOptionList = optionList;
    }
}
