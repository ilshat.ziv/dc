package com.decenturion.mobile.ui.fragment.seller.token.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.token.model.TokenModelView;
import com.decenturion.mobile.ui.fragment.seller.token.model.CoinModelView;

public interface ISellerTokenView extends IScreenFragmentView {

    void onBindViewData(@NonNull CoinModelView model, @NonNull TokenModelView model2);
}
