package com.decenturion.mobile.ui.fragment.profile.passport.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;

public interface IPassportView extends IScreenFragmentView {

    void onBindViewData(@NonNull SexModelView model,
                        @NonNull CountryModelView model1,
                        @NonNull PassportModelView model2);

    void onSignout();
}
