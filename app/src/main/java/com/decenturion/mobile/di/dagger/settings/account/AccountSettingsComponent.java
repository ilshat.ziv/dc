package com.decenturion.mobile.di.dagger.settings.account;

import com.decenturion.mobile.ui.fragment.settings.account.view.AccountSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        AccountSettingsModule.class
})

@AccountSettingsScope
public interface AccountSettingsComponent {

    void inject(AccountSettingsFragment view);
}
