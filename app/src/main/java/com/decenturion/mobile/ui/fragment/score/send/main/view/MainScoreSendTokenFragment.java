package com.decenturion.mobile.ui.fragment.score.send.main.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.localisation.view.LocalizedButtonView;
import com.decenturion.mobile.app.localisation.view.LocalizedCheckBox;
import com.decenturion.mobile.app.localisation.view.LocalizedTextView;
import com.decenturion.mobile.di.dagger.score.main.MainScoreSendTokenComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.activity.twofa.EnableTwoFAActivity;
import com.decenturion.mobile.ui.component.BuyTokenView;
import com.decenturion.mobile.ui.component.EditTextView;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.spinner.SpinnerView;
import com.decenturion.mobile.ui.dialog.fullscreen.token.ScoreSendTokenSuccessDialog;
import com.decenturion.mobile.ui.dialog.fullscreen.token.SendTokenFailDialog;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.score.send.main.MethodSendToken;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendBalanceModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendOptionModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendToModel;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendToModelView;
import com.decenturion.mobile.ui.fragment.score.send.main.model.MainScoreSendTokenModelView;
import com.decenturion.mobile.ui.fragment.score.send.main.presenter.IMainScoreSendTokenPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.utils.DateTimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MainScoreSendTokenFragment extends BaseFragment implements IMainScoreSendTokenView {

    public static final String TOKEN_ID = "TOKEN_ID";
    public static final String TOKEN_SCORE_TYPE = "TOKEN_SCORE_TYPE";

    private int mTokenId;
    private String mTokenName;

    /* UI */

    @BindView(R.id.nameTokenView)
    TextView mNameTokenView;

    @BindView(R.id.sendToView)
    SpinnerView mSendToView;

    @BindView(R.id.walletAddressView)
    EditTextView mDcnWalletView;

    @BindView(R.id.amountView)
    BuyTokenView mAmountView;


    @BindView(R.id.optionSendInfoView)
    LinearLayout mOptionSendInfoView;

    @BindView(R.id.addressReceiveValueView)
    TextView mAddressReceiveValueView;

    @BindView(R.id.gasValueView)
    TextView mGasValueView;

    @BindView(R.id.balanceLabelView)
    TextView mBalanceLabelView;

    @BindView(R.id.balanceValueView)
    TextView mBalanceValueView;


    @BindView(R.id.methodSendView)
    LinearLayout mMethodSendView;

    @BindView(R.id.simpleMethodSendTextView)
    LocalizedTextView mSimpleMethodSendTextView;

    @BindView(R.id.expressMethodSendButtonView)
    LocalizedCheckBox mExpressMethodSendButtonView;



    @BindView(R.id.secretCodeView)
    EditTextView mSecretCodeView;


    @BindView(R.id.controlsView)
    FloatView mControlsView;

    @BindView(R.id.sendTokensButtonView)
    LocalizedButtonView mSendTokensButtonView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    IMainScoreSendTokenPresenter mISendTokenPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();

        FragmentActivity activity = requireActivity();
        mIFloatViewManager = new FloatViewManager(activity);
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenId = arguments.getInt(TOKEN_ID);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        MainScoreSendTokenComponent component = appDelegate
                .getIDIManager()
                .plusMainScoreSendTokenComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_score_main_token_send, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        initToolbar();

        mSendToView.showLabel();
        mSendToView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_main_sendto_title));
        mSendToView.setVisibleDivider(true);
        mSendToView.setOnSpinnerItemClickListener(iSpinnerItem -> {
            String itemKey = iSpinnerItem.getItemKey();
            MainScoreSendToModel.Key key = MainScoreSendToModel.Key.valueOf(itemKey);
            switch (key) {
                case TOCITIZEN : {
                    mDcnWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_main_send_wallet_dcnt_title));
                    mDcnWalletView.setVisibility(View.VISIBLE);

                    mAmountView.setVisibility(View.VISIBLE);
                    mSecretCodeView.setVisibility(View.VISIBLE);

                    mMethodSendView.setVisibility(View.VISIBLE);
                    mOptionSendInfoView.setVisibility(View.GONE);
                    break;
                }
                case TOEXTERNAL : {
                    mDcnWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_main_send_wallet_title));
                    mDcnWalletView.setVisibility(View.VISIBLE);
                    mAmountView.setVisibility(View.VISIBLE);
                    mSecretCodeView.setVisibility(View.VISIBLE);

                    mMethodSendView.setVisibility(View.GONE);
                    mOptionSendInfoView.setVisibility(View.VISIBLE);
                    break;
                }
                case TOTRADE : {
                    mDcnWalletView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_main_send_wallet_title));
                    mDcnWalletView.setVisibility(View.GONE);
                    mAmountView.setVisibility(View.VISIBLE);
                    mSecretCodeView.setVisibility(View.VISIBLE);

                    mMethodSendView.setVisibility(View.VISIBLE);
                    mOptionSendInfoView.setVisibility(View.GONE);
                    break;
                }
            }
        });

        mDcnWalletView.showLabel();
        mDcnWalletView.editMode();
        mDcnWalletView.setHint(mILocalistionManager.getLocaleString(R.string.app_score_main_send_wallet_placeholder));
        mDcnWalletView.setVisibleDivider(true);

        mAmountView.showLabel();
        mAmountView.setLabel(mILocalistionManager.getLocaleString(R.string.app_score_main_send_amount_title));
        mAmountView.setHint(mILocalistionManager.getLocaleString(R.string.app_score_main_send_amount_placeholder));
        mAmountView.editMode();
        mAmountView.setVisibleDivider(true);

        mSecretCodeView.showLabel();
        mSecretCodeView.setLabel("Google Authenticator");
        mSecretCodeView.setHint(mILocalistionManager.getLocaleString(R.string.app_account_send_code_placeholder));
        mSecretCodeView.setTypeInput(InputType.TYPE_CLASS_NUMBER);
        mSecretCodeView.editMode();
        mSecretCodeView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_score_main_send_title));
        iToolbarController.setNavigationOnClickListener(v -> {
            ISingleFragmentActivity activity1 = (ISingleFragmentActivity) requireActivity();
            INavigationManager iNavigationManager = activity1.getINavigationManager();
            iNavigationManager.navigateToBack();
        });
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mISendTokenPresenter.bindView(this);
        mISendTokenPresenter.bindViewData(mTokenId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mISendTokenPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mISendTokenPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mISendTokenPresenter.unbindView();
        mISendTokenPresenter.onSaveInstanceState(outState,
                mDcnWalletView.getValue(),
                mAmountView.getValue(),
                mSecretCodeView.getValue()
        );
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mISendTokenPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull MainScoreSendTokenModelView model,
                               @NonNull MainScoreSendToModelView model1) {
        mTokenName = model.getTokenName();

        mNameTokenView.setText(model.getTokenName());

        mDcnWalletView.setText(model.getDcntWallet());
        mAmountView.setText(model.getAmount());
        mAmountView.setCoin(model.getSymbol());
        mSecretCodeView.setText(model.getTwoFA());

        mSendToView.setData(model1.getSendToData());

        mExpressMethodSendButtonView.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_main_send_express));
            } else {
                mSendTokensButtonView.setText(mILocalistionManager.getLocaleString(R.string.app_score_main_send_normal));
            }
        });

        MainScoreSendOptionModel model2 = model.getMainScoreSendOptionModel();
        long date = model2.getFreeCoolDown();
        if (date > System.currentTimeMillis()) {
            String dateFormat = DateTimeUtils.getDateFormat(date, DateTimeUtils.Format.DEAL_DATETIME);
            String string = mILocalistionManager.getLocaleString(R.string.app_score_main_send_normal_warning);
            mSimpleMethodSendTextView.setText(string.replace("{datetime}", dateFormat));
            mSimpleMethodSendTextView.setVisibility(View.VISIBLE);
            mExpressMethodSendButtonView.setChecked(true);
            mExpressMethodSendButtonView.setEnabled(false);
        }

        // TOEXTERNAL
        MainScoreSendBalanceModel balanceModel = model.getMainScoreSendBalanceModel();
        mAddressReceiveValueView.setText(balanceModel.getAddress());
        String string = mILocalistionManager.getLocaleString(R.string.app_score_main_send_balance_title);
        mBalanceLabelView.setText(string.replace("{currency}", balanceModel.getCoin()));

        String balance = balanceModel.getBalance() + " " + balanceModel.getCoin();
        mBalanceValueView.setText(balance);

        String gas = model2.getGas() + " " + model2.getCurrence();
        mGasValueView.setText(gas);
    }

    @OnClick(R.id.sendTokensButtonView)
    protected void onSendTokens() {
        String selection = mSendToView.getSelection();
        MainScoreSendToModel.Key key = MainScoreSendToModel.Key.valueOf(selection);
        switch (key) {
            case TOCITIZEN : {
                mISendTokenPresenter.sendToken(
                        mDcnWalletView.getValue(),
                        mAmountView.getValue(),
                        mExpressMethodSendButtonView.isChecked() ? MethodSendToken.URGENT : MethodSendToken.NORMAL,
                        mSecretCodeView.getValue()
                );
                break;
            }
            case TOEXTERNAL : {
                mISendTokenPresenter.sendToExternal(
                        mDcnWalletView.getValue(),
                        mAmountView.getValue(),
                        mSecretCodeView.getValue()
                );
                break;
            }
            case TOTRADE : {
                mISendTokenPresenter.sendToTrade(
                        mAmountView.getValue(),
                        mExpressMethodSendButtonView.isChecked() ? MethodSendToken.URGENT : MethodSendToken.NORMAL,
                        mSecretCodeView.getValue()
                );
                break;
            }
        }
    }

    @Override
    public void onEnableTwoFA() {
        FragmentActivity activity = requireActivity();
        Intent intent = new Intent(activity, EnableTwoFAActivity.class);
        activity.startActivityForResult(intent, IMainActivity.ACTION_ENABLE_TWOFA);
    }

    @Override
    public void onSendTokenSuccess() {
        Bundle bundle = new Bundle();
        String selection = mSendToView.getSelection();
        MainScoreSendToModel.Key key = MainScoreSendToModel.Key.valueOf(selection);
        bundle.putInt(ScoreSendTokenSuccessDialog.TYPE_TRANSACTION, key.ordinal());
        bundle.putString(ScoreSendTokenSuccessDialog.COIN, mTokenName);
        bundle.putString(ScoreSendTokenSuccessDialog.AMOUNT, mAmountView.getValue());
        ScoreSendTokenSuccessDialog.show(requireFragmentManager(), bundle);
    }

    @Override
    public void onSendTokenFail() {
        SendTokenFailDialog.show(requireFragmentManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMainActivity.ACTION_ENABLE_TWOFA && resultCode == Activity.RESULT_OK) {
            onSendTokens();
        }
    }

}