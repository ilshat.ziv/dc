package com.decenturion.mobile.ui.fragment.settings.account.presenter;

import android.text.TextUtils;

import com.decenturion.mobile.business.settings.account.IAccountSettingsInteractor;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.account.model.AccountSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.account.view.IAccountSettingsView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class AccountSettingsPresenter extends Presenter<IAccountSettingsView> implements IAccountSettingsPresenter {

    private IAccountSettingsInteractor mIAccountSettingsInteractor;
    private AccountSettingsModelView mSigninModelView;

    public AccountSettingsPresenter(IAccountSettingsInteractor iAccountSettingsInteractor) {
        mIAccountSettingsInteractor = iAccountSettingsInteractor;
        mSigninModelView = new AccountSettingsModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mSigninModelView = null;
    }

    @Override
    public void bindViewData() {
//        showProgressView();

        Observable<AccountSettingsModelView> obs = Observable.concat(
                Observable.just(mSigninModelView),
                mIAccountSettingsInteractor.getProfileInfo()
        )
                .takeFirst(d -> !TextUtils.isEmpty(d.getUsername()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<AccountSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(AccountSettingsModelView model) {
        mSigninModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

}
