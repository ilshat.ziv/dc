package com.decenturion.mobile.ui.fragment.profile_v2.deals.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.profile_v2.deals.model.DealListModelView;

public interface IDealListView extends IScreenFragmentView {

    void onBindViewData(@NonNull DealListModelView model);

    void onSignout();
}
