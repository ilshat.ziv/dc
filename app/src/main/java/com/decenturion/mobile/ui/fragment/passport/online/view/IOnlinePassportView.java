package com.decenturion.mobile.ui.fragment.passport.online.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.passport.online.model.OnlinePassportModelView;

public interface IOnlinePassportView extends IScreenFragmentView {

    void onBindViewData(@NonNull SexModelView model2,
                        @NonNull CountryModelView model1,
                        @NonNull OnlinePassportModelView model);

    void onSavedChanges(UpdatePassportResult result);
}
