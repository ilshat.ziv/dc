package com.decenturion.mobile.network.client.device;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.csrf.CsrfResponse;
import com.decenturion.mobile.network.response.version.AppMinVersionResponse;

import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface IDeviceClient {

    @GET(BuildConfig.API_V + "app/minversion?platform=android")
    Observable<AppMinVersionResponse> getAppMinVersion();

    @GET(BuildConfig.API_V + "refresh")
    Observable<CsrfResponse> getCsrfToken();
}
