package com.decenturion.mobile.network.client.resident;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class ResidentClient extends DHttpClient<IResidentClient> {

    public ResidentClient(NetworkModule module) {
        super(module);
    }
}
