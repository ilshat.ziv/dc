package com.decenturion.mobile.ui.fragment.profile_v2.score.item.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.R;
import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.database.type.model.CoinTypeModel;
import com.decenturion.mobile.database.type.model.StartupTypeModel;
import com.decenturion.mobile.network.response.model.Startup;
import com.decenturion.mobile.network.response.model.Token;
import com.decenturion.mobile.network.response.model.TokenCoin;

public class ScoreTokenModelView {

    private int mId;
    private String mName;
    private String mBalance;
    private String mWallet;

    private String mIconUrl;

    private String mCoinUUID;
    private String mCategory;
    private String mTokenCategory;

    private String mResidentUUID;

    public ScoreTokenModelView(OrmTokenModel model) {
        mId = model.getId();
        mBalance = model.getBalance();
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        mWallet = coinTypeModel.getSymbol();

        mCoinUUID = coinTypeModel.getUuid();
        mCategory = model.getCategory();

        initName(model);
    }

    public ScoreTokenModelView(OrmTokenModel model, String logo) {
        this(model);
        mIconUrl = logo;
    }

    public ScoreTokenModelView(OrmTokenModel model, String logo, String s) {
        mId = model.getId();
        mBalance = model.getSellPrice();
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        mWallet = coinTypeModel.getSymbol();

        mCoinUUID = coinTypeModel.getUuid();
        mCategory = model.getCategory();

        initName(model);

        mIconUrl = logo;
    }

    public ScoreTokenModelView(@NonNull Token token) {
        TokenCoin tokenCoin = token.getTokenCoin();
        mCoinUUID = tokenCoin.getUuid();
        mCategory = token.getCategory();

        Startup startup = token.getStartup();
        mName = startup.getName();
        mIconUrl = startup.getLogo();

        mBalance = token.getBalance();
        mWallet = tokenCoin.getSymbol();
    }

    private void initName(OrmTokenModel model) {
        CoinTypeModel coinTypeModel = model.getCoinTypeModel();
        if (TextUtils.equals(coinTypeModel.getName(), "DECENTURION")) {
            String category = model.getCategory();
            switch (category) {
                case "passport" : {
                    mName = "DCNT Passport";
                    mTokenCategory = "passport";
                    return;
                }
                case "internal" : {
                    mName = "DCNT Classic";
                    mTokenCategory = "classic";
                    return;
                }
                case "external" : {
                    mName = "DCNT Liquid";
                    mTokenCategory = "liquid";
                    return;
                }
            }
        }

        StartupTypeModel startupTypeModel = model.getStartupTypeModel();
        mName = startupTypeModel.getName();
    }

    public String getIconUrl() {
        return mIconUrl;
    }

    public int getIconRes() {
        return R.drawable.ic_logo;
    }

    public String getName() {
        return mName;
    }

    public String getBalance() {
        return mBalance;
    }

    public String getWallet() {
        return mWallet;
    }

    public int getId() {
        return mId;
    }

    public String getCoinUUID() {
        return mCoinUUID;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getTokenCategory() {
        return mTokenCategory;
    }
}
