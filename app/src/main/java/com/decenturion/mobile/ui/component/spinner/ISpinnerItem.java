package com.decenturion.mobile.ui.component.spinner;

public interface ISpinnerItem {

    String getItemName();

    String getItemKey();
}
