package com.decenturion.mobile.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.dialog.SpinnerDateDialog;
import com.decenturion.mobile.utils.DateTimeUtils;

import butterknife.BindView;

@SuppressLint("WrongConstant")
public class CalendarView extends UBaseView implements SpinnerDateDialog.OnChangeDateListener {

    @Nullable
    @BindView(R.id.dateView)
    TextView mDateView;

    private long mDate;

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mDateView != null;
    }

    @Override
    protected void init() {
        super.init();
        if (mDate == 0) {
            mDate = System.currentTimeMillis();
        }
    }

    public void setHint(String hint) {
        init();
        mDateView.setHint(String.valueOf(hint));
    }

    public void setEnable(boolean isEnable) {
        init();
        mDateView.setEnabled(isEnable);
    }

    public void setText(String text) {
        init();
        mDateView.setText(String.valueOf(text));
    }

    @Override
    public void changeData(long date) {
        mDate = date;

        mDateView.setTag(date);

        long dateLocalGmt = DateTimeUtils.convertToLocalGMT(date);
        String dateString = DateTimeUtils.getDateFormat(dateLocalGmt, DateTimeUtils.Format.LABEL_BUTTON_DATE);
        mDateView.setText(dateString);

//        CDate.ItemValueModel valueModel = (CDate.ItemValueModel) mComponentModel.getModelValue();
//        valueModel.setDate(date);

        if (mChangeComponentListener != null) {
            mChangeComponentListener.onChangeComponentData(this);
        }
    }

    public long getDate() {
        return mDate;
    }

    @Override
    public void setMustFill(boolean isMust) {

    }
}
