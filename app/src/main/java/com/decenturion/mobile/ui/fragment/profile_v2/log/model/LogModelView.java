package com.decenturion.mobile.ui.fragment.profile_v2.log.model;

import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class LogModelView {

    private List<LogModel> mActionList;
    private int mTotal;
    private long mOffset;
    public final int LIMIT = 15;

    public LogModelView() {
    }

    public LogModelView(List<LogModel> list, int total) {
        mActionList = list;
        mTotal = total;
    }

    public LogModelView(List<LogModel> d) {
        mActionList = d;
    }

    public List<LogModel> getActionList() {
        return mActionList;
    }

    public void addActions(List<LogModel> list) {
        if (CollectionUtils.isEmpty(mActionList)) {
            mActionList = new ArrayList<>();
        }

        if (!CollectionUtils.isEmpty(list)) {
            mActionList.addAll(list);
        }
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setOffset(long offset) {
        mOffset = offset;
    }

    public long getOffset() {
        return mOffset;
    }

}
