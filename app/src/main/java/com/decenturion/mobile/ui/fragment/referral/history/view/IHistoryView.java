package com.decenturion.mobile.ui.fragment.referral.history.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.history.model.HistoryModelView;

public interface IHistoryView extends IScreenFragmentView {

    void onBindViewData(@NonNull HistoryModelView model);
}
