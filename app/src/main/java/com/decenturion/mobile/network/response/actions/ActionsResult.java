package com.decenturion.mobile.network.response.actions;

import com.decenturion.mobile.network.response.model.Action;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActionsResult {

    @JsonProperty("list")
    private List<Action> mActionList;

    @JsonProperty("total")
    private int mTotal;

    public ActionsResult() {
    }

    @JsonIgnore
    public List<Action> getActionList() {
        return mActionList;
    }

    @JsonIgnore
    public int getTotal() {
        return mTotal;
    }
}
