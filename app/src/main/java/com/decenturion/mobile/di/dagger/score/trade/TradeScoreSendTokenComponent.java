package com.decenturion.mobile.di.dagger.score.trade;

import com.decenturion.mobile.ui.fragment.score.send.trade.view.TradeScoreSendTokenFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        TradeScoreSendTokenModule.class
})

@TradeScoreSendTokenScope
public interface TradeScoreSendTokenComponent {

    void inject(TradeScoreSendTokenFragment view);
}
