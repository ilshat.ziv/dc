package com.decenturion.mobile.business.settings.account;

import com.decenturion.mobile.ui.fragment.settings.account.model.AccountSettingsModelView;

import rx.Observable;

public interface IAccountSettingsInteractor {

    Observable<AccountSettingsModelView> getProfileInfo();
}
