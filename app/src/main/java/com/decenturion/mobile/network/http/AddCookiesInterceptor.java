package com.decenturion.mobile.network.http;

import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.token.ITokensPrefsManager;
import com.decenturion.mobile.app.prefs.token.TokenPrefsOptions;
import com.decenturion.mobile.utils.CollectionUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddCookiesInterceptor implements Interceptor {

    private IPrefsManager mIPrefsManager;

    public AddCookiesInterceptor(@NonNull IPrefsManager IPrefsManager) {
        super();
        mIPrefsManager = IPrefsManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        String agent = Build.MODEL +
                " Android " + Build.VERSION.RELEASE +
                " (v" + BuildConfig.VERSION_NAME + " build " + BuildConfig.VERSION_CODE + ")";
        builder.header("User-Agent", agent);

        ITokensPrefsManager iTokensPrefsManager = mIPrefsManager.getITokensPrefsManager();
        Set param = iTokensPrefsManager.getParam(TokenPrefsOptions.Keys.COOKIES, new HashSet<String>());
        if (!CollectionUtils.isEmpty(param)) {
            HashSet<String> preferences = (HashSet<String>) param;
            for (String cookie : preferences) {
                builder.addHeader("Cookie", cookie);
            }
        }

        String csrfToken = iTokensPrefsManager.getParam(TokenPrefsOptions.Keys.CSRF_TOKEN, "");
        if (!TextUtils.isEmpty(csrfToken)) {
            builder.addHeader("X-CSRF-TOKEN", csrfToken);
        }

        return chain.proceed(builder.build());
    }
}