package com.decenturion.mobile.ui.component;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.decenturion.mobile.R;

import butterknife.BindView;

public class AmountView extends UBaseView implements View.OnClickListener {

    @Nullable
    @BindView(R.id.valueView)
    TextView mValueView;

    @Nullable
    @BindView(R.id.exchangeView)
    TextView mExchangeView;

    public AmountView(Context context) {
        super(context);
    }

    public AmountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AmountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean isInitView() {
        return super.isInitView() && mValueView != null;
    }

    @Override
    protected void init() {
        super.init();

        if (isInitView()) {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font/Canada Type - VoxRound-Light.otf");
            assert mValueView != null;
            mValueView.setTypeface(typeface);
        }
    }

    public void setHint(String hint) {
        init();
        assert mValueView != null;
        mValueView.setHint(String.valueOf(hint));
    }

    public void setText(String text) {
        init();
        assert mValueView != null;
        mValueView.setText(text != null ? String.valueOf(text) : "");

    }

    @Override
    public void setMustFill(boolean isMust) {

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        this.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    public String getValue() {
        init();
        assert mValueView != null;
        return mValueView.getText().toString();
    }

    public void setExchange(String exchange) {
        init();
        assert mExchangeView != null;
        mExchangeView.setText(exchange);
    }
}