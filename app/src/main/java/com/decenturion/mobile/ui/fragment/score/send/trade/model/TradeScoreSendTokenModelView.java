package com.decenturion.mobile.ui.fragment.score.send.trade.model;

import com.decenturion.mobile.ui.fragment.score.send.trade.MethodSendToken;

public class TradeScoreSendTokenModelView {

    private int mTokenId;
    private String mCoinUUID;
    private String mCoinCategory;
    private String mTokenName;
    private String mAmount;
    private String mTwoFA;
    private String mSymbol;

    private MethodSendToken mTypeSend = MethodSendToken.NORMAL;

    private TradeScoreSendBalanceModel mTradeScoreSendBalanceModel;
    private TradeScoreSendOptionModel mTradeScoreSendOptionModel;

    public TradeScoreSendTokenModelView() {
    }

    public TradeScoreSendTokenModelView(int tokenId) {
        mTokenId = tokenId;
    }

    public int getTokenId() {
        return mTokenId;
    }

    public String getCoinUUID() {
        return mCoinUUID;
    }

    public String getCoinCategory() {
        return mCoinCategory;
    }

    public String getAmount() {
        return mAmount;
    }

    public String getTwoFA() {
        return mTwoFA;
    }

    public void setCoinUUID(String coinUUID) {
        mCoinUUID = coinUUID;
    }

    public void setCoinCategory(String coinCategory) {
        mCoinCategory = coinCategory;
    }

    public void setTwoFA(String twoFA) {
        mTwoFA = twoFA;
    }

    public void setAmount(String amount) {
        mAmount = amount;
    }

    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    public String getSymbol() {
        return mSymbol;
    }

    public String getTokenName() {
        return mTokenName;
    }

    public MethodSendToken getTypeSend() {
        return mTypeSend;
    }

    public TradeScoreSendBalanceModel getTradeScoreSendBalanceModel() {
        return mTradeScoreSendBalanceModel;
    }

    public void setTradeScoreSendBalanceModel(TradeScoreSendBalanceModel tradeScoreSendBalanceModel) {
        mTradeScoreSendBalanceModel = tradeScoreSendBalanceModel;
    }

    public void setTradeScoreSendOptionModel(TradeScoreSendOptionModel tradeScoreSendOptionModel) {
        mTradeScoreSendOptionModel = tradeScoreSendOptionModel;
    }

    public TradeScoreSendOptionModel getTradeScoreSendOptionModel() {
        return mTradeScoreSendOptionModel;
    }

    public void setTokenName(String tokenName) {
        mTokenName = tokenName;
    }
}
