package com.decenturion.mobile.ui.fragment.twofa.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.twofa.view.IEnableTwoFAView;

public interface IEnableTwoFAPresenter extends IPresenter<IEnableTwoFAView> {

    void bindViewData();

    void saveChanges(@NonNull String secretString, @NonNull String secretCode);

    void onSaveInstanceState(@NonNull Bundle outState,
                             @NonNull String secretString, @NonNull String secretCode);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
