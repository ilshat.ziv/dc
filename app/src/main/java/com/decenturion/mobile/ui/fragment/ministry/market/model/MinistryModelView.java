package com.decenturion.mobile.ui.fragment.ministry.market.model;

import android.support.annotation.NonNull;

import java.util.List;

public class MinistryModelView {

    private List<MinistryProduct> mMinistryProductList;

    public MinistryModelView() {
    }

    public MinistryModelView(@NonNull List<MinistryProduct> list) {
        mMinistryProductList = list;
    }

    public List<MinistryProduct> getMinistryProductList() {
        return mMinistryProductList;
    }
}
