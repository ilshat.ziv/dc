package com.decenturion.mobile.network.response.referral;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralSettingsResult {

    @JsonProperty("costing_method")
    private String mCostingMethod;

    @JsonProperty("fixed_cost")
    private String mFixedCost;

    @JsonProperty("fixed_token_count")
    private String mFixedTokenCount;

    @JsonProperty("fixed_comment")
    private String mFixedComment;

    @JsonProperty("percent")
    private int mPercent;

    @JsonProperty("percent_token_count")
    private String mPercentTokenCount;

    @JsonProperty("percent_comment")
    private String mPercentComment;

    @JsonProperty("enabled")
    private boolean isEnabel;

    public ReferralSettingsResult() {
    }

    @JsonIgnore
    public String getCostingMethod() {
        return mCostingMethod;
    }

    @JsonIgnore
    public String getFixedCost() {
        return mFixedCost;
    }

    @JsonIgnore
    public String getFixedTokenCount() {
        return mFixedTokenCount;
    }

    @JsonIgnore
    public String getFixedComment() {
        return mFixedComment;
    }

    @JsonIgnore
    public int getPercent() {
        return mPercent;
    }

    @JsonIgnore
    public String getPercentTokenCount() {
        return mPercentTokenCount;
    }

    @JsonIgnore
    public String getPercentComment() {
        return mPercentComment;
    }

    @JsonIgnore
    public boolean isEnabel() {
        return isEnabel;
    }
}
