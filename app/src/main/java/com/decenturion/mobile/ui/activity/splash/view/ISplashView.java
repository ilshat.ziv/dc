package com.decenturion.mobile.ui.activity.splash.view;

import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;

public interface ISplashView extends IScreenActivityView {

    void onSuccess();

    void onUpdateApp();

    void onAccessRestricted();
}
