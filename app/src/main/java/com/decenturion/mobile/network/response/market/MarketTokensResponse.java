package com.decenturion.mobile.network.response.market;

import com.decenturion.mobile.network.response.BaseResponse;
import com.decenturion.mobile.network.response.model.MarketCoinsToken;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketTokensResponse extends BaseResponse {

    @JsonProperty("result")
    private ArrayList<MarketCoinsToken> mResult;

    public MarketTokensResponse() {
    }

    @JsonIgnore
    public ArrayList<MarketCoinsToken> getResult() {
        return mResult;
    }
}
