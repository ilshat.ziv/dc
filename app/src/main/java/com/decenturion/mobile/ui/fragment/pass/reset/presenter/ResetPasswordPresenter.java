package com.decenturion.mobile.ui.fragment.pass.reset.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.restore.IRestorePassInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.reset.ResetPasswordResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.reset.model.ResetPasswordModelView;
import com.decenturion.mobile.ui.fragment.pass.reset.view.IResetPasswordView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ResetPasswordPresenter extends Presenter<IResetPasswordView> implements IResetPasswordPresenter {

    private IRestorePassInteractor mIRestorePassInteractor;
    private IPrefsManager mIPrefsManager;
    private ResetPasswordModelView mPasswordSettingsModelView;

    public ResetPasswordPresenter(IRestorePassInteractor iPasswordSettingsInteractor,
                                  IPrefsManager iPrefsManager) {
        mIRestorePassInteractor = iPasswordSettingsInteractor;
        mIPrefsManager = iPrefsManager;
        mPasswordSettingsModelView = new ResetPasswordModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mPasswordSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<ResetPasswordModelView> obs = Observable.just(mPasswordSettingsModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ResetPasswordModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(ResetPasswordModelView model) {
        mPasswordSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void resetPassword(@NonNull String token,
                              @NonNull String newPassword,
                              @NonNull String retypePassword) {

        showProgressView();

        Observable<ResetPasswordResult> obs = mIRestorePassInteractor.resetPass(token,
                newPassword, retypePassword
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ResetPasswordResult>) this::resetPasswordSuccess,
                this::resetPasswordFailure
        );
    }

    private void resetPasswordFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());
                return;
            }
        }

        showErrorView(throwable);
    }

    private void resetPasswordSuccess(ResetPasswordResult result) {
        hideProgressView();
        Resident resident = result.getResident();

        int step = resident.getStep();
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

        if (mView != null) {
            mView.onResetPasswordSuccess(step);
        }
    }
}
