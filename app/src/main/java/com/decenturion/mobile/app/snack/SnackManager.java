package com.decenturion.mobile.app.snack;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class SnackManager implements ISnackManager {

    @ColorInt
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#FFFFFF");

    @ColorInt
    private static final int ERROR_COLOR = Color.parseColor("#D50000");

    @ColorInt
    private static final int INFO_COLOR = Color.parseColor("#3F51B5");

    @ColorInt
    private static final int SUCCESS_COLOR = Color.parseColor("#388E3C");

    @ColorInt
    private static final int WARNING_COLOR = Color.parseColor("#FFA900");

    private View mRootView;
    private Snackbar mSnackbar;

    public SnackManager(@NonNull View view) {
        mRootView = view;
    }

    private Snackbar showNormal(@NonNull String message, int duration, Drawable drawable) {
        Snackbar snackbar;
        if (drawable != null) {
            snackbar = Snackbar.make(mRootView, message, duration);
        } else {
            snackbar = Snackbar.make(mRootView, message, duration);
        }
        View view = snackbar.getView();
        view.setBackgroundColor(DEFAULT_TEXT_COLOR);
        setTextStyle(view, Color.parseColor("#000000"));
        snackbar.show();
        return snackbar;
    }

    private Snackbar showSuccess(@NonNull String message, int duration, boolean showIcon) {
        Snackbar snackbar = Snackbar.make(mRootView, message, duration);
        View view = snackbar.getView();
        view.setBackgroundColor(SUCCESS_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showInfo(@NonNull String message, int duration, boolean showIcon) {
        Snackbar snackbar = Snackbar.make(mRootView, message, duration);
        View view = snackbar.getView();
        view.setBackgroundColor(INFO_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showWarning(@NonNull String message, int duration, boolean showIcon) {
        Snackbar snackbar = Snackbar.make(mRootView, message, duration);
        View view = snackbar.getView();
        view.setBackgroundColor(WARNING_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showError(@NonNull String message, int duration, boolean showIcon) {
        Snackbar snackbar = Snackbar.make(mRootView, message, duration);
        View view = snackbar.getView();
        view.setBackgroundColor(ERROR_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private void setTextStyle(@NonNull View view) {
        setTextStyle(view, 0);
    }

    private void setTextStyle(@NonNull View view, int textColor) {
        Context context = view.getContext();
        AssetManager assets = context.getAssets();
        Typeface typeface = Typeface.createFromAsset(assets, "font/Canada Type - VoxRound-Medium.otf");

        TextView tv = view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTypeface(typeface);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        if (textColor != 0) {
            tv.setTextColor(textColor);
        }
    }

    private boolean isMessageValid(String message) {
        return !TextUtils.isEmpty(message)
                && !TextUtils.equals(message, "thread interrupted");
    }

    @Override
    public void showShortSnack(@NonNull TypeMessage type, @NonNull String message) {
        if (!isMessageValid(message)) {
            return;
        }

        dismissSnackbar();

        switch (type) {
            case Normal : {
                mSnackbar = showNormal(message, Snackbar.LENGTH_SHORT, null);
                break;
            }
            case Success : {
                mSnackbar = showSuccess(message, Snackbar.LENGTH_SHORT, false);
                break;
            }
            case Info : {
                mSnackbar = showInfo(message, Snackbar.LENGTH_SHORT, false);
                break;
            }
            case Warning : {
                mSnackbar = showWarning(message, Snackbar.LENGTH_SHORT, false);
                break;
            }
            case Error : {
                mSnackbar = showError(message, Snackbar.LENGTH_SHORT, false);
                break;
            }
        }
    }

    @Override
    public void showLongSnack(@NonNull TypeMessage type, @NonNull String message) {
        dismissSnackbar();

        switch (type) {
            case Normal : {
                mSnackbar = showNormal(message, Snackbar.LENGTH_LONG, null);
                break;
            }
            case Success : {
                mSnackbar = showSuccess(message, Snackbar.LENGTH_LONG, false);
                break;
            }
            case Info : {
                mSnackbar = showInfo(message, Snackbar.LENGTH_LONG, false);
                break;
            }
            case Warning : {
                mSnackbar = showWarning(message, Snackbar.LENGTH_LONG, false);
                break;
            }
            case Error : {
                mSnackbar = showError(message, Snackbar.LENGTH_LONG, false);
                break;
            }
        }
    }

    @Override
    public void dismissSnackbar() {
        if (mSnackbar != null) {
            mSnackbar.dismiss();
        }
    }
}
