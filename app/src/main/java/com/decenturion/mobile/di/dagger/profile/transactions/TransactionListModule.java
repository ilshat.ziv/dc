package com.decenturion.mobile.di.dagger.profile.transactions;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.profile.transactions.ITransaionListInteractor;
import com.decenturion.mobile.business.profile.transactions.TransactionListInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.presenter.ITransactionListPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.transactions.presenter.TransactionListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionListModule {

    @Provides
    @TransactionListScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @TransactionListScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TransactionListScope
    ITransaionListInteractor provideITransaionListInteractor(IResidentClient IResidentClient,
                                                             IResidentRepository iResidentRepository,
                                                             IPrefsManager iPrefsManager,
                                                             ILocalistionManager iLocalistionManager) {

        return new TransactionListInteractor(IResidentClient, iResidentRepository,
                iPrefsManager, iLocalistionManager);
    }

    @Provides
    @TransactionListScope
    ITransactionListPresenter provideITransactionListPresenter(ITransaionListInteractor iTransaionListInteractor) {
        return new TransactionListPresenter(iTransaionListInteractor);
    }
}
