package com.decenturion.mobile.network.http.adapter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.http.exception.CsrfAccessException;
import com.decenturion.mobile.network.http.exception.NotModifyException;
import com.decenturion.mobile.utils.LoggerUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Func1;

public class RxJavaCallAdapterFactory extends CallAdapter.Factory {

    /**
     * Returns an instance which creates synchronous observables that do not operate on any scheduler
     * by default.
     */

    public static RxJavaCallAdapterFactory create(Observable observableCsrfAccessException) {
        return new RxJavaCallAdapterFactory(null, observableCsrfAccessException);
    }

    public static RxJavaCallAdapterFactory create() {
        return new RxJavaCallAdapterFactory(null);
    }

    /**
     * Returns an instance which creates synchronous observables that
     * {@linkplain Observable#subscribeOn(Scheduler) subscribe on} {@code scheduler} by default.
     */
    public static RxJavaCallAdapterFactory createWithScheduler(Scheduler scheduler) {
        if (scheduler == null) throw new NullPointerException("scheduler == null");
        return new RxJavaCallAdapterFactory(scheduler);
    }

    private Observable mObservableCsrfAccessException;
    private final Scheduler scheduler;

    private RxJavaCallAdapterFactory(Scheduler scheduler, Observable observableAfterAccessException) {
        this.scheduler = scheduler;
        this.mObservableCsrfAccessException = observableAfterAccessException;
    }

    private RxJavaCallAdapterFactory(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public void setObservableCsrfAccessException(Observable observableCsrfAccessException) {
        mObservableCsrfAccessException = observableCsrfAccessException;
    }

    public Observable getObservableCsrfAccessException() {
        return mObservableCsrfAccessException;
    }

    @Override
    public CallAdapter<?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        Class<?> rawType = getRawType(returnType);
        String canonicalName = rawType.getCanonicalName();
        boolean isSingle = "rx.Single".equals(canonicalName);
        boolean isCompletable = "rx.Completable".equals(canonicalName);
        if (rawType != Observable.class && !isSingle && !isCompletable) {
            return null;
        }
        if (!isCompletable && !(returnType instanceof ParameterizedType)) {
            String name = isSingle ? "Single" : "Observable";
            throw new IllegalStateException(name + " return type must be parameterized"
                    + " as " + name + "<Foo> or " + name + "<? extends Foo>");
        }

        if (isCompletable) {
            // Add Completable-converter wrapper from a separate class. This defers classloading such that
            // regular Observable operation can be leveraged without relying on this unstable RxJava API.
            // Note that this has to be done separately since Completable doesn't have a parametrized
            // type.
            return CompletableHelper.createCallAdapter(scheduler);
        }

        CallAdapter<Observable<?>> callAdapter = getCallAdapter(returnType, scheduler);
        if (isSingle) {
            // Add Single-converter wrapper from a separate class. This defers classloading such that
            // regular Observable operation can be leveraged without relying on this unstable RxJava API.
            return SingleHelper.makeSingle(callAdapter);
        }
        return callAdapter;
    }

    private CallAdapter<Observable<?>> getCallAdapter(Type returnType, Scheduler scheduler) {
        Type observableType = getParameterUpperBound(0, (ParameterizedType) returnType);
        Class<?> rawObservableType = getRawType(observableType);
        if (rawObservableType == Response.class) {
            if (!(observableType instanceof ParameterizedType)) {
                throw new IllegalStateException("Response must be parameterized"
                        + " as Response<Foo> or Response<? extends Foo>");
            }
            Type responseType = getParameterUpperBound(0, (ParameterizedType) observableType);
            return new RxJavaCallAdapterFactory.ResponseCallAdapter(responseType, scheduler);
        }

        if (rawObservableType == Result.class) {
            if (!(observableType instanceof ParameterizedType)) {
                throw new IllegalStateException("Result must be parameterized"
                        + " as Result<Foo> or Result<? extends Foo>");
            }
            Type responseType = getParameterUpperBound(0, (ParameterizedType) observableType);
            return new RxJavaCallAdapterFactory.ResultCallAdapter(responseType, scheduler);
        }

        return new RxJavaCallAdapterFactory.SimpleCallAdapter(this, observableType, scheduler);
    }

    static final class CallOnSubscribe<T> implements Observable.OnSubscribe<Response<T>> {
        private final Call<T> originalCall;

        CallOnSubscribe(Call<T> originalCall) {
            this.originalCall = originalCall;
        }

        @Override
        public void call(final Subscriber<? super Response<T>> subscriber) {
            // Since Call is a one-shot type, clone it for each new subscriber.
            Call<T> call = originalCall.clone();

            // Wrap the call in a helper which handles both unsubscription and backpressure.
            RxJavaCallAdapterFactory.RequestArbiter<T> requestArbiter = new RxJavaCallAdapterFactory.RequestArbiter<>(call, subscriber);
            subscriber.add(requestArbiter);
            subscriber.setProducer(requestArbiter);
        }
    }

    static final class RequestArbiter<T> extends AtomicBoolean implements Subscription, Producer {
        private final Call<T> call;
        private final Subscriber<? super Response<T>> subscriber;

        RequestArbiter(Call<T> call, Subscriber<? super Response<T>> subscriber) {
            this.call = call;
            this.subscriber = subscriber;
        }

        @Override
        public void request(long n) {
            if (n < 0) throw new IllegalArgumentException("n < 0: " + n);
            if (n == 0) return; // Nothing to do when requesting 0.
            if (!compareAndSet(false, true)) return; // Request was already triggered.

            try {
                Response<T> response = call.execute();
                // TODO: 19.01.2018 Resolve with answer code 401
                answerIsValid(response);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(response);
                }
            } catch (Throwable t) {
                Exceptions.throwIfFatal(t);
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(t);
                }
                return;
            }

            if (!subscriber.isUnsubscribed()) {
                subscriber.onCompleted();
            }
        }

        private void answerIsValid(@NonNull Response<T> response) {
            if (response.code() == 304) {
                throw new NotModifyException();
            }
        }

        @Override
        public void unsubscribe() {
            call.cancel();
        }

        @Override
        public boolean isUnsubscribed() {
            return call.isCanceled();
        }
    }

    static final class ResponseCallAdapter implements CallAdapter<Observable<?>> {
        private final Type responseType;
        private final Scheduler scheduler;

        ResponseCallAdapter(Type responseType, Scheduler scheduler) {
            this.responseType = responseType;
            this.scheduler = scheduler;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public <R> Observable<Response<R>> adapt(Call<R> call) {
            Observable<Response<R>> observable = Observable.create(new RxJavaCallAdapterFactory.CallOnSubscribe<>(call));
            if (scheduler != null) {
                return observable.subscribeOn(scheduler);
            }
            return observable;
        }
    }

    static final class SimpleCallAdapter implements CallAdapter<Observable<?>> {

        private final Type responseType;
        private final Scheduler scheduler;
        private RxJavaCallAdapterFactory callAdapterFactory;

        private HashMap<String, Observable<?>> requestList = new HashMap<>();

        SimpleCallAdapter(Type responseType, Scheduler scheduler) {
            this.responseType = responseType;
            this.scheduler = scheduler;
        }

        SimpleCallAdapter(RxJavaCallAdapterFactory callAdapterFactory, Type responseType, Scheduler scheduler) {
            this(responseType, scheduler);
            this.callAdapterFactory = callAdapterFactory;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public <R> Observable<R> adapt(Call<R> call) {
            String v = call.request().toString();

            Observable<R> observable = (Observable<R>) requestList.get(v);
            if (observable != null) {
                return observable;
            }

            observable = Observable.create(new RxJavaCallAdapterFactory.CallOnSubscribe<>(call))
                    .lift(OperatorMapResponseToBodyOrError.instance())
                    .doOnCompleted(() -> requestList.remove(v))
                    .doOnError(throwable -> requestList.remove(v));

            if (callAdapterFactory != null) {
                observable = observable.retryWhen((Func1<Observable<? extends Throwable>, Observable<Boolean>>) observable1 ->
                        observable1.flatMap((Func1<Throwable, Observable<Boolean>>) throwable -> {
                            if (throwable instanceof CsrfAccessException) {
                                LoggerUtils.exception(throwable);
                                return callAdapterFactory.getObservableCsrfAccessException();
                            }
                            return Observable.error(throwable);
                        })
                );
            }

            observable = observable.replay()
                    .autoConnect();

            if (scheduler != null) {
                requestList.put(v, observable);
                return observable.subscribeOn(scheduler);
            }

            requestList.put(v, observable);
            return observable;
        }
    }

    static final class ResultCallAdapter implements CallAdapter<Observable<?>> {
        private final Type responseType;
        private final Scheduler scheduler;

        ResultCallAdapter(Type responseType, Scheduler scheduler) {
            this.responseType = responseType;
            this.scheduler = scheduler;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public <R> Observable<Result<R>> adapt(Call<R> call) {
            Observable<Result<R>> observable = Observable.create(new RxJavaCallAdapterFactory.CallOnSubscribe<>(call)) //
                    .map(Result::response)
                    .onErrorReturn(Result::error);
            if (scheduler != null) {
                return observable.subscribeOn(scheduler);
            }
            return observable;
        }
    }
}
