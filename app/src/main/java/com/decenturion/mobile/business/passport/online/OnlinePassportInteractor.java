package com.decenturion.mobile.business.passport.online;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.database.model.OrmResidentModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.UpdatePassportModel;
import com.decenturion.mobile.network.response.UpdatePassportResponse;
import com.decenturion.mobile.network.response.UpdatePassportResult;
import com.decenturion.mobile.network.response.model.Passport;
import com.decenturion.mobile.network.response.model.Photo;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.resident.ResidentResponse;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.passport.online.model.OnlinePassportModelView;
import com.decenturion.mobile.utils.DateTimeUtils;
import com.decenturion.mobile.utils.ImageUtils;
import com.decenturion.mobile.utils.LoggerUtils;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

public class OnlinePassportInteractor implements IOnlinePassportInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public OnlinePassportInteractor(IResidentClient IResidentClient,
                                    IResidentRepository iResidentRepository,
                                    IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<OnlinePassportModelView> getProfileInfo() {
        Observable<OnlinePassportModelView> obs2 = mIResidentClient.getResident()
                .map(ResidentResponse::getResult)
                .map(d -> {
                    Resident resident = d.getResident();
                    Passport passport = d.getPassport();
                    Photo photo = d.getPhoto();
                    try {
                        mIResidentRepository.saveLocale(d.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(passport);
                        mIResidentRepository.savePhotoInfo(photo);
                        mIResidentRepository.saveSettings(d.getSettings());
//                        mIResidentRepository.saveWallets(d.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new OnlinePassportModelView(resident, passport, photo);
                });

        Observable<OnlinePassportModelView> obs1 = Observable.zip(
                Observable.just(mIResidentRepository.getResidentInfo()),
                Observable.just(mIResidentRepository.getPassportInfo()),
                Observable.just(mIResidentRepository.getPhotoInfo()),
                OnlinePassportModelView::new
        )
                .onErrorReturn(throwable -> null);

        return Observable.concat(obs1, obs2)
                .takeFirst(d -> d != null);
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(@NonNull Bitmap bitmap) {
        return Observable.just(mIResidentRepository.getResidentInfo())
                .map(OrmResidentModel::getUuid)
                .map(d -> {
                    byte[] bytes = ImageUtils.getBytes(bitmap);
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    return MultipartBody.Part.createFormData("file", d + ".png", requestFile);
                })
                .flatMap(d -> mIResidentClient.uploadPhoto(d));
    }

    @Override
    public Observable<UpdatePassportResult> setPassportData(@NonNull String photo,
                                                            @NonNull String firstName,
                                                            @NonNull String secondName,
                                                            long birth,
                                                            @NonNull String sex,
                                                            @NonNull String country,
                                                            @NonNull String city) {
        UpdatePassportModel model = new UpdatePassportModel();
        model.setFirstName(firstName);
        model.setLastName(secondName);
        if (birth != 0) {
            String dateFormat = DateTimeUtils.getDateFormat(birth, DateTimeUtils.Format.SIMPLE_FORMAT_2);
            model.setBirth(dateFormat);
        }
        model.setSex(sex);
        model.setCountry(country);
        model.setCity(city);

        return Observable.just(model)
//                .map(this::isValidModel)
                .flatMap(m -> mIResidentClient.updatePassport(m))
                .map(UpdatePassportResponse::getResult)
                .doOnNext(result -> {
                    try {
                        Resident resident = result.getResident();
                        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                        iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, resident.getStep());

                        mIResidentRepository.saveLocale(result.getLocale());
                        mIResidentRepository.saveResidentInfo(resident);
                        mIResidentRepository.savePassportInfo(result.getPassport());
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                        mIResidentRepository.saveSettings(result.getSettings());
//                        mIResidentRepository.saveWallets(result.getWalletList());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                });
    }

    private UpdatePassportModel isValidModel(UpdatePassportModel d) {
        isVaildCity(d.getCity());
        isVaildCountry(d.getCountry());
        isVaildSex(d.getSex());
        isVaildBirth(d.getBirth());
        isVaildFirstName(d.getFirstName());
        isVaildSecondName(d.getLastName());

        return d;
    }

    private void isVaildCity(String city) {
        if (TextUtils.isEmpty(city)) {
            throw new RuntimeException("City is empty");
        }
    }

    private void isVaildCountry(String country) {
        if (TextUtils.isEmpty(country)) {
            throw new RuntimeException("Country is empty");
        }
    }

    private void isVaildSex(String sex) {
        if (TextUtils.isEmpty(sex)) {
            throw new RuntimeException("Sex is empty");
        }
    }

    private void isVaildBirth(String birth) {
        if (TextUtils.isEmpty(birth)) {
            throw new RuntimeException("Birth is empty");
        }
    }

    private void isVaildFirstName(String firstName) {
        if (TextUtils.isEmpty(firstName)) {
            throw new RuntimeException("FirstName is empty");
        }
    }

    private void isVaildSecondName(String secondName) {
        if (TextUtils.isEmpty(secondName)) {
            throw new RuntimeException("SecondName is empty");
        }
    }

}
