package com.decenturion.mobile.di.dagger.settings.password;

import com.decenturion.mobile.ui.fragment.settings.password.view.PasswordSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        PasswordSettingsModule.class
})

@PasswordSettingsScope
public interface PasswordSettingsComponent {

    void inject(PasswordSettingsFragment view);
}
