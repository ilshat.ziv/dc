package com.decenturion.mobile.ui.dialog.bottomsheet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.activity.invite.InviteActivity;
import com.decenturion.mobile.utils.LoggerUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreActionDialogFragment extends BaseBottomSheetDialog {

    private OnMoreActionDialogListener mOnMoreActionDialogListener;

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fr_dialog_bottomsheet_more_action, null);
        dialog.setContentView(contentView);

        ButterKnife.bind(this, contentView);

        setPeekHeight(contentView);
    }

    public void setOnMoreActionDialogListener(OnMoreActionDialogListener listener) {
        mOnMoreActionDialogListener = listener;
    }

    @OnClick(R.id.copyWalletAddressView)
    protected void onCopyWalletAddress() {
        if (mOnMoreActionDialogListener != null) {
            mOnMoreActionDialogListener.onCopyWalletAddress();
        } else {
            LoggerUtils.exception(new NullPointerException("OnMoreActionDialogListener is null"));
        }
        dismiss();
    }

    @OnClick(R.id.copyProfileLinkView)
    protected void onCopyProfileLink() {
        if (mOnMoreActionDialogListener != null) {
            mOnMoreActionDialogListener.onCopyProfileLink();
        } else {
            LoggerUtils.exception(new NullPointerException("OnMoreActionDialogListener is null"));
        }
        dismiss();
    }

    @OnClick(R.id.inviteNewCitizenView)
    protected void onInviteNewCitizen() {
        Intent intent = new Intent(getActivity(), InviteActivity.class);
        startActivity(intent);
        dismiss();
    }

    @OnClick(R.id.copyReferalLincView)
    protected void onCopyReferalLinc() {
        if (mOnMoreActionDialogListener != null) {
            mOnMoreActionDialogListener.onCopyReferalLink();
        } else {
            LoggerUtils.exception(new NullPointerException("OnMoreActionDialogListener is null"));
        }
        dismiss();
    }

    @OnClick(R.id.shareView)
    protected void onShare() {
        if (mOnMoreActionDialogListener != null) {
            mOnMoreActionDialogListener.onShare();
        } else {
            LoggerUtils.exception(new NullPointerException("OnMoreActionDialogListener is null"));
        }
        dismiss();
    }

    public interface OnMoreActionDialogListener {
        void onCopyWalletAddress();

        void onCopyProfileLink();

        void onCopyReferalLink();

        void onShare();
    }
}