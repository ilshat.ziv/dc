package com.decenturion.mobile.ui.fragment.settings.security.model;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmSettingsModel;
import com.decenturion.mobile.network.response.model.Settings;

public class SecurityModelView {

    private boolean isShowName;
    private boolean isShowBirth;
    private boolean isShowPhoto;
    private boolean isShowTokens;
    private boolean isShowMoney;
    private boolean isShowPower;
    private boolean isShowGlory;

    private GfaModel mGfaModel;

    public SecurityModelView() {
        this.mGfaModel = new GfaModel();
    }

    private SecurityModelView(@NonNull OrmSettingsModel model) {
        this();
        this.isShowName = model.isShowName();
        this.isShowBirth = model.isShowBirth();
        this.isShowPhoto = model.isShowPhoto();
        this.isShowTokens = model.isShowTokens();
        this.isShowMoney = model.isShowMoney();
        this.isShowPower = model.isShowPower();
        this.isShowGlory = model.isShowGlory();
    }

    private SecurityModelView(@NonNull Settings model) {
        this();
        this.isShowName = model.isShowName();
        this.isShowBirth = model.isShowBirth();
        this.isShowPhoto = model.isShowPhoto();
        this.isShowTokens = model.isShowTokens();
        this.isShowMoney = model.isShowMoney();
        this.isShowPower = model.isShowPower();
        this.isShowGlory = model.isShowGlory();
    }

    public SecurityModelView(Settings settings, boolean isEnableG2fa) {
        this(settings);
        mGfaModel.setEnableGfa(isEnableG2fa);
        mGfaModel.setEnabledGfa(isEnableG2fa);
    }

    public SecurityModelView(OrmSettingsModel settings, boolean isEnableG2fa) {
        this(settings);
        mGfaModel.setEnableGfa(isEnableG2fa);
        mGfaModel.setEnabledGfa(isEnableG2fa);
    }

    public boolean isShowName() {
        return this.isShowName;
    }

    public boolean isShowBirth() {
        return this.isShowBirth;
    }

    public boolean isShowPhoto() {
        return this.isShowPhoto;
    }

    public boolean isShowTokens() {
        return this.isShowTokens;
    }

    public boolean isShowMoney() {
        return this.isShowMoney;
    }

    public boolean isShowPower() {
        return this.isShowPower;
    }

    public boolean isShowGlory() {
        return this.isShowGlory;
    }

    public GfaModel getGfaModel() {
        return mGfaModel;
    }

    public void setShowName(boolean showName) {
        isShowName = showName;
    }

    public void setShowBirth(boolean showBirth) {
        isShowBirth = showBirth;
    }

    public void setShowPhoto(boolean showPhoto) {
        isShowPhoto = showPhoto;
    }

    public void setShowTokens(boolean showTokens) {
        isShowTokens = showTokens;
    }

    public void setShowMoney(boolean showMoney) {
        isShowMoney = showMoney;
    }

    public void setShowPower(boolean showPower) {
        isShowPower = showPower;
    }

    public void setShowGlory(boolean showGlory) {
        isShowGlory = showGlory;
    }

    public void setGfaModel(GfaModel gfaModel) {
        mGfaModel = gfaModel;
    }
}
