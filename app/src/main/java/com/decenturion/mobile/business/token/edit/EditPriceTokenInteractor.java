package com.decenturion.mobile.business.token.edit;

import android.support.annotation.NonNull;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.request.EditPriceTokenModel;
import com.decenturion.mobile.network.response.EditPriceTokenResponse;
import com.decenturion.mobile.network.response.MinPriceResponse;
import com.decenturion.mobile.network.response.model.Token;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.ui.fragment.token.edit.model.EditPriceModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class EditPriceTokenInteractor implements IEditPriceTokenInteractor {

    private IResidentClient mIResidentClient;
    private ITokenClient mITokenClient;
    private ITokenRepository mITokenRepository;

    public EditPriceTokenInteractor(IResidentClient IResidentClient,
                                    ITokenClient iTokenClient,
                                    ITokenRepository ITokenRepository) {
        mIResidentClient = IResidentClient;
        mITokenClient = iTokenClient;
        mITokenRepository = ITokenRepository;
    }

    @Override
    public Observable<EditPriceModelView> getTokenDetailsModule(int tokenId) {
        Observable<String> obs1 = mITokenClient.getMinPrice()
                .map(MinPriceResponse::getResult);

        Observable<EditPriceModelView> obs2 =  Observable.just(mITokenRepository.getTokenById(tokenId))
                .map(EditPriceModelView::new);

        return Observable.zip(obs1, obs2, (s, model) -> {
            model.setSellMinPrice(s);
            return model;
        });
    }

    @Override
    public Observable<EditPriceModelView> saveChanges(
            int tokenId,
            @NonNull String coinId,
            @NonNull String sell,
            @NonNull String category) {

        EditPriceTokenModel value = new EditPriceTokenModel();
        value.setCoinId(coinId);
        value.setAskPrice(sell);
        value.setCategroy(category);

        return mITokenClient.editPriceToken(value)
                .map(EditPriceTokenResponse::getResult)
                .map(d -> {
                    Token token = d.get(0);
                    OrmTokenModel model = mITokenRepository.getTokenById(tokenId);
                    model.setBuyPrice(token.getBidPrice());
                    model.setSellPrice(token.getAskPrice());
                    try {
                        mITokenRepository.saveTokenInfo(model);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }

                    return new EditPriceModelView(token);
                });
    }
}
