package com.decenturion.mobile.ui.fragment.settings.details.presenter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.settings.details.IDetailsSettingsInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.photo.UploadPhotoResponse;
import com.decenturion.mobile.network.response.details.ChangeDetailsResponse;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.settings.details.model.DetailsSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.details.view.IDetailsSettingsView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DetailsSettingsPresenter extends Presenter<IDetailsSettingsView> implements IDetailsSettingsPresenter {

    private IDetailsSettingsInteractor mIDetailsSettingsInteractor;
    private DetailsSettingsModelView mDetailsSettingsModelView;
    private ILocalistionManager mILocalistionManager;

    public DetailsSettingsPresenter(IDetailsSettingsInteractor iDetailsSettingsInteractor,
                                    ILocalistionManager iLocalistionManager) {
        mILocalistionManager = iLocalistionManager;
        mIDetailsSettingsInteractor = iDetailsSettingsInteractor;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDetailsSettingsModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<DetailsSettingsModelView> obs = mIDetailsSettingsInteractor.getPassportInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<DetailsSettingsModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(DetailsSettingsModelView model) {
        mDetailsSettingsModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void saveChanges(@NonNull String bio, @NonNull String telegram,
                            @NonNull String whatsApp, @NonNull String viber,
                            @NonNull String wechat) {
        showProgressView();

        Observable<ChangeDetailsResponse> obs = mIDetailsSettingsInteractor.saveChanges(
                bio,
                telegram,
                whatsApp,
                viber,
                wechat
        )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<ChangeDetailsResponse>) this::saveChangesSuccess,
                this::saveChangesFailure
        );
    }

    private void saveChangesSuccess(ChangeDetailsResponse response) {
        hideProgressView();
        showSuccessView(mILocalistionManager.getLocaleString(R.string.app_alert_saved));
        if (mView != null) {
            mView.onSavedChanges();
        }
    }

    private void saveChangesFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    @Override
    public void uploadPhoto(@NonNull Bitmap photo) {
        showProgressView();

        Observable<UploadPhotoResponse> obs = mIDetailsSettingsInteractor.uploadPhoto(photo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<UploadPhotoResponse>) this::uploadPhotoSuccess,
                this::uploadPhotoFailure
        );
    }

    private void uploadPhotoFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void uploadPhotoSuccess(UploadPhotoResponse uploadPhotoResponse) {
        hideProgressView();
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }

}
