package com.decenturion.mobile.ui.activity.referral;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.decenturion.mobile.ui.activity.SingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.referral.acquisition.view.AcquisitionFragment;
import com.decenturion.mobile.ui.fragment.referral.history.view.HistoryFragment;
import com.decenturion.mobile.ui.fragment.referral.invite.view.InviteListFragment;
import com.decenturion.mobile.ui.navigation.INavigationManager;

public class ReferralActivity extends SingleFragmentActivity {

    public static final String TYPE_SCREEN = "TYPE_SCREEN";
    public static final int INVITATIONS = 0;
    public static final int HISTORY_TRANSACTION = 1;
    public static final int ACQUISITION = 2;

    private int mTypeScreen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initArgs();

        INavigationManager iNavigationManager = getINavigationManager();
        if (savedInstanceState != null) {
            iNavigationManager.restoreInstanceState(savedInstanceState);
            return;
        }

        switch (mTypeScreen) {
            case HISTORY_TRANSACTION : {
                iNavigationManager.navigateTo(HistoryFragment.class);
                break;
            }
            case INVITATIONS : {
                iNavigationManager.navigateTo(InviteListFragment.class);
                break;
            }
            case ACQUISITION : {
                iNavigationManager.navigateTo(AcquisitionFragment.class);
                break;
            }
            default : {
                iNavigationManager.navigateTo(InviteListFragment.class);
            }
        }

    }

    private void initArgs() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mTypeScreen = intent.getIntExtra(TYPE_SCREEN, INVITATIONS);
        }
    }
}
