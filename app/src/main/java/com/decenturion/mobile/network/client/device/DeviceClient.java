package com.decenturion.mobile.network.client.device;

import com.decenturion.mobile.di.dagger.app.NetworkModule;
import com.decenturion.mobile.network.client.DHttpClient;

public class DeviceClient extends DHttpClient<IDeviceClient> {

    public DeviceClient(NetworkModule module) {
        super(module);
    }
}
