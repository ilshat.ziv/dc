package com.decenturion.mobile.ui.fragment.passport.wellcome.presenter;


import com.decenturion.mobile.business.passport.wellcome.IWellcomPassportInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.signup.skip.SignupSkipResult;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.passport.wellcome.model.WellcomePassportModelView;
import com.decenturion.mobile.ui.fragment.passport.wellcome.view.IWellcomePassportView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class WellcomePresenter extends Presenter<IWellcomePassportView> implements IWellcomePresenter {

    private IWellcomPassportInteractor mIWellcomPassportInteractor;
    private WellcomePassportModelView mWellcomePassportModelView;

    public WellcomePresenter(IWellcomPassportInteractor iWellcomPassportInteractor) {

        mIWellcomPassportInteractor = iWellcomPassportInteractor;

        mWellcomePassportModelView = new WellcomePassportModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mWellcomePassportModelView = null;
    }

    @Override
    public void bindViewData() {
        showProgressView();

        Observable<WellcomePassportModelView> obs = mIWellcomPassportInteractor.getResidentInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<WellcomePassportModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(WellcomePassportModelView model) {
        mWellcomePassportModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    @Override
    public void skipWellcomeMessage() {
        showProgressView();

        Observable<SignupSkipResult> obs = mIWellcomPassportInteractor.skipWellcomeMessage()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SignupSkipResult>) this::skipWellcomeSuccess,
                this::skipWellcomeFailure
        );
    }

    private void skipWellcomeFailure(Throwable throwable) {
        hideProgressView();
        inputDataProcessing(throwable);
    }

    private void skipWellcomeSuccess(SignupSkipResult result) {
        if (mView != null) {
            mView.onSkipedWellcome();
        }
        hideProgressView();
    }

    private void inputDataProcessing(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
