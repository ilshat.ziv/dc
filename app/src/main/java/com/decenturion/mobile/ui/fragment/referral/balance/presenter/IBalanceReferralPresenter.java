package com.decenturion.mobile.ui.fragment.referral.balance.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.balance.view.IBalanceReferralView;

public interface IBalanceReferralPresenter extends IPresenter<IBalanceReferralView> {

    void bindViewData();
}
