package com.decenturion.mobile.app.lifecycle;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.activity.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class AppLifecycleManager implements IAppLifecycleManager {

    private boolean isAppForeground;
    private List<OnAppLifecycleStateListener> mOnAppLifecycleStateListeners = new ArrayList<>();

    public AppLifecycleManager(@NonNull Application app) {
        Lifecycler lifecycler = new Lifecycler();
        app.registerActivityLifecycleCallbacks(lifecycler);
    }

    @Override
    public boolean isAppForeground() {
        return this.isAppForeground;
    }

    public void addOnAppLifecycleStateListener(OnAppLifecycleStateListener listener) {
        mOnAppLifecycleStateListeners.add(listener);
    }

    public void removeOnAppLifecycleStateListener(OnAppLifecycleStateListener listener) {
        mOnAppLifecycleStateListeners.add(listener);
    }

    private class Lifecycler implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (activity instanceof MainActivity) {
                AppLifecycleManager.this.isAppForeground = true;
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {
            if (activity instanceof MainActivity) {
                AppLifecycleManager.this.isAppForeground = true;
                for (OnAppLifecycleStateListener listener : mOnAppLifecycleStateListeners) {
                    listener.onAppForeground();
                }
            }
        }

        @Override
        public void onActivityResumed(Activity activity) {
            if (activity instanceof MainActivity) {
                AppLifecycleManager.this.isAppForeground = true;
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
            if (activity instanceof MainActivity) {
                AppLifecycleManager.this.isAppForeground = false;
                for (OnAppLifecycleStateListener listener : mOnAppLifecycleStateListeners) {
                    listener.onAppBackground();
                }
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            if (activity instanceof MainActivity) {
                AppLifecycleManager.this.isAppForeground = false;
            }
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }
    }
}
