package com.decenturion.mobile.ui.fragment.profile.passport.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.view.IPassportView;

public interface IPassportPresenter extends IPresenter<IPassportView> {

    void bindViewData();

    PassportModelView getProfileView();
}
