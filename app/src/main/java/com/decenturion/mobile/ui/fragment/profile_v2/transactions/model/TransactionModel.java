package com.decenturion.mobile.ui.fragment.profile_v2.transactions.model;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.network.response.model.Transaction;
import com.decenturion.mobile.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

public class TransactionModel {

    private String mTraidId;
    private String mTraidDate;
    private String mTraidAmount;
    private String mCoin;
    private String mTraidStatus;
    private String mCategory;

    public TransactionModel(Transaction trade) {
        String createdAt = trade.getCreatedAt();
        mTraidId = createdAt;

        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            mTraidDate = "Сегодня " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mTraidDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }


        mTraidAmount = trade.getAmount();
        mCoin = trade.getCoin();
        mTraidStatus = trade.getStatus();

        mCategory = trade.getCategory();
    }

    public TransactionModel(Transaction transaction, ILocalistionManager iLocalistionManager) {
        String createdAt = transaction.getCreatedAt();
        mTraidId = createdAt;

        long datetime = DateTimeUtils.getDatetime(createdAt, DateTimeUtils.Format.FORMAT_ISO_8601);
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()) == TimeUnit.MILLISECONDS.toDays(datetime)) {
            String s = iLocalistionManager.getLocaleString(R.string.app_constants_today);
            mTraidDate = s + " " + DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.TIME);
        } else {
            mTraidDate = DateTimeUtils.getDateFormat(datetime, DateTimeUtils.Format.DEAL_DATETIME);
        }


        mTraidAmount = transaction.getAmount();
        mCoin = transaction.getCoin();
        mTraidStatus = transaction.getStatus();

        mCategory = transaction.getCategory();
    }

    public String getTraidId() {
        return mTraidId;
    }

    public String getTraidDate() {
        return mTraidDate;
    }

    public String getTraidAmount() {
        return mTraidAmount;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getTraidStatus() {
        return mTraidStatus;
    }

    public String getCategory() {
        return mCategory;
    }
}
