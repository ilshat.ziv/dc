package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wallet {

    @JsonProperty("uuid")
    private String mUuid;

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("balance")
    private String mBalance;

    public Wallet() {
    }

    @JsonIgnore
    public String getUuid() {
        return mUuid;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getAddress() {
        return mAddress;
    }

    @JsonIgnore
    public String getBalance() {
        return mBalance;
    }
}
