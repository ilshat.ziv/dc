package com.decenturion.mobile.di.dagger.twofa;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.twofa.EnableTwoFAInteractor;
import com.decenturion.mobile.business.twofa.IEnableTwoFAInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.twofa.presenter.EnableTwoFAPresenter;
import com.decenturion.mobile.ui.fragment.twofa.presenter.IEnableTwoFAPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class EnableTwoFAModule {

    @Provides
    @EnableTwoFAScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @EnableTwoFAScope
    IEnableTwoFAInteractor provideIEnableTwoFAInteractor(IResidentClient iResidentClient,
                                                   IResidentRepository iResidentRepository) {
        return new EnableTwoFAInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @EnableTwoFAScope
    IEnableTwoFAPresenter provideIEnableTwoFAPresenter(IEnableTwoFAInteractor iEnableTwoFAInteractor,
                                                    ILocalistionManager iLocalistionManager) {
        return new EnableTwoFAPresenter(iEnableTwoFAInteractor, iLocalistionManager);
    }
}
