package com.decenturion.mobile.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class BaseRecyclerView extends RecyclerView {

    private OnRecycleScrollListener mOnScrollListener;

    public BaseRecyclerView(Context context) {
        super(context);
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        this.addOnScrollListener(new BaseScrollRecyclerListener());
    }

    public void setOnScrollListener(OnRecycleScrollListener onScrollListener) {
        mOnScrollListener = onScrollListener;
    }

    public class BaseScrollRecyclerListener extends RecyclerView.OnScrollListener {

        BaseScrollRecyclerListener() {
            super();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) {
                int pastVisiblesItems = 0;

                RecyclerView.LayoutManager layoutManager = getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    pastVisiblesItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                }
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();

                if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    if (mOnScrollListener != null) {
                        mOnScrollListener.onBottomScrolled();
                    }
                }
            }
        }
    }

    public interface OnRecycleScrollListener {
        void onBottomScrolled();
    }
}
