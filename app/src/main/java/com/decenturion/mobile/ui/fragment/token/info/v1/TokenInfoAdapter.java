package com.decenturion.mobile.ui.fragment.token.info.v1;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.ui.fragment.token.info.v1.model.InfoModelView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenInfoAdapter extends RecyclerView.Adapter<TokenInfoAdapter.ViewHolder> {

    private Context mContext;
    private IResourceManager mIResourceManager;
    private final LayoutInflater mInflater;
    private List<InfoModelView> mInfoModelViews;

    public TokenInfoAdapter(@NonNull Context context, @NonNull IResourceManager iResourceManager) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mIResourceManager = iResourceManager;
    }

    public void replaceDataSet(List<InfoModelView> list) {
        mInfoModelViews = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return CollectionUtils.size(mInfoModelViews);
    }

    @NonNull
    @Override
    public TokenInfoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.view_token_info_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TokenInfoAdapter.ViewHolder holder, int position) {
        final InfoModelView param = mInfoModelViews.get(position);
        holder.bind(param);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        @BindView(R.id.tokenInfoNameView)
        TextView textView;

        @BindView(R.id.tokenInfoParamsView)
        RecyclerView tokenInfoParamsView;
        TokenInfoParamAdapter tokenInfoParamAdapter;

        InfoModelView infoModelView;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);

            tokenInfoParamAdapter = new TokenInfoParamAdapter(mContext);
            tokenInfoParamsView.setLayoutManager(new LinearLayoutManager(mContext));
            tokenInfoParamsView.setAdapter(tokenInfoParamAdapter);
        }

        public void bind(@NonNull InfoModelView model) {
            this.infoModelView = model;

            this.textView.setText(model.getName());

            Drawable drawable = mIResourceManager.getDrawable(R.drawable.bg_token_info_default);
            if (model.isSelected()) {
                drawable = mIResourceManager.getDrawable(R.drawable.bg_token_info_selected);
            }
            this.view.setBackground(drawable);

            tokenInfoParamAdapter.replaceDataSet(model.getInfoParamsModelViews());
        }
    }

}
