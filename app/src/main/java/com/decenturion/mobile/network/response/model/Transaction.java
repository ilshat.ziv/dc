package com.decenturion.mobile.network.response.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("created_at")
    private String mCreatedAt;

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("category")
    private String mCategory;

    @JsonProperty("amount")
    private String mAmount;

    @JsonProperty("coin")
    private String mCoin;

    //"expired_at": "2018-08-29T04:59:44.008619422Z",
    @JsonProperty("payment_date")
    private String mPaymentDate;

    @JsonProperty("payment_amount")
    private String mPaymentAmount;

    @JsonProperty("payment_currency")
    private String mPaymentCurrency;

    @JsonProperty("payment_status")
    private String mPaymentStatus;


    public Transaction() {
    }

    @JsonIgnore
    public String getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getStatus() {
        return mStatus;
    }

    @JsonIgnore
    public String getCreatedAt() {
        return mCreatedAt;
    }

    @JsonIgnore
    public String getCategory() {
        return mCategory;
    }

    @JsonIgnore
    public String getPaymentDate() {
        return mPaymentDate;
    }

    @JsonIgnore
    public String getPaymentAmount() {
        return mPaymentAmount;
    }

    @JsonIgnore
    public String getPaymentCurrency() {
        return mPaymentCurrency;
    }

    @JsonIgnore
    public String getPaymentStatus() {
        return mPaymentStatus;
    }
}
