package com.decenturion.mobile.ui.dialog.fullscreen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.R;
import com.decenturion.mobile.ui.dialog.fullscreen.calendar.DatePicker;
import com.decenturion.mobile.utils.DateTimeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class DateDialog extends FullScreenDialogFragment {

	@Nullable
	@BindView(R.id.calendarView)
	protected DatePicker mCalendarView;

	private DatePicker.OnChangeDateListener mOnChangeDateListener;
	private long mDateTime;

	private int mRequestCode;
	private String mTitle;

	public static void show(@NonNull FragmentManager fm, int requestCode,
							DatePicker.OnChangeDateListener changeDateListener,
							long date, String title) {
		final DateDialog dialog = new DateDialog(changeDateListener, date, title);

		Bundle bundle = new Bundle();
		bundle.putInt(Args.RQUEST_CODE, requestCode);
		dialog.setArguments(bundle);

		dialog.show(fm, DateDialog.class.getName());
	}

	public DateDialog() {
	}

	public DateDialog(DatePicker.OnChangeDateListener changeDateListener) {
		mOnChangeDateListener = changeDateListener;
	}

	public DateDialog(DatePicker.OnChangeDateListener changeDateListener, long datetime) {
		this(changeDateListener);
		mDateTime = (datetime == 0L) ? System.currentTimeMillis() : datetime;
	}

	public DateDialog(DatePicker.OnChangeDateListener changeDateListener, String title) {
		this(changeDateListener);
		mDateTime = System.currentTimeMillis();
		mTitle = title;
	}

	public DateDialog(DatePicker.OnChangeDateListener changeDateListener, long datetime, String title) {
		this(changeDateListener, datetime);
		mTitle = title;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			mRequestCode = bundle.getInt(Args.RQUEST_CODE, 0);
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		setContentView(R.layout.fr_bs_get_date);
		setFullVisual(false);
		setTitleText(mTitle);
		ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mCalendarView.setDateTime(mDateTime);
	}

	@Override
	public void onPositiveClickButton() {
		long date = mCalendarView.getDate();
//		date = DateTimeUtils.convertToGMT(date);
		if (mOnChangeDateListener != null) {
			mOnChangeDateListener.changeData(date);
			Intent intent = new Intent();
//			intent.putExtra(Body.DATE, mCalendarView.get());
//			OttoBus.getInstance().post(OnActivityResult.produce(mRequestCode, Activity.RESULT_OK, intent));
		}

		dismiss();
	}

	public interface Args {
		String RQUEST_CODE = "REQUEST_CODE";
		String TITLE = "TITLE";
	}

	public interface Body {
		String DATE = "DATE";
	}
}