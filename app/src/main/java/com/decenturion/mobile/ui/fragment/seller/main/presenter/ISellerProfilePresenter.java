package com.decenturion.mobile.ui.fragment.seller.main.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.seller.main.view.ISellerProfileView;

public interface ISellerProfilePresenter extends IPresenter<ISellerProfileView> {

    void bindViewData(@NonNull String residentUuid);

    void onCopyProfileLink(@NonNull String residentUuid);

    void onShare(@NonNull String residentUuid);
}
