package com.decenturion.mobile.database.type.model;

import com.decenturion.mobile.network.response.model.TokenCoin;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CoinTypeModel extends TokenCoin {

    public CoinTypeModel() {
    }

    @JsonIgnore
    public CoinTypeModel(TokenCoin coin) {
        setContract(coin.getContract());
        setDecimals(coin.getDecimals());
        setName(coin.getName());
        setSymbol(coin.getSymbol());
        setUuid(coin.getUuid());
    }
}
