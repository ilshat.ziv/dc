package com.decenturion.mobile.ui.fragment.settings.password.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.settings.password.PasswordSettingsComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.component.FloatView;
import com.decenturion.mobile.ui.component.PasswordView;
import com.decenturion.mobile.ui.floating.FloatViewManager;
import com.decenturion.mobile.ui.floating.IFloatViewManager;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.settings.password.model.PasswordSettingsModelView;
import com.decenturion.mobile.ui.fragment.settings.password.presenter.IPasswordSettingsPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PasswordSettingsFragment extends BaseFragment implements IPasswordSettingsView
        , View.OnClickListener {

    /* UI */

    @BindView(R.id.currenPasswordView)
    PasswordView mCurrenPasswordView;

    @BindView(R.id.newPasswordView)
    PasswordView mNewPasswordView;

    @BindView(R.id.retypePasswordView)
    PasswordView mRetypePasswordView;

    @BindView(R.id.nestedScroolView)
    NestedScrollView mNestedScroolView;

    @BindView(R.id.controlsView)
    FloatView mControlsView;

    private IFloatViewManager mIFloatViewManager;


    /* DI */

    @Inject
    IPasswordSettingsPresenter mIPasswordSettingsPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();

        mIFloatViewManager = new FloatViewManager(requireActivity());
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        PasswordSettingsComponent passwordSettingsComponent = appDelegate
                .getIDIManager()
                .plusPasswordSettingsComponent();
        passwordSettingsComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_password_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();

        mCurrenPasswordView.showLabel();
        mCurrenPasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_current_title));
        mCurrenPasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_current_placeholder));
        mCurrenPasswordView.setVisibleDivider(true);

        mNewPasswordView.showLabel();
        mNewPasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_new_title));
        mNewPasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_new_placeholder));
        mNewPasswordView.setVisibleDivider(true);

        mRetypePasswordView.showLabel();
        mRetypePasswordView.setLabel(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_retype_title));
        mRetypePasswordView.setHint(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_retype_placeholder));
        mRetypePasswordView.setVisibleDivider(true);

        mIFloatViewManager.bindIFloatView(mControlsView);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(mILocalistionManager.getLocaleString(R.string.app_settings_changepassword_title));
        iToolbarController.setNavigationOnClickListener(this);
        iToolbarController.setNavigationIcon(R.drawable.ic_close_24dp);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIPasswordSettingsPresenter.bindView(this);
        mIPasswordSettingsPresenter.bindViewData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIPasswordSettingsPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIPasswordSettingsPresenter.unbindView();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIPasswordSettingsPresenter.unbindView();
        mIFloatViewManager.unbindIFloatView(mControlsView);
    }

    @Override
    public void onBindViewData(@NonNull PasswordSettingsModelView model) {
        mCurrenPasswordView.setText(model.getCurrentPassword());
        mNewPasswordView.setText(model.getNewPassword());
        mRetypePasswordView.setText(model.getReypePassword());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

    @OnClick(R.id.saveChangesButtonView)
    protected void onSaveChanges() {
        mIPasswordSettingsPresenter.saveNewPassword(
                mCurrenPasswordView.getValue(),
                mNewPasswordView.getValue(),
                mRetypePasswordView.getValue());
    }
}