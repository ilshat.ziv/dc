package com.decenturion.mobile.database.dao;

import com.decenturion.mobile.database.model.OrmTraideModel;
import com.j256.ormlite.support.ConnectionSource;

public class OrmTraideDAO extends BaseDao<OrmTraideModel, Integer> {

    public OrmTraideDAO(ConnectionSource connectionSource,
                        Class<OrmTraideModel> dataClass) throws Exception {

        super(connectionSource, dataClass);
    }
}
