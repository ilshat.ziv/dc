package com.decenturion.mobile.business.tokendetails.details;

import com.decenturion.mobile.ui.fragment.token.details.model.TokenDetailsModelView;

import rx.Observable;

public interface ITokenDetailsInteractor {

    Observable<TokenDetailsModelView> getTokenDetails(int tokenId);

    Observable<TokenDetailsModelView> getTokenDetails(TokenDetailsModelView modelView);
}
