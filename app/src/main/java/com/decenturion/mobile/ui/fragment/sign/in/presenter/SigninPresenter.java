package com.decenturion.mobile.ui.fragment.sign.in.presenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.business.signin.ISigninInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.http.exception.RestrictedException;
import com.decenturion.mobile.network.response.signin.SigninResult;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.architecture.view.IScreenActivityView;
import com.decenturion.mobile.ui.fragment.sign.in.model.SigninModelView;
import com.decenturion.mobile.ui.fragment.sign.in.view.ISigninView;
import com.decenturion.mobile.utils.CollectionUtils;
import com.decenturion.mobile.utils.LoggerUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SigninPresenter extends Presenter<ISigninView> implements ISigninPresenter {

    private ISigninInteractor mISigninInteractor;
    private SigninModelView mSigninModelView;
    private IPrefsManager mIPrefsManager;

    public SigninPresenter(ISigninInteractor iSigninInteractor, IPrefsManager iPrefsManager) {
        mISigninInteractor = iSigninInteractor;
        mIPrefsManager = iPrefsManager;

        mSigninModelView = new SigninModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mSigninModelView = null;
    }

    @Override
    public void bindViewData() {
        Observable<SigninModelView> obs = Observable.just(mSigninModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SigninModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        bundle.putString("username", mSigninModelView.getEmail());
        bundle.putString("password", mSigninModelView.getPassword());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle,
                                    @NonNull String username, @NonNull String password) {
        bundle.putString("username", username);
        bundle.putString("password", password);
    }

    @Override
    public void onViewStateRestored(@NonNull Bundle savedInstanceState) {
        mSigninModelView.setUsername(savedInstanceState.getString("username", ""));
        mSigninModelView.setPassword(savedInstanceState.getString("password", ""));

        Observable<SigninModelView> obs = Observable.just(mSigninModelView)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SigninModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(SigninModelView model) {
        mSigninModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    @Override
    public void signin(@NonNull String username,
                       @NonNull String password,
                       String captcha) {

        mSigninModelView = new SigninModelView();
        mSigninModelView.setUsername(username);
        mSigninModelView.setPassword(password);

        showProgressView();

        Observable<SigninResult> obs = mISigninInteractor.signin(username, password, captcha)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<SigninResult>) this::signinSuccess,
                this::signinFailure
        );
    }

    private void signinSuccess(SigninResult result) {
        hideProgressView();
        Resident resident = result.getResident();

        int step = resident.getStep();
        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, step);

        if (mView != null) {
            mView.onSigninSuccess(step);
        }
    }

    private void signinFailure(Throwable throwable) {
        hideProgressView();

        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                String key = error.getKey();
                switch (key) {
//                    case "username" : {
//                        if (mView != null) {
//                            mView.onResendEmail(mSigninModelView.getEmail());
//                        }
//                        return;
//                    }
                    case "recaptcha": {
                        reCaptcha();
                        return;
                    }
                    case "g2fa": {
                        if (mView != null) {
                            mView.onSignByG2fa();
                        }
                        return;
                    }
                }

                showErrorView(error.getMessage());
            }
        } else if (throwable instanceof RestrictedException) {
            if (mView != null) {
                mView.onAccessRestricted();
            }
        } else {
            showErrorView(throwable);
        }
    }

    private void reCaptcha() {
        if (mView == null) {
            return;
        }

        IScreenActivityView iScreenActivityView = mView.getIScreenActivityView();
        Activity activity = (Activity) iScreenActivityView;
        SafetyNet.getClient(activity)
                .verifyWithRecaptcha(BuildConfig.CAPTCHA_ANDROID_KEY)
                .addOnSuccessListener(activity,
                        response -> {
                            String tokenResult = response.getTokenResult();
                            signin(
                                    mSigninModelView.getEmail(),
                                    mSigninModelView.getPassword(),
                                    tokenResult
                            );
                        })
                .addOnFailureListener(activity, e -> {
                    if (e instanceof ApiException) {
                        ApiException apiException = (ApiException) e;
                        int statusCode = apiException.getStatusCode();
                        String statusCodeString = CommonStatusCodes.getStatusCodeString(statusCode);

                        LoggerUtils.exception(new Exception("Error: ApiException status code" + statusCodeString));
                    } else {
                        LoggerUtils.exception(new Exception("Error: " + e.getMessage()));
                    }

                    if (mView != null) {
                        showErrorView("Google authorization error, please try again");
                    }
                });
    }
}
