package com.decenturion.mobile.ui.fragment.token.info.v2.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.resource.IResourceManager;
import com.decenturion.mobile.di.dagger.token.info.TokenInfoComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.token.info.v2.TokenInfoParamAdapter;
import com.decenturion.mobile.ui.fragment.token.info.v2.model.TokenInfoModelView;
import com.decenturion.mobile.ui.fragment.token.info.v2.presenter.ICategoryTokenPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;

import javax.inject.Inject;

import butterknife.BindView;

public class CategoryTokenFragment extends BaseFragment implements ICategoryTokenView
        , View.OnClickListener {

    public static final String CATEGORY = "CATEGORY";
    private String mTokenCategory;

    /* UI */

    @BindView(R.id.tokenInfoListView)
    RecyclerView mTokenInfoListView;
    private TokenInfoParamAdapter mTokenInfoParamAdapter;


    /* DI */

    @Inject
    ICategoryTokenPresenter mICategoryTokenPresenter;

    @Inject
    IResourceManager mIResourceManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArgs();
        injectToDagger();
    }

    private void initArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTokenCategory = arguments.getString(CATEGORY);
        }
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        TokenInfoComponent tokenInfoComponent = appDelegate
                .getIDIManager()
                .plusTokenInfoComponent();
        tokenInfoComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_token_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
    }

    private void setupUi() {
        Context context = getContext();
        assert context != null;
        mTokenInfoListView.setLayoutManager(new LinearLayoutManager(context));
        mTokenInfoParamAdapter = new TokenInfoParamAdapter(context);
        mTokenInfoListView.setAdapter(mTokenInfoParamAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mICategoryTokenPresenter.bindView(this);
        mICategoryTokenPresenter.bindViewData(mTokenCategory);
    }

    @Override
    public void onBindViewData(@NonNull TokenInfoModelView model) {
        mTokenInfoParamAdapter.replaceDataSet(model.getInfoModelViews());
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) getActivity();
        assert activity != null;
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }

}
