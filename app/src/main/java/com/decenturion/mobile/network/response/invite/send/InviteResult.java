package com.decenturion.mobile.network.response.invite.send;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteResult {

    @JsonProperty("email")
    private String mResult;

    public InviteResult() {
    }

    @JsonIgnore
    public String getResult() {
        return mResult;
    }
}
