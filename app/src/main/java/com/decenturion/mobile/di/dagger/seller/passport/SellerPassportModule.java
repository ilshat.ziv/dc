package com.decenturion.mobile.di.dagger.seller.passport;

import android.content.Context;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.business.passport.IPassportInteractor;
import com.decenturion.mobile.business.passport.PassportInteractor;
import com.decenturion.mobile.business.seller.passport.ISellerPassportInteractor;
import com.decenturion.mobile.business.seller.passport.SellerPassportInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.repository.resident.ResidentRepository;
import com.decenturion.mobile.ui.fragment.profile.passport.presenter.IPassportPresenter;
import com.decenturion.mobile.ui.fragment.profile.passport.presenter.PassportPresenter;
import com.decenturion.mobile.ui.fragment.seller.passport.presenter.ISellerPassportPresenter;
import com.decenturion.mobile.ui.fragment.seller.passport.presenter.SellerPassportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SellerPassportModule {

    @Provides
    @SellerPassportScope
    IResidentRepository provideIResidentRepository(IPrefsManager iPrefsManager, OrmHelper ormHelper) {
        return new ResidentRepository(iPrefsManager, ormHelper);
    }

    @Provides
    @SellerPassportScope
    ISellerPassportInteractor provideISellerPassportInteractor(IResidentClient iResidentClient,
                                                         IResidentRepository iResidentRepository) {
        return new SellerPassportInteractor(iResidentClient, iResidentRepository);
    }

    @Provides
    @SellerPassportScope
    ISellerPassportPresenter provideISellerPassportPresenter(Context context,
                                                             ISellerPassportInteractor iSellerPassportInteractor,
                                                             ILocalistionManager iLocalistionManager) {
        return new SellerPassportPresenter(context, iSellerPassportInteractor, iLocalistionManager);
    }
}
