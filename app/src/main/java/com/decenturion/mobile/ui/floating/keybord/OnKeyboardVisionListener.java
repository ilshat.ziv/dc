package com.decenturion.mobile.ui.floating.keybord;

public interface OnKeyboardVisionListener {

    void onShowKeyboard();

    void onHideKeyboard();
}
