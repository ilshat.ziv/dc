package com.decenturion.mobile.ui.fragment.referral.invite.view;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decenturion.mobile.AppDelegate;
import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.di.dagger.referral.invite.InviteListComponent;
import com.decenturion.mobile.ui.activity.ISingleFragmentActivity;
import com.decenturion.mobile.ui.activity.main.IMainActivity;
import com.decenturion.mobile.ui.dialog.IDialogManager;
import com.decenturion.mobile.ui.dialog.referral.RemoveInviteDialog;
import com.decenturion.mobile.ui.dialog.referral.invite.NewInviteDialog;
import com.decenturion.mobile.ui.fragment.BaseFragment;
import com.decenturion.mobile.ui.fragment.referral.invite.InviteListAdapter;
import com.decenturion.mobile.ui.fragment.referral.invite.create.view.CreateInviteFragment;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteModel;
import com.decenturion.mobile.ui.fragment.referral.invite.model.InviteListModelView;
import com.decenturion.mobile.ui.fragment.referral.invite.presenter.IInviteListPresenter;
import com.decenturion.mobile.ui.navigation.INavigationManager;
import com.decenturion.mobile.ui.toolbar.IToolbarController;
import com.decenturion.mobile.ui.widget.BaseRecyclerView;
import com.decenturion.mobile.ui.widget.RefreshView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class InviteListFragment extends BaseFragment implements IInviteListView,
        InviteListAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        BaseRecyclerView.OnRecycleScrollListener,
        View.OnClickListener {

    /* UI */

    @BindView(R.id.refreshView)
    RefreshView mRefreshView;

    @BindView(R.id.invitationListView)
    BaseRecyclerView mInvitationListView;
    private InviteListAdapter mInviteListAdapter;

    /* DI */

    @Inject
    IInviteListPresenter mIInviteListPresenter;

    @Inject
    ILocalistionManager mILocalistionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectToDagger();
    }

    private void injectToDagger() {
        AppDelegate appDelegate = getApplication();
        InviteListComponent component = appDelegate
                .getIDIManager()
                .plusInviteListComponent();
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_invitation_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        initToolbar();

        mRefreshView.setOnRefreshListener(this);
        mInvitationListView.setOnScrollListener(this);

        Context context = getContext();
        assert context != null;
        mInvitationListView.setLayoutManager(new LinearLayoutManager(context));
        mInviteListAdapter = new InviteListAdapter(context, this);
        mInvitationListView.setAdapter(mInviteListAdapter);
    }

    private void initToolbar() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        String string = mILocalistionManager.getLocaleString(R.string.app_referral_invite_list_title);
        iToolbarController.setTitle(string.replace("{count}", "0"));
        iToolbarController.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        iToolbarController.setNavigationOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIInviteListPresenter.bindView(this);
//        if (savedInstanceState == null ||
//                !savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIInviteListPresenter.bindViewData();
//        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null &&
                savedInstanceState.getBoolean(this.getClass().getSimpleName(), false)) {
            mIInviteListPresenter.onViewStateRestored(savedInstanceState);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mIInviteListPresenter.bindView(this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mIInviteListPresenter.unbindView();
        outState.putBoolean(this.getClass().getSimpleName(), true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBindViewData(@NonNull InviteListModelView model) {
        mInviteListAdapter.replaceDataSet(model.getHistoryList());

        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        String string = mILocalistionManager.getLocaleString(R.string.app_referral_invite_list_title);
        iToolbarController.setTitle(string.replace("{count}", String.valueOf(model.getCountUnuseInvites())));
    }

    @Override
    public void onItemClick(@NonNull InviteModel model) {
        Context context = getContext();
        assert context != null;
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("InviteModel", model.getUrl());
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            showMessage(mILocalistionManager.getLocaleString(R.string.app_alert_copied));
        }
    }

    @Override
    public void onRemoveInvite(@NonNull InviteModel model) {
        Bundle bundle = new Bundle();
        bundle.putInt(RemoveInviteDialog.INVITE_ID, model.getId());
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        IDialogManager iDialogManager = activity.getIDialogManager();
        iDialogManager.showDialog(
                RemoveInviteDialog.class,
                bundle,
                this,
                IMainActivity.ACTION_REMOVE_INVITE
        );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMainActivity.ACTION_REMOVE_INVITE : {
                if (resultCode == Activity.RESULT_OK) {
                    int inviteId = data.getIntExtra(RemoveInviteDialog.INVITE_ID, 0);
                    mIInviteListPresenter.removeInvite(inviteId);
                }
                break;
            }
            case IMainActivity.ACTION_CREATE_INVITE : {
                if (resultCode == Activity.RESULT_OK) {
                    String inviteUrl = data.getStringExtra(CreateInviteFragment.INVITE_URL);
                    Bundle bundle = new Bundle();
                    bundle.putString(NewInviteDialog.INVITE_URL, inviteUrl);
                    ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
                    IDialogManager iDialogManager = activity.getIDialogManager();
                    iDialogManager.showDialog(NewInviteDialog.class, bundle);
                }
                break;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIInviteListPresenter.unbindView();
    }

    @Override
    public void showProgressView() {
        mRefreshView.show();
    }

    @Override
    public void hideProgressView() {
        mRefreshView.hide();
    }

    @Override
    public void onRefresh() {
        mIInviteListPresenter.bindViewData();
    }

    @Override
    public void onBottomScrolled() {
        mIInviteListPresenter.onBottomScrolled();
    }

    @OnClick(R.id.createInviteButtonView)
    protected void onCreateInvite() {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateTo(
                CreateInviteFragment.class,
                true,
                this,
                IMainActivity.ACTION_CREATE_INVITE);
    }

    @Override
    public void onClick(View v) {
        ISingleFragmentActivity activity = (ISingleFragmentActivity) requireActivity();
        INavigationManager iNavigationManager = activity.getINavigationManager();
        iNavigationManager.navigateToBack();
    }
}
