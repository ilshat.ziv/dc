package com.decenturion.mobile.ui.fragment.referral.main.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.referral.main.model.ReferralModelView;

public interface IReferralView extends IScreenFragmentView {

    void onBindViewData(@NonNull ReferralModelView model);

}
