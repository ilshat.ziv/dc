package com.decenturion.mobile.ui.fragment.pass.confirm.phone.presenter;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.pass.confirm.phone.view.IConfirmPhoneRestorePassView;

public interface IConfirmPhoneRestorePassPresenter extends IPresenter<IConfirmPhoneRestorePassView> {

    void sendSmsCode(@NonNull String data);

    void bindViewData();
}
