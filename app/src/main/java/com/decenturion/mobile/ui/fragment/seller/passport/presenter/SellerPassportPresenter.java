package com.decenturion.mobile.ui.fragment.seller.passport.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decenturion.mobile.R;
import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.seller.passport.ISellerPassportInteractor;
import com.decenturion.mobile.network.response.model.Country;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.pass.edit.model.CountryModelView;
import com.decenturion.mobile.ui.fragment.pass.edit.model.Sex;
import com.decenturion.mobile.ui.fragment.pass.edit.model.SexModelView;
import com.decenturion.mobile.ui.fragment.profile.passport.model.PassportModelView;
import com.decenturion.mobile.ui.fragment.seller.passport.view.ISellerPassportView;
import com.decenturion.mobile.utils.FileUtils;
import com.decenturion.mobile.utils.JsonUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SellerPassportPresenter extends Presenter<ISellerPassportView> implements ISellerPassportPresenter {

    private Context mContext;
    private ISellerPassportInteractor mISellerPassportInteractor;
    private ILocalistionManager mILocalistionManager;

    private PassportModelView mPassportModelView;
    private CountryModelView mCountryModelView;
    private SexModelView mSexModelView;

    public SellerPassportPresenter(Context context,
                                   ISellerPassportInteractor iSellerPassportInteractor,
                                   ILocalistionManager iLocalistionManager) {
        mContext = context;
        mISellerPassportInteractor = iSellerPassportInteractor;
        mILocalistionManager = iLocalistionManager;
        mPassportModelView = new PassportModelView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mPassportModelView = null;
    }

    @Override
    public void bindViewData(@NonNull String residentUuid) {
        showProgressView();

        Observable<PassportModelView> obs = mISellerPassportInteractor.getProfileInfo(residentUuid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<PassportModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    @Override
    public PassportModelView getProfileView() {
        return mPassportModelView;
    }

    private void bindViewDataSuccess(PassportModelView model) {
        mPassportModelView = model;

        ArrayList<Sex> sexData = new ArrayList<>();
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_male), "male"));
        sexData.add(new Sex(mILocalistionManager.getLocaleString(R.string.app_sex_female), "female"));
        mSexModelView = new SexModelView(sexData);

        ArrayList<Country> list = initCountryData();
        mCountryModelView = new CountryModelView(list);

        if (mView != null) {
            mView.onBindViewData(mSexModelView, mCountryModelView, model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        showErrorView(throwable);
    }

    private ArrayList<Country> initCountryData() {
        String json = mILocalistionManager.getLocaleResource("data/country");
        Object obj = JsonUtils.getCollectionFromJson(json, Country.class);
        return (ArrayList<Country>) obj;
    }
}
