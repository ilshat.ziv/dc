package com.decenturion.mobile.ui.fragment.profile_v2.main.presenter;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.profile_v2.main.view.IProfileView;

public interface IProfilePresenter extends IPresenter<IProfileView> {

    void bindViewData();

    void onCopyWalletAddress();

    void onCopyProfileLink();

    void onCopyReferalLinc();

    void onShare();
}
