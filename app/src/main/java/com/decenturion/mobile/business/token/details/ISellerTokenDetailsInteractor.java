package com.decenturion.mobile.business.token.details;

import com.decenturion.mobile.ui.fragment.token.seller.details.model.SellerTokenDetailsModelView;

import rx.Observable;

public interface ISellerTokenDetailsInteractor {

    Observable<SellerTokenDetailsModelView> getTokenDetails(int tokenId);

    Observable<SellerTokenDetailsModelView> getTokenDetails(SellerTokenDetailsModelView modelView);
}
