package com.decenturion.mobile.network.response.payment.deliver;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliverPaymentResult {

    @JsonProperty("coin")
    private String mCoin;

    @JsonProperty("address")
    private String mAddress;

    @JsonProperty("amount")
    private double mAmount;

    @JsonProperty("amount_left")
    private double mAmountLeft;

    public DeliverPaymentResult() {
    }

    @JsonIgnore
    public String getCoin() {
        return mCoin;
    }

    @JsonIgnore
    public String getAddress() {
        return mAddress;
    }

    @JsonIgnore
    public double getAmount() {
        return mAmount;
    }

    @JsonIgnore
    public double getAmountLeft() {
        return mAmountLeft;
    }

    @JsonIgnore
    public void setCoin(String coin) {
        mCoin = coin;
    }

    @JsonIgnore
    public void setAddress(String address) {
        mAddress = address;
    }

    @JsonIgnore
    public void setAmount(double amount) {
        mAmount = amount;
    }

    @JsonIgnore
    public void setAmountLeft(double amountLeft) {
        mAmountLeft = amountLeft;
    }
}
