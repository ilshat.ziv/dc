package com.decenturion.mobile.di.dagger.profile.actions;

import com.decenturion.mobile.ui.fragment.profile_v2.log.view.LogsFragment;
import com.decenturion.mobile.ui.fragment.settings.action.view.ActionListSettingsFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        LogsModule.class
})

@LogsScope
public interface LogsComponent {

    void inject(LogsFragment view);
}
