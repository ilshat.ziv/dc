package com.decenturion.mobile.network.response.referral.invite.create;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateInviteResult {

    @JsonProperty("id")
    private int mId;

    @JsonProperty("url")
    private String mUrl;

    @JsonProperty("price")
    private String mPrice;

    //"2018-10-19T12:56:47.708290916Z"
    @JsonProperty("created_at")
    private String mDate;

    public CreateInviteResult() {
    }

    @JsonIgnore
    public int getId() {
        return mId;
    }

    @JsonIgnore
    public String getUrl() {
        return mUrl;
    }

    @JsonIgnore
    public String getPrice() {
        return mPrice;
    }

    @JsonIgnore
    public String getDate() {
        return mDate;
    }
}
