package com.decenturion.mobile.ui.fragment.header.presenter;

import com.decenturion.mobile.business.header.IHeaderInteractor;
import com.decenturion.mobile.network.http.exception.InputDataException;
import com.decenturion.mobile.network.response.model.Error;
import com.decenturion.mobile.ui.architecture.presenter.Presenter;
import com.decenturion.mobile.ui.fragment.header.model.HeaderModelView;
import com.decenturion.mobile.ui.fragment.header.view.IHeaderView;
import com.decenturion.mobile.utils.CollectionUtils;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class HeaderPresenter extends Presenter<IHeaderView> implements IHeaderPresenter {

    private IHeaderInteractor mIHeaderInteractor;
    private HeaderModelView mHeaderModelView;

    public HeaderPresenter(IHeaderInteractor iProfileInteractor) {
        mIHeaderInteractor = iProfileInteractor;
    }

    @Override
    public void bindViewData() {
        Observable<HeaderModelView> obs = mIHeaderInteractor.getProfileInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mIDCompositeSubscription.subscribe(obs,
                (Action1<HeaderModelView>) this::bindViewDataSuccess,
                this::bindViewDataFailure
        );
    }

    private void bindViewDataSuccess(HeaderModelView model) {
        mHeaderModelView = model;

        if (mView != null) {
            mView.onBindViewData(model);
        }
        hideProgressView();
    }

    private void bindViewDataFailure(Throwable throwable) {
        hideProgressView();
        proccessInputData(throwable);
    }

    private void proccessInputData(Throwable throwable) {
        if (throwable instanceof InputDataException) {
            ArrayList<Error> errorList = ((InputDataException) throwable).getErrorList();
            if (!CollectionUtils.isEmpty(errorList)) {
                Error error = errorList.get(0);
                showErrorView(error.getMessage());

                return;
            }
        }
        showErrorView(throwable);
    }
}
