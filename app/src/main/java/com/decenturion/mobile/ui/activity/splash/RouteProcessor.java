package com.decenturion.mobile.ui.activity.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.BuildConfig;
import com.decenturion.mobile.ui.activity.sign.SignActivity;
import com.decenturion.mobile.ui.fragment.seller.main.view.SellerProfileFragment;

public class RouteProcessor {

    public RouteProcessor() {
    }

    public Route routeProcess(@NonNull Intent intent) {
        Uri uri = intent.getData();
        if (uri == null) {
            return null;
        }

        String urlStr = uri.toString();
        String encodedPath = uri.getEncodedPath();

        /*
          Подтверждение почты при регистрации.
          Только для пользователя с шагом < 2
         */
        if (urlStr.matches(BuildConfig.HOST + "/confirm-email\\?token=.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/confirm-email\\?token=.*")) {
            String token = uri.getQueryParameter("token");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.CONFIRM_EMAIL_TOKEN, token);
            Route route = new Route(Route.TypeRoute.CONFIRM_EMAIL);
            route.setBundle(bundle);
            return route;
        } else

        /*
          Сброс пароля, если пользователь забыл и хочет восстановить пароль.
          Только для пользователя с шагом < 2
         */
        if (urlStr.matches(BuildConfig.HOST + "/reset-password\\?token=.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/reset-password\\?token=.*")) {
            String token = uri.getQueryParameter("token");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.RESET_TOKEN, token);
            Route route = new Route(Route.TypeRoute.RESET_PASSWORD);
            route.setBundle(bundle);
            return route;
        } else

        /*
          Переход по приглашению гражданина, код используется для регистрации по реферальной программе.
          Код активируется до регистрации почты и получения куков.
          Полученный код активации указывается при регистрации почты.
          Только для пользователя с шагом < 2
         */
        if (urlStr.matches(BuildConfig.HOST + "/signup/.*-.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/signup/.*-.*")) {
            String inviteToken = encodedPath.replaceAll("^.*signup/", "");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.INVITE_TOKEN, inviteToken);
            Route route = new Route(Route.TypeRoute.SINGLE_INVITE);
            route.setBundle(bundle);
            return route;
        } else

        /*
          Переход по реферальной ссылке гражданина, код используется для регистрации по реферальной программе.
          Код активируется до регистрации почты и получения куков.
          Полученный код активации указывается при регистрации почты.
          Только для пользователя с шагом < 2
         */
        if (urlStr.matches(BuildConfig.HOST + "/signup/.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP+ "/signup/.*")) {
            String referallKey = encodedPath.replaceAll("^.*signup/", "");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.REFERALL_KEY, referallKey);
            Route route = new Route(Route.TypeRoute.REFERRAL_KEY);
            route.setBundle(bundle);
            return route;
        } else

        /*
          Переход по ссылке баннера гражданина, код используется для регистрации по реферальной программе.
          Код активируется только после регистрации почты и получения куков
          Только для пользователя с шагом >= 2 && шагом < 4
         */
        if (urlStr.matches(BuildConfig.HOST + "/promo/.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/promo/.*")) {
            String inviteToken = encodedPath.replaceAll("^.*promo/", "");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.BANNER_REFERALL_KEY, inviteToken);
            Route route = new Route(Route.TypeRoute.BANNER_REFERRAL_KEY);
            route.setBundle(bundle);
            return route;
        } else

        /*
          Переход по ссылке платного инвайта гражданина, код используется для регистрации по реферальной программе.
          Код активируется только после регистрации почты и получения куков
          Только для пользователя с шагом >= 2 && шагом < 4
         */
        if (urlStr.matches(BuildConfig.HOST + "/r/.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/r/.*")) {
            String inviteToken = encodedPath.replaceAll("^.*r/", "");
            Bundle bundle = new Bundle();
            bundle.putString(SignActivity.PAY_REFERALL_KEY, inviteToken);
            Route route = new Route(Route.TypeRoute.PAY_REFERRAL_KEY);
            route.setBundle(bundle);
            return route;
        }

        /*
          Переход по ссылке лэндинга гражданина
         */
        if (urlStr.matches(BuildConfig.HOST + "/.*-.*") ||
                urlStr.matches(BuildConfig.HOST_HTTP + "/.*-.*")) {
            Bundle bundle = new Bundle();
            String uuid = urlStr.replace(BuildConfig.HOST + "/", "");
            uuid = uuid.replace(BuildConfig.HOST_HTTP + "/", "");
            bundle.putString(SellerProfileFragment.SELLER_UUID, uuid);
            Route route = new Route(Route.TypeRoute.CITIZEN);
            route.setBundle(bundle);
            return route;
        }

        return null;
    }
}
