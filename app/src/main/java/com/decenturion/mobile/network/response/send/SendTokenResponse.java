package com.decenturion.mobile.network.response.send;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendTokenResponse extends BaseResponse {

    @JsonProperty("result")
    private SendTokenResult mResult;

    public SendTokenResponse() {
    }

    @JsonIgnore
    public SendTokenResult getResult() {
        return mResult;
    }
}
