package com.decenturion.mobile.ui.fragment.seller.trade.model;

import com.decenturion.mobile.database.model.OrmTokenModel;
import com.decenturion.mobile.network.response.model.Trade;
import com.decenturion.mobile.network.response.model.WrapperTrade;

public class SellerTraideModel {

    private String mTraidId;
    private String mTraidDate;
    private String mTraidAmount;
    private String mCoin;
    private String mTraidStatus;

    public SellerTraideModel() {
    }

    public SellerTraideModel(OrmTokenModel model) {
    }

    public SellerTraideModel(WrapperTrade d) {
        Trade trade = d.getTrade();
        mTraidId = trade.getUuid();
        mTraidDate = trade.getCreatedAt();
        mTraidAmount = trade.getAmount();
        mCoin = trade.getCoin();
        mTraidStatus = trade.getStatus();
    }

    public String getTraidId() {
        return mTraidId;
    }

    public String getTraidDate() {
        return mTraidDate;
    }

    public String getTraidAmount() {
        return mTraidAmount;
    }

    public String getCoin() {
        return mCoin;
    }

    public String getTraidStatus() {
        return mTraidStatus;
    }

    public void setTraidId(String traidId) {
        mTraidId = traidId;
    }

    public void setTraidDate(String traidDate) {
        mTraidDate = traidDate;
    }

    public void setTraidAmount(String traidAmount) {
        mTraidAmount = traidAmount;
    }

    public void setCoin(String coin) {
        mCoin = coin;
    }

    public void setTraidStatus(String traidStatus) {
        mTraidStatus = traidStatus;
    }
}