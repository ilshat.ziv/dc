package com.decenturion.mobile.business.validate;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.email.ConfirmEmailResponse;
import com.decenturion.mobile.network.response.ResendEmailResponse;
import com.decenturion.mobile.ui.fragment.validate.model.ResendEmailModelView;

import rx.Observable;

public interface IValidEmailInteractor {

    Observable<ResendEmailResponse> resendEmail(@NonNull String username);

    Observable<ConfirmEmailResponse> confirmEmail(String token);

    Observable<ResendEmailModelView> getEmailInfo();
}
