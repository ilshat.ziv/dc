package com.decenturion.mobile.network.response.delivery.skip;

import com.decenturion.mobile.network.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SkipDeliveryResponse extends BaseResponse {

    @JsonProperty("result")
    private SkipDeliveryResult mResult;

    public SkipDeliveryResponse() {
    }

    @JsonIgnore
    public SkipDeliveryResult getResult() {
        return mResult;
    }
}
