package com.decenturion.mobile.business.validate;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.decenturion.mobile.app.prefs.IPrefsManager;
import com.decenturion.mobile.app.prefs.client.ClientPrefOptions;
import com.decenturion.mobile.app.prefs.client.IClientPrefsManager;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.request.ConfirmEmailModel;
import com.decenturion.mobile.network.request.ResendEmailModel;
import com.decenturion.mobile.network.response.email.ConfirmEmailResponse;
import com.decenturion.mobile.network.response.email.ConfirmEmailResult;
import com.decenturion.mobile.network.response.ResendEmailResponse;
import com.decenturion.mobile.network.response.model.Resident;
import com.decenturion.mobile.repository.resident.IResidentRepository;
import com.decenturion.mobile.ui.fragment.validate.model.ResendEmailModelView;
import com.decenturion.mobile.utils.LoggerUtils;

import rx.Observable;

public class ValidEmailInteractor implements IValidEmailInteractor {

    private IResidentClient mIResidentClient;
    private IResidentRepository mIResidentRepository;
    private IPrefsManager mIPrefsManager;

    public ValidEmailInteractor(IResidentClient IResidentClient,
                                IResidentRepository iResidentRepository,
                                IPrefsManager iPrefsManager) {
        mIResidentClient = IResidentClient;
        mIResidentRepository = iResidentRepository;
        mIPrefsManager = iPrefsManager;
    }

    @Override
    public Observable<ResendEmailResponse> resendEmail(@NonNull String username) {
        return isVaildUserName(username)
                .map(s -> {
                    ResendEmailModel model = new ResendEmailModel();
                    model.setUsername(s);
                    return model;
                })
                .flatMap(m -> mIResidentClient.resendEmail(m));
    }

    private Observable<String> isVaildUserName(@NonNull String username) {
        return Observable.just(username)
                .map(s -> {
                    if (TextUtils.isEmpty(s)) {
                        throw new RuntimeException("Email is empty");
                    }
                    return s;
                });
    }

    @Override
    public Observable<ConfirmEmailResponse> confirmEmail(String token) {
        return mIResidentClient.confirmEmail(new ConfirmEmailModel(token))
                .doOnNext(d -> {
                    ConfirmEmailResult result = d.getResult();

                    Resident resident = result.getResident();
                    IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
                    iClientPrefsManager.setParam(ClientPrefOptions.Keys.SIGN_STEP, resident.getStep());

                    try {
                        mIResidentRepository.saveLocale(result.getLocale());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    try {
                        mIResidentRepository.saveResidentInfo(resident);
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    try {
                        mIResidentRepository.savePassportInfo(result.getPassport());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    try {
                        mIResidentRepository.savePhotoInfo(result.getPhoto());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                    try {
                        mIResidentRepository.saveSettings(result.getSettings());
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
//                    try {
//                        mIResidentRepository.saveWallets(result.getWalletList());
//                    } catch (Exception e) {
//                        LoggerUtils.exception(e);
//                    }
                });
    }

    @Override
    public Observable<ResendEmailModelView> getEmailInfo() {
//        return Observable.just(mIResidentRepository.getResidentInfo())
//                .map(d -> {
//                    String email = d.getEmail();
//                    return new ResendEmailModelView(email);
//                })

        IClientPrefsManager iClientPrefsManager = mIPrefsManager.getIClientPrefsManager();
        String email = iClientPrefsManager.getParam(ClientPrefOptions.Keys.RESIDENT_EMAIL, "");

        return Observable.just(email)
                .doOnNext(d -> {
                    if (TextUtils.isEmpty(d)) {
                        throw new RuntimeException("Email is empty");
                    }
                })
                .doOnError(throwable -> {
                    try {
                        mIResidentRepository.clearData();
                    } catch (Exception e) {
                        LoggerUtils.exception(e);
                    }
                })
                .map(ResendEmailModelView::new);
    }


}
