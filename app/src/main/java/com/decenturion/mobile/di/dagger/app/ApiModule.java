package com.decenturion.mobile.di.dagger.app;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.client.device.DeviceClient;
import com.decenturion.mobile.network.client.device.IDeviceClient;
import com.decenturion.mobile.network.client.ministry.IMinistryClient;
import com.decenturion.mobile.network.client.ministry.MinistryClient;
import com.decenturion.mobile.network.client.referral.IReferralClient;
import com.decenturion.mobile.network.client.referral.ReferralClient;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.network.client.resident.ResidentClient;
import com.decenturion.mobile.network.client.score.IScoreClient;
import com.decenturion.mobile.network.client.score.ScoreClient;
import com.decenturion.mobile.network.client.token.ITokenClient;
import com.decenturion.mobile.network.client.token.TokenClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApiModule {

    /* Resident */

    @Singleton
    @Provides
    IResidentClient provideIResidentClient(@NonNull NetworkModule module) {
        return new ResidentClient(module).get();
    }

    /* Token */

    @Singleton
    @Provides
    ITokenClient provideITokenClient(@NonNull NetworkModule module) {
        return new TokenClient(module).get();
    }

    /* Device */

    @Singleton
    @Provides
    IDeviceClient provideIDeviceClient(@NonNull NetworkModule module) {
        return new DeviceClient(module).get();
    }

    /* MinistryProduct */

    @Singleton
    @Provides
    IMinistryClient provideIMinistryClient(@NonNull NetworkModule module) {
        return new MinistryClient(module).get();
    }

    /* Referral program */

    @Singleton
    @Provides
    IReferralClient provideIReferralClient(@NonNull NetworkModule module) {
        return new ReferralClient(module).get();
    }

    /* Scores */

    @Singleton
    @Provides
    IScoreClient provideIScoreClient(@NonNull NetworkModule module) {
        return new ScoreClient(module).get();
    }
}
