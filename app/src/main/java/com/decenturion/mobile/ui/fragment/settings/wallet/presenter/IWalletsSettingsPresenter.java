package com.decenturion.mobile.ui.fragment.settings.wallet.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.settings.wallet.view.IWalletsSettingsView;

public interface IWalletsSettingsPresenter extends IPresenter<IWalletsSettingsView> {

    void bindViewData();

    void saveWallets(@NonNull String btcAddress,
                     @NonNull String ethAddress,
                     @NonNull String twofacode);

    void onSaveInstanceState(@NonNull Bundle bundle,
                             @NonNull String btcAddress,
                             @NonNull String ethAddress,
                             @NonNull String twofacode);

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
