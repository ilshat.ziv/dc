package com.decenturion.mobile.ui.fragment.ministry.honorary.view;

import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.view.IScreenFragmentView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.CoinModelView;
import com.decenturion.mobile.ui.fragment.ministry.product.model.MinistryProductModelView;

public interface IMinistryHonoraryView extends IScreenFragmentView {

    void onBindViewData(@NonNull CoinModelView model1, @NonNull MinistryProductModelView model);

    void onBindPaymentViewData(@NonNull MinistryProductModelView model);
}
