package com.decenturion.mobile.business.restore;

import android.support.annotation.NonNull;

import com.decenturion.mobile.network.response.reset.ResetPasswordResult;
import com.decenturion.mobile.network.response.restore.RestorePassResponse;

import rx.Observable;

public interface IRestorePassInteractor {

    Observable<RestorePassResponse> restorePass(@NonNull String contact, String captcha);

    Observable<ResetPasswordResult> resetPass(@NonNull String token,
                                              @NonNull String newPassword, @NonNull String retypePassword);
}
