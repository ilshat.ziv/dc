package com.decenturion.mobile.business.settings.security;

import com.decenturion.mobile.ui.fragment.settings.security.model.SecurityModelView;

import rx.Observable;

public interface ISecurityInteractor {

    Observable<SecurityModelView> getSettingsInfo();

    Observable<SecurityModelView> saveChanges(boolean isShowName, boolean isShowBirth,
                                     boolean isShowPhoto, boolean isShowTokens,
                                     boolean isShowMoney, boolean isShowPower,
                                     boolean isShowGlory,

                                              boolean isEnabledGfa, boolean isEnableGfa,
                                              String secretString, String secretCode);
}
