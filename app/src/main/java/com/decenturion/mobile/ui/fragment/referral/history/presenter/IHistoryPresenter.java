package com.decenturion.mobile.ui.fragment.referral.history.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.decenturion.mobile.ui.architecture.presenter.IPresenter;
import com.decenturion.mobile.ui.fragment.referral.history.view.IHistoryView;

public interface IHistoryPresenter extends IPresenter<IHistoryView> {

    void bindViewData(String type);

    void onBottomScrolled();

    void onViewStateRestored(@NonNull Bundle savedInstanceState);
}
