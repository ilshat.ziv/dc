package com.decenturion.mobile.network.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckReferallModel {

    @JsonProperty("uuid")
    private String mReferallKey;

    public CheckReferallModel() {
    }

    @JsonIgnore
    public CheckReferallModel(String referallKey) {
        mReferallKey = referallKey;
    }

    @JsonIgnore
    public void setReferallKey(@NonNull String referallKey) {
        mReferallKey = referallKey;
    }
}
