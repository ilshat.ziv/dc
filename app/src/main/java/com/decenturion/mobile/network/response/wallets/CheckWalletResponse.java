package com.decenturion.mobile.network.response.wallets;

import com.decenturion.mobile.network.response.BaseResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckWalletResponse extends BaseResponse {

    @JsonProperty("result")
    private CheckWalletResult mResult;

    public CheckWalletResponse() {
    }

    @JsonIgnore
    public CheckWalletResult getResult() {
        return mResult;
    }
}
