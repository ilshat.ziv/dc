package com.decenturion.mobile.di.dagger.tokendetails.info;

import com.decenturion.mobile.app.localisation.ILocalistionManager;
import com.decenturion.mobile.business.tokendetails.info.ITokenInfoInteractor;
import com.decenturion.mobile.business.tokendetails.info.TokenInfoInteractor;
import com.decenturion.mobile.database.OrmHelper;
import com.decenturion.mobile.network.client.resident.IResidentClient;
import com.decenturion.mobile.repository.token.ITokenRepository;
import com.decenturion.mobile.repository.token.TokenRepository;
import com.decenturion.mobile.ui.fragment.token.info.v1.presenter.ITokenInfoPresenter;
import com.decenturion.mobile.ui.fragment.token.info.v1.presenter.TokenInfoPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TokenInfoModule {

    @Provides
    @TokenInfoScope
    ITokenRepository provideITokenRepository(OrmHelper ormHelper) {
        return new TokenRepository(ormHelper);
    }

    @Provides
    @TokenInfoScope
    ITokenInfoInteractor provideITokenInfoInteractor(IResidentClient IResidentClient,
                                                     ITokenRepository ITokenRepository,
                                                     ILocalistionManager iLocalistionManager) {

        return new TokenInfoInteractor(IResidentClient, ITokenRepository, iLocalistionManager);
    }

    @Provides
    @TokenInfoScope
    ITokenInfoPresenter provideITokenInfoPresenter(ITokenInfoInteractor iTokenInfoInteractor) {
        return new TokenInfoPresenter(iTokenInfoInteractor);
    }
}
